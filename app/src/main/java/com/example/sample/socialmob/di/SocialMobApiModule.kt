/*
 * MIT License
 *
 * Copyright (c) 2020 Shreyas Patil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.example.sample.socialmob.di

import android.content.Context
import com.example.sample.socialmob.di.util.CacheMapperPodCast
import com.example.sample.socialmob.di.util.CacheMapperPodCastHistory
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.BackEndApiChat
import com.example.sample.socialmob.repository.repo.ProfileRepository
import com.example.sample.socialmob.repository.repo.SearchRepository
import com.example.sample.socialmob.repository.repo.SettingRepository
import com.example.sample.socialmob.repository.repo.feed.FeedImageRepository
import com.example.sample.socialmob.repository.repo.feed.FeedRepository
import com.example.sample.socialmob.repository.repo.feed.PhotoGridRepository
import com.example.sample.socialmob.repository.repo.login_module.LoginRepository
import com.example.sample.socialmob.repository.repo.login_module.SignUpRepository
import com.example.sample.socialmob.repository.repo.music.*
import com.example.sample.socialmob.repository.repo.notification.NotificationRepository
import com.example.sample.socialmob.repository.repo.profile.ProfileDetailsRepository
import com.example.sample.socialmob.repository.repo.profile.ProfileUploadRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.rxwebsocket.RxWebSocket
import com.example.sample.socialmob.viewmodel.feed.FeedImageViewModel
import com.example.sample.socialmob.viewmodel.feed.FeedViewModel
import com.example.sample.socialmob.viewmodel.feed.MusicAndPhotoGridViewModel
import com.example.sample.socialmob.viewmodel.login.LoginViewModel
import com.example.sample.socialmob.viewmodel.login.SignUpViewModel
import com.example.sample.socialmob.viewmodel.music_menu.*
import com.example.sample.socialmob.viewmodel.notification.NotificationViewModel
import com.example.sample.socialmob.viewmodel.profile.PostCommentLikeViewModel
import com.example.sample.socialmob.viewmodel.profile.ProfileDetailsViewModel
import com.example.sample.socialmob.viewmodel.profile.ProfileUploadPostViewModel
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import com.example.sample.socialmob.viewmodel.settings.SettingViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Reference
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Dependency injection App module
 *
 */
@Module
@InstallIn(SingletonComponent::class)
class SocialMobApiModule {

    private lateinit var interceptor: HttpLoggingInterceptor
    private lateinit var okHttpClient: OkHttpClient

    private lateinit var interceptorChat: HttpLoggingInterceptor
    private lateinit var okHttpClientChat: OkHttpClient

    @Singleton
    @Provides
    fun provideRetrofitService(): BackEndApi {
        interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
//        interceptor.level = HttpLoggingInterceptor.Level.NONE

        okHttpClient = OkHttpClient.Builder()
            //TODO : Enable Log
            .addInterceptor(interceptor)
            .retryOnConnectionFailure(true)
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .build()
//        okHttpClient.dispatcher.maxRequestsPerHost = 1
        return Retrofit.Builder()
            .baseUrl(RemoteConstant.mBaseUrl)
            .addConverterFactory(
                GsonConverterFactory.create()
            )
            .client(okHttpClient)
            .build()
            .create(BackEndApi::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofitServiceChat(): BackEndApiChat {
        interceptorChat = HttpLoggingInterceptor()
//        interceptorChat.level = HttpLoggingInterceptor.Level.BODY
        interceptorChat.level = HttpLoggingInterceptor.Level.NONE

        okHttpClientChat = OkHttpClient.Builder()
            //TODO : Enable Log
            .addInterceptor(interceptorChat)
            .retryOnConnectionFailure(true)
//            .connectTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .build()
//        okHttpClientChat.dispatcher.maxRequestsPerHost = 1
        return Retrofit.Builder()
            .baseUrl(RemoteConstant.mBaseUrlChat)
            .addConverterFactory(
                GsonConverterFactory.create()
            )
            .client(okHttpClientChat)
            .build()
            .create(BackEndApiChat::class.java)
    }

    var rxWebSocket: RxWebSocket? = null

    @Singleton
    @Provides
    fun provideWebSocket(@ApplicationContext context: Context): RxWebSocket {
        val mJwt =
            SharedPrefsUtils.getStringPreference(MyApplication.application, RemoteConstant.mJwt, "")
        val appId =
            SharedPrefsUtils.getStringPreference(
                context, RemoteConstant.mAppId, ""
            )

        val mWebSocketUrl = RemoteConstant.mBaseUrlWebSocket + mJwt + "&appId=" + appId
        if (mJwt != "") {
            rxWebSocket = RxWebSocket(mWebSocketUrl)
//            rxWebSocket?.connect()
        }
        return rxWebSocket!!
    }


    @Provides
    fun provideMusicDashBoardRepository(backEndApi: BackEndApi,@ApplicationContext context: Context): MusicDashBoardRepository =
        MusicDashBoardRepository(backEndApi,context)

    @Provides
    fun provideMusicDashBoardViewModel(
        musicDashBoardRepository: MusicDashBoardRepository,
        @ApplicationContext context: Context, cacheMapper: CacheMapperTrack,
        cacheMapperPodCastHistory: CacheMapperPodCastHistory, cacheMapperPodCast: CacheMapperPodCast
    ): MusicDashBoardViewModel =
        MusicDashBoardViewModel(
            musicDashBoardRepository, context, cacheMapper, cacheMapperPodCastHistory,
            cacheMapperPodCast
        )

    @Provides
    fun provideArtistRepository(backEndApi: BackEndApi): ArtistRepository =
        ArtistRepository(backEndApi)

    @Provides
    fun provideArtistViewModel(
        artistRepository: ArtistRepository,
        @ApplicationContext context: Context,cacheMapper: CacheMapperTrack
    ): ArtistViewModel =
        ArtistViewModel(artistRepository, context,cacheMapper)

    @Provides
    fun provideRecentlyPlayedRepository(backEndApi: BackEndApi): RecentlyPlayedRepository =
        RecentlyPlayedRepository(backEndApi)

    @Provides
    fun provideRecentlyPlayedViewModel(
        recentlyPlayedRepository: RecentlyPlayedRepository,
        @ApplicationContext context: Context,cacheMapper: CacheMapperTrack
    ): RecentlyPlayedViewModel =
        RecentlyPlayedViewModel(recentlyPlayedRepository, context,cacheMapper)

    @Provides
    fun providePlayListRepository(backEndApi: BackEndApi): PlayListRepository =
        PlayListRepository(backEndApi)

    @Provides
    fun providePlayListViewModel(
        playListRepository: PlayListRepository,
        @ApplicationContext context: Context
    ): PlayListViewModel =
        PlayListViewModel(playListRepository, context)

    @Provides
    fun provideGenreRepository(backEndApi: BackEndApi): GenreRepository =
        GenreRepository(backEndApi)

    @Provides
    fun provideGenreViewModel(
        genreRepository: GenreRepository,
        @ApplicationContext context: Context,cacheMapper: CacheMapperTrack,cacheMapperPodCast: CacheMapperPodCast
    ): GenreListViewModel =
        GenreListViewModel(genreRepository, context,cacheMapper,cacheMapperPodCast)

    @Provides
    fun providePostLikeRepository(backEndApi: BackEndApi): PostLIkeCommentRepository =
        PostLIkeCommentRepository(backEndApi)

    @Provides
    fun providePostLikeViewModel(
        postLIkeCommentRepository: PostLIkeCommentRepository,
        @ApplicationContext context: Context
    ): PostCommentLikeViewModel =
        PostCommentLikeViewModel(postLIkeCommentRepository, context)

    @Provides
    fun provideSettingRepository(backEndApi: BackEndApi): SettingRepository =
        SettingRepository(backEndApi)

    @Provides
    fun provideSettingViewModel(
        settingRepository: SettingRepository,
        @ApplicationContext context: Context
    ): SettingViewModel =
        SettingViewModel(settingRepository, context)

    @Provides
    fun provideNotificationRepository(backEndApi: BackEndApi): NotificationRepository =
        NotificationRepository(backEndApi)

    @Provides
    fun provideNotificationViewModel(
        settingRepository: NotificationRepository,
        @ApplicationContext context: Context
    ): NotificationViewModel =
        NotificationViewModel(settingRepository, context)

    @Provides
    fun provideSearchRepository(backEndApi: BackEndApi): SearchRepository =
        SearchRepository(backEndApi)

    @Provides
    fun provideSearchViewModel(
        settingRepository: SearchRepository,
        @ApplicationContext context: Context,cacheMapper: CacheMapperTrack,
    ): SearchViewModel =
        SearchViewModel(settingRepository, context,cacheMapper)

    @Provides
    fun provideSignUpRepository(backEndApi: BackEndApi): SignUpRepository =
        SignUpRepository(backEndApi)

    @Provides
    fun provideSignUpViewModel(
        signUpRepository: SignUpRepository,
        @ApplicationContext context: Context
    ): SignUpViewModel =
        SignUpViewModel(signUpRepository, context)

    @Provides
    fun provideLoginRepository(backEndApi: BackEndApi): LoginRepository =
        LoginRepository(backEndApi)

    @Provides
    fun provideLoginViewModel(
        loginRepository: LoginRepository,
        @ApplicationContext context: Context
    ): LoginViewModel =
        LoginViewModel(loginRepository, context)


    @Provides
    fun providePhotoGridRepository(backEndApi: BackEndApi): PhotoGridRepository =
        PhotoGridRepository(backEndApi)

    @Provides
    fun providePhotoGridViewModel(
        photoGridRepository: PhotoGridRepository,
        @ApplicationContext context: Context
    ): MusicAndPhotoGridViewModel =
        MusicAndPhotoGridViewModel(photoGridRepository, context)

    @Provides
    fun provideFeedRepository(backEndApi: BackEndApi): FeedRepository =
        FeedRepository(backEndApi)

    @Provides
    fun provideFeedViewModel(
        feedRepository: FeedRepository,
        @ApplicationContext context: Context
    ): FeedViewModel =
        FeedViewModel(feedRepository, context)

    @Provides
    fun provideFeedImageRepository(backEndApi: BackEndApi): FeedImageRepository =
        FeedImageRepository(backEndApi)

    @Provides
    fun provideFeedImageViewModel(
        feedRepository: FeedImageRepository,
        @ApplicationContext context: Context
    ): FeedImageViewModel =
        FeedImageViewModel(feedRepository, context)

    @Provides
    fun provideNowPlayingRepository(backEndApi: BackEndApi): NowPlayingRepository =
        NowPlayingRepository(backEndApi)

    @Provides
    fun provideNowPlayingViewModel(
        nowPlayingRepository: NowPlayingRepository,
        @ApplicationContext context: Context,cacheMapper: CacheMapperTrack,cacheMapperPodCast: CacheMapperPodCast
    ): NowPlayingViewModel =
        NowPlayingViewModel(nowPlayingRepository, context,cacheMapper,cacheMapperPodCast)

    @Provides
    fun provideProfileRepository(backEndApi: BackEndApi): ProfileRepository =
        ProfileRepository(backEndApi)

    @Provides
    fun provideProfileViewModel(
        profileRepository: ProfileRepository, @ApplicationContext context: Context,
        cacheMapper: CacheMapperTrack,cacheMapperPodCast: CacheMapperPodCast
    ): ProfileViewModel =
        ProfileViewModel(profileRepository, context,cacheMapper,cacheMapperPodCast)

    @Provides
    fun provideProfileUploadRepository(backEndApi: BackEndApi): ProfileUploadRepository =
        ProfileUploadRepository(backEndApi)

    @Provides
    fun provideProfileUploadViewModel(
        profileUploadRepository: ProfileUploadRepository, @ApplicationContext context: Context
    ): ProfileUploadPostViewModel =
        ProfileUploadPostViewModel(profileUploadRepository, context)

    @Provides
    fun provideProfileDetailsRepository(backEndApi: BackEndApi): ProfileDetailsRepository =
        ProfileDetailsRepository(backEndApi)

    @Provides
    fun provideProfileDetailsViewModel(
        profileDetailsRepository: ProfileDetailsRepository, @ApplicationContext context: Context
        ,cacheMapper: CacheMapperTrack,cacheMapperPodCast: CacheMapperPodCast
    ): ProfileDetailsViewModel =
        ProfileDetailsViewModel(profileDetailsRepository, context,cacheMapper,cacheMapperPodCast)

//    @Provides
//    fun provideChatRepository(backEndApi: BackEndApiChat): ChatRepository =
//        ChatRepository(backEndApi)
//
//    @Provides
//    fun provideWebSocketChatViewModel(
//        chatRepository: ChatRepository, @ApplicationContext context: Context
//    ): WebSocketChatViewModel =
//        WebSocketChatViewModel(chatRepository, context)

}
