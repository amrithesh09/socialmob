package com.example.sample.socialmob.di.util

import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.view.utils.socialview.Mention
import javax.inject.Inject

/**
 * Mapper function for mention ( Search mention )
 * Mapping a data class to another
 */
class CacheMapperMention
@Inject
constructor() : EntityMapperMention<Profile, Mention> {


    fun mapFromEntityListMention(entities: MutableList<Profile>): List<Mention> {
        return entities.map { mapFromMention(it) }
    }

    override fun mapFromMention(entity: Profile): Mention {
        return Mention(
            entity.username?.trim()!!,
            entity.username?.replace("[^A-Za-z0-9 ]".toRegex(), ""),
            entity.pimage
        )
    }
}