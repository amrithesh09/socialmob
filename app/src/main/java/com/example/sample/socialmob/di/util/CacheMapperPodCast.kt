package com.example.sample.socialmob.di.util

import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.utils.RemoteConstant
import javax.inject.Inject

/**
 * Mapper function for PodCast episode tack
 * Mapping a data class to another
 */
class CacheMapperPodCast
@Inject
constructor() : EntityMapperPodCast<RadioPodCastEpisode, PlayListDataClass> {

    override fun mapFromPlayListDataClassPodCast(
        entity: RadioPodCastEpisode,
        playListName: String
    ): PlayListDataClass {
        return PlayListDataClass(
            JukeBoxTrackId = entity.number_id ?: "",
            JukeBoxCategoryName = entity.podcast_name ?: "",
            TrackName = entity.episode_name ?: "",
            Author = entity.podcast_name ?: "",
            TrackFile = entity.source_path ?: "",
            AllowOffline = "0",
            Length = "",
            TrackImage = RemoteConstant.getImageUrlFromWidth(entity.cover_image ?: "", 400),
            percentage = entity.percentage ?: "",
            Millisecond = entity.Millisecond ?: "",
            Favorite = "",
            mExtraId = entity._id ?: "",
            mExtraIdPodCast = entity.podcast_id ?: "",
            artistId = "",
            albumName = "",
            albumId = "",
            playCount = entity.play_count ?: "", playListName = playListName
        )
    }

    fun mapFromEntityListPodCast(
        entities: MutableList<RadioPodCastEpisode>,
        playListName: String
    ): List<PlayListDataClass> {
        return entities.map { mapFromPlayListDataClassPodCast(it, playListName) }
    }


}
