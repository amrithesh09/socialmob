package com.example.sample.socialmob.di.util

import com.example.sample.socialmob.model.music.PodcastHistoryTrack
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.utils.RemoteConstant
import javax.inject.Inject
/**
 * Mapper function for PodCast history class
 * Mapping a data class to another
 */
class CacheMapperPodCastHistory
@Inject
constructor() : EntityMapperPodCastHistory<PodcastHistoryTrack, PlayListDataClass> {

    override fun mapFromPlayListDataClassPodCastHistory(
        entity: PodcastHistoryTrack,
        playListName: String
    ): PlayListDataClass {
        return PlayListDataClass(
            JukeBoxTrackId = entity.EpisodeId ?: "",
            JukeBoxCategoryName = entity.PodcastName ?: "",
            TrackName = entity.EpisodeName ?: "",
            Author = entity.PodcastName ?: "",
            TrackFile = entity.EpisodeSourcePath ?: "",
            AllowOffline = "0",
            Length = "",
            TrackImage = RemoteConstant.getImageUrlFromWidth(entity.ThumbnailPath?:"", 400),
            percentage = entity.percentage ?: "",
            Millisecond = entity.Millisecond ?: "",
            Favorite = "",
            mExtraId = entity.mExtraId ?: "",
            mExtraIdPodCast = entity.podcast_id ?: "",
            artistId = "",
            albumName = "",
            albumId = "",
            playCount = "",playListName = playListName
        )
    }

    fun mapFromEntityListPodCastHistory(
        entities: MutableList<PodcastHistoryTrack>,
        playListName: String
    ): List<PlayListDataClass> {
        return entities.map { mapFromPlayListDataClassPodCastHistory(it,playListName) }
    }


}

