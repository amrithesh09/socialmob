package com.example.sample.socialmob.di.util


import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.utils.RemoteConstant
import javax.inject.Inject
/**
 * Mapper function for Track common
 * Mapping a data class to another
 */
class CacheMapperTrack
@Inject
constructor() : EntityMapperTrack<TrackListCommon, PlayListDataClass> {

    override fun mapFromPlayListDataClassTrack(
        entity: TrackListCommon,
        playListName: String
    ): PlayListDataClass {
        return PlayListDataClass(
            JukeBoxTrackId = entity.numberId ?: "",
            JukeBoxCategoryName = entity.genre?.genreName ?: "",
            TrackName = entity.trackName ?: "",
            Author = entity.artist?.artistName ?: "",
            TrackFile = entity.media?.trackFile ?: "",
            AllowOffline = entity.allowOfflineDownload ?: "",
            Length = "",
            TrackImage = RemoteConstant.getImageUrlFromWidth(entity.media?.coverFile, 400),
            percentage = "",
            Millisecond = "",
            Favorite = entity.favorite ?: "",
            mExtraId = entity.id ?: "",
            mExtraIdPodCast = "",
            artistId = entity.artist?.id ?: "",
            albumName = entity.album?.albumName ?: "",
            albumId = entity.album?.id ?: "",
            playCount = entity.playCount ?: "",playListName = playListName
        )
    }

    override fun mapToEntity(domainModel: PlayListDataClass): TrackListCommon {
        return TrackListCommon()
    }

    fun mapFromEntityList(entities: MutableList<TrackListCommon>,playListName:String): List<PlayListDataClass> {
        return entities.map { mapFromPlayListDataClassTrack(it,playListName) }
    }


}











