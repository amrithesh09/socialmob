package com.example.sample.socialmob.di.util

interface EntityMapperMention  <Entity, DomainModel>{
    fun mapFromMention(entity: Entity): DomainModel
}