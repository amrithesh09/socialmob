package com.example.sample.socialmob.di.util

interface EntityMapperPodCast <Entity, DomainModel>{

    fun mapFromPlayListDataClassPodCast(entity: Entity, playListName: String): DomainModel

}