package com.example.sample.socialmob.di.util

interface EntityMapperPodCastHistory <Entity, DomainModel>{

    fun mapFromPlayListDataClassPodCastHistory(entity: Entity, playListName: String): DomainModel

}