package com.example.sample.socialmob.di.util

interface EntityMapperTrack <Entity, DomainModel>{

    fun mapFromPlayListDataClassTrack(entity: Entity, playListName: String): DomainModel

    fun mapToEntity(domainModel: DomainModel): Entity

}