package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ArticleCommentResponseModel {
    @SerializedName("status")
    @Expose
     val status: String? = null
    @SerializedName("code")
    @Expose
     val code: Int? = null
    @SerializedName("payload")
    @Expose
     val payload: ArticleCommentResponseModelPayload? = null
    @SerializedName("now")
    @Expose
     val now: String? = null
}
