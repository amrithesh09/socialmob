package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ArticleCommentResponseModelPayload {
    @SerializedName("comments")
    @Expose
    val comments: List<SingleArticleApiModelPayloadComment>? = null
    @SerializedName("commentsCount")
    @Expose
    val commentsCount: Int? = null
}
