package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ArticleDashBoardResponseModelPayload {

    @SerializedName("HalfscreenDashboard")
    @Expose
    var articles: List<HalfscreenDashboard>? = null

}

//@Entity(tableName = "userArticleDashboard")
//data class ArticleDashBoardResponseModelPayload(
//    @PrimaryKey(autoGenerate = true) val id: Int,
//    @ColumnInfo(name = "Title") val Title: String? = null,
//    @ColumnInfo(name = "Link") val Link: String? = null,
//    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null
//
//)

//class  {
//    @SerializedName("Title")
//    @Expose
//    var title: String? = null
//    @SerializedName("Link")
//    @Expose
//    var link: String? = null
//    @SerializedName("SourcePath")
//    @Expose
//    var sourcePath: String? = null
//}
