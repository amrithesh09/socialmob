package com.example.sample.socialmob.model.article

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "userRecommendedBookmark")
data class ArticleData(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "Bookmarked") var Bookmarked: String? = null,
    @ColumnInfo(name = "ContentId") val ContentId: String? = null,
    @ColumnInfo(name = "CategoryId") val CategoryId: String? = null,
    @ColumnInfo(name = "Title") val Title: String? = null,
    @ColumnInfo(name = "Description") val Description: String? = null,
    @ColumnInfo(name = "CreatedDate") val CreatedDate: String? = null,
    @ColumnInfo(name = "UpdatedDate") val UpdatedDate: String? = null,
    @ColumnInfo(name = "Status") val Status: String? = null,
    @ColumnInfo(name = "Order") val Order: String? = null,
    @ColumnInfo(name = "Author") var Author: String? = null,
    @ColumnInfo(name = "ContentType") val ContentType: String? = null,
    @ColumnInfo(name = "ContentDescription") val ContentDescription: String? = null,
//    @ColumnInfo(name = "MappedGender") val MappedGender: String? = null,
//    @ColumnInfo(name = "WriterId") val WriterId: String? = null,
//    @ColumnInfo(name = "Assignee") val Assignee: String? = null,
//    @ColumnInfo(name = "ProcessStatus") val ProcessStatus: String? = null,
//    @ColumnInfo(name = "Deleted") val Deleted: String? = null,
//    @ColumnInfo(name = "PublishedStatus") val PublishedStatus: String? = null,
    @ColumnInfo(name = "PublishedDate") val PublishedDate: String? = null,
//    @ColumnInfo(name = "Uid") val Uid: String? = null,
    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null

)
//
//class ArticleData {
//    @SerializedName("Bookmarked")
//    @Expose
//    var bookmarked: Boolean? = null
//    @SerializedName("ContentId")
//    @Expose
//    var contentId: Int? = null
//    @SerializedName("CategoryId")
//    @Expose
//    var categoryId: Int? = null
//    @SerializedName("Title")
//    @Expose
//    var title: String? = null
//    @SerializedName("Description")
//    @Expose
//    var description: String? = null
//    @SerializedName("CreatedDate")
//    @Expose
//    var createdDate: String? = null
//    @SerializedName("UpdatedDate")
//    @Expose
//    var updatedDate: String? = null
//    @SerializedName("Status")
//    @Expose
//    var status: Boolean? = null
//    @SerializedName("Order")
//    @Expose
//    var order: Int? = null
//    @SerializedName("Author")
//    @Expose
//    var author: String? = null
//    @SerializedName("ContentType")
//    @Expose
//    var contentType: String? = null
//    @SerializedName("ContentDescription")
//    @Expose
//    var contentDescription: String? = null
//    @SerializedName("MappedGender")
//    @Expose
//    var mappedGender: String? = null
//    @SerializedName("WriterId")
//    @Expose
//    var writerId: Any? = null
//    @SerializedName("Assignee")
//    @Expose
//    var assignee: Any? = null
//    @SerializedName("ProcessStatus")
//    @Expose
//    var processStatus: Any? = null
//    @SerializedName("Deleted")
//    @Expose
//    var deleted: Boolean? = null
//    @SerializedName("PublishedStatus")
//    @Expose
//    var publishedStatus: Int? = null
//    @SerializedName("PublishedDate")
//    @Expose
//    var publishedDate: String? = null
//    @SerializedName("Uid")
//    @Expose
//    var uid: Any? = null
//    @SerializedName("SourcePath")
//    @Expose
//    var sourcePath: String? = null
//    @SerializedName("Tags")
//    @Expose
//    var tags: List<ForYouTag>? = null
//}
