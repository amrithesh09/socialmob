package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BookMarkedArticleResponseModel {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: BookMarkedArticleResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null
}
