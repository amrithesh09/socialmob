package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BookMarkedArticleResponseModelPayload {
    @SerializedName("Bookmarks")
    @Expose
    var bookmarks: List<Bookmark>? = null
}
