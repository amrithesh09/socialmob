package com.example.sample.socialmob.model.article

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "userArticleBookmark")
data class Bookmark(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "Bookmarked") var Bookmarked: String? = null,
    @ColumnInfo(name = "ContentId") val ContentId: String? = null,
    @ColumnInfo(name = "Title") val Title: String? = null,
    @ColumnInfo(name = "Description") val Description: String? = null,
    @ColumnInfo(name = "CreatedDate") val CreatedDate: String? = null,
    @ColumnInfo(name = "PublishedDate") val PublishedDate: String? = null,
    @ColumnInfo(name = "Author") val Author: String? = null,
    @ColumnInfo(name = "ContentDescription") val ContentDescription: String? = null,
    @ColumnInfo(name = "MappedGender") val MappedGender: String? = null,
    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null
//    @TypeConverters(ProductTypeConverter::class)
//    val Tags: List<UserReadArticleTag>? = null

)

//class Bookmark {
//
//    @SerializedName("Bookmarked")
//    @Expose
//    var bookmarked: Boolean? = null
//    @SerializedName("ContentId")
//    @Expose
//    var contentId: Int? = null
//    @SerializedName("Title")
//    @Expose
//    var title: String? = null
//    @SerializedName("Description")
//    @Expose
//    var description: String? = null
//    @SerializedName("PublishedDate")
//    @Expose
//    var publishedDate: String? = null
//    @SerializedName("Author")
//    @Expose
//    var author: String? = null
//    @SerializedName("ContentDescription")
//    @Expose
//    var contentDescription: String? = null
//    @SerializedName("MappedGender")
//    @Expose
//    var mappedGender: String? = null
//    @SerializedName("SourcePath")
//    @Expose
//    var sourcePath: String? = null
//    @SerializedName("Tags")
//    @Expose
//    var tags: List<BookmarkTag>? = null
//
//}