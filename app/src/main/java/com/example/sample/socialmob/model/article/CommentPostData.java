package com.example.sample.socialmob.model.article;

import java.util.List;

public class CommentPostData {
    public String comment;
    public List<Mention> mentions;

    public CommentPostData(String comment, List<Mention> mentions) {
        this.comment = comment;
        this.mentions = mentions;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<Mention> getMentions() {
        return mentions;
    }

    public void setMentions(List<Mention> mentions) {
        this.mentions = mentions;
    }


    private class Mention {
        public String Name;
        public String text;
        public String _id;
        public String thumbnail;

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int position;

        public Mention(String name, String text, String _id, String thumbnail, int position) {
            Name = name;
            this.text = text;
            this._id = _id;
            this.thumbnail = thumbnail;
            this.position = position;
        }


    }
}
