package com.example.sample.socialmob.model.article

import com.example.sample.socialmob.model.article.ArticleData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForYouArticleResponseModelPayload {
    @SerializedName("articles")
    @Expose
    var articles: List<ArticleData>? = null

}
