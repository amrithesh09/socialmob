package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForYouTag {
    @SerializedName("TagId")
    @Expose
    var tagId: Int? = null
    @SerializedName("TagName")
    @Expose
    var tagName: String? = null
}
