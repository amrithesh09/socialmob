package com.example.sample.socialmob.model.article

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "userArticleDashboard")
data class HalfscreenDashboard(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "AdvertisementId") val AdvertisementId: String? = null,
    @ColumnInfo(name = "Link") val Link: String? = null,
    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null

)
//class HalfscreenDashboard {
//    @SerializedName("AdvertisementId")
//    @Expose
//    private var advertisementId: Int? = null
//    @SerializedName("Link")
//    @Expose
//    private var link: String? = null
//    @SerializedName("SourcePath")
//    @Expose
//    private var sourcePath: String? = null
//
//
//}
