package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HistoryArticleResponseModelPayload {
    @SerializedName("UserReadArticles")
    @Expose
    var userReadArticles: List<UserReadArticle>? = null
}
