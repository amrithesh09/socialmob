package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MentionPeople {
    @SerializedName("text")
    @Expose
    val text: String? = null
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("position")
    @Expose
    val position: Int? = null
    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null
}
