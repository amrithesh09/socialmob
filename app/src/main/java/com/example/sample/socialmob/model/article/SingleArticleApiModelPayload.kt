package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleArticleApiModelPayload {
    @SerializedName("content")
    @Expose
    var content: SingleArticleApiModelPayloadContent? = null
    @SerializedName("similarArticles")
    @Expose
    var similarArticles: List<SingleArticleApiModelPayloadSimilarArticle>? = null
    @SerializedName("comments")
    @Expose
    var comments: List<SingleArticleApiModelPayloadComment>? = null


}
