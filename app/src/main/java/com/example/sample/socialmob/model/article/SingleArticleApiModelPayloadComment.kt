package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleArticleApiModelPayloadComment {
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("profileId")
    @Expose
    var profileId: String? = null
    @SerializedName("contentId")
    @Expose
    var contentId: Int? = null
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("created_date")
    @Expose
    var createdDate: String? = null
    @SerializedName("profile")
    @Expose
    var profile: SingleArticleApiModelPayloadCommentProfile? = null
    @SerializedName("mentions")
    @Expose
    val mentions: List<MentionPeople>? = null
    @SerializedName("own")
    @Expose
    var own: Boolean? = null


}
