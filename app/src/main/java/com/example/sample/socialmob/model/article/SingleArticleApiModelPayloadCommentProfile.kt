package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleArticleApiModelPayloadCommentProfile {
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("Location")
    @Expose
    var location: String? = null
    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
    @SerializedName("celebrityVerified")
    @Expose
    var celebrityVerified: Boolean? = null
    @SerializedName("pimage")
    @Expose
    val pimage: String? = null
}
