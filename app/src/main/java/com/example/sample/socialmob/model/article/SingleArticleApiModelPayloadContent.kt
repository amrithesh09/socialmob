package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleArticleApiModelPayloadContent {
    @SerializedName("ContentId")
    @Expose
    var contentId: String? = null
    @SerializedName("Uid")
    @Expose
    var uid: String? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Description")
    @Expose
    var description: String? = null
    @SerializedName("ContentDescription")
    @Expose
    var contentDescription: String? = null
    @SerializedName("PublishedDate")
    @Expose
    var publishedDate: String? = null
    @SerializedName("Author")
    @Expose
    var author: String? = null
    @SerializedName("Bookmarked")
    @Expose
    var bookmarked: String? = null
    @SerializedName("UserRead")
    @Expose
    var userRead: Boolean? = null
    @SerializedName("SourcePath")
    @Expose
    var sourcePath: String? = null
    @SerializedName("Tags")
    @Expose
    var tags: List<SingleArticleApiModelPayloadContentTag>? = null


}
