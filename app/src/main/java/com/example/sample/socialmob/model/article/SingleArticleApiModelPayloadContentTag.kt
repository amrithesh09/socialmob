package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleArticleApiModelPayloadContentTag {
    constructor(tagId: String?, tagName: String?) {
        this.tagId = tagId
        this.tagName = tagName
    }

    @SerializedName("TagId")
    @Expose
    var tagId: String? = null
    @SerializedName("TagName")
    @Expose
    var tagName: String? = null


}
