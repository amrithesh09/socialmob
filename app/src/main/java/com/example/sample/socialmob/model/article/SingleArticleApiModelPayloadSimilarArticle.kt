package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleArticleApiModelPayloadSimilarArticle {
    @SerializedName("ContentId")
    @Expose
    var contentId: String? = null
    @SerializedName("CategoryId")
    @Expose
    var categoryId: Int? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Description")
    @Expose
    var description: String? = null
    @SerializedName("CreatedDate")
    @Expose
    var createdDate: String? = null
    @SerializedName("UpdatedDate")
    @Expose
    var updatedDate: String? = null
    @SerializedName("Status")
    @Expose
    var status: Boolean? = null
    @SerializedName("Order")
    @Expose
    var order: Int? = null
    @SerializedName("Author")
    @Expose
    var author: String? = null
    @SerializedName("ContentType")
    @Expose
    var contentType: String? = null
    @SerializedName("ContentDescription")
    @Expose
    var contentDescription: String? = null
    @SerializedName("MappedGender")
    @Expose
    var mappedGender: String? = null
    @SerializedName("WriterId")
    @Expose
    var writerId: Any? = null
    @SerializedName("Assignee")
    @Expose
    var assignee: Any? = null
    @SerializedName("ProcessStatus")
    @Expose
    var processStatus: Any? = null
    @SerializedName("Deleted")
    @Expose
    var deleted: Boolean? = null
    @SerializedName("PublishedStatus")
    @Expose
    var publishedStatus: Int? = null
    @SerializedName("PublishedDate")
    @Expose
    var publishedDate: String? = null
    @SerializedName("Uid")
    @Expose
    var uid: Any? = null
    @SerializedName("Bookmarked")
    @Expose
    var bookmarked: String? = null
    @SerializedName("UserRead")
    @Expose
    var userRead: Boolean? = null
    @SerializedName("SourcePath")
    @Expose
    var sourcePath: String? = null


}
