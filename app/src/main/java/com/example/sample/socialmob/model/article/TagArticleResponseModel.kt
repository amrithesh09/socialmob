package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class TagArticleResponseModel {
    @SerializedName("status")
    @Expose
     val status: String? = null
    @SerializedName("code")
    @Expose
     val code: Int? = null
    @SerializedName("payload")
    @Expose
     val payload: TagArticleResponseModelPayload? = null
    @SerializedName("now")
    @Expose
     val now: String? = null
}
