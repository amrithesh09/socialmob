package com.example.sample.socialmob.model.article

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TagArticleResponseModelPayloadTag {
    @SerializedName("TagId")
    @Expose
    val tagId: Int? = null
    @SerializedName("TagName")
    @Expose
    val tagName: String? = null
    @SerializedName("FollowerCount")
    @Expose
    val followerCount: Int? = null
    @SerializedName("LandscapeImage")
    @Expose
    val landscapeImage: String? = null
    @SerializedName("SquareImage")
    @Expose
    val squareImage: String? = null
    @SerializedName("Following")
    @Expose
    var following: String? = null
}
