package com.example.sample.socialmob.model.article

import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.model.search.SearchTagResponseModelPayloadTag
import com.example.sample.socialmob.model.search.SearchArticleResponseModelPayloadContent
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

 class TopItemsModelPayload {
    @SerializedName("profiles")
    @Expose
    val profiles: List<SearchProfileResponseModelPayloadProfile>? = null
    @SerializedName("hashTags")
    @Expose
    val hashTags: List<HashTag>? = null
    @SerializedName("articles")
    @Expose
    val articles: List<SearchArticleResponseModelPayloadContent>? = null
    @SerializedName("tags")
    @Expose
    val tags: List<SearchTagResponseModelPayloadTag>? = null


}
