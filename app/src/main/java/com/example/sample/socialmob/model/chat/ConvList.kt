package com.example.sample.socialmob.model.chat

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.sample.socialmob.viewmodel.feed.LastMessage
import com.example.sample.socialmob.model.login.Profile


@Entity(tableName = "ConvList")
data class ConvList(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "conv_id")
    var conv_id: String? = null,

    @Embedded
    var profile: Profile? = null,

    @Embedded
    var last_message: LastMessage? = null,

    @ColumnInfo(name = "timeStamp")
    var timeStamp: String? = null,

    @ColumnInfo(name = "unread_message_count")
    var unread_message_count: String? = null

)
//class ConvList {
//    @SerializedName("conv_id")
//    @Expose
//    var conv_id: String? = null
//
//    @SerializedName("profile")
//    @Expose
//    var profile: Profile? = null
//
//    @SerializedName("last_message")
//    @Expose
//    var last_message: LastMessage? = null
//
//    @SerializedName("timeStamp")
//    @Expose
//    var timeStamp: String? = null
//
//}