package com.example.sample.socialmob.model.chat

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ConversationListResponse {
    @SerializedName("conv_list")
    @Expose
     val convList: MutableList<ConvList>? = null
}
