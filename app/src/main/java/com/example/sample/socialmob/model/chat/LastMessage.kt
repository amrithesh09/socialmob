package com.example.sample.socialmob.viewmodel.feed

import androidx.room.ColumnInfo
import androidx.room.Embedded
import com.example.sample.socialmob.model.chat.MessageSender


data class  LastMessage (
     @ColumnInfo(name ="msg_id")
    var msg_id: String? = null,

     @ColumnInfo(name ="conv_id")
    var conv_id: String? = null,

     @ColumnInfo(name ="text")
    var text: String? = null,

     @ColumnInfo(name ="msg_type")
    var msg_type: String? = null,

     @ColumnInfo(name ="timestamp")
    var timestamp: String? = null,

     @ColumnInfo(name ="status")
    var status: String? = null,

     @Embedded
    var message_sender: MessageSender? = null
)
