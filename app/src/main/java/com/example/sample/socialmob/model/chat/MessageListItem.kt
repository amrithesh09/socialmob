package com.example.sample.socialmob.model.chat

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MessageListItem")
data class MessageListItem(
    @PrimaryKey(autoGenerate = true) val _id: Int,

    @ColumnInfo(name = "timestamp")
    var timestamp: String? = null,

    @ColumnInfo(name = "message")
    var message: String? = null
)
