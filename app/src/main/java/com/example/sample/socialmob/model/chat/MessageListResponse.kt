package com.example.sample.socialmob.viewmodel.feed

import com.example.sample.socialmob.model.chat.MsgList
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class MessageListResponse {
    @SerializedName("msg_list")
    @Expose
    var msgList: MutableList<MsgList>? = null
}
