package com.example.sample.socialmob.model.chat

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class  MessageSender (
     @ColumnInfo(name ="_id")
    var _id: String? = "",

     @ColumnInfo(name ="username")
     var username: String? = null,

     @ColumnInfo(name ="Name")
    var name: String? = null,

     @ColumnInfo(name ="pimage")
    var pimage: String? = "",

     @ColumnInfo(name ="isown")
    var isown: String? = null

)
