package com.example.sample.socialmob.model.chat

import com.example.sample.socialmob.view.ui.home_module.chat.model.MsgListFile
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.util.*


class MsgList {
    @SerializedName("msg_id")
    @Expose
    var msg_id: String? = null

    @SerializedName("conv_id")
    @Expose
    var conv_id: String? = null

    @SerializedName("text")
    @Expose
    var text: String? = null

    @SerializedName("msg_type")
    @Expose
    var msg_type: String? = null

    @SerializedName("timestamp")
    @Expose
    var timestamp: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("progress")
    @Expose
    var progress: String? = null

    @SerializedName("message_sender")
    @Expose
    var message_sender: MessageSender? = null

    @SerializedName("file")
    @Expose
    var file: MsgListFile? = MsgListFile()

    private val chatTime: Date? = null
}
