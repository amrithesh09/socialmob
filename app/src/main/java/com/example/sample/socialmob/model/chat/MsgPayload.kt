package com.example.sample.socialmob.model.chat

import com.example.sample.socialmob.view.ui.home_module.chat.model.PayloadFile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MsgPayload {
    @SerializedName("user")
    @Expose
    var user: String? = null

    @SerializedName("msg_type")
    @Expose
    var msgType: String? = null

    @SerializedName("text")
    @Expose
    var text: String? = null

    @SerializedName("timestamp")
    @Expose
    var timestamp: Long? = null

    @SerializedName("msg_id")
    @Expose
    var msg_id: String? = null

    @SerializedName("file")
    @Expose
//    var files: List<PayloadFile>? = null
    var file: PayloadFile? = null
}