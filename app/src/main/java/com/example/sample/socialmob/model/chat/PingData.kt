package com.example.sample.socialmob.viewmodel.feed

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class PingData {
    @SerializedName("ping_id")
    @Expose
    var pingId: Int? = null
}
