package com.example.sample.socialmob.model.chat

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SendMessage {
    @SerializedName("user")
    @Expose
    var user: String? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("msg_payload")
    @Expose
    var msgPayload: MsgPayload? = null

    @SerializedName("msg_payload_list")
    @Expose
    var msg_payload_list: List<MsgPayload>? = null


}