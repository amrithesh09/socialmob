package com.example.sample.socialmob.model.chat

import com.example.sample.socialmob.view.ui.home_module.chat.model.AckMsg
import com.example.sample.socialmob.viewmodel.feed.PingData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class WebSocketResponse {

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("from")
    @Expose
    var from: String? = null

    @SerializedName("user")
    @Expose
    var user: String? = null

    @SerializedName("msg_payload")
    @Expose
    var msgPayload: WebSocketResponseMsgPayload? = WebSocketResponseMsgPayload()

    @SerializedName("ping_data")
    @Expose
     val pingData: PingData? = PingData()

    @SerializedName("ack_msg")
    @Expose
    val ackMsg: AckMsg? = AckMsg()

}
