package com.example.sample.socialmob.model.chat

import com.example.sample.socialmob.view.ui.home_module.chat.model.MsgListFile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class WebSocketResponseMsgPayload {
    @SerializedName("msg_id")
    @Expose
    val msgId: String = ""

    @SerializedName("conv_id")
    @Expose
    val convId: String = ""

    @SerializedName("from")
    @Expose
    val from: String = ""

    @SerializedName("to")
    @Expose
    val to: String = ""

    @SerializedName("text")
    @Expose
    val text: String = ""

    @SerializedName("msg_type")
    @Expose
    val msgType: String = ""

    @SerializedName("timestamp")
    @Expose
    val timestamp: String = ""

    @SerializedName("status")
    @Expose
    val status: String = ""

    @SerializedName("Deleted")
    @Expose
    val deleted: Boolean = false

    @SerializedName("ping_id")
    @Expose
    val ping_id: Boolean = false

    @SerializedName("message_sender")
    @Expose
    var message_sender: MessageSender? = MessageSender()

    @SerializedName("file")
    @Expose
    val file: MsgListFile? = null
}
