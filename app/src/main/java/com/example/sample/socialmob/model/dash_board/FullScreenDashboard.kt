package com.example.sample.socialmob.model.dash_board

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "fullScreenDashboard")
data class FullScreenDashboard(
    @PrimaryKey
    @ColumnInfo(name = "RecordId") val RecordId: Int,
    @ColumnInfo(name = "Link") val Link: String,
    @ColumnInfo(name = "Status") val Status: String,
    @ColumnInfo(name = "Position") val Position: String,
    @ColumnInfo(name = "CreatedDate") val CreatedDate: String,
    @ColumnInfo(name = "FileName") val FileName: String,
    @ColumnInfo(name = "SourcePath") val SourcePath: String
)



//class FullScreenDashboard {
//
//    @SerializedName("RecordId")
//    @Expose
//    var recordId: Int? = null
//    @SerializedName("Link")
//    @Expose
//    var link: String? = null
//    @SerializedName("Status")
//    @Expose
//    var status: Boolean? = null
//    @SerializedName("Position")
//    @Expose
//    var position: Int? = null
//    @SerializedName("CreatedDate")
//    @Expose
//    var createdDate: String? = null
//    @SerializedName("FileName")
//    @Expose
//    var fileName: String? = null
//    @SerializedName("SourcePath")
//    @Expose
//    var sourcePath: String? = null
//}
