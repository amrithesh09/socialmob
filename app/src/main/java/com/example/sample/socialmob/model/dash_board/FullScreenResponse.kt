package com.example.sample.socialmob.model.dash_board

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FullScreenResponse {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: FullScreenResponsePayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null
}
