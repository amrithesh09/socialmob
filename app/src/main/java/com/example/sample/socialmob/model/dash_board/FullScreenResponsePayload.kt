package com.example.sample.socialmob.model.dash_board

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FullScreenResponsePayload {

    @SerializedName("FullScreenDashboard")
    @Expose
    var fullScreenDashboard: List<FullScreenDashboard>? = null

}
