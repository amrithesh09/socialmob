package com.example.sample.socialmob.model.dash_board

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.RemoteConstant
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IntroSlidingDashBoardViewModel(application: Application) : AndroidViewModel(application),
    Callback<FullScreenResponse> {

    var mListImages: MutableLiveData<List<FullScreenDashboard>>? = MutableLiveData()

    private var auth: String = ""


    fun getDashBoardImages(mAuth: String) {
        auth = mAuth
        WebServiceClient.client.create(BackEndApi::class.java)
            .getDashBoardApi(mAuth, RemoteConstant.mPlatform)
            .enqueue(this)

    }

    override fun onResponse(
        call: Call<FullScreenResponse>?,
        response: Response<FullScreenResponse>?
    ) {

        when (response!!.code()) {
            200 -> {
                doAsync {
                    mListImages?.postValue(response.body()?.payload?.fullScreenDashboard)
                }
            }

            else -> RemoteConstant.apiError(response.code())
        }


    }

    override fun onFailure(call: Call<FullScreenResponse>?, t: Throwable?) {
        getDashBoardImages(auth)
    }


}