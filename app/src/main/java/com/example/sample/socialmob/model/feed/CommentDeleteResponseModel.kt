package com.example.sample.socialmob.model.feed

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CommentDeleteResponseModel {
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("code")
    @Expose
    val code: Int? = null
    @SerializedName("payload")
    @Expose
    val payload: CommentDeleteResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    val now: String? = null
}
