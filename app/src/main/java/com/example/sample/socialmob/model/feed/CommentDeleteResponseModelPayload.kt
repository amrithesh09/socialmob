package com.example.sample.socialmob.model.feed

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CommentDeleteResponseModelPayload {
    @SerializedName("success")
    @Expose
    val success: Boolean? = null
    @SerializedName("commentsCount")
    @Expose
    val commentsCount: String? = null
}
