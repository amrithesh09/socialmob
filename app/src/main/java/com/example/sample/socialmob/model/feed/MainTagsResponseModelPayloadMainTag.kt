package com.example.sample.socialmob.viewmodel.feed

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MainTagsResponseModelPayloadMainTag {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("name")
    @Expose
    val name: String? = null
}
