package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.feed.SingleFeedResponseModelPayload
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MobCategoryFeedModel {

    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("status")
    @Expose
    var status: MobCategoryFeedStatusModel? = null
    @SerializedName("now")
    @Expose
    var now: String? = null


}
