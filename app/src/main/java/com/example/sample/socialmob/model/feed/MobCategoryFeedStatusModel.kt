package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.feed.SingleFeedResponseModelPayload
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MobCategoryFeedStatusModel {
  
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
}
