package com.example.sample.socialmob.model.feed

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class PostFeedDataMention(
    val _id: String? = null,
    val text: String? = null,
    val position: String? = null
) : Parcelable
