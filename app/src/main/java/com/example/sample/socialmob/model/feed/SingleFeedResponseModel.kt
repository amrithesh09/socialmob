package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.feed.SingleFeedResponseModelPayload
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleFeedResponseModel {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: SingleFeedResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null


}
