package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleFeedResponseModelPayload {
    @SerializedName("post")
    @Expose
    var post: SingleFeedResponseModelPayloadPost? = null
    @SerializedName("comments")
    @Expose
    var comments: List<CommentResponseModelPayloadComment>? = null
    @SerializedName("deleted")
    @Expose
    var deleted: Boolean? = null


}
