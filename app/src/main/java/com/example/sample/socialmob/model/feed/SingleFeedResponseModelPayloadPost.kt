package com.example.sample.socialmob.model.feed

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleFeedResponseModelPayloadPost {
    @SerializedName("actor")
    @Expose
    var actor: SingleFeedResponseModelPayloadPostActor? = null
    @SerializedName("action")
    @Expose
    var action: SingleFeedResponseModelPayloadPostAction? = null

}
