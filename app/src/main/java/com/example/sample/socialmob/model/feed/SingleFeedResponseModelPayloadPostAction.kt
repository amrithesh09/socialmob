package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.profile.OgTags
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPost
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleFeedResponseModelPayloadPostAction {
    @SerializedName("feedId")
    @Expose
    var feedId: String? = null
    @SerializedName("actionId")
    @Expose
    var actionId: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("source")
    @Expose
    var source: String? = null
    @SerializedName("sourceId")
    @Expose
    var sourceId: String? = null
    @SerializedName("created_date")
    @Expose
    var createdDate: String? = null
    @SerializedName("post")
    @Expose
    var post: PersonalFeedResponseModelPayloadFeedActionPost? = null
    @SerializedName("ogTags")
    @Expose
    var ogTags: OgTags? = null

}
