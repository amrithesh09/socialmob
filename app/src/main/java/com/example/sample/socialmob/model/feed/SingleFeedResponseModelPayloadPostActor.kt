package com.example.sample.socialmob.model.feed

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SingleFeedResponseModelPayloadPostActor {
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("ProfileId")
    @Expose
    var profileId: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("Email")
    @Expose
    var email: String? = null
    @SerializedName("About")
    @Expose
    var about: Any? = null
    @SerializedName("Location")
    @Expose
    var location: String? = null
    @SerializedName("SourcePath")
    @Expose
    var sourcePath: Any? = null
    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
    @SerializedName("pimage")
    @Expose
    var pimage: String? = null
    @SerializedName("followerCount")
    @Expose
    var followerCount: Int? = null
    @SerializedName("followingCount")
    @Expose
    var followingCount: Int? = null
    @SerializedName("statusId")
    @Expose
    var statusId: String? = null
    @SerializedName("likesCount")
    @Expose
    val likesCount: String? = null
    @SerializedName("commentsCount")
    @Expose
    val commentsCount: String? = null
    @SerializedName("username")
    @Expose
    val username: String? = null

}
