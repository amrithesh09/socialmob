package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TopItemsNetworkResponsePayload {
    @SerializedName("profiles")
    @Expose
    val profiles: List<SearchProfileResponseModelPayloadProfile>? = null
    @SerializedName("hashTags")
    @Expose
    val hashTags: List<HashTag>? = null
}
