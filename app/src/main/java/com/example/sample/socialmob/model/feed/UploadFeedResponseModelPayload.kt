package com.example.sample.socialmob.model.feed

import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPost
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UploadFeedResponseModelPayload {

    @SerializedName("success")
    @Expose
    val success: Boolean? = null
    @SerializedName("post")
    @Expose
    val post: PersonalFeedResponseModelPayloadFeedActionPost? = null
}
