package com.example.sample.socialmob.model.feed

import android.os.Parcelable
import com.example.sample.socialmob.model.profile.OgTags
import com.example.sample.socialmob.model.search.HashTag
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class UploadFeedResponseModelPayloadPost : Parcelable {
    @Expose
    val profileId: String? = null
    @SerializedName("description")
    @Expose
    val description: String? = null
    @SerializedName("privacy")
    @Expose
    val privacy: Int? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("hashTags")
    @Expose
    val hashTags: List<HashTag>? = null
    @SerializedName("mentions")
    @Expose
    val mentions: List<Any>? = null
    @SerializedName("media")
    @Expose
    val media: List<Any>? = null
    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null
    @SerializedName("likes")
    @Expose
    val likes: List<Any>? = null
    @SerializedName("comments")
    @Expose
    val comments: List<Any>? = null
    @SerializedName("status")
    @Expose
    val status: Int? = null
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("ogTags")
    @Expose
    val ogTags: OgTags? = null
}
