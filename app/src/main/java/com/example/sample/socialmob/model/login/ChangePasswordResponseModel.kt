package com.example.sample.socialmob.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ChangePasswordResponseModel {
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("code")
    @Expose
    val code: Int? = null
    @SerializedName("payload")
    @Expose
    val payload: ChangePasswordResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    val now: String? = null
}
