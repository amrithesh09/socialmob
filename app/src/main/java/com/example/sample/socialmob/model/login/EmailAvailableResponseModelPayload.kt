package com.example.sample.socialmob.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class EmailAvailableResponseModelPayload {
    @SerializedName("available")
    @Expose
    val available: Boolean? = null
}
