package com.example.sample.socialmob.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ForgotPasswordResponseModel {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: ForgotPasswordResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null

}
