package com.example.sample.socialmob.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class JukeboxFavorite {
    @SerializedName("JukeBoxTrackId")
    @Expose
    var jukeBoxTrackId: Int? = null
    @SerializedName("TrackName")
    @Expose
    var trackName: String? = null
    @SerializedName("Author")
    @Expose
    var author: String? = null
    @SerializedName("AllowOffline")
    @Expose
    var allowOffline: Boolean? = null
    @SerializedName("Length")
    @Expose
    var length: Int? = null
    @SerializedName("TrackFile")
    @Expose
    var trackFile: String? = null
    @SerializedName("TrackImage")
    @Expose
    var trackImage: String? = null

    @SerializedName("Position")
    @Expose
    var position: Int? = null

    @SerializedName("Favorite")
    @Expose
    var Favorite: String? = null

    @SerializedName("GenreName")
    @Expose
    var GenreName: String? = null

}
