package com.example.sample.socialmob.model.login

import com.google.gson.annotations.SerializedName

class LoginResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("payload")
    var payload: Payload? = null

    @SerializedName("now")
    var now: String? = null


}
