package com.example.sample.socialmob.model.login

import android.os.Parcel
import android.os.Parcelable
import com.example.sample.socialmob.model.music.TrackListCommon
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Payload() : Parcelable {
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("profile")
    @Expose
    var profile: Profile? = null
    @Expose
    val jukeboxFavorites: List<JukeboxFavorite>? = null
    @SerializedName("photoGrid")
    @Expose
    val photoGrid: List<Any>? = null
    @SerializedName("feeds")
    @Expose
    val feeds: List<Any>? = null
    @SerializedName("profileAnthemTrack")
    @Expose
    val profileAnthemTrack: TrackListCommon? = null
    @SerializedName("remainingPenny")
    @Expose
    val remainingPenny: Int? = null
    @SerializedName("totalPenny")
    @Expose
    val totalPenny: Int? = null
    @SerializedName("themeGenre")
    @Expose
    val themeGenre: ThemeGenre? = null
    @SerializedName("new_user")
    @Expose
    var newUser: Boolean? = null
    @SerializedName("fullscreendashboardUUID")
    @Expose
    val fullscreendashboardUUID: String? = null

    @SerializedName("jwt")
    var jwt: String? = ""

    constructor(parcel: Parcel) : this() {
        success = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        message = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(success)
        parcel.writeString(message)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Payload> {
        override fun createFromParcel(parcel: Parcel): Payload {
            return Payload(parcel)
        }

        override fun newArray(size: Int): Array<Payload?> {
            return arrayOfNulls(size)
        }
    }
}
