package com.example.sample.socialmob.model.login

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Profile")
data class Profile(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "_id") val _id: String? = null,
    @ColumnInfo(name = "ProfileId") val ProfileId: String? = null,
    @ColumnInfo(name = "Name") var Name: String? = null,
    @ColumnInfo(name = "Email") val Email: String? = null,
    @ColumnInfo(name = "Dob") val Dob: String? = null,
    @ColumnInfo(name = "Gender") val Gender: String? = null,
    @ColumnInfo(name = "About") val About: String? = null,
    @ColumnInfo(name = "CreatedDate") val CreatedDate: String? = null,
    @ColumnInfo(name = "Geners") val Geners: String? = null,
    @ColumnInfo(name = "Interests") val Interests: String? = null,
    @ColumnInfo(name = "Status") val Status: Boolean? = null,
    @ColumnInfo(name = "SystemFileName") val SystemFileName: String? = null,
    @ColumnInfo(name = "FileName") val FileName: String? = null,
    @ColumnInfo(name = "Path") val Path: String? = null,
    @ColumnInfo(name = "Location") val Location: String? = null,
    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null,
    @ColumnInfo(name = "ZodiacSign") val ZodiacSign: Int? = null,
    @ColumnInfo(name = "thumbnail") val thumbnail: String? = null,
    @ColumnInfo(name = "pimage") val pimage: String? = null,
    @ColumnInfo(name = "privateProfile") val privateProfile: Boolean? = null,
//    @TypeConverters(Personalities::class)
//    @ColumnInfo(name = "personalities") val personalities: Personalities,
    @ColumnInfo(name = "referral_id") val referral_id: String? = null,
    @ColumnInfo(name = "referrer_id") val referrer_id: String? = null,
    @ColumnInfo(name = "remainingPenny") val remainingPenny: Int? = null,
    @ColumnInfo(name = "totalPenny") val totalPenny: Int? = null,
    @ColumnInfo(name = "followerCount") val followerCount: String? = null,
    @ColumnInfo(name = "followingCount") val followingCount: String? = null,
    @ColumnInfo(name = "emailVerified") val emailVerified: Boolean? = null,
    @ColumnInfo(name = "celebrityVerified") val celebrityVerified: Boolean? = null,
    @ColumnInfo(name = "inviteLink") val inviteLink: String? = null,
    @ColumnInfo(name = "statusId") val statusId: String? = null,
//    @ColumnInfo(name = "about") val about1: String? = null,
    @ColumnInfo(name = "AppId") val AppId: String? = null,
    @ColumnInfo(name = "ApiKey") val ApiKey: String? = null,
    @ColumnInfo(name = "pushSubscription") val PushSubscription: Boolean? = null,
    @ColumnInfo(name = "profileAnthemTrackId") val profileAnthemTrackId: String? = null,
    @ColumnInfo(name = "notificationCount") val notificationCount: Int? = null,
    @ColumnInfo(name = "username") var username: String? = null,
    @ColumnInfo(name = "unreadMessageCount") var unreadMessageCount: String? = null

)
