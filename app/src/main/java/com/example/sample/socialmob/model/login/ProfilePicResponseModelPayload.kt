package com.example.sample.socialmob.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProfilePicResponseModelPayload {
    @SerializedName("success")
    @Expose
    val success: Boolean? = null
    @SerializedName("url")
    @Expose
    val url: String? = null
    @SerializedName("filename")
    @Expose
    val filename: String? = null
}
