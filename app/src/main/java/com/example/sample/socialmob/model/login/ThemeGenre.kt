package com.example.sample.socialmob.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ThemeGenre {
    @SerializedName("JukeBoxCategoryId")
    @Expose
    val jukeBoxCategoryId: Int? = null
    @SerializedName("JukeBoxCategoryName")
    @Expose
    val jukeBoxCategoryName: String? = null
    @SerializedName("V2Image")
    @Expose
    val v2Image: String? = null
    @SerializedName("ThemeCover")
    @Expose
    val themeCover: String? = null
    @SerializedName("ThemeThumbnail")
    @Expose
    val themeThumbnail: String? = null
}
