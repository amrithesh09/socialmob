package com.example.sample.socialmob.model.music

//class AddCommentResponseModelPayloadVideo {
//
//}
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddCommentResponseModelPayloadVideo {
    @SerializedName("success")
    @Expose
    val success: Boolean? = null
    @SerializedName("comment")
    @Expose
    val comment: CommentResponseModelPayloadComment? = null
}
