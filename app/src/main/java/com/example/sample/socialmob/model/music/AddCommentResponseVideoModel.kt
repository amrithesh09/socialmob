package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.AddCommentResponseModelPayloadVideo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

//class AddCommentResponseVideoModel {
//
//}



class AddCommentResponseVideoModel {
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("code")
    @Expose
    val code: Int? = null
    @SerializedName("payload")
    @Expose
    val payload: AddCommentResponseModelPayloadVideo? = null
    @SerializedName("now")
    @Expose
    val now: String? = null
}
