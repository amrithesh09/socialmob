package com.example.sample.socialmob.model.music

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Artist() : Parcelable {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("artist_name")
    @Expose
    var artistName: String? = null

    @SerializedName("genre_ids")
    @Expose
    val genreIds: List<String>? = null

    @SerializedName("media")
    @Expose
    val media: ArtistMedia? = null

    @SerializedName("bio")
    @Expose
    val bio: String? = null

    @SerializedName("profileId")
    @Expose
    val profileId: String? = ""

    @SerializedName("own")
    @Expose
    var own: String? = null

    @SerializedName("relation")
    @Expose
    var relation: String? = null

    @SerializedName("country")
    @Expose
    val country: ArtistCountry? = null

    @SerializedName("featured_video_id")
    @Expose
    val featured_video_id: String? = null

    @SerializedName("video_count")
    @Expose
    val video_count: String? = null

    @SerializedName("featured_video")
    @Expose
    val featuredVideo: FeaturedVideo? = null


    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        artistName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(artistName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Artist> {
        override fun createFromParcel(parcel: Parcel): Artist {
            return Artist(parcel)
        }

        override fun newArray(size: Int): Array<Artist?> {
            return arrayOfNulls(size)
        }
    }
}
