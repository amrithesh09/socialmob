package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.model.music.SinglesMedia
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ArtistAlbum {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("artist_id")
    @Expose
    val artistId: String? = null
    @SerializedName("album_name")
    @Expose
    val albumName: String? = null
    @SerializedName("is_singles")
    @Expose
    val isSingles: String? = null
    @SerializedName("status")
    @Expose
    val status: Int? = null
    @SerializedName("artist")
    @Expose
    val artist: Artist? = null
    @SerializedName("singles_media")
    @Expose
    val singlesMedia: SinglesMedia? = null
    @SerializedName("album_media")
    @Expose
     val albumMedia: SinglesMedia? = null
}
