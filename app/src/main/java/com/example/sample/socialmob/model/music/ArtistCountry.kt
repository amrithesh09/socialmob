package com.example.sample.socialmob.model.music

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ArtistCountry() : Parcelable {
    @SerializedName("country_code")
    @Expose
    var countryCode: String? = null

    @SerializedName("country_name")
    @Expose
    var countryName: String? = null

    @SerializedName("icon_filename")
    @Expose
    var iconFilename: String? = null

    @SerializedName("flag")
    @Expose
    var flag: String? = null

    constructor(parcel: Parcel) : this() {
        countryCode = parcel.readString()
        countryName = parcel.readString()
        iconFilename = parcel.readString()
        flag = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(countryCode)
        parcel.writeString(countryName)
        parcel.writeString(iconFilename)
        parcel.writeString(flag)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArtistCountry> {
        override fun createFromParcel(parcel: Parcel): ArtistCountry {
            return ArtistCountry(parcel)
        }

        override fun newArray(size: Int): Array<ArtistCountry?> {
            return arrayOfNulls(size)
        }
    }


}
