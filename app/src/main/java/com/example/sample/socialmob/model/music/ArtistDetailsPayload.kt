package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ArtistDetailsPayload {
    @SerializedName("artist")
    @Expose
    val artist: Artist? = null
    @SerializedName("topTracks")
    @Expose
    val topTracks: List<TrackListCommon>? = null
    @SerializedName("albums")
    @Expose
    val albums: List<ArtistDetailsPayloadAlbum>? = null
    @SerializedName("similarArtists")
    @Expose
    val similarArtists: List<Artist>? = null
}
