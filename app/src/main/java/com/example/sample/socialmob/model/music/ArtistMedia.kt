package com.example.sample.socialmob.model.music

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ArtistMedia() : Parcelable {
    @SerializedName("cover_image")
    @Expose
    var coverImage: String? = null

    @SerializedName("master_playlist")
    @Expose
    var master_playlist: String? = null

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null

    constructor(parcel: Parcel) : this() {
        coverImage = parcel.readString()
        master_playlist = parcel.readString()
        thumbnail = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(coverImage)
        parcel.writeString(master_playlist)
        parcel.writeString(thumbnail)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArtistMedia> {
        override fun createFromParcel(parcel: Parcel): ArtistMedia {
            return ArtistMedia(parcel)
        }

        override fun newArray(size: Int): Array<ArtistMedia?> {
            return arrayOfNulls(size)
        }
    }


}
