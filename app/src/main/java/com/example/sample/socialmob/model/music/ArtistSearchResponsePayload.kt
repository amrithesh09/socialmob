package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.Artist
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ArtistSearchResponsePayload {

    @SerializedName("artists")
    @Expose
    val artists: MutableList<Artist>? = null
}
