package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ArtistVideoListPayload {
    @SerializedName("videos")
    @Expose
     val videos: MutableList<MusicVideoPayloadVideo>? = null
}
