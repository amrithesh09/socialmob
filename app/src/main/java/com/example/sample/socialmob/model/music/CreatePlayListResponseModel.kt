package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreatePlayListResponseModel {
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: CreatePlayListResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null

}
