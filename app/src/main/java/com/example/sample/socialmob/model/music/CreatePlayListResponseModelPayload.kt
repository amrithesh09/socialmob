package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreatePlayListResponseModelPayload {
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
}
