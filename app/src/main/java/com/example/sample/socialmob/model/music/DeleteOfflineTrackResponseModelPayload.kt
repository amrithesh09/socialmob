package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DeleteOfflineTrackResponseModelPayload {

    @SerializedName("track")
    @Expose
    val track: MutableList<String>? = null

}
