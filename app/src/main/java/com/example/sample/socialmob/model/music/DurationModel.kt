package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DurationModel {
    @SerializedName("duration")
    @Expose
    var duration: String? = null
}
