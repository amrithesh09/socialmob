package com.example.sample.socialmob.model.music

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class EditorsChoicePlaylist() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("playlist_name")
    @Expose
    var playlistName: String? = null

    @SerializedName("tracks")
    @Expose
    var tracks: List<TrackListCommon>? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        playlistName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(playlistName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EditorsChoicePlaylist> {
        override fun createFromParcel(parcel: Parcel): EditorsChoicePlaylist {
            return EditorsChoicePlaylist(parcel)
        }

        override fun newArray(size: Int): Array<EditorsChoicePlaylist?> {
            return arrayOfNulls(size)
        }
    }
}
