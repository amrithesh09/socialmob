package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "episode")
data class Episode(
    @PrimaryKey
    @ColumnInfo(name = "EpisodeId") val EpisodeId: String,
    @ColumnInfo(name = "EpisodeName") val EpisodeName: String,
    @ColumnInfo(name = "EpisodeSourcePath") val EpisodeSourcePath: String,
    @ColumnInfo(name = "PodcastName") val PodcastName: String,
    @ColumnInfo(name = "Position") val Position: String,
    @ColumnInfo(name = "ThumbnailPath") val ThumbnailPath: String
)


