package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Favorite {
    @SerializedName("JukeBoxTrackId")
    @Expose
    var jukeBoxTrackId: String? = null
    @SerializedName("TrackName")
    @Expose
    val trackName: String? = null
    @SerializedName("Author")
    @Expose
    val author: String? = null
    @SerializedName("AllowOffline")
    @Expose
    val allowOffline: Boolean? = null
    @SerializedName("Length")
    @Expose
    val length: Int? = null
    @SerializedName("TrackFile")
    @Expose
    val trackFile: String? = null

    @SerializedName("TrackImage")
    @Expose
    val trackImage: String? = null

    @SerializedName("GenreName")
    @Expose
    val GenreName: String? = null

    @SerializedName("Favorite")
    @Expose
    var favorite: String? = null

}
