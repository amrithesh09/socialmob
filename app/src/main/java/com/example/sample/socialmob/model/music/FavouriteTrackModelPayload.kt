package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class FavouriteTrackModelPayload {
    @SerializedName("tracks")
    @Expose
    val favorites: List<TrackListCommon>? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
}
