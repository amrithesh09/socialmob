package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class FeaturedPodcast {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("podcast_name")
    @Expose
    val podcastName: String? = null
    @SerializedName("description")
    @Expose
    val description: String? = null
    @SerializedName("position")
    @Expose
    val position: Int? = null
    @SerializedName("cover_image")
    @Expose
    val coverImage: String? = null
    @SerializedName("is_new")
    @Expose
    val isNew: String? = null
}
