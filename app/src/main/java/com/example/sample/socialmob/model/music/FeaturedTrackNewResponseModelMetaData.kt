package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class FeaturedTrackNewResponseModelMetaData {

    @SerializedName("genre")
    @Expose
    val genre: FeaturedTrackNewResponseModelMetaDataGenre? = null
    @SerializedName("lastPlayed")
    @Expose
    val lastPlayed: FeaturedTrackNewResponseModelMetaDataLastPlayed? = null
    @SerializedName("likedTracks")
    @Expose
    val likedTracks: FeaturedTrackNewResponseModelMetaDataLikedTracks? = null
    @SerializedName("allPodcasts")
    @Expose
    val allPodcasts: AllPodcasts? = null
}
