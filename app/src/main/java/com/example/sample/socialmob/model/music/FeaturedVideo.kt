package com.example.sample.socialmob.model.music

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class FeaturedVideo() : Parcelable {
    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("track_id")
    @Expose
    val trackId: String? = null

    @SerializedName("title")
    @Expose
    val title: String? = null

    @SerializedName("description")
    @Expose
    val description: String? = null

    @SerializedName("notes")
    @Expose
    val notes: String? = null

    @SerializedName("tag")
    @Expose
    val tag: String? = null

    @SerializedName("status")
    @Expose
    val status: String? = null

    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null

    @SerializedName("media")
    @Expose
    val media: ArtistMedia? = null

    @SerializedName("view_count")
    @Expose
    val viewCount: String? = "0"

    @SerializedName("likesCount")
    @Expose
    val likesCount: String? = null

    @SerializedName("duration")
    @Expose
    val duration: String? = null

    @SerializedName("commentsCount")
    @Expose
    val commentsCount: String? = null

    @SerializedName("liked")
    @Expose
    val liked: Boolean? = null

    @SerializedName("artist")
    @Expose
    val artist: List<Artist>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FeaturedVideo> {
        override fun createFromParcel(parcel: Parcel): FeaturedVideo {
            return FeaturedVideo(parcel)
        }

        override fun newArray(size: Int): Array<FeaturedVideo?> {
            return arrayOfNulls(size)
        }
    }


}
