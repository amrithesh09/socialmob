package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Genre {
    @SerializedName("_id")
    @Expose
    val id: String = ""
    @SerializedName("genre_name")
    @Expose
    val genreName: String = ""
    @SerializedName("media")
    @Expose
    val media: GenreMedia? = null
}
