package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GenreMedia {

    @SerializedName("square_image")
    @Expose
    val squareImage: String? = null

    @SerializedName("cover_image_filename")
    @Expose
    val cover_image_filename: String? = null


}
