package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GenreResponse {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: GenreResponsePayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null
}
