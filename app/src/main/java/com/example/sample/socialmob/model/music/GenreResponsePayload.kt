package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.Genre
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

 class GenreResponsePayload {
    @SerializedName("genres")
    @Expose
    var genres: MutableList<Genre>? = null
}
