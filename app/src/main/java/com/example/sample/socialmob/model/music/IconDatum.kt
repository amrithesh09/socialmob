package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class IconDatum {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("category_name")
    @Expose
    val categoryName: String? = null
    @SerializedName("subscription_required")
    @Expose
    val subscriptionRequired: Boolean? = null
    @SerializedName("icons")
    @Expose
    val icons: List<IconDatumIcon>? = null
}
