package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "iconData")
data class IconDatumIcon(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "_id")
    var _id: String? = null,

    @ColumnInfo(name = "status")
    var status: Int? = null,

    @ColumnInfo(name = "file_url")
    var file_url: String? = null,

    @ColumnInfo(name = "category_name")
    var category_name: String? = null,

    @ColumnInfo(name = "is_selected")
    var is_selected: Boolean? = null


)
