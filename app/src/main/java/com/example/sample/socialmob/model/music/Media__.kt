package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Media__ {
    @SerializedName("cover_image")
    @Expose
    val coverImage: String? = null
}
