package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MusicMetaResponsePayload {
    @SerializedName("featuredTracks")
    @Expose
    var featuredTracks: MutableList<TrackListCommon>? = null

    @SerializedName("myPlaylists")
    @Expose
    val myPlaylists: MutableList<PlaylistCommon>? = null

    @SerializedName("editorsChoicePlaylist")
    @Expose
    val editorsChoicePlaylist: List<EditorsChoicePlaylist>? = null

    @SerializedName("featuredArtists")
    @Expose
    val featuredArtists: MutableList<Artist>? = null

    @SerializedName("latestArtists")
    @Expose
    val latestArtists: MutableList<Artist>? = null

    @SerializedName("playlistsForYou")
    @Expose
    val playlistsForYou: MutableList<PlaylistCommon>? = null

    @SerializedName("topTracksOfWeek")
    @Expose
    var topTracksOfWeek: MutableList<TrackListCommon>? = null

    @SerializedName("trendingTracks")
    @Expose
    var trendingTracks: MutableList<TrackListCommon>? = null

    @SerializedName("releasesForYou")
    @Expose
    var releasesForYou: MutableList<TrackListCommon>? = null

    @SerializedName("weeklyNewTracks")
    @Expose
    var weeklyNewTracks: MutableList<TrackListCommon>? = null

    @SerializedName("popularAlbums")
    @Expose
    var popularAlbums: MutableList<ArtistDetailsPayloadAlbum>? = null

    @SerializedName("remoteImages")
    @Expose
    val remoteImages: RemoteImages? = null

    @SerializedName("stations")
    @Expose
     val stations: List<MusicStation>? = null
}

class RemoteImages {
    @SerializedName("genre")
    @Expose
    val genre: AllPodcasts? = null

    @SerializedName("lastPlayed")
    @Expose
    val lastPlayed: AllPodcasts? = null

    @SerializedName("likedTracks")
    @Expose
    val likedTracks: AllPodcasts? = null

    @SerializedName("allPodcasts")
    @Expose
    val allPodcasts: AllPodcasts? = null

}


