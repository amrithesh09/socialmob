package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class MusicStation {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("genre_name")
    @Expose
    val genreName: String? = null

    @SerializedName("station_data")
    @Expose
    var stationData: StationData? = null

    var isPlaying: Boolean? = false
}

class StationData {
    @SerializedName("cover_image")
    @Expose
    var coverImage: String? = null

    @SerializedName("station_url")
    @Expose
    var station_url: String? = null
}
