package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MusicVideoList {

    @SerializedName("status")
    @Expose
    val status: String? = null

    @SerializedName("code")
    @Expose
    val code: Int? = null

    @SerializedName("payload")
    @Expose
    val payload: MusicVideoPayload? = null

    @SerializedName("now")
    @Expose
    val now: String? = null
}