package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class MusicVideoPayloadVideo {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("track_id")
    @Expose
    val trackId: String? = null

    @SerializedName("title")
    @Expose
    val title: String? = null

    @SerializedName("description")
    @Expose
    val description: String? = null

    @SerializedName("notes")
    @Expose
    val notes: String? = null

    @SerializedName("tag")
    @Expose
    val tag: String? = null

    @SerializedName("status")
    @Expose
    val status: Int? = null

    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null

    @SerializedName("media")
    @Expose
    val media: MusicVideoPayloadVideoMedia? = null

    @SerializedName("view_count")
    @Expose
    val viewCount: String? = null

    @SerializedName("duration")
    @Expose
    val duration: String? = null

    @SerializedName("likesCount")
    @Expose
    var likesCount: String? = null

    @SerializedName("commentsCount")
    @Expose
    val commentsCount: String? = null

    @SerializedName("liked")
    @Expose
    var liked: Boolean? = null

    @SerializedName("artist")
    @Expose
    val artist: List<Artist>? = null
}
