package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class MusicVideoPayloadVideoMedia {
    @SerializedName("master_playlist")
    @Expose
     val masterPlaylist: String? = null

    @SerializedName("thumbnail")
    @Expose
     val thumbnail: String? = null
}
