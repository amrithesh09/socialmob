package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.IconDatum
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PlayListImagesPayload {
    @SerializedName("icon_data")
    @Expose
    val iconData: List<IconDatum>? = null
}
