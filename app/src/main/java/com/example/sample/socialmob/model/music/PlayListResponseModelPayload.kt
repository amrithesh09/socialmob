package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlayListResponseModelPayload {
    @SerializedName("playlists")
    @Expose
    var playlists: MutableList<PlaylistCommon>? = null
}
