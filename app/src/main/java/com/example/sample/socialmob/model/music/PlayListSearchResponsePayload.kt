package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PlayListSearchResponsePayload {
    @SerializedName("playlists")
    @Expose
    val playlists: MutableList<PlaylistCommon>? = null
}
