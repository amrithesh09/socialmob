package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(
    tableName = "playlist"
)
data class Playlist(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "_id") val _id: String? = null,
    @ColumnInfo(name = "playlist_name") val playlist_name: String? = null,
    @ColumnInfo(name = "playlist_type") val playlist_type: String? = null,
    @ColumnInfo(name = "profileId") val profileId: String? = null,
    @ColumnInfo(name = "created_on") val created_on: String? = null,
    @ColumnInfo(name = "is_private") val is_private: String? = null,

    @Embedded
    val playlistIcon: PlaylistIcon? = null,
    @Embedded
    val media: Media? = null

)

data class Media(
    @ColumnInfo(name = "cover_image") val cover_image: String? = null
)

data class PlaylistIcon(
    @ColumnInfo(name = "file_url") val file_url: String? = null,
    @ColumnInfo(name = "icon_id") val icon_id: String? = null
)


