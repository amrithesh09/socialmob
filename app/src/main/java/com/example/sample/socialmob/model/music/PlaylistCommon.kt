package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.login.Profile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PlaylistCommon {
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("playlist_name")
    @Expose
    var playlistName: String? = null
    @SerializedName("playlist_type")
    @Expose
    val playlistType: String? = null
    @SerializedName("media")
    @Expose
    val media: TrackListCommonMedia? = null
    @SerializedName("created_on")
    @Expose
    val createdOn: String? = null
    @SerializedName("is_private")
    @Expose
    val isPrivate: String? = ""
    @SerializedName("icon")
    @Expose
    val icon: PlaylistDataIcon? = PlaylistDataIcon()
    @SerializedName("profileId")
    @Expose
    val profileId: String? = null
    @SerializedName("profile")
    @Expose
     val profile: Profile? = null
}
