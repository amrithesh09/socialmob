package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PlaylistDataIcon {
    @SerializedName("icon_id")
    @Expose
    val id: String? = null
    @SerializedName("status")
    @Expose
    val status: Int? = null
    @SerializedName("icon_name")
    @Expose
    val iconName: String? = null
    @SerializedName("file_url")
    @Expose
    val fileUrl: String? = ""
}
