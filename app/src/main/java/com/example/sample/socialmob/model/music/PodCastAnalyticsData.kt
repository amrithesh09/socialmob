package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PodCastAnalytics
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PodCastAnalyticsData {
    @SerializedName("data")
    @Expose
    var data: List<PodCastAnalytics>? = null
}
