package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PodCastResponsePayload {
    @SerializedName("podcasts")
    @Expose
    var podcasts: List<Podcast>? = null
    @SerializedName("metaData")
    @Expose
    val metaData: FeaturedTrackNewResponseModelMetaData? = null
}
