package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "podcast")
data class Podcast(
    @PrimaryKey(autoGenerate = true) var mId: Int,
    @ColumnInfo(name = "_id") val _id: String? = null,
    @ColumnInfo(name = "podcast_name") val podcast_name: String? = null,
    @ColumnInfo(name = "position") val position: String? = null,
    @ColumnInfo(name = "description") val description: String? = null,
    @ColumnInfo(name = "cover_image") val cover_image: String? = null,
    @ColumnInfo(name = "is_new") val is_new: String? = null
)