package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "podcastHistoryTrack")
data class PodcastHistoryTrack(
    @PrimaryKey
    @ColumnInfo(name = "EpisodeId") val EpisodeId: String,
    @ColumnInfo(name = "EpisodeName") val EpisodeName: String?,
    @ColumnInfo(name = "EpisodeSourcePath") val EpisodeSourcePath: String?,
    @ColumnInfo(name = "PodcastName") val PodcastName: String?,
    @ColumnInfo(name = "Position") val Position: String?,
    @ColumnInfo(name = "percentage") val percentage: String?,
    @ColumnInfo(name = "millisecond") val Millisecond: String?,
    @ColumnInfo(name = "ThumbnailPath") val ThumbnailPath: String?,
    @ColumnInfo(name = "mExtraId") val mExtraId: String?,
    @ColumnInfo(name = "podcast_id") val podcast_id: String?,
    @ColumnInfo(name = "isPlaying") var isPlaying: String?,
    @ColumnInfo(name = "timeStamp") var timeStamp: String?,
    @ColumnInfo(name = "play_count") var play_count: String?
)
