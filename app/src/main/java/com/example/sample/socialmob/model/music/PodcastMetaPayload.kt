package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.AllPodcastImage
import com.example.sample.socialmob.model.music.FeaturedPodcast
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.model.music.Station
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PodcastMetaPayload {
    @SerializedName("featuredPodcasts")
    @Expose
    val featuredPodcasts: ArrayList<FeaturedPodcast>? = null
    @SerializedName("topPodcasts")
    @Expose
    val topPodcasts: List<FeaturedPodcast>? = null
    @SerializedName("recommendedEpisodes")
    @Expose
    val recommendedEpisodes: List<RadioPodCastEpisode>? = null
    @SerializedName("radioStations")
    @Expose
    val radioStations: List<Station>? = null
    @SerializedName("allPodcastImage")
    @Expose
    val allPodcastImage: AllPodcastImage? = null
}
