package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PodcastResponseModelPayload {
    @SerializedName("episode")
    @Expose
    var episode: RadioPodCastEpisode? = null


}
