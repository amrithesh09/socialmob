package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "radioPodcastEpisode")
data class RadioPodCastEpisode(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "EpisodeId") val _id: String?,
    @ColumnInfo(name = "number_id") val number_id: String?,
    @ColumnInfo(name = "PodcastName") val podcast_name: String?,
    @ColumnInfo(name = "EpisodeName") val episode_name: String?,
    @ColumnInfo(name = "EpisodeSourcePath") val source_path: String?,
    @ColumnInfo(name = "ThumbnailPath") val cover_image: String?,
    @ColumnInfo(name = "percentage") val percentage: String?,
    @ColumnInfo(name = "millisecond") val Millisecond: String?,
    @ColumnInfo(name = "Length") val duration: String?,
    @ColumnInfo(name = "CreatedDate") val created_date: String?,
    @ColumnInfo(name = "podcast_id") val podcast_id: String?,
    @ColumnInfo(name = "isPlaying") var isPlaying: String?,
    @ColumnInfo(name = "play_count") var play_count: String?
)

