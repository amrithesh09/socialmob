package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RadioPodCastPayload {
    @SerializedName("episodes")
    @Expose
    var episodes: List<RadioPodCastEpisode>? = null
    @SerializedName("podcast")
    @Expose
    val podcast: Podcast? = null

}
