package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RadioResponseModelPayload {

    @SerializedName("stations")
    @Expose
    var stations: List<Station>? = null
}
