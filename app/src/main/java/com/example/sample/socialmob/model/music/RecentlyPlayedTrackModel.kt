package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.music.GenreListPayload
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class RecentlyPlayedTrackModel {
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("code")
    @Expose
    val code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: GenreListPayload? = null
    @SerializedName("now")
    @Expose
    val now: String? = null
}
