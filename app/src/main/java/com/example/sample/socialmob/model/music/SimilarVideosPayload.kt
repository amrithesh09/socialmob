package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SimilarVideosPayload {
    @SerializedName("video")
    @Expose
    val video: MusicVideoPayloadVideo? = null

    @SerializedName("similarVideos")
    @Expose
    val similarVideos: List<MusicVideoPayloadVideo>? = null

    @SerializedName("track")
    @Expose
    var track: TrackListCommon? = null

}
