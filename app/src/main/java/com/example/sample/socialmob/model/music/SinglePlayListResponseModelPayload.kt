package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SinglePlayListResponseModelPayload {
    @SerializedName("tracks")
    @Expose
    var tracks: List<TrackListCommon>? = null
    @SerializedName("playlist")
    @Expose
    val playlist: PlaylistCommon? = null
}
