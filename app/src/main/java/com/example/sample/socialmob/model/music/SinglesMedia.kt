package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SinglesMedia {
    @SerializedName("cover_image")
    @Expose
    val coverImage: String? = null
}
