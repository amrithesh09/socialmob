package com.example.sample.socialmob.model.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName =  "stationList")
data class Station(
    @PrimaryKey
    @ColumnInfo(name = "_id") val _id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "station_url") val station_url: String,
    @ColumnInfo(name = "created_date") val created_date: String,
    @ColumnInfo(name = "albumArt") val albumArt: String,
    @ColumnInfo(name = "onair") val onair: String
)
