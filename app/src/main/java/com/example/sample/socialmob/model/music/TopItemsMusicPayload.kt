package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TopItemsMusicPayload {
    @SerializedName("trendingTracks")
    @Expose
    val trendingTracks: List<TrackListCommon>? = ArrayList()
    @SerializedName("featuredArtists")
    @Expose
    val featuredArtists: List<Artist>? = ArrayList()
    @SerializedName("playlistsForYou")
    @Expose
    val playlistsForYou: List<PlaylistCommon>? = ArrayList()
}
