package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackAddCommentResponseModelPayload {

    @SerializedName("success")
    @Expose
    val success: Boolean? = null
    @SerializedName("comment")
    @Expose
    val comment: TrackCommentModelPayloadComment? = null

}
