package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackAnalyticsDataList {
    @SerializedName("action")
    @Expose
    var action: String? = null
    @SerializedName("offline")
    @Expose
    var offline: String? = null
    @SerializedName("duration")
    @Expose
    var duration: String? = null
    @SerializedName("track_id")
    @Expose
    var track_id: String? = null
}
