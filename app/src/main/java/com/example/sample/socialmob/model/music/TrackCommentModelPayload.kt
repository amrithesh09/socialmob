package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackCommentModelPayload {
    @SerializedName("track")
    @Expose
    val track: TrackListCommon? = null
    @SerializedName("comments")
    @Expose
    val comments: MutableList<TrackCommentModelPayloadComment>? = null
    @SerializedName("commentsCount")
    @Expose
    val commentsCount: String? = null

    @SerializedName("podcast")
    @Expose
    val podcast: Podcast? = null
}
