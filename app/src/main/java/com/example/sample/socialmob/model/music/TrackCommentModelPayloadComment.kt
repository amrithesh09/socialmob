package com.example.sample.socialmob.model.music

import com.example.sample.socialmob.model.article.MentionPeople
import com.example.sample.socialmob.model.login.Profile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackCommentModelPayloadComment {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("profile")
    @Expose
    val profile: Profile? = null
    @SerializedName("commentId")
    @Expose
    var commentId: String? = null
    @SerializedName("text")
    @Expose
    val text: String? = null
    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null
    @SerializedName("duration")
    @Expose
    val duration: Any? = null
    @SerializedName("own")
    @Expose
    val own: Boolean? = null

    @SerializedName("mentions")
    @Expose
    val mentions: List<MentionPeople>? = null
}
