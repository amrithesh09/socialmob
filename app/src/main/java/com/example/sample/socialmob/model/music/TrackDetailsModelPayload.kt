package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackDetailsModelPayload {
    @SerializedName("track")
    @Expose
    var track: TrackListCommon? = null


}
