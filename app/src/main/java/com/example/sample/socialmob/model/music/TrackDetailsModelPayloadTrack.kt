package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackDetailsModelPayloadTrack {
    @SerializedName("JukeBoxTrackId")
    @Expose
    var jukeBoxTrackId: String? = null
    @SerializedName("JukeBoxCategoryName")
    @Expose
    var jukeBoxCategoryName: String? = null
    @SerializedName("Author")
    @Expose
    var author: String? = null
    @SerializedName("TrackName")
    @Expose
    var trackName: String? = null
    @SerializedName("AllowOffline")
    @Expose
    var allowOffline: String? = null
    @SerializedName("Length")
    @Expose
    var length: String? = null
    @SerializedName("TrackImage")
    @Expose
    var trackImage: String? = null
    @SerializedName("TrackFile")
    @Expose
    var trackFile: String? = null
    @SerializedName("Favorite")
    @Expose
    var favorite: Boolean? = null
}
