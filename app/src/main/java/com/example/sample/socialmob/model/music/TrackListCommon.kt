package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrackListCommon : Serializable {
    @SerializedName("_id")
    @Expose
    var id: String? = ""

    @SerializedName("number_id")
    @Expose
    var numberId: String? = ""

    @SerializedName("track_name")
    @Expose
    var trackName: String? = ""

    @SerializedName("media")
    @Expose
    var media: TrackListCommonMedia? = null

    @SerializedName("duration")
    @Expose
    var duration: String? = ""

    @SerializedName("allow_offline_download")
    @Expose
    var allowOfflineDownload: String? = ""

    @SerializedName("album")
    @Expose
    var album: TrackListCommonAlbum? = null

    @SerializedName("artist")
    @Expose
    var artist: TrackListCommonArtist? = null

    @SerializedName("favorite")
    @Expose
    var favorite: String? = ""

    @SerializedName("genre")
    @Expose
    var genre: TrackListCommonGenre? = null

    @SerializedName("playlist_id")
    @Expose
    var play_list_id: String? = ""

    @SerializedName("like_count")
    @Expose
    val likeCount: String? = ""

    @SerializedName("play_count")
    @Expose
    var playCount: String? = "0"

    @SerializedName("isPlaying")
    @Expose
    var isPlaying: Boolean? = false

    var playListName: String? = ""


}
