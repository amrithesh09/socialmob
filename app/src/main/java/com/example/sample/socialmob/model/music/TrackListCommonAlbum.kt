package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrackListCommonAlbum : Serializable {
    @SerializedName("_id")
    @Expose
    var id: String? = ""
    @SerializedName("album_name")
    @Expose
    var albumName: String? = ""

}
