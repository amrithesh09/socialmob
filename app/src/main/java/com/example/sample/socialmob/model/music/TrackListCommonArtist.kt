package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrackListCommonArtist : Serializable {
    @SerializedName("_id")
    @Expose
    var id: String? = ""
    @SerializedName("artist_name")
    @Expose
    var artistName: String? = ""

}
