package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrackListCommonGenre : Serializable {
    @SerializedName("_id")
    @Expose
    var id: String? = ""
    @SerializedName("genre_name")
    @Expose
    var genreName: String? = ""
}
