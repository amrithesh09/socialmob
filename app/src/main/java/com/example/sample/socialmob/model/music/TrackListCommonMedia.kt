package com.example.sample.socialmob.model.music

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrackListCommonMedia : Serializable {
    @SerializedName("track_file")
    @Expose
    var trackFile: String? = ""
    @SerializedName("cover_file")
    @Expose
    var coverFile: String? = ""
    @SerializedName("cover_image")
    @Expose
    var coverImage: String? = ""

}
