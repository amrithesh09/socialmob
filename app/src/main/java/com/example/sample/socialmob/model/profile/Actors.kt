package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Actors {

    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("ProfileId")
    @Expose
    val profileId: String? = null
    @SerializedName("Name")
    @Expose
    val name: String? = null
    @SerializedName("Email")
    @Expose
    val email: String? = null
    @SerializedName("pimage")
    @Expose
    val pimage: String? = null
    @SerializedName("privateProfile")
    @Expose
    val privateProfile: Boolean? = null
    @SerializedName("relation")
    @Expose
    var relation: String? = null
    @SerializedName("username")
    @Expose
    var username: String? = null

}
