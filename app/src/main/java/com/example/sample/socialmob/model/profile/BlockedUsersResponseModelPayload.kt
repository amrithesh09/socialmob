package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BlockedUsersResponseModelPayload {
    @SerializedName("users")
    @Expose
    var users: MutableList<SearchProfileResponseModelPayloadProfile>? = null


}
