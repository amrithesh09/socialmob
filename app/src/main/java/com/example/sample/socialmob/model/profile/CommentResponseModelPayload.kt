package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CommentResponseModelPayload {
    @SerializedName("comments")
    @Expose
    var comments: MutableList<CommentResponseModelPayloadComment>? = null
    @SerializedName("commentsCount")
    @Expose
    var commentsCount: String? = null
}
