package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.article.MentionPeople
import com.example.sample.socialmob.model.login.Profile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CommentResponseModelPayloadComment {
    @SerializedName("commentId")
    @Expose
    var commentId: String? = null
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("created_date")
    @Expose
    var createdDate: String? = null
    @SerializedName("mentions")
    @Expose
    var mentions: List<MentionPeople>? = null
    @SerializedName("own")
    @Expose
    var own: Boolean? = null
    @SerializedName("profile")
    @Expose
    var profile: Profile? = null

}
