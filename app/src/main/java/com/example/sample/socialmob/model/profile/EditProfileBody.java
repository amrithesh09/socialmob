package com.example.sample.socialmob.model.profile;

public class EditProfileBody {

    private String Name;
    private String Location;
    private String filename;
    private String anthem_track_id;
    private String themeGenreId;
    private String username;
    private String About;
    private LocationCoordinates location_coordinates;

    public EditProfileBody(String name, String location, String filename, String anthem_track_id, String themeGenreId, String username, String About,
                           LocationCoordinates locationCoordinates) {
        this.Name = name;
        this.Location = location;
        this.filename = filename;
        this.anthem_track_id = anthem_track_id;
        this.themeGenreId = themeGenreId;
        this.username = username;
        this.About = About;
        this.location_coordinates = locationCoordinates;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        this.Location = location;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getProfileAnthemTrackId() {
        return anthem_track_id;
    }

    public void setProfileAnthemTrackId(String profileAnthemTrackId) {
        this.anthem_track_id = profileAnthemTrackId;
    }

    public String getThemeGenreId() {
        return themeGenreId;
    }

    public void setThemeGenreId(String themeGenreId) {
        this.themeGenreId = themeGenreId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocationCoordinates getLocation_coordinates() {
        return location_coordinates;
    }

    public void setLocation_coordinates(LocationCoordinates location_coordinates) {
        this.location_coordinates = location_coordinates;
    }
}
