package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FollowersResponseModelPayload {
    @SerializedName("followers")
    @Expose
    var followers: MutableList<Following>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("followerCount")
    @Expose
    var followerCount: Int? = null
}
