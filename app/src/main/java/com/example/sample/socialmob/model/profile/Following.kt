package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Following {
    @SerializedName("recordId")
    @Expose
    var recordId: String? = null
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("username")
    @Expose
    var username: String? = null
    @SerializedName("ProfileId")
    @Expose
    var profileId: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("Email")
    @Expose
    var email: String? = null
    @SerializedName("Gender")
    @Expose
    var gender: String? = null
    @SerializedName("About")
    @Expose
    var about: String? = null
    @SerializedName("Location")
    @Expose
    var location: String? = null
    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
    @SerializedName("pimage")
    @Expose
    var pimage: String? = null
    @SerializedName("followingCount")
    @Expose
    var followingCount: String? = null
    @SerializedName("followerCount")
    @Expose
    var followerCount: String? = null
    @SerializedName("privateProfile")
    @Expose
    var privateProfile: Boolean? = null
    @SerializedName("celebrityVerified")
    @Expose
    var celebrityVerified: Boolean? = null
    @SerializedName("relation")
    @Expose
    var relation: String? = null
    @SerializedName("own")
    @Expose
    var own: String? = null

}
