package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FollowingResponseModelPayload {
    @SerializedName("followings")
    @Expose
    var followings: MutableList<Following>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("followingCount")
    @Expose
    var followingCount: Int? = null
}
