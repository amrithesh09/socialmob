package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetAllNotificationResponseModelPayload {
    @SerializedName("notifications")
    @Expose
    val notifications: MutableList<GetAllNotificationResponseModelPayloadNotification>? = null
    @SerializedName("unreadPennyCount")
    @Expose
    val unreadPennyCount: String? = null
    @SerializedName("unreadNotificationsCount")
    @Expose
    val unreadNotificationsCount: Int? = null
}
