package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GoogleData {
    @SerializedName("azp")
    @Expose
    var azp: String? = null
    @SerializedName("aud")
    @Expose
    var aud: String? = null
    @SerializedName("sub")
    @Expose
    var sub: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("email_verified")
    @Expose
    var emailVerified: Boolean? = null
    @SerializedName("exp")
    @Expose
    var exp: Int? = null
    @SerializedName("iss")
    @Expose
    var iss: String? = null
    @SerializedName("iat")
    @Expose
    var iat: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("picture")
    @Expose
    var picture: String? = null
    @SerializedName("given_name")
    @Expose
    var givenName: String? = null
    @SerializedName("family_name")
    @Expose
    var familyName: String? = null
    @SerializedName("locale")
    @Expose
    var locale: String? = null
}
