package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.search.HashTag
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class HashTagsResponseModelPayload {
    @SerializedName("hashTags")
    @Expose
    val hashTags: MutableList<HashTag>? = null
}
