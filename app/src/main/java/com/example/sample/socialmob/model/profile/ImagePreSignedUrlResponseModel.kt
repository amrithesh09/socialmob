package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImagePreSignedUrlResponseModel {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: ImagePreSignedUrlResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null
}
