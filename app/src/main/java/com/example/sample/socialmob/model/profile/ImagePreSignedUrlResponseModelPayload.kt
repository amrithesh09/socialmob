package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImagePreSignedUrlResponseModelPayload {
    @SerializedName("videoMedia")
    @Expose
    var videoMedia: VideoMedia? = null
}
