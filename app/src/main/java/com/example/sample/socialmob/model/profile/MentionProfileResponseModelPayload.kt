package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.login.Profile
import com.google.gson.annotations.Expose


class MentionProfileResponseModelPayload {
    @Expose
    var profiles: MutableList<Profile>? = null


}
