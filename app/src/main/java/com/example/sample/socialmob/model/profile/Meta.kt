package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Meta {
    @SerializedName("ogSiteName")
    @Expose
    val ogSiteName: String? = null

    @SerializedName("ogType")
    @Expose
    val ogType: String? = null

    @SerializedName("ogTitle")
    @Expose
    val ogTitle: String? = null

    @SerializedName("ogDescription")
    @Expose
    val ogDescription: String? = null

    @SerializedName("twitterCard")
    @Expose
    val twitterCard: String? = null

    @SerializedName("twitterSite")
    @Expose
    val twitterSite: String? = null

    @SerializedName("twitterTitle")
    @Expose
    val twitterTitle: String? = null

    @SerializedName("twitterDescription")
    @Expose
    val twitterDescription: String? = null

    @SerializedName("ogImage")
    @Expose
    val ogImage: OgImage? = null

    @SerializedName("twitterImage")
    @Expose
    val twitterImage: TwitterImage? = null

    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null

    @SerializedName("expiry")
    @Expose
    val expiry: String? = null

    @SerializedName("socialmob_meta")
    @Expose
    val socialmobMeta: SocialmobMeta? = null

}
