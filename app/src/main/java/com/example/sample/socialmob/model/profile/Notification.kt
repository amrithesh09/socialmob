package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Notification {
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("toUserId")
    @Expose
    val toUserId: String? = null
    @SerializedName("sourceType")
    @Expose
    val sourceType: String? = null
    @SerializedName("actors")
    @Expose
    val actors: NotificationActors? = null
    @SerializedName("sourceId")
    @Expose
    val sourceId: String? = null
    @SerializedName("actionType")
    @Expose
    val actionType: String? = null
    @SerializedName("actionTitle")
    @Expose
    val actionTitle: String? = null
    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null
    @SerializedName("read")
    @Expose
    val read: Boolean? = null
    @SerializedName("penny")
    @Expose
    val penny: Penny? = null
}
