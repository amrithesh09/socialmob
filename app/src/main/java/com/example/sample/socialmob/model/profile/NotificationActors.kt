package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class NotificationActors {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("username")
    @Expose
    val username: String? = null
    @SerializedName("ProfileId")
    @Expose
    val profileId: String? = null
    @SerializedName("Name")
    @Expose
    val name: String? = null
    @SerializedName("Email")

    @Expose
    val email: String? = null
    @SerializedName("pimage")
    @Expose
    val pimage: String? = null
    @SerializedName("Profile")
    @Expose
    val privateProfileProfile: Boolean? = null
}
