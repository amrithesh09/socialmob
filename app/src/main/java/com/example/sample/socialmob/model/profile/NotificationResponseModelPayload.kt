package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class NotificationResponseModelPayload {

    @SerializedName("success")
    @Expose
    val success: Boolean? = null
}
