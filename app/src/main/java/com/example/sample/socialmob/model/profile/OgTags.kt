package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.example.sample.socialmob.model.profile.Meta
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class OgTags : Parcelable {
    @SerializedName("_id")
    @Expose
    var id: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null

}
