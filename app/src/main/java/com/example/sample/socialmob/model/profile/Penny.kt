package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Penny {
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("toUserId")
    @Expose
    val toUserId: String? = null
    @SerializedName("fromUserId")
    @Expose
    val fromUserId: String? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null
    @SerializedName("status")
    @Expose
    val status: Int? = null
    @SerializedName("sourceId")
    @Expose
    val sourceId: String? = null
}
