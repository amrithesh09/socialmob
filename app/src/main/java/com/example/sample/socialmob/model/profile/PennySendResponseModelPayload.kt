package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PennySendResponseModelPayload {
    @SerializedName("success")
    @Expose
    val success: Boolean? = null
    @SerializedName("remainingPenny")
    @Expose
    val remainingPenny: Int? = null
    @SerializedName("totalPenny")
    @Expose
    val totalPenny: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
}
