package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.feed.MobSpaceCategories
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PersonalFeedResponseModelPayload {

    @SerializedName("feeds")
    @Expose
    var feeds: MutableList<PersonalFeedResponseModelPayloadFeed>? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("suggestedMobbers")
    @Expose
    var suggestedMobbers: List<SearchProfileResponseModelPayloadProfile>? = null

    @SerializedName("mobSpaceCategories")
    @Expose
    var mobSpaceCategories: List<MobSpaceCategories>? = null

    @SerializedName("displayMobSpaceIntro")
    @Expose
    var displayMobSpaceIntro: Boolean? = null

    @SerializedName("hashTag")
    @Expose
    val hashTag: PersonalFeedResponseModelPayloadHashTag? = null

}
