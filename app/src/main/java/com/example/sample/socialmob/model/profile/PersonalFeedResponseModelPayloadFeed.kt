package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeed : Parcelable {


    @SerializedName("actor")
    @Expose
    var actor: PersonalFeedResponseModelPayloadFeedActor? = null

    @SerializedName("recordId")
    @Expose
    var recordId: String? = null

    @SerializedName("hashTagId")
    @Expose
    var hashTagId: String? = null

    @SerializedName("type")
    @Expose
    var type: String? = ""

    @SerializedName("hashTagName")
    @Expose
    var hashTagName: String? = ""

    @SerializedName("action")
    @Expose
    var action: PersonalFeedResponseModelPayloadFeedAction? = null

    @Expose
    var isLoader: String? = null

    @Expose
    var isAd: String? = null

}
