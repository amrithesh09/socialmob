package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeedAction : Parcelable {


    @SerializedName("actionId")

    var actionId: String? = null

    @SerializedName("title")

    var title: String? = null

    @SerializedName("post")

    var post: PersonalFeedResponseModelPayloadFeedActionPost? = null

    @SerializedName("created_date")
    var created_date: String? = null


}
