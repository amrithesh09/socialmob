package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.example.sample.socialmob.model.feed.PostFeedDataMention
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeedActionComment : Parcelable {

    @SerializedName("commenId")
    var commenId: String? = null

    @SerializedName("text")
    var test: String? = null

    @SerializedName("profileId")
    var profileId: String? = null

    @SerializedName("created_date")
    var craeted_date: String? = null

    @SerializedName("mentions")
    @Expose
    var mentions: List<PostFeedDataMention>? = null

    @SerializedName("profile")
    @Expose
    var profile: UserProfileResponseModelPayloadProfile? = null

    @SerializedName("status")
    @Expose
    val status: Int? = null

}
