package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.example.sample.socialmob.model.feed.PostFeedDataMention
import com.example.sample.socialmob.model.search.HashTag
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeedActionPost : Parcelable {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("profileId")
    @Expose
    var profileId: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("privacy")
    @Expose
    var privacy: String? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("hashTags")
    @Expose
    var hashTags: List<HashTag>? = null

    @SerializedName("mentions")
    @Expose
    var mentions: List<PostFeedDataMention>? = null

    @SerializedName("commentsCount")
    @Expose
    var commentsCount: String? = null

    @SerializedName("media")
    @Expose
    var media: List<PersonalFeedResponseModelPayloadFeedActionPostMedium>? = null

    @SerializedName("own")
    @Expose
    var own: Boolean? = null

    @SerializedName("likesCount")
    @Expose
    var likesCount: String? = null

    @SerializedName("liked")
    @Expose
    var liked: Boolean? = null

    @SerializedName("ogTags")
    @Expose
    var ogTags: OgTags? = null

    @SerializedName("likedConnections")
    @Expose
    var likedConnections: List<PersonalFeedResponseModelPayloadFeedActionlikedConnections>? = null

    @SerializedName("lastComment")
    @Expose
    var lastComment: PersonalFeedResponseModelPayloadFeedActionComment? = null
}
