package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeedActionPostMedium : Parcelable {
    @SerializedName("sourcePath")
    @Expose
    var sourcePath: String? = null
    @SerializedName("width")
    @Expose
    var width: Int? = null
    @SerializedName("height")
    @Expose
    var height: Int? = null
    @SerializedName("orientation")
    @Expose
    var orientation: String? = null
    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null
    @SerializedName("ratio")
    @Expose
    var ratio: String? = null
}
