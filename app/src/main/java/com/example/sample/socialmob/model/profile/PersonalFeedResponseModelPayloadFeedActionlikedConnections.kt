package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeedActionlikedConnections : Parcelable {


    @SerializedName("_id")
    var _id: String? = null

    @SerializedName("Name")
    var Name: String? = null

    @SerializedName("pimage")
    var pimage: String? = null

    @SerializedName("username")
    var username: String? = null

}
