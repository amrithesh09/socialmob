package com.example.sample.socialmob.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalFeedResponseModelPayloadFeedActor : Parcelable {

    @SerializedName("_id")
    var id: String? = null
    @SerializedName("ProfileId")
    var profileId: String? = null
    @SerializedName("Name")
    var name: String? = null
    @SerializedName("username")
    var username: String? = null
    @SerializedName("relation")
    var relation: String? = null
    @SerializedName("own")
    var own: String? = null
    @SerializedName("pimage")
    var pimage: String? = null
    @SerializedName("privateProfile")
    var privateProfile: Boolean? = null
    @SerializedName("celebrityVerified")
    var celebrityVerified: Boolean? = null

}
