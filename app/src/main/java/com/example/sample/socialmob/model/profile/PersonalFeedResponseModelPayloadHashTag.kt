package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class PersonalFeedResponseModelPayloadHashTag {
    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("name")
    @Expose
    val name: String? = null

    @SerializedName("description")
    @Expose
    val description: String? = null

    @SerializedName("cover_image")
    @Expose
    val coverImage: String? = null

    @SerializedName("campaign_mode")
    @Expose
    val campaign_mode: String? = ""


}
