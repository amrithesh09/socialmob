package com.example.sample.socialmob.model.profile


class PostFeedData {
    var description: String? = null
    var type: String? = null
    var media: List<PostFeedDataMedia>? = null
    var ogTags: OgTags? = null
}
