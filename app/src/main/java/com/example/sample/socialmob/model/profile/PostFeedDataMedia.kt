package com.example.sample.socialmob.model.profile

class PostFeedDataMedia {

    var filename: String? = null
    var width: String? = null
    var height: String? = null
    var orientation: String? = null
    var thumbnailFilename: String? = null
    var ratio: String = ""
}
