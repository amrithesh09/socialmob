package com.example.sample.socialmob.model.profile

import java.io.Serializable

class ProfileRegisterData constructor(
   var Email: String?,
   var Password: String?,
   var Gender: String?,
   var Location: String?,
   var Dob: String?,
   var Name: String?,
   var ZodiacSign: String?,
   var MainTags: IntArray?,
   var Personalities: IntArray?,
   var platformid: String?,
   var AppVersion: String?,
   var referrer_id: String?,
   var SystemFileName: String?,
   var width: String?,
   var height: String?,
   var orientation: String?
) : Serializable
