package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.profile.OgTags
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ScrapUrlResponseModelPayload {
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
    @SerializedName("ogTags")
    @Expose
    var ogTags: OgTags? = null


}
