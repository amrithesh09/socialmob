package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.MusicVideoPayloadVideo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SocialmobMeta {
    @SerializedName("pageType")
    @Expose
    val pageType: String? = null

    @SerializedName("source")
    @Expose
    val source: String? = null

    @SerializedName("artist")
    @Expose
    val artist: Artist? = null

    @SerializedName("track")
    @Expose
    val track: TrackListCommon? = null

    @SerializedName("playlist")
    @Expose
    val playlist: PlaylistCommon? = null

    @SerializedName("profile")
    @Expose
    val profile: Profile? = null

    @SerializedName("track_video")
    @Expose
    val trackVideo: MusicVideoPayloadVideo? = null

}
