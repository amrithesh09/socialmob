package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.viewmodel.feed.MainTagsResponseModelPayloadMainTag
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SubResponseModelPayload {
    @SerializedName("hashTags")
    @Expose
    val hashTags: List<MainTagsResponseModelPayloadMainTag>? = null

}
