package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TrackRecommendation {
    @SerializedName("JukeBoxTrackId")
    @Expose
    var jukeBoxTrackId: Int? = null
    @SerializedName("TrackName")
    @Expose
    var trackName: String? = null
    @SerializedName("JukeBoxCategoryName")
    @Expose
    var jukeBoxCategoryName: String? = null
    @SerializedName("TrackFile")
    @Expose
    var trackFile: String? = null
    @SerializedName("TrackImage")
    @Expose
    var trackImage: String? = null
    @SerializedName("Length")
    @Expose
    var length: Int? = null


}
