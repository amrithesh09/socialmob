package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TwitterImage {
    @SerializedName("url")
    @Expose
    val url: String? = null
    @SerializedName("width")
    @Expose
    val width: Any? = null
    @SerializedName("height")
    @Expose
    val height: Any? = null
    @SerializedName("alt")
    @Expose
    val alt: Any? = null
}
