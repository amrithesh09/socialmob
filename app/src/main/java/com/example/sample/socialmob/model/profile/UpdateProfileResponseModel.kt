package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UpdateProfileResponseModel {
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("code")
    @Expose
    val code: Int? = null
    @SerializedName("payload")
    @Expose
    val payload: UpdateProfileResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    val now: String? = null
}