package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UpdateProfileResponseModelPayload {
    @SerializedName("success")
    @Expose
     val success: Boolean? = null
    @SerializedName("profile")
    @Expose
     val profile: UpdateProfileResponseModelPayloadProfile? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
}