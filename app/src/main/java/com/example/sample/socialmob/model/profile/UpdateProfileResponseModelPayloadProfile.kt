package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UpdateProfileResponseModelPayloadProfile {
    @SerializedName("n")
    @Expose
     val n: Int? = null
    @SerializedName("nModified")
    @Expose
     val nModified: Int? = null
    @SerializedName("ok")
    @Expose
     val ok: Int? = null
}
