package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UserNameAvailableResponsePayload {
    @SerializedName("available")
    @Expose
    val available: Boolean? = null
}
