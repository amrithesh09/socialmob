package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.login.JukeboxFavorite
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserProfileResponseModelPayload {
    @SerializedName("profile")
    @Expose
    var profile: UserProfileResponseModelPayloadProfile? = null
    @SerializedName("jukeboxFavorites")
    @Expose
    var jukeboxFavorites: List<JukeboxFavorite>? = null
    @SerializedName("photoGrid")
    @Expose
    var photoGrid: List<Any>? = null
    @SerializedName("feeds")
    @Expose
    var feeds: List<Any>? = null
    @SerializedName("profileAnthemTrack")
    @Expose
    var profileAnthemTrack: TrackListCommon? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("remainingPenny")
    @Expose
    var remainingPenny: Int? = null
    @SerializedName("totalPenny")
    @Expose
    var totalPenny: Int? = null
    @SerializedName("relation")
    @Expose
    var relation: String? = null
    @SerializedName("conv_id")
    @Expose
    var conv_id: String? = null

}
