package com.example.sample.socialmob.model.profile

import com.example.sample.socialmob.model.profile.GoogleData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserProfileResponseModelPayloadProfile {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("ProfileId")
    @Expose
    var profileId: String? = null

    @SerializedName("Name")
    @Expose
    var name: String? = null

    @SerializedName("Email")
    @Expose
    var email: String? = null

    @SerializedName("Dob")
    @Expose
    var dob: String? = null

    @SerializedName("Gender")
    @Expose
    var gender: String? = null

    @SerializedName("About")
    @Expose
    var about: String? = null

    @SerializedName("Location")
    @Expose
    var location: String? = null

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null

    @SerializedName("pimage")
    @Expose
    var pimage: String? = null

    @SerializedName("privateProfile")
    @Expose
    var privateProfile: Boolean? = null

    @SerializedName("googleData")
    @Expose
    var googleData: GoogleData? = null

    @SerializedName("referrer_id")
    @Expose
    var referrerId: Any? = null

    @SerializedName("followerCount")
    @Expose
    var followerCount: String? = null

    @SerializedName("followingCount")
    @Expose
    var followingCount: String? = null

    @SerializedName("avatarAlbumId")
    @Expose
    var avatarAlbumId: String? = null

    @SerializedName("pImage")
    @Expose
    var pImage: String? = null

    @SerializedName("emailVerified")
    @Expose
    var emailVerified: String? = null

    @SerializedName("celebrityVerified")
    @Expose
    var celebrityVerified: String? = null

    @SerializedName("totalPenny")
    @Expose
    var totalPenny: String? = null

    @SerializedName("remainingInvite")
    @Expose
    var remainingInvite: String? = null

    @SerializedName("statusId")
    @Expose
    var statusId: String? = null

    @SerializedName("userStatus")
    @Expose
    var userStatus: Any? = null

    @SerializedName("relation")
    @Expose
    var relation: String? = null

    @SerializedName("own")
    @Expose
    var own: String? = null

    @SerializedName("profileAnthemTrackId")
    @Expose
    val profileAnthemTrackId: String? = null

    @SerializedName("username")
    @Expose
    var username: String? = null

}
