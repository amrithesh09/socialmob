package com.example.sample.socialmob.model.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VideoMedia {
    @SerializedName("rawFileNames")
    @Expose
    var rawFileNames: List<String>? = null
    @SerializedName("rawFileUrls")
    @Expose
    var rawFileUrls: List<String>? = null
    @SerializedName("thumbnailFileNames")
    @Expose
    var thumbnailFileNames: List<String>? = null
    @SerializedName("thumbnailUrls")
    @Expose
    var thumbnailUrls: List<String>? = null

}
