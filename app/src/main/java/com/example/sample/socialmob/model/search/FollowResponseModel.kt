package com.example.sample.socialmob.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FollowResponseModel {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("payload")
    @Expose
    var payload: FollowResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null

}
