package com.example.sample.socialmob.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FollowResponseModelPayload {
    @SerializedName("success")
    @Expose
    var success: Boolean? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
}
