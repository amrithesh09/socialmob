package com.example.sample.socialmob.model.search

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "searchHashTag")
data class HashTag(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "name") val name: String? = null,
    @ColumnInfo(name = "_id") val _id: String? = null

) : Parcelable
