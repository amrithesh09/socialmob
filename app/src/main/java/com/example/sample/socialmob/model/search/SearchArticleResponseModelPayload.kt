package com.example.sample.socialmob.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchArticleResponseModelPayload {
    @SerializedName("contents")
    @Expose
    var contents: List<SearchArticleResponseModelPayloadContent>? = null
}
