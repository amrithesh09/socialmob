package com.example.sample.socialmob.model.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "searchArticle")
data class SearchArticleResponseModelPayloadContent(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "Slno") val Slno: String? = null,
    @ColumnInfo(name = "ContentId") val ContentId: String? = null,
    @ColumnInfo(name = "Title") val Title: String? = null,
    @ColumnInfo(name = "PublishedDate") val PublishedDate: String? = null,
    @ColumnInfo(name = "Description") val Description: String? = null,
    @ColumnInfo(name = "MappedGender") val MappedGender: String? = null,
    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null,
    @ColumnInfo(name = "Bookmarked") var Bookmarked: String? = null

)

//class SearchArticleResponseModelPayloadContent {
//
//    @SerializedName("Slno")
//    @Expose
//    var slno: Int? = null
//    @SerializedName("ContentId")
//    @Expose
//    var contentId: Int? = null
//    @SerializedName("Title")
//    @Expose
//    var title: String? = null
//    @SerializedName("PublishedDate")
//    @Expose
//    var publishedDate: String? = null
//    @SerializedName("Description")
//    @Expose
//    var description: String? = null
//    @SerializedName("MappedGender")
//    @Expose
//    var mappedGender: String? = null
//    @SerializedName("SourcePath")
//    @Expose
//    var sourcePath: String? = null
//    @SerializedName("Bookmarked")
//    @Expose
//    var bookmarked: Boolean? = null
//
//}
