package com.example.sample.socialmob.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchHashTagResponseModelPayload {
    @SerializedName("hashTags")
    @Expose
    var hashTags: MutableList<HashTag>? = null

}
