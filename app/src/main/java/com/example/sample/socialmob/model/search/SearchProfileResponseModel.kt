package com.example.sample.socialmob.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchProfileResponseModel {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("code")
    @Expose
    var code: String? = null
    @SerializedName("payload")
    @Expose
    var payload: SearchProfileResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    var now: String? = null
}
