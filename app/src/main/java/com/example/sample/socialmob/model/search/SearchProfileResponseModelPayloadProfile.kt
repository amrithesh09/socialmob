package com.example.sample.socialmob.model.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "searchProfile")
data class SearchProfileResponseModelPayloadProfile(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "_id") var _id: String? = null,
    @ColumnInfo(name = "ProfileId") val ProfileId: String? = null,
    @ColumnInfo(name = "Name") val Name: String? = null,
    @ColumnInfo(name = "username") val username: String? = null,
    @ColumnInfo(name = "Email") val Email: String? = null,
    @ColumnInfo(name = "Location") val Location: String? = null,
    @ColumnInfo(name = "SourcePath") val SourcePath: String? = null,
    @ColumnInfo(name = "thumbnail") val thumbnail: String? = null,
    @ColumnInfo(name = "pimage") val pimage: String? = null,
    @ColumnInfo(name = "privateProfile") val privateProfile: String? = null,
    @ColumnInfo(name = "celebrityVerified") val celebrityVerified: Boolean? = null,
    @ColumnInfo(name = "own") val own: String? = null,
    @ColumnInfo(name = "relation") var relation: String? = null,
    @ColumnInfo(name = "conv_id") var conv_id: String? = ""

)


//class SearchProfileResponseModelPayloadProfile {
//    @SerializedName("_id")
//    @Expose
//    var id: String? = null
//    @SerializedName("ProfileId")
//    @Expose
//    var profileId: Int? = null
//    @SerializedName("Name")
//    @Expose
//    var name: String? = null
//    @SerializedName("Email")
//    @Expose
//    var email: String? = null
//    @SerializedName("Location")
//    @Expose
//    var location: String? = null
//    @SerializedName("SourcePath")
//    @Expose
//    var sourcePath: Any? = null
//    @SerializedName("thumbnail")
//    @Expose
//    var thumbnail: String? = null
//    @SerializedName("pimage")
//    @Expose
//    var pimage: String? = null
//    @SerializedName("privateProfile")
//    @Expose
//    var privateProfile: Boolean? = null
//    @SerializedName("celebrityVerified")
//    @Expose
//    var celebrityVerified: Boolean? = null
//    @SerializedName("own")
//    @Expose
//    var own: Boolean? = null
//    @SerializedName("relation")
//    @Expose
//    var relation: String? = null
//
//}
