package com.example.sample.socialmob.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchTagResponseModelPayload {

    @SerializedName("tags")
    @Expose
    var tags: List<SearchTagResponseModelPayloadTag>? = null

}
