package com.example.sample.socialmob.model.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "searchTag")
data class SearchTagResponseModelPayloadTag(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "TagId") val TagId: String? = null,
    @ColumnInfo(name = "TagName") val TagName: String? = null,
    @ColumnInfo(name = "FollowerCount") val FollowerCount: String? = null,
    @ColumnInfo(name = "SquareImage") val SquareImage: String? = null,
    @ColumnInfo(name = "Following") var Following: String? = null

)