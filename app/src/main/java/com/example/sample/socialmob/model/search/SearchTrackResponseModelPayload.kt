package com.example.sample.socialmob.model.search

import com.example.sample.socialmob.model.music.TrackListCommon
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchTrackResponseModelPayload {
    @SerializedName("tracks")
    @Expose
    var tracks: MutableList<TrackListCommon>? = null
}
