package com.example.sample.socialmob.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ContactListDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertContacts(mMessage: List<ContactModel>)

    @Query("DELETE FROM ContactModel")
    fun deleteAllMessage()

    @Query("SELECT * FROM ContactModel WHERE isAdded=:isAdded")
    fun getAllContacts(isAdded: String): LiveData<List<ContactModel>>


    @Query("UPDATE ContactModel SET isAdded=:isAdded WHERE phone=:phone")
    fun updateAdded(isAdded: String, phone: String)
}