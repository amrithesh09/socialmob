package com.example.sample.socialmob.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.view.ui.home_module.chat.model.PreSignedPayLoadList

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface DbChatFileList {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFile(mMessage: List<PreSignedPayLoadList>)

    @Query("DELETE FROM PreSignedPayLoadList")
    fun deleteAllMessage()

    @Query("DELETE FROM PreSignedPayLoadList WHERE filename=:file_name")
    fun deleteFile(file_name: String)

    @Query("SELECT * FROM PreSignedPayLoadList ORDER BY filename ASC")
    fun getAllFiles(): LiveData<List<PreSignedPayLoadList>>


    @Query("UPDATE PreSignedPayLoadList SET status=:status WHERE filename=:mFilename")
    fun updateStatus(mFilename: String, status: String)
}
