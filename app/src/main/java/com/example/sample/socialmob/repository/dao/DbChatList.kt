package com.example.sample.socialmob.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.chat.MessageListItem
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface DbChatList {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewMessage(mMessage: List<MessageListItem>)

    @Query("DELETE FROM messageListItem")
    fun deleteAllMessage()

    @Query("DELETE FROM messageListItem WHERE timestamp=:mId")
    fun deleteOfflineMessage(mId: String)

    @Query("SELECT * FROM messageListItem ORDER BY timestamp ASC")
    fun getAllMessage(): LiveData<List<MessageListItem>>

}
