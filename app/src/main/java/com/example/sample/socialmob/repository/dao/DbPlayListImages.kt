package com.example.sample.socialmob.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.music.IconDatumIcon
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface DbPlayListImages {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIconData(list: MutableList<IconDatumIcon>)

    @Query("DELETE FROM iconData")
    fun deleteAllIconData()

    @Query("SELECT * FROM iconData where category_name =:mCategoeryName")
    fun getAllIconData(mCategoeryName: String): LiveData<List<IconDatumIcon>>

    @Query("UPDATE iconData SET is_selected=:value WHERE _id=:id")
    fun setSelected(id: String, value: Boolean)
}
