package com.example.sample.socialmob.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.music.PodcastHistoryTrack
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface DbPodcastHistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDbPodcastHistory(list: MutableList<PodcastHistoryTrack>)

    @Query("DELETE FROM podcastHistoryTrack")
    fun deleteAllDbPodcastHistory()

    @Query("select rpe.*,pcp.percentage,pcp.millisecond from podcastHistoryTrack as rpe left join podCastProgress as pcp on pcp.episodeId=rpe.EpisodeId")
    fun getAllDbPodcastHistory(): LiveData<List<PodcastHistoryTrack>>

    @Query("select rpe.*,pcp.percentage,pcp.millisecond from podcastHistoryTrack as rpe left join podCastProgress as pcp on pcp.episodeId=rpe.EpisodeId order by rpe.timeStamp DESC limit 3")
    fun getAllDbPodcastHistoryLast(): LiveData<List<PodcastHistoryTrack>>

}