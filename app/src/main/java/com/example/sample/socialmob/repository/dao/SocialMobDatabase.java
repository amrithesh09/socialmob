package com.example.sample.socialmob.repository.dao;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.sample.socialmob.model.article.Bookmark;
import com.example.sample.socialmob.model.article.HalfscreenDashboard;
import com.example.sample.socialmob.model.article.UserReadArticle;
import com.example.sample.socialmob.repository.dao.article.ArticleDashBoardDao;
import com.example.sample.socialmob.repository.dao.article.BookMarkedArticleDao;
import com.example.sample.socialmob.repository.dao.article.HistoryArticleDao;
import com.example.sample.socialmob.repository.dao.article.RecommendedArticleDao;
import com.example.sample.socialmob.repository.dao.login.LoginDao;
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioDao;
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioPodCastListDao;
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload;
import com.example.sample.socialmob.repository.model.TrackListCommonDb;
import com.example.sample.socialmob.repository.dao.search.SearchArticleDao;
import com.example.sample.socialmob.repository.dao.search.SearchTagDao;
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList;
import com.example.sample.socialmob.repository.model.PlayListDataClass;
import com.example.sample.socialmob.view.ui.home_module.chat.model.PreSignedPayLoadList;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DbTrackAnalytics;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DbTrackProgress;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PodCastAnalytics;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PodCastProgress;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.TrackAnalytics;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.TrackAnalyticsData;
import com.example.sample.socialmob.model.article.ArticleData;
import com.example.sample.socialmob.model.dash_board.FullScreenDashboard;
import com.example.sample.socialmob.model.login.Profile;
import com.example.sample.socialmob.model.music.Episode;
import com.example.sample.socialmob.model.music.IconDatumIcon;
import com.example.sample.socialmob.model.music.Playlist;
import com.example.sample.socialmob.model.music.Podcast;
import com.example.sample.socialmob.model.music.PodcastHistoryTrack;
import com.example.sample.socialmob.model.music.RadioPodCastEpisode;
import com.example.sample.socialmob.model.music.Station;
import com.example.sample.socialmob.model.search.SearchTagResponseModelPayloadTag;
import com.example.sample.socialmob.model.search.SearchArticleResponseModelPayloadContent;
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile;
import com.example.sample.socialmob.model.chat.MessageListItem;

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Database(entities = {Episode.class, Podcast.class, RadioPodCastEpisode.class,
        FullScreenDashboard.class, Station.class, Playlist.class,
        Profile.class, SearchProfileResponseModelPayloadProfile.class,
        SearchArticleResponseModelPayloadContent.class, SearchTagResponseModelPayloadTag.class,
        UserReadArticle.class, Bookmark.class, ArticleData.class, HalfscreenDashboard.class,
        PlayListDataClass.class, PodCastAnalytics.class, PodCastProgress.class,
        TrackAnalyticsData.class, PodcastHistoryTrack.class, IconDatumIcon.class, TrackListCommonDb.class,
        MessageListItem.class, PreSignedPayLoadList.class, ContactModel.class}, version = 19, exportSchema = false)
public abstract class SocialMobDatabase extends RoomDatabase {

    public abstract ContactListDao contactListDaoDao();

    public abstract RadioPodCastListDao radioPodcastListDao();

    public abstract RadioDao radioDao();

    public abstract TrackDownload trackDownloadDao();

    public abstract CreateDbPlayList createplayListDao();

    public abstract LoginDao loginDao();

    public abstract SearchArticleDao searchArticleDao();

    public abstract SearchTagDao searchTagDao();

    public abstract HistoryArticleDao historyArticleDao();

    public abstract BookMarkedArticleDao bookMarkedArticleDao();

    public abstract RecommendedArticleDao recommendedArticleDao();

    public abstract ArticleDashBoardDao articleDashBoardDao();

    public abstract DbTrackAnalytics trackAnalytics();

    public abstract DbPodcastHistoryDao dbPodcastHistory();

    public abstract TrackAnalytics musicTrackAnalytics();

    public abstract DbTrackProgress trackProgress();

    public abstract DbPlayListImages playListImages();

    public abstract DbChatList dbChatList();

    public abstract DbChatFileList dbChatFileList();

    private static SocialMobDatabase INSTANCE;

    public static SocialMobDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (SocialMobDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SocialMobDatabase.class, "social_mob_database")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static Callback sRoomDatabaseCallback =
            new Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final DbPodcastHistoryDao mDao;

        PopulateDbAsync(SocialMobDatabase db) {
            mDao = db.dbPodcastHistory();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            return null;
        }
    }
}
