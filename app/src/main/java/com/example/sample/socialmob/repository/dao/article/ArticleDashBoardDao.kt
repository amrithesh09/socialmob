package com.example.sample.socialmob.repository.dao.article

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.article.HalfscreenDashboard

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface ArticleDashBoardDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticleDashBoard(list: List<HalfscreenDashboard>)

    @Query("DELETE FROM userArticleDashboard")
    fun deleteAllArticleDashBoard()

    @Query("SELECT * FROM userArticleDashboard")
    fun getAllArticleDashBoard(): LiveData<List<HalfscreenDashboard>>

//    @Query("UPDATE userArticleBookmark SET Bookmarked=:mValue WHERE ContentId=:mId")
//    fun deleteBookmark(mValue: String, mId: String)
}