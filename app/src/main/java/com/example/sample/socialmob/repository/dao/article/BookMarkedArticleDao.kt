package com.example.sample.socialmob.repository.dao.article

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.article.Bookmark
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface BookMarkedArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBookMarkedArticle(list: List<Bookmark>)

    @Query("DELETE FROM userArticleBookmark")
    fun deleteAllBookMarkedArticle()

    @Query("SELECT * FROM userArticleBookmark")
    fun getAllBookMarkedArticle(): LiveData<List<Bookmark>>

//    @Query("UPDATE userArticleBookmark SET Bookmarked=:mValue WHERE ContentId=:mId")
//    fun deleteBookmark(mValue: String, mId: String)

    @Query("DELETE FROM userArticleBookmark WHERE ContentId=:mId")
    fun deleteBookmark(mId: String)


}
