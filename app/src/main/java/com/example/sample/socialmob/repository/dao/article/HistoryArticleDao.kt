package com.example.sample.socialmob.repository.dao.article

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.article.UserReadArticle
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface HistoryArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHistoryArticle(list: List<UserReadArticle>)

    @Query("DELETE FROM userReadArticle")
    fun deleteAllHistoryArticle()

    @Query("SELECT * FROM userReadArticle")
    fun getAllHistoryArticle(): LiveData<List<UserReadArticle>>

    @Query("UPDATE userReadArticle SET Bookmarked=:mValue WHERE ContentId=:mId")
    fun updateBookmark(mValue: String, mId: String)


}
