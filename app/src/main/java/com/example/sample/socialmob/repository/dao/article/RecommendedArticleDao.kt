package com.example.sample.socialmob.repository.dao.article

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.article.ArticleData
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface RecommendedArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecommendedArticle(list: List<ArticleData>)

    @Query("DELETE FROM userRecommendedBookmark")
    fun deleteAllRecommendedArticle()

    @Query("SELECT * FROM userRecommendedBookmark")
    fun getAllRecommendedArticle(): LiveData<List<ArticleData>>

    @Query("UPDATE userRecommendedBookmark SET Bookmarked=:mValue WHERE ContentId = :mId")
    fun updateBookmark(mValue: String, mId: String)

    @Query("SELECT * FROM userRecommendedBookmark WHERE ContentId = :mId")
    fun getRecommendedArticleById(mId: String):LiveData<ArticleData>


}
