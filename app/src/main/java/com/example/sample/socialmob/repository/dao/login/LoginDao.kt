package com.example.sample.socialmob.repository.dao.login

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.login.Profile

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface LoginDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProfileData(list: Profile)

    @Query("DELETE FROM Profile")
    fun deleteAllProfileData()

    @Query("SELECT * FROM Profile")
    fun getAllProfileData(): LiveData<Profile>

    @Query("UPDATE Profile SET followingCount=:mCount")
    fun upDateFollowersCount(mCount: String)

    @Query("UPDATE Profile SET Name=:mName")
    fun upDateName(mName: String)

    @Query("UPDATE Profile SET username=:mUserName")
    fun upDateUserName(mUserName: String)

    @Query("UPDATE Profile SET Location=:mlocation")
    fun upDateLocation(mlocation: String)

    @Query("UPDATE Profile SET pimage=:pimage")
    fun upDateProfileImage(pimage: String)

}