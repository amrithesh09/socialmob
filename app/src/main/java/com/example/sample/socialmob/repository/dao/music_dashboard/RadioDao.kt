package com.example.sample.socialmob.repository.dao.music_dashboard

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.music.Station
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface RadioDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRadio(list: List<Station>)

    @Query("DELETE FROM stationList")
    fun deleteAllRadio()

    @Query("SELECT * FROM stationList")
    fun getAllRadio(): LiveData<List<Station>>
}