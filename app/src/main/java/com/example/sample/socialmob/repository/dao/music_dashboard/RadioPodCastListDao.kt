package com.example.sample.socialmob.repository.dao.music_dashboard

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface RadioPodCastListDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRadioPodCastList(list: List<RadioPodCastEpisode>)

    @Query("DELETE FROM radioPodcastEpisode")
    fun deleteAllRadioPodCastList()

    @Query("select rpe.*,pcp.percentage,pcp.millisecond from radioPodcastEpisode as rpe left join podCastProgress as pcp on pcp.episodeId=rpe.number_id")
    fun getRadioPodCastList(): LiveData<List<RadioPodCastEpisode>>


    @Query("UPDATE radioPodcastEpisode SET isPlaying=:isPlaying WHERE EpisodeId=:id")
    fun setPlayingTrue(id: String, isPlaying: String)

    @Query("UPDATE radioPodcastEpisode SET isPlaying=:isPlaying WHERE isPlaying=:isPlayingUpdateStatus")
    fun setPlayingFalse(isPlaying: String, isPlayingUpdateStatus: String)

}
//UPDATE radioPodcastEpisode SET isPlaying = "false" WHERE isPlaying = "true"