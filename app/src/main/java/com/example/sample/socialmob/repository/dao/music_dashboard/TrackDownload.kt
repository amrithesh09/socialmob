package com.example.sample.socialmob.repository.dao.music_dashboard

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.repository.model.TrackListCommonDb

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface TrackDownload {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTrackDownload(list: MutableList<TrackListCommonDb>)

    @Query("DELETE FROM trackListDb")
    fun deleteAllTrackDownload()


    @Query("DELETE FROM trackListDb WHERE track_file=:mUrl")
    fun deleteOfflineTrack(mUrl: String)

    @Query("SELECT * FROM trackListDb")
    fun getAllTrackDownload(): LiveData<MutableList<TrackListCommonDb>>


    @Query("SELECT * FROM trackListDb where is_downloaded=:value")
    fun getTrackDownloadedFalse(value: String): LiveData<MutableList<TrackListCommonDb>>

    @Query("SELECT * FROM trackListDb where is_downloaded=:value")
    fun getTrackDownloadedTrue(value: String): LiveData<MutableList<TrackListCommonDb>>

    @Query("UPDATE trackListDb SET is_downloaded=:value WHERE _id=:id")
    fun updateDownloadStatus(id: String, value: String)
}