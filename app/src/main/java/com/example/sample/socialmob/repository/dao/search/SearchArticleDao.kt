package com.example.sample.socialmob.repository.dao.search

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.search.SearchArticleResponseModelPayloadContent
/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 *//**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface SearchArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSearchArticle(list: List<SearchArticleResponseModelPayloadContent>)

    @Query("DELETE FROM searchArticle")
    fun deleteAllSearchArticle()

    @Query("SELECT * FROM searchArticle")
    fun getAllSearchArticle(): LiveData<List<SearchArticleResponseModelPayloadContent>>

    @Query("UPDATE searchArticle SET Bookmarked=:mValue WHERE ContentId=:mId")
    fun updateBookmark(mValue: String, mId: String)

}
