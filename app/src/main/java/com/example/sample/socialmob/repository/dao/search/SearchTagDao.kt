package com.example.sample.socialmob.repository.dao.search

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.model.search.SearchTagResponseModelPayloadTag

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface SearchTagDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSearchTag(list: List<SearchTagResponseModelPayloadTag>)

    @Query("DELETE FROM searchTag")
    fun deleteAllSearchTag()

    @Query("SELECT * FROM searchTag")
    fun getAllSearchTag(): LiveData<List<SearchTagResponseModelPayloadTag>>

    @Query("UPDATE searchTag SET Following=:mValue WHERE TagId=:mId")
    fun updateFollowing(mValue: String, mId: String)


}

