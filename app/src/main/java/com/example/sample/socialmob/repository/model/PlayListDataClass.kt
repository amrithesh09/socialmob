package com.example.sample.socialmob.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "playListDataClass")
data class PlayListDataClass(
    @PrimaryKey
    @ColumnInfo(name = "JukeBoxTrackId") val JukeBoxTrackId: String,
    @ColumnInfo(name = "JukeBoxCategoryName") val JukeBoxCategoryName: String,
    @ColumnInfo(name = "TrackName") val TrackName: String,
    @ColumnInfo(name = "Author") val Author: String,
    @ColumnInfo(name = "TrackFile") val TrackFile: String,
    @ColumnInfo(name = "AllowOffline") val AllowOffline: String,
    @ColumnInfo(name = "Length") val Length: String,
    @ColumnInfo(name = "TrackImage") val TrackImage: String,
    @ColumnInfo(name = "percentage") val percentage: String,
    @ColumnInfo(name = "millisecond") val Millisecond: String,
    @ColumnInfo(name = "Favorite") var Favorite: String,
    @ColumnInfo(name = "mExtraId") var mExtraId: String,
    @ColumnInfo(name = "mExtraIdPodCast") var mExtraIdPodCast: String,
    @ColumnInfo(name = "artistId") var artistId: String,
    @ColumnInfo(name = "albumName") var albumName: String,
    @ColumnInfo(name = "albumId") var albumId: String,
    @ColumnInfo(name = "playCount") var playCount: String,
    @ColumnInfo(name = "playListName") var playListName: String
)
