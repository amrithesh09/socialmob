package com.example.sample.socialmob.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName =  "trackListDb")
data class TrackListCommonDb(
    @PrimaryKey
    @ColumnInfo(name = "_id") var _id: String,
    @ColumnInfo(name = "number_id") var number_id: String,
    @ColumnInfo(name = "track_name") var track_name: String,
    @ColumnInfo(name = "track_image") var trackImage: String,
    @ColumnInfo(name = "track_file") var trackFile: String,
    @ColumnInfo(name = "artist") var artist: String,
    @ColumnInfo(name = "genre") var genre: String,
    @ColumnInfo(name = "is_downloaded") var is_downloaded: String,
    @ColumnInfo(name = "is_playing") var is_playing: String
)