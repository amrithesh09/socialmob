package com.example.sample.socialmob.repository.playlist

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sample.socialmob.repository.model.PlayListDataClass

/**
 * Reference
 * https://medium.com/@tonia.tkachuk/android-app-example-using-room-database-63f7091e69af
 * Database module
 */
@Dao
interface CreateDbPlayList {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCreateDbPlayList(list: List<PlayListDataClass>)

    @Query("DELETE FROM playListDataClass")
    fun deleteAllCreateDbPlayList()

    @Query("SELECT * FROM playListDataClass")
    fun getAllCreateDbPlayList(): LiveData<List<PlayListDataClass>>

    @Query("UPDATE playListDataClass SET Favorite=:isFavourite WHERE mExtraId=:id")
    fun setFavourite(id: String, isFavourite: String)

}