package com.example.sample.socialmob.repository.remote

import com.example.sample.socialmob.model.article.*
import com.example.sample.socialmob.model.chat.ConversationListResponse
import com.example.sample.socialmob.model.chat.FailedFiles
import com.example.sample.socialmob.model.dash_board.FullScreenResponse
import com.example.sample.socialmob.model.feed.*
import com.example.sample.socialmob.model.login.*
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.*
import com.example.sample.socialmob.model.search.*
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.backlog.articles.AddCommentResponseModel
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.chat.model.PreSignedPayLoad
import com.example.sample.socialmob.view.utils.DeviceInfo.device.DeviceDataInfo
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.article.*
import com.example.sample.socialmob.viewmodel.feed.MessageListResponse
import com.example.sample.socialmob.viewmodel.login.*
import com.example.sample.socialmob.viewmodel.music_menu.*
import com.example.sample.socialmob.viewmodel.profile.*
import com.example.sample.socialmob.viewmodel.search.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface BackEndApi {

    // TODO : Login API
    @FormUrlEncoded
    @POST("/api/v1/profile/login")
    suspend fun loginApi(
        @Header("platform") platform: String,
        @Field("Email") username: String,
        @Field("Password") password: String
    ): Response<LoginResponse>

    @POST("/api/v4/profile/register")
    suspend fun mobEnter(
        @Header("platform") platform: String,
        @Body profileData: ProfileRegisterData
    ): Response<LoginResponse>

    @POST("/api/v1/profile/referralcode/verify")
    fun refferalCodeVerify(
        @Header("platform") platform: String,
        @Body code: RefferalCode
    ): Call<RefferalResponseModel>

    @POST("api/v1/profile/login/sociallogin")
    suspend fun socialLogin(
        @Header("platform") platform: String,
        @Body profileData: ProfileSocialRegisterData
    ): Response<LoginResponse>

    @POST("/api/v4/profile/update")
    suspend fun updateProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Body editProfileBody: EditProfileBody
    ): Response<UpdateProfileResponseModel>

    @GET("/api/v2/availability/email/{email}")
    suspend fun emailAvailable(
        @Path("email") email: String
    ): Response<EmailAvailableResponseModel>

    @FormUrlEncoded
    @POST("/api/v1/profile/password/forgot")
    suspend fun forgotPassword(
        @Header("platform") platform: String,
        @Field("email") email: String
    ): Response<ForgotPasswordResponseModel>

    @GET("/api/v2/profile/register/avatar")
    suspend fun getImagePreDesignedUrl(
    ): Response<ProfilePicResponseModel>

    @GET("api/file/v1/upload")
    fun preSignedUrlChat(
        @Header("Authorization") authorization: String,
        @Query("image") image: String,
        @Query("video") video: String
    ): Call<PreSignedPayLoad>

    // TODO : Genre Track API
    @GET("/api/v3/genre/all")
    suspend fun getGenre(
        @Header("Authorization") authorization: String, @Header("platform") platform: String
    ): Response<GenreResponse>

    // TODO : Podcast API
    @GET("/api/v2/podcast/list")
    suspend fun getPodCastApi(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("offset") mOffset: String,
        @Query("count") mCount: String,
        @Query("sortBy") sortBy: String
    ): Response<PodCastResponse>


    // TODO : Radio API
    @GET("/api/v1/radio/station/all")
    suspend fun getRadio(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<RadioResponseModel>


    // TODO Play playList API
    @GET("/api/v1/music/playlist/list")
    suspend fun getPlayList(
        @Header("Authorization") authorization: String
    ): Response<PlayListResponseModel>

    // TODO : Artist Video List
    @GET("/api/v1/trackvideo/artist/list")
    suspend fun getVideoList(
        @Header("Authorization") authorization: String,
        @Query("artist") artistId: String
    ): Response<ArtistVideoList>

    // TODO : Artist Video List
    @GET("api/v1/trackvideo/{artistId}/detail")
    suspend fun getSimilarVideo(
        @Header("Authorization") authorization: String,
        @Path("artistId") artistId: String
    ): Response<SimilarVideos>

    // TODO Play playList API
    @GET("/api/v1/profile/{profileId}/playlists")
    suspend fun getUserPlayList(
        @Header("Authorization") authorization: String,
        @Path("profileId") profileId: String
    ): Response<PlayListSearchResponse>

    // TODO Create Play playList API
    @POST("/api/v2/music/playlist")
    suspend fun createPlayList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Body playListBody: CreatePlayListBody
    ): Response<CreatePlayListResponseModel>

    // TODO Delete Play playList API
    @DELETE("/api/v2/music/playlist/{playlistId}")
    suspend fun deletePlayList(
        @Header("Authorization") authorization: String,
        @Path("playlistId") mPlayListId: String
    ): Response<PlayListResponseModel>

    // TODO Play playList Track API
    @GET("/api/v2/music/playlist/{playlistId}/tracks")
    suspend fun getPlayListTrack(
        @Header("Authorization") authorization: String,
        @Path("playlistId") mPlayListId: String
    ): Response<SinglePlayListResponseModel>

    // TODO Ad Track to Play playList API
    @PUT("/api/v2/music/playlist/{playlistId}/{trackId}")
    suspend fun addTrackToPlayList(
        @Header("Authorization") authorization: String,
        @Path("playlistId") mPlayListId: String,
        @Path("trackId") mTrackId: String
    ): Response<AddPlayListResponseModel>

    // TODO Ad Track to Play playList API
    @DELETE("/api/v2/music/playlist/{playlistId}/{trackId}")
    suspend fun deleteTrackToPlayList(
        @Header("Authorization") authorization: String,
        @Path("playlistId") mPlayListId: String,
        @Path("trackId") mTrackId: String
    ): Response<DeleteTrackResponseModel>


//    @FormUrlEncoded
//    @POST("/api/v1/music/playlist/{PlaylistId}/{PlaylistName}")
//    fun createPlayList(
//        @Header("Authorization") authorization: String,
//        @Path("PlaylistId") mPlaylistId: String,
//        @Path("PlaylistName") mPlaylistName: String
//
//        ): Call<FeaturedTrackModel>

    // TODO : Genre List API
    @GET("/api/v2/track/genre/{genreId}/{offset}/{count}")
    suspend fun getGenreTrackList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("genreId") genreId: String,
        @Path("offset") mOffset: String,
        @Path("count") mCOunt: String
    ): Response<GenreListTrackModel>

    @GET("/api/v1/track/station/{genreId}/{count}")
    suspend fun getStationTrackList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("genreId") genreId: String,
        @Path("count") mCOunt: String
    ): Response<GenreListTrackModel>

  @GET("/api/v1/trackvideo/list")
    suspend fun getMusicVideo(
      @Header("Authorization") authorization: String,
      @Header("platform") platform: String,
      @Query("offset") offset: String,
      @Query("count") count: String
    ): Response<MusicVideoList>

    @GET("/api/v1/album/{albumId}/detail")
    suspend fun getAlbumTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("albumId") genreId: String
    ): Response<GenreListTrackModel>

    // TODO : Podcast Episode List API
    @GET("/api/v3/podcast/{PodcastId}/episode/list")
    suspend fun getPodcastEpisodeList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("PodcastId") genreId: String,
        @Query("offset") mOffset: String,
        @Query("count") mCOunt: String,
        @Query("sort") mAscDesc: String
    ): Response<RadioPodCastListModel>


    // TODO : DashBoard API
    @GET("/api/v2/advertisement/fullscreen")
    fun getDashBoardApi(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Call<FullScreenResponse>

    // TODO :Search Profile API
    @GET("/api/v2/search/profile")
    suspend fun searchProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("offset") mOffset: String,
        @Query("count") mCount: String,
        @Query("searchText") mString: String
    ): Response<SearchProfileResponseModel>


    // TODO :Search Article API
    @GET("/api/v2/search/content/{offset}/{count}")
    fun searchArticle(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("offset") mOffset: String,
        @Path("count") mCount: String,
        @Query("searchText") mString: String
    ): Call<SearchArticleResponseModel>

    // TODO :Search Hash Tag API
    @GET("/api/v1/hashtag/suggestion")
    suspend fun searchHashTags(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("offset") mOffset: String,
        @Query("count") mCount: String,
        @Query("searchText") mString: String
    ): Response<SearchHashTagResponseModel>

    // TODO :Search  Tag API
    @GET("/api/v2/search/tag/{offset}/{count}")
    fun searchTags(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("offset") mOffset: String,
        @Path("count") mCount: String,
        @Query("searchText") mString: String
    ): Call<SearchTagResponseModel>

    // TODO :Article Dash board API
    @GET("/api/v2/advertisement/halfscreen")
    fun getHalfScreenDAshBoard(
        @Header("Authorization") authorization: String
//        @Path("slotNumber") slotNumber: String
    ): Call<ArticleDashBoardResponseModel>


    // TODO : History Article API
    @GET("/api/v1/content/userread/{platformId}/{offset}/{count}")
    fun getHistoryArticle(
        @Header("Authorization") mAuth: String,
        @Header("platform") mPlatform: String,
        @Path("platformId") mPlatformId: String,
        @Path("offset") mOffset: String,
        @Path("count") mCount: String
    ): Call<HistoryArticleResponseModel>

    // TODO : Bookmarks Article API
    @GET("/api/v1/content/bookmarks/{platformId}/{offset}/{count}")
    fun getBookMarkedArticle(
        @Header("Authorization") mAuth: String,
        @Header("platform") mPlatform: String,
        @Path("platformId") mPlatformId: String,
        @Path("offset") mOffset: String,
        @Path("count") mCount: String
    ): Call<BookMarkedArticleResponseModel>

    // TODO : Recommended Article API
    @GET("/api/v2/content/foryou/{offset}/{count}")
    fun getRecommendedArticle(
        @Header("Authorization") mAuth: String,
        @Header("platform") mPlatform: String,
        @Path("offset") mOffset: String,
        @Path("count") mCount: String
    ): Call<ForYouArticleResponseModel>

    // TODO : Follow Profile API
    @PUT("/api/v1/profile/{profileId}/follow")
    suspend fun followProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<FollowResponseModel>

    // TODO : Un Follow Profile API
    @DELETE("/api/v1/profile/{profileId}/follow")
    suspend fun unFollowProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<FollowResponseModel>

    @DELETE("/api/v1/profile/{profileId}/follow/request")
    suspend fun cancelFollowUser(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<FollowResponseModel>

    // TODO : Follow Tag API
    @PUT("/api/v1/tag/{tagId}/follow")
    fun followTag(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("tagId") profileId: String
    ): Call<FollowResponseModel>

    // TODO : Un Follow Profile API
    @DELETE("/api/v1/tag/{tagId}/follow")
    fun unFollowTag(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("tagId") profileId: String
    ): Call<FollowResponseModel>

    // TODO : Add To Bookmark API
    @PUT("/api/v1/content/{contentId}/bookmark")
    fun addToBookmark(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") profileId: String
    ): Call<FollowResponseModel>

    // TODO : Remove To Bookmark API
    @DELETE("/api/v1/content/{contentId}/bookmark")
    fun removeBookmark(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") profileId: String
    ): Call<FollowResponseModel>

    @GET("/api/v1/content/{contentId}/detail")
    fun getArticleDetails(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") contentId: String
    ): Call<SingleArticleApiModel>

    @GET("/api/v1/content/{contentId}/comment/{offset}/{count}")
    fun getCommentData(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") contentId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Call<ArticleCommentResponseModel>

    @POST("/api/v2/content/{contentId}/comment")
    fun postComment(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") contentId: String,
        @Body list: CommentPostData
    ): Call<AddCommentResponseModel>

    @PUT("/api/v1/content/comment/{contentId}")
    fun updateComment(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") contentId: String,
        @Body list: CommentPostData
    ): Call<Any>

    @DELETE("/api/v2/content/{contentId}/comment/{commentId}")
    fun deleteArticleComment(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("contentId") contentId: String,
        @Path("commentId") commentId: String
    ): Call<Any>

    @GET(RemoteConstant.topItemsMusic)
    suspend fun searchTopItemsMusic(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<TopItemsMusic>

    @GET(RemoteConstant.topItemsNetworking)
    suspend fun searchTopNetwork(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<TopItemsNetworkResponse>

    @GET("/api/v1/search/playlist")
    suspend fun searchPlayList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("playlist") mString: String,
        @Query("offset") mOffset: String,
        @Query("count") mCount: String
    ): Response<PlayListSearchResponse>

    @GET("/api/v1/search/artist")
    suspend fun searchArtist(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("artist") mString: String,
        @Query("offset") mOffset: String,
        @Query("count") mCount: String
    ): Response<ArtistSearchResponse>

    @GET("api/v1/artist/list/{lastId}/{count}")
    suspend fun getAllArtist(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("lastId") offset: String,
        @Path("count") count: String
    ): Response<ArtistSearchResponse>

    // TODO :Search Track API
    @GET("/api/v2/search/track")
    suspend fun searchTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("track") mString: String,
        @Query("offset") mOffset: String,
        @Query("count") mCount: String
    ): Response<SearchTrackResponseModel>

    @GET("/api/v3/profile/{profileId}")
    suspend fun getUserProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<UserProfileResponseModel>

    @GET("/api/v3/profile/{profileId}")
    suspend fun getMyProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<LoginResponse>


    @GET("/api/v2/profile/{profileId}/follower/{lastRecordid}/{count}")
    suspend fun getFollowersList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String,
        @Path("lastRecordid") offset: String,
        @Path("count") count: String
//        @Path("lastRecordid") mLastRecordId: String
    ): Response<FollowersResponseModel>

    @GET("/api/v2/profile/{profileId}/following/{offset}/{count}")
    suspend fun getFollowingList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<FollowingResponseModel>

    @GET("/api/v2/track/{trackId}/detail")
  suspend  fun getTrackDetails(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("trackId") trackId: String
    ): Response<TrackDetailsModel>

    //    api/v1/podcast/episode/:episodeId
    @GET("api/v2/podcast/episode/{episodeId}")
   suspend fun getPodCastDetails(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("episodeId") trackId: String
    ): Response<PodcastResponseModel>


    @GET("/api/v5/feed/personal/{profileId}/{lastRecordid}/{count}")
    suspend fun getPersonalFeed(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String,
        @Path("lastRecordid") offset: String,
        @Path("count") count: String
    ): Response<PersonalFeedResponseModel>

    @GET("/api/v7/feed/timeline/{lastRecordId}/{count}")
    suspend fun getTimeLineFeed(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("lastRecordId") offset: String,
        @Path("count") count: String
    ): Response<PersonalFeedResponseModel>

    @PUT("/api/v1/post/{postId}/like")
    suspend fun likePost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String
    ): Response<Any>

    @DELETE("/api/v1/post/{postId}/like")
    suspend fun disLikePost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String
    ): Response<Any>

    @GET("/api/v2/post/{postId}/comment/{offset}/{count}")
    suspend fun getFeedComments(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<CommentResponseModel>

    @GET("/api/v1/post/like/list/{postId}/{offset}/{count}")
    suspend fun getLikeListPost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") albumId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<LikeListResponseModel>

    @POST("/api/v2/post/{postId}/comment")
    suspend fun postCommentPost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String,
        @Body postFeedData: CommentPostData
    ): Response<Any>

    @PUT("/api/v1/profile/{profileId}/block")
    suspend fun blockUser(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<Any>

    @DELETE("/api/v1/profile/{profileId}/block")
    suspend fun unblockUser(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<Any>

    @GET("/api/v1/profile/blocked/{offset}/{count}")
    suspend fun getBlockedUsers(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<BlockedUsersResponseModel>


    @GET("/api/v2/post/presignedurl/{type}/{count}")
    fun getPreSignedUrl(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("type") type: String,
        @Path("count") count: String
    ): Call<ImagePreSignedUrlResponseModel>

    @PUT
    fun uploadImageProgressAmazon(
        @Url baseUrl: String,
        @Body file: RequestBody
    ): Call<ResponseBody>

    @PUT
    suspend fun uploadImageProgressAmazonSuspend(
        @Url baseUrl: String,
        @Body file: RequestBody
    ): Response<ResponseBody>


    @POST("/api/v2/post/new")
    suspend fun createFeedPost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Body postFeedData: PostFeedData
    ): Response<UploadFeedResponseModel>

    @PUT("/api/v2/post/{postId}")
    suspend fun updateFeedPost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String,
        @Body postFeedData: PostFeedData
    ): Response<UploadFeedResponseModel>

    @DELETE("/api/v1/post/{postId}/comment/{commentId}")
    suspend fun deleteCommentPost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String,
        @Path("commentId") commentId: String
    ): Response<CommentDeleteResponseModel>

    @GET("/api/v3/feed/mediagrid/{offset}/{count}")
    suspend fun getPhotoGridmediaGrid(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<PersonalFeedResponseModel>

    @GET("/api/v2/feed/gallery/{profileId}/{offset}/{count}")
    suspend fun getMediaGrid(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<PersonalFeedResponseModel>

    @GET("/api/v4/hashtag/feed/{tagId}/{feedType}/{offset}/{count}")
    suspend fun getMediaGridCampaign(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("tagId") tagId: String,
        @Path("feedType") feedType: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<PersonalFeedResponseModel>

    @GET("/api/v1/post/{postId}/detail")
    suspend fun getPostFeedData(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String
    ): Response<SingleFeedResponseModel>

    @GET("/api/v1/action/{actionId}")
    suspend fun getPostFeedDataAction(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("actionId") postId: String
    ): Response<SingleFeedResponseModel>

    @GET("/api/v1/hashtag/suggestion")
    fun getHashTags(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("searchText") name: String,
        @Query("offset") offset: String,
        @Query("count") count: String
    ): Call<HashTagsResponseModel>

    @GET("/api/v1/profile/mention/list")
    suspend fun mentionProfile(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("name") name: String
    ): Response<MentionProfileResponseModel>

    @POST("/api/v2/utilities/urlscrapper")
    suspend fun getUrlScrapped(
        @Body list: ScrapperUrl
    ): Response<ScrapUrlResponseModel>

    @DELETE("/api/v1/post/{postId}")
    suspend fun deletePost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String
    ): Response<Any>

    @FormUrlEncoded
    @POST("/api/v1/pushnotification/register/token")
    suspend fun postDeviceToken(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Field("token") token: String
    ): Response<Any>

    @PUT("/api/v1/pushnotificaton/update/subscription")
    suspend fun onPushnotification(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<NotificationResponseModel>

    /**
     * Off pushnotification call.
     *
     * @param authorization the authorization
     * @param platform      the platform
     * @return the call [AddRemoveBookmarkResponse]
     */
    @DELETE("/api/v1/pushnotificaton/update/subscription")
    suspend fun offPushnotification(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<NotificationResponseModel>

    @GET("/api/v4/profile/notifications/{offset}/{count}")
    suspend fun getAllNotification(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<GetAllNotificationResponseModel>


    @GET("/api/conversation/v1/list")
    suspend fun getChatMessageList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<ConversationListResponse>

    @GET("/api/user/v1/suggestion")
    suspend fun getChatSuggestion(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<SearchProfileResponseModel>

    @GET("/api/conversation/v1/search")
    suspend fun searchChatUsers(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("search") search: String
    ): Response<ConversationListResponse>

    @DELETE("/api/conversation/v1/remove")
    suspend fun deleteConversation(
        @Header("Authorization") authorization: String,
        @Query("conversation") conversation: String
    ): Response<Any>

    @GET("/api/message/v1/list")
    suspend fun getRecentMessageList(
        @Header("Authorization") authorization: String,
        @Query("conversation") conv_id: String,
        @Query("offset") offset: String,
        @Query("count") count: String,
        @Query("app") app: String
    ): Response<MessageListResponse>

    //   @DELETE("/api/message/{messageid}")
    @DELETE("api/message/v1/remove")
    suspend fun deleteMessage(
        @Header("Authorization") authorization: String,
        @Query("message") messageid: String
    ): Response<Any>


    @PUT("api/conversation/v1/mark-read")
    suspend fun callApiMarkAllAsRead(
        @Header("Authorization") authorization: String,
        @Query("conversation") conversation: String
    ): Response<Any>

    @FormUrlEncoded
    @POST("/api/v1/profile/password/change")
    suspend fun changePassword(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Field("current_password") current_password: String,
        @Field("password") password: String,
        @Field("cpassword") cpassword: String,
        @Field("logoutAllDevice") logoutAllDevice: String
    ): Response<ChangePasswordResponseModel>

    @GET("/api/v1/tag/{platformId}/{tagId}/{offset}/{count}")
    fun getTagData(
        @Header("Authorization") authorization: String,
        @Path("platformId") platformId: String,
        @Path("tagId") tagId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Call<TagArticleResponseModel>

    @PUT("/api/v1/profile/privacy/update")
    suspend fun changeToPrivateAccount(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<NotificationResponseModel>


    @DELETE("/api/v1/profile/privacy/update")
    suspend fun changeToPublicAccount(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<NotificationResponseModel>

    @PUT("/api/v1/profile/notifications/{notificationId}/read")
    suspend fun readSingleNotification(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("notificationId") notificationId: String
    ): Response<NotificationResponseModel>

    @PUT("/api/v1/profile/{profileId}/follow/accept")
    suspend fun acceptRequest(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<NotificationResponseModel>

    /**
     * Reject request call.
     *
     * @param authorization the authorization
     * @param platform      the platform
     * @param profileId     the profile id
     * @return the call [FollowUnfollwUserResponse]
     */
    @DELETE("/api/v1/profile/{profileId}/follow/accept")
    suspend fun rejectRequest(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String
    ): Response<NotificationResponseModel>

    @GET("/api/v1/subtags/{mainTagId}/list/{offset}/{count}")
    suspend fun getSubHashTag(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("mainTagId") mainTagId: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<SubTagResponseModel>

    @POST("/api/v1/content/{platformId}/{contentId}/activity")
    fun postArticleAnalytics(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("platformId") platformId: String,
        @Path("contentId") contentId: String,
        @Body analyticsArticleBody: AnalyticsArticleBody
    ): Call<Any>

    @GET("/api/v4/hashtag/feed/{tagId}/{feedType}/{offset}/{count}")
    suspend fun getHashTagFeed(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("tagId") hashTagId: String,
        @Path("feedType") feedType: String,
        @Path("offset") offset: String,
        @Path("count") count: String
    ): Response<PersonalFeedResponseModel>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/api/v1/profile/logout", hasBody = true)
    suspend fun deleteUUID(
        @Field("appId") appId: String
    ): Response<Any>

    @FormUrlEncoded
    @PUT("/api/v1/profile/{profileId}/report")
    suspend fun reportUser(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("profileId") profileId: String,
        @Field("message") message: String
    ): Response<ForgotPasswordResponseModel>

    @FormUrlEncoded
    @POST("/api/v1/feedback")
    suspend fun postFeedBack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Field("source") source: String,
        @Field("subject") subject: String,
        @Field("message") message: String
    ): Response<Any>

    @POST("/api/v2/podcast/episode/analytics")
    suspend fun podcastAnalytcs(
        @Header("Authorization") authorization: String,
        @Body podcasData: PodCastAnalyticsData
    ): Response<Any>

    @FormUrlEncoded
    @PUT("/api/v1/post/{postId}/report")
    suspend fun reportPost(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("postId") postId: String,
        @Field("message") message: String
    ): Response<Any>

    @POST("/api/v2/track/analytics")
    suspend fun tarckAnalytcs(
        @Header("Authorization") authorization: String,
        @Body trackAnalyticsData: List<TrackAnalyticsDataList>
    ): Response<Any>

    // TODO : Don't change ( job will not working when activity is closed )
    @POST("/api/v1/trackvideo/{videoId}/analytics")
    suspend fun videoAnalytcs(
        @Header("Authorization") authorization: String,
        @Path("videoId") videoId: String,
        @Body mDurationModel: DurationModel
    ): Response<CommentDeleteResponseModel>


    @POST("/api/v1/device/info")
    suspend fun postDeviceInfo(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Body info: DeviceDataInfo
    ): Response<Any>

    @POST("/api/v2/device/info")
    suspend fun postDeviceInfoV1(
        @Header("req_origin") req_origin: String,
        @Header("platform") platform: String,
        @Body info: DeviceDataInfo
    ): Response<Any>

    @PUT("/api/v1/profile/notifications/read")
    suspend fun markAllAsRead(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<Any>


    @POST("/api/v2/track/analytics/lastplayed")
    suspend fun recentPlayedTrack(
        @Header("Authorization") authorization: String,
        @Query("trackId") trackId: String
    ): Response<Any>

    @GET("/api/v2/track/lastplayed/{offset}/{count}")
    suspend fun getRecentlyPlaytedTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("offset") mOffset: String,
        @Path("count") mCount: String
    ): Response<RecentlyPlayedTrackModel>


    /**
     * Add favourite track call.
     *
     * @param authorization the authorization
     * @param platform      the platform
     * @param trackId       the track id
     * @return the call [AddRemoveBookmarkResponse]
     */
    @PUT("/api/v2/track/{trackId}/favorite")
    suspend fun addFavouriteTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("trackId") trackId: String
    ): Response<Any>

    /**
     * Remove favourite track call.
     *
     * @param authorization the authorization
     * @param platform      the platform
     * @param trackId       the track id
     * @return the call [AddRemoveBookmarkResponse]
     */
    @DELETE("/api/v2/track/{trackId}/favorite")
    suspend fun removeFavouriteTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("trackId") trackId: String
    ): Response<Any>

    @GET("/api/v2/track/favorite/list")
    suspend fun getFavouriteTracks(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("offset") offset: String,
        @Query("count") count: String
    ): Response<FavouriteTrackModel>

    @GET("/api/v1/music/playlist/all")
    suspend fun playListAll(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("offset") offset: String,
        @Query("count") count: String
    ): Response<PlayListSearchResponse>

    @GET("/api/v2/availability/username/{username}")
    suspend fun checkUserNameAvailable(
        @Header("Authorization") authorization: String,
        @Path("username") username: String
    ): Response<UserNameAvailableResponse>

    @GET("/api/v1/music/playlist/icondata")
    suspend fun callApiPlayListImages(
        @Header("Authorization") authorization: String
    ): Response<PlayListImages>

    @GET("/api/v2/artist/{artistId}/detail")
    suspend fun getArtistDetails(
        @Header("Authorization") authorization: String,
        @Path("artistId") artistId: String
    ): Response<ArtistDetails>

    @GET("/api/v1/artist/{artistId}/toptracks/{offset}/{count}")
    suspend fun artistAllTracks(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("artistId") artistId: String,
        @Path("offset") mOffset: String,
        @Path("count") mCOunt: String
    ): Response<GenreListTrackModel>

    @GET("/api/v3/music/metadata")
    suspend fun getMetaData(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<MusicMetaResponse>

    @GET("/api/v1/music/podcastmeta")
    suspend fun podcastMeta(
//        @Header("ise") ise: String,
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<PodcastMeta>

    @GET("/api/v1/track/{trackId}/comment/list")
    suspend fun getTrackComments(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("trackId") genreId: String,
        @Query("offset") mOffset: String,
        @Query("count") mCOunt: String
    ): Response<TrackCommentModel>

    @GET("/api/v1/podcast/comment")
    suspend fun getPodcastComments(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("target") target: String,
        @Query("source") genreId: String,
        @Query("offset") mOffset: String,
        @Query("count") mCOunt: String
    ): Response<TrackCommentModel>

    @GET("/api/v1/podcast/comment")
    suspend fun getPodCastEpisodeComments(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("target") target: String,
        @Query("source") genreId: String,
        @Query("offset") mOffset: String,
        @Query("count") mCOunt: String
    ): Response<TrackCommentModel>

    @POST("/api/v1/track/{trackId}/comment")
    suspend fun postCommentTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("trackId") postId: String,
        @Body postTrackComment: CommentPostDataTrack
    ): Response<TrackAddCommentResponseModel>

    @POST("/api/v1/podcast/comment")
    suspend fun postCommentPodcast(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("target") target: String,
        @Query("source") genreId: String,
        @Body postTrackComment: CommentPostDataTrack
    ): Response<TrackAddCommentResponseModel>

    @DELETE("/api/v1/track/{trackId}/comment/{commentId}")
    suspend fun deleteCommentTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("trackId") postId: String,
        @Path("commentId") commentId: String
    ): Response<Any>

    @DELETE("/api/v1/podcast/comment")
    suspend fun deletePodcastComment(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Query("target") target: String,
        @Query("source") source: String,
        @Query("comment") comment: String
    ): Response<Any>

    @POST("/api/v2/track/offline/verify")
    suspend fun checkDeletedTrack(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Body mTrackIdList: TrackIdList?
    ): Response<DeleteOfflineTrackResponseModel>

    @PUT("/api/file/v1/failed")
    suspend fun updateFailedFiles(
        @Header("Authorization") authorization: String,
        @Body mFilesList: FailedFiles,
        @Query("app") app: String
    ): Response<Any>


   @POST("/api/v2/contactlist/upload")
  suspend fun upLoadContacts(
       @Header("Authorization") authorization: String,
       @Body mFilesList: HomeActivity.ContactModelList,
    ): Response<Any>


    // TODO : Follow artistId API
    @PUT("/api/v1/artist/{artistId}/follow")
    suspend fun followArtist(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("artistId") profileId: String
    ): Response<FollowResponseModel>

    // TODO : Un Follow artistId API
    @DELETE("/api/v1/artist/{artistId}/follow")
    suspend fun unFollowArtist(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("artistId") profileId: String
    ): Response<FollowResponseModel>

    //
    @PUT("/api/v1/trackvideo/{videoId}/like")
    suspend fun addFavouriteVideo(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("videoId") trackId: String
    ): Response<Any>

    @DELETE("/api/v1/trackvideo/{videoId}/like")
    suspend fun removeFavouriteVideo(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("videoId") trackId: String
    ): Response<Any>

    @GET("/api/v1/trackvideo/{videoId}/comment/list")
    suspend fun getVideoComments(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("videoId") videoId: String,
        @Query("lastCommentId") lastCommentId: String,
        @Query("count") count: String
    ): Response<CommentResponseModel>

    @PUT("/api/v1/trackvideo/{videoId}/comment")
    suspend fun postCommentVideo(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("videoId") contentId: String,
        @Body list: CommentPostDataVideo
    ): Response<AddCommentResponseVideoModel>

    @DELETE("/api/v1/trackvideo/{videoId}/comment/{commentId}")
    suspend fun deleteVideoComment(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Path("videoId") contentId: String,
        @Path("commentId") commentId: String
    ): Response<Any>

    @FormUrlEncoded
    @POST("/api/v1/profile/mob-space-category")
    suspend fun setMobSpaceCategory(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String,
        @Field("_id") _Id: String,
        @Field("name") name: String
    ): Response<MobCategoryFeedModel>
}

