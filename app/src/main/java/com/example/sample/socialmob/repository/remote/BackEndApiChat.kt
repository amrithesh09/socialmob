package com.example.sample.socialmob.repository.remote

import com.example.sample.socialmob.model.chat.ConversationListResponse
import com.example.sample.socialmob.model.chat.FailedFiles
import com.example.sample.socialmob.model.search.SearchProfileResponseModel
import com.example.sample.socialmob.view.ui.home_module.chat.model.PreSignedPayLoad
import com.example.sample.socialmob.viewmodel.feed.MessageListResponse
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface BackEndApiChat {
    // TODO : Login API

    @GET("api/file/v1/upload")
    fun preSignedUrlChat(
        @Header("Authorization") authorization: String,
        @Query("image") image: String,
        @Query("video") video: String
    ): Call<PreSignedPayLoad>

    @GET("/api/conversation/v1/list")
    suspend fun getChatMessageList(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<ConversationListResponse>

    @GET("/api/user/v1/suggestion")
    suspend fun getChatSuggestion(
        @Header("Authorization") authorization: String,
        @Header("platform") platform: String
    ): Response<SearchProfileResponseModel>

    @DELETE("/api/conversation/v1/remove")
    suspend fun deleteConversation(
        @Header("Authorization") authorization: String,
        @Query("conversation") conversation: String
    ): Response<Any>

    @GET("/api/message/v1/list")
    suspend fun getRecentMessageList(
        @Header("Authorization") authorization: String,
        @Query("conversation") conv_id: String,
        @Query("offset") offset: String,
        @Query("count") count: String,
        @Query("app") app: String
    ): Response<MessageListResponse>

    //   @DELETE("/api/message/{messageid}")
    @DELETE("api/message/v1/remove")
    suspend fun deleteMessage(
        @Header("Authorization") authorization: String,
        @Query("message") messageid: String
    ): Response<Any>


    @PUT("api/conversation/v1/mark-read")
    suspend fun callApiMarkAllAsRead(
        @Header("Authorization") authorization: String,
        @Query("conversation") conversation: String
    ): Response<Any>

    @PUT("/api/file/v1/failed")
    suspend fun updateFailedFiles(
        @Header("Authorization") authorization: String,
        @Body mFilesList: FailedFiles,
        @Query("app") app: String
    ): Response<Any>

    @PUT
    fun uploadImageProgressAmazon(
        @Url baseUrl: String,
        @Body file: RequestBody
    ): Call<ResponseBody>
}