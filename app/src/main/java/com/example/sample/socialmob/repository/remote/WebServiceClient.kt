package com.example.sample.socialmob.repository.remote

import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.MyApplication
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.ConnectionPool
import okhttp3.Protocol


object WebServiceClient {
    private lateinit var interceptor: HttpLoggingInterceptor
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var okHttpClientSingle: OkHttpClient
    private var retrofitSingle: Retrofit? = null
    private var retrofit: Retrofit? = null
    private var retrofitChat: Retrofit? = null


    private const val cacheSize = (5 * 1024 * 1024).toLong()
    private val myCache = Cache(MyApplication.application?.cacheDir!!, cacheSize)

    val client: Retrofit
        get() {
            interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
//            interceptor.level = HttpLoggingInterceptor.Level.NONE
            okHttpClient = OkHttpClient.Builder()
                //TODO : Enable Log
                .addInterceptor(interceptor)
//                    .connectionSpecs(Arrays.asList(
//                            ConnectionSpec.MODERN_TLS,
//                            ConnectionSpec.COMPATIBLE_TLS))
//                    .followRedirects(true)
//                    .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    var request = chain.request()
                    request = request.newBuilder()
                        .build()
                    val response = chain.proceed(request)
                    response }
                .connectionPool(ConnectionPool(0, 1, TimeUnit.MINUTES))
                .protocols(listOf(Protocol.HTTP_1_1))
                .cache(myCache)
                .build()
            okHttpClient.dispatcher.maxRequestsPerHost = 1

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(RemoteConstant.mBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
            }
            return retrofit!!

        }
    val clientSingle: Retrofit
        get() {
//            interceptor = HttpLoggingInterceptor()
//                     HttpLoggingInterceptor.Level.BODY

           interceptor = HttpLoggingInterceptor()
                     HttpLoggingInterceptor.Level.NONE

            okHttpClientSingle = OkHttpClient.Builder()
                //TODO : Enable Log
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    var request = chain.request()
                    request = request.newBuilder()
                        .build()
                    val response = chain.proceed(request)
                    response }
                .retryOnConnectionFailure(true)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .connectionPool(ConnectionPool(0, 1, TimeUnit.MINUTES))
                .protocols(listOf(Protocol.HTTP_1_1))
                .cache(myCache)
                .build()
            okHttpClientSingle.dispatcher.maxRequestsPerHost = 1
            if (retrofitSingle == null) {
                retrofitSingle = Retrofit.Builder()
                    .baseUrl(RemoteConstant.mBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClientSingle)
                    .build()
            }
            return retrofitSingle!!

        }
    val clientChat: Retrofit
        get() {
            interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
//            interceptor.level = HttpLoggingInterceptor.Level.NONE
//            interceptor.level = HttpLoggingInterceptor.Level.BASIC
            okHttpClient = OkHttpClient.Builder()
                //TODO : Enable Log
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    var request = chain.request()
                    request = request.newBuilder()
                        .build()
                    val response = chain.proceed(request)
                    response }
//                    .connectionSpecs(Arrays.asList(
//                            ConnectionSpec.MODERN_TLS,
//                            ConnectionSpec.COMPATIBLE_TLS))
//                    .followRedirects(true)
//                    .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .connectionPool(ConnectionPool(0, 1, TimeUnit.MINUTES))
                .protocols(listOf(Protocol.HTTP_1_1))
//                .cache(null)
                .build()

            if (retrofitChat == null) {
                retrofitChat = Retrofit.Builder()
                    .baseUrl(RemoteConstant.mBaseUrlChat)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
            }
            return retrofitChat!!

        }


}
