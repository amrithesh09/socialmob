package com.example.sample.socialmob.repository.repo

import com.example.sample.socialmob.model.chat.ConversationListResponse
import com.example.sample.socialmob.model.chat.FailedFiles
import com.example.sample.socialmob.model.search.SearchProfileResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApiChat
import com.example.sample.socialmob.repository.utils.ProgressBodyImage
import com.example.sample.socialmob.view.ui.home_module.chat.model.PreSignedPayLoad
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.viewmodel.feed.MessageListResponse
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class ChatRepository @Inject constructor(private val backEndApi: BackEndApiChat) {
    fun uploadImageProgressAmazonVideo(
        mVideoUrl: String,
        mRequestBodyVideo: ProgressRequestBody
    ): Call<ResponseBody> {
        return backEndApi.uploadImageProgressAmazon(
            mVideoUrl, mRequestBodyVideo
        )
    }

    fun uploadImageProgressAmazon(
        url: String,
        mRequestBodyImage: ProgressBodyImage
    ): Call<ResponseBody> {
        return backEndApi.uploadImageProgressAmazon(
            url, mRequestBodyImage
        )
    }

    fun uploadImageProgressAmazonThumbnail(
        mThumbFileUrl: String,
        mRequestBodyImageThumbnail: RequestBody
    ): Call<ResponseBody> {
        return backEndApi.uploadImageProgressAmazon(
            mThumbFileUrl,
            mRequestBodyImageThumbnail
        )
    }

    suspend fun getRecentMessageList(
        nJwt: String, convId: String, mOffset: String, mCount: String, stringPreference: String
    ): Response<MessageListResponse> {
        return backEndApi.getRecentMessageList(
            nJwt, convId, mOffset, mCount, stringPreference
        )
    }

    suspend fun deleteMessage(nJwt: String, msgId: String): Response<Any> {
        return backEndApi.deleteMessage(nJwt, msgId)
    }

    suspend fun callApiMarkAllAsRead(nJwt: String, convId: String): Response<Any> {
        return backEndApi.callApiMarkAllAsRead(nJwt, convId)
    }

    suspend fun getChatSuggestion(
        mAuthPenny: String, mPlatform: String
    ): Response<SearchProfileResponseModel> {
        return backEndApi.getChatSuggestion(mAuthPenny, mPlatform)
    }

    suspend fun getChatMessageList(
        mAuthPenny: String, mPlatform: String
    ): Response<ConversationListResponse> {
        return backEndApi.getChatMessageList(mAuthPenny, mPlatform)
    }

    suspend fun deleteConversation(mJwt: String, convId: String): Response<Any> {
        return backEndApi.deleteConversation(mJwt, convId)
    }

    fun preSignedUrlChat(
        nJwt: String,
        imageSize: String,
        videoSize: String
    ): Call<PreSignedPayLoad> {
        return backEndApi.preSignedUrlChat(nJwt, imageSize, videoSize)
    }

    suspend fun updateFailedFiles(
        mJwt: String, mFilesList: FailedFiles, stringPreference: String
    ): Response<Any> {
        return backEndApi.updateFailedFiles(mJwt, mFilesList, stringPreference)
    }
}