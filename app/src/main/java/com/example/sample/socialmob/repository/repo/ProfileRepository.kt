package com.example.sample.socialmob.repository.repo

import com.example.sample.socialmob.model.feed.UploadFeedResponseModel
import com.example.sample.socialmob.model.login.ChangePasswordResponseModel
import com.example.sample.socialmob.model.login.ForgotPasswordResponseModel
import com.example.sample.socialmob.model.login.LoginResponse
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.*
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class ProfileRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getUserProfile(
        mAuth: String, mPlatform: String, mProfileId: String
    ): Flow<Response<UserProfileResponseModel>> {
        return flow { emit(backEndApi.getUserProfile(mAuth, mPlatform, mProfileId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getMyProfile(
        mAuth: String, mPlatform: String, mProfileId: String
    ): Flow<Response<LoginResponse>> {
        return flow { emit(backEndApi.getMyProfile(mAuth, mPlatform, mProfileId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getFollowersList(
        mAuth: String, mPlatform: String, mProfileId: String, mLastRecordId: String, mCount: String
    ): Flow<Response<FollowersResponseModel>> {
        return flow {
            emit(
                backEndApi.getFollowersList(mAuth, mPlatform, mProfileId, mLastRecordId, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getFollowingList(
        mAuth: String, mPlatform: String, mProfileId: String, mOffset: String, mCount: String
    ): Flow<Response<FollowingResponseModel>> {
        return flow {
            emit(
                backEndApi.getFollowingList(mAuth, mPlatform, mProfileId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    fun getTrackDetails(
        mAuth: String, mPlatform: String, profileAnthemTrackId: String
    ): Flow<Response<TrackDetailsModel>> {
        return flow {
            emit(
                backEndApi.getTrackDetails(mAuth, mPlatform, profileAnthemTrackId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPersonalFeed(
        mAuth: String, mPlatform: String, mProfileId: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(
                backEndApi.getPersonalFeed(mAuth, mPlatform, mProfileId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun likePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow { emit(backEndApi.likePost(mAuth, mPlatform, postId)) }.flowOn(Dispatchers.IO)
    }

    suspend fun blockUser(
        mAuth: String,
        mPlatform: String,
        mProfileId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(
                backEndApi.blockUser(mAuth, mPlatform, mProfileId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun unblockUser(
        mAuth: String,
        mPlatform: String,
        mProfileId: String
    ): Flow<Response<Any>> {
        return flow { emit(backEndApi.unblockUser(mAuth, mPlatform, mProfileId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun disLikePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(
                backEndApi.disLikePost(mAuth, mPlatform, postId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getBlockedUsers(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<BlockedUsersResponseModel>> {
        return flow { emit(backEndApi.getBlockedUsers(mAuth, mPlatform, mOffset, mCount)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun updateProfile(
        mAuth: String, mPlatform: String, editProfileBody: EditProfileBody
    ): Flow<Response<UpdateProfileResponseModel>> {
        return flow { emit(backEndApi.updateProfile(mAuth, mPlatform, editProfileBody)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun changePassword(
        mAuth: String, mPlatform: String, mOldPassword: String, mNewPassWord: String,
        mReenterNewPassWord: String, s: String
    ): Flow<Response<ChangePasswordResponseModel>> {
        return flow {
            emit(
                backEndApi.changePassword(
                    mAuth,
                    mPlatform,
                    mOldPassword,
                    mNewPassWord,
                    mReenterNewPassWord,
                    s
                )
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun cancelFollowUser(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(
                backEndApi.cancelFollowUser(mAuth, mPlatform, mId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(
                backEndApi.unFollowProfile(mAuth, mPlatform, mId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun followProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow { emit(backEndApi.followProfile(mAuth, mPlatform, mId)) }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePost(mAuth: String, mPlatform: String, mId: String): Flow<Response<Any>> {
        return flow { emit(backEndApi.deletePost(mAuth, mPlatform, mId)) }.flowOn(Dispatchers.IO)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow { emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow { emit(backEndApi.getPlayList(mAuth)) }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(
                backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun reportUser(
        mAuth: String, mPlatform: String, mProfileId: String, mMessage: String
    ): Flow<Response<ForgotPasswordResponseModel>> {
        return flow { emit(backEndApi.reportUser(mAuth, mPlatform, mProfileId, mMessage)) }.flowOn(
            Dispatchers.IO
        )
    }

    fun getPreSignedUrl(
        mAuth: String,
        mPlatform: String,
        mType: String,
        mCount: String
    ): Call<ImagePreSignedUrlResponseModel> {
        return backEndApi.getPreSignedUrl(mAuth, mPlatform, mType, mCount)
    }

    fun uploadImageProgressAmazon(mUrl: String, mRequestBody: RequestBody): Call<ResponseBody> {
        return backEndApi.uploadImageProgressAmazon(mUrl, mRequestBody)
    }

    suspend fun createFeedPost(
        mAuth: String, mPlatform: String, postData: PostFeedData
    ): Flow<Response<UploadFeedResponseModel>> {
        return flow { emit(backEndApi.createFeedPost(mAuth, mPlatform, postData)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getUserPlayList(
        mAuth: String,
        mId: String
    ): Flow<Response<PlayListSearchResponse>> {
        return flow { emit(backEndApi.getUserPlayList(mAuth, mId)) }.flowOn(Dispatchers.IO)
    }

    suspend fun getFavouriteTracks(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<FavouriteTrackModel>> {
        return flow {
            emit(
                backEndApi.getFavouriteTracks(mAuth, mPlatform, /*mProfileId,*/mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePlayList(mAuth: String, mId: String): Flow<Response<PlayListResponseModel>> {
        return flow { emit(backEndApi.deletePlayList(mAuth, mId)) }.flowOn(Dispatchers.IO)
    }

    fun getPodCastDetails(
        mAuth: String,
        mPlatform: String,
        mSongId: String
    ): Flow<Response<PodcastResponseModel>> {
        return flow { emit(backEndApi.getPodCastDetails(mAuth, mPlatform, mSongId)) }.flowOn(
            Dispatchers.IO
        )
    }
}