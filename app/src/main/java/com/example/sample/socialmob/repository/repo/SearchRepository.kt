package com.example.sample.socialmob.repository.repo

import com.example.sample.socialmob.model.feed.TopItemsNetworkResponse
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.search.*
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class SearchRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun followProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow { emit(backEndApi.followProfile(mAuth, mPlatform, mId)) }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.unFollowProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    fun followTag(mAuth: String, mPlatform: String, mId: String): Call<FollowResponseModel> {
        return backEndApi.followTag(mAuth, mPlatform, mId)
    }

    fun unFollowTag(mAuth: String, mPlatform: String, mId: String): Call<FollowResponseModel> {
        return backEndApi.unFollowTag(mAuth, mPlatform, mId)
    }

    fun addToBookmark(mAuth: String, mPlatform: String, mId: String): Call<FollowResponseModel> {
        return backEndApi.addToBookmark(mAuth, mPlatform, mId)
    }

    fun removeBookmark(mAuth: String, mPlatform: String, mId: String): Call<FollowResponseModel> {
        return backEndApi.removeBookmark(mAuth, mPlatform, mId)
    }

    suspend fun searchProfile(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String, mString: String
    ): Flow<Response<SearchProfileResponseModel>> {
        return flow {
            emit(backEndApi.searchProfile(mAuth, mPlatform, mOffset, mCount, mString))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun searchTrack(
        mAuth: String, mPlatform: String, mString: String, mOffset: String, mCount: String
    ): Flow<Response<SearchTrackResponseModel>> {
        return flow {
            emit(
                backEndApi.searchTrack(mAuth, mPlatform, mString, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    fun searchArticle(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String, mString: String
    ): Call<SearchArticleResponseModel> {
        return backEndApi.searchArticle(mAuth, mPlatform, mOffset, mCount, mString)
    }

    suspend fun searchHashTags(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String, mString: String
    ): Flow<Response<SearchHashTagResponseModel>> {
        return flow {
            emit(backEndApi.searchHashTags(mAuth, mPlatform, mOffset, mCount, mString))
        }.flowOn(Dispatchers.IO)
    }

    fun searchTags(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String, mString: String
    ): Call<SearchTagResponseModel> {
        return backEndApi.searchTags(mAuth, mPlatform, mOffset, mCount, mString)
    }

    suspend fun cancelFollowUser(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.cancelFollowUser(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow { emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow { emit(backEndApi.getPlayList(mAuth)) }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun searchTopItemsMusic(
        mAuth: String,
        mPlatform: String
    ): Flow<Response<TopItemsMusic>> {
        return flow {
            emit(backEndApi.searchTopItemsMusic(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun searchTopNetwork(
        mAuth: String,
        mPlatform: String
    ): Flow<Response<TopItemsNetworkResponse>> {
        return flow { emit(backEndApi.searchTopNetwork(mAuth, mPlatform)) }.flowOn(Dispatchers.IO)
    }

    suspend fun searchPlayList(
        mAuth: String, mPlatform: String, mSearchWord: String, mOffset: String, mCount: String
    ): Flow<Response<PlayListSearchResponse>> {
        return flow {
            emit(
                backEndApi.searchPlayList(mAuth, mPlatform, mSearchWord, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun searchArtist(
        mAuth: String, mPlatform: String, mSearchWord: String, mOffset: String, mCount: String
    ): Flow<Response<ArtistSearchResponse>> {
        return flow {
            emit(
                backEndApi.searchArtist(mAuth, mPlatform, mSearchWord, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addFavouriteTrack(
        mAuth: String, mPlatform: String, mTrackId: String
    ): Flow<Response<Any>> {
        return flow { emit(backEndApi.addFavouriteTrack(mAuth, mPlatform, mTrackId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun removeFavouriteTrack(
        mAuth: String, mPlatform: String, mTrackId: String
    ): Flow<Response<Any>> {
        return flow { emit(backEndApi.removeFavouriteTrack(mAuth, mPlatform, mTrackId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getAllArtist(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<ArtistSearchResponse>> {
        return flow { emit(backEndApi.getAllArtist(mAuth, mPlatform, mOffset, mCount)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun playListAll(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<PlayListSearchResponse>> {
        return flow { emit(backEndApi.playListAll(mAuth, mPlatform, mOffset, mCount)) }.flowOn(
            Dispatchers.IO
        )
    }
}