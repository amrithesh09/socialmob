package com.example.sample.socialmob.repository.repo

import com.example.sample.socialmob.model.profile.NotificationResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class SettingRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun onPushNotification(
        mAuth: String,
        mPlatform: String
    ): Flow<Response<NotificationResponseModel>> {
        return flow {
            emit(backEndApi.onPushnotification(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun offPushNotification(
        mAuth: String,
        mPlatform: String
    ): Flow<Response<NotificationResponseModel>> {
        return flow {
            emit(backEndApi.offPushnotification(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun changeToPublicAccount(
        mAuth: String, mPlatform: String
    ): Flow<Response<NotificationResponseModel>> {
        return flow {
            emit(backEndApi.changeToPublicAccount(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun changeToPrivateAccount(
        mAuth: String,
        mPlatform: String
    ): Flow<Response<NotificationResponseModel>> {
        return flow {
            emit(backEndApi.changeToPrivateAccount(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }
}