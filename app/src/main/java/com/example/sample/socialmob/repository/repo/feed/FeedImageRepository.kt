package com.example.sample.socialmob.repository.repo.feed

import com.example.sample.socialmob.model.article.CommentPostData
import com.example.sample.socialmob.model.feed.CommentDeleteResponseModel
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModel
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject

/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class FeedImageRepository @Inject constructor(private var backEndApi: BackEndApi) {

    suspend fun getPostFeedData(
        mAuth: String, mPlatform: String, mPostId: String
    ): Flow<Response<SingleFeedResponseModel>> {
        return flow {
            emit(backEndApi.getPostFeedData(mAuth, mPlatform, mPostId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPostFeedDataAction(
        mAuth: String, mPlatform: String, mPostId: String
    ): Flow<Response<SingleFeedResponseModel>> {
        return flow {
            emit(backEndApi.getPostFeedDataAction(mAuth, mPlatform, mPostId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun likePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.likePost(mAuth, mPlatform, postId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun disLikePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.disLikePost(mAuth, mPlatform, postId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deleteCommentPost(
        mAuth: String, mPlatform: String, mPostId: String, mId: String
    ): Flow<Response<CommentDeleteResponseModel>> {
        return flow {
            emit(backEndApi.deleteCommentPost(mAuth, mPlatform, mPostId, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePost(mAuth: String, mPlatform: String, mId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.deletePost(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getFeedComments(
        mAuth: String, mPlatform: String, mId: String, mOffset: String, mCount: String
    ): Flow<Response<CommentResponseModel>> {
        return flow {
            emit(backEndApi.getFeedComments(mAuth, mPlatform, mId, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun mentionProfile(
        mAuth: String, mPlatform: String, mMention: String
    ): Flow<Response<MentionProfileResponseModel>> {
        return flow { emit(backEndApi.mentionProfile(mAuth, mPlatform, mMention)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun postCommentPost(
        mAuth: String, mPlatform: String, mPostId: String, commentPostData: CommentPostData
    ): Flow<Response<Any>> {
        return flow {
            emit(
                backEndApi.postCommentPost(mAuth, mPlatform, mPostId, commentPostData)
            )
        }.flowOn(Dispatchers.IO)
    }
}