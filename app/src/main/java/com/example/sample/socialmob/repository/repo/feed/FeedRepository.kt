package com.example.sample.socialmob.repository.repo.feed

import com.example.sample.socialmob.model.feed.MobCategoryFeedModel
import com.example.sample.socialmob.model.feed.UploadFeedResponseModel
import com.example.sample.socialmob.model.music.AddPlayListResponseModel
import com.example.sample.socialmob.model.music.CreatePlayListResponseModel
import com.example.sample.socialmob.model.music.PlayListResponseModel
import com.example.sample.socialmob.model.profile.ImagePreSignedUrlResponseModel
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModel
import com.example.sample.socialmob.model.profile.PostFeedData
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class FeedRepository @Inject constructor(private var backEndApi: BackEndApi) {
    suspend fun getTimeLineFeed(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(backEndApi.getTimeLineFeed(mAuth, mPlatform, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun likePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.likePost(mAuth, mPlatform, postId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun disLikePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.disLikePost(mAuth, mPlatform, postId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePost(mAuth: String, mPlatform: String, mId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.deletePost(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Response<PlayListResponseModel> {
        return backEndApi.getPlayList(mAuth)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow {
            emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    fun getPreSignedUrl(mAuth: String, mPlatform: String, mType: String, mCount: String):
            Call<ImagePreSignedUrlResponseModel> {
        return backEndApi.getPreSignedUrl(mAuth, mPlatform, mType, mCount)
    }

    fun uploadImageProgressAmazon(mUrl: String, mRequestBody: RequestBody): Call<ResponseBody> {
        return backEndApi.uploadImageProgressAmazon(mUrl, mRequestBody)
    }

    suspend fun createFeedPost(
        mAuth: String, mPlatform: String, postData: PostFeedData
    ): Flow<Response<UploadFeedResponseModel>> {
        return flow {
            emit(backEndApi.createFeedPost(mAuth, mPlatform, postData))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun followProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.followProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.unFollowProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun cancelFollowUser(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.cancelFollowUser(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun setMobSpaceCategory(
        mAuth: String, mPlatform: String, _id: String, name:String
    ): Flow<Response<MobCategoryFeedModel>> {
        return flow {
            emit(backEndApi.setMobSpaceCategory(mAuth, mPlatform, _id, name))
        }.flowOn(Dispatchers.IO)
    }

}