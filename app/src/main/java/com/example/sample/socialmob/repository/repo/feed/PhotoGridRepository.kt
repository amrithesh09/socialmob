package com.example.sample.socialmob.repository.repo.feed

import com.example.sample.socialmob.model.music.AddPlayListResponseModel
import com.example.sample.socialmob.model.music.CreatePlayListResponseModel
import com.example.sample.socialmob.model.music.PlayListResponseModel
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class PhotoGridRepository @Inject constructor(private var backEndApi: BackEndApi) {
    suspend fun getPhotoGridMediaGrid(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(backEndApi.getPhotoGridmediaGrid(mAuth, mPlatform, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMediaGridCampaign(
        mAuth: String, mPlatform: String, tagId: String, feedType: String, mOffset: String,
        mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(
                backEndApi.getMediaGridCampaign(
                    mAuth, mPlatform, tagId, feedType, mOffset, mCount
                )
            )
        }.flowOn(Dispatchers.IO)

    }

    suspend fun getMediaGrid(
        mAuth: String, mPlatform: String, mProfileId: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(backEndApi.getMediaGrid(mAuth, mPlatform, mProfileId, mOffset, mCount))
        }.flowOn(Dispatchers.IO)

    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow {
            emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayList(mAuth))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePost(mAuth: String, mPlatform: String, mId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.deletePost(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun disLikePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.disLikePost(mAuth, mPlatform, postId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun likePost(mAuth: String, mPlatform: String, postId: String): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.likePost(mAuth, mPlatform, postId))
        }.flowOn(Dispatchers.IO)
    }
}