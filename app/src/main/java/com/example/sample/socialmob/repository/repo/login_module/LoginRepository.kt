package com.example.sample.socialmob.repository.repo.login_module

import com.example.sample.socialmob.model.login.ForgotPasswordResponseModel
import com.example.sample.socialmob.model.login.LoginResponse
import com.example.sample.socialmob.model.login.RefferalCode
import com.example.sample.socialmob.model.login.RefferalResponseModel
import com.example.sample.socialmob.model.profile.ProfileSocialRegisterData
import com.example.sample.socialmob.repository.remote.BackEndApi
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class LoginRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun loginApi(
        mPlatform: String, mEmail: String, mPassword: String
    ): Response<LoginResponse> {
        return backEndApi.loginApi(mPlatform, mEmail, mPassword)
    }

    fun referralCodeVerify(
        mPlatform: String, codeModel: RefferalCode
    ): Call<RefferalResponseModel> {
        return backEndApi.refferalCodeVerify(mPlatform, codeModel)
    }

    suspend fun socialLogin(
        mPlatform: String, mSocialData: ProfileSocialRegisterData
    ): Response<LoginResponse> {

        return backEndApi.socialLogin(mPlatform, mSocialData)
    }

    suspend fun forgotPassword(mPlatform: String, mEmail: String): Response<ForgotPasswordResponseModel> {
        return backEndApi.forgotPassword(mPlatform, mEmail)
    }
}