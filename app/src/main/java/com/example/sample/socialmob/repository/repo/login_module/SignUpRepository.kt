package com.example.sample.socialmob.repository.repo.login_module

import com.example.sample.socialmob.model.login.EmailAvailableResponseModel
import com.example.sample.socialmob.model.login.LoginResponse
import com.example.sample.socialmob.model.profile.ProfileRegisterData
import com.example.sample.socialmob.repository.remote.BackEndApi
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class SignUpRepository @Inject constructor(private var backEndApi: BackEndApi) {
    suspend fun emailAvailable(mEmail: String): Response<EmailAvailableResponseModel> {
        return backEndApi.emailAvailable(mEmail)
    }

    suspend fun mobEnter(mPlatform: String, profileData: ProfileRegisterData): Response<LoginResponse> {
        return backEndApi.mobEnter(mPlatform, profileData)
    }
}