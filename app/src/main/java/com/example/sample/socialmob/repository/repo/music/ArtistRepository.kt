package com.example.sample.socialmob.repository.repo.music

import com.example.sample.socialmob.model.feed.CommentDeleteResponseModel
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.CommentResponseModel
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class ArtistRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getArtistDetails(
        mAuth: String, mPlayListId: String
    ): Flow<Response<ArtistDetails>> {
        return flow {
            emit(backEndApi.getArtistDetails(mAuth, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun artistAllTracks(
        mAuth: String, mPlatform: String, mArtistId: String, mOffset: String, mCount: String
    ): Flow<Response<GenreListTrackModel>> {
        return flow {
            emit(backEndApi.artistAllTracks(mAuth, mPlatform, mArtistId, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun removeFavouriteTrack(
        mAuth: String, mPlatform: String, mTrackId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.removeFavouriteTrack(mAuth, mPlatform, mTrackId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addFavouriteVideo(
        mAuth: String, mPlatform: String, mVideoId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.addFavouriteVideo(mAuth, mPlatform, mVideoId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun removeFavouriteVideo(
        mAuth: String, mPlatform: String, mVideoId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.removeFavouriteVideo(mAuth, mPlatform, mVideoId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addFavouriteTrack(
        mAuth: String, mPlatform: String, mTrackId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.addFavouriteTrack(mAuth, mPlatform, mTrackId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayList(mAuth))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow {
            emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun followArtist(
        mAuth: String, mPlatform: String, mArtistId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.followArtist(mAuth, mPlatform, mArtistId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowArtist(
        mAuth: String, mPlatform: String, mArtistId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.unFollowArtist(mAuth, mPlatform, mArtistId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getVideoList(mAuth: String, mArtistId: String): Flow<Response<ArtistVideoList>> {
        return flow {
            emit(backEndApi.getVideoList(mAuth, mArtistId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getSimilarVideo(mAuth: String, mArtistId: String): Flow<Response<SimilarVideos>>? {
        return flow {
            emit(backEndApi.getSimilarVideo(mAuth, mArtistId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getVideoComments(
        mAuth: String, mPlatform: String, mVideoId: String, mLastId: String, s: String
    ): Flow<Response<CommentResponseModel>> {
        return flow {
            emit(
                backEndApi.getVideoComments(
                    mAuth, mPlatform, mVideoId, mLastId, "10"
                )
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun postCommentVideo(
        mAuth: String, mPlatform: String, mVideoId: String, commentPostData: CommentPostDataVideo
    ): Flow<Response<AddCommentResponseVideoModel>> {
        return flow {
            emit(backEndApi.postCommentVideo(mAuth, mPlatform, mVideoId, commentPostData))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deleteVideoComment(
        mAuth: String, mPlatform: String, mArtistId: String, mId: String
    ) {
        backEndApi.deleteVideoComment(mAuth, mPlatform, mArtistId, mId)
    }

    suspend fun mentionProfile(
        mAuth: String, mPlatform: String, mMnention: String
    ): Flow<Response<MentionProfileResponseModel>> {
        return flow {
            emit(backEndApi.mentionProfile(mAuth, mPlatform, mMnention))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun videoAnalytcs(
        mAuth: String, mArtistId: String, mDuration: DurationModel
    ): Flow<Response<CommentDeleteResponseModel>> {
        return flow {
            emit(backEndApi.videoAnalytcs(mAuth, mArtistId, mDuration))
        }.flowOn(Dispatchers.IO)
    }

}