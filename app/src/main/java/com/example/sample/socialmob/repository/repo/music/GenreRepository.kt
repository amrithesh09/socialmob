package com.example.sample.socialmob.repository.repo.music

import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class GenreRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getAlbumTrack(mAuth: String, mPlatform: String, mGenreId: String):
            Flow<Response<GenreListTrackModel>> {
        return flow {
            emit(backEndApi.getAlbumTrack(mAuth, mPlatform, mGenreId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getStationTrackList(
        mAuth: String, mPlatform: String, mGenreId: String, s: String
    ): Flow<Response<GenreListTrackModel>> {
        return flow {
            emit(backEndApi.getStationTrackList(mAuth, mPlatform, mGenreId, s))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getGenreTrackList(
        mAuth: String, mPlatform: String, mGenreId: String, mOffset: String, mCount: String
    ): Flow<Response<GenreListTrackModel>> {
        return flow {
            emit(backEndApi.getGenreTrackList(mAuth, mPlatform, mGenreId, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPodCastEpisodeList(
        mAuth: String, mPlatform: String, id: String, mOffset: String, mCount: String,
        mAscDesc: String
    ): Flow<Response<RadioPodCastListModel>> {
        return flow {
            emit(backEndApi.getPodcastEpisodeList(
                    mAuth, mPlatform, id, mOffset, mCount, mAscDesc))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayListTrack(
        mAuth: String, id: String
    ): Flow<Response<SinglePlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayListTrack(mAuth, id))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow {
            emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun removeFavouriteTrack(
        mAuth: String, mPlatform: String, mTrackId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.removeFavouriteTrack(mAuth, mPlatform, mTrackId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deleteTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<DeleteTrackResponseModel>> {
        return flow {
            emit(backEndApi.deleteTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayList(mAuth))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addFavouriteTrack(mAuth: String, mPlatform: String, mTrackId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.addFavouriteTrack(mAuth, mPlatform, mTrackId))
        }.flowOn(Dispatchers.IO)

    }
}