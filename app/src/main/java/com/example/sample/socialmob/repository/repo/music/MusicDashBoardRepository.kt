package com.example.sample.socialmob.repository.repo.music

import android.content.Context
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject

/**
 * Well for the sake of the simplicity
 * we assume everything will go fine here hence no error handling!
 */
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class MusicDashBoardRepository @Inject constructor(
    private val backEndApi: BackEndApi,
    private val application: Context
) {

    suspend fun getMusicMetaData(
        mAuth: String, mPlatform: String
    ): Flow<Response<MusicMetaResponse>> {
        return flow {
            emit(backEndApi.getMetaData(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getGenre(
        mAuth: String, mPlatform: String
    ): Flow<ResultResponse<MutableList<Genre>>> {
        return flow {
            val response = backEndApi.getGenre(mAuth, mPlatform)
            when (response.code()) {
                200 -> {
                    val mResult =
                        ResultResponse.success(response.body()?.payload?.genres!!)
                    emit(mResult)
                }
                502, 522, 523, 500 -> {
                    emit(ResultResponse.error("", mutableListOf(Genre())))
                }
                else -> {
                    RemoteConstant.apiErrorDetails(
                        application, response.code(), "Genre Api"
                    )
                }
            }
        }.catch {
            emit(ResultResponse.error("", ArrayList()))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPodCastApi(
        mAuth: String,
        mPlatform: String,
        mOffset: String,
        mCount: String,
        mSortBy: String
    ): Flow<ResultResponse<MutableList<Podcast>>> {
        return flow {
            val mGetPodCastApi =
                backEndApi.getPodCastApi(mAuth, mPlatform, mOffset, mCount, mSortBy)
            when (mGetPodCastApi.code()) {
                200 -> {
                    if (mOffset == "0") {
                        if (mGetPodCastApi.body()?.payload?.podcasts?.size != 0) {
                            emit(
                                ResultResponse.success(
                                    mGetPodCastApi.body()?.payload?.podcasts!! as MutableList<Podcast>
                                )
                            )
                        } else {
                            emit(
                                ResultResponse.noData(
                                    mGetPodCastApi.body()?.payload?.podcasts!! as MutableList<Podcast>
                                )
                            )
                        }
                    } else {
                        if (mGetPodCastApi.body()?.payload?.podcasts?.size != 0) {
                            emit(
                                ResultResponse.paginatedList(
                                    mGetPodCastApi.body()?.payload?.podcasts!! as MutableList<Podcast>
                                )
                            )
                        } else {
                            emit(
                                ResultResponse.loadingCompletedWithEmpty(
                                    mGetPodCastApi.body()?.payload?.podcasts!! as MutableList<Podcast>
                                )
                            )
                        }
                    }
                }
                else -> RemoteConstant.apiErrorDetails(
                    application, mGetPodCastApi.code(), "PodCast Api"
                )
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayList(mAuth))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addTrackToPlayList(mAuth: String, mTrackId: String, mPlayListId: String)
            : Flow<Response<AddPlayListResponseModel>> {
        return flow {
            emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getRadio(mAuth: String, mPlatform: String): Flow<Response<RadioResponseModel>> {
        return flow {
            emit(backEndApi.getRadio(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)

    }

    suspend fun podCastMeta(mAuth: String, mPlatform: String): Flow<Response<PodcastMeta>> {
        return flow {
            emit(backEndApi.podcastMeta(mAuth, mPlatform))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePlayList(mAuth: String, mId: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.deletePlayList(mAuth, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getStationTrackList(
        mAuth: String, mPlatform: String, mGenreId: String, s: String
    ): Flow<Response<GenreListTrackModel>> {
        return flow {
            emit(backEndApi.getStationTrackList(mAuth, mPlatform, mGenreId, s))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMusicVideo(mAuth: String, mCount: String): Flow<Response<MusicVideoList>> {
        return flow {
            emit(
                backEndApi.getMusicVideo(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mCount,
                    RemoteConstant.mCount
                )
            )
        }.flowOn(Dispatchers.IO)
    }

}