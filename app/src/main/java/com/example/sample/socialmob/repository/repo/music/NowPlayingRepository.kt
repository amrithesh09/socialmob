package com.example.sample.socialmob.repository.repo.music

import com.example.sample.socialmob.model.login.LoginResponse
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject

/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class NowPlayingRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayList(mAuth))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun podcastAnalytcs(mAuth: String, mData: PodCastAnalyticsData): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.podcastAnalytcs(mAuth, mData))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow {
            emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun postDeviceToken(mAuth: String, mPlatform: String, token: String) {
        backEndApi.postDeviceToken(mAuth, mPlatform, token)
    }

    suspend fun getMyProfile(
        mAuth: String, mPlatform: String, mProfileId: String
    ): Flow<Response<LoginResponse>> {
        return flow {
            emit(backEndApi.getMyProfile(mAuth, mPlatform, mProfileId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPodCastDetails(
        mAuth: String, mPlatform: String, mSongId: String
    ): Flow<Response<PodcastResponseModel>> {
        return flow {
            emit(backEndApi.getPodCastDetails(mAuth, mPlatform, mSongId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun trackAnalytics(
        mAuth: String,
        mData: List<TrackAnalyticsDataList>
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.tarckAnalytcs(mAuth, mData))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun addFavouriteTrack(
        mAuth: String,
        mPlatform: String,
        mTrackId: String
    ): Flow<Response<Any>> {
        return flow {
            emit(backEndApi.addFavouriteTrack(mAuth, mPlatform, mTrackId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun removeFavouriteTrack(mAuth: String, mPlatform: String, mTrackId: String) {
        backEndApi.removeFavouriteTrack(mAuth, mPlatform, mTrackId)
    }

    suspend fun getTrackDetails(
        mAuth: String, mPlatform: String, profileAnthemTrackId: String
    ): Flow<Response<TrackDetailsModel>> {
        return flow {
            emit(backEndApi.getTrackDetails(mAuth, mPlatform, profileAnthemTrackId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun checkDeletedTrack(
        mAuth: String, mPlatform: String, mTrackIdList: TrackIdList?
    ): Flow<Response<DeleteOfflineTrackResponseModel>> {
        return flow {
            emit(backEndApi.checkDeletedTrack(mAuth, mPlatform, mTrackIdList))
        }.flowOn(Dispatchers.IO)
    }


}