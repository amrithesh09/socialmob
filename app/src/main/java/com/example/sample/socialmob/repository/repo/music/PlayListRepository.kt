package com.example.sample.socialmob.repository.repo.music

import com.example.sample.socialmob.model.music.CreatePlayListResponseModel
import com.example.sample.socialmob.model.music.PlayListImages
import com.example.sample.socialmob.model.music.PlayListResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class PlayListRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun callApiPlayListImages(mAuth: String): Flow<Response<PlayListImages>> {
        return flow {
            emit(backEndApi.callApiPlayListImages(mAuth))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePlayList(mAuth: String, mId: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.deletePlayList(mAuth, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuthNew: String): Flow<Response<PlayListResponseModel>> {
        return flow {
            emit(backEndApi.getPlayList(mAuthNew))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow {
            emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))
        }.flowOn(Dispatchers.IO)
    }
}