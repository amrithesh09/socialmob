package com.example.sample.socialmob.repository.repo.music


import com.example.sample.socialmob.model.article.CommentPostData
import com.example.sample.socialmob.model.feed.CommentDeleteResponseModel
import com.example.sample.socialmob.model.music.CommentPostDataTrack
import com.example.sample.socialmob.model.music.TrackAddCommentResponseModel
import com.example.sample.socialmob.model.music.TrackCommentModel
import com.example.sample.socialmob.model.profile.CommentResponseModel
import com.example.sample.socialmob.model.profile.LikeListResponseModel
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class PostLIkeCommentRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getTrackComments(
        mAuth: String, mPlatform: String, mId: String, mOffset: String, mCount: String
    ): Flow<Response<TrackCommentModel>> {
        return flow {
            emit(
                backEndApi.getTrackComments(mAuth, mPlatform, mId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPodCastComments(
        mAuth: String, mPlatform: String, target: String, mId: String,
        mOffset: String, mCount: String
    ): Flow<Response<TrackCommentModel>> {
        return flow {
            emit(
                backEndApi.getPodcastComments(mAuth, mPlatform, target, mId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPodCastEpisodeComments(
        mAuth: String, mPlatform: String, target: String, mId: String,
        mOffset: String, mCount: String
    ): Flow<Response<TrackCommentModel>> {
        return flow {
            emit(
                backEndApi.getPodCastEpisodeComments(mAuth, mPlatform, target, mId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getFeedComments(
        mAuth: String, mPlatform: String, mId: String, mOffset: String, mCount: String
    ): Flow<Response<CommentResponseModel>> {
        return flow {
            emit(
                backEndApi.getFeedComments(mAuth, mPlatform, mId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getLikeListPost(
        mAuth: String, mPlatform: String, mId: String, mOffset: String, mCount: String
    ): Flow<Response<LikeListResponseModel>> {
        return flow {
            emit(
                backEndApi.getLikeListPost(mAuth, mPlatform, mId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun postCommentTrack(
        mAuth: String, mPlatform: String, id: String, commentPostData: CommentPostDataTrack
    ): Flow<Response<TrackAddCommentResponseModel>> {
        return flow {
            emit(
                backEndApi.postCommentTrack(mAuth, mPlatform, id, commentPostData)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun postCommentPodCast(
        mAuth: String, mPlatform: String, target: String, id: String,
        commentPostData: CommentPostDataTrack
    ): Flow<Response<TrackAddCommentResponseModel>> {
        return flow {
            emit(
                backEndApi.postCommentPodcast(mAuth, mPlatform, target, id, commentPostData)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun postCommentPost(
        mAuth: String, mPlatform: String, mPostId: String, commentPostData: CommentPostData
    ): Flow<Response<Any>> {
        return flow {
            emit(
                backEndApi.postCommentPost(mAuth, mPlatform, mPostId, commentPostData)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun followProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow { emit(backEndApi.followProfile(mAuth, mPlatform, mId)) }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(
                backEndApi.unFollowProfile(mAuth, mPlatform, mId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deleteCommentPost(
        mAuth: String, mPlatform: String, mPostId: String, mId: String
    ): Flow<Response<CommentDeleteResponseModel>> {
        return flow { emit(backEndApi.deleteCommentPost(mAuth, mPlatform, mPostId, mId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun deleteCommentTrack(mAuth: String, mPlatform: String, mPostId: String, mId: String) {
        backEndApi.deleteCommentTrack(mAuth, mPlatform, mPostId, mId)
    }

    suspend fun deletePodCastComment(
        mAuth: String, mPlatform: String, mTarget: String, mPostId: String, mId: String
    ) {
        backEndApi.deletePodcastComment(mAuth, mPlatform, mTarget, mPostId, mId)
    }

    suspend fun cancelFollowUser(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(
                backEndApi.cancelFollowUser(mAuth, mPlatform, mId)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun mentionProfile(
        mAuth: String, mPlatform: String, mMnention: String
    ): Flow<Response<MentionProfileResponseModel>> {
        return flow { emit(backEndApi.mentionProfile(mAuth, mPlatform, mMnention)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun addFavouriteTrack(mAuth: String, mPlatform: String, mTrackId: String): Flow<Response<Any>> {
        return flow { emit(backEndApi.addFavouriteTrack(mAuth, mPlatform, mTrackId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun removeFavouriteTrack(mAuth: String, mPlatform: String, mTrackId: String): Flow<Response<Any>> {
        return flow { emit(backEndApi.removeFavouriteTrack(mAuth, mPlatform, mTrackId)) }.flowOn(
            Dispatchers.IO
        )
    }
}