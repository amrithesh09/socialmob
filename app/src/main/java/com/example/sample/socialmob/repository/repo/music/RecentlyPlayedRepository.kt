package com.example.sample.socialmob.repository.repo.music

import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class RecentlyPlayedRepository @Inject constructor(private val backEndApi: BackEndApi) {

    suspend fun getFavouriteTracks(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<FavouriteTrackModel>> {
        return flow { emit(backEndApi.getFavouriteTracks(mAuth, mPlatform, /*mProfileId,*/mOffset, mCount))}.flowOn(Dispatchers.IO)
    }

    suspend fun getRecentlyPlayedTrack(
        mAuth: String, mPlatform: String, mOffset: String, mCount50: String
    ): Flow<Response<RecentlyPlayedTrackModel>> {
        return flow { emit(backEndApi.getRecentlyPlaytedTrack(mAuth, mPlatform, mOffset, mCount50))}.flowOn(Dispatchers.IO)
    }

    suspend fun createPlayList(
        mAuth: String, mPlatform: String, createPlayListBody: CreatePlayListBody
    ): Flow<Response<CreatePlayListResponseModel>> {
        return flow { emit(backEndApi.createPlayList(mAuth, mPlatform, createPlayListBody))}.flowOn(Dispatchers.IO)
    }

    suspend fun getPlayList(mAuth: String): Flow<Response<PlayListResponseModel>> {
        return flow { emit(backEndApi.getPlayList(mAuth))}.flowOn(Dispatchers.IO)
    }

    suspend fun addFavouriteTrack(mAuth: String, mPlatform: String, mTrackId: String) {
        backEndApi.addFavouriteTrack(mAuth, mPlatform, mTrackId)
    }

    suspend fun removeFavouriteTrack(mAuth: String, mPlatform: String, mTrackId: String) {
        backEndApi.removeFavouriteTrack(mAuth, mPlatform, mTrackId)
    }

    suspend fun addTrackToPlayList(
        mAuth: String, mTrackId: String, mPlayListId: String
    ): Flow<Response<AddPlayListResponseModel>> {
        return flow { emit(backEndApi.addTrackToPlayList(mAuth, mTrackId, mPlayListId))}.flowOn(Dispatchers.IO)
    }
}