package com.example.sample.socialmob.repository.repo.notification

import com.example.sample.socialmob.model.profile.GetAllNotificationResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class NotificationRepository  @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getAllNotification(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ):Flow<Response<GetAllNotificationResponseModel>> {
        return flow {
            emit( backEndApi.getAllNotification(mAuth, mPlatform, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit( backEndApi.unFollowProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun followProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit( backEndApi.followProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun readSingleNotification(mAuth: String, mPlatform: String, mNotificationId: String) {
        backEndApi.readSingleNotification(mAuth, mPlatform, mNotificationId)
    }

    suspend fun acceptRequest(mAuth: String, mPlatform: String, mId: String) {
        backEndApi.acceptRequest(mAuth, mPlatform, mId)
    }

    suspend fun rejectRequest(mAuth: String, mPlatform: String, mId: String) {
        backEndApi.rejectRequest(mAuth, mPlatform, mId)
    }

    suspend fun cancelFollowUser(mAuth: String, mPlatform: String, mId: String) {
        backEndApi.cancelFollowUser(mAuth, mPlatform, mId)
    }

    suspend fun markAllAsRead(mAuth: String, mPlatform: String) {
        backEndApi.markAllAsRead(mAuth, mPlatform)
    }
}