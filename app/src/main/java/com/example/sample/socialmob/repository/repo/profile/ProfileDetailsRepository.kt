package com.example.sample.socialmob.repository.repo.profile

import com.example.sample.socialmob.model.music.PodcastResponseModel
import com.example.sample.socialmob.model.music.TrackDetailsModel
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.remote.BackEndApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class ProfileDetailsRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getPhotoGridMediaGrid(
        mAuth: String, mPlatform: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(backEndApi.getPhotoGridmediaGrid(mAuth, mPlatform, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMediaGrid(
        mAuth: String, mPlatform: String, mProfileId: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(backEndApi.getMediaGrid(mAuth, mPlatform, mProfileId, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deletePost(mAuth: String, mPlatform: String, mId: String): Response<Any> {
        return backEndApi.deletePost(mAuth, mPlatform, mId)
    }

    suspend fun likePost(mAuth: String, mPlatform: String, postId: String): Response<Any> {
        return backEndApi.likePost(mAuth, mPlatform, postId)
    }

    suspend fun disLikePost(mAuth: String, mPlatform: String, postId: String): Response<Any> {
        return backEndApi.disLikePost(mAuth, mPlatform, postId)
    }

    suspend fun getMediaGridCampaign(
        mAuth: String, mPlatform: String, tagId: String, feedType: String,
        mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(
                backEndApi.getMediaGridCampaign(
                    mAuth, mPlatform, tagId, feedType, mOffset, mCount
                )
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getHashTagFeed(
        mAuth: String, mPlatform: String, mTagId: String, s: String, mOffset: String, mCount: String
    ): Flow<Response<PersonalFeedResponseModel>> {
        return flow {
            emit(backEndApi.getHashTagFeed(mAuth, mPlatform, mTagId, s, mOffset, mCount))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun unFollowProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.unFollowProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun cancelFollowUser(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.cancelFollowUser(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun followProfile(
        mAuth: String, mPlatform: String, mId: String
    ): Flow<Response<FollowResponseModel>> {
        return flow {
            emit(backEndApi.followProfile(mAuth, mPlatform, mId))
        }.flowOn(Dispatchers.IO)
    }

    fun getTrackDetails(
        mAuth: String, mPlatform: String, profileAnthemTrackId: String
    ): Flow<Response<TrackDetailsModel>> {
        return flow {
            emit(backEndApi.getTrackDetails(mAuth, mPlatform, profileAnthemTrackId))
        }.flowOn(Dispatchers.IO)
    }

    fun getPodCastDetails(
        mAuth: String, mPlatform: String, mSongId: String
    ): Flow<Response<PodcastResponseModel>> {
        return flow {
            emit(backEndApi.getPodCastDetails(mAuth, mPlatform, mSongId))
        }.flowOn(Dispatchers.IO)
    }
}