package com.example.sample.socialmob.repository.repo.profile

import com.example.sample.socialmob.model.feed.ScrapperUrl
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.feed.UploadFeedResponseModel
import com.example.sample.socialmob.model.login.ProfilePicResponseModel
import com.example.sample.socialmob.model.profile.*
import com.example.sample.socialmob.repository.remote.BackEndApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
/**
 * Dagger hilt
 * https://rahul9650ray.medium.com/dagger-hilt-retrofit-coroutines-9e8af89500ab
 * Kotlin Flow
 * https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
 */
class ProfileUploadRepository @Inject constructor(private val backEndApi: BackEndApi) {
    suspend fun getImagePreDesignedUrl(): Flow<Response<ProfilePicResponseModel>> {
        return flow { emit(backEndApi.getImagePreDesignedUrl()) }.flowOn(Dispatchers.IO)
    }

    suspend fun uploadImageProgressAmazonSuspend(
        profilePicFile: String, mRequestBodyImage: RequestBody
    ): Flow<Response<ResponseBody>> {
        return flow {
            emit(
                backEndApi.uploadImageProgressAmazonSuspend(profilePicFile, mRequestBodyImage)
            )
        }.flowOn(Dispatchers.IO)
    }

    fun getHashTags(
        mAuth: String, mPlatform: String, mHashTag: String, mOffset: String, mCounttwenty: String
    ): Call<HashTagsResponseModel> {
        return backEndApi.getHashTags(mAuth, mPlatform, mHashTag, mOffset, mCounttwenty)
    }

    suspend fun mentionProfile(
        mAuth: String, mPlatform: String, mMnention: String
    ): Flow<Response<MentionProfileResponseModel>> {
        return flow { emit(backEndApi.mentionProfile(mAuth, mPlatform, mMnention)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getUrlScrapped(mUrl: ScrapperUrl): Response<ScrapUrlResponseModel> {
        return backEndApi.getUrlScrapped(mUrl)
    }

    suspend fun createFeedPost(
        mAuth: String, mPlatform: String, postData: PostFeedData
    ): Flow<Response<UploadFeedResponseModel>> {
        return flow { emit(backEndApi.createFeedPost(mAuth, mPlatform, postData)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun getSubHashTag(
        mAuth: String, mPlatform: String, mId: String, mOffset: String, mCount: String
    ): Flow<Response<SubTagResponseModel>> {
        return flow {
            emit(
                backEndApi.getSubHashTag(mAuth, mPlatform, mId, mOffset, mCount)
            )
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPostFeedData(
        mAuth: String, mPlatform: String, mPostId: String
    ): Flow<Response<SingleFeedResponseModel>> {
        return flow { emit(backEndApi.getPostFeedData(mAuth, mPlatform, mPostId)) }.flowOn(
            Dispatchers.IO
        )
    }

    suspend fun updateFeedPost(
        mAuth: String, mPlatform: String, mPostId: String, postData: PostFeedData
    ): Flow<Response<UploadFeedResponseModel>> {
        return flow { emit(backEndApi.updateFeedPost(mAuth, mPlatform, mPostId, postData)) }.flowOn(
            Dispatchers.IO
        )
    }
}