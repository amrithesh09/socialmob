package com.example.sample.socialmob.repository.utils

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.GifView

/**
 * Custom progress dialog
 */
class CustomProgressDialog(context: Context?) : Dialog(context!!) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_progress)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        val gifView1 = findViewById<GifView>(R.id.gif)
        gifView1.visibility = View.VISIBLE
        gifView1.play()
        gifView1.gifResource = R.drawable.loadergif
    }
}