package com.example.sample.socialmob.repository.utils;


import android.os.Handler;
import android.os.Looper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

/**
 * To display upload progress of files Retrofit
 */

public class ProgressBodyImage extends RequestBody {
    private final File mFile;
    private String mPath;
    private final UploadCallbacksImage mListener;
    private final MediaType content_type;
    private final String timestamp;

    private static final int DEFAULT_BUFFER_SIZE = 2048;

    public interface UploadCallbacksImage {
        void onProgressUpdateImage(int percentage, String timestamp);

        void onError();

        void onFinish();
    }


    public ProgressBodyImage(final File file, MediaType content_type, final UploadCallbacksImage listener, final String timestamp) {
        this.content_type = content_type;
        mFile = file;
        mListener = listener;
        this.timestamp = timestamp;
    }


    @Override
    public MediaType contentType() {
        return MediaType.parse("image/jpeg");
    }

    @Override
    public long contentLength() throws IOException {
        return mFile.length();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        FileInputStream in = new FileInputStream(mFile);
        long uploaded = 0;

        try {
            int read;
            Handler handler = new Handler(Looper.getMainLooper());
            while ((read = in.read(buffer)) != -1) {

                // update progress on UI thread
                handler.post(new ProgressUpdater(uploaded, fileLength));

                uploaded += read;
                sink.write(buffer, 0, read);
            }
        } finally {
            in.close();
        }
    }

    private class ProgressUpdater implements Runnable {
        private final long mUploaded;
        private final long mTotal;

        public ProgressUpdater(long uploaded, long total) {
            mUploaded = uploaded;
            mTotal = total;
        }

        @Override
        public void run() {
            mListener.onProgressUpdateImage((int) (100 * mUploaded / mTotal), timestamp);
        }
    }

}
