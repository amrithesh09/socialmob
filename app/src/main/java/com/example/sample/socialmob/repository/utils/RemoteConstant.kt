package com.example.sample.socialmob.repository.utils

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Base64
import android.util.DisplayMetrics
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.example.sample.socialmob.model.feed.MobSpaceCategories
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


object RemoteConstant {
    enum class Environment {
        PRODUCTION,
        DEVELOPMENT
    }

    var environment: Environment = Environment.DEVELOPMENT

    var mBaseUrl = ""
    var mBaseUrlChat = ""
    var mBaseUrlWebSocket = ""

    var mPlatform = "313a3336"
    var mPlatformId = "1"
    var pathQuestionmark = "?"
    var mSlash = "/"
    var postMethod = "post"
    var mGetMethod = "get"
    var putMethod = "put"
    var deleteMethod = "delete"
    var frameWorkName = "amx:"
    var mOffset = "0"
    var mCountFive = "15"
    var mCounttwenty = "20"
    var mCounttwentyFive = "25"
    var mCount = "15"
    var mCount50 = "50"
    val mFive: String = "5"
    val mCountTwleve: String = "12"
    val mCountTen: String = "10"

    const val mTrackId = "TRACK_ID"
    const val mTrackName = "TRACK_NAME"
    const val mTrackGenre = "TRACK_GENRE"
    const val mTrackArtist = "TRACK_ARTIST"
    const val mTrackImage = "TRACK_IMAGE"
    const val mTrackPlayCount = "TRACK_PLAY_COUNT"
    const val mIsFrom = "IS_FROM"
    const val mDuration = "DURATION"

    const val mNeedToFilter = "LIST_NEED_TO_FILTER"
    const val mExtraIndex = "EXTRA_INDEX"
    const val extraIndexImage = "EXTRA_INDEX_IMAGE"
    const val extraIndexIsFrom = "EXTRA_INDEX_IS_FROM"
    const val extraIsFromRadio = "EXTRA_IS_FROM_RADIO"
    const val NIGHT_MODE = "night_mode"
    const val NIGHT_MODE_CHANGED = "night_mode_changed"
    const val NIGHT_MODE_CHANGED_POSTION = "night_mode_changed_POSTION"
    const val SELECTEDPLAYLISTNAME = "mSelectedPlayListName"

    const val IS_PALYING = "IS_PALYING"
    const val SELECTED_POSITION = "SELECTED_POSITION"
    const val LATITUDE = "latitude"
    const val LONGITUDE = "longitude"
    const val IS_LOCATION_UPDATED = "location_updated"
    const val NEED_TO_SHOW_TOOL_TIP = "NEED_TO_SHOW_TOOL_TIP"
    const val PARCELABLE_KEY  = "parcelableKey"
    const val KEY_STORIES_LIST_DATA = "KEY_STORY_DATA"
    const val CONTACT_UPLOADED = "CONTACT_UPLOADED"


    // API END POINT
    var musicmetadata = "api/v3/music/metadata"
    var mAdvertmntData = "api/v2/advertisement/fullscreen"
    var pathFullscreen = "/fullscreen"
    var pathTrendingTrack = "api/v2/track/trending/"
    var pathTopTrack = "api/v2/track/toptracks/"
    var pathFeatured = "api/v2/track/featured/"
    var pathFeaturedNew = "api/v3/track/featured/dashboard/data"
    var pathPlayList = "api/v1/music/playlist/list"
    var pathGenre = "api/v3/genre/all"
    var pathGenreList = "api/v2/track/genre/"
    var pathTrack = "api/v1/track"
    var pathTrackV2 = "api/v2/track"
    var pathPodCast = "api/v2/podcast/list?"
    var pathPodCastTop = "api/v1/podcast/top/list"
    var pathRadio = "api/v1/radio/station/all"
    var pathPodCastRecommended = "api/v1/podcast/episode/recommended"
    var pathPodCastFeatured = "api/v1/podcast/featured/list"
    var pathPodCastMeta = "api/v1/music/podcastmeta"
    var pathPodCastEpisodeList = "api/v3/podcast/"
    var pathPodCastEpisodeDetails = "api/v2/podcast/episode"
    var pathEpisodeList = "episode/list/"
    var pathEpisodeListNew = "episode/list?"
    var pathDeletePlayListV1 = "api/v1/music/playlist"
    var pathDeletePlayList = "api/v2/music/playlist"
    var pathProfileSearch = "api/v2/search/profile"
    var pathTrackSearch = "api/v1/search/track"
    var pathContentSearch = "api/v2/search/content"
    var pathTagSearch = "api/v2/search/tag"

    //    var pathHashTagSearch = "api/v1/search/community"
    var mChangePassword = "/api/v1/profile/password/change"
    var pathHashTagSearch = "api/v1/hashtag/suggestion"
    var mPathPlayListTrackApi = "/tracks"
    var mPathContentData = "api/v1/content/foryou"
    var pathHistoryArticle = "api/v1/content/userread"
    var pathBookMarkedArticle = "api/v1/content/bookmarks"
    var pathRecommendedArticle = "api/v2/content/foryou"
    var profileData = "/api/v1/profile"
    var profileDataV2 = "/api/v2/profile"
    var profileDataV3 = "/api/v3/profile"
    var pathFollow = "/follow"
    var pathTag = "api/v1/tag"
    var pathContent = "api/v1/content"
    var pathBookmark = "/bookmark"
    var mPathAdvertisement = "/api/v2/advertisement"
    var mPathHalfscreen = "/halfscreen"
    var mPathDetails = "/detail"
    var mPathComment = "/comment"
    var mContentData = "api/v2/content/"
    var mContentDataV1 = "api/v1/content"
    var mPathContentDataV1Comment = "comment"
    var mFollowerProfile = "/follower"
    var mFollowingProfile = "/following"
    var mAnthem = "anthem"
    var mPersonalFeed = "api/v5/feed/personal"
    var mPersonalFeedV4 = "api/v5/feed/personal"
    var mTimeLineFeed = "api/v7/feed/timeline"
    var mPostPath = "/api/v2/post"
    var mPostPathV1 = "/api/v1/post"
    var mPathLike = "/like"
    var mPathBlock = "/block"
    var mPathBlocked = "/blocked"
    var mPresignedurl = "api/v2/post/presignedurl"
    var createFeedPost = "/api/v2/post/new"
    var mPostPathV2 = "/api/v2/post"
    var mLikeListPost = "/api/v1/post/like/list"
    var mFeedGallery = "api/v1/feed/gallery"
    var mFeedmediagrid = "api/v3/feed/mediagrid"
    var mActionPathV1 = "/api/v1/action"

    var mMediaGrid = "api/v2/feed/gallery"
    var mHashTagData = "/api/v1/hashtag/suggestion"
    var mMentionProfile = "/api/v1/profile/mention/list"
    var mPushnotificationRegister = "/api/v1/pushnotification/register/token"
    var mNotificationOnOffData = "/api/v1/pushnotificaton/update/subscription"
    var mNotificationData = "/api/v4/profile/notifications"
    var updateProfileData = "/api/v4/profile/update"
    var mPennyNotificationData = "/api/v1/profile/penny-notifications"
    var mPennyData = "/api/v1/penny/send"
    var mPennyDataUrl = "/api/v1/penny"
    var mPathReport = "/report"
    var feedbackData = "/api/v1/feedback"
    var postData = "/api/v1/post"
    var tagData = "/api/v1/tag"
    var mPathFollowCancel = "/follow/request"
    var mPrivacyOnOffData = "/api/v1/profile/privacy/update"
    var notificationData = "/api/v1/profile/notifications"
    var mPathRead = "/read"
    var pathAcceptRequest = "/follow/accept"
    var pathFollowCancel = "/follow/request"
    var pathMainHashTag = "api/v1/mainhashtag/list/"
    var pathSubHashTag = "api/v1/subtags/"
    var pathAnalytics = "/activity"
    var trackData = "/api/v2/track"
    var stattionTrackDataV1 = "/api/v1/track/station"
    var pathFavorite = "/favorite"
    var pathFavoriteList = "/favorite/list"
    const val topItemsNetworking: String = "/api/v2/search/topitems/networking"
    const val topItemsMusic: String = "/api/v2/search/topitems/music"
    const val mobSubCategory: String = "/api/v1/profile/mob-space-category"
    // SharedPreference

    var mRepeatStatus = "REPEAT_STATUS"
    var mShuffleStatus = "SHUFFLE_STATUS"
    val REPEAT_MODE_NONE = 0
    val REPEAT_MODE_ALL = 1
    val REPEAT_MODE_THIS = 2

    val mAppOpenCount = "mAppOpenCount"
    val mChatOpenCount = "mChatOpenCount"

    var mAppId = "APP_ID"
    var mJwt = "API_JWT"
    var mApiKey = "API_KEY"
    var mConnectedUser = "CONNECTED_USER"
    var mUserImage = "USER_IMAGE"
    var mProfileId = "USER_PROFILE_ID"
    var mScreenWidth = "SCREEN_WIDTH"
    var mSearchWord = "SERACH_WORD"
    var referrerUid = "REFERRERUID"
    var isFirstTime = "ISFIRSTTIME"
    var isFirstTimeInNowPlaying = "ISFIRSTTIMEINNOWPLAYING"
    var mEnabledPushNotification = "USER_ENABLED_NOTIFICATION"
    var mEnabledPrivateProfile = "USER_ENABLED_PRIVATE_PROFILE"
    var mUserName = "USER_NAME"
    val mState: String = "PLAY_STATE"
    var deviceInfoData = "/api/v1/device/info"
    val pathRentlyPlayed = "api/v2/track/lastplayed/"
    var pathPlayListImage = "api/v1/music/playlist/icondata"
    val mAlbumDetails = "api/v1/album"

    private var isOpenedVerisionObsolute: Boolean = false
    const val isTapTargetNowPlayingActivity: String = "isTapTargetNowPlayingActivity"
    const val isTapTargetPlayedHome: String = "isTapTargetPlayedHome"
    const val isTapTargetPlayedPenny: String = "isTapTargetPlayedPenny"
    var mNativeAds: MutableList<UnifiedNativeAd>? = ArrayList()
    var mMobSpaceCategories: MutableList<MobSpaceCategories>? = ArrayList()
    var displayMobSpaceIntro: Boolean = false


    const val COUNTER_TIME = 10L
    const val GAME_OVER_REWARD = 1


    /**
     * Get encrypted string string [ ].
     *
     * @param appId           the app id
     * @param apiKey          the api key
     * @param requestedUrl    the requested url
     * @param requestedMethod the requested method
     * @return the string [ ]
     */
    fun getEncryptedString(
        appId: String,
        apiKey: String,
        requestedUrl: String,
        requestedMethod: String
    ): Array<String?> {
        val encrptedOutput = arrayOfNulls<String>(3)
        try {
            encrptedOutput[1] =
                getTimeStamp().replace("[\\t\\n\\r]+".toRegex(), " ").replace(" ", "")
            if (encrptedOutput[1] == null || encrptedOutput[1] == "") {
                encrptedOutput[1] =
                    getNonce().replace("[\\t\\n\\r]+".toRegex(), " ").replace(" ", "")
            }
            encrptedOutput[0] = getNonce().replace("[\\t\\n\\r]+".toRegex(), " ").replace(
                " ",
                ""
            ) + encrptedOutput[1]
            encrptedOutput[2] = hashMac(
                appId + requestedMethod.toLowerCase() + requestedUrl.toLowerCase() + encrptedOutput[1] + encrptedOutput[0],
                apiKey
            ).replace(
                "[\\t\\n\\r]+".toRegex(), " "
            ).replace(" ", "")
        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }

        return encrptedOutput
    }

    /**
     * Gets time stamp.
     *
     * @return the time stamp
     */
    private fun getTimeStamp(): String {
        var diffInSec: Long = 0
        try {
            val dtStart = "1970-01-01  00:00:00"
            val timeZone = TimeZone.getTimeZone("UTC")
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            format.timeZone = timeZone
            val startDate = format.parse(dtStart)
            val c = Calendar.getInstance(timeZone)
            val formattedDate = format.format(c.time)
            val endDate = format.parse(formattedDate)
            val diffInMs = endDate.time - startDate.time
            diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs)
        } catch (ignored: ParseException) {
        }

        return Gson().toJson(diffInSec)
    }

    /**
     * Gets nonce.
     *
     * @return the nonce
     */
    private fun getNonce(): String {
        return UUID.randomUUID().toString()
    }

    /**
     * Hash mac string.
     *
     * @param text      the text
     * @param secretKey the secret key
     * @return the string
     */
    private fun hashMac(text: String, secretKey: String): String {
        var hash = ""
        try {
            val sha256_HMAC = Mac.getInstance("HmacSHA256")
            val secret_key = SecretKeySpec(Base64.decode(secretKey, Base64.DEFAULT), "HmacSHA256")
            sha256_HMAC.init(secret_key)
            hash = Base64.encodeToString(sha256_HMAC.doFinal(text.toByteArray()), Base64.DEFAULT)
        } catch (ignored: Exception) {
        }

        return hash
    }

    fun getImageUrlFromWidth(imageUrl: String?, width: Int): String {
        var width = width
        if (width == 0) {
            width = 500
        }
        var url = ""
        if (imageUrl != null && imageUrl != "") {
            url = imageUrl + "?imwidth=" + width.toString() + "&impolicy=resize"
        }
        return url
    }

    fun getImageUrlFromWidthLocal(imageUrl: String?, width: Int): String {
        var width = width
        if (width == 0) {
            width = 500
        }
        var url = ""
        if (imageUrl != null && imageUrl != "") {
            url = imageUrl + "?imwidth=" + width.toString() + "&impolicy=resize"
        }
        return url
    }

    fun getWidth(context: Context?): Int {
        return if (context != null) {
            val displayMetrics = DisplayMetrics()
            val windowManager: WindowManager =
                context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay?.getMetrics(displayMetrics)
            displayMetrics.widthPixels
        } else {
            400
        }
    }

    fun getAuthWithoutOffsetAndCount(mContext: Context, mPath: String): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash + mPath, mGetMethod
        )

        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthCommon(activity: Context?, mUrl: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + mUrl +
                    mOffset + mSlash + mCountFive,
            mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun apiError(code: Int) {

//        AppUtils.showCommonToast(mContext, "Error Code ----- > $code")

    }

    fun apiErrorDetails(mContext: Context?, code: Int, erroMethod: String) {
        System.out.print("$erroMethod + Error Code ----- > $code")
        doAsync {
            if (code == 403) {
                if (mContext != null && !isOpenedVerisionObsolute) {
                    isOpenedVerisionObsolute = true
                    val intent = Intent(mContext, VersionObsolete::class.java)
                    intent.putExtra("code", code.toString())
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    mContext.startActivity(intent)
                }
            } else if (code == 401) {
                if (mContext != null) {
                    val intent = Intent(mContext, VersionObsolete::class.java)
                    intent.putExtra("code", code.toString())
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    mContext.startActivity(intent)

                }

            } else if (code == 502) {
                if (mContext != null && !isOpenedVerisionObsolute) {
                    isOpenedVerisionObsolute = true
                    val intent = Intent(mContext, VersionObsolete::class.java)
                    intent.putExtra("code", code.toString())
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    mContext.startActivity(intent)

                }
            }
        }
    }

    fun getAuthGenreListApi(
        mContext: Context,
        jukeBoxCategoryId: String,
        mOffset: String,
        mCount: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash + pathGenreList + jukeBoxCategoryId + mSlash +
                    mOffset + mSlash + mCount, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthPodcastEpisode(
        mContext: Context,
        id: String,
        mOffset: String,
        mCount: String,
        mAscDesc: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash + pathPodCastEpisodeList + id + mSlash +
//                    RemoteConstant.pathEpisodeList + mOffset + RemoteConstant.mSlash + mCount + RemoteConstant.mSlash + mAscDesc,
                    pathEpisodeListNew + "offset=" + mOffset + "&count=" + mCount + "&sort=" + mAscDesc,
            mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthPlayList(mContext: Context): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    mSlash +
                    pathDeletePlayList, postMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext,
            mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthDeletePlayList(mContext: Context, mId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    pathDeletePlayList +
                    mSlash + mId, deleteMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext,
            mAppId,
            ""
        )!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthAlbumTrack(id: String, context: Context): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(context, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(context, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    mAlbumDetails + mSlash +
                    id + mPathDetails, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            context, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthPlayListTrack(id: String, context: Context): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(context, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(context, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    pathDeletePlayList + mSlash +
                    id + mPathPlayListTrackApi, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            context, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthStationTrack(id: String, context: Context): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(context, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(context, mApiKey, "")!!,
            mBaseUrl + stattionTrackDataV1 + mSlash + id + "/50", mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            context, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthDeleteTrack(mContext: Context, mPlayListId: String, mTrackId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash + pathDeletePlayList + mSlash +
                    mPlayListId + mSlash + mTrackId, deleteMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthSearchProfile(
        activity: Context,
        pathProfile: String,
        mString: String,
        mOffset: String,
        mCount: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathProfile + pathQuestionmark +
                    "offset=" + mOffset + "&count=" + mCount + "&" + "searchText=" + mString,
            mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthSearchHashTag(
        activity: Context,
        pathHashTagSearch: String,
        mString: String,
        mOffset: String,
        mCount: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    pathHashTagSearch +
                    pathQuestionmark +
                    "offset=" + mOffset + "&count=" + mCount + "&" + "searchText=" + mString,
            mGetMethod
        )

        return frameWorkName + SharedPrefsUtils.getStringPreference(activity, mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthArticleHistory(
        activity: androidx.fragment.app.FragmentActivity?,
        mOffset: String,
        mCount: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathHistoryArticle +
                    mSlash + mPlatformId + mSlash +
                    mOffset + mSlash + mCount, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthBookMarkedArticle(
        activity: androidx.fragment.app.FragmentActivity?,
        mOffset: String,
        mCount: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathBookMarkedArticle +
                    mSlash + mPlatformId + mSlash +
                    mOffset + mSlash + mCount, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthArticleRecommended(
        activity: androidx.fragment.app.FragmentActivity?, mOffset: String, mCount: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathRecommendedArticle +
                    mSlash +
                    mOffset + mSlash + mCount,
            mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthRemoveBookmark(
        activity: androidx.fragment.app.FragmentActivity?,
        mId: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathContent + mSlash + mId +
                    pathBookmark, deleteMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthAddBookmark(activity: androidx.fragment.app.FragmentActivity?, mId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathContent + mSlash + mId +
                    pathBookmark, putMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthHalfScreenDashBoard(activity: androidx.fragment.app.FragmentActivity?): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl +
                    mPathAdvertisement +
                    mPathHalfscreen, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthSingleArticle(activity: Context, mId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl + mSlash + pathContent + mSlash + mId +
                    mPathDetails, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthArticleComment(
        mContext: Context, contentId: String?
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    mContentData + contentId +
                    mPathComment, postMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthDeleteComment(mContext: Context, mId: String, mIdComment: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    mContentData + mIdComment + mSlash +
                    mPathContentDataV1Comment + mSlash +
                    mId, deleteMethod
        )
        return frameWorkName +
                SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthUpdateComment(mContext: Context, mId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    mContentDataV1 + mSlash +
                    mPathContentDataV1Comment + mSlash +
                    mId, putMethod
        )
        return frameWorkName +
                SharedPrefsUtils.getStringPreference(
                    mContext, mAppId, ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthUser(mContext: Context, mProfileId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    profileDataV3 + mSlash +
                    mProfileId, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext,
            mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthTrackDetails(mContext: Context, profileAnthemTrackId: String?): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash +
                    pathTrackV2 +
                    mSlash + profileAnthemTrackId +
                    mPathDetails, mGetMethod
        )
        return frameWorkName +
                SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthPersonalFeed(
        mContext: Context?,
        mId: String,
        mCount: String,
        mOffset: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl + mSlash + mPersonalFeedV4 + mSlash +
                    mId + mSlash
                    + mCount + mSlash + mOffset,
            mGetMethod
        )
        return frameWorkName +
                SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthDislikePost(
        activity: androidx.fragment.app.FragmentActivity?,
        postId: String
    ): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl +
                    mPostPathV1 +
                    mSlash + postId +
                    mPathLike, deleteMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            mAppId,
            ""
        )!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthLikePost(activity: androidx.fragment.app.FragmentActivity?, postId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, mApiKey, "")!!,
            mBaseUrl +
                    mPostPathV1 +
                    mSlash + postId +
                    mPathLike, putMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            mAppId,
            ""
        )!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthBlockUser(mContext: Context, mProfileId: String): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    profileData +
                    mSlash + mProfileId + mPathBlock,
            putMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId, ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthUnBlockUser(mContext: Context, mProfileId: String): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    profileData +
                    mSlash + mProfileId + mPathBlock,
            deleteMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId, ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthBlockedUsers(mContext: Context, size: Int): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    profileData +
                    mPathBlocked +
                    mSlash + size +
                    mSlash + mCount, mGetMethod
        )
        return frameWorkName +
                SharedPrefsUtils.getStringPreference(
                    mContext,
                    mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthCommentList(
        mContext: Context,
        mPostId: String,
        size: String,
        size1: Int
    ): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    mPostPathV2 +
                    mSlash + mPostId +
                    mPathComment +
                    mSlash + size +
                    mSlash + size1, mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthPostComment(mContext: Context, mPostId: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    mPostPathV2 +
                    mSlash + mPostId +
                    mPathComment, postMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthTrack(mContext: Context, mTrackId: String, size: String): String {
        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    mSlash + "api/v1/track/" + mTrackId + "/comment/list?offset=" + size + "&count=15",
            mGetMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
    }

    fun getAuthAddTackComment(mContext: Context, mTrackId: String): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(
                mContext,
                mApiKey,
                ""
            )!!, mBaseUrl +
                    mSlash + "api/v1/track/" + mTrackId + "/comment",
            postMethod
        )
        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun getAuthDeleteTrackComment(
        mContext: Context,
        mTrackId: String,
        mId: String
    ): String {

        val fullData = getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, mApiKey, "")!!,
            mBaseUrl +
                    "/api/v1/track/" + mTrackId + "/comment/" + mId, deleteMethod
        )

        return frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext, mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

    }

    fun clearGlideCache() {
        // This method must be called on the main thread.
        ClearGlideCacheAsyncTask().execute()
    }

    class ClearGlideCacheAsyncTask :
        AsyncTask<Void?, Void?, Boolean?>() {
        private var result = false
        override fun doInBackground(vararg params: Void?): Boolean {
            try {
                Glide.get(MyApplication.application!!).clearDiskCache()
                result = true
            } catch (e: java.lang.Exception) {
            }
            return result
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            if (result!!) {
            }
        }
    }
}
