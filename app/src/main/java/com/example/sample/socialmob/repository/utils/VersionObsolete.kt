package com.example.sample.socialmob.repository.utils

import android.app.ActivityManager
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.VersionObsoleteActivityBinding
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

/**
 * To handle webservice error conditions
 */
@AndroidEntryPoint
class VersionObsolete : BaseCommonActivity(), GoogleApiClient.OnConnectionFailedListener {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private var code = ""
    private var mGoogleApiClient: GoogleApiClient? = null
    private val profileViewModel: ProfileViewModel by viewModels()
    private var binding: VersionObsoleteActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding  = DataBindingUtil.setContentView(this, R.layout.version_obsolete_activity)

        val extras = intent.extras!!
        code = extras.getString("code", "")
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestScopes(Scope(Scopes.PLUS_ME), Scope(Scopes.PROFILE), Scope(Scopes.EMAIL))
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
        if (code != "") {
            when (code) {
                "401" -> {
                    // TODO : Error code 401 - Auth token error
                    signOut()
                }
                "502" -> {
                    // TODO : Error code 502 - Server request time out need to show server error page
                    binding?.serverErrorLinear?.visibility = View.VISIBLE
                    glideRequestManager.load(R.drawable.server_error).apply(RequestOptions().fitCenter())
                        .into(binding?.noDataImageView!!)
                    binding?.noInternetTextView?.text = getString(R.string.server_error)
                    binding?.noInternetSecTextView?.text =
                        getString(R.string.server_error_content)
                }
                "403" -> {
                    // TODO : Error code 403 - Version obsolete need to show version update dialog
                    showVersionDialog()
                }
            }
        }

        binding?.refreshTextView?.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(0, 0)
            this.finish()
        }
    }

    private fun signOut() {
        //TODO : To clear glide cache
        Glide.get(this).clearMemory()
        RemoteConstant.clearGlideCache()
        GlobalScope.launch {
            var response: Response<Any>? = null
            kotlin.runCatching {
                response = WebServiceClient.client.create(BackEndApi::class.java)
                    .deleteUUID(
                        SharedPrefsUtils.getStringPreference(
                            this@VersionObsolete,
                            RemoteConstant.mAppId,
                            ""
                        )!!
                    )
            }.onSuccess {
                when (response!!.code()) {
                    200 -> clearAll()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Log out api"
                    )
                }
            }.onFailure {
                clearAll()
            }
        }
    }

    private fun clearAll() {
        GlobalScope.launch(Dispatchers.Main) {
            if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                val serviceClass1 = MediaServiceRadio::class.java
                val intent11 = Intent(this@VersionObsolete, serviceClass1)
                stopService(intent11)
            }
            if (isMyServiceRunning(MediaService::class.java)) {
                val serviceClass1 = MediaService::class.java
                val intent11 = Intent(this@VersionObsolete, serviceClass1)
                stopService(intent11)
            }
            profileViewModel.clearDataBase()
            if (getSystemService(Context.NOTIFICATION_SERVICE) != null) {
                val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                nMgr.cancelAll()
            }
            if (mGoogleApiClient!!.isConnected) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient)
                mGoogleApiClient!!.disconnect()
            }
            SharedPrefsUtils.setStringPreference(this@VersionObsolete, RemoteConstant.mAppId, "")
            SharedPrefsUtils.setStringPreference(this@VersionObsolete, RemoteConstant.mApiKey, "")
            SharedPrefsUtils.setStringPreference(this@VersionObsolete, RemoteConstant.mJwt, "")
            SharedPrefsUtils.setStringPreference(
                this@VersionObsolete,
                RemoteConstant.mProfileId,
                ""
            )
            SharedPrefsUtils.setBooleanPreference(
                this@VersionObsolete, RemoteConstant.mEnabledPushNotification, false
            )
            SharedPrefsUtils.setBooleanPreference(
                this@VersionObsolete, RemoteConstant.mEnabledPrivateProfile, false
            )

            val intent = Intent(this@VersionObsolete, MainLandingActivity::class.java)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
            try {
                finishAffinity()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showVersionDialog() {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.verision_activity, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val updateVextView: TextView = dialogView.findViewById(R.id.update_text_view)
        val closeButton: TextView = dialogView.findViewById(R.id.close_button)


        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            finishAffinity()
        }
        updateVextView.setOnClickListener {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }

        }

    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient?.isConnected!!) {
            mGoogleApiClient?.disconnect()
        }
    }
}
