package com.example.sample.socialmob.view.ui

import android.content.BroadcastReceiver
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.database.Cursor
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.feed.MainFeedFragment
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import ly.img.android.pesdk.backend.model.EditorSDKResult
import ly.img.android.pesdk.backend.model.state.TransformSettings
import ly.img.android.serializer._3.IMGLYFileWriter
import java.io.File
import java.io.FileOutputStream
import java.util.*







class EditorResultReceiverKotlin : BroadcastReceiver() {
    private var mAuth = ""
    private var mThumbList: ArrayList<File>? = ArrayList()
    override fun onReceive(context: Context, intent: Intent) {
        val data = EditorSDKResult(intent)

        try {
            when (data.resultStatus) {
                EditorSDKResult.Status.CANCELED -> {
                    MainFeedFragment.isCompletedVideoEdit = false
                    ProfileFeedFragment.isCompletedVideoEdit = false
                    ProfileMediaGridFragment.isCompletedVideoEdit = false
//                    Toast.makeText(
//                        context,
//                        "Editor is closed because the User clicks the cancel button.",
//                        Toast.LENGTH_LONG
//                    ).show()
                }
                EditorSDKResult.Status.CANCELED_BY_SYSTEM -> {
                    MainFeedFragment.isCompletedVideoEdit = false
                    ProfileFeedFragment.isCompletedVideoEdit = false
                    ProfileMediaGridFragment.isCompletedVideoEdit = false
                    /********************************************************************
                     * The editor is canceled by the System, because of unknown reason. *
                     * The User has maybe force closed the App                          *
                     *******************************************************************/
                }
                EditorSDKResult.Status.DONE_WITHOUT_EXPORT -> {
                    MainFeedFragment.isCompletedVideoEdit = false
                    ProfileFeedFragment.isCompletedVideoEdit = false
                    ProfileMediaGridFragment.isCompletedVideoEdit = false
//                    Toast.makeText(
//                        context,
//                        "Editor result accepted but not changed.",
//                        Toast.LENGTH_LONG
//                    ).show()
                }
                EditorSDKResult.Status.EXPORT_STARTED -> {


//                    MainFeedFragment.EXPORT_STARTED = true
                    MainFeedFragment.isCompletedVideoEdit = false
//                    ProfileFeedFragment.EXPORT_STARTED = true
                    ProfileFeedFragment.isCompletedVideoEdit = false
//                    ProfileMediaGridFragment.EXPORT_STARTED = true
                    ProfileMediaGridFragment.isCompletedVideoEdit = false
                    // The editor has started the export, we can now save the serialization in the meantime.
                    File(Environment.getExternalStorageDirectory(), "SavedEditorState.json").also {
                        it.delete()
                    }.outputStream().use {
                        IMGLYFileWriter(data.settingsList).writeJson(it)
                    }

                    val intentVideo =
                        Intent(MyApplication.application, UploadPhotoActivityMentionHashTag::class.java)
                    intentVideo.setFlags(FLAG_ACTIVITY_NEW_TASK)
                    intentVideo.putExtra("title", "UPLOAD VIDEO")
                    intentVideo.putExtra("file", "")
                    intentVideo.putExtra("RATIO", "")
                    intentVideo.putExtra("type", "video")
                    intentVideo.putExtra("mImageList", "")
                    MyApplication.application?.startActivity(intentVideo)


                }
                EditorSDKResult.Status.EXPORT_DONE -> {
                    // The editor export is done, we can now save the image and notify the gallery to update the image.
                    data.notifyGallery(to = EditorSDKResult.UPDATE_RESULT and EditorSDKResult.UPDATE_SOURCE)


                    val settingsList = data.settingsList
                    val ts = settingsList.getSettingsModel(
                        TransformSettings::class.java
                    )
                    val mRatio = ts.getAspectRation() // Returns the aspect ratio

                    var mVideoRatio = "1:1"
                    if (mRatio.toString() == "1.333333") {
                        mVideoRatio = "4:3"
                    } else if (mRatio.toString() == "1.777778") {
                        mVideoRatio = "16:9"
                    } else if (mRatio.toString() == "1") {
                        mVideoRatio = "1:1"
                    }


                    val mImageList: ArrayList<GalleryData> = ArrayList()
                    val mGalleryData = GalleryData()
                    val mPath = getFilePath(MyApplication.application!!, data.resultUri!!)
//                    val mPath =  data.resultUri?.path
                    mGalleryData.photoUri = mPath!!
                    mImageList.add(mGalleryData)


                    val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(
                            MyApplication.application!!,
                            RemoteConstant.mAppId,
                            ""
                        )!!,
                        SharedPrefsUtils.getStringPreference(
                            MyApplication.application!!,
                            RemoteConstant.mApiKey,
                            ""
                        )!!,
                        RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mPresignedurl +
                                RemoteConstant.mSlash + "video" + RemoteConstant.mSlash + mImageList.size.toString(),
                        RemoteConstant.mGetMethod
                    )
                    mAuth =
                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                            MyApplication.application!!,
                            RemoteConstant.mAppId,
                            ""
                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]




                    for (i in 0 until mImageList.size) {

                        val videoThumbImageFile: File

                        val bitmap =
                            ThumbnailUtils.createVideoThumbnail(
                                mImageList[i].photoUri,
    //                                mImageListNew[i].photoUri,
                                MediaStore.Video.Thumbnails.MINI_KIND
                            )

                        val file =
                            File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
                        if (!file.exists()) {
                            file.mkdir()
                        }
    //                        val filesDir = filesDir
                        videoThumbImageFile =
                            File(file, "socialmob" + System.currentTimeMillis() / 1000 + ".jpg")

                        try {
                            val os = FileOutputStream(videoThumbImageFile)
                            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, os)
                            os.flush()
                            os.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        mThumbList?.add(videoThumbImageFile)

                    }

                    when {
                        SharedPrefsUtils.getStringPreference(
                            MyApplication.application,
                            "POST_FROM",
                            ""
                        ) == "CAMPAIGN_FEED" -> {

//                            ProfileMediaGridFragment.titlePost = title
//                            ProfileMediaGridFragment.mAuthPostType = type
//                            ProfileMediaGridFragment.postDataPost = postData
                            ProfileMediaGridFragment.isAddedFeed = true
                            ProfileMediaGridFragment.mThumbListPost = mThumbList
                            ProfileMediaGridFragment.mAuthPost = mAuth
                            ProfileMediaGridFragment.mImageListPost = mImageList.size.toString()
                            ProfileMediaGridFragment.mImageListPostList = mImageList
                            ProfileMediaGridFragment.ratio = mVideoRatio
                            ProfileMediaGridFragment.isCompletedVideoEdit = true
//                            customProgressDialog?.dismiss()
//                            finish()


                        }
                        SharedPrefsUtils.getStringPreference(MyApplication.application, "POST_FROM", "") == "MAIN_FEED" -> {

//                            MainFeedFragment.titlePost = title
//                            MainFeedFragment.mAuthPostType = type
//                            MainFeedFragment.postDataPost = postData
                            MainFeedFragment.isAddedFeed = true
                            MainFeedFragment.mThumbListPost = mThumbList
                            MainFeedFragment.mAuthPost = mAuth
                            MainFeedFragment.mImageListPost = mImageList.size.toString()
                            MainFeedFragment.mImageListPostList = mImageList
                            MainFeedFragment.ratio = mVideoRatio
                            MainFeedFragment.isCompletedVideoEdit = true
                            println(""+MainFeedFragment.isCompletedVideoEdit)
//                            customProgressDialog?.dismiss()
//                            finish()


                        }
                        else -> {
//                            ProfileFeedFragment.titlePost = title
//                            ProfileFeedFragment.mAuthPostType = type
//                            ProfileFeedFragment.postDataPost = postData
                            ProfileFeedFragment.isAddedFeed = true
                            ProfileFeedFragment.mAuthPost = mAuth
                            ProfileFeedFragment.mImageListPost = mImageList.size.toString()
                            ProfileFeedFragment.mImageListPostList = mImageList
                            ProfileFeedFragment.mThumbListPost = mThumbList
                            ProfileFeedFragment.ratio = mVideoRatio
                            ProfileFeedFragment.isCompletedVideoEdit = true

//                            customProgressDialog?.dismiss()
//                            finish()

                        }
                    }
//                    if (SharedPrefsUtils.getStringPreference(
//                            MyApplication.application,
//                            "POST_FROM", ""
//                        ) == "MAIN_FEED"
//                    ) {
//                        MainFeedFragment.mImageListPost = mImageList.size.toString()
//                        MainFeedFragment.mImageListPostList = mImageList
//                        MainFeedFragment.ratio = mVideoRatio
//                        MainFeedFragment.mAuthPost = mAuth
//                        MainFeedFragment.mThumbListPost = mThumbList
//                        MainFeedFragment.isCompeletedVideoEdit = true
//                    } else {
//                        ProfileFeedFragment.mImageListPost = mImageList.size.toString()
//                        ProfileFeedFragment.mImageListPostList = mImageList
//                        ProfileFeedFragment.ratio = mVideoRatio
//                        ProfileFeedFragment.mAuthPost = mAuth
//                        ProfileFeedFragment.mThumbListPost = mThumbList
//                        ProfileFeedFragment.isCompeletedVideoEdit = true
//                    }


//                    Toast.makeText(
//                        PESDK.getAppContext(),
//                        "Editor Export is done. File is saved at: ${data.resultUri}.",
//                        Toast.LENGTH_LONG
//                    ).show()

//                    EventBus.getDefault().post(UploadBottom(true))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                context.applicationContext,
                uri
            )
        ) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {


            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor? = null
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }
}