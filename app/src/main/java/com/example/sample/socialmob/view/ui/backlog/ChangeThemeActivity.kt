package com.example.sample.socialmob.view.ui.backlog

import android.content.Intent
import android.os.Bundle
import android.transition.Transition
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.profile.ThemeAdapter
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import kotlinx.android.synthetic.main.edit_theme_activity.*

class ChangeThemeActivity : AppCompatActivity(), ThemeAdapter.ThemeItemClickListener {

    private var themeViewModel: MusicDashBoardViewModel? = null
    private var jukeBoxCategoryId = ""
    private var mThemeCover = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.edit_theme_activity)
        themeViewModel = ViewModelProviders.of(this).get(MusicDashBoardViewModel::class.java)

        getTransitions()
        callApi()
        observeData()
        val i = intent
        jukeBoxCategoryId = i.getStringExtra("jukeBoxCategoryId") ?: ""
        mThemeCover = i.getStringExtra("mThemeCover") ?: ""

        Glide.with(this).load(mThemeCover).into(theme_image_view)

    }

    fun saveChangesTheme(view: View) {
        onBackPressed()

    }

    private fun callApi() {

        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathGenre)

//            GlobalScope.launch {
//                themeViewModel?.getGenre(mAuth)
//            }
        }

    }

    private fun observeData() {

//        themeViewModel!!.genreModel.nonNull().observe(this, Observer {
//            if (it!!.isNotEmpty()) {
//                genreList = it
//                theme_recycler_view.adapter = ThemeAdapter(it, this)
//                onThemeItemClickListener(jukeBoxCategoryId, mThemeCover)
//            }
//        })
    }


    fun editThemeOnBackPressed(view: View) {
        onBackPressed()
    }

    private fun getTransitions() {

        //add transition listener to load full-size image on transition end
        window.sharedElementEnterTransition.addListener(object : Transition.TransitionListener {
            override fun onTransitionEnd(transition: Transition?) {
                //load full-size
                //Picasso.get().load(item.photoUrl).noFade().noPlaceholder().into(ivDetail)
                //transition?.removeListener(this)
            }

            override fun onTransitionResume(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTransitionPause(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTransitionCancel(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTransitionStart(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }
        })
    }

    override fun onThemeItemClickListener(jukeBoxCategoryId: String, mThemeCover: String) {
        this.jukeBoxCategoryId = jukeBoxCategoryId
        this.mThemeCover = mThemeCover

        Glide.with(this).load(mThemeCover).into(theme_image_view)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra("jukeBoxCategoryId", jukeBoxCategoryId)
        intent.putExtra("mThemeCover", mThemeCover)
        setResult(1401, intent)
        AppUtils.hideKeyboard(this@ChangeThemeActivity, edit_theme_back_image_view)
    }

    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.profile_bg_color)
    }

}