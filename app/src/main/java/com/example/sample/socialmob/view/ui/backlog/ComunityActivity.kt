package com.example.sample.socialmob.view.ui.backlog

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AppUtils
import kotlinx.android.synthetic.main.community_activity.*
import java.util.*

class ComunityActivity : AppCompatActivity() {
    val treadingTrackList: ArrayList<String> = ArrayList()
    val topTrackList: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.community_activity)




        // TODO : TO UNCOMMENT
//        community_trending_track_recycler_view.adapter = TrendingTrackAdapter(treadingTrackList, this)
//        community_top_track_recycler_view.adapter = TopTrackAdapter(topTrackList, this)

//        community_users_recycler_view.adapter = UsersAdapter(userList, this)

        community_back_image_view.setOnClickListener {
            onBackPressed()
        }


    }
    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@ComunityActivity, community_back_image_view)
    }

    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }
}
