package com.example.sample.socialmob.view.ui.backlog.articles

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AddCommentResponseModel {
    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("code")
    @Expose
    val code: Int? = null
    @SerializedName("payload")
    @Expose
    val payload: AddCommentResponseModelPayload? = null
    @SerializedName("now")
    @Expose
    val now: String? = null
}
