package com.example.sample.socialmob.view.ui.backlog.articles

import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadComment
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddCommentResponseModelPayload {
    @SerializedName("success")
    @Expose
     val success: Boolean? = null
    @SerializedName("comment")
    @Expose
     val comment: SingleArticleApiModelPayloadComment? = null
}
