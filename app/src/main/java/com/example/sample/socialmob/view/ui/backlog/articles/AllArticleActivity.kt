package com.example.sample.socialmob.view.ui.backlog.articles

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AllArticleActivityBinding
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.model.article.ArticleData
import com.example.sample.socialmob.viewmodel.article.ArticleViewModel
import kotlinx.android.synthetic.main.all_article_activity.*


class AllArticleActivity : AppCompatActivity(), RecommendedArticleAdapter.ItemClickRecommendedArticleAdapter,
    RecommendedArticleAdapter.RecommendedArticleAdapterBookmarkClick {

    private var mLastId: String = ""
    private var articleViewModel: ArticleViewModel? = null
    private var mAllArticles: MutableList<ArticleData> = ArrayList()
    private lateinit var mAdapter: RecommendedArticleAdapter
    private var binding: AllArticleActivityBinding? = null
    private var isAdded: Boolean = false

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myList = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                for (i in 0 until mAllArticles.size) {
                    for (i1 in 0 until myList.size) {
                        if (mAllArticles[i].ContentId == myList[i1].mId) {
                            mAllArticles[i].Bookmarked = myList[i1].mIsLike
                        }
                    }
                }
                mAdapter.addAll(mAllArticles)
                mAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        binding = DataBindingUtil.setContentView(this, R.layout.all_article_activity)
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)

        observeList()
        mAdapter = RecommendedArticleAdapter(
            mAllArticles, this, this, false
        )
        binding!!.allArticleRecyclerView.adapter = mAdapter
        binding!!.allArticleRecyclerView.layoutManager =
            WrapContentLinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        binding!!.allArticleRecyclerView.addOnScrollListener(CustomScrollListener())

        // TODO : Check Internet connection
        checkInternet()

    }

    inner class CustomScrollListener : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {

        }

        override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (!articleViewModel!!.isLoadAllArticle) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    loadMoreItems(mAllArticles.size)
                }
            }
        }
    }

    private fun loadMoreItems(size: Int) {
        if (!articleViewModel!!.isLoadCompletedBookmarkedArticle) {

            mLastId = mAllArticles[size.minus(1)].ContentId!!
            val mutableList: MutableList<ArticleData>? = ArrayList()
            val mList = ArticleData(
                0, "", "", "", "", "",
                "", "", "", "", "", "", "", ""
            )

            mutableList!!.add(mList)
            mAdapter.addLoader(mutableList)


            val mAuth =
                RemoteConstant.getAuthArticleRecommended(this@AllArticleActivity, mLastId, RemoteConstant.mCount)
            articleViewModel!!.recommendedArticle(
                mAuth, RemoteConstant.mPlatform,
                RemoteConstant.mPlatformId, mLastId, RemoteConstant.mCount
            )
        }

    }

    private fun checkInternet() {

        if (InternetUtil.isInternetOn()) {
            binding!!.isVisibleList = false
            binding!!.isVisibleLoading = true
            binding!!.isVisibleNoData = false
            binding!!.isVisibleNoInternet = false
            callApiAllArticle()
        } else {
            binding!!.isVisibleList = false
            binding!!.isVisibleLoading = false
            binding!!.isVisibleNoData = false
            binding!!.isVisibleNoInternet = true
            val shake: Animation = AnimationUtils.loadAnimation(this@AllArticleActivity, R.anim.shake)
            binding!!.noInternetLinear.startAnimation(shake) // starts animation

        }
    }

    private fun callApiAllArticle() {

        //TODO:  API FOR RECOMMENDED ARTICLE
        val mAuth = RemoteConstant.getAuthArticleRecommended(this, RemoteConstant.mOffset, RemoteConstant.mCount)
        articleViewModel!!.recommendedArticle(
            mAuth, RemoteConstant.mPlatform,
            RemoteConstant.mPlatformId, RemoteConstant.mOffset, RemoteConstant.mCount
        )

    }

    fun backPressAllArticle(view: View) {
        onBackPressed()
    }

    private fun observeList() {

        articleViewModel!!.allArticleListLiveData.nonNull().observe(this, Observer {
            mAllArticles = it!!
            if (!articleViewModel!!.isLoadAllArticle) {
                if (mAllArticles.isNotEmpty()) {
                    binding!!.isVisibleList = true
                    binding!!.isVisibleLoading = false
                    binding!!.isVisibleNoData = false
                    binding!!.isVisibleNoInternet = false

                    mAdapter.removeLoader()


                    val newAdList: MutableList<ArticleData>? = ArrayList()
                    for (i in 0 until it.size) {

                        if (i % 7 == 0) {
                            if (i != 0) {
                                val mList = ArticleData(
                                    0, "", "-1", "", "", "",
                                    "", "", "", "", "", "", "", ""
                                )

                                newAdList!!.add(mList)
                            }
                            newAdList!!.add(it[i])
                        } else {
                            if (!isAdded) {
                                if (i == 2) {
                                    isAdded = true
                                    val mList = ArticleData(
                                        0, "", "-1", "", "", "",
                                        "", "", "", "", "", "", "", ""
                                    )

                                    newAdList!!.add(mList)
                                }
                            }
                            newAdList!!.add(it[i])
                        }

                    }

                    mAdapter.addAll(newAdList!!)
                    mAdapter.notifyDataSetChanged()

                }
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        binding!!.allArticleRecyclerView.adapter = null
        articleViewModel!!.allArticleListLiveData.removeObservers(this)
        binding!!.unbind()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@AllArticleActivity, all_article_back_image_view)
    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
//        window.statusBarColor = ContextCompat.getColor(this, R.color.black);
    }

    override fun onRecommendedArticleAdapterBookmarkClick(isBookMarked: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isBookMarked) {
                "true" -> removeBookmarkApi(mId)
                "false" -> addToBookmarkApi(mId)
            }
        } else {
            AppUtils.showCommonToast(this@AllArticleActivity, getString(R.string.no_internet))
        }

    }

    private fun removeBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthRemoveBookmark(this@AllArticleActivity, mId)
        articleViewModel!!.removeBookmark(mAuth, mId)
    }

    private fun addToBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthAddBookmark(this@AllArticleActivity, mId)
        articleViewModel!!.addToBookmark(mAuth, mId)
    }

    override fun onItemClick(mId: String) {
        val intent = Intent(this, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mId)
        startActivityForResult(intent, 141)
    }


}
