package com.example.sample.socialmob.view.ui.backlog.articles

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.BookmarkArticleItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.article.Bookmark
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.ItemClick

class ArticleBookMarkedAdapter(
    private val bookmarkedItems: MutableList<Bookmark>,
    private val clickListener: ItemClick,
    private val articleBookMarkedItemClick: ArticleBookMarkedItemClick
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ProgressItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgerssViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<BookmarkArticleItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.bookmark_article_item, parent, false
        )
        return ArticleViewHolder(binding)
    }

    fun addAll(it: MutableList<Bookmark>?) {
        if (it?.size!! > 0) {
            val size = bookmarkedItems.size
            bookmarkedItems.clear() //here podcastList is an ArrayList populating the RecyclerView
            bookmarkedItems.addAll(it)// add new data
            notifyItemChanged(size, bookmarkedItems.size)// notify adapter of new data
        }
    }

    fun addLoader(it: MutableList<Bookmark>?) {
        bookmarkedItems.addAll(it!!)
    }

    fun removeLoader() {
        if (bookmarkedItems.isNotEmpty()) {
            bookmarkedItems.removeAt(bookmarkedItems.size.minus(1))
        }
    }


    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(bookmarkedItems[position])

    }

    private fun notify(adapterPosition: Int) {
        if (bookmarkedItems.size > 0) {
            bookmarkedItems.removeAt(adapterPosition)
            notifyItemRemoved(adapterPosition)
            notifyItemChanged(adapterPosition, bookmarkedItems.size)
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (bookmarkedItems[position].ContentId != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    override fun getItemCount(): Int {
        return bookmarkedItems.size
    }

    inner class ProgerssViewHolder(val binding: ProgressItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class ArticleViewHolder(val binding: BookmarkArticleItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.bookmarkArticleModel = bookmarkedItems[adapterPosition]
            binding.executePendingBindings()
            itemView.setOnClickListener {
                clickListener.onItemClick(bookmarkedItems[adapterPosition].ContentId!!)

            }
            binding.bookmarkImageView.setOnClickListener {
                articleBookMarkedItemClick.onArticleBookMarkedItemClick(bookmarkedItems[adapterPosition].ContentId!!)
                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }
        }


    }

    interface ArticleBookMarkedItemClick {
        fun onArticleBookMarkedItemClick(mId: String)
    }
}


