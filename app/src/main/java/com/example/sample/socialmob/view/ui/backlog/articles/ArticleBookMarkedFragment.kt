package com.example.sample.socialmob.view.ui.backlog.articles

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArticleBookmarkedBinding
import com.example.sample.socialmob.model.article.Bookmark
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.ItemClick
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.viewmodel.article.ArticleViewModel

class ArticleBookMarkedFragment : Fragment(), ItemClick, ArticleBookMarkedAdapter.ArticleBookMarkedItemClick {
    private var articleViewModel: ArticleViewModel? = null
    private var binding: ArticleBookmarkedBinding? = null
    private var mGlobalList: MutableList<Bookmark>? = ArrayList()
    private var mBookmarkedAdapter: ArticleBookMarkedAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.article_bookmarked, container, false)
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        return binding!!.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myList = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                if (myList.size > 0) {
                    for (i in 0 until mGlobalList!!.size) {
                        for (i1 in 0 until myList.size) {
                            if (mGlobalList!![i].ContentId == myList[i1].mId) {
                                mGlobalList!![i].Bookmarked = myList[i1].mIsLike
//                                mGlobalList!!.removeAt(i)
//                                removePosId(myList[i1].mId)
                            }
                        }
                    }
                    mBookmarkedAdapter!!.addAll(mGlobalList!!)
                    mBookmarkedAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        callApiCommon()

        binding!!.refreshTextView.setOnClickListener {
            callApiCommon()
        }
        binding!!.bookmarkedArticleRecyclerView.layoutManager =
                WrapContentLinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        binding!!.bookmarkedArticleRecyclerView.addOnScrollListener(CustomScrollListener())

        mBookmarkedAdapter = ArticleBookMarkedAdapter(
            mGlobalList!!, this, this
        )
        binding!!.bookmarkedArticleRecyclerView.adapter = mBookmarkedAdapter

        binding!!.swipeLayout.setColorSchemeResources(R.color.purple_500)
        binding!!.swipeLayout.setOnRefreshListener {
            mGlobalList!!.clear()
            callApiCommon()
        }
    }

    inner class CustomScrollListener : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {

        }

        override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (!articleViewModel!!.isLoadBookmarkedArticle) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    loadMoreItems(mGlobalList!!.size)
                }
            }
        }
    }

    private fun loadMoreItems(size: Int) {
        if (!articleViewModel!!.isLoadCompletedBookmarkedArticle) {

            val mutableList: MutableList<Bookmark>? = ArrayList()
            val mList = Bookmark(
                0, "", "", "", "", "",
                "", "", "", "", ""
            )

            mutableList!!.add(mList)
            mBookmarkedAdapter!!.addLoader(mutableList)
            val mAuth = RemoteConstant.getAuthBookMarkedArticle(activity, size.toString(), RemoteConstant.mCount)

            articleViewModel!!.getBookMarkedArticle(
                mAuth, RemoteConstant.mPlatform,
                RemoteConstant.mPlatformId, size.toString(), RemoteConstant.mCount
            )
        }

    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi()

        } else {
            if (mGlobalList != null && mGlobalList!!.size > 0) {
                binding!!.isVisibleList = true
                binding!!.isVisibleLoading = false
                binding!!.isVisibleNoData = false
                binding!!.isVisibleNoInternet = false
            } else {
                binding!!.isVisibleList = false
                binding!!.isVisibleLoading = false
                binding!!.isVisibleNoData = false
                binding!!.isVisibleNoInternet = true
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding!!.noInternetLinear.startAnimation(shake) // starts animation
            }
        }

    }


    private fun callApi() {
        binding!!.isVisibleList = false
        binding!!.isVisibleLoading = true
        binding!!.isVisibleNoData = false
        binding!!.isVisibleNoInternet = false
        val mAuth = RemoteConstant.getAuthBookMarkedArticle(activity, RemoteConstant.mOffset, RemoteConstant.mCount)
        articleViewModel!!.getBookMarkedArticle(
            mAuth, RemoteConstant.mPlatform,
            RemoteConstant.mPlatformId, RemoteConstant.mOffset, RemoteConstant.mCount
        )

        // TODO : Observe Data
        observeBookmarkedArticle()
        mGlobalList!!.clear()
    }

    private fun observeBookmarkedArticle() {
        binding!!.isVisibleList = false
        binding!!.isVisibleLoading = true
        binding!!.isVisibleNoData = false
        binding!!.isVisibleNoInternet = false
        articleViewModel!!.boomarkedArticleListLiveData.nonNull().observe(this, Observer {
            mGlobalList = it
            if (!articleViewModel!!.isLoadBookmarkedArticle) {
                if (mGlobalList!!.isNotEmpty()) {
                    binding!!.isVisibleList = true
                    binding!!.isVisibleLoading = false
                    binding!!.isVisibleNoData = false
                    binding!!.isVisibleNoInternet = false
                    mBookmarkedAdapter!!.removeLoader()
                    mBookmarkedAdapter!!.addAll(mGlobalList!!)
                    mBookmarkedAdapter!!.notifyDataSetChanged()
                } else {
                    binding!!.isVisibleList = false
                    binding!!.isVisibleLoading = false
                    binding!!.isVisibleNoData = true
                    binding!!.isVisibleNoInternet = false
                }
                binding!!.swipeLayout.isRefreshing = false
            }

        })

    }

    override fun onItemClick(mString: String) {
        val intent = Intent(activity, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mString)
        intent.putExtra("isNeedToCallApi", true)
        startActivityForResult(intent, 141)


    }

    override fun onArticleBookMarkedItemClick(mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthRemoveBookmark(activity, mId)
            articleViewModel!!.removeBookmark(mAuth, mId)
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }

    }
}