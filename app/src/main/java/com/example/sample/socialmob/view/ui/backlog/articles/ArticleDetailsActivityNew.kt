package com.example.sample.socialmob.view.ui.backlog.articles

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArticleDetailActivityNewBinding
import com.example.sample.socialmob.model.article.CommentPostData
import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadComment
import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadContent
import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadSimilarArticle
import com.example.sample.socialmob.model.feed.PostFeedDataMention
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.socialview.Mention
import com.example.sample.socialmob.view.utils.socialview.MentionArrayAdapter
import com.example.sample.socialmob.view.utils.webview.MarkdownView
import com.example.sample.socialmob.viewmodel.article.*
import com.example.sample.socialmob.model.login.Profile
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.article_detail_activity.*
import java.util.regex.Pattern


class ArticleDetailsActivityNew : AppCompatActivity(), ItemClick,
    CommentsAdapter.OptionArticleItemClickListener {
    private var articleViewModel: ArticleViewModel? = null
    private var mId: String = ""
    private var binding: ArticleDetailActivityNewBinding? = null
    private var data: SingleArticleApiModelPayloadContent? = null
    private var isLoaded = false
    private var isLoadedTags = false
    private var isNeedToCallApi = false
    private var isOffline = false
    private var mSimilarArticles: SimilarArticleAdapter? = null
    private var similarArticles: MutableList<SingleArticleApiModelPayloadSimilarArticle>? =
        ArrayList()
    private val tagList: MutableList<String> = ArrayList()
    private val tagIdList: MutableList<String> = ArrayList()

    var myList = ArrayList<BookmarkList>()
    private var comments: MutableList<SingleArticleApiModelPayloadComment>? = ArrayList()
    private var mCommentAdapter: CommentsAdapter? = null
    private var isFirstTimeCall = false
    private var isCalled = false

    // For mention
    private var mentionList: MutableList<Profile>? = ArrayList()
    private var mListMention: MutableList<PostFeedDataMention>? = ArrayList()
    private var defaultMentionAdapter: ArrayAdapter<Mention>? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myListNew = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                if (myListNew.size > 0) {
                    for (i in 0 until similarArticles!!.size) {
                        for (i1 in 0 until myListNew.size) {
                            if (similarArticles!![i].contentId == myListNew[i1].mId) {
                                similarArticles!![i].bookmarked = myListNew[i1].mIsLike
                            }
                        }
                    }
                    mSimilarArticles!!.addAll(similarArticles)
                    mSimilarArticles!!.notifyDataSetChanged()
                    myList.addAll(myListNew)

                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        binding = DataBindingUtil.setContentView(this, R.layout.article_detail_activity_new)
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)

        articleViewModel!!.mUserImage =
            SharedPrefsUtils.getStringPreference(
                this@ArticleDetailsActivityNew,
                RemoteConstant.mUserImage,
                ""
            )!!

        // TODO: Get extras intent
        if (intent.extras != null) {
            val bundle = intent.extras
            if (bundle != null) {
                if (bundle.containsKey("mId")) {
                    mId = bundle.getString("mId")!!
                    isNeedToCallApi = bundle.getBoolean("isNeedToCallApi")
                    isOffline = bundle.getBoolean("isOffline")
                    if (mId != "") {
                        callApi(mId)
                        articleViewModel!!.callArticleAnaltics(this@ArticleDetailsActivityNew, mId)
                        observeData()

                    }
                }
            }
        }

        // TODO : Share Link Clicl
        if (intent?.action != null && intent?.data != null) {
            val data: Uri? = intent?.data
            if (data!!.lastPathSegment != null) {
                mId = data.lastPathSegment!!
                val hashids = Hashids("hgdhfdf44hfg8h8fg8h8fg8hf8h8f8f8hfh8f")
                val mIdLong = hashids.decode(data.lastPathSegment!!)
                if (mIdLong != null && mIdLong.size > 0) {
                    mId = mIdLong[0].toString()
                    callApi(mId)
                    articleViewModel!!.callArticleAnaltics(this@ArticleDetailsActivityNew, mId)
                    observeData()
                }
            }
        }

        // TODO : Observe comment list
        observeCommentList()

        // TODO : Mention in comments
        binding?.commentEditText?.isMentionEnabled = true
        binding?.commentEditText?.setMentionTextChangedListener { view, text ->
            if (text.isNotEmpty()) {
                defaultMentionAdapter?.clear()
                searchMention(text.toString())
            }
        }

        defaultMentionAdapter = MentionArrayAdapter(this)
        binding!!.commentEditText.mentionAdapter = defaultMentionAdapter


        // TODO : Share Click
        binding!!.shareArticle.setOnClickListener {
            ShareAppUtils.shareArticle(mId.toLong(), this)
        }

        // TODO : Bookmark Click
        binding!!.bookmarkImageView.setOnClickListener {
            // TODO : Check Internet connection
            if (InternetUtil.isInternetOn()) {
                onBookmarkClick(data!!.bookmarked!!, data!!.contentId!!)

                if (data!!.bookmarked == "true") {
                    data!!.bookmarked = "false"
                    val mBookmark = BookmarkList(data!!.contentId!!, "false")
                    myList.add(mBookmark)

                } else {
                    data!!.bookmarked = "true"
                    val mBookmark = BookmarkList(data!!.contentId!!, "true")
                    myList.add(mBookmark)

                }
                binding?.articleModel = data

            } else {
                AppUtils.showCommonToast(this, getString(R.string.no_internet))
            }
        }

        // TODO : Add comment
        binding!!.commentSendImageView.setOnClickListener {
            val mComment = binding!!.commentEditText.text.toString()
            if (mComment.isNotEmpty()) {
                if (InternetUtil.isInternetOn()) {
                    val mAuth = RemoteConstant.getAuthArticleComment(
                        this@ArticleDetailsActivityNew,
                        data!!.contentId
                    )
                    val dataPost =
                        CommentPostData(
                            mComment,
                            null
                        )
                    articleViewModel!!.postComment(mAuth, data!!.contentId, dataPost)
                    binding!!.commentEditText.setText("")
                    AppUtils.hideKeyboard(this@ArticleDetailsActivityNew, binding!!.commentEditText)
                } else {
                    AppUtils.showCommonToast(
                        this@ArticleDetailsActivityNew,
                        getString(R.string.no_internet)
                    )
                }

            } else {
                AppUtils.showCommonToast(this@ArticleDetailsActivityNew, "Type your comment")
            }
        }

        // TODO : Hashtag onClick
        binding!!.hashtags.addOnTagClickListener {
            for (i in 0 until tagList.size) {
                if (it == tagList[i]) {
                    if (!isCalled) {
                        isCalled = true
                        Handler().postDelayed({
                            isCalled = false
                        }, 1000)
                        val intent = Intent(this, TagDetails::class.java)
                        intent.putExtra("tagName", tagList[i])
                        intent.putExtra("tagId", tagIdList[i])
                        intent.putExtra("tagImage", "")
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }
        }

        // TODO : Pagination Comments
        binding!!.nestedScrollViewArticle.viewTreeObserver?.addOnScrollChangedListener {
            val view =
                binding!!.nestedScrollViewArticle.getChildAt(binding!!.nestedScrollViewArticle.childCount - 1)

            val diff =
                view.bottom - (binding!!.nestedScrollViewArticle.height + binding!!.nestedScrollViewArticle.scrollY)

            if (diff == 0) {
                //your api call to fetch data
                if (!articleViewModel!!.isLoadingComments && !articleViewModel!!.isLoadCompletedCommentList) {
                    callApiComments()
                }
            }
        }
        // TODO : Set adapter comment list
        mCommentAdapter = CommentsAdapter(comments, this, this)
        binding!!.articleCommentsRecyclerView.adapter = mCommentAdapter

        binding!!.refreshTextView.setOnClickListener {
            callApi(mId)
        }

        // TODO : Ad mob request
        val adRequest = AdRequest.Builder()
            .build()
        binding!!.adViewArticle.loadAd(adRequest)
    }

    private fun searchMention(mMention: String) {
        articleViewModel!!.getMentions(this, mMention)
        articleViewModel?.mentionProfileResponseModel?.nonNull()?.observe(this, { profiles ->
            if (profiles != null) {
                val mMentionList: MutableList<Mention> = ArrayList()
                if (profiles.isNotEmpty()) {
                    mentionList = profiles

                    val mMentionEditTextList: MutableList<String> =
                        binding?.commentEditText?.mentions ?: ArrayList()

                    val mPopulateList: MutableList<Profile> =
                        mentionList?.filter {
                            it.username !in mMentionEditTextList.map { item ->
                                item.replace("@", "")
                            }
                        } as MutableList<Profile>
                    for (i in 0 until mPopulateList.size) {
                        val mention = Mention(
                            mPopulateList[i].username?.trim()!!,
                            mPopulateList[i].username?.replace("[^A-Za-z0-9 ]".toRegex(), ""),
                            mPopulateList[i].pimage
                        )
                        mMentionList.add(mention)
                    }
                    defaultMentionAdapter?.addAll(mMentionList)
                }

                if (defaultMentionAdapter == null) {
                    defaultMentionAdapter = MentionArrayAdapter(this)
                    binding?.commentEditText?.mentionAdapter = defaultMentionAdapter
                } else {
                    binding?.commentEditText?.mentionAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun setDataOffline(mId: String) {
        articleViewModel!!.mId = mId
        articleViewModel!!.singleRecommendedArticleLiveData.nonNull().observe(this, Observer {

            if (it != null) {
//                binding?.articleModel = it!!
//                binding!!.viewModel = articleViewModel
//                data = it.payload!!.content
                binding!!.markdownView.setOnMarkdownRenderingListener(object :
                    MarkdownView.OnMarkdownRenderingListener {
                    override fun onMarkdownFinishedRendering() {
                        //                    Toast.makeText(this@ArticleDetailsActivity, "Rendered!", Toast.LENGTH_SHORT).show()
                        binding!!.isVisibleList = true
                        binding!!.isVisibleLoading = false
                        binding!!.isVisibleNoData = false
                        binding!!.isVisibleNoInternet = false
                    }

                    override fun onMarkdownRenderError() {

                    }
                })
                val mHeader =
                    "<html><head><style type=\"text/css\">@fonts-face {fonts-family: MyFont;src: " +
                            "url(\"file:///android_asset/fonts/latoregular.ttf\")}body " +
                            "{fonts-family: MyFont;fonts-size: medium; background: #FFFFFF ; padding : 0 15px ; color :#000000 ;}" +
                            "</style></head><body>"
                if (!isLoaded) {
                    isLoaded = true
                    binding!!.markdownView.loadFromText("$mHeader${it.ContentDescription}</body> \n</html>")
                }
                binding!!.markdownView.setOnLongClickListener { true }
                binding!!.markdownView.isLongClickable = false
                binding!!.markdownView.isVerticalScrollBarEnabled = false
            }
        })
    }

    private fun callApiComments() {

        if (!articleViewModel!!.isLoadCompletedCommentList) {
            val user: MutableList<SingleArticleApiModelPayloadComment>? = ArrayList()
            val mList = SingleArticleApiModelPayloadComment()
            mList.id = ""

            user!!.add(mList)
            mCommentAdapter!!.addLoader(user)
            mCommentAdapter!!.notifyDataSetChanged()
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash +
                        RemoteConstant.mContentDataV1 +
                        RemoteConstant.mSlash + mId +
                        RemoteConstant.mPathComment +
                        RemoteConstant.mSlash + comments!!.size +
                        RemoteConstant.mSlash + RemoteConstant.mCount, RemoteConstant.mGetMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    this, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


            articleViewModel!!.getAllComments(mAuth, mId, comments!!.size, RemoteConstant.mCount)


        }

    }

    private fun observeCommentList() {

        articleViewModel!!.mCommentLiveadta!!.nonNull().observe(this, Observer {
            comments = it!!
            if (!articleViewModel!!.isLoadingComments) {
                if (comments!!.isNotEmpty()) {
                    Handler().postDelayed(
                        {
                            mCommentAdapter!!.removeLoader()
                            mCommentAdapter!!.addAll(it)
                            mCommentAdapter!!.notifyDataSetChanged()
                        }, 100
                    )

                } else {
                    if (!isFirstTimeCall) {
                        isFirstTimeCall = true
                        mCommentAdapter!!.removeLoader()
                        mCommentAdapter!!.notifyDataSetChanged()
                    }
                }
                articleViewModel!!.isLoadingComments = false
            }

        })
    }

    private fun callApi(mId: String) {
        if (InternetUtil.isInternetOn()) {
            binding!!.isVisibleList = false
            binding!!.isVisibleLoading = true
            binding!!.isVisibleNoData = false
            binding!!.isVisibleNoInternet = false
            val mAuth = RemoteConstant.getAuthSingleArticle(this@ArticleDetailsActivityNew, mId)
            articleViewModel!!.getArticleDetails(mAuth, mId)
            observeData()
        } else {
            binding!!.isVisibleList = false
            binding!!.isVisibleLoading = false
            binding!!.isVisibleNoData = false
            binding!!.isVisibleNoInternet = true
            val shake: Animation = AnimationUtils.loadAnimation(this, R.anim.shake)
            binding!!.noInternetLinear.startAnimation(shake) // starts animation
        }

    }

    fun onBookmarkClick(isBookMarked: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isBookMarked) {
                "true" -> removeBookmarkApi(mId)
                "false" -> addToBookmarkApi(mId)
            }
        } else {
            AppUtils.showCommonToast(
                this@ArticleDetailsActivityNew,
                getString(R.string.no_internet)
            )
        }

    }

    private fun removeBookmarkApi(mId: String) {
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthRemoveBookmark(this@ArticleDetailsActivityNew, mId)
            articleViewModel!!.removeBookmark(mAuth, mId)
        } else {
            AppUtils.showCommonToast(
                this@ArticleDetailsActivityNew,
                getString(R.string.no_internet)
            )
        }
    }

    private fun addToBookmarkApi(mId: String) {
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthAddBookmark(this@ArticleDetailsActivityNew, mId)
            articleViewModel!!.addToBookmark(mAuth, mId)
        } else {
            AppUtils.showCommonToast(
                this@ArticleDetailsActivityNew,
                getString(R.string.no_internet)
            )
        }
    }

    private fun observeData() {

        articleViewModel!!.singleArticleLiveData.nonNull().observe(this, Observer {

            binding?.articleModel = it!!.payload!!.content
            binding!!.viewModel = articleViewModel
            data = it.payload!!.content
            binding!!.markdownView.setOnMarkdownRenderingListener(object :
                MarkdownView.OnMarkdownRenderingListener {
                override fun onMarkdownFinishedRendering() {
                    //                    Toast.makeText(this@ArticleDetailsActivity, "Rendered!", Toast.LENGTH_SHORT).show()
                    binding!!.isVisibleList = true
                    binding!!.isVisibleLoading = false
                    binding!!.isVisibleNoData = false
                    binding!!.isVisibleNoInternet = false
                }

                override fun onMarkdownRenderError() {

                }
            })
            val mHeader =
                "<html><head><style type=\"text/css\">@fonts-face {fonts-family: MyFont;src: " +
                        "url(\"file:///android_asset/fonts/latoregular.ttf\")}body " +
                        "{fonts-family: MyFont;fonts-size: medium; background: #FFFFFF ; padding : 0 15px ; color :#000000 ;}" +
                        "</style></head><body>"
            if (!isLoaded) {
                isLoaded = true
                binding!!.markdownView.loadFromText("$mHeader${it.payload!!.content!!.contentDescription}</body> \n</html>")
            }
            binding!!.markdownView.setOnLongClickListener { true }
            binding!!.markdownView.isLongClickable = false
            binding!!.markdownView.isVerticalScrollBarEnabled = false

            // TODO : Similar Article
            similarArticles = it.payload!!.similarArticles as MutableList
            mSimilarArticles = SimilarArticleAdapter(
                similarArticles,
                this, this
            )
            binding!!.articleSimilarRecyclerView.adapter = mSimilarArticles

            // TODO : Comments
//            comments = it.payload!!.comments as MutableList
//            mCommentAdapter = CommentsAdapter(it.payload!!.comments as MutableList, this, this)
//            article_comments_recycler_view.adapter = mCommentAdapter

            //TODO : Hash Tags
            for (i in 0 until it.payload!!.content!!.tags!!.size) {
                tagList.add(it.payload!!.content!!.tags!![i].tagName!!)
                tagIdList.add(it.payload!!.content!!.tags!![i].tagId!!)
            }
            if (!isLoadedTags) {
                isLoadedTags = true
                hashtags.setData(tagList, Transformers.HASH)
            }
            Handler().postDelayed({
                binding!!.editTextView.requestFocus()
            }, 100)

        })


    }

    fun articleDetailsBack(view: View) {
        onBackPressed()

    }

    override fun onOptionItemClickOption(mMethod: String, mId: String, mComment: String) {
        when (mMethod) {
            "editComment" -> editComment(mId, mComment)
            "deleteComment" -> deleteComment(mId, mComment)
        }
    }

    private fun deleteComment(mIdComment: String, mComment: String) {
        val mAuth = RemoteConstant.getAuthDeleteComment(this, mIdComment, mId)
        articleViewModel!!.deleteArticleComment(mAuth, mId, mIdComment)
    }

    private fun editComment(mId: String, mComment: String) {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(it) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.update_comment_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.comment_close_button) as TextView
        val updateCommentTextView: TextView =
            dialogView.findViewById(R.id.update_comment_text_view) as TextView
        val commentNameEditText: AutoCompleteTextView =
            dialogView.findViewById(R.id.comment_edit_text) as AutoCompleteTextView
        commentNameEditText.setText(mComment)

        val b = dialogBuilder.create()
        b?.show()

        closeButton.setOnClickListener {
            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        updateCommentTextView.setOnClickListener {
            if (commentNameEditText.text.isNotEmpty()) {

                val mAuth = RemoteConstant.getAuthUpdateComment(this@ArticleDetailsActivityNew, mId)
                val dataPost =
                    CommentPostData(
                        commentNameEditText.text.toString(),
                        null
                    )
                articleViewModel!!.updateArticleComment(mAuth, mId, dataPost)
                AppUtils.hideKeyboard(this@ArticleDetailsActivityNew, updateCommentTextView)
            } else
                AppUtils.showCommonToast(this@ArticleDetailsActivityNew, "Comment cannot be empty")
            //To close alert
            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }


    }

    override fun onItemClick(mString: String) {
        val intent = Intent(this@ArticleDetailsActivityNew, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mString)
        intent.putExtra("isNeedToCallApi", true)
        startActivityForResult(intent, 141)

    }

    override fun onBackPressed() {
        val intent = intent
        intent.putExtra("mCommentList", myList)
        setResult(141, intent)
        super.onBackPressed()
        AppUtils.hideKeyboard(this@ArticleDetailsActivityNew, article_details_back_image_view)
    }

    private fun changeStatusBarColor() {
        // finally change the color
//        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding!!.articleCommentsRecyclerView.adapter = null
        binding!!.articleSimilarRecyclerView.adapter = null
        articleViewModel!!.singleArticleLiveData.removeObservers(this)
        articleViewModel!!.singleRecommendedArticleLiveData.removeObservers(this)
        articleViewModel!!.mCommentLiveadta!!.removeObservers(this)
        binding!!.markdownView.destroy()
        similarArticles!!.clear()
        tagList.clear()
        tagIdList.clear()
        myList.clear()
        comments!!.clear()
        binding!!.unbind()
    }
}
