package com.example.sample.socialmob.view.ui.backlog.articles

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sample.socialmob.R

class ArticleDetailsAdapter(private val listViewType: List<Int>) : androidx.recyclerview.widget.RecyclerView.Adapter<ArticleDetailsAdapter.ViewHolder>() {

    companion object {
        const val ITEM_TITLE_CONTENT = 1
        const val ITEM_IMAGE = 2
        const val ITEM_TEXT = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_TITLE_CONTENT -> ViewHolderItemA(inflater.inflate(R.layout.layout_item_article_content,  parent,false))
            ITEM_IMAGE -> ViewHolderItemA(inflater.inflate(R.layout.layout_item_article_image,  parent,false))
            else -> ViewHolderItemB(inflater.inflate(R.layout.layout_item_article_text, parent,false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewType = listViewType[position]
        when (viewType) {
            ITEM_TITLE_CONTENT -> {
                val viewHolderA = holder as ViewHolderItemA
//                viewHolderA.textView.text = "Ini layout item a dengan position $position"
            }
            else -> {
                // Lakukan sesuatu jika kamu ingin mengubah gambar pada ViewHolderItemText
            }
        }
    }

    override fun getItemCount(): Int = listViewType.size

    override fun getItemViewType(position: Int): Int = listViewType[position]

    open inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

    inner class ViewHolderItemA(itemView: View) : ViewHolder(itemView) {
//        val textView: TextView = itemView.findViewById(R.id.text_view)
    }

    inner class ViewHolderItemB(itemView: View) : ViewHolder(itemView)

}