package com.example.sample.socialmob.view.ui.backlog.articles

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.HistoryArticleItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.article.UserReadArticle
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.ui.home_module.settings.BlockedUserAdapter
import com.example.sample.socialmob.view.utils.ItemClick

class ArticleHistoryAdapter(
    private var userReadArticleItems: MutableList<UserReadArticle>?,
    private val clickListener: ItemClick,
    private val articleHistoryBookmarkItemClick: ArticleHistoryBookmarkItemClick
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            BlockedUserAdapter.ITEM_DATA -> bindData(parent)
            BlockedUserAdapter.ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding:ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgerssViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding:HistoryArticleItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.history_article_item, parent, false
        )
        return ArticleHistoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(userReadArticleItems!![position])
    }

    private fun notify(adapterPosition: Int) {
        if (userReadArticleItems!![adapterPosition].Bookmarked == "true") {
            userReadArticleItems!![adapterPosition].Bookmarked = "false"
        } else {
            userReadArticleItems!![adapterPosition].Bookmarked = "true"
        }
        notifyItemChanged(adapterPosition, userReadArticleItems!!.size)
    }

    override fun getItemCount(): Int {
        return userReadArticleItems!!.size
    }

    fun addAll(it: MutableList<UserReadArticle>) {
        if (it.size > 0) {
            val size = userReadArticleItems!!.size
            userReadArticleItems!!.clear() //here podcastList is an ArrayList populating the RecyclerView
            userReadArticleItems!!.addAll(it)// add new data
            notifyItemChanged(size, userReadArticleItems!!.size)// notify adapter of new data
        }
    }

    fun addLoader(it: MutableList<UserReadArticle>) {
        userReadArticleItems!!.addAll(it)
    }

    fun removeLoader() {
        if (userReadArticleItems!!.isNotEmpty()) {
            userReadArticleItems!!.removeAt(userReadArticleItems!!.size.minus(1))
        }
    }

    inner class ArticleHistoryViewHolder(val binding: HistoryArticleItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.historyArticleModel = userReadArticleItems!![adapterPosition]
            binding.executePendingBindings()
            itemView.setOnClickListener {
                clickListener.onItemClick(userReadArticleItems!![adapterPosition].ContentId!!)
            }
            binding.bookmarkImageView.setOnClickListener {
                articleHistoryBookmarkItemClick.onArticleHistoryBookmarkItemClick(
                    userReadArticleItems!![adapterPosition].Bookmarked!!,
                    userReadArticleItems!![adapterPosition].ContentId!!
                )
                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }
        }
    }

    inner class ProgerssViewHolder(val binding: ProgressItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    interface ArticleHistoryBookmarkItemClick {
        fun onArticleHistoryBookmarkItemClick(isBookMarked: String, mId: String)
    }

}

