package com.example.sample.socialmob.view.ui.backlog.articles

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArticleBookmarkedBinding
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.ItemClick
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.model.article.ArticleData
import com.example.sample.socialmob.model.article.UserReadArticle
import com.example.sample.socialmob.viewmodel.article.ArticleViewModel

class ArticleHistoryFragment : Fragment(), ItemClick, ArticleHistoryAdapter.ArticleHistoryBookmarkItemClick {

    private var articleViewModel: ArticleViewModel? = null
    private var binding: ArticleBookmarkedBinding? = null
    private var mGlobalList: MutableList<UserReadArticle>? = ArrayList()
    private var isLoading = false
    private var mAdapter: ArticleHistoryAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.article_bookmarked, container, false)

        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        return binding!!.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myList = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                if (myList.size>0) {
                    for (i in 0 until mGlobalList!!.size) {
                        for (i1 in 0 until myList.size) {
                            if (mGlobalList!![i].ContentId == myList[i1].mId) {
                                mGlobalList!![i].Bookmarked = myList[i1].mIsLike
                            }
                        }
                    }
                    mAdapter!!.addAll(mGlobalList!!)
                    mAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        // TODO : Observe Data
        callApiCommon()

        binding!!.bookmarkedArticleRecyclerView.layoutManager =
                WrapContentLinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        binding!!.bookmarkedArticleRecyclerView.addOnScrollListener(CustomScrollListener())

        mAdapter = ArticleHistoryAdapter(
            mGlobalList,
            this, this
        )
        binding!!.bookmarkedArticleRecyclerView.adapter = mAdapter

        binding!!.refreshTextView.setOnClickListener {
            callApiCommon()
        }
        // TODO : Swipe To Refresh
        binding!!.swipeLayout.setColorSchemeResources(R.color.purple_500)
        binding!!.swipeLayout.setOnRefreshListener {
//            binding!!.swipeLayout.isRefreshing = false
            mGlobalList!!.clear()
            callApiCommon()
        }

    }

    inner class CustomScrollListener : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {

        }

        override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

                if (!articleViewModel!!.isLoadHistoryArticle) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        isLoading = true
                        loadMoreItems(mGlobalList!!.size)
                    }
                }
            }
        }
    }

    private fun loadMoreItems(size: Int) {
        if (!articleViewModel!!.isLoadCompletedHistoryArticle) {

            val blockedUser: MutableList<ArticleData>? = ArrayList()
            val mList = ArticleData(
                0, "", "", "", "", "",
                "", "", "", "", "", "", "", ""
            )

            blockedUser!!.add(mList)
            val mAuth =
                RemoteConstant.getAuthArticleHistory(activity, size.toString(), RemoteConstant.mCount)
            articleViewModel!!.historyArticle(
                mAuth, RemoteConstant.mPlatform,
                RemoteConstant.mPlatformId, size.toString(), RemoteConstant.mCount
            )
        }

    }

    private fun callApiCommon() {

        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi()
            // TODO : Observe Data
            observeHistoryArticle()
        } else {
            if (mGlobalList != null && mGlobalList!!.size > 0) {
                binding!!.isVisibleList = true
                binding!!.isVisibleLoading = false
                binding!!.isVisibleNoData = false
                binding!!.isVisibleNoInternet = false
                observeHistoryArticle()
            } else {
                binding!!.isVisibleList = false
                binding!!.isVisibleLoading = false
                binding!!.isVisibleNoData = false
                binding!!.isVisibleNoInternet = true
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding!!.noInternetLinear.startAnimation(shake) // starts animation
            }
        }
    }

    private fun callApi() {
        binding!!.isVisibleList = false
        binding!!.isVisibleLoading = true
        binding!!.isVisibleNoData = false
        binding!!.isVisibleNoInternet = false
        val mAuth = RemoteConstant.getAuthArticleHistory(activity, RemoteConstant.mOffset, RemoteConstant.mCount)
        articleViewModel!!.historyArticle(
            mAuth, RemoteConstant.mPlatform,
            RemoteConstant.mPlatformId, RemoteConstant.mOffset, RemoteConstant.mCount
        )
    }

    private fun observeHistoryArticle() {
        articleViewModel!!.userReadArticleLiveData!!.nonNull().observe(this, Observer {
            if (!articleViewModel!!.isLoadHistoryArticle) {
                mGlobalList = it
                if (mGlobalList!!.isNotEmpty()) {
                    binding!!.isVisibleList = true
                    binding!!.isVisibleLoading = false
                    binding!!.isVisibleNoData = false
                    binding!!.isVisibleNoInternet = false
                    mAdapter!!.addAll(mGlobalList!!)
                } else {
                    binding!!.isVisibleList = false
                    binding!!.isVisibleLoading = false
                    binding!!.isVisibleNoData = true
                    binding!!.isVisibleNoInternet = false
                }
            }
            binding!!.swipeLayout.isRefreshing = false
        })
    }


    override fun onItemClick(mString: String) {
        val intent = Intent(activity, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mString)
        intent.putExtra("isNeedToCallApi", true)
        startActivityForResult(intent,141)

    }

    override fun onArticleHistoryBookmarkItemClick(isBookMarked: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isBookMarked) {
                "true" -> removeBookmarkApi(mId)
                "false" -> addToBookmarkApi(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }
    }

    private fun removeBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthRemoveBookmark(activity, mId)
        articleViewModel!!.removeBookmark(mAuth, mId)
    }

    private fun addToBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthAddBookmark(activity, mId)
        articleViewModel!!.addToBookmark(mAuth, mId)
    }


}