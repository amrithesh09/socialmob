package com.example.sample.socialmob.view.ui.backlog.articles

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.UpdateArticleBookmaredEvent
import com.example.sample.socialmob.model.article.ArticleData
import com.example.sample.socialmob.viewmodel.article.ArticleViewModel
import kotlinx.android.synthetic.main.article_library.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class ArticleLibraryFragment : Fragment(), RecommendedArticleAdapter.ItemClickRecommendedArticleAdapter,
    RecommendedArticleAdapter.RecommendedArticleAdapterBookmarkClick, LibraryViewPagerAdapter.DashBoardItemClick {

    private var isLoaded = false
    private var articleViewModel: ArticleViewModel? = null
    private var mAllArticles: MutableList<ArticleData> = ArrayList()
    private var mAdapter: RecommendedArticleAdapter? = null

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = layoutInflater.inflate(R.layout.article_library, container, false)
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myList = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                for (i in 0 until mAllArticles.size) {
                    for (i1 in 0 until myList.size) {
                        if (mAllArticles[i].ContentId == myList[i1].mId) {
                            mAllArticles[i].Bookmarked = myList[i1].mIsLike
                        }
                    }
                }
                mAdapter!!.notifyDataSetChanged()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            checkInternet()
        } else {
            callApi()
        }
        // TODO : Observe Data
        observeRecommendedArticle()
        observeArticleDashBoard()

        view_all_button.setOnClickListener {
            viewAllArticle()
        }


        // perform all ui related operation here

        // Gets the layout params that will allow you to resize the layout
        val params = main_relative.layoutParams
        // Changes the height and width to the specified *pixels*
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val screenHeight = displayMetrics.heightPixels * 0.70
        val screenWidth = displayMetrics.widthPixels
        params.height = screenHeight.toInt()
        main_relative.layoutParams = params


    }

    private fun viewAllArticle() {
        val intent = Intent(activity, AllArticleActivity::class.java)
        startActivity(intent)
    }

    private fun checkInternet() {
        InternetUtil.observe(this, Observer { status ->
            if (status == true) {
                if (!isLoaded) {
                    isLoaded = true
                    callApi()
                }
            } else {
                AppUtils.showCommonToast(requireActivity(), "No Internet connection")
            }
        })
    }

    private fun callApi() {

        // TODO : API Half Screen Dash board
        val mAuthHalfScreen = RemoteConstant.getAuthHalfScreenDashBoard(activity)
        articleViewModel!!.getHalfScreenDashBoard(mAuthHalfScreen, "1")

        //TODO:  API FOR RECOMMENDED ARTICLE
        val mAuth = RemoteConstant.getAuthArticleRecommended(activity, RemoteConstant.mOffset, RemoteConstant.mCount)
        articleViewModel!!.recommendedArticle(
            mAuth, RemoteConstant.mPlatform,
            RemoteConstant.mPlatformId, RemoteConstant.mOffset, RemoteConstant.mCount
        )


    }

    private fun observeArticleDashBoard() {
        articleViewModel!!.articleDashBoardResponseModelPayload.nonNull().observe(this, Observer {
            if (it!!.isNotEmpty()) {
                val mMusicDashBoardAdapter = LibraryViewPagerAdapter(activity, it, this)
                article_viewPager.adapter = mMusicDashBoardAdapter
                library_tabs.setupWithViewPager(article_viewPager)
            }
            // TODO : Setting Space b/w tab strip
            val betweenSpace = 10
            val slidingTabStrip = library_tabs.getChildAt(0) as ViewGroup
            for (i in 0 until slidingTabStrip.childCount - 1) {
                val v = slidingTabStrip.getChildAt(i)
                val params = v.layoutParams as ViewGroup.MarginLayoutParams
                params.rightMargin = betweenSpace
            }
        })

    }

    private fun observeRecommendedArticle() {
        articleViewModel!!.recommendedArticleLiveData.nonNull().observe(this, Observer {
            if (it!!.isNotEmpty()) {
                mAllArticles = it as MutableList
                val newAdList: MutableList<ArticleData>? = ArrayList()
                for (i in 0 until it.size) {

                    if (i % 3 == 0) {
                        if (i != 0) {
                            val mList = ArticleData(
                                0, "", "-1", "", "", "",
                                "", "", "", "", "", "", "", ""
                            )

                            newAdList!!.add(mList)
                        }
                        newAdList!!.add(it[i])
                    } else {
                        newAdList!!.add(it[i])
                    }
                }
                mAdapter = RecommendedArticleAdapter(
                    newAdList, this, this, true
                )
                recommended_article_recycler_view.adapter = mAdapter
            }
        })
    }


    override fun onRecommendedArticleAdapterBookmarkClick(isBookMarked: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isBookMarked) {
                "true" -> removeBookmarkApi(mId)
                "false" -> addToBookmarkApi(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }

    }

    private fun removeBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthRemoveBookmark(activity, mId)
        articleViewModel!!.removeBookmark(mAuth, mId)
    }

    private fun addToBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthAddBookmark(activity, mId)
        articleViewModel!!.addToBookmark(mAuth, mId)
    }

    override fun onItemClick(mId: String) {
        val intent = Intent(activity, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mId)
        if (!InternetUtil.isInternetOn()) {
            intent.putExtra("isOffline", true)
        }
        startActivity(intent)

    }

    override fun onDashBoardItemClick(mLink: String) {

        if (mLink.startsWith("http")) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(mLink))
            startActivity(browserIntent)
        } else {
            val mLastIndex = mLink.substring(mLink.lastIndexOf("/") + 1)
            val intent = Intent(activity, ArticleDetailsActivityNew::class.java)
            intent.putExtra("mId", mLastIndex)
            intent.putExtra("isNeedToCallApi", true)
            startActivityForResult(intent, 141)
        }

    }


    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onMessageEvent(event: UpdateArticleBookmaredEvent) {
        if (mAllArticles.size > 0) {
            for (i in 0 until mAllArticles.size) {
                if (mAllArticles[i].ContentId == event.mId) {
                    mAllArticles[i].Bookmarked = event.isBookmarked
                }
            }
        }

    }
}