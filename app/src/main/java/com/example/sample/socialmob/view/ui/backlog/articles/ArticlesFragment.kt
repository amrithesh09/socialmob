package com.example.sample.socialmob.view.ui.backlog.articles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.events.SelectFragmentEvent
import kotlinx.android.synthetic.main.article_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ArticlesFragment : Fragment() {

    private val fragmentLibrary = ArticleLibraryFragment()
    private val fragmentBookMarked= ArticleBookMarkedFragment()
    private val fragmentHistory= ArticleHistoryFragment()

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        // TODO : Setting Top Menu
        content_frame_article_header.bringToFront()

        // TODO : To Remove ( Article Disabled in V_151)
//        val fragmentManager = childFragmentManager
//        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.replace(R.id.content_frame_article_header, fragmentTopMenu)
//        fragmentTransaction.commit()
//
//        fragmentTransaction(fragmentLibrary)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return layoutInflater.inflate(R.layout.article_fragment, container, false)
    }

    private fun fragmentTransaction(mFragment: Fragment) {
        val fragmentManager = childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.content_frame_article, mFragment)
        fragmentTransaction.commit()

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SelectFragmentEvent) {
        when {
            event.gemSelectFragment() == getString(R.string.library) -> fragmentTransaction(fragmentLibrary)
            event.gemSelectFragment() == getString(R.string.bookmarked) -> fragmentTransaction(fragmentBookMarked)
            event.gemSelectFragment() == getString(R.string.history) -> fragmentTransaction(fragmentHistory)
        }
    }
}
