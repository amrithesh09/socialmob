package com.example.sample.socialmob.view.ui.backlog.articles

import android.os.Parcel
import android.os.Parcelable



//class BookmarkList( var mId: String, var mIsLike: String)
class BookmarkList : Parcelable {

    var mId: String? = ""
        internal set
    var mIsLike: String? = ""
        internal set

    constructor(id: String, name: String) {
        this.mId = id
        this.mIsLike = name

    }


    override fun describeContents(): Int {
        // TODO Auto-generated method stub
        return 0
    }

    override fun writeToParcel(dest: Parcel, arg1: Int) {
        // TODO Auto-generated method stub
        dest.writeString(mId)
        dest.writeString(mIsLike)
    }

    constructor(`in`: Parcel) {
        mId = `in`.readString()
        mIsLike = `in`.readString()
    }

    companion object CREATOR : Parcelable.Creator<BookmarkList> {
        override fun createFromParcel(parcel: Parcel): BookmarkList {
            return BookmarkList(parcel)
        }

        override fun newArray(size: Int): Array<BookmarkList?> {
            return arrayOfNulls(size)
        }
    }

}
