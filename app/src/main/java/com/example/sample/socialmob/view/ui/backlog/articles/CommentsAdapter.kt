package com.example.sample.socialmob.view.ui.backlog.articles

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemCommentsBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadComment
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import kotlinx.android.synthetic.main.options_menu_article_edit.view.*

class CommentsAdapter(
    private val commentItems: MutableList<SingleArticleApiModelPayloadComment>?,
    private val mContext: androidx.fragment.app.FragmentActivity?,
    private val optionArticleItemClickListener: OptionArticleItemClickListener
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }


    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ProgressItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<LayoutItemCommentsBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_comments, parent, false
        )
        return CommentsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(commentItems!![position])
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    override fun getItemCount(): Int {
        return commentItems!!.size
    }

    fun addAll(it: MutableList<SingleArticleApiModelPayloadComment>) {
        if (it.size > 0) {
            val size = commentItems!!.size
            commentItems.clear() //here podcastList is an ArrayList populating the RecyclerView
            commentItems.addAll(it)// add new data
            notifyItemChanged(size, commentItems.size)// notify adapter of new data
        }
    }


    fun addLoader(it: MutableList<SingleArticleApiModelPayloadComment>) {
        commentItems!!.addAll(it)
    }

    fun removeLoader() {
        if (commentItems!!.isNotEmpty()) {
            commentItems.removeAt(commentItems.size.minus(1))
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (commentItems!![position].id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    inner class CommentsViewHolder(private val binding: LayoutItemCommentsBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.commentViewModel = commentItems!![adapterPosition]
            binding.executePendingBindings()

            binding.articleCommentOptionImageView.setOnClickListener {
                val viewGroup: ViewGroup? = null
                val view: View = LayoutInflater.from(mContext)
                    .inflate(R.layout.options_menu_article_edit, viewGroup)
                val mQQPop = PopupWindow(
                    view,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                mQQPop.animationStyle = R.style.RightTopPopAnim
                mQQPop.isFocusable = true
                mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mQQPop.isOutsideTouchable = true
                mQQPop.showAsDropDown(binding.articleCommentOptionImageView, -180, -25)
                view.edit_text_view.visibility = View.GONE
                view.edit_text_view.setOnClickListener {
                    optionArticleItemClickListener.onOptionItemClickOption(
                        "editComment",
                        commentItems[adapterPosition].id!!, commentItems[adapterPosition].text!!
                    )
                    mQQPop.dismiss()
                }
                view.delete_text_view.setOnClickListener {


                    val dialogBuilder: AlertDialog.Builder =
                        this.let { AlertDialog.Builder(mContext) }
                    val inflater: LayoutInflater = mContext!!.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton = dialogView.findViewById<View>(R.id.cancel_button) as Button
                    val okButton = dialogView.findViewById<View>(R.id.ok_button) as Button

                    val b: AlertDialog? = dialogBuilder.create()
                    b?.show()
                    okButton.setOnClickListener {
                        optionArticleItemClickListener.onOptionItemClickOption(
                            "deleteComment",
                            commentItems[adapterPosition].id!!, ""
                        )
                        Handler().postDelayed({
                            notifyRemovePostion(adapterPosition)
                        }, 100)
                        mQQPop.dismiss()
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                    cancelButton.setOnClickListener {
                        mQQPop.dismiss()
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }


                }
            }
            binding.commentUserNameTextView.setOnClickListener {
                binding.commentProfileImageView.performClick()
            }
            binding.commentProfileImageView.setOnClickListener {
                val intent = Intent(mContext, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", commentItems[adapterPosition].profileId)
                mContext!!.startActivity(intent)
            }

            // TODO : Mention Click
            binding.descriptionTextView.setOnMentionClickListener { view, text ->
                for (i in 0 until commentItems[adapterPosition].mentions!!.size) {
                    if (commentItems[adapterPosition].mentions!![i].text!!.contains(text)) {
                        if (commentItems[adapterPosition].mentions!![i].id != null) {
                            val intent = Intent(mContext, UserProfileActivity::class.java)
                            intent.putExtra(
                                "mProfileId",
                                commentItems[adapterPosition].mentions!![i].id
                            )
                            mContext!!.startActivity(intent)
                        }
                    }
                }
            }
        }
    }

    private fun notifyRemovePostion(adapterPosition: Int) {
        if (commentItems!!.size > 0) {
            commentItems.removeAt(adapterPosition)
            notifyItemRemoved(adapterPosition)
            notifyItemChanged(adapterPosition, commentItems.size)
        }
    }

    interface OptionArticleItemClickListener {
        fun onOptionItemClickOption(mMethod: String, mId: String, mComment: String)
    }

}
