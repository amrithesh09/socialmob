package com.example.sample.socialmob.view.ui.backlog.articles

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.article.HalfscreenDashboard
import com.example.sample.socialmob.repository.utils.RemoteConstant

class LibraryViewPagerAdapter(
    internal var mContext: androidx.fragment.app.FragmentActivity?,
    private val mResources: List<HalfscreenDashboard>?,
    private val dashBoardItemClick: DashBoardItemClick
) :
    PagerAdapter() {
    private var mLayoutInflater: LayoutInflater =
        mContext?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return if (mResources!!.size > 3) {
            3
        } else {
            mResources.size
        }
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = mLayoutInflater.inflate(R.layout.article_library_item, container, false)

        val articleDashboardImageView = itemView.findViewById<ImageView>(R.id.article_dashboard_image_view)

        Glide.with(mContext!!)
            .load(
                RemoteConstant.getImageUrlFromWidth(
                    mResources!![position].SourcePath,
                    RemoteConstant.getWidth(mContext)/2
                )
            ).apply(
                RequestOptions.placeholderOf(R.drawable.emptypicture).error(R.drawable.emptypicture).centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH))
            .into(articleDashboardImageView)

        articleDashboardImageView.setOnClickListener {
            dashBoardItemClick.onDashBoardItemClick(mResources[position].Link!!)
        }

        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    interface DashBoardItemClick {
        fun onDashBoardItemClick(mLink: String)
    }
}