package com.example.sample.socialmob.view.ui.backlog.articles

import androidx.databinding.DataBindingUtil
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AdmobItemBinding
import com.example.sample.socialmob.databinding.ArticleItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.article.ArticleData
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.admob_item.view.*


class RecommendedArticleAdapter(
    private var recommendedItems: MutableList<ArticleData>?,
    private val itemClickListener: ItemClickRecommendedArticleAdapter,
    private val recommendedArticleAdapterBookmarkClick: RecommendedArticleAdapterBookmarkClick,
    private val showOnlyFive: Boolean
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_AD = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_AD -> bindAd(parent)
            else -> bindData(parent)
        }

    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {

        val binding = DataBindingUtil.inflate<AdmobItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.admob_item, parent, false
        )
        return AdViewHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ProgressItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgerssViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ArticleItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.article_item, parent, false
        )
        return RecommendedArticleViewHolder(binding)
    }

    fun addAll(it: MutableList<ArticleData>) {
        if (it.size > 0) {
            val size = recommendedItems!!.size
            recommendedItems!!.addAll(it)// add new data
            notifyItemRangeChanged(size, recommendedItems!!.size)// notify adapter of new data
        }
    }

    fun addLoader(it: MutableList<ArticleData>) {
        recommendedItems!!.addAll(it)
    }

    fun removeLoader() {
        if (recommendedItems!!.isNotEmpty()) {
            recommendedItems!!.removeAt(recommendedItems!!.size.minus(1))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(recommendedItems!![position])
    }

    override fun getItemCount(): Int {

        return if (showOnlyFive) {
            return if (recommendedItems!!.size > 5) {
                5
            } else {
                recommendedItems!!.size
            }
        } else
            recommendedItems!!.size
    }

    override fun getItemViewType(position: Int): Int {

        return if (recommendedItems!![position].ContentId != "" && recommendedItems!![position].ContentId != "-1") {
            ITEM_DATA
        }
        else if (recommendedItems!![position].ContentId == "-1") {
            ITEM_AD
        }
        else {
            ITEM_LOADING
        }
    }

    inner class RecommendedArticleViewHolder(val binding: ArticleItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.articleModel = recommendedItems!![adapterPosition]
            binding.executePendingBindings()
            itemView.setOnClickListener {
                itemClickListener.onItemClick(recommendedItems!![adapterPosition].ContentId!!)

            }
            binding.bookmarkImageView.setOnClickListener {
                recommendedArticleAdapterBookmarkClick.onRecommendedArticleAdapterBookmarkClick(
                    recommendedItems!![adapterPosition].Bookmarked!!,
                    recommendedItems!![adapterPosition].ContentId!!
                )

                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }
        }
    }

    private fun notify(adapterPosition: Int) {
        if (recommendedItems!![adapterPosition].Bookmarked == "true") {
            recommendedItems!![adapterPosition].Bookmarked = "false"
        } else {
            recommendedItems!![adapterPosition].Bookmarked = "true"
        }
        notifyItemChanged(adapterPosition, recommendedItems!!.size)
    }

    inner class ProgerssViewHolder(val binding: ProgressItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class AdViewHolder(val binding: AdmobItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            val adRequest = AdRequest.Builder()
//                .addTestDevice("6A55E8D9F9E755271441D0891D626792")
                .build()
            binding.root.adViewArticle.loadAd(adRequest)
        }

    }

    interface RecommendedArticleAdapterBookmarkClick {
        fun onRecommendedArticleAdapterBookmarkClick(isBookMarked: String, mId: String)
    }

    interface ItemClickRecommendedArticleAdapter {
        fun onItemClick(mId: String)
    }
}



