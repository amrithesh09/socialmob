package com.example.sample.socialmob.view.ui.backlog.articles

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemSimilarArticleBinding
import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadSimilarArticle
import com.example.sample.socialmob.view.utils.ItemClick


class SimilarArticleAdapter(
    private var similarArticleItems: MutableList<SingleArticleApiModelPayloadSimilarArticle>?,
    private val mContext: androidx.fragment.app.FragmentActivity?,
    private val clickListener: ItemClick
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<SimilarArticleAdapter.SimilarArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SimilarArticleViewHolder {
        val binding = DataBindingUtil.inflate<LayoutItemSimilarArticleBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_similar_article, parent, false
        )
        return SimilarArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SimilarArticleViewHolder, position: Int) {
        holder.binding.similarArticleModel = similarArticleItems!![position]
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener {
            clickListener.onItemClick(similarArticleItems!![position].contentId!!)
        }
        holder.binding.bookmarkImageView.setOnClickListener {


            (mContext as ArticleDetailsActivityNew).onBookmarkClick(
                similarArticleItems!![position].bookmarked!!,
                similarArticleItems!![position].contentId!!)

            Handler().postDelayed({
                notify(holder.adapterPosition)
            }, 100)
            Handler().postDelayed({
                // TODO : TO add list in Activity
                if (similarArticleItems!![position].bookmarked == "true") {
                    similarArticleItems!![position].bookmarked = "false"
                    val mBookmark = BookmarkList(similarArticleItems!![position].contentId!!, "false")
                    mContext.myList.add(mBookmark)
                } else {
                    similarArticleItems!![position].bookmarked = "true"
                    val mBookmark = BookmarkList(similarArticleItems!![position].contentId!!, "true")
                    mContext.myList.add(mBookmark)

                }
            }, 200)
        }
    }
    private fun notify(adapterPosition: Int) {
        if (similarArticleItems!![adapterPosition].bookmarked == "true") {
            similarArticleItems!![adapterPosition].bookmarked = "false"
        } else {
            similarArticleItems!![adapterPosition].bookmarked = "true"
        }
        notifyItemChanged(adapterPosition, similarArticleItems!!.size)
    }

    override fun getItemCount(): Int {
        return similarArticleItems!!.size
    }

    fun addAll(similarArticles: MutableList<SingleArticleApiModelPayloadSimilarArticle>?) {
        if (similarArticles!!.size > 0) {
            similarArticleItems = similarArticles
            notifyDataSetChanged()
        }
    }

    class SimilarArticleViewHolder(val binding: LayoutItemSimilarArticleBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
//
    }

}
