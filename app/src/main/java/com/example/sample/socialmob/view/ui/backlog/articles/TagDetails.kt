package com.example.sample.socialmob.view.ui.backlog.articles

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.TagDetailsActivityBinding
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.search.SearchTagsFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.model.article.ArticleData
import com.example.sample.socialmob.model.article.TagArticleResponseModel
import com.example.sample.socialmob.viewmodel.article.ArticleViewModel
import kotlinx.android.synthetic.main.tag_details_activity.*

class TagDetails : AppCompatActivity(), RecommendedArticleAdapter.ItemClickRecommendedArticleAdapter,
    RecommendedArticleAdapter.RecommendedArticleAdapterBookmarkClick {
    private var binding: TagDetailsActivityBinding? = null
    private var tagName: String = ""
    private var tagId: String = ""
    private var tagImage: String = ""
    private lateinit var mAdapter: RecommendedArticleAdapter
    private var articleViewModel: ArticleViewModel? = null
    private var mResult: TagArticleResponseModel? = null
    private var mGlobalIsFollow: String = ""
    private var mAllArticles: MutableList<ArticleData> = ArrayList()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myList = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                for (i in 0 until mAllArticles.size) {
                    for (i1 in 0 until myList.size) {
                        if (mAllArticles[i].ContentId == myList[i1].mId) {
                            mAllArticles[i].Bookmarked = myList[i1].mIsLike
                        }
                    }
                }
                mAdapter.notifyDataSetChanged()
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        binding = DataBindingUtil.setContentView(this, R.layout.tag_details_activity)
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)

        val i = intent
        if (i.getStringExtra("tagName") != null) {
            tagName = i.getStringExtra("tagName")!!
        }
        if (i.getStringExtra("tagId") != null) {
            tagId = i.getStringExtra("tagId")!!
        }
        if (i.getStringExtra("tagImage") != null) {
            tagImage = i.getStringExtra("tagImage")!!
        }

        if (tagImage != "") {
            Glide.with(this).load(tagImage)
                .apply(
                    RequestOptions.placeholderOf(0).error(0).centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                )
                .thumbnail(0.1f)
                .into(binding!!.tagImageView)
        }

        binding!!.toolBarTextView.text = getString(R.string.tag) + " : " + tagName

        follow_text_view.setOnClickListener {

            if (mResult != null && mResult!!.payload != null && mResult!!.payload!!.tag!!.following != null &&
                mResult!!.payload!!.tag!!.following != null && mResult!!.payload!!.tag!!.tagId != null
            ) {
                tagFollowClick(mResult!!.payload!!.tag!!.following!!, mResult!!.payload!!.tag!!.tagId.toString())
            }
        }


        callApiCommon()
    }

    fun tagFollowClick(isFollowing: String, mId: String) {
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "true" -> unFollowTagApi(mId)
                "false" -> followTagApi(mId)
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    private fun unFollowTagApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathTag + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        articleViewModel!!.unFollowTag(mAuth, mId)

        mResult!!.payload!!.tag!!.following = "false"
        binding!!.tagViewModel = mResult
        mGlobalIsFollow = "false"
    }

    private fun followTagApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathTag + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        articleViewModel!!.followTag(mAuth, mId)

        mResult!!.payload!!.tag!!.following = "true"
        binding!!.tagViewModel = mResult
        mGlobalIsFollow = "true"

    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi(tagId)
            observeData()
            binding!!.followTextView.visibility = View.GONE
            binding!!.isVisibleList = false
            binding!!.isVisibleNoData = false
            binding!!.isVisibleLoading = true
        } else {
            binding!!.isVisibleList = false
            binding!!.isVisibleNoData = false
            binding!!.isVisibleLoading = false

        }

    }

    private fun callApi(tagId: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.tagData +
                    RemoteConstant.mSlash + RemoteConstant.mPlatformId +
                    RemoteConstant.mSlash + tagId +
                    RemoteConstant.mSlash + RemoteConstant.mOffset +
                    RemoteConstant.mSlash + RemoteConstant.mCount, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        articleViewModel!!.getTagArticle(mAuth, tagId)

    }

    private fun observeData() {
        articleViewModel!!.tagArticleResponseModel.nonNull().observe(this, Observer {
            mResult = it

            binding!!.tagViewModel = it

            if (it!!.payload!!.contents!!.isNotEmpty()) {
                mAllArticles= it.payload!!.contents as MutableList<ArticleData>
                mAdapter = RecommendedArticleAdapter(
                    it.payload.contents!! as MutableList,
                    this, this, false
                )
                tag_recycler_view.adapter = mAdapter
                binding!!.isVisibleList = true
                binding!!.isVisibleLoading = false
                binding!!.isVisibleNoData = false
                binding!!.followTextView.visibility = View.VISIBLE
                Glide.with(this).load(it.payload.tag!!.squareImage)
                    .apply(
                        RequestOptions.placeholderOf(0).error(0).centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                    )
                    .thumbnail(0.1f)
                    .into(binding!!.tagImageView)
                mGlobalIsFollow = it.payload.tag.following!!

            } else {
                binding!!.isVisibleList = false
                binding!!.isVisibleLoading = false
                binding!!.isVisibleNoData = true
            }
        })

    }

    override fun onRecommendedArticleAdapterBookmarkClick(isBookMarked: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isBookMarked) {
                "true" -> removeBookmarkApi(mId)
                "false" -> addToBookmarkApi(mId)
            }
        } else {
            AppUtils.showCommonToast(this@TagDetails, getString(R.string.no_internet))
        }

    }

    private fun removeBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthRemoveBookmark(this@TagDetails, mId)
        articleViewModel!!.removeBookmark(mAuth, mId)
    }

    private fun addToBookmarkApi(mId: String) {
        val mAuth = RemoteConstant.getAuthAddBookmark(this@TagDetails, mId)
        articleViewModel!!.addToBookmark(mAuth, mId)
    }

    override fun onItemClick(mId: String) {
        val intent = Intent(this, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mId)
//        startActivity(intent)
        startActivityForResult(intent, 141)
    }

    fun tagBackPress(view: View) {
        onBackPressed()
    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
//        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

    override fun onBackPressed() {
        SearchTagsFragment.mTagId = tagId
        SearchTagsFragment.isFollow = mGlobalIsFollow
        super.onBackPressed()
    }
}
