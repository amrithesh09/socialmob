package com.example.sample.socialmob.view.ui.backlog.articles

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.events.OpenDrawerEvent
import com.example.sample.socialmob.view.utils.events.SelectFragmentEvent
import kotlinx.android.synthetic.main.top_menu_article.*
import org.greenrobot.eventbus.EventBus

class TopArticleMenuFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return layoutInflater.inflate(R.layout.top_menu_article, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        selectTabColorBlack(library_text_view, bookmarked_text_view, history_text_view)

        library_text_view.setOnClickListener {
            EventBus.getDefault().post(
                SelectFragmentEvent(
                    getString(R.string.library)
                )
            )
            selectTabColorBlack(library_text_view, bookmarked_text_view, history_text_view)
        }
        bookmarked_text_view.setOnClickListener {
            EventBus.getDefault().post(
                SelectFragmentEvent(
                    getString(R.string.bookmarked)
                )
            )
            selectTabColorBlack(bookmarked_text_view, library_text_view, history_text_view)
        }
        history_text_view.setOnClickListener {
            EventBus.getDefault().post(
                SelectFragmentEvent(
                    getString(R.string.history)
                )
            )
            selectTabColorBlack(history_text_view, library_text_view, bookmarked_text_view)
        }
        article_drawer_image_view.setOnClickListener {
            EventBus.getDefault().post(OpenDrawerEvent())

        }

    }

    private fun selectTabColorBlack(textView: TextView?, textView1: TextView?, textView2: TextView?) {
        textView?.setTextColor(Color.parseColor("#1e90ff"))
        textView1?.setTextColor(Color.parseColor("#000000"))
        textView2?.setTextColor(Color.parseColor("#000000"))

    }

    private fun selectTab(textView: TextView?, textView1: TextView?, textView2: TextView?) {

        textView?.setTextColor(Color.parseColor("#FFFFFF"))
        textView1?.setTextColor(Color.parseColor("#1e90ff"))
        textView2?.setTextColor(Color.parseColor("#1e90ff"))

    }


}
