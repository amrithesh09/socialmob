package com.example.sample.socialmob.view.ui.home_module

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.Constants

class CommonPlayListAdapter internal constructor(
    private val mList: List<PlaylistCommon>?,
    private val context: Context,
    private val isOptionsVisible: Boolean,
    private val glideRequest: RequestManager
) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    private val mInflater: LayoutInflater = LayoutInflater.from(context)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return PlayListHolder(mInflater.inflate(R.layout.play_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mList?.get(holder.adapterPosition))
    }


    override fun getItemViewType(position: Int): Int {
        return if (mList?.get(position)?.playlistName.equals("")) {
            Constants.ViewType.RIGHT
        } else
            Constants.ViewType.LEFT

    }

    override fun getItemCount(): Int {
        return mList?.size!!
    }

    inner class PlayListHolder(itemView: View) : BaseViewHolder<Any>(itemView) {
        private val mPlayListName: TextView =
            itemView.findViewById(R.id.quick_play_list_title_text_view)
        private val mPlayListImage: ImageView = itemView.findViewById(R.id.play_list_image_view)
        private val playListOptionImageView: ImageView =
            itemView.findViewById(R.id.play_list_option_image_view)

        //
        override fun bind(genre: Any) {

            if (!isOptionsVisible) {
                playListOptionImageView.visibility = View.GONE
            }

            mPlayListName.text = mList?.get(adapterPosition)?.playlistName

            if (mList?.get(adapterPosition)?.playlistType == "system-generated" || mList?.get(
                    adapterPosition
                )?.playlistType == "admin-generated"
            ) {
                glideRequest
                    .load(mList[adapterPosition].media?.coverFile)
                    .apply(RequestOptions.placeholderOf(0).error(R.drawable.default_list_icon))
                    .into(mPlayListImage)
            } else {
                glideRequest
                    .load(mList?.get(adapterPosition)?.icon?.fileUrl)
                    .apply(RequestOptions.placeholderOf(0).error(R.drawable.default_list_icon))
                    .into(mPlayListImage)
            }

            if (mList?.get(adapterPosition)?.playlistName == "Listen Later") {
                glideRequest
                    .load(R.drawable.listen_later_new_one)
                    .into(mPlayListImage)
            }

        }
    }
}