package com.example.sample.socialmob.view.ui.home_module

import android.Manifest
import android.app.ActivityManager
import android.app.KeyguardManager
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MusicMenuHomeBinding
import com.example.sample.socialmob.model.chat.FailedFiles
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.dao.ContactModel
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.remote.*
import com.example.sample.socialmob.repository.utils.*
import com.example.sample.socialmob.view.ui.backlog.articles.ArticlesFragment
import com.example.sample.socialmob.view.ui.home_module.chat.WebSocketChatActivity
import com.example.sample.socialmob.view.ui.home_module.chat.WebSocketChatFragment
import com.example.sample.socialmob.view.ui.home_module.feed.*
import com.example.sample.socialmob.view.ui.home_module.music.EmptyPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.HomeViewPagerAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.MusicFragment
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.PlayListImageActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.settings.InviteActivity
import com.example.sample.socialmob.view.ui.home_module.settings.ProfileFragment
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.Config
import com.example.sample.socialmob.view.ui.write_new_post.Config.KeyName.IMAGE_LIST
import com.example.sample.socialmob.view.ui.write_new_post.KVideoEditorDemoActivity
import com.example.sample.socialmob.view.ui.write_new_post.utils.CropActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.TapTarget.OnSpotlightStateChangedListener
import com.example.sample.socialmob.view.utils.TapTarget.Spotlight
import com.example.sample.socialmob.view.utils.TapTarget.shape.Circle
import com.example.sample.socialmob.view.utils.TapTarget.target.SimpleTarget
import com.example.sample.socialmob.view.utils.events.*
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.view.utils.offline.ActionListener
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.offline.DownloadListActivity
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaProgress
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.listener.ProgressListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.viewmodel.music_menu.NowPlayingViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Download
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.bottom_menu.*
import kotlinx.android.synthetic.main.music_menu_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import java.net.URL
import java.net.URLConnection
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(),
        PlaylistListener<MediaItem>, ProgressListener, ComponentCallbacks2, ActionListener {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var appUpdateManager: AppUpdateManager? = null
    private val PERMISSIONS_READ_WRITE = 123
    private var isDeleteCAlled = false
    private var isInviteDisplayed = false
    private var doubleBackToExitPressedOnce = false

    private val mPosition: ArrayList<Int> = ArrayList()
    var playlistManager: PlaylistManager? = null
    private var shouldSetDuration: Boolean = false
    private var isCheckedOpenCount: Boolean = false
    private var mNightModeStatus: Boolean = false
    private var isFailedApiCalled: Boolean = false
    private var mediaItems = LinkedList<MediaItem>()
    private var isAddedPlayList: Boolean = false

    private var mImageList: ArrayList<GalleryData> = ArrayList()
    private var isCallAnalyticsApi: Boolean = false
    private var isCallTrackAnalyticsApi: Boolean = false
    private var mGlobalList: MutableList<PlayListDataClass>? = ArrayList()

    private var binding: MusicMenuHomeBinding? = null
    private val nowPlayingViewModel: NowPlayingViewModel by viewModels()
    private var homeViewPagerAdapter: HomeViewPagerAdapter? = null
    private var mProfileDta: Profile? = null
    private var playListImageView: ImageView? = null
    private var mLastId: String = ""
    private var mShareUrl: String = ""
    private var mLastClickTime: Long = 0
    private var customProgressDialog: CustomProgressDialog? = null

    // List of native ads that have been successfully loaded.
    private val mNativeAds: MutableList<UnifiedNativeAd>? = ArrayList()
    private lateinit var mRewardedAd: RewardedAd
    private var mIsLoading = false
    private var isRewardEarned = false

    // The number of native ads to load.
    private val NUMBER_OF_ADS = 10

    // The AdLoader used to load ads.
    private var adLoader: AdLoader? = null

    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private var downloadTrackAllList: List<TrackListCommonDb>? = ArrayList()
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"
    private var mOfflineTrackDownloaded: ArrayList<Download>? = null
    private val TAG = "MainActivity"

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val viewModelChat: WebSocketChatViewModel by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }
    private var mReceiver: BroadcastReceiver? = null

    companion object {
        val DATA = "DATA"
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)

    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        //TODO : Do not change transclusent issue in one plus devices
        mNightModeStatus =
                SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)
        if (mNightModeStatus) {
            setTheme(R.style.AppTheme_NoActionBarTranslucentNightHome)
        } else {
            setTheme(R.style.AppTheme_NoActionBarTranslucentHomeDayLight)
        }
        super.onCreate(savedInstanceState)
        //TODO : Broadcast receiver for screen turn off/on For websocket connection
        registerBroadcastReceiver()
        // TODO : Change status bar color ( Do not change issue in one plus devices theme translucent )
        changeStatusBarColorBlack()
        binding = DataBindingUtil.setContentView(this, R.layout.music_menu_home)

        // TODO : AppUpdate
        appUpdateManager = AppUpdateManagerFactory.create(this)
        // TODO : Set orientation ( Do not change issue in one plus devices when setting from manifest )
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val currentOrientation = resources.configuration.orientation
            requestedOrientation = if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
            }
        }
        playlistManager = MyApplication.playlistManager
        //TODO : Enable dark mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        /**
         * https://proandroiddev.com/android-coroutine-recipes-33467a4302e9
         * Refer for Kotlin Coroutine
         */

        // carry on the normal flow, as the case of  permissions  granted.
        checkAndRequestPermissions()

        observeProfileData()

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, getString(R.string.admob_app_id))
        // Preload ads
        loadNativeAds()
        loadRewardedAd()

        // TODO : Earphone unplug
//        myReceiver = MusicIntentReceiver()

        customProgressDialog = CustomProgressDialog(this)

        // TODO : Load profile image
        val mProfileImage =
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mUserImage, "")
        /*if (mProfileImage != "") {
            val thumbnail = glideRequestManager
                    .load(R.drawable.emptypicture)
            glideRequestManager
                    .load(RemoteConstant.getImageUrlFromWidth(mProfileImage, 100))
                    .error(thumbnail)
                    .thumbnail(0.1f)
                    .into(search_icon_image_view)
        } else {
            val thumbnail = glideRequestManager
                    .load(R.drawable.emptypicture)
            glideRequestManager
                    .load(thumbnail)
                    .into(search_icon_image_view)
        }*/



        FirebaseApp.initializeApp(this)
        // TODO : Firebase token id - Register Fcm token API
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("", "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    if (SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "") != "") {
                        // Get new Instance ID token
                        val token = task.result?.token
                        val fullData = RemoteConstant.getEncryptedString(
                                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                                RemoteConstant.mBaseUrl + RemoteConstant.mPushnotificationRegister,
                                RemoteConstant.postMethod
                        )
                        val mAuth =
                                RemoteConstant.frameWorkName +
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mAppId,
                                                ""
                                        )!! +
                                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                        if (InternetUtil.isInternetOn()) {
                            //Register Fcm token API
                            nowPlayingViewModel.registerFcm(mAuth, token!!)
                        }
                    }
                })

        if (InternetUtil.isInternetOn()) {
            // TODO : PodCast Analytics Post( Getting data from database ) - API
            nowPlayingViewModel.mAnalyticsData?.nonNull()
                    ?.observe(this, androidx.lifecycle.Observer { mAnalyticsData ->
                        if (!isCallAnalyticsApi) {
                            isCallAnalyticsApi = true
                            if (mAnalyticsData != null && mAnalyticsData.isNotEmpty()) {

                                val mData = PodCastAnalyticsData()
                                val fullData = RemoteConstant.getEncryptedString(
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mAppId,
                                                ""
                                        )!!,
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mApiKey,
                                                ""
                                        )!!,
                                        RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v2/podcast/episode/analytics",
                                        RemoteConstant.postMethod
                                )
                                val mAuth =
                                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                                this, RemoteConstant.mAppId,
                                                ""
                                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                                mData.data = mAnalyticsData
                                nowPlayingViewModel.podcastAnalytcs(mAuth, mData)
                            }
                            nowPlayingViewModel.mAnalyticsData!!.removeObservers(this)
                        }
                    })

            // TODO : Track Analytics Post ( Getting data from database ) - API
            nowPlayingViewModel.mTrackAnalyticsData?.nonNull()?.observe(
                    this, androidx.lifecycle.Observer { mTrackAnalyticsData ->
                if (!isCallTrackAnalyticsApi) {
                    isCallTrackAnalyticsApi = true
                    if (mTrackAnalyticsData != null && mTrackAnalyticsData.isNotEmpty()) {
                        val mDataList: MutableList<TrackAnalyticsDataList> = ArrayList()
                        val fullData = RemoteConstant.getEncryptedString(
                                SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mAppId,
                                        ""
                                )!!,
                                SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mApiKey,
                                        ""
                                )!!,
                                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v2/track/analytics",
                                RemoteConstant.postMethod
                        )
                        val mAuth: String =
                                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                        this, RemoteConstant.mAppId,
                                        ""
                                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                        for (i in mTrackAnalyticsData.indices) {
                            val mData = TrackAnalyticsDataList()
                            mData.action = "1"
                            mData.duration = mTrackAnalyticsData[i].duration
                            mData.track_id = mTrackAnalyticsData[i].episodeId
                            mData.offline = mTrackAnalyticsData[i].offline
                            mDataList.add(mData)
                        }

                        nowPlayingViewModel.trackAnalytics(mAuth, mDataList)
                    }
                    nowPlayingViewModel.mTrackAnalyticsData?.removeObservers(this)
                }
            })
        }

        // TODO: Recent App Icon and color ( Red-mi device)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                val bm =
                        BitmapFactory.decodeResource(resources, R.mipmap.ic_custom_launcher_new_color)
                setTaskDescription(
                        ActivityManager.TaskDescription(
                                null, // Leave the default title.
                                bm,
                                ContextCompat.getColor(this, R.color.colorPrimary)
                        )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        // TODO : Set Up Listener Music Play
        initialPlayList()

        // TODO : Home View Pager
        homeViewPagerAdapter = HomeViewPagerAdapter(supportFragmentManager)
        addPagerFragments()
        view_pager_home.adapter = homeViewPagerAdapter
        tabs.setupWithViewPager(view_pager_home)
        view_pager_home.offscreenPageLimit = 25

        view_pager_home.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                // Check if this is the page you want.
                when (position) {
                    0 -> selectMusicTab()
                    1 -> selectArticleTab()
                    2 -> selectFeedTab()
                    3 -> selectNotificationTab()
                    4 -> selectProfileTab()
                }
            }
        })


        // TODO: Default Add First Position
        addPositiion(0)
        selectMusicTab()


        article_icon_linear.setOnClickListener {
            tabs.getTabAt(1)?.select()
            addPositiion(1)
            AppUtils.hideKeyboard(this@HomeActivity, article_icon_linear)
            EventBus.getDefault().post(
                    StopVideoPlayBack(
                            true
                    )
            )
        }
        music_icon_linear.setOnClickListener {
            /*changeBottomTabColor(
                    music_icon_image_view,
                    article_icon_image_view,
                    feed_icon_image_view,
                    search_icon_image_view,
            )*/
            selectBootomTabItem(0)
            tabs.getTabAt(0)?.select()
            addPositiion(0)
            AppUtils.hideKeyboard(this@HomeActivity, music_icon_linear)
            changeStatusBarColorBlack()

            EventBus.getDefault().post(
                    StopVideoPlayBack(
                            true
                    )
            )
        }
        feed_icon_linear.setOnClickListener {
            val mTabPosition = tabs.selectedTabPosition
            if (mTabPosition == 2) {
                EventBus.getDefault().post(
                        ScrollToTopEvent(
                                "true"
                        )
                )
            }
            tabs.getTabAt(2)?.select()
            addPositiion(2)
            AppUtils.hideKeyboard(this@HomeActivity, feed_icon_linear)
            if (mNightModeStatus) {
                changeStatusBarColorBlack()
            } else {
                changeStatusBarColorWhite()
            }
            EventBus.getDefault().post(
                    StopVideoPlayBack(
                            false
                    )
            )


        }
        profile_icon_linear.setOnClickListener {
            EventBus.getDefault().post(
                    SearchEventPeople(
                            ""
                    )
            )
            tabs.getTabAt(4)?.select()
            addPositiion(4)
            AppUtils.hideKeyboard(this@HomeActivity, profile_icon_linear)
            if (mNightModeStatus) {
                changeStatusBarColorBlack()
            } else {
                changeStatusBarColorWhite()
            }
            EventBus.getDefault().post(
                    StopVideoPlayBack(
                            true
                    )
            )
        }
        notification_icon_linear.setOnClickListener {
            tabs.getTabAt(3)?.select()
            addPositiion(3)
            AppUtils.hideKeyboard(this@HomeActivity, notification_icon_linear)
            if (mNightModeStatus) {
                changeStatusBarColorBlack()
            } else {
                changeStatusBarColorWhite()
            }
            EventBus.getDefault().post(
                    StopVideoPlayBack(
                            true
                    )
            )
        }




        now_playing_icon_linear.setOnClickListener {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                EventBus.getDefault().post(
                        StopVideoPlayBack(
                                true
                        )
                )
                var mediaUrl =
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.mediaUrl
                                ?: ""

                MainFeedAdapter.isMute = true
//                if (MyApplication.playlistManagerRadio != null &&
//                    MyApplication.playlistManagerRadio?.playlistHandler != null &&
//                    MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer != null &&
//                    MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!
//                ) {
//                    view_pager_home.currentItem = 0
//                    EventBus.getDefault().post(
//                        SelectFragmentEvent(
//                            getString(R.string.genre)
//                        )
//                    )
//                    EventBus.getDefault()
//                        .post(
//                            SelectFragmentEventRadio(
//                                getString(R.string.radio)
//                            )
//                        )
//
//                }
                var mImageUrlPass = ""
                if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                }
                if (mediaUrl.contains("station")) {
                    val intent = Intent(this@HomeActivity, CommonFragmentActivity::class.java)
                    intent.putExtra("isFrom", "RadioStation")
                    startActivity(intent)
                } else {
                    if (playlistManager?.currentPosition != null && playlistManager?.currentPosition != -1) {
                        if (
                                playlistManager != null &&
                                playlistManager?.playlistHandler != null &&
                                playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                        ) {
                            val detailIntent =
                                    Intent(this@HomeActivity, NowPlayingActivity::class.java)
                            detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                            startActivity(detailIntent)
                        } else if (playlistManager != null &&
                                playlistManager?.playlistHandler != null &&
                                playlistManager?.playlistHandler?.currentMediaPlayer != null
                        ) {

                            val detailIntent =
                                    Intent(this@HomeActivity, NowPlayingActivity::class.java)
                            detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                            startActivity(detailIntent)
                        } else {
                            launchEmptyPlayList()
                        }
                    } else {
                        launchEmptyPlayList()
                    }

                }
            }
            mLastClickTime = SystemClock.elapsedRealtime()

        }


        // TODO : Handle navigation from pending intent
        if (intent != null && intent?.extras != null) {
            val extras = intent?.extras!!

            when {
                extras.getBoolean("general") ->
                    showImageAlert(extras.getString("mediasourcePath"))
                extras.getBoolean("radio") -> {
                    stopMusicPlay()
                    Handler().postDelayed({
                        view_pager_home.currentItem = 0
                        EventBus.getDefault().post(
                                SelectFragmentEvent(
                                        getString(R.string.genre)
                                )
                        )
                        EventBus.getDefault()
                                .post(
                                        SelectFragmentEventRadio(
                                                getString(R.string.music_video)
                                        )
                                )
                        EventBus.getDefault().post(
                                PlayRadioEvent(
                                        true
                                )
                        )
                    }, 2000)

                }
                extras.getBoolean("radioNotification") -> {
                    Handler().postDelayed({
                        view_pager_home.currentItem = 0
                        EventBus.getDefault().post(
                                SelectFragmentEvent(
                                        getString(R.string.genre)
                                )
                        )
                        EventBus.getDefault()
                                .post(
                                        SelectFragmentEventRadio(
                                                getString(R.string.music_video)
                                        )
                                )
//                        EventBus.getDefault().post(PlayRadioEvent(true))
                    }, 1000)

                }
                extras.getBoolean("track") -> {
                    var trackId = ""
                    var numberId = ""
                    var trackName = ""
                    var coverImage = ""
                    var trackFile = ""
                    var duration = ""
                    var allowOfflineDownload = ""
                    var albumName = ""
                    var albumId = ""
                    var artistName = ""
                    var artistId = ""
                    var genreName = ""
                    var genreId = ""
                    var play_count = ""


                    if (extras.containsKey("trackid")) {
                        trackId = extras.getString("trackid")!!
                    }
                    if (extras.containsKey("play_count")) {
                        play_count = extras.getString("play_count")!!
                    }
                    if (extras.containsKey("numberId")) {
                        numberId = extras.getString("numberId")!!
                    }
                    if (extras.containsKey("trackName")) {
                        trackName = extras.getString("trackName")!!
                    }
                    if (extras.containsKey("coverImage")) {
                        coverImage = extras.getString("coverImage")!!
                    }
                    if (extras.containsKey("trackFile")) {
                        trackFile = extras.getString("trackFile")!!
                    }
                    if (extras.containsKey("duration")) {
                        duration = extras.getString("duration")!!
                    }
                    if (extras.containsKey("allowOfflineDownload")) {
                        allowOfflineDownload = extras.getString("allowOfflineDownload")!!
                    }
                    if (extras.containsKey("albumName")) {
                        albumName = extras.getString("albumName")!!
                    }
                    if (extras.containsKey("albumId")) {
                        albumId = extras.getString("albumId")!!
                    }
                    if (extras.containsKey("artistName")) {
                        artistName = extras.getString("artistName")!!
                    }
                    if (extras.containsKey("artistId")) {
                        artistId = extras.getString("artistId")!!
                    }
                    if (extras.containsKey("genreName")) {
                        genreName = extras.getString("genreName")!!
                    }
                    if (extras.containsKey("genreId")) {
                        genreId = extras.getString("genreId")!!
                    }

                    if (trackFile != "") {
                        val dbPlayList: MutableList<TrackListCommon> = ArrayList()
                        val mPlayList = TrackListCommon()

                        val mediaNew = TrackListCommonMedia()

                        mediaNew.coverFile = coverImage
                        mediaNew.coverImage = coverImage
                        mediaNew.trackFile = trackFile

                        val albumNew = TrackListCommonAlbum()

                        albumNew.albumName = albumName
                        albumNew.id = albumId

                        val artistNew = TrackListCommonArtist()
                        artistNew.id = artistId
                        artistNew.artistName = artistName

                        val genreNew = TrackListCommonGenre()
                        genreNew.id = genreId
                        genreNew.genreName = genreName

                        mPlayList.id = trackId
                        mPlayList.numberId = numberId
                        mPlayList.trackName = trackName
                        mPlayList.media = mediaNew
                        mPlayList.duration = duration
                        mPlayList.allowOfflineDownload = allowOfflineDownload
                        mPlayList.album = albumNew
                        mPlayList.artist = artistNew
                        mPlayList.favorite = "false"
                        mPlayList.genre = genreNew
                        mPlayList.playCount = play_count
                        dbPlayList.add(mPlayList)

                        // TODO : Track Push notification

                        nowPlayingViewModel.createDbPlayListModelNotification(
                                dbPlayList,
                                "Track suggestion"
                        )

                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            val detailIntent = Intent(this, NowPlayingActivity::class.java)
                            detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                            detailIntent.putExtra(
                                    RemoteConstant.extraIndexIsFrom,
                                    "dblistMyFavTrack"
                            )
                            detailIntent.putExtra(RemoteConstant.extraIndexImage, coverImage)
                            startActivity(detailIntent)
                        }, 2000)
                    }

                }
                else -> {
                    var mPos: String? = "0"
                    if (extras.containsKey("mPos")) {
                        mPos = extras.getString("mPos", "0")
                    }
                    var mPosInner: String? = "0"
                    if (extras.containsKey("mPosInner")) {
                        mPosInner = extras.getString("mPosInner", "0")
                    }
//                    if (mPos == "3") {
//                        notificationViewModel.isWebSocketCalled = false
//                        notificationViewModel.isWebSocketOpen = false
//                    }

                    Handler().postDelayed({

                        if (mPos == "3") {
                            val intent = Intent(this, WebSocketChatActivity::class.java)
                            if (extras.containsKey("convId")) {
                                intent.putExtra("convId", extras.getString("convId"))
                            }
                            if (extras.containsKey("mProfileId")) {
                                intent.putExtra("mProfileId", extras.getString("mProfileId"))
                            }
                            if (extras.containsKey("mUserName")) {
                                intent.putExtra("mUserName", extras.getString("mUserName"))
                            }
                            if (extras.containsKey("mUserImage")) {
                                intent.putExtra("mUserImage", extras.getString("mUserImage"))
                            }
                            intent.putExtra("mStatus", extras.getString("1"))
                            startActivity(intent)
                            selectNotificationTab()
                        }
                        view_pager_home?.setCurrentItem(mPos?.toInt()!!, false)

                        if (mPosInner != "0") {
                            EventBus.getDefault().post(
                                    SelectPos(
                                            "1"
                                    )
                            )
                        }

                    }, 1000)

                    Handler().postDelayed({
                        if (mPos == "4") {
                            selectProfileTab()
                        }
                    }, 2000)
                }
            }

            try {
                mImageList = intent?.getParcelableArrayListExtra(IMAGE_LIST)!!

                if (mImageList.size > 0) {
                    // TODO : since we uploading from main feed fragment
                    SharedPrefsUtils.setStringPreference(this, "POST_FROM", "MAIN_FEED")
                    Handler().postDelayed({
                        feed_icon_linear.performClick()
                    }, 1000)
                    if (getMimeType(mImageList[0].photoUri).contains("image")) {
                        val i = Intent(this, CropActivity::class.java)
                        i.putExtra("IS_NEED_TO_ADD", false)
                        i.putExtra(Config.KeyName.TITLE, getString(R.string.upload_photo))
                        i.putExtra(IMAGE_LIST, mImageList)
                        startActivity(i)
                        overridePendingTransition(0, 0)
                    }
                    if (isVideoFile(mImageList[0].photoUri)) {
                        val i = Intent(this, KVideoEditorDemoActivity::class.java)
                        i.putExtra(Config.KeyName.TITLE, getString(R.string.upload_video))
                        i.putExtra(IMAGE_LIST, mImageList)
                        startActivity(i)
                        overridePendingTransition(0, 0)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val mAuth = RemoteConstant.getAuthUser(
                this@HomeActivity,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")!!
        )


        // TODO : Get profile API
        nowPlayingViewModel.getMyProfile(
                mAuth, SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mProfileId, ""
        )!!
        )

        //TODO : Check if Track Hyperlink clicked ( From main feed )
        nowPlayingViewModel.trackDetailsModel.nonNull()
                .observe(this, androidx.lifecycle.Observer
                {
                    if (it?.payload != null && it.payload?.track != null
                    ) {
                        val dbPlayList: MutableList<TrackListCommon> = ArrayList()
                        dbPlayList.add(it.payload?.track!!)

                        nowPlayingViewModel.createDbPlayListModelNotification(
                                dbPlayList, "Track suggestion"
                        )
                        if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                            customProgressDialog?.dismiss()
                        }

                        val detailIntent = Intent(this, NowPlayingActivity::class.java)
                        detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                        detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                        startActivityForResult(detailIntent, 0)
                        overridePendingTransition(0, 0)
                        nowPlayingViewModel.trackDetailsModel.value = null
                    } else {
                        AppUtils.showCommonToast(this, "No track found")
                        if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                            customProgressDialog?.dismiss()
                        }
                    }


                })


        nowPlayingViewModel.podcastResponseModel.observe(this, { podcastResponseModel ->
            if (podcastResponseModel?.payload != null && podcastResponseModel.payload?.episode != null
            ) {
                val episode: MutableList<RadioPodCastEpisode> = ArrayList()
                episode.add(podcastResponseModel.payload?.episode!!)
                nowPlayingViewModel.createDbPlayListModelNotificationPodcast(
                        episode, "Podcast suggestion"
                )

                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
                val detailIntent = Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                startActivity(detailIntent)
                overridePendingTransition(0, 0)
                nowPlayingViewModel.podcastResponseModel.value = null
            } else {
                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }
        })


        //TODO : Show rating dialog
//        showRatingDialog()


        // TODO : Download offlie track pending
        if (nowPlayingViewModel.downloadFalseTrackListModel != null) {
            nowPlayingViewModel.downloadFalseTrackListModel?.nonNull()
                    ?.observe(this, androidx.lifecycle.Observer { trackListCommonDbs ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            this.trackListCommonDbs = trackListCommonDbs
                        }
                    })
        }

        nowPlayingViewModel.downloadTrackListModel?.nonNull()
                ?.observe(this, androidx.lifecycle.Observer
                { trackListCommonDbs ->
                    if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                        this.downloadTrackAllList = trackListCommonDbs
                        // TODO : Delete track
                        if (!isDeleteCAlled) {
                            isDeleteCAlled = true
                            if (InternetUtil.isInternetOn()) {
                                checkDeletedTrack()
                            }

                            observeDeleteTrackData()
                        }
                    }
                })
        val fetchConfiguration = FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(4)
                .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE)
//            .setNotificationManager(object : DefaultFetchNotificationManager(this) {
//                override fun getFetchInstanceForNamespace(namespace: String): Fetch {
//                    return fetch!!
//                }
//            })
                .build()
        fetch = Fetch.getInstance(fetchConfiguration)

//        try {
//            val manager: AudioManager =
//                getSystemService(Context.AUDIO_SERVICE) as AudioManager
//            manager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 1, AudioManager.FLAG_SHOW_UI)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }


        //TODO : Get All uploaded file
        nowPlayingViewModel.mDbChatFileListLiveData?.nonNull()
                ?.observeOnce(this,
                        { mDbChatFileListLiveData ->
                            if (mDbChatFileListLiveData != null && mDbChatFileListLiveData.isNotEmpty()) {
                                val files: MutableList<String> = ArrayList()

                                if (!isFailedApiCalled) {
                                    isFailedApiCalled = true
                                    for (i in mDbChatFileListLiveData.indices) {
                                        if (mDbChatFileListLiveData[i].status == "3" || mDbChatFileListLiveData[i].status == "0") {
                                            files.add(mDbChatFileListLiveData[i].filename!!)
                                        }
                                    }
                                    if (files.size > 0) {
                                        val mFilesList = FailedFiles(files)
                                        nowPlayingViewModel.updateFailedFiles(
                                                "Bearer " + SharedPrefsUtils.getStringPreference(
                                                        this,
                                                        RemoteConstant.mJwt,
                                                        ""
                                                )!!, mFilesList
                                        )
                                    }
                                }
                            } else {
                                isFailedApiCalled = true
                            }
                        })

        viewModelChat.mIsUploading?.nonNull()?.observe(this@HomeActivity,
                androidx.lifecycle.Observer
                { mIsUploading ->
//                mIsUploadingOnProgress = mIsUploading
                })

//        throw  RuntimeException("Test Crash")
        var isCalled = false
        nowPlayingViewModel.contactList?.observe(this, {
            if (it != null && it.isNotEmpty()) {
                if (!isCalled) {
                    isCalled = true
                    val mPostList = ContactModelList(it.toMutableList())
                    println("" + it)
                    if (!InternetUtil.isInternetOn()) {
                    } else {
                        nowPlayingViewModel.postContacts(mPostList)
                    }
                }
            }
        })
        checkUpdate()
        // Before starting an update, register a listener for updates.
        appUpdateManager?.registerListener(listener!!)
    }

     private val listener: InstallStateUpdatedListener? =
        InstallStateUpdatedListener { installState ->
            if (installState.installStatus() == InstallStatus.DOWNLOADED) {
                // After the update is downloaded, show a notification
                // and request user confirmation to restart the app.
                Log.d("TAG", "An update has been downloaded")
//            showSnackBarForCompleteUpdate()
            }
        }



    private fun checkUpdate() {
        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager?.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        Log.d("TAG", "Checking for updates")
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                // Request the update.
                Log.d("TAG", "Update available")
                appUpdateManager?.startUpdateFlowForResult(
                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                    appUpdateInfo,
                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    100
                )
            } else {
                Log.d("TAG", "No Update available")
            }
        }
    }


    //    private var mIsUploadingOnProgress = false
    private fun observeDeleteTrackData() {
        // TODO : Observe remove data
        nowPlayingViewModel.mTrackOfflineDelete?.nonNull()
                ?.observe(this, androidx.lifecycle.Observer { mTrackIds ->
                    try {
                        if (mTrackIds != null && mTrackIds.isNotEmpty()) {

//                        mOfflineTrackDownloaded
//                        mTrackIds

                            val mDeleteTrackFileList: ArrayList<String> = ArrayList()
                            val mDeleteFetchList: ArrayList<Int> = ArrayList()
                            val mDeleteFetchListUrl: ArrayList<String> = ArrayList()
                            downloadTrackAllList?.map {
                                for (j in 0 until mTrackIds.size) {
                                    if (it._id == mTrackIds.get(j)) {
                                        mDeleteTrackFileList.add(it.trackFile)
                                    }
                                }

                            }

                            if (mOfflineTrackDownloaded != null && mOfflineTrackDownloaded?.size!! > 0) {
                                for (i in 0 until mOfflineTrackDownloaded?.size!!) {
                                    for (j in 0 until mDeleteTrackFileList.size) {
                                        if (mOfflineTrackDownloaded?.get(i)?.url
                                                == mDeleteTrackFileList.get(j)
                                        ) {
                                            mDeleteFetchList.add(mOfflineTrackDownloaded?.get(i)?.id!!)
                                            mDeleteFetchListUrl.add(mOfflineTrackDownloaded?.get(i)?.url!!)
                                        }

                                    }

                                }

                            }

                            if (mDeleteFetchList.size > 0) {
                                for (i in 0 until mDeleteFetchList.size) {
                                    fetch?.delete(mDeleteFetchList.get(i))
                                }

                                // TODO : delete if url exist in downloaded track db

                                for (i in 0 until mDeleteFetchListUrl.size) {
                                    nowPlayingViewModel.clearOfflineTrackDb(mDeleteFetchListUrl.get(i))
                                }

                                // TODO : If track deleted clear now playing db
                                nowPlayingViewModel.clearNowPlayingDb()
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })

    }

    private fun checkDeletedTrack() {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            if (downloadTrackAllList != null && downloadTrackAllList?.size!! > 0) {
                //  api/v2/track/offline/verify

                val mTrackList: MutableList<String> = ArrayList()
                for (i in 0 until downloadTrackAllList?.size!!) {
                    mTrackList.add(downloadTrackAllList?.get(i)?._id!!)
                }

                val mTrackIdList = TrackIdList(mTrackList)

                val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                        RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v2/track/offline/verify",
                        RemoteConstant.postMethod
                )
                val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                nowPlayingViewModel.checkDeletedTrack(mAuth, mTrackIdList)

            }
        }, 1000)

    }


    private val GROUP_ID = "listGroup".hashCode()
    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size ?: 0 > 0) {

            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }
    }

    class ContactModelList(var contacts: MutableList<ContactModel>)

    private fun launchEmptyPlayList() {
        val detailIntent = Intent(this@HomeActivity, EmptyPlayingActivity::class.java)
        startActivity(detailIntent)
    }

    private fun observeProfileData() {


        nowPlayingViewModel.profile.nonNull()
                .observe(this, androidx.lifecycle.Observer { mProfile ->
                    if (mProfile != null) {
                        mProfileDta = mProfile

                        if (!isInviteDisplayed) {
                            if (mProfile.inviteLink != null) {
                                if (!isCheckedOpenCount) {
                                    isCheckedOpenCount = true
                                    mShareUrl = mProfile.inviteLink
                                    // TODO : App open count
                                    var mAppOpenCount =
                                            SharedPrefsUtils.getIntegerPreference(
                                                    this,
                                                    RemoteConstant.mAppOpenCount,
                                                    0
                                            )
                                    if (mAppOpenCount == 3) {
                                        val manager = ReviewManagerFactory.create(this)
                                        val request = manager.requestReviewFlow()
                                        request.addOnCompleteListener { request ->
                                            if (request.isSuccessful) {
                                                // We got the ReviewInfo object
                                                val reviewInfo = request.result
                                                val flow = manager.launchReviewFlow(
                                                        this,
                                                        reviewInfo
                                                )
                                                flow.addOnCompleteListener { _ ->
                                                    // The flow has finished. The API does not indicate whether the user
                                                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                                                    // matter the result, we continue our app flow.
                                                    //                                                Toast.makeText(this, "Show ", Toast.LENGTH_SHORT).show()
                                                }
                                            } else {
                                                println("There was some problem, continue regardless of the result.")
                                                //                                            Toast.makeText(activity, "Error ", Toast.LENGTH_SHORT).show()
                                            }
                                        }
                                    }
                                    if (mAppOpenCount == 5) {
                                        mAppOpenCount = 0
                                        SharedPrefsUtils.setIntegerPreference(
                                                this,
                                                RemoteConstant.mAppOpenCount,
                                                mAppOpenCount
                                        )
                                        isInviteDisplayed = true
                                        val intent = Intent(this, InviteActivity::class.java)
                                        intent.putExtra("mShareUrl", mShareUrl)
                                        startActivity(intent)


                                    } else {
                                        mAppOpenCount += 1
                                        SharedPrefsUtils.setIntegerPreference(
                                                this,
                                                RemoteConstant.mAppOpenCount,
                                                mAppOpenCount
                                        )
                                    }
                                }

                            }
                        }

                        val thumbnail = glideRequestManager
                                .load(R.drawable.emptypicture)

                        /*if (mProfile.pimage != null) {
                            glideRequestManager
                                    .load(RemoteConstant.getImageUrlFromWidth(mProfile.pimage, 200))
                                    .error(thumbnail)
                                    .thumbnail(0.1f)
                                    .into(search_icon_image_view)
                            SharedPrefsUtils.setStringPreference(
                                    this@HomeActivity,
                                    RemoteConstant.mUserImage, mProfile.pimage

                            )
                        }*/


                    }

                })

        nowPlayingViewModel.mNotificationCount?.nonNull()
                ?.observe(this, androidx.lifecycle.Observer { notificationCount ->
                    if (notificationCount ?: 0 > 0) {
                        red_dot_profile_image_view.visibility = View.VISIBLE
                        (homeViewPagerAdapter?.getItem(4) as ProfileFragment).hasNotification()
                    } else {
                        (homeViewPagerAdapter?.getItem(4) as ProfileFragment).removeNotification()
                        red_dot_profile_image_view.visibility = View.GONE
                    }
                })

        nowPlayingViewModel.mUnReadMessage?.nonNull()
                ?.observe(this, androidx.lifecycle.Observer { notificationCount ->
                    if (notificationCount.toInt() > 0) {
                        red_dot_new_message_image_view.visibility = View.VISIBLE
                    }
                })

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            setIntent(intent)
            try {
                val extras: Bundle? = intent.extras
                if (extras?.containsKey("radioNotification")!!) {
                    when {
                        extras.getBoolean("radioNotification") -> {
                            Handler().postDelayed({
                                view_pager_home.currentItem = 0
                                EventBus.getDefault().post(
                                        SelectFragmentEvent(
                                                getString(R.string.genre)
                                        )
                                )
                                EventBus.getDefault()
                                        .post(
                                                SelectFragmentEventRadio(
                                                        getString(R.string.music_video)
                                                )
                                        )
                                //                        EventBus.getDefault().post(PlayRadioEvent(true)).
                            }, 1000)
                        }

                    }
                }

                if (extras.containsKey("radio") && extras.getBoolean("radio")) {
                    stopMusicPlay()
                    Handler().postDelayed({
                        view_pager_home.currentItem = 0
                        EventBus.getDefault().post(
                                SelectFragmentEvent(
                                        getString(R.string.genre)
                                )
                        )
                        EventBus.getDefault()
                                .post(
                                        SelectFragmentEventRadio(
                                                getString(R.string.music_video)
                                        )
                                )
                        EventBus.getDefault().post(
                                PlayRadioEvent(
                                        true
                                )
                        )
                    }, 2000)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun tapTarget() {


        val profileLocation = IntArray(2)
        drawer_menu_image_view.getLocationInWindow(profileLocation)
        profileLocation[0] + drawer_menu_image_view.width / 1f
        profileLocation[1] + drawer_menu_image_view.height / 2f
        profileLocation[1] + drawer_menu_image_view.height / 2f
        val profileRadius = 60F

        // first target
        val profileTarget = SimpleTarget.Builder(this@HomeActivity)
                .setPoint(65f, 110f)
                .setShape(Circle(profileRadius))
                .setTitle("Main menu")
                .setDescription("Check out what's playing. \nView your profile.")
                .setOverlayPoint(50f, 200f)
                .build()

        val musicHeight = 100F
        val musicRadius = music_icon_linear.height / 2.toFloat()
        val musicLocation = IntArray(2)
        music_icon_linear.getLocationInWindow(musicLocation)
        val y = musicLocation[1] + music_icon_linear.height / 2

        // third target
        val musicTarget = SimpleTarget.Builder(this@HomeActivity)
                .setPoint(music_icon_linear)
                .setShape(Circle(musicRadius))
                .setTitle("Music")
                .setDescription("Explore the world of indie music")
                .setOverlayPoint(50f, y - musicHeight - 300)
                .build()

        val feedHeight = 100F
        val feedRadius = feed_icon_linear.height / 2.toFloat()
        val feedLocation = IntArray(2)
        feed_icon_linear.getLocationInWindow(feedLocation)
        val yFeed = feedLocation[1] + feed_icon_linear.height / 2

        // third target
        val feedTarget = SimpleTarget.Builder(this@HomeActivity)
                .setPoint(feed_icon_linear)
                .setShape(Circle(feedRadius))
                .setTitle("Feed")
                .setDescription("See what your friends are up to.")
                .setOverlayPoint(50f, yFeed - feedHeight - 300)
                .build()

        val notificationHeight = 100F
        val notificationRadius = notification_icon_linear.height / 2.toFloat()
        val notificationLocation = IntArray(2)
        notification_icon_linear.getLocationInWindow(notificationLocation)
        val ynotification = notificationLocation[1] + notification_icon_linear.height / 2

        // third target
        val notificationTarget = SimpleTarget.Builder(this@HomeActivity)
                .setPoint(notification_icon_linear)
                .setShape(Circle(notificationRadius))
                .setTitle("Notification")
                .setDescription("To check your notifications and Pennys")
                .setOverlayPoint(50f, ynotification - notificationHeight - 300)
                .build()


        // create spotlight
        Spotlight.with(this@HomeActivity)
                .setOverlayColor(R.color.background_shade)
                .setDuration(1000L)
                .setAnimation(DecelerateInterpolator(50f))
                .setTargets(profileTarget, musicTarget, feedTarget, notificationTarget)
//            .setTargets(firstTarget, secondTarget, thirdTarget)
                .setClosedOnTouchedOutside(true)
                .setOnSpotlightStateListener(object : OnSpotlightStateChangedListener {
                    override fun onStarted() {
                    }

                    override fun onEnded() {
                        EventBus.getDefault().post(ShowTapTaget(""))
                    }
                })
                .start()

    }


    private fun checkAndRequestPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                    arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.READ_CONTACTS
                    ), PERMISSIONS_READ_WRITE
            )
        }
        return true
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_READ_WRITE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                println(
                        ""
                )
                enqueueDownloads()
                if (checkReadContactPermission()) {
                    uploadContacts()
                }
            }
        }
    }

    private fun checkReadContactPermission(): Boolean {
        val permission = Manifest.permission.READ_CONTACTS
        val res: Int = checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    private fun uploadContacts() {
        // TODO : Get all contacts

        try {
            GlobalScope.launch(Dispatchers.IO) {
                /*if (contentResolver != null) {
                    val users: MutableList<ContactModel> = mutableListOf()
                    val cursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            null,
                            null,
                            null
                    )
                    if (cursor != null) {


                        while (cursor.moveToNext()) {
                            var time =
                                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID))
                            var name =
                                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                            var phonenumber =
                                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            //                var photo = openDisplayPhoto(cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID))).toString()
                            val id =
                                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID))
                            if (!cursor.isClosed) {
                                val cur1 = contentResolver.query(
                                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                        arrayOf(id), null
                                )
                                var email = ""
                                if (cur1?.moveToNext()!!) {
                                    email =
                                            cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                                }

                                users.add(
                                        ContactModel(
                                                time.toInt(),
                                                name.toString(),
                                                email,
                                                phonenumber.toString(),
                                                "false"
                                        )
                                )
                            }
                        }
                        cursor.close()
                        val hs = HashSet<ContactModel>()
                        hs.addAll(users) // demoArrayList= name of arrayList from which u want to remove duplicates
                        users.clear()
                        users.addAll(hs)
                        nowPlayingViewModel.insetContact(users)
                    }
                }*/

                // TODO : Code Edit AmR
                if (contentResolver != null) {
                    val users: MutableList<ContactModel> = mutableListOf()
                    val cr = contentResolver
                    val cursor: Cursor? = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                    )
                    if (cursor != null && cursor.count > 0 && cursor.moveToFirst()) {
                        do {
                            // get the contact's information
                            val time =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID))
                            var name =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                            val hasPhone = cursor.getInt(
                                cursor.getColumnIndex(
                                    ContactsContract.Contacts.HAS_PHONE_NUMBER
                                )
                            )

                            val id =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID))

                            // get the user's email address
                            var email = ""
                            val ce: Cursor? = cr.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                arrayOf(id),
                                null
                            )
                            if (ce != null && ce.moveToFirst()) {
                                email =
                                    ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                                ce.close()
                            }

                            // get the user's phone number
                            var phone: String? = null
                            if (hasPhone > 0) {
                                val cp: Cursor? = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    arrayOf(id),
                                    null
                                )
                                if (cp != null && cp.moveToFirst()) {
                                    phone =
                                        cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                                    cp.close()
                                }
                            }
                            users.add(
                                ContactModel(
                                    time.toInt(),
                                    name.toString(),
                                    email,
                                    phone.toString(),
                                    "false"
                                )
                            )

                            val hs = HashSet<ContactModel>()
                            hs.addAll(users) // demoArrayList= name of arrayList from which u want to remove duplicates
                            users.clear()
                            users.addAll(hs)
                            nowPlayingViewModel.insetContact(users)

                        } while (cursor.moveToNext())

                        // clean up cursor
                        cursor.close()
                        Log.d("AmR_", "cursor_Close")
                    }
                }
            }
        } catch (e: Exception) {
            Log.d("AmR_", "uploadContacts" + e.toString())
            e.printStackTrace()
        }
    }

    private fun isVideoFile(path: String): Boolean {
        val mimeType: String? = URLConnection.guessContentTypeFromName(path)
        return mimeType != null && mimeType.indexOf("video") == 0

    }

    private fun getMimeType(path: String): String {
        var type = "image" // Default Value
        try {

            val extension = MimeTypeMap.getFileExtensionFromUrl(path)
            if (extension != null) {
                if (MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension) != null) {
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)!!
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "getMimeType_" + e.toString())
        }
        return type
    }

    private fun showImageAlert(mMedia: String?) {

        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.image_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeImageView: ImageView = dialogView.findViewById(R.id.close_image_view)
        val mImageView: ImageView = dialogView.findViewById(R.id.image_view)
        closeImageView.bringToFront()
        glideRequestManager
                .load(mMedia)
                .apply(
                        RequestOptions.placeholderOf(R.drawable.ic_place_holder)
                                .error(R.drawable.ic_place_holder).centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                )
                .into(mImageView)
        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeImageView.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }


    private fun initialPlayList() {
        SharedPrefsUtils.setStringPreference(this@HomeActivity, AppUtils.SELECTED_CATEGORY, "")
        SharedPrefsUtils.setStringPreference(this@HomeActivity, AppUtils.SELECTED_FILTER, "")

        // TODO : Do not change ( Db observe also need when app is reopened )
        nowPlayingViewModel.createDbPlayListModel?.nonNull()
                ?.observe(this, androidx.lifecycle.Observer { playList ->
                    if (!isAddedPlayList) {
                        if (playList?.isNotEmpty()!!) {
                            isAddedPlayList = true
                            mGlobalList = playList as MutableList<PlayListDataClass>

                            playlistManager = MyApplication.playlistManager
                            for (sample in playList) {
                                val mediaItem = MediaItem(sample, true)
                                mediaItems.add(mediaItem)
                            }
                            val mSelectedPosition = SharedPrefsUtils.getIntegerPreference(
                                    this@HomeActivity,
                                    RemoteConstant.SELECTED_POSITION,
                                    0
                            )

                            playlistManager?.currentPosition = mSelectedPosition
                            playlistManager?.setParameters(mediaItems, mSelectedPosition)

                            playlistManager?.play(0, true)
                        }

                    } else {
                        isAddedPlayList = true
                    }

                })
    }

    private fun selectNotificationTab() {
        if (red_dot_new_message_image_view.visibility == View.VISIBLE) {
            red_dot_new_message_image_view.visibility = View.INVISIBLE
        }
        /*changeBottomTabColor(
                article_icon_image_view, search_icon_image_view, feed_icon_image_view,
                music_icon_image_view
        )
        search_icon_border_image_view.visibility = View.GONE*/

        selectBootomTabItem(2)

    }

    fun selectProfileTab() {
        if (red_dot_profile_image_view.visibility == View.VISIBLE) {
            red_dot_profile_image_view.visibility = View.INVISIBLE
        }
       /* changeBottomTabColor(
                search_icon_image_view, feed_icon_image_view, music_icon_image_view,
                article_icon_image_view
        )
        search_icon_border_image_view.visibility = View.VISIBLE*/

        selectBootomTabItem(3)

    }

    private fun selectFeedTab() {

        /*changeBottomTabColor(
                feed_icon_image_view,
                music_icon_image_view,
                article_icon_image_view,
                search_icon_image_view,

        )
        search_icon_border_image_view.visibility = View.GONE*/
        selectBootomTabItem(1)

    }

    private fun selectArticleTab() {
        /*changeBottomTabColor(
                article_icon_image_view, music_icon_image_view, feed_icon_image_view,
                search_icon_image_view
        )
        search_icon_border_image_view.visibility = View.GONE*/

    }

    private fun selectMusicTab() {
        /*changeBottomTabColor(
                music_icon_image_view,
                article_icon_image_view,
                feed_icon_image_view,
                search_icon_image_view
        )
        search_icon_border_image_view.visibility = View.GONE*/
        selectBootomTabItem(0)
    }

    private fun addPositiion(i: Int) {

        if (!mPosition.contains(i)) {
            mPosition.add(i)
        } else {
            mPosition.remove(i)
            mPosition.add(i)
        }
    }

    private fun addPagerFragments() {

        homeViewPagerAdapter?.addFragments(MusicFragment())
        homeViewPagerAdapter?.addFragments(ArticlesFragment())
        homeViewPagerAdapter?.addFragments(FeedFragment())
//        homeViewPagerAdapter?.addFragments(SearchFragment())
//        homeViewPagerAdapter?.addFragments(NotificationFragment())
//        homeViewPagerAdapter?.addFragments(NotificationPennyFragment())
        homeViewPagerAdapter?.addFragments(WebSocketChatFragment())
        homeViewPagerAdapter?.addFragments(ProfileFragment())

    }
    private fun selectBootomTabItem(position: Int) {

        if (position == 0) {
            neuBottomMusic.visibility = View.VISIBLE
            ivBottomMusic.visibility = View.GONE

           neuBottomDashboard.visibility = View.GONE
            ivBottomDashboard.visibility = View.VISIBLE

            neuBottomPlay.setShapeType(0)
            neuBottomPlay.visibility = View.VISIBLE

            neuBottomComment.visibility = View.GONE
            ivBottomComment.visibility = View.VISIBLE

           neuBottomProfile.visibility = View.GONE
            ivBottomProfile.visibility = View.VISIBLE
        } else if (position == 1) {
            neuBottomMusic.visibility = View.GONE
            ivBottomMusic.visibility = View.VISIBLE

            neuBottomDashboard.visibility = View.VISIBLE
            ivBottomDashboard.visibility = View.GONE

            neuBottomPlay.setShapeType(0)
            neuBottomPlay.visibility = View.VISIBLE

            neuBottomComment.visibility = View.GONE
           ivBottomComment.visibility = View.VISIBLE

            neuBottomProfile.visibility = View.GONE
            ivBottomProfile.visibility = View.VISIBLE
        } else if (position == 2) {
            neuBottomMusic.visibility = View.GONE
            ivBottomMusic.visibility = View.VISIBLE

            neuBottomDashboard.visibility = View.GONE
            ivBottomDashboard.visibility = View.VISIBLE

            neuBottomPlay.setShapeType(0)
            neuBottomPlay.visibility = View.VISIBLE

            neuBottomComment.visibility = View.VISIBLE
            ivBottomComment.visibility = View.GONE

            neuBottomProfile.visibility = View.GONE
            ivBottomProfile.visibility = View.VISIBLE
        } else if (position == 3) {
           neuBottomMusic.visibility = View.GONE
            ivBottomMusic.visibility = View.VISIBLE

           neuBottomDashboard.visibility = View.GONE
            ivBottomDashboard.visibility = View.VISIBLE

            neuBottomPlay.setShapeType(0)
            neuBottomPlay.visibility = View.VISIBLE

           neuBottomComment.visibility = View.GONE
            ivBottomComment.visibility = View.VISIBLE

            neuBottomProfile.visibility = View.VISIBLE
            ivBottomProfile.visibility = View.GONE
        }

    }
    private fun changeBottomTabColor(
            image_view_one: ImageView, image_view_two: ImageView,
            image_view_three: ImageView,
            image_view_four: ImageView
    ) {

        try {
            DrawableCompat.setTint(
                    image_view_one.drawable,
                    ContextCompat.getColor(this, R.color.selected_tab_color)
            )
            DrawableCompat.setTint(
                    image_view_two.drawable,
                    ContextCompat.getColor(this, R.color.un_selected_tab_color)
            )
            if (image_view_three.drawable != null) {
                DrawableCompat.setTint(
                        image_view_three.drawable,
                        ContextCompat.getColor(this, R.color.un_selected_tab_color)
                )
            }
            if (image_view_four.drawable != null) {
                DrawableCompat.setTint(
                        image_view_four.drawable,
                        ContextCompat.getColor(this, R.color.un_selected_tab_color)
                )
            }
//            DrawableCompat.setTint(
//                image_view_five.drawable,
//                ContextCompat.getColor(this, R.color.un_selected_tab_color)
//            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onBackPressed() {
        EventBus.getDefault().post(
                StopVideoPlayBack(
                        true
                )
        )
        if (mPosition.size > 1) {
            mPosition.removeAt(mPosition.size - 1)
            tabs.getTabAt(mPosition[mPosition.size - 1])?.select()
        } else {
            if (doubleBackToExitPressedOnce) {
                moveTaskToBack(true)
                return
            }
            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    private fun changeStatusBarColorBlack() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decor = window.decorView
            decor.systemUiVisibility = 0
        }
    }

    private fun changeStatusBarColorWhite() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)

    }

    // UI updates must run on MainThread
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onEvent(event: UpdatePlaylist) {
        if (event.mPlayList != null && event.mPlayList?.size ?: 0 > 0) {
            if ((homeViewPagerAdapter?.getItem(0) is MusicFragment)) {
                (homeViewPagerAdapter?.getItem(0) as MusicFragment).updatePlayList(
                        event.mPlayList?.toMutableList() ?: ArrayList()
                )
            } else {
                println("change")
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ChangeStatusBarColor) {
        if (event.mColor == "white") {
            changeStatusBarColorWhite()
        } else {
            changeStatusBarColorBlack()
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: NotificationEvent) {
        Handler().postDelayed({
            if (event.mCount == "im-desktop-notification") {
                red_dot_new_message_image_view.visibility = View.VISIBLE
            } else if (event.mCount == "penny_new") {
                red_dot_new_message_image_view.visibility = View.VISIBLE
            }
        }, 1000)
        Handler().postDelayed({
            if (
                    event.mCount == "user_post_like" ||
                    event.mCount == "user_post_re_comment" ||
                    event.mCount == "user_post_comment" ||
                    event.mCount == "user_post_mention" ||
                    event.mCount == "content_re_comment" ||
                    event.mCount == "user_follow" ||
                    event.mCount == "user_follow_request" ||
                    event.mCount == "user_follow_accepted" ||
                    event.mCount == "user_mention_post_comment" ||
                    event.mCount == "user_mention_album_comment"

            ) {
                red_dot_profile_image_view.visibility = View.VISIBLE
                // Update notification badge in profile page
                (homeViewPagerAdapter?.getItem(4) as ProfileFragment).hasNotification()
            }
        }, 1000)
    }

    private inner class MusicIntentReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals(Intent.ACTION_HEADSET_PLUG)) {
                when (intent.getIntExtra("state", -1)) {
                    0 -> {
                        Log.d(TAG, "Headset is unplugged")
//                        stopMusicPlay()
                    }
                    1 -> Log.d(TAG, "Headset is plugged")
                    else -> Log.d(TAG, "I have no idea what the headset state is")
                }
            }
        }
    }

    override fun onPause() {
//        unregisterReceiver(myReceiver)
        try {
            val handler = Handler()
            handler.postDelayed({
                if (!MyApplication.isForeground) {
                    viewModelChat.closeSocketSm()
                    viewModelChat.isWebSocketOpen = false
                    viewModelChat.isWebSocketOpenCalled = false
//                    println("--------------->Fragment closed")
                }
            }, 1000)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        super.onPause()
        MyApplication.playlistManagerRadio?.unRegisterPlaylistListener(this)
        playlistManager?.unRegisterPlaylistListener(this)
        playlistManager?.unRegisterProgressListener(this)
    }
    // Checks that the update is not stalled during 'onResume()'.
// However, you should execute this check at all entry points into the app.
    override fun onResume() {
//        val filter = IntentFilter(Intent.ACTION_HEADSET_PLUG)
//        registerReceiver(myReceiver, filter)

        super.onResume()
        playlistManager = MyApplication.playlistManager

        MyApplication.playlistManagerRadio?.registerPlaylistListener(this)
        playlistManager?.registerPlaylistListener(this)
        playlistManager?.registerProgressListener(this)

        //Makes sure to retrieve the current playback information
        updateCurrentPlaybackInformation()


        fetch?.getDownloadsInGroup(GROUP_ID) { downloads ->
            mOfflineTrackDownloaded = ArrayList(downloads)
        }

         appUpdateManager
            ?.appUpdateInfo
            ?.addOnSuccessListener { appUpdateInfo ->
                if (appUpdateInfo.updateAvailability()
                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager?.startUpdateFlowForResult(
                        appUpdateInfo,
                        IMMEDIATE,
                        this,
                        100
                    )
                }
            }

    }

    override fun onPlaylistItemChanged(
            currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        shouldSetDuration = true

        if (currentItem?.thumbnailUrl != null) {
            if (currentItem.thumbnailUrl?.contains("podcast")!!) {
                if (currentItem.mExtraId != null) {
                    checkPlayStatusChange(currentItem.mExtraId.toString())
                }
            } else {
                checkPlayStatusChange(currentItem.id.toString())
            }
        }
        return true
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {

        try {
            when (playbackState) {
                //            PlaybacxkState.STOPPED -> finish()
                //            PlaybackState.RETRIEVING, PlaybackState.PREPARING, PlaybackState.SEEKING -> restartLoading()
                PlaybackState.PLAYING -> {

                    // TODO : Dismiss loader in radio page
                    if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                        if (MyApplication.playlistManagerRadio != null &&
                                MyApplication.playlistManagerRadio?.playlistHandler != null
                        ) {
                            if (MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                                EventBus.getDefault()
                                        .post(EventDismissRadioDialog(""))
                                EventBus.getDefault().post(
                                        PlayRadioEvent(
                                                true
                                        )
                                )
                            }

                        }
                    }

                    EventBus.getDefault().post(StopVideoPlayBack(true))
                    MainFeedAdapter.isMute = true
                    doneLoading(true)
                }
                PlaybackState.ERROR -> {
                    // TODO : Dismiss loader in radio page
                    if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                        if (MyApplication.playlistManagerRadio != null &&
                                MyApplication.playlistManagerRadio?.playlistHandler != null
                        ) {
                            if (MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                                EventBus.getDefault()
                                        .post(EventDismissRadioDialog(""))
                            }
                        }
                    }
                }
                PlaybackState.PAUSED -> {
                    doneLoading(false)
                    // TODO : Dismiss loader in radio page
                    if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                        if (MyApplication.playlistManagerRadio != null &&
                                MyApplication.playlistManagerRadio?.playlistHandler != null
                        ) {
                            if (!MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                                EventBus.getDefault().post(
                                        PlayRadioEvent(
                                                false
                                        )
                                )
                            }
                        }
                    }

                    // TODO :Play pause from notification
                    if (playlistManager != null &&
                            playlistManager?.currentItemChange != null &&
                            playlistManager?.currentItemChange?.currentItem != null
                    ) {
                        val currentItem = playlistManager?.currentItemChange?.currentItem
                        if (currentItem?.thumbnailUrl != null) {
                            if (currentItem.thumbnailUrl?.contains("podcast")!!) {
                                if (currentItem.mExtraId != null) {
                                    checkPlayStatusChange(currentItem.mExtraId.toString())
                                }
                            } else {
                                checkPlayStatusChange(currentItem.id.toString())
                            }
                        }
                    }

                }
                else -> {
                    doneLoading(false)
                }
            }
        } catch (e: Exception) {
            doneLoading(false)
            e.printStackTrace()
        }

        return true
    }

    private fun checkPlayStatusChange(mTrackId: String) {
        if (homeViewPagerAdapter?.getItem(0) != null) {
            val fr = homeViewPagerAdapter?.getItem(0)
            if (fr is MusicFragment) {
                fr.updatePlayPause(mTrackId)
            }
        }
    }

    private fun doneLoading(isPlaying: Boolean) {

        /*if (isPlaying) {
            now_playing_icon_image_view.visibility = View.GONE
            gif_now_playing_bottom_tab.visibility = View.VISIBLE
            gif_now_playing_bottom_tab.playAnimation()
        } else {
            if (gif_now_playing_bottom_tab.isAnimating) {
                gif_now_playing_bottom_tab.pauseAnimation()
            }
            gif_now_playing_bottom_tab.visibility = View.GONE
            now_playing_icon_image_view.visibility = View.VISIBLE
        }*/
    }

    override fun onProgressUpdated(mediaProgress: MediaProgress): Boolean {
        try {
            if (mediaProgress.position.toInt() != 0) {
                val mDuration =
                        playlistManager?.playlistHandler?.currentMediaPlayer?.duration?.toInt()
                val mCurrentPosition =
                        playlistManager?.playlistHandler?.currentMediaPlayer?.currentPosition?.toInt()
                val mProgress = mCurrentPosition!! * 100 / mDuration!!
                    //home_progress_circular?.progress = mProgress.toFloat()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return true
    }

    /**
     * Makes sure to update the UI to the current playback item.
     */
    private fun updateCurrentPlaybackInformation() {
        val itemChange = playlistManager?.currentItemChange
        if (itemChange != null) {
            onPlaylistItemChanged(
                    itemChange.currentItem,
                    itemChange.hasNext,
                    itemChange.hasPrevious
            )
        }

        val currentPlaybackState = playlistManager?.currentPlaybackState
        if (currentPlaybackState !== PlaybackState.STOPPED) {
            onPlaybackStateChanged(currentPlaybackState!!)
        }

        val mediaProgress = playlistManager?.currentProgress
        if (mediaProgress != null) {
            onProgressUpdated(mediaProgress)
        }
    }

    /**
     * Update playlist image
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }

                    if (playListImageView != null) {
                        glideRequestManager
                                .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                                .into(playListImageView!!)
                    }
                }

            }
        }
    }

    override fun onDestroy() {
        if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
            customProgressDialog?.dismiss()
        }
        if (applicationContext != null) {
            Glide.with(applicationContext).pauseRequests()
        }
        nowPlayingViewModel.radioPodCastListModel.removeObservers(this)
        nowPlayingViewModel.playListModel.removeObservers(this)
        nowPlayingViewModel.profile.removeObservers(this)
        nowPlayingViewModel.createDbPlayListModel?.removeObservers(this)

        if (mReceiver != null) {
            unregisterReceiver(mReceiver)
        }
        super.onDestroy()

        viewModelChat.rxWebSocket?.close()?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        { success: Boolean ->
                            print("---->closeSocketSm Cleared" + success)
                        }
                ) { throwable: Throwable ->

                }?.dispose()

    }


    /**
     * Release memory when the UI becomes hidden or when system resources become low.
     * @param level the memory-related event that was raised.
     */
    override fun onTrimMemory(level: Int) {

        // Determine which lifecycle or system event was raised.
        when (level) {

            ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN -> {
                /*
                   Release any UI objects that currently hold memory.

                   The user interface has moved to the background.
                */
            }

            ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE,
            ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW -> {
                println("<><><>TRIM_MEMORY_RUNNING_LOW")
//                System.runFinalization()
//                Runtime.getRuntime().gc()
//                System.gc()
//                recreate()
            }
            ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL -> {
                /*
                   Release any memory that your app doesn't need to run.

                   The device is running low on memory while the app is running.
                   The event raised indicates the severity of the memory-related event.
                   If the event is TRIM_MEMORY_RUNNING_CRITICAL, then the system will
                   begin killing background processes.
                */
            }

            ComponentCallbacks2.TRIM_MEMORY_BACKGROUND,
            ComponentCallbacks2.TRIM_MEMORY_MODERATE,
            ComponentCallbacks2.TRIM_MEMORY_COMPLETE -> {
                /*
                   Release as much memory as the process can.

                   The app is on the LRU list and the system is running low on memory.
                   The event raised indicates where the app sits within the LRU list.
                   If the event is TRIM_MEMORY_COMPLETE, the process will be one of
                   the first to be terminated.
                */
            }

            else -> {
                /*
                  Release any non-critical data structures.

                  The app received an unrecognized memory level value
                  from the system. Treat this as a generic low-memory message.
                */
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onMessageEvent(event: StopMusic) {
        stopMusicPlay()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: NotificationSwipeClose) {
        doneLoading(false)
    }

    fun stopMusicPlay() {

        if (isMyServiceRunning(MediaServiceRadio::class.java)) {

            if (MyApplication.playlistManagerRadio != null &&
                    MyApplication.playlistManagerRadio?.playlistHandler != null
            ) {
                if (MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {

                    MyApplication.playlistManagerRadio?.playlistHandler?.pause(true)
                }
            }
        }
        if (isMyServiceRunning(MediaService::class.java)) {

            if (MyApplication.playlistManager != null && MyApplication.playlistManager?.playlistHandler != null) {
                if (MyApplication.playlistManager?.isPlayingItem(MyApplication.playlistManager?.currentItem)!!) {

                    if (SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mState,
                                    ""
                            ) == "PLAYING"
                    ) {
                        MyApplication.playlistManager?.playlistHandler?.pause(true)
                    }
                }
            }
        }


    }

    /**
     * To get running services
     */
    @SuppressWarnings("deprecation")
    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager: ActivityManager =
                getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun stopMusicPlayFromRadio() {
        // TODO : Mute video when music is played
        if (isMyServiceRunning(MediaService::class.java)) {
            val serviceClass1 = MediaService::class.java
            val intent11 = Intent(application, serviceClass1)
            application.stopService(intent11)
        }
    }

    /**
     * Track options
     */
    fun musicOptionsDialog(mTrackDetails: TrackListCommon) {
        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
                sheetView.findViewById(R.id.add_play_list_image_view)
        val equalizerImageView: ImageView = sheetView.findViewById(R.id.equalizer_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
                sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)

        val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)

        val allowOfflineDownload: String? = mTrackDetails.allowOfflineDownload
        if (allowOfflineDownload == "1") {
            downloadLinear.visibility = View.VISIBLE
        } else {
            downloadLinear.visibility = View.GONE
        }


        downloadLinear.setOnClickListener {
            mBottomSheetDialog.dismiss()
            showRewardedVideo(mTrackDetails)

        }

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE

        val mTrackId: String? = mTrackDetails.id
        var isLiked: String? = mTrackDetails.favorite
        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }


        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                        UpdateTrackEvent(
                                mTrackId!!,
                                isLiked!!
                        )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                        )!!,
                        RemoteConstant.mBaseUrl +
                                RemoteConstant.trackData +
                                RemoteConstant.mSlash + mTrackId +
                                RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                nowPlayingViewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                        UpdateTrackEvent(
                                mTrackId!!,
                                isLiked!!
                        )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "false"
                    }

                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                        )!!,
                        RemoteConstant.mBaseUrl +
                                RemoteConstant.trackData +
                                RemoteConstant.mSlash + mTrackId +
                                RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                nowPlayingViewModel.removeFavourites(mAuth, mTrackId)

            }

        }


        val mAuth =
                RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        nowPlayingViewModel.getPlayList(mAuth)

        nowPlayingViewModel.playListModel.nonNull()
                .observe(this, androidx.lifecycle.Observer {

                    if (it.isNotEmpty()) {
                        mPlayList = it as MutableList
                        playListRecyclerView.adapter =
                                CommonPlayListAdapter(mPlayList, this, false, glideRequestManager)

                        // TODO : Update playlist ( Home page )
                        if (homeViewPagerAdapter?.getItem(0) is MusicFragment) {
                            (homeViewPagerAdapter?.getItem(0) as MusicFragment).updatePlayList(
                                    mPlayList?.toMutableList()!!
                            )
                        }
                    }

                })
        albumLinear.setOnClickListener {
            val intent = Intent(this, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackDetails.album?.id)
            intent.putExtra("title", mTrackDetails.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivity(intent)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackDetails.artist?.id)
            detailIntent.putExtra("mArtistName", mTrackDetails.artist?.artistName)
            startActivity(detailIntent)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
                RecyclerItemClickListener(
                        context = this,
                        recyclerView = playListRecyclerView,
                        mListener = object : RecyclerItemClickListener.ClickListener {
                            override fun onItemClick(view: View, position: Int) {
                                //TODO : Call Add to playlist API
                                if (mPlayList != null && mPlayList!![position].id != null) {
                                    addTrackToPlayList(mTrackDetails.id!!, mPlayList!![position].id!!)
                                }
                                mBottomSheetDialog.dismiss()
                            }

                            override fun onLongItemClick(view: View?, position: Int) {

                            }


                        })
        )

        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(
                    mTrackDetails.id!!,
                    this
            )

//            val shareIntent = Intent(Intent.ACTION_SEND)
//            shareIntent.type = "image/*" // set mime type
//
//            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("https://media-cdn1.socialmob.me/socialmob-images/profile-images/dda0a4dc-2108-44ff-9b7b-aaed8cc72058.jpg")) // set uri
//
//            shareIntent.setPackage("com.instagram.android")
//            startActivity(shareIntent)

            mBottomSheetDialog.dismiss()
        }
    }

    /**
     * Add track to playlist API
     */
    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {

        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                        mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        nowPlayingViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    /**
     * Create playlist
     */
    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val switchButtonPlayList: SwitchButton =
                dialogView.findViewById(R.id.switch_button_play_list)
        val createPlayListTextView: TextView =
                dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
                dialogView.findViewById(R.id.play_list_name_edit_text)

        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                            && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                                "0", playListNameEditText.text.toString(), isPrivate,
                                mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }

                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }
    }

    /**
     * Create playlist API
     */
    private fun createPlayListApi(
            playListId: String, playListName: String, private: String, mLastId: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody =
                    CreatePlayListBody(playListId, playListName, mLastId, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            nowPlayingViewModel.createPlayList(mAuth, createPlayListBody)

        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    /**
     * Play track from hyperlink
     */
    private fun playTrack(mId: String) {
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(this)
        }
        runOnUiThread {
            customProgressDialog?.show()
        }
        val mAuth =
                RemoteConstant.getAuthTrackDetails(this@HomeActivity, mId)
        GlobalScope.launch {
            nowPlayingViewModel.getTrackDetails(mAuth, mId)
        }
    }

    /**
     * User playlist navigation
     */
    private fun gotoPlayList(mId: String) {
        val intent = Intent(this, CommonPlayListActivity::class.java)
        intent.putExtra("mId", mId)
        intent.putExtra("title", "")
        intent.putExtra("imageUrl", "")
        intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
        startActivity(intent)
    }

    /**
     * Hyperlink podcast naviagtion navigation
     */
    private fun gotoPodCastEpisode(mId: String) {

        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(this)
        }
        runOnUiThread {
            customProgressDialog?.show()
        }

        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathPodCastEpisodeDetails
                        + RemoteConstant.mSlash + mId, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        GlobalScope.launch {
            nowPlayingViewModel.getPodCastDetails(mAuth, mId)
        }

    }

    /**
     * User profile navigation
     */
    private fun gotoProfile(mId: String) {
        val intent = Intent(this, UserProfileActivity::class.java)
        intent.putExtra("mProfileId", mId)
        startActivity(intent)
    }

    /**
     * Single artist page navigation
     */
    private fun gotoArtistProfile(mId: String) {
        val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
        detailIntent.putExtra("mArtistId", mId)
        startActivity(detailIntent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    /**
     * Hyperlink click navigation
     */
    private fun goToPost(mId: String, mPostType: String) {
//        Toast.makeText(this, " Got to post", Toast.LENGTH_SHORT).show()
        when (mPostType) {
            "user_text_post" -> {
                val intent = Intent(this, FeedTextDetailsActivity::class.java)
                intent.putExtra("mPostId", mId)
                startActivity(intent)

            }
            "user_video_post" -> {
                val intent = Intent(this, FeedVideoDetailsActivity::class.java)
                intent.putExtra("mPostId", mId)
                startActivity(intent)


            }
            "user_image_post" -> {
                val intent = Intent(this, FeedImageDetailsActivity::class.java)
                intent.putExtra("mPostId", mId)
                startActivity(intent)

            }
        }
    }

    /**
     *  Navigate to podcast episode
     */
    private fun gotoPodCast(mId: String) {
        val intent = Intent(this, CommonPlayListActivity::class.java)
        intent.putExtra("mId", mId)
        intent.putExtra("title", "")
        intent.putExtra("imageUrl", "")
        intent.putExtra("mDescription", "")
        intent.putExtra("mIsFrom", "fromRadio")
        startActivity(intent)
    }

    /**
     * Preload Native ads
     */
    private fun loadNativeAds() {
        val builder = AdLoader.Builder(this, getString(R.string.ad_unit_id))

        adLoader = builder.forUnifiedNativeAd { unifiedNativeAd ->
            // A native ad loaded successfully, check if the ad loader has finished loading
            // and if so, insert the ads into the list.
            mNativeAds?.add(unifiedNativeAd)
            if (!adLoader?.isLoading!!) {
                //                    insertAdsInMenuItems()
                println("loading...")
            }
        }.withAdListener(
                object : AdListener() {
                    override fun onAdFailedToLoad(errorCode: Int) {
                        // A native ad failed to load, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        Log.e(
                                "MainActivity",
                                "The previous native ad failed to load. Attempting to" + " load another."
                        )
                        if (!adLoader?.isLoading!!) {
//                        insertAdsInMenuItems()
                            println("loading...")
                        }
                    }
                }).build()

        // Load the Native ads.

        adLoader?.loadAds(
                AdRequest.Builder().addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0").build(),
                NUMBER_OF_ADS
        )
        RemoteConstant.mNativeAds = mNativeAds
    }

    /**
     * Return preloaded ads to Feed page
     */
    fun getLoadedAds(): MutableList<UnifiedNativeAd>? {
        return mNativeAds
    }

    /**
     * Get data from Hyper link and navigate to coressponding pages
     */
    fun hyperLinkLinkClick(url: String?) {
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(this)
        }
        runOnUiThread {
            customProgressDialog?.show()
        }

        try {
            var mType = ""
            var mId = ""
            var mTarget = ""
            var mPostType = ""

            doAsync {
                val htmlSrc = URL(url).readText()
                val doc = Jsoup.parse(htmlSrc)
                val eMETA = doc.select("meta")

                for (i in 0 until eMETA.size) {
                    val name = eMETA[i].attr("name")
                    val content = eMETA[i].attr("content")

                    if (name == "pagetype") {
                        mType = content
                    }
                    if (name == "source") {
                        mId = content
                    }
                    if (name == "target") {
                        mTarget = content
                    }
                    if (name == "post-type") {
                        mPostType = content
                    }
                }

                if (mTarget == "socialmob") {
                    when (mType) {
                        "music" -> playTrack(mId)
                        "playlist" -> {
                            gotoPlayList(mId)
                        }
                        "podcast-episode" -> {
                            gotoPodCastEpisode(mId)
                        }
                        "podcast" -> {
                            gotoPodCast(mId)
                        }
                        "profile" -> {
                            gotoProfile(mId)
                        }
                        "artist" -> {
                            gotoArtistProfile(mId)
                        }
                        "post" -> {
                            goToPost(mId, mPostType)
                        }
                        else -> {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.data = Uri.parse(url)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                        }
                    }

                } else {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }

                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                customProgressDialog?.dismiss()
            }
        }
    }

    /**
     * Navigation to downloaded tracks
     */
    fun goToOffline() {
        val detailIntent =
                Intent(this@HomeActivity, DownloadListActivity::class.java)
        intent.putExtra("mIsFrom", "")
        startActivity(detailIntent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    /**
     * To delete downloaded tracks
     */
    fun deleteDownloadedTracks() {
        if (mOfflineTrackDownloaded != null && mOfflineTrackDownloaded?.size!! > 0) {
            for (i in 0 until mOfflineTrackDownloaded?.size!!) {
                fetch?.delete(mOfflineTrackDownloaded?.get(i)?.id!!)
            }
        }

    }

    /**
     * Preload Rewarded ads
     */
    private fun loadRewardedAd() {
        if (!(::mRewardedAd.isInitialized) || !mRewardedAd.isLoaded) {
            mIsLoading = true
            mRewardedAd = RewardedAd(this, getString(R.string.ad_unit_id_reward))
            mRewardedAd.loadAd(
                    AdRequest.Builder()
                            .build(),
                    object : RewardedAdLoadCallback() {
                        override fun onRewardedAdLoaded() {
                            mIsLoading = false
                        }

                        override fun onRewardedAdFailedToLoad(errorCode: Int) {
                            mIsLoading = false
                        }
                    }
            )
        }
    }

    private fun showRewardedVideo(mTrackDetails: TrackListCommon) {
        if (mRewardedAd.isLoaded) {

            mRewardedAd.show(
                    this,
                    object : RewardedAdCallback() {
                        override fun onUserEarnedReward(
                                rewardItem: RewardItem
                        ) {
                            isRewardEarned = true

                        }

                        override fun onRewardedAdClosed() {
                            isRewardEarned = if (!isRewardEarned) {

                                Toast.makeText(
                                        this@HomeActivity,
                                        "Finish the ad to download the track",
                                        Toast.LENGTH_LONG
                                ).show()

                                false
                            } else {
                                startDownload(mTrackDetails)
                                false
                            }
                            loadRewardedAd()
                        }

                        override fun onRewardedAdFailedToShow(errorCode: Int) {
                            Toast.makeText(
                                    this@HomeActivity,
                                    "Something went wrong please try again",
                                    Toast.LENGTH_LONG
                            ).show()
                        }

                        override fun onRewardedAdOpened() {
                        }
                    }
            )
        } else {
            startDownload(mTrackDetails)
        }
    }

    /**
     *  Adding firebase analytics when track download
     */
    private fun startDownload(mTrackDetails: TrackListCommon) {

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val paramsNotification = Bundle()
        paramsNotification.putString(
                "user_id",
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        )
        paramsNotification.putString("track_id", mTrackDetails.id)
        paramsNotification.putString("event_type", "track_download")
        firebaseAnalytics.logEvent("offline_track_download", paramsNotification)


        nowPlayingViewModel.addToDownload(mTrackDetails)
        Toast.makeText(this, "Track added to queue", Toast.LENGTH_SHORT).show()

        // TODO : Resume track download
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            enqueueDownloads()
        }, 1000)
    }


    /**
     * Unregister receiver
     */
    private fun registerBroadcastReceiver() {
        mReceiver = getReceiver()
        val theFilter = IntentFilter()
        /** System Defined Broadcast  */
        theFilter.addAction(Intent.ACTION_SCREEN_ON)
        theFilter.addAction(Intent.ACTION_SCREEN_OFF)
        theFilter.addAction(Intent.ACTION_USER_PRESENT)

        registerReceiver(mReceiver, theFilter)
    }


    /**
     * Register brodcast receiver for screen off/on
     * Disconnect Web Socket when phone is locked
     * When socket is connected user didn't receive any notification
     */
    private fun getReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val strAction = intent.action
                val myKM: KeyguardManager =
                        context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                if (strAction == Intent.ACTION_USER_PRESENT ||
                        strAction == Intent.ACTION_SCREEN_OFF || strAction == Intent.ACTION_SCREEN_ON
                )
                    if (myKM.inKeyguardRestrictedInputMode()) {
                        println("---->Screen off " + "LOCKED")
                        viewModelChat.closeSocketSm()
                        viewModelChat.isWebSocketOpen = false
                        viewModelChat.isWebSocketOpenCalled = false
                    } else {
                        println("---->Screen off " + "UNLOCKED")
                    }
            }
        }
    }

    /**
     * Chat notification dot
     */
    fun updateMessageIcon() {
        if (red_dot_new_message_image_view.visibility == View.VISIBLE) {
            red_dot_new_message_image_view.visibility = View.INVISIBLE
        }
    }

    override fun onPauseDownload(id: Int) {
        fetch?.pause(id)
    }

    override fun onResumeDownload(id: Int) {
        fetch?.resume(id)
    }

    override fun onRemoveDownload(id: Int, mUrl: String) {
        fetch?.remove(id)
    }

    override fun onRetryDownload(id: Int) {
        fetch?.retry(id)

    }

    fun removeStationAnimation() {
        // TODO : Remove music station animation
        if (homeViewPagerAdapter?.getItem(0) is MusicFragment) {
            (homeViewPagerAdapter?.getItem(0) as MusicFragment).removeStationAnimation()
        }
    }

}