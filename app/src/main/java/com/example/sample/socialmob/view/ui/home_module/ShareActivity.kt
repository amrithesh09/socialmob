package com.example.sample.socialmob.view.ui.home_module

import android.Manifest
import android.content.ClipDescription
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.view.ui.write_new_post.Config
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.IOUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import kotlinx.android.synthetic.main.share_activity.*
import java.io.*
import java.net.URLConnection

/**
 * Handled shared from gallery or other apps
 * Receive data from clipboard and navigate
 * We can only choose 1 video or 5 images in a time
 */
class ShareActivity : AppCompatActivity() {
    private val PERMISSIONS_READ_WRITE = 123
    private var mImageList: ArrayList<GalleryData> = ArrayList()
    private var customProgressDialog: CustomProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change Hide status bar
        hideStatusBar()
        setContentView(R.layout.share_activity)

        customProgressDialog = CustomProgressDialog(this)
        progress_bar_share.visibility = View.VISIBLE
        customProgressDialog?.show()

        try {
            if (intent?.clipData != null) {
                // TODO : Check permission read and write storage
                if (checkAndRequestPermissions()) {
                    // carry on the normal flow, as the case of  permissions  granted.

                    val clip = intent?.clipData!!
                    if (clip.itemCount > 0) {
                        // TODO : Only accept when we have only 5 files 
                        if (clip.itemCount <= 5) {
                            for (i in 0 until clip.itemCount) {
                                val mGalleryData = GalleryData()
                                val mPath = getFilePath(this, clip.getItemAt(i).uri)
                                if (mPath != null) {
                                    mGalleryData.photoUri = mPath
                                    mImageList.add(mGalleryData)
                                } else {
                                    val mInnerPath =
                                        getFilePathFromURI(
                                            this,
                                            clip.getItemAt(i).uri,
                                            clip.description
                                        )
                                    mGalleryData.photoUri = mInnerPath!!
                                    mImageList.add(mGalleryData)
                                }
                            }
                            if (mImageList.size > 0) {
                                if (getMimeType(mImageList[0].photoUri).contains("image")) {

                                    val i = Intent(this, HomeActivity::class.java)
                                    i.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    i.putExtra(Config.KeyName.IMAGE_LIST, mImageList)
                                    startActivity(i)
                                    overridePendingTransition(0, 0)
                                    finish()
                                    finishAffinity()

                                } else if (getMimeType(mImageList[0].photoUri).contains("")) {
                                    Handler().postDelayed({
                                        try {
                                            //TODO : Need to write image when the uri is an reference
                                            val mImage = getImageUrlWithAuthority(
                                                this,
                                                clip.getItemAt(0).uri
                                            )
                                            mImageList.clear()
                                            val mGalleryData = GalleryData()
                                            val mPath = getFilePath(this, Uri.parse(mImage))
                                            if (mPath != null) {
                                                mGalleryData.photoUri = mPath
                                                mImageList.add(mGalleryData)
                                            }

                                            val i = Intent(this, HomeActivity::class.java)
                                            i.flags =
                                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            i.putExtra(Config.KeyName.IMAGE_LIST, mImageList)
                                            startActivity(i)
                                            overridePendingTransition(0, 0)
                                            finish()
                                            finishAffinity()
                                        } catch (e: Exception) {
                                            e.printStackTrace()
//                                            fileSizeLarge("Cannot share file ")
                                        }
                                    }, 2000)
                                }
                                if (isVideoFile(mImageList[0].photoUri)) {

                                    val file = File(mImageList[0].photoUri)
                                    val fileSize = file.length()
                                    val fileInMb = fileSizeInMb(fileSize)
                                    val retriever = MediaMetadataRetriever()
                                    //use one of overloaded setDataSource() functions to set your data source
                                    //TODO : Only uploading video less than 3 min
                                    retriever.setDataSource(this, Uri.fromFile(file))
                                    val time =
                                        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                                    val timeInMillisec = time?.toLong()

                                    retriever.release()


                                    val min = timeInMillisec!! / 60000


                                    if (min <= 2) {
                                        val i = Intent(this, HomeActivity::class.java)
                                        i.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        i.putExtra(Config.KeyName.IMAGE_LIST, mImageList)
                                        startActivity(i)
                                        overridePendingTransition(0, 0)
                                        finish()
                                        finishAffinity()
                                    } else {
                                        fileSizeLarge("File length exceeded the maximum 3 minute permitted")
                                    }

                                }

                            } else {
                                Handler().postDelayed({
                                    AppUtils.showCommonToast(this, "Error occur")
                                    finish()
                                }, 1000)
                            }

                        } else {

                            Handler().postDelayed({
                                fileSizeLarge("You can share only five files")
                            }, 1000)
                        }

                    }
                } else {
                    checkAndRequestPermissions()
                    fileSizeLarge("You have no permission to share files")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            fileSizeLarge("Cannot share file ")
        }
    }

    /**
     * To write temp file to external storage
     */
    private fun getImageUrlWithAuthority(context: Context, uri: Uri): String? {
        var `is`: InputStream? = null

        try {
            if (uri.authority != null) {
                try {
                    `is` = context.contentResolver.openInputStream(uri)
                    val bmp = BitmapFactory.decodeStream(`is`)
                    return writeToTempImageAndGetPathUri(context, bmp).toString()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } finally {
                    try {
                        `is`?.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    private fun writeToTempImageAndGetPathUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }


    private fun fileSizeLarge(s: String) {
        Handler().postDelayed({
            Toast.makeText(
                this, s,
                Toast.LENGTH_SHORT
            ).show()
        }, 3000)
        Handler().postDelayed({
            val i = Intent(this, HomeActivity::class.java)
            startActivity(i)
            finish()
            finishAffinity()
        }, 4000)
    }

    /**
     * To get real path URI
     */
    private fun getFilePathFromURI(
        context: Context,
        contentUri: Uri,
        description: ClipDescription
    ): String? {
        val fileName = getFileName(contentUri)
        if (!TextUtils.isEmpty(fileName)) {

            val file = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
            if (!file.exists()) {
                file.mkdir()
            }

            val copyFile: File
            copyFile = if (description.toString().contains("video")) {
                File(file, "/" + System.currentTimeMillis() + "socialmob.mp4")
            } else {
                File(file, "/" + System.currentTimeMillis() + "socialmob.jpg")
            }
            copy(context, contentUri, copyFile)
            return copyFile.absolutePath
        }
        return null
    }

    private fun getFileName(uri: Uri?): String? {
        if (uri == null) return null
        var fileName: String? = null
        val path = uri.path
        val cut = path!!.lastIndexOf('/')
        if (cut != -1) {
            fileName = path.substring(cut + 1)
        }
        return fileName
    }

    private fun copy(context: Context, srcUri: Uri, dstFile: File) {
        try {
            val inputStream = context.contentResolver.openInputStream(srcUri) ?: return
            val outputStream = FileOutputStream(dstFile)
            IOUtils.copy(inputStream, outputStream)
            inputStream.close()
            outputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1011) {
            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("mPos", "2")
            intent.putExtra("mPosInner", "1")
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
    }

    /**
     * Check whether selected file is video or not
     */
    private fun isVideoFile(path: String): Boolean {
        val mimeType = URLConnection.guessContentTypeFromName(path)
        return mimeType != null && mimeType.indexOf("video") == 0
    }

    /**
     *To get MIME type of the file ( checking selected file is image/video )
     */
    private fun getMimeType(path: String): String {
        var type = "image" // Default Value
        try {
            val extension = MimeTypeMap.getFileExtensionFromUrl(path)
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)!!
            }
        } catch (e: Exception) {
            return ""
        }
        return type
    }

    private fun checkAndRequestPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), PERMISSIONS_READ_WRITE
            )
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_READ_WRITE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) println(
                ""
            )
        }
    }

    //    @Throws(URISyntaxException::class)
    /**
     * TODO : To get file path
     */
    private fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                this, uri)
        ) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {


            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor? = null
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    private fun hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window
            .setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
    }

    /**
     * Get file size
     */
    private fun fileSizeInMb(sizeLong: Long): Long {
        return sizeLong / (1024 * 1024)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customProgressDialog != null && customProgressDialog!!.isShowing)
            customProgressDialog!!.dismiss()

    }

}
