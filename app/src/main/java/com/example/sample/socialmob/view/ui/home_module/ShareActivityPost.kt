package com.example.sample.socialmob.view.ui.home_module

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedTextDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedVideoDetailsActivity

/**
 * Launching share activity for common for shared link click
 * Navigate to corresponding page
 */
class ShareActivityPost : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change Hide status bar
        hideStatusBar()
        setContentView(R.layout.share_activity)


        // TODO : To Handle Share Track
        if (intent?.data != null) {
            val data: Uri? = intent?.data
            // TODO : Check last segment and navigate to corresponding
            if (data!!.lastPathSegment != null) {
                when {
                    data.queryParameterNames.toString().contains("image") -> {
                        val intent =
                            Intent(this@ShareActivityPost, FeedImageDetailsActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        intent.putExtra("mPostId", data.lastPathSegment)
                        startActivity(intent)
                        finish()
                    }
                    data.queryParameterNames.toString().contains("video") -> {
                        val intent =
                            Intent(this@ShareActivityPost, FeedVideoDetailsActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        intent.putExtra("mPostId", data.lastPathSegment)
                        startActivity(intent)
                        this.finish()
                    }
                    data.queryParameterNames.toString().contains("text") -> {
                        val intent = Intent(this@ShareActivityPost, FeedTextDetailsActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        intent.putExtra("mPostId", data.lastPathSegment)
                        startActivity(intent)
                        this.finish()
                    }
                }
            }
        }
    }

    private fun hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window
            .setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
    }
}
