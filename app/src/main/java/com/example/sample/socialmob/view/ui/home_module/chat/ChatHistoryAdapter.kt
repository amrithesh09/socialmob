package com.example.sample.socialmob.view.ui.home_module.chat


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AddChatItemBinding
import com.example.sample.socialmob.databinding.ChatHistoryItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.chat.ConvList
import com.example.sample.socialmob.view.utils.BaseViewHolder
import kotlinx.android.synthetic.main.options_menu_article_edit.view.*
import org.jetbrains.anko.layoutInflater

class ChatHistoryAdapter(
    private val mContext: Context?,
    private val mMessageItemClick: MessageItemClick?,
    private val mProfileId: String?,
    private val deleteConversation: DeleteConversation?,
    private val isFromSearch: Boolean,
    private val mNightModeStatus: Boolean,
    private var glideRequestManager: RequestManager
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var mLastClickTime: Long = 0
    private val conversationList = mutableListOf<ConvList>()

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_MORE = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_MORE -> bindMore(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    private fun bindMore(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: AddChatItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.add_chat_item, parent, false
        )
        return AddChatViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ChatHistoryItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.chat_history_item, parent, false
        )
        return ChatHistoryViewHolder(binding)
    }

    override fun getItemCount() = conversationList.size

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(conversationList[holder.adapterPosition])
    }

    inner class ChatHistoryViewHolder(val binding: ChatHistoryItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(mData: Any?) {
            val mConversationList = mData as ConvList
            binding?.chatHistoryViewModel = mConversationList
            binding?.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                val isRead = mConversationList.last_message?.status
                // Read status 3
                if (isRead == "3") {
                    binding?.unReadLine?.visibility = View.GONE
                    binding?.unReadDot?.visibility = View.GONE
                    binding?.lastMsgNormal?.visibility = View.VISIBLE
                    binding?.lastMsgBold?.visibility = View.GONE
                } else {
                    if (mConversationList.last_message != null &&
                        mConversationList.last_message?.message_sender != null &&
                        mConversationList.last_message?.message_sender?._id != null &&
                        mConversationList.last_message?.message_sender?._id != mProfileId
                    ) {
                        binding?.unReadLine?.visibility = View.VISIBLE
                        binding?.unReadDot?.visibility = View.VISIBLE
                        binding?.lastMsgNormal?.visibility = View.GONE
                        binding?.lastMsgBold?.visibility = View.VISIBLE
                    } else {
                        binding?.lastMsgNormal?.visibility = View.VISIBLE
                        binding?.lastMsgBold?.visibility = View.GONE
                        binding?.unReadDot?.visibility = View.GONE
                        binding?.unReadLine?.visibility = View.GONE
                    }
                }
            }
            if (isFromSearch) {
                binding?.chatOptions?.visibility = View.GONE
            } else {
                binding?.chatOptions?.visibility = View.VISIBLE
            }
            /**
             * Run time font setting issue
             */
            if (adapterPosition != RecyclerView.NO_POSITION) {
                val mDataNew = mConversationList.last_message
                when {
                    mDataNew?.msg_type.equals("text") -> {
                        binding?.lastMsgNormal?.text = setTextMarquee10(mDataNew?.text?.trim())
                        binding?.lastMsgBold?.text = setTextMarquee10(mDataNew?.text?.trim())
                        binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                        binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                    }
                    mDataNew?.msg_type.equals("ping") -> {
                        binding?.lastMsgNormal?.text = "Sent a ping"
                        binding?.lastMsgBold?.text = "Sent a ping"
                        binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                        binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                    }
                    mDataNew?.msg_type.equals("video") -> {
                        binding?.lastMsgNormal?.text = "Video"
                        binding?.lastMsgBold?.text = "Video"
                        binding?.lastMsgNormal?.compoundDrawablePadding = 10
                        binding?.lastMsgBold?.compoundDrawablePadding = 10

                        if (mNightModeStatus) {
                            binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_video_night, 0, 0, 0
                            )
                            binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_video_night, 0, 0, 0
                            )
                        } else {
                            binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_video_day, 0, 0, 0
                            )
                            binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_video_day, 0, 0, 0
                            )
                        }
                    }
                    mDataNew?.msg_type.equals("image") -> {
                        binding?.lastMsgNormal?.text = "Image"
                        binding?.lastMsgBold?.text = "Image"

                        binding?.lastMsgNormal?.compoundDrawablePadding = 10
                        binding?.lastMsgBold?.compoundDrawablePadding = 10

                        if (mNightModeStatus) {
                            binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_chat_image_night, 0, 0, 0
                            )
                            binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_chat_image_night, 0, 0, 0
                            )
                        } else {
                            binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_chat_image_day, 0, 0, 0
                            )
                            binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_chat_image_day, 0, 0, 0
                            )

                        }
                    }
                    else -> {
                        binding?.lastMsgNormal?.text = setTextMarquee10(mDataNew?.text?.trim())
                        binding?.lastMsgBold?.text = setTextMarquee10(mDataNew?.text?.trim())

                        binding?.lastMsgNormal?.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                        binding?.lastMsgBold?.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                    }
                }
            }

            binding?.chatOptions?.setOnClickListener {
                val viewGroup: ViewGroup? = null
                val view: View = LayoutInflater.from(mContext)
                    .inflate(R.layout.options_menu_article_edit, viewGroup)
                val mQQPop = PopupWindow(
                    view,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                mQQPop.animationStyle = R.style.RightTopPopAnim
                mQQPop.isFocusable = true
                mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mQQPop.isOutsideTouchable = true
                mQQPop.showAsDropDown(binding.chatOptions, -180, -25)
                view.edit_text_view.visibility = View.GONE

                view.delete_text_view.setOnClickListener {
                    val dialogBuilder: AlertDialog.Builder =
                        this.let { AlertDialog.Builder(mContext) }
                    val inflater: LayoutInflater = mContext!!.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton = dialogView.findViewById<View>(R.id.cancel_button) as Button
                    val okButton = dialogView.findViewById<View>(R.id.ok_button) as Button

                    val b: AlertDialog? = dialogBuilder.create()
                    b?.show()
                    okButton.setOnClickListener {
                        if (adapterPosition != RecyclerView.NO_POSITION) {
                            deleteConversation?.onDeleteConversation(
                                adapterPosition,
                                mConversationList.conv_id,
                                mConversationList.profile?._id
                            )
                        }
                        mQQPop.dismiss()
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                    cancelButton.setOnClickListener {
                        mQQPop.dismiss()
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                }
            }
            binding?.chatUserDetailsLinear?.setOnClickListener {
                binding.chatProfileImageView.performClick()
            }

            binding?.chatUserDetailsLinear?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return@setOnClickListener
                    } else {
                        mMessageItemClick?.messageItemClick(
                            mConversationList.conv_id,
                            mConversationList.profile?._id,
                            mConversationList.profile?.username,
                            mConversationList.profile?.pimage,
                            mConversationList.last_message?.status,
                            mConversationList.last_message?.message_sender?._id,
                            binding.chatProfileImageView
                        )
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }
            }
            // If profile is null need load default image
            if (mConversationList.profile?.pimage != null) {
                glideRequestManager
                    .load(mConversationList.profile?.pimage)
                    .thumbnail(0.1f)
                    .apply(
                        RequestOptions.placeholderOf(R.drawable.emptypicture)
                            .error(R.drawable.emptypicture)
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                    )
                    .into(binding?.chatProfileImageView!!)
            } else {
                glideRequestManager
                    .load(R.drawable.emptypicture)
                    .thumbnail(0.1f)
                    .into(binding?.chatProfileImageView!!)
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    inner class AddChatViewHolder(val binding: AddChatItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            conversationList[position].conv_id == "-1" -> {
                ITEM_MORE
            }
            conversationList[position].conv_id != "" -> {
                ITEM_DATA
            }
            else -> {
                ITEM_DATA
            }
        }
    }

    interface MessageItemClick {
        fun messageItemClick(
            convId: String?, mProfileId: String?, mUserName: String?,
            mUserImage: String?, mStatus: String?, mLastMessageUser: String?, imageView: ImageView?
        )
    }

    interface DeleteConversation {
        fun onDeleteConversation(
            position: Int, convId: String?, mProfileId: String?
        )
    }

    fun swap(mutableList: MutableList<ConvList>) {
        val diffCallback =
            SingleConversationListDiffUtils(
                this.conversationList,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.conversationList.clear()
        this.conversationList.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    /**
     *
     */
    private fun setTextMarquee10(value: String?): String {
        var mTitle = ""
        if (value != null && value != "") {
            mTitle = if (value.length > 30) {
                value.substring(0, 30) + "..."
            } else {
                value
            }
        }
        return mTitle
    }
}

private class SingleConversationListDiffUtils(
    private val oldList: MutableList<ConvList>, private val newList: MutableList<ConvList>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].last_message?.status == newList[newItemPosition].last_message?.status
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].last_message?.timestamp == newList[newItemPosition].last_message?.timestamp

    }
}