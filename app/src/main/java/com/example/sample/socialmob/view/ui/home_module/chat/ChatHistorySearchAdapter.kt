package com.example.sample.socialmob.view.ui.home_module.chat


import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AddChatItemBinding
import com.example.sample.socialmob.databinding.ChatHistoryItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.chat.ConvList
import com.example.sample.socialmob.view.utils.BaseViewHolder
import kotlinx.android.synthetic.main.bottom_menu.*
import kotlinx.android.synthetic.main.options_menu_article_edit.view.*
import org.jetbrains.anko.layoutInflater

class ChatHistorySearchAdapter(
    private val mContext: Context?,
    private val mMessageItemClick: MessageItemClick?,
    private val mProfileId: String?,
    private val deleteConversation: DeleteConversation?,
    private val isFromSearch: Boolean,
    private var glideRequestManager: RequestManager
) :
    ListAdapter<ConvList, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    private var mLastClickTime: Long = 0

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_MORE = 3

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<ConvList>() {
                override fun areItemsTheSame(
                    oldItem: ConvList,
                    newItem: ConvList
                ): Boolean {
                    return oldItem.profile?.username == newItem.profile?.username
                }

                override fun areContentsTheSame(
                    oldItem: ConvList,
                    newItem: ConvList
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_MORE -> bindMore(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    private fun bindMore(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: AddChatItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.add_chat_item, parent, false
        )
        return AddChatViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ChatHistoryItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.chat_history_item, parent, false
        )
        return ChatHistoryViewHolder(binding)
    }

//    override fun getItemCount() = siz

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }

    inner class ChatHistoryViewHolder(val binding: ChatHistoryItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(mData: Any?) {
            binding?.chatHistoryViewModel = getItem(adapterPosition)
            binding?.executePendingBindings()

            val isRead = getItem(adapterPosition).last_message?.status
            // Last message status == 3 ( User seen last messagec)
            if (isRead == "3") {
                binding?.unReadLine?.visibility = View.GONE
                binding?.unReadDot?.visibility = View.GONE
                binding?.lastMsgNormal?.visibility = View.VISIBLE
                binding?.lastMsgBold?.visibility = View.GONE
            } else {
                if (getItem(adapterPosition).last_message != null &&
                    getItem(adapterPosition).last_message?.message_sender != null &&
                    getItem(adapterPosition).last_message?.message_sender?._id != null &&
                    getItem(adapterPosition).last_message?.message_sender?._id != mProfileId
                ) {
                    binding?.unReadLine?.visibility = View.VISIBLE
                    binding?.unReadDot?.visibility = View.VISIBLE
                    binding?.lastMsgNormal?.visibility = View.GONE
                    binding?.lastMsgBold?.visibility = View.VISIBLE
//                    binding?.lastMsgBold?.text =
//                        conversationList[adapterPosition].unreadMessageCount
                } else {
                    binding?.lastMsgNormal?.visibility = View.VISIBLE
                    binding?.lastMsgBold?.visibility = View.GONE
                    binding?.unReadDot?.visibility = View.GONE
                    binding?.unReadLine?.visibility = View.GONE
                }
            }
            if (isFromSearch) {
                binding?.chatOptions?.visibility = View.GONE
            } else {
                binding?.chatOptions?.visibility = View.VISIBLE
            }


            if (getItem(adapterPosition).profile?.pimage != null) {
                glideRequestManager
                    .load(getItem(adapterPosition).profile?.pimage)
                    .apply(
                        RequestOptions.placeholderOf(R.drawable.emptypicture)
                            .error(R.drawable.emptypicture)
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                    )
                    .thumbnail(0.1f)
                    .into(binding?.chatProfileImageView!!)
            } else {
                glideRequestManager
                    .load(R.drawable.emptypicture)
                    .into(binding?.chatProfileImageView!!)
            }

            val mDataNew = getItem(adapterPosition).last_message
            when {
                mDataNew?.msg_type.equals("text") -> {
                    binding.lastMsgNormal.text = setTextMarquee10(mDataNew?.text?.trim())
                    binding.lastMsgBold.text = setTextMarquee10(mDataNew?.text?.trim())
                }
                mDataNew?.msg_type.equals("ping") -> {
                    binding.lastMsgNormal.text = "Sent a ping"
                    binding.lastMsgBold.text = "Sent a ping"
                }
                else -> {
                    binding.lastMsgNormal.text = setTextMarquee10(mDataNew?.text?.trim())
                    binding.lastMsgBold.text = setTextMarquee10(mDataNew?.text?.trim())
                }
            }




            binding.chatOptions.setOnClickListener {

                if (adapterPosition != RecyclerView.NO_POSITION) {
                    val viewGroup: ViewGroup? = null
                    val view: View = LayoutInflater.from(mContext)
                        .inflate(R.layout.options_menu_article_edit, viewGroup)
                    val mQQPop = PopupWindow(
                        view,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    mQQPop.animationStyle = R.style.RightTopPopAnim
                    mQQPop.isFocusable = true
                    mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    mQQPop.isOutsideTouchable = true
                    mQQPop.showAsDropDown(binding.chatOptions, -180, -25)
                    view.edit_text_view.visibility = View.GONE

                    view.delete_text_view.setOnClickListener {
                        val dialogBuilder: AlertDialog.Builder =
                            this.let { AlertDialog.Builder(mContext) }
                        val inflater: LayoutInflater = mContext?.layoutInflater!!
                        val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton =
                            dialogView.findViewById<View>(R.id.cancel_button) as Button
                        val okButton = dialogView.findViewById<View>(R.id.ok_button) as Button

                        val b: AlertDialog? = dialogBuilder.create()
                        b?.show()
                        okButton.setOnClickListener {
                            deleteConversation?.onDeleteConversation(
                                adapterPosition, getItem(adapterPosition).conv_id
                            )
                            mQQPop.dismiss()
                            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            mQQPop.dismiss()
                            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                    }
                }
            }
            binding.chatUserDetailsLinear.setOnClickListener {
                binding.chatProfileImageView.performClick()
            }

            binding.chatUserDetailsLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return@setOnClickListener
                    } else {
                        mMessageItemClick?.messageItemClick(
                            getItem(adapterPosition).conv_id,
                            getItem(adapterPosition).profile?._id,
                            getItem(adapterPosition).profile?.username,
                            getItem(adapterPosition).profile?.pimage,
                            getItem(adapterPosition).last_message?.status,
                            getItem(adapterPosition).last_message?.message_sender?._id
                        )
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class AddChatViewHolder(val binding: AddChatItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    override fun getItemViewType(position: Int): Int {

        return when {
            getItem(position).conv_id == "-1" -> {
                ITEM_MORE
            }
            getItem(position).conv_id != "" -> {
                ITEM_DATA
            }
            else -> {
                ITEM_DATA
            }
        }
    }

    interface MessageItemClick {
        fun messageItemClick(
            convId: String?, mProfileId: String?, mUserName: String?,
            mUserImage: String?, mStatus: String?, mLastMessageUser: String?
        )
    }

    interface DeleteConversation {
        fun onDeleteConversation(
            position: Int, convId: String?
        )
    }

//    fun swap(mutableList: MutableList<ConvList>) {
//        val diffCallback =
//            SingleConversationListDiffUtil(
//                this.conversationList,
//                mutableList
//            )
//        val diffResult = DiffUtil.calculateDiff(diffCallback)
//
//        this.conversationList.clear()
//        this.conversationList.addAll(mutableList)
//        diffResult.dispatchUpdatesTo(this)
//    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun setTextMarquee10(value: String?): String {
        var mTitle = ""
        if (value != null && value != "") {

            mTitle = if (value.length > 20) {
                value.substring(0, 20) + "..."
            } else {
                value
            }

        }
        return mTitle
    }
}


//private class SingleConversationListDiffUtil(
//    private val oldList: MutableList<ConvList>,
//    private val newList: MutableList<ConvList>
//) : DiffUtil.Callback() {
//
//    override fun getOldListSize() = oldList.size
//
//    override fun getNewListSize() = newList.size
//
//    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldList[oldItemPosition].profile?.pimage?:"" == newList[newItemPosition].profile?.pimage ?:"" ||
//                oldList[oldItemPosition].last_message?.status == newList[newItemPosition].last_message?.status
//
//    }
//
//    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldList[oldItemPosition].profile?.pimage?:"" == newList[newItemPosition].profile?.pimage ?:"" ||
//                oldList[oldItemPosition].last_message?.status == newList[newItemPosition].last_message?.status
//    }
//
//}