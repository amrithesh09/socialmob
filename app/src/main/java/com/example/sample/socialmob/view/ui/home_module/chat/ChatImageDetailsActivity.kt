package com.example.sample.socialmob.view.ui.home_module.chat

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ChatImageDetailsActivityBinding
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ChatImageDetailsActivity : BaseCommonActivity() {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: ChatImageDetailsActivityBinding? = null
    private var image: String? = null
    private var time: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.chat_image_details_activity)

        val i: Intent = intent
        if (i.hasExtra("image") && i.getStringExtra("image") != null) {
            image = i.getStringExtra("image")!!
        }
        if (i.hasExtra("time") && i.getStringExtra("time") != null) {
            time = i.getStringExtra("time") ?: ""
            //binding?.chatTime?.text = time
            binding?.chatTime?.let { loadDateConvertTimeStamp(it, time) }

        }

        if (image != null) {

            val circularProgressDrawable = CircularProgressDrawable(this)
            circularProgressDrawable.strokeWidth = 10f
            circularProgressDrawable.centerRadius = 60f
            circularProgressDrawable.setColorSchemeColors(Color.parseColor("#1e90ff"))
            circularProgressDrawable.start()

            glideRequestManager.load(image).thumbnail(0.1f)
                .apply(
                    RequestOptions.placeholderOf(circularProgressDrawable)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                ).fitCenter()
                .into(binding?.chatListImage!!)
        }

        binding?.backImageView?.setOnClickListener {
            onBackPressed()
        }
    }

   private fun loadDateConvertTimeStamp(textView: TextView, s: String?) {
        try {
            if (s != null && s != "") {
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val output = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val d = sdf.parse(time)
                val formattedTime = output.format(d)
                textView.text = formattedTime
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = intent
        setResult(1501, intent)
    }
}
