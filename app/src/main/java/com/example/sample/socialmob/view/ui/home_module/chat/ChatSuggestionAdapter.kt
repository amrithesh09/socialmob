package com.example.sample.socialmob.view.ui.home_module.chat

import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AddChatItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.RecentChatItemBinding
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder

class ChatSuggestionAdapter(
    private val mContext: Context?,
    private val mMessageItemClick: ChatHistoryAdapter.MessageItemClick?,
    private var glideRequestManager: RequestManager
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var mLastClickTime: Long = 0
    private val conversationList = mutableListOf<SearchProfileResponseModelPayloadProfile>()

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_MORE = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_MORE -> bindMore(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    private fun bindMore(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: AddChatItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.add_chat_item, parent, false
        )
        return AddChatViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: RecentChatItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recent_chat_item, parent, false
        )
        return RecentChatViewHolder(binding)
    }

    override fun getItemCount() = conversationList.size

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(conversationList[holder.adapterPosition])
    }

    inner class RecentChatViewHolder(val binding: RecentChatItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(`object`: Any?) {
            val mConversationListItem = `object` as SearchProfileResponseModelPayloadProfile
            binding?.suggestionChatList = mConversationListItem
            binding?.executePendingBindings()

            glideRequestManager.load(mConversationListItem.pimage)
                .thumbnail(0.1f)
                .into(binding?.chatProfileImageView!!)
            binding.chatProfileImageView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return@setOnClickListener
                    } else {
                        mMessageItemClick?.messageItemClick(
                            mConversationListItem.conv_id,
                            mConversationListItem._id,
                            mConversationListItem.username,
                            mConversationListItem.pimage,
                            "",
                            "",
                            binding.chatProfileImageView
                        )
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class AddChatViewHolder(val binding: AddChatItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.imageRelative.setOnClickListener {
                val intent = Intent(mContext, CommonFragmentActivity::class.java)
                intent.putExtra("isFrom", "CreateNewChat")
                mContext?.startActivity(intent)
            }

        }

    }

    override fun getItemViewType(position: Int): Int {

        return when {
            conversationList[position].conv_id == "-1" -> {
                ITEM_MORE
            }
            conversationList[position].conv_id != "" -> {
                ITEM_DATA
            }
            else -> {
                ITEM_LOADING
            }
        }
    }

    fun swap(mutableList: MutableList<SearchProfileResponseModelPayloadProfile>) {
        val diffCallback =
            RecentConversationListDiffUtils(
                this.conversationList,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.conversationList.clear()
        this.conversationList.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}


class RecentConversationListDiffUtils(
    private val oldList: MutableList<SearchProfileResponseModelPayloadProfile>,
    private val newList: MutableList<SearchProfileResponseModelPayloadProfile>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition]._id == newList[newItemPosition]._id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition]._id == newList[newItemPosition]._id

    }

}