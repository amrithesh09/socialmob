package com.example.sample.socialmob.view.ui.home_module.chat

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ActivityChatVideoDetailsBinding
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.bvpkotlin.BetterVideoPlayer
import com.example.sample.socialmob.view.utils.bvpkotlin.VideoCallback

class ChatVideoDetailsActivity : BaseCommonActivity() {
    private var binding: ActivityChatVideoDetailsBinding? = null
    private var video: String? = null
    private var isonPause: Boolean = false
    private var time: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_video_details)

        val i: Intent = intent
        if (i.hasExtra("video") && i.getStringExtra("video") != null) {
            video = i.getStringExtra("video")!!
        }

        if (i.hasExtra("time") && i.getStringExtra("time") != null) {
            time = i.getStringExtra("time") ?: ""
            binding?.chatTime?.text = time
        }
        binding?.backImageView?.setOnClickListener {
            onBackPressed()
        }

        binding?.betterVideoPlayer?.setHideControlsOnPlay(true)
        if (video != null) {
            binding?.betterVideoPlayer?.setSource(Uri.parse(video))
        }
        binding?.betterVideoPlayer?.setCallback(object : VideoCallback {
            override fun onStarted(player: BetterVideoPlayer) {
                println("Started")
            }

            override fun onPaused(player: BetterVideoPlayer) {
                println("Paused")
                player.pause()
            }

            override fun onPreparing(player: BetterVideoPlayer) {
                println(">>>Preparing")
            }

            override fun onPrepared(player: BetterVideoPlayer) {
                if (!isonPause) {
                    binding?.betterVideoPlayer?.setAutoPlay(true)
                    binding?.betterVideoPlayer?.start()
                }
                println(">>>Prepared")
            }

            override fun onBuffering(percent: Int) {
                println(">>>Buffering $percent")
            }

            override fun onError(player: BetterVideoPlayer, e: Exception) {
                println(">>>Error " + e.message)
            }

            override fun onCompletion(player: BetterVideoPlayer) {
                println(">>>Completed")
            }

            override fun onToggleControls(
                player: BetterVideoPlayer,
                isShowing: Boolean
            ) {

            }
        })
    }

    override fun onPause() {
        binding?.betterVideoPlayer?.pause()
        isonPause = true
        super.onPause()
    }


    override fun onResume() {
        super.onResume()
        binding?.betterVideoPlayer?.start()
        isonPause = false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = intent
        setResult(1501, intent)
    }

}