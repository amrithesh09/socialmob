package com.example.sample.socialmob.view.ui.home_module.chat

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.media.ThumbnailUtils
import android.os.*
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.OvershootInterpolator
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.anilokcun.uwmediapicker.UwMediaPicker
import com.anilokcun.uwmediapicker.model.UwMediaPickerMediaModel
import com.anilokcun.uwmediapicker.model.UwMediaPickerMediaType
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.OneToOneActivtyBinding
import com.example.sample.socialmob.model.chat.*
import com.example.sample.socialmob.repository.utils.*
import com.example.sample.socialmob.view.ui.home_module.chat.model.*
import com.example.sample.socialmob.view.ui.home_module.search.SearchTrackFragment
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.ConnectedSocket
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.view.utils.stickyheader.StickyHeaderDecoration
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tomergoldst.tooltips.ToolTip
import com.tomergoldst.tooltips.ToolTipsManager
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers
import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

@AndroidEntryPoint
class WebSocketChatActivity : BaseCommonActivity(),
    WebSocketChatAdapter.ChatOptions, ToolTipsManager.TipListener,
    ProgressRequestBody.UploadCallbacks, ProgressBodyImage.UploadCallbacksImage {
    @Inject
    lateinit var glideRequestManager: RequestManager


    private var mLastId = "0"
    private var mLastClickTime: Long = 0
    private var imageSize = 0
    private val PERMISSION_REQUEST_CODE_PICK_IMAGE_VIDEO = 11
    private var convId = ""
    private var mProfileId = ""
    private var mUserName = ""
    private var mUserImage = ""
    private var mLastMessageUser = ""
    private var nJwt = ""

    private var isAddedLoader: Boolean = false
    private var isAddedMessage: Boolean = false
    private var isNewActivityLaunched: Boolean = false
    private var isOffline: Boolean = false

    private var mOfflineMessageList: List<MessageListItem>? = ArrayList()
    private val allSelectedMediaPaths by lazy { arrayListOf<String>() }
    private val viewModelChat: WebSocketChatViewModel by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }
    private var mLayoutManager: WrapContentLinearLayoutManagerChat? = null
    private var rxTask: Disposable? = null
    private var messageAdapter: WebSocketChatAdapter? = null
    private var binding: OneToOneActivtyBinding? = null
    private var mChatHistoryList: MutableList<MsgList>? = ArrayList()
    private var mStatus: ResultResponse.Status? = null


    override fun onBackPressed() {
        mLastId = ""
        viewModelChat.getSingleChatResponseModel.postValue(null)
        AppUtils.hideKeyboard(this@WebSocketChatActivity, binding?.chatRecyclerView!!)
        super.onBackPressed()

    }
    /**
     * Update message list back press from image/video detail page
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1501) {
            if (!isAddedLoader && mChatHistoryList?.size ?: 0 > 0) {
                val mNewListSize = mChatHistoryList?.size!!
                // Check for new messages
                callApi(mChatHistoryList?.get(mNewListSize - 1)?.msg_id ?: "0")
                val mutableList: MutableList<MsgList> = ArrayList()
                val mList = MsgList()
                mutableList.add(mList)
                mList.msg_id = ""
                mChatHistoryList?.addAll(mutableList)
                binding?.chatRecyclerView?.post {
                    refreshList()
                }
                isAddedLoader = true
            }
        }
    }


    override fun onDestroy() {
        if (rxTask != null) {
            rxTask?.dispose()
        }
        viewModelChat.getSingleChatResponseModel.postValue(null)
        viewModelChat.getSingleChatResponseModel.value = null
        mLastId = ""
        SharedPrefsUtils.setStringPreference(this, RemoteConstant.mConnectedUser, "")
        super.onDestroy()

    }

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.one_to_one_activty)
        nJwt = "Bearer " + SharedPrefsUtils.getStringPreference(this, RemoteConstant.mJwt, "")!!

        viewModelChat.getSingleChatResponseModel.postValue(null)
        viewModelChat.getSingleChatResponseModel.value = null
        viewModelChat.newMessage?.removeObservers(this)


        val i: Intent = intent
        if (i.hasExtra("convId") && i.getStringExtra("convId") != null) {
            convId = i.getStringExtra("convId")!!
        }
        if (i.hasExtra("mProfileId") && i.getStringExtra("mProfileId") != null) {
            mProfileId = i.getStringExtra("mProfileId")!!
            SharedPrefsUtils.setStringPreference(this, RemoteConstant.mConnectedUser, mProfileId)
        }
        if (i.hasExtra("mUserName") && i.getStringExtra("mUserName") != null) {
            mUserName = i.getStringExtra("mUserName")!!
        }
        if (i.hasExtra("mLastMessageUser") && i.getStringExtra("mLastMessageUser") != null) {
            mLastMessageUser = i.getStringExtra("mLastMessageUser")!!
        }
        if (i.hasExtra("mUserImage") && i.getStringExtra("mUserImage") != null) {
            mUserImage = i.getStringExtra("mUserImage")!!
            binding?.userImage = mUserImage
        }
        if (i.hasExtra("mStatus") && i.getStringExtra("mStatus") != null) {
            if (i.getStringExtra("mStatus") != "") {
                val mStatus = i.getStringExtra("mStatus")
                if (mLastMessageUser != SharedPrefsUtils.getStringPreference(
                        this@WebSocketChatActivity, RemoteConstant.mProfileId, ""
                    )!!
                ) {
                    // TODO : Update seen status API
                    if (mStatus != "3") {
                        callApiMarkAllAsRead()
                    }
                }
            }
        }
        binding?.chatRecyclerView?.addOnLayoutChangeListener { view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom) {
                binding?.chatRecyclerView?.scrollBy(0, oldBottom - bottom)
            }
        }
        binding?.profileImage = SharedPrefsUtils.getStringPreference(
            this@WebSocketChatActivity, RemoteConstant.mUserImage, ""
        )!!


        binding?.attachmentImageView?.setOnClickListener {
            imagePickerAlert()
        }
        binding?.userImageView?.setOnClickListener {
            binding?.titleTextView?.performClick()
        }
        binding?.titleTextView?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(this, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mProfileId)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.executePendingBindings()

        binding?.titleTextView?.text = mUserName
        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }

        binding?.uploadBackImageView?.setOnClickListener {
            onBackPressed()
        }

        binding?.progressLinear?.visibility = View.VISIBLE
        binding?.noInternetLinear?.visibility = View.GONE
        binding?.noDataLinear?.visibility = View.GONE
        binding?.dataLinear?.visibility = View.GONE

        mLayoutManager =
            WrapContentLinearLayoutManagerChat(
                this, LinearLayoutManager.VERTICAL, true
            )
        binding?.chatRecyclerView?.layoutManager = mLayoutManager
        binding?.chatRecyclerView?.isNestedScrollingEnabled = true
        binding?.chatRecyclerView?.addOnScrollListener(CustomScrollListener())


        val mOwnProfileId =
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        messageAdapter =
            WebSocketChatAdapter(
                mOwnProfileId, this, this@WebSocketChatActivity, viewModelChat,
                glideRequestManager
            )
        binding?.chatRecyclerView?.itemAnimator = ScaleInBottomAnimator(OvershootInterpolator(1f))
        binding?.chatRecyclerView?.itemAnimator = null
        binding?.chatRecyclerView?.addItemDecoration(StickyHeaderDecoration())
        binding?.chatRecyclerView?.adapter = messageAdapter
        refreshList()

        binding?.messageEditText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.length == 0) {
                    binding?.commentSendImageView?.setColorFilter(Color.parseColor("#9198a3"))
                } else {
                    binding?.commentSendImageView?.setColorFilter(Color.parseColor("#1e90ff"))
                }
            }
        })
        binding?.commentSendLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">>>Double tap")
            } else {
                if (binding?.messageEditText?.text?.isNotEmpty()!!) {
                    if (InternetUtil.isInternetOn()) {
                        if (viewModelChat.isWebSocketOpen && !viewModelChat.isWebSocketOpenCalled) {

                            if (binding?.imageNewMessage?.visibility == View.VISIBLE) {
                                binding?.imageNewMessage?.visibility = View.GONE
                            }
                            val mSendMessage = SendMessage()
                            val mMsgPayload = MsgPayload()

                            mSendMessage.user = mProfileId
                            mSendMessage.type = "message"

                            mMsgPayload.user = mProfileId
                            mMsgPayload.msgType = "text"
                            mMsgPayload.text =
                                binding?.messageEditText?.text.toString().trim()
                            mMsgPayload.timestamp = System.currentTimeMillis()
                            mSendMessage.msgPayload = mMsgPayload

                            val mGson = Gson()
                            val mMessage =
                                mGson.toJson(mSendMessage)
                            viewModelChat.sendMessageSm(mMessage)
                            binding?.messageEditText?.setText("")

                            val mMsgListItem: MutableList<MsgList> = ArrayList()
                            val mMsgList = MsgList()
                            val mMessageSender = MessageSender()

                            mMessageSender._id =
                                SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mProfileId, ""
                                )
                            mMessageSender.username =
                                SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mUserName, ""
                                )
                            mMessageSender.name =
                                SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mUserName, ""
                                )
                            mMessageSender.pimage =
                                SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mUserImage, ""
                                )

                            mMsgList.msg_id = mMsgPayload.timestamp?.toString()
                            mMsgList.conv_id = ""
                            mMsgList.msg_type = mMsgPayload.msgType
                            mMsgList.text = mMsgPayload.text
                            mMsgList.timestamp = mMsgPayload.timestamp.toString()
                            mMsgList.status = "-1"
                            mMsgList.message_sender = mMessageSender

                            mMsgListItem.add(mMsgList)
                            mChatHistoryList?.addAll(0, mMsgListItem)

                            binding?.chatRecyclerView?.post {
                                refreshList()
                            }
                        } else {
                            Toast.makeText(
                                this@WebSocketChatActivity,
                                "Waiting for connection", Toast.LENGTH_SHORT
                            ).show()

                            if (!viewModelChat.isWebSocketOpen && !viewModelChat.isWebSocketOpenCalled) {
                                viewModelChat.connectToSocketSm()
                            }
                        }
                    }
                }
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }

        /**
         * Added an message you are offline when there is no internet connectivity
         */
        InternetUtil.observe(this, { internet ->
            if (internet) {
                binding?.socketStateTv?.setBackgroundColor(Color.parseColor("#006400"))
                binding?.socketStateTv?.text = "Welcome back..."
                val handlerWelcome = Handler(Looper.getMainLooper())
                handlerWelcome.postDelayed({
                    binding?.socketStateTv?.visibility = View.GONE
                    if (viewModelChat.isWebSocketCalled) {
                        viewModelChat.isWebSocketCalled = false
                    }
                }, 1000)
                if (isOffline) {
                    if (mChatHistoryList?.size == 0) {
                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            onResume()
                        }, 2000)
                    }
                    isOffline = false
                }

            } else {
                viewModelChat.isWebSocketOpen = false
                isOffline = true
                binding?.socketStateTv?.visibility = View.VISIBLE
                binding?.socketStateTv?.setBackgroundColor(Color.parseColor("#f40505"))
                binding?.socketStateTv?.text = "You are offline..."
            }
        })
        binding?.imageNewMessage?.bringToFront()
        binding?.imageNewMessage?.setOnClickListener {
            binding?.chatRecyclerView?.post {
                binding?.chatRecyclerView?.smoothScrollToPosition(0)
                mLayoutManager?.scrollToPositionWithOffset(0, 0)
            }
            binding?.imageNewMessage?.visibility = View.GONE
        }
        binding?.commentProfileImageView?.setOnClickListener { it ->
            if (InternetUtil.isInternetOn()) {
                if (viewModelChat.isWebSocketOpen && !viewModelChat.isWebSocketOpenCalled) {

                    if (binding?.imageNewMessage?.visibility == View.VISIBLE) {
                        binding?.imageNewMessage?.visibility = View.GONE
                    }
                    val mSendMessage = SendMessage()
                    val mMsgPayload = MsgPayload()

                    mSendMessage.user = mProfileId
                    mSendMessage.type = "message"

                    mMsgPayload.user = mProfileId
                    mMsgPayload.msgType = "ping"
                    mMsgPayload.text = binding?.messageEditText?.text.toString().trim()
                    mMsgPayload.timestamp = System.currentTimeMillis()
                    mSendMessage.msgPayload = mMsgPayload

                    val mGson = Gson()
                    val mMessage =
                        mGson.toJson(mSendMessage)
                    viewModelChat.sendMessageSm(mMessage)
                    binding?.messageEditText?.setText("")

                    val mMsgList = MsgList()
                    val mMessageSender = MessageSender()

                    mMessageSender._id =
                        SharedPrefsUtils.getStringPreference(
                            this,
                            RemoteConstant.mProfileId,
                            ""
                        )
                    mMessageSender.username =
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mUserName, "")
                    mMessageSender.name =
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mUserName, "")
                    mMessageSender.pimage =
                        SharedPrefsUtils.getStringPreference(
                            this,
                            RemoteConstant.mUserImage,
                            ""
                        )

                    mMsgList.msg_id = mMsgPayload.timestamp?.toString()
                    mMsgList.conv_id = ""
                    mMsgList.msg_type = mMsgPayload.msgType
                    mMsgList.text = mMsgPayload.text
                    mMsgList.timestamp = mMsgPayload.timestamp?.toString()
                    mMsgList.status = "1"
                    mMsgList.message_sender = mMessageSender
                    val mMsgListItem: MutableList<MsgList> = ArrayList()
                    mMsgListItem.add(mMsgList)
                    //TODO : Add message locally
                    mChatHistoryList?.addAll(0, mMsgListItem)

                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        binding?.chatRecyclerView?.post {
                            binding?.chatRecyclerView?.smoothScrollToPosition(0)
                            mLayoutManager?.scrollToPositionWithOffset(0, 0)
                        }
                    }, 1000)

                } else {
                    // Socket is not connected
                    viewModelChat.connectToSocketSm()
                    Toast.makeText(this, "Waiting for connection", Toast.LENGTH_SHORT).show()
                }
            }
        }

        viewModelChat.mIsUploading?.nonNull()?.observe(this, Observer { mIsUploading ->
            when {
                mIsUploading -> {
                    binding?.attachmentImageView?.visibility = View.GONE
                }
                else -> {
                    binding?.attachmentImageView?.visibility = View.VISIBLE
                }
            }
        })


        //TODO : Observes offline message
        offlineMessage()
        observeSingleChatHistory()
        // Update upload progress
        viewModelChat.uploadProgress?.nonNull()?.observe(this, Observer { uploadProgress ->
            if (uploadProgress != null) {
                if (messageAdapter != null) {
                    try {
                        mChatHistoryList?.map {
                            if (it.timestamp == uploadProgress.mTimeStamp) {
                                val mPos: Int = mChatHistoryList?.indexOf(it) ?: 0

                                val viewHolder =
                                    binding?.chatRecyclerView?.findViewHolderForAdapterPosition(mPos)
                                if (viewHolder is WebSocketChatAdapter.MessageRightVideo) {
                                    binding?.chatRecyclerView?.post {
                                        viewHolder.binding.progressBar.progress =
                                            uploadProgress.mProgress.toInt()
                                    }

                                } else if (viewHolder is WebSocketChatAdapter.MessageRightImage) {
                                    binding?.chatRecyclerView?.post {
                                        viewHolder.binding.progressBar.progress =
                                            uploadProgress.mProgress.toInt()
                                    }
                                }
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

        })    }

    private fun imagePickerAlert() {
        isNewActivityLaunched = true
        requestToOpenImagePicker(
            PERMISSION_REQUEST_CODE_PICK_IMAGE_VIDEO,
            UwMediaPicker.GalleryMode.ImageAndVideoGallery,
            ::openUwMediaPicker
        )
    }

    /** Opens UwMediaPicker for select images*/
    private fun openUwMediaPicker(galleryMode: UwMediaPicker.GalleryMode) {
        try {
            val gridColumnCount = 3
            val maxSelectableMediaCount = 5
            val maxWidth = 1280F
            val maxHeight = 720F
            val quality = 85
            UwMediaPicker.with(this)
                .setGalleryMode(galleryMode)
                .setGridColumnCount(gridColumnCount)
                .setMaxSelectableMediaCount(maxSelectableMediaCount)
                .setLightStatusBar(true)
                .enableImageCompression(false)
                .setCompressionMaxWidth(maxWidth)
                .setCompressionMaxHeight(maxHeight)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setCompressionQuality(quality)
                .setCompressedFileDestinationPath("${application.getExternalFilesDir(null)!!.path}/Pictures")
                .launch(::onMediaSelected)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "openUwMediaPicker" + e.toString())
        }
    }

    /** Request to open Image Picker Intent and Handle the permissions */
    private fun requestToOpenImagePicker(
        requestCode: Int,
        galleryMode: UwMediaPicker.GalleryMode,
        openMediaPickerFunc: (UwMediaPicker.GalleryMode) -> Unit
    ) {
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                // First time
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), requestCode
                )
            } else {
                // Not first time
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), requestCode
                )
            }
        } else {
            // Permission has already been granted
            openMediaPickerFunc.invoke(galleryMode)
        }
    }


    private fun onMediaSelected(selectedMediaList: List<UwMediaPickerMediaModel>?) {
        if (selectedMediaList != null) {
            val selectedMediaPathList = selectedMediaList.map { it.mediaPath }
            allSelectedMediaPaths.addAll(selectedMediaPathList)
            var videoThumbImageFile: File? = null
            var videoSize = 0
            imageSize = 0
            val imageList: MutableList<String> = ArrayList()
            var videoUrl = ""
            selectedMediaList.map {
                if (it.mediaType == UwMediaPickerMediaType.VIDEO) {
                    videoSize += 1
                    videoUrl = it.mediaPath

                } else if (it.mediaType == UwMediaPickerMediaType.IMAGE) {
                    imageSize += 1
                    imageList.add(it.mediaPath)
                }
            }

            if (videoUrl != "") {
                runOnUiThread {
                //TODO : Create thumbnail for vide
                    try {
                        val bitmap =
                            ThumbnailUtils.createVideoThumbnail(
                                videoUrl,
                                MediaStore.Video.Thumbnails.MINI_KIND
                            )

                        val file =
                            File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
                        if (!file.exists()) {
                            file.mkdir()
                        }
                        videoThumbImageFile =
                            File(file, "socialmob" + System.currentTimeMillis() / 1000 + ".jpg")


                        val os = FileOutputStream(videoThumbImageFile!!)
                        bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, os)
                        os.flush()
                        os.close()
                    } catch (e: Exception) {
                        Toast.makeText(this, "Error when generating thumbnail", Toast.LENGTH_SHORT)
                            .show()
                        e.printStackTrace()
                    }
                }
            }


            // TODO : Upload image and video
            if (allSelectedMediaPaths.size > 0) {
                viewModelChat.preSignedUrlChat(
                    imageSize, nJwt, imageList, videoUrl, mProfileId,
                    getThumbnail(videoThumbImageFile), "", this,
                    this, ""
                )
            }

            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                binding?.chatRecyclerView?.post {
                    binding?.chatRecyclerView?.smoothScrollToPosition(0)
                    mLayoutManager?.scrollToPositionWithOffset(0, 0)
                }
            }, 1000)
        }
    }

    private fun getThumbnail(videoThumbImageFile: File?): String {
        return if (videoThumbImageFile != null && videoThumbImageFile.absolutePath != "") {
            videoThumbImageFile.absolutePath
        } else {
            ""
        }
    }

    override fun onResume() {
        super.onResume()
        if (rxTask != null) {
            rxTask?.dispose()
        }
        if (!viewModelChat.isWebSocketOpen && !viewModelChat.isWebSocketOpenCalled) {
            viewModelChat.connectToSocketSm()
        }
        rxTask = viewModelChat.rxWebSocket?.onTextMessage()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnNext { socketMessageEvent ->
                val isActivityInForeground =
                    lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)
                if (isActivityInForeground) {
                    handleMessage(socketMessageEvent?.text)
                }
            }
            ?.subscribe(
                Functions.emptyConsumer(),
                { throwable: Throwable -> println(throwable) })


        isNewActivityLaunched = if (!isNewActivityLaunched) {
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                callApiCommon()
            }, 100)
            false
        } else {
            false
        }
        if (mChatHistoryList != null && mChatHistoryList?.size!! > 0 &&
            mChatHistoryList?.get(0)?.msg_id != null
        ) {
            mLastId = mChatHistoryList?.get(0)?.msg_id ?: ""
        }

    }

    private fun callApiCommon() {
        if (InternetUtil.isInternetOn()) {
            callApi(RemoteConstant.mOffset)
        } else {
            viewModelChat.getSingleChatResponseModel.postValue(ResultResponse.noInternet(ArrayList()))
        }

    }


    private fun callApi(mOffset: String) {
        // TODO : Chat List

        viewModelChat.getRecentMessageList(
            nJwt,
            convId,
            mOffset,
            RemoteConstant.mCount50
        )
    }

    private fun removeLoaderFormList() {

        if (mChatHistoryList != null && mChatHistoryList?.size!! > 0 && mChatHistoryList?.get(
                mChatHistoryList?.size!! - 1
            ) != null && mChatHistoryList?.get(mChatHistoryList?.size!! - 1)?.msg_id == ""
        ) {
            mChatHistoryList?.removeAt(mChatHistoryList?.size!! - 1)
            binding?.chatRecyclerView?.post {
                refreshList()
            }
            isAddedLoader = false
        }
    }

    private fun offlineMessage() {
        viewModelChat.mOfflineMessage?.nonNull()?.observe(this, Observer { mOfflineMessage ->
            if (mOfflineMessage != null && mOfflineMessage.isNotEmpty()) {
                mOfflineMessageList = mOfflineMessage
            }
        })
    }

    inner class CustomScrollListener :
        RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: RecyclerView,
            dx: Int,
            dy: Int
        ) {
            if (dy > 0) {
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManagerChat).findFirstVisibleItemPosition()
                if (firstVisibleItemPosition == 0) {
                    if (binding?.imageNewMessage?.visibility == View.VISIBLE) {
                        binding?.imageNewMessage?.visibility = View.GONE
                    }
                }
            } else {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManagerChat).findFirstVisibleItemPosition()
                if (InternetUtil.isInternetOn()) {
                    if (mChatHistoryList?.size!! >= RemoteConstant.mCount50.toInt()) {
                        if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                            mStatus != ResultResponse.Status.LOADING &&
                            visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                        ) {
                            if (!isAddedLoader) {
                                val mNewListSize = mChatHistoryList?.size!!
                                callApi(mChatHistoryList?.get(mNewListSize - 1)?.msg_id!!)
                                val mutableList: MutableList<MsgList> = ArrayList()
                                val mList = MsgList()
                                mutableList.add(mList)
                                mList.msg_id = ""
                                mChatHistoryList?.addAll(mutableList)
                                refreshList()
                                isAddedLoader = true
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDate(time: Long): String? {
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        return formatter.format(Date(time))

    }

    /**
     * Adding API list to Hash-map for filter with date
     * Reference
     */
    private fun refreshList() {
        val groupedHashMap: LinkedHashMap<String, MutableSet<MsgList>?> = LinkedHashMap()
        val recyclerItems = mutableListOf<RecyclerItem>()
        var list: MutableSet<MsgList>? = null
        groupedHashMap.clear()
        recyclerItems.clear()
        for (chatModel in mChatHistoryList!!) {
            //Log.d(TAG, travelActivityDTO.toString());
            var hashMapKey = ""
            if (chatModel.timestamp != null) {
                hashMapKey = getDate(chatModel.timestamp?.toLong()!!)!!
            }
            //Log.d(TAG, "start date: " + DateParser.convertDateToString(travelActivityDTO.getStartDate()));
            if (hashMapKey != "" && groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap[hashMapKey]!!.add(chatModel)
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                list = LinkedHashSet()
                list.add(chatModel)
                groupedHashMap[hashMapKey] = list
            }
        }
        for (date in groupedHashMap.keys) {
            recyclerItems.run {
                val mList: MutableSet<MsgList>? = groupedHashMap[date]
                mList?.map {
                    add(RecyclerItem(date, false, it))
                }
                add(RecyclerItem(date, true, MsgList()))
                this
            }
        }

        if (recyclerItems.size > 0) {
            binding?.chatRecyclerView?.post {
                messageAdapter?.swap(recyclerItems)
            }
        }
    }

    data class RecyclerItem(
        val title: String,
        val isSticky: Boolean = false,
        val list: MsgList
    )

    private fun handleMessage(text: String?) {
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)

            if (webSocketResponse?.type != null) {


                if (webSocketResponse.from == mProfileId) {


                    if (webSocketResponse.type == "message") {
                        isAddedMessage = false
//                        println("--------------->" + text)
                        val mMsgList = MsgList()
                        val mMessageSender = MessageSender()
                        mMsgList.msg_id = webSocketResponse.msgPayload?.msgId
                        mMsgList.conv_id = webSocketResponse.msgPayload?.convId
                        mMsgList.text = webSocketResponse.msgPayload?.text
                        mMsgList.msg_type = webSocketResponse.msgPayload?.msgType




                        mMsgList.timestamp = webSocketResponse.msgPayload?.timestamp
                        mMsgList.status = webSocketResponse.msgPayload?.status!!
                        mMsgList.file = webSocketResponse.msgPayload?.file

                        val mProfileId = SharedPrefsUtils.getStringPreference(
                            this, RemoteConstant.mProfileId, ""
                        )
                        if (mProfileId == webSocketResponse.user) {
                            mMessageSender._id = webSocketResponse.user
                        } else {
                            mMessageSender._id = webSocketResponse.from
                        }
                        mMessageSender.username =
                            webSocketResponse.msgPayload?.message_sender?.username
                        mMessageSender.name =
                            webSocketResponse.msgPayload?.message_sender?.name
                        mMessageSender.pimage =
                            webSocketResponse.msgPayload?.message_sender?.pimage

                        mChatHistoryList?.map {
                            if (it.timestamp == webSocketResponse.msgPayload?.timestamp) {
                                val mPosition = mChatHistoryList?.indexOf(it)
                                mMsgList.message_sender = mMessageSender
                                mMsgList.file = webSocketResponse.msgPayload?.file
                                mChatHistoryList?.set(mPosition!!, mMsgList)

                                binding?.chatRecyclerView?.post {
                                    refreshList()

                                    if (mMsgList.status != "1") {
                                        binding?.chatRecyclerView?.smoothScrollToPosition(
                                            0
                                        )
                                        mLayoutManager?.scrollToPositionWithOffset(0, 0)
                                    }
                                    if (mMsgList.msg_type == "text") {
                                        binding?.chatRecyclerView?.smoothScrollToPosition(
                                            0
                                        )
                                        mLayoutManager?.scrollToPositionWithOffset(0, 0)
                                    }
                                }
                                isAddedMessage = true
                            }
                        }

                        if (!isAddedMessage) {
                            isAddedMessage = false
                            mMsgList.message_sender = mMessageSender
                            mMsgList.file = webSocketResponse.msgPayload?.file
                            mChatHistoryList?.add(0, mMsgList)
                            binding?.chatRecyclerView?.post {
                                binding?.chatRecyclerView?.smoothScrollToPosition(0)
                                mLayoutManager?.scrollToPositionWithOffset(0, 0)
                            }
                            refreshList()
                        }

                    }
                    if (webSocketResponse.type == "delete-message") {
                        var mId = -1
                        val msgId = webSocketResponse.msgPayload?.msgId
                        mChatHistoryList?.map {
                            if (msgId == it.msg_id) {
                                mId = mChatHistoryList?.indexOf(it)!!
                            }
                        }
                        if (mId != -1) {
                            mChatHistoryList?.removeAt(mId)
                            binding?.chatRecyclerView?.post {
                                refreshList()
                                messageAdapter?.notifyDataSetChanged()
                            }
                        }
                    }
                    if (webSocketResponse.type == "desktop-notification") {

                        isAddedMessage = false
                        val mMsgListItem: MutableList<MsgList> = ArrayList()
                        val mMsgList = MsgList()
                        val mMessageSender = MessageSender()
                        mMsgList.msg_id = webSocketResponse.msgPayload?.msgId
                        mMsgList.conv_id = webSocketResponse.msgPayload?.convId
                        mMsgList.text = webSocketResponse.msgPayload?.text
                        mMsgList.msg_type = webSocketResponse.msgPayload?.msgType
                        mMsgList.timestamp = webSocketResponse.msgPayload?.timestamp
                        mMsgList.status = webSocketResponse.msgPayload?.status!!
                        mMsgList.file = webSocketResponse.msgPayload?.file
                        mMessageSender._id = webSocketResponse.from
                        mMessageSender.username =
                            webSocketResponse.msgPayload?.message_sender?.username
                        mMessageSender.name = webSocketResponse.msgPayload?.message_sender?.name
                        mMessageSender.pimage =
                            webSocketResponse.msgPayload?.message_sender?.pimage

                        if (!isAddedMessage) {
                            isAddedMessage = false
                            mMsgList.message_sender = mMessageSender
                            mMsgListItem.add(mMsgList)
                            mChatHistoryList?.addAll(0, mMsgListItem)
                            binding?.chatRecyclerView?.post {
                                refreshList()
                                messageAdapter?.notifyDataSetChanged()
                                val firstVisibleItemPosition =
                                    (binding?.chatRecyclerView?.layoutManager as WrapContentLinearLayoutManagerChat).findFirstVisibleItemPosition()
                                if (firstVisibleItemPosition == 0) {
                                    binding?.chatRecyclerView?.smoothScrollToPosition(0)
                                    mLayoutManager?.scrollToPositionWithOffset(0, 0)
                                } else {
                                    showNewMessageDot()
                                }
                            }
                        }
                        if (webSocketResponse.from != SharedPrefsUtils.getStringPreference(
                                this, RemoteConstant.mProfileId, ""
                            )
                        ) {
                            val mSeenMessagePayload =
                                SeenMessagePayload()
                            val mAckMsg =
                                AckMsg()
                            mSeenMessagePayload.type = "ack"
                            mSeenMessagePayload.user = mProfileId
                            mAckMsg.msgId = webSocketResponse.msgPayload?.msgId
                            mAckMsg.message = "seen"
                            mSeenMessagePayload.ackMsg = mAckMsg

                            val mGson = Gson()
                            val mMessage =
                                mGson.toJson(mSeenMessagePayload)

                            viewModelChat.sendMessageSm(mMessage)
                        }

                    }
                    if (webSocketResponse.type == "ack") {
//                        AppUtils.showCommonToast(this, "Same")
                    }
                } else {
                    //TODO : If two logged i users are same condition
                    if (webSocketResponse.type == "ack") {

                        mChatHistoryList?.map {
                            if (webSocketResponse.ackMsg?.status == "update-file" || webSocketResponse.ackMsg?.status == "file-upload-failed") {
                                if (it.msg_id != null && it.msg_id != "") {
                                    if (it.msg_id?.contains(
                                            webSocketResponse.ackMsg.msgId ?: ""
                                        )!!
                                    ) {
                                        val mIndex = mChatHistoryList?.indexOf(it)!!
                                        if (webSocketResponse.ackMsg.status == "file-upload-failed") {
                                            mChatHistoryList?.get(mIndex)?.file?.status = "3"
                                        } else if (webSocketResponse.ackMsg.status == "update-file") {
                                            mChatHistoryList?.get(mIndex)?.file?.status = "0"
                                        }
                                        mChatHistoryList?.get(mIndex)?.file?.url =
                                            webSocketResponse.ackMsg.filename
                                    }
                                }
                            }
                        }
                        binding?.chatRecyclerView?.post {
                            refreshList()
                        }

                    }
                    if (webSocketResponse.type == "delete-message") {
                        var mId = -1
                        val msgId = webSocketResponse.msgPayload?.msgId
                        mChatHistoryList?.map {
                            if (msgId == it.msg_id) {
                                mId = mChatHistoryList?.indexOf(it)!!
                            }
                        }
                        if (mId != -1) {
                            mChatHistoryList?.removeAt(mId)
                            binding?.chatRecyclerView?.post {
                                refreshList()
                            }
                        }


                    }
                }

            }
        } catch (e: Exception) {
            AppUtils.showCommonToast(this, "" + e.printStackTrace())
            e.printStackTrace()
        }

    }

    private fun showNewMessageDot() {
        if (mChatHistoryList?.size ?: 0 >= 15) {
            if (binding?.imageNewMessage?.visibility == View.GONE) {
                binding?.imageNewMessage?.visibility = View.VISIBLE
                binding?.imageNewMessage?.bringToFront()
            }
        }
    }
    private fun observeSingleChatHistory() {
        viewModelChat.getSingleChatResponseModel.nonNull()
            .observe(this, Observer { resultResponse ->


                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.ADD_LOCAL_DATA -> {
                        val mLocalList = resultResponse.data!!
                        mChatHistoryList?.addAll(0, mLocalList)
                        binding?.chatRecyclerView?.post {
                            refreshList()
                            binding?.chatRecyclerView?.smoothScrollToPosition(0)
                            mLayoutManager?.scrollToPositionWithOffset(0, 0)
                        }
                    }
                    ResultResponse.Status.SUCCESS -> {
                        mChatHistoryList = resultResponse.data!!

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.VISIBLE

                        // TODO : Offline messages ( Backlogged )
                        if (mOfflineMessageList != null && mOfflineMessageList?.size!! > 0) {
                            mOfflineMessageList?.map { mOfflineList ->
                                if (mOfflineList.message != null) {
                                    try {
                                        val gsonWebSocketResponse = GsonBuilder()
                                            .setLenient()
                                            .create()
                                        val webSocketResponse: WebSocketResponse? =
                                            gsonWebSocketResponse.fromJson(
                                                mOfflineList.message,
                                                WebSocketResponse::class.java
                                            )
                                        val mMsgList = MsgList()
                                        val mMessageSender = MessageSender()
                                        mMsgList.msg_id = webSocketResponse?.msgPayload?.msgId
                                        mMsgList.conv_id = webSocketResponse?.msgPayload?.convId
                                        mMsgList.text = webSocketResponse?.msgPayload?.text
                                        mMsgList.msg_type = webSocketResponse?.msgPayload?.msgType
                                        mMsgList.timestamp =
                                            webSocketResponse?.msgPayload?.timestamp

                                        mMsgList.status = webSocketResponse?.msgPayload?.status!!
                                        mMessageSender._id = webSocketResponse.user
                                        mMessageSender.username =
                                            webSocketResponse.msgPayload?.message_sender?.username
                                        mMessageSender.name =
                                            webSocketResponse.msgPayload?.message_sender?.name
                                        mMessageSender.pimage =
                                            webSocketResponse.msgPayload?.message_sender?.pimage
                                        mMessageSender.isown = "true"
                                        mMsgList.message_sender = mMessageSender
                                        if (webSocketResponse.user == mProfileId) {
                                            mChatHistoryList?.add(0, mMsgList)
                                        }


                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            }

                            if (mChatHistoryList?.size!! > 0) {
                                refreshList()
                            } else {
                                binding?.isVisibleList = false
                                binding?.isVisibleLoading = false
                                binding?.isVisibleNoData = false
                                binding?.isVisibleNoInternet = true
                                val shake: Animation =
                                    AnimationUtils.loadAnimation(this, R.anim.shake)
                                binding?.noInternetLinear?.startAnimation(shake) // starts animation
                                glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                                    .into(binding?.noInternetImageView!!)
                            }
                            val handler = Handler(Looper.getMainLooper())
                            handler.postDelayed({
                                binding?.chatRecyclerView?.post {
                                    binding?.chatRecyclerView?.smoothScrollToPosition(0)
                                    mLayoutManager?.scrollToPositionWithOffset(0, 0)
                                }
                            }, 1000)

                        }

                        if (mLastId != "0") {
                            if (mChatHistoryList != null && mChatHistoryList?.size!! > 0 &&
                                mChatHistoryList?.get(0)?.msg_id != null
                            ) {
                                val mLastIdList = mChatHistoryList?.get(0)?.msg_id!!
                                if (mLastId != mLastIdList) {
                                    val firstVisibleItemPosition =
                                        (binding?.chatRecyclerView?.layoutManager as WrapContentLinearLayoutManagerChat).findFirstVisibleItemPosition()
                                    if (firstVisibleItemPosition == 0) {
                                        binding?.chatRecyclerView?.smoothScrollToPosition(0)
                                        mLayoutManager?.scrollToPositionWithOffset(0, 0)
                                    } else {
                                        showNewMessageDot()
                                    }
                                }
                            }
                        }
                        refreshList()

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                        //TODO :Show Mark tip

                        var mChatOpenCount =
                            SharedPrefsUtils.getIntegerPreference(
                                this,
                                RemoteConstant.mChatOpenCount,
                                0
                            )
                        if (mChatOpenCount == 5) {
                            mChatOpenCount = 0
                            SharedPrefsUtils.setIntegerPreference(
                                this,
                                RemoteConstant.mChatOpenCount,
                                mChatOpenCount
                            )

                            val handler = Handler(Looper.getMainLooper())
                            handler.postDelayed({
                                val mToolTipsManager = ToolTipsManager(this)
                                mToolTipsManager.findAndDismiss(binding?.displayTooltipHere)

                                val builder: ToolTip.Builder = ToolTip.Builder(
                                    this,
                                    binding?.displayTooltipHere!!,
                                    binding?.rootView!!,
                                    "Tap here to ping your friend",
                                    ToolTip.POSITION_ABOVE
                                )
                                builder.setBackgroundColor(Color.parseColor("#1e90ff"))
                                builder.setTypeface(
                                    Typeface.createFromAsset(
                                        assets,
                                        "fonts/poppinsmedium.otf"
                                    )
                                )
                                builder.setAlign(ToolTip.ALIGN_LEFT)
                                mToolTipsManager.findAndDismiss(binding?.chatRecyclerView)
                                mToolTipsManager.show(builder.build())


                                val handlerDismiss = Handler(Looper.getMainLooper())
                                handlerDismiss.postDelayed({
                                    mToolTipsManager.dismissAll()
                                }, 4000)

                            }, 1000)

                        } else {
                            mChatOpenCount += 1
                            SharedPrefsUtils.setIntegerPreference(
                                this,
                                RemoteConstant.mChatOpenCount,
                                mChatOpenCount
                            )
                        }
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }

                        binding?.chatRecyclerView?.post {
                            refreshList()
                        }

                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING_PAGINATED_LIST -> {
                        val mLocalList = resultResponse.data!!
                        mChatHistoryList?.addAll(0, mLocalList)
                        binding?.chatRecyclerView?.post {
                            refreshList()
                        }
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                            mChatHistoryList?.addAll(resultResponse.data!!)
                            refreshList()

                            mStatus = ResultResponse.Status.LOADING_COMPLETED
                        }, 1000)

                    }
                    ResultResponse.Status.ERROR -> {

                        if (SearchTrackFragment.dbPlayList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.dataLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        if (mChatHistoryList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.GONE
                            binding?.dataLinear?.visibility = View.VISIBLE
                            binding?.noDataLinear?.visibility = View.GONE
                        }
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.NO_INTERNET -> {
                        // TODO : Offline messages ( Backlogged )
                        if (mOfflineMessageList != null && mOfflineMessageList?.size!! > 0) {
                            mOfflineMessageList?.map { mOfflineList ->
                                if (mOfflineList.message != null) {


                                    try {
                                        val gsonWebSocketResponse = GsonBuilder()
                                            .setLenient()
                                            .create()
                                        val webSocketResponse: WebSocketResponse? =
                                            gsonWebSocketResponse.fromJson(
                                                mOfflineList.message,
                                                WebSocketResponse::class.java
                                            )
                                        val mMsgList = MsgList()
                                        val mMessageSender = MessageSender()
                                        mMsgList.msg_id = webSocketResponse?.msgPayload?.msgId
                                        mMsgList.conv_id = webSocketResponse?.msgPayload?.convId
                                        mMsgList.text = webSocketResponse?.msgPayload?.text
                                        mMsgList.msg_type = webSocketResponse?.msgPayload?.msgType
                                        mMsgList.timestamp =
                                            webSocketResponse?.msgPayload?.timestamp

                                        mMsgList.status = webSocketResponse?.msgPayload?.status!!
                                        mMessageSender._id = webSocketResponse.user
                                        mMessageSender.username =
                                            webSocketResponse.msgPayload?.message_sender?.username
                                        mMessageSender.name =
                                            webSocketResponse.msgPayload?.message_sender?.name
                                        mMessageSender.pimage =
                                            webSocketResponse.msgPayload?.message_sender?.pimage
                                        mMessageSender.isown = "true"
                                        mMsgList.message_sender = mMessageSender
                                        if (webSocketResponse.user == mProfileId) {
                                            mChatHistoryList?.add(0, mMsgList)
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            }

                            if (mChatHistoryList?.size!! > 0) {
                                refreshList()
                            }
                        }

                        if (mChatHistoryList?.size == 0) {
                            binding?.dataLinear?.visibility = View.VISIBLE
                            binding?.socketStateTv?.visibility = View.VISIBLE
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            binding?.socketStateTv?.setBackgroundColor(Color.parseColor("#f40505"))
                            binding?.socketStateTv?.text = "You are offline..."
                        }
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE

                    }
                    else -> {
                    }
                }
            })


    }

    private fun callApiMarkAllAsRead() {
        val nJwt =
            "Bearer " + SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mJwt,
                ""
            )!!
        viewModelChat.callApiMarkAllAsRead(nJwt, convId)
    }

    /**
     * Delete chat section
     */
    override fun chatOptionsDialog(mId: Int?, mTimeStamp: String?) {

        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.delete_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(true)

        val deleteTextView: TextView = dialogView.findViewById(R.id.delete_text_view)

        val b: AlertDialog? = dialogBuilder.create()
        b?.show()


        deleteTextView.setOnClickListener {

            try {
                if (InternetUtil.isInternetOn()) {
                    if (mChatHistoryList != null &&
                        mChatHistoryList?.get(mId!!) != null &&
                        mChatHistoryList?.get(mId!!)?.msg_id != null &&
                        mChatHistoryList?.get(mId!!)?.msg_id != ""
                    ) {
                        val nJwt =
                            "Bearer " + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mJwt,
                                ""
                            )!!
                        viewModelChat.deleteMessage(
                            nJwt,
                            mChatHistoryList?.get(mId!!)?.msg_id
                        )

                        mChatHistoryList?.removeAt(mId!!)
                        binding?.chatRecyclerView?.post {
                            refreshList()
                        }
                    } else {
                        // viewModel?.deleteMessageOffline(mTimeStamp!!)
                    }
                } else {
                    viewModelChat.deleteMessageOffline(mTimeStamp!!)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    // TODO : Reconnected to web-socket
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ConnectedSocket) {
        if (event.isConnected) {
            if (viewModelChat.isWebSocketOpen) {
                binding?.socketStateTv?.setBackgroundColor(Color.parseColor("#006400"))
                binding?.socketStateTv?.text = "Welcome back..."
                if (viewModelChat.isWebSocketCalled) {
                    viewModelChat.isWebSocketCalled = false
                }
                binding?.socketStateTv?.visibility = View.GONE
                if (isOffline) {
                    isNewActivityLaunched = false
                    onResume()
                    isOffline = false
                }
            }
        }

    }

    override fun onTipDismissed(view: View?, anchorViewId: Int, byUser: Boolean) {

    }

    fun goToImageDetailsPage(mMsgListFile: MsgListFile?, time: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            isNewActivityLaunched = true
            if (mMsgListFile?.type == "video") {
                val intentImageDetailsActivity = Intent(this, ChatVideoDetailsActivity::class.java)
                intentImageDetailsActivity.putExtra("video", mMsgListFile.url)
                intentImageDetailsActivity.putExtra("time", time)
                startActivityForResult(intentImageDetailsActivity, 1501)
            } else if (mMsgListFile?.type == "image") {
                val intentImageDetailsActivity = Intent(this, ChatImageDetailsActivity::class.java)
                intentImageDetailsActivity.putExtra("image", mMsgListFile.url)
                intentImageDetailsActivity.putExtra("time", time)
                startActivityForResult(intentImageDetailsActivity, 1501)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    private class WrapContentLinearLayoutManagerChat(
        context: Context?,
        orientation: Int,
        reverseLayout: Boolean
    ) :
        LinearLayoutManager(context, orientation, reverseLayout) {
        override fun onLayoutChildren(
            recycler: RecyclerView.Recycler?,
            state: RecyclerView.State?
        ) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (e: Exception) {
                Log.e("probe", "meet a IOOBE in RecyclerView")
            }
        }
    }


    override fun onProgressUpdateImage(percentage: Int, timestamp: String?) {
        val mUploadProgress = UploadProgress(timestamp!!, percentage.toString())
        viewModelChat.uploadProgress?.postValue(mUploadProgress)
    }

    override fun onFinish() {

    }

    /**
     * API upload progress
     */
    override fun onProgressUpdate(percentage: Int, timestamp: String?) {
        val mUploadProgress = UploadProgress(timestamp!!, percentage.toString())
        viewModelChat.uploadProgress?.postValue(mUploadProgress)
    }

    override fun onError() {

    }

    /**
     * Cancel upload API and remove from local list
     */
    fun cancelUpload(localFilePath: String?, mType: String, mFilePath: String, nMessageId: String) {
        viewModelChat.cancelApiCall(localFilePath!!, mType, mFilePath)
        mChatHistoryList?.map {
            if (it.msg_id != null && it.msg_id != "") {
                if (it.msg_id?.contains(nMessageId)!!) {
                    val mIndex = mChatHistoryList?.indexOf(it)!!
                    mChatHistoryList?.get(mIndex)?.file?.status = "3"
                }
            }
        }
        binding?.chatRecyclerView?.post {
            refreshList()
        }
    }

    /**
     * Retry file upload with new pre-signed url
     */
    fun retryUpload(msgId: String?, localFilePath: String?, mType: String?, mTimeStamp: String?) {
        val videoSize: Int
        val imageSize: Int
        val imageList: MutableList<String> = ArrayList()
        if (mType == "image") {
            videoSize = 0
            imageSize = 1
            imageList.add(localFilePath!!)
            viewModelChat.preSignedUrlChat(
                imageSize, nJwt, imageList, "", mProfileId, "",
                msgId, this, this, mTimeStamp
            )
        } else {
            // TODO : Clear progress in video upload
            onProgressUpdate(0, mTimeStamp)
            videoSize = 1
            imageSize = 0
            viewModelChat.preSignedUrlChat(
                imageSize, nJwt, imageList, localFilePath!!, mProfileId,
                getThumbnail(File(localFilePath)), msgId, this,
                this, mTimeStamp
            )
        }
        mChatHistoryList?.map {
            if (it.msg_id != null && it.msg_id != "") {
                if (it.msg_id?.contains(msgId.toString())!!) {
                    val mIndex = mChatHistoryList?.indexOf(it)!!
                    mChatHistoryList?.get(mIndex)?.file?.status = "0"
                }
            }
        }
        binding?.chatRecyclerView?.post {
            refreshList()
        }
    }
}
