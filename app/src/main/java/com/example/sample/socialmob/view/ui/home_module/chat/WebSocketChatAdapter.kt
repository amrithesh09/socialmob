package com.example.sample.socialmob.view.ui.home_module.chat

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.*
import com.example.sample.socialmob.model.chat.MsgList
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import java.util.*


class WebSocketChatAdapter(
    private var mUserId: String?,
    private var chatOptionsDialog: ChatOptions?,
    private var mContext: Context,
    private var viewModel: WebSocketChatViewModel, private var glideRequestManager: RequestManager
) : RecyclerView.Adapter<BaseViewHolder<Any>>() {

    private val DOUBLE_CLICK_TIME_DELTA: Long = 300 //milliseconds
    private var lastClickTime: Long = 0
    private val mutableMessageList = mutableListOf<WebSocketChatActivity.RecyclerItem>()
    private var isVisible = false

    companion object {
        const val ITEM_LOADING = 1
        const val ITEM_LEFT_TEXT = 2
        const val ITEM_RIGHT_TEXT = 3

        const val ITEM_LEFT_IMAGE = 4
        const val ITEM_RIGHT_IMAGE = 5

        const val ITEM_LEFT_VIDEO = 6
        const val ITEM_RIGHT_VIDEO = 7
        const val TYPE_DATE = 8

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_LOADING -> bindLoader(parent)

            ITEM_LEFT_TEXT -> bindLeftText(parent)
            ITEM_RIGHT_TEXT -> bindRightText(parent)

            ITEM_LEFT_IMAGE -> bindLeftImage(parent)
            ITEM_RIGHT_IMAGE -> bindRightImage(parent)

            ITEM_LEFT_VIDEO -> bindLeftVideo(parent)
            ITEM_RIGHT_VIDEO -> bindRightVideo(parent)
            TYPE_DATE -> bindDate(parent)

            else -> bindLoader(parent)
        }
    }

    private fun bindDate(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageDateBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_date, parent, false
        )
        return MessageDate(binding)
    }

    private fun bindLeftText(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageLeftItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_left_item, parent, false
        )
        return MessageLeftText(binding)
    }

    private fun bindRightText(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageRightItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_right_item, parent, false
        )
        return MessageRightText(binding)
    }

    private fun bindLeftImage(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageLeftItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_left_item_image, parent, false
        )
        return MessageLeftImage(binding)
    }

    private fun bindRightImage(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageRightItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_right_item_image, parent, false
        )
        return MessageRightImage(binding)
    }

    private fun bindLeftVideo(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageLeftItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_left_item_image, parent, false
        )
        return MessageLeftVideo(binding)
    }


    private fun bindRightVideo(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MessageRightItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_right_item_image, parent, false
        )
        return MessageRightVideo(binding)
    }


    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        if (holder is MessageDate) {
            holder.bind(mutableMessageList[holder.adapterPosition].title)
        } else {
            holder.bind(mutableMessageList[holder.adapterPosition].list)
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }


    inner class MessageDate(val binding: MessageDateBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            binding.dateTextView.text = mData.toString()
        }
    }

    inner class MessageLeftText(val binding: MessageLeftItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            val mDataChatItem = mData as MsgList
            binding.chatData = mDataChatItem
            binding.executePendingBindings()
            when (mDataChatItem.msg_type) {
                "text" -> {
                    binding.messageText.isHyperlinkEnabled = true
                    binding.messageText.setText(mDataChatItem.text?.trim())
                    binding.messageText.visibility = View.VISIBLE
                    binding.pennyImageView.visibility = View.GONE
                }
                "ping" -> {
                    binding.messageText.visibility = View.GONE
                    binding.pennyImageView.visibility = View.VISIBLE
                }
                else -> {
                    binding.messageText.visibility = View.VISIBLE
                    binding.pennyImageView.visibility = View.GONE
                    binding.messageText.setText("Unsupported message")
                }
            }
            binding.pennyImageView.setOnClickListener {
                val clickTime = System.currentTimeMillis()
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    if (!binding.lottieImage.isAnimating) {
                        binding.lottieImage.playAnimation()
                    }
                    lastClickTime = 0
                } else {
                    if (!binding.leftEL.isExpanded) {
                        binding.leftEL.expand()
                    } else {
                        binding.leftEL.close()
                    }
                }
                lastClickTime = clickTime
            }
        }

    }

    inner class MessageLeftImage(val binding: MessageLeftItemImageBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            val mDataNew = mData as MsgList
            binding.chatData = mDataNew
            binding.executePendingBindings()
            if (mDataNew.file != null) {
                glideRequestManager.load(mDataNew.file?.url).thumbnail(0.1f)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {

                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }


                    })
                    .into(binding.imageView)
            }
            binding.imageView.setOnClickListener {
                (mContext as WebSocketChatActivity).goToImageDetailsPage(
                    mDataNew.file,
                    binding.messageTime.text.toString()
                )
            }
        }

    }

    inner class MessageLeftVideo(val binding: MessageLeftItemImageBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            val mDataNew = mData as MsgList

            binding.chatData = mDataNew
            binding.executePendingBindings()

            if (mDataNew.file != null &&
                mDataNew.file?.thumbnail != null && mDataNew.file?.thumbnail != ""
            ) {
                glideRequestManager.load(
                    mDataNew.file?.thumbnail
                ).thumbnail(0.1f)
                    .into(binding.imageView)
            }
            binding.imageView.setOnClickListener {
                (mContext as WebSocketChatActivity).goToImageDetailsPage(
                    mDataNew.file!!,
                    binding.messageTime.text.toString()
                )
            }
            binding.videoImageView.visibility = View.VISIBLE
            binding.videoBgGradient.visibility = View.VISIBLE

        }

    }

    inner class MessageRightVideo(val binding: MessageRightItemImageBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            val mDataNew = mData as MsgList

            binding.chatData = mDataNew
            binding.executePendingBindings()

            binding.videoImageView.visibility = View.VISIBLE
            binding.videoBgGradient.visibility = View.VISIBLE
            if (mDataNew.file != null && mDataNew.file?.thumbnail != null) {
                glideRequestManager.load(mDataNew.file?.thumbnail)
                    .apply(
                        RequestOptions.placeholderOf(0)
                            .error(0)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                    )
                    .into(binding.imageView)
            }
            /**
            0- upload in progress
            1- upload complete
            3- upload failed
             */
            when (mDataNew.file?.status) {
                "-1" -> {
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE
                    binding.sendingProgressBar.visibility = View.VISIBLE
                    binding.sendingProgressBar.bringToFront()
                }
                "0" -> {
                    binding.sendingProgressBar.visibility = View.GONE
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                    binding.cancelUpload.visibility = View.VISIBLE
                    binding.sendingProgressBar.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE
                }
                "1" -> {
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.sendingProgressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE
                }
                "3" -> {
                    binding.sendingProgressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.retryUpload.visibility = View.VISIBLE
                    binding.imageView.visibility = View.VISIBLE

                    binding.retryUpload.setOnClickListener {
                        if (!viewModel.mIsUploadingVideo && !viewModel.isFfmpegOnProgress) {
                            binding.retryUpload.post {
                                binding.retryUpload.visibility = View.GONE
                            }
                            binding.progressBar.visibility = View.VISIBLE
                            binding.cancelUpload.visibility = View.VISIBLE
                            (mContext as WebSocketChatActivity).retryUpload(
                                mDataNew.msg_id,
                                mDataNew.file?.local_file_path,
                                mDataNew.msg_type, mDataNew.timestamp
                            )
                        } else {
                            Toast.makeText(
                                mContext,
                                "Another video upload on progress",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }
                else -> {
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                    binding.cancelUpload.visibility = View.VISIBLE
                    binding.imageView.visibility = View.VISIBLE
                }
            }

            binding.cancelUpload.setOnClickListener {
                if (mDataNew.msg_id != null && mDataNew.msg_id != "") {
                    (mContext as WebSocketChatActivity).cancelUpload(
                        mDataNew.file?.local_file_path, "video",
                        mDataNew.file?.url!!, mDataNew.msg_id!!
                    )
                    binding.retryUpload.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE
                    if (viewModel.mIsUploadingVideo) {
                        viewModel.mIsUploadingVideo = false
                    }
                    binding.retryUpload.setOnClickListener {
                        if (!viewModel.mIsUploadingVideo && !viewModel.isFfmpegOnProgress) {
                            binding.retryUpload.visibility = View.GONE
                            binding.progressBar.visibility = View.VISIBLE
                            binding.cancelUpload.visibility = View.VISIBLE

                            (mContext as WebSocketChatActivity).retryUpload(
                                mDataNew.msg_id,
                                mDataNew.file?.local_file_path,
                                mDataNew.msg_type, mDataNew.timestamp
                            )
                        } else {
                            Toast.makeText(
                                mContext,
                                "Another video upload on progress",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

            }
            binding.imageView.setOnClickListener {
                if (mDataNew.file?.status == "1") {
                    if (mDataNew.file != null) {
                        (mContext as WebSocketChatActivity).goToImageDetailsPage(
                            mDataNew.file!!,
                            binding.messageTime.text.toString()
                        )
                    }
                }
            }
            binding.imageView.setOnLongClickListener {
                if (mDataNew.file?.status == "1" || mDataNew.file?.status == "3") {
                    chatOptionsDialog?.chatOptionsDialog(adapterPosition, mDataNew.timestamp)
                }
                return@setOnLongClickListener true
            }
        }
    }

    inner class MessageRightImage(val binding: MessageRightItemImageBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            val mDataNew = mData as MsgList
            binding.chatData = mDataNew
            binding.executePendingBindings()
            /**
            0- upload in progress
            1- upload complete
            3- upload failed
             */
            when (mDataNew.file?.status) {
                "-1" -> {
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE
                    binding.sendingProgressBar.visibility = View.VISIBLE
                    binding.sendingProgressBar.bringToFront()
                    glideRequestManager.load(mDataNew.file?.local_file_path)
                        .thumbnail(0.1f)
                        .into(binding.imageView)
                }
                "0" -> {
                    glideRequestManager.load(mDataNew.file?.local_file_path)
                        .thumbnail(0.1f)
                        .into(binding.imageView)
                    binding.sendingProgressBar.visibility = View.GONE
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                    binding.cancelUpload.visibility = View.VISIBLE
                    binding.imageView.visibility = View.VISIBLE
                }
                "1" -> {
                    glideRequestManager.load(mDataNew.file?.local_file_path)
                        .apply(
                            RequestOptions.placeholderOf(0).error(0)
                                .diskCacheStrategy(DiskCacheStrategy.NONE).priority(
                                    Priority.HIGH
                                ).skipMemoryCache(true)
                        )
                        .thumbnail(0.1f)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any?,
                                target: Target<Drawable>?,
                                isFirstResource: Boolean
                            ): Boolean {
                                binding.imageView.post {
                                    if (mDataNew.file != null && mDataNew.file?.url != null) {
                                        if (mContext != null) {
                                            glideRequestManager.load(mDataNew.file?.url)
                                                .thumbnail(0.1f)
                                                .into(binding.imageView)
                                        }
                                    }
                                }


                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any?,
                                target: Target<Drawable>?,
                                dataSource: DataSource?,
                                isFirstResource: Boolean
                            ): Boolean {

                                return false
                            }

                        })
                        .into(binding.imageView)
                    binding.imageView.setOnClickListener {
                        if (mDataNew.file?.status == "1") {
                            (mContext as WebSocketChatActivity).goToImageDetailsPage(
                                mDataNew.file,
                                binding.messageTime.text.toString()
                            )
                        }
                    }
                    binding.sendingProgressBar.visibility = View.GONE
                    binding.retryUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE

                }
                "3" -> {
                    glideRequestManager.load(mDataNew.file?.local_file_path)
                        .thumbnail(0.1f)
                        .into(binding.imageView)

                    binding.cancelUpload.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.retryUpload.visibility = View.VISIBLE
                    binding.imageView.visibility = View.VISIBLE
                    binding.sendingProgressBar.visibility = View.GONE

                    binding.retryUpload.setOnClickListener {

                        binding.retryUpload.visibility = View.GONE
                        binding.progressBar.visibility = View.VISIBLE
                        binding.cancelUpload.visibility = View.VISIBLE

                        (mContext as WebSocketChatActivity).retryUpload(
                            mDataNew.msg_id,
                            mDataNew.file?.local_file_path, mDataNew.msg_type, mDataNew.timestamp
                        )
                    }
                }
            }

            binding.cancelUpload.setOnClickListener {
                if (mDataNew.msg_id != null && mDataNew.msg_id != "") {
                    (mContext as WebSocketChatActivity).cancelUpload(
                        mDataNew.file?.local_file_path, "image",
                        mDataNew.file?.url!!, mDataNew.msg_id!!
                    )
                    binding.retryUpload.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    binding.cancelUpload.visibility = View.GONE
                    binding.imageView.visibility = View.VISIBLE

                    binding.retryUpload.setOnClickListener {
                        binding.retryUpload.visibility = View.GONE
                        binding.progressBar.visibility = View.VISIBLE
                        binding.cancelUpload.visibility = View.VISIBLE

                        (mContext as WebSocketChatActivity).retryUpload(
                            mDataNew.msg_id,
                            mDataNew.file?.local_file_path,
                            mDataNew.msg_type, mDataNew.timestamp
                        )
                    }
                } else {
                    Toast.makeText(mContext, "No message id", Toast.LENGTH_SHORT).show()
                }
            }
            binding.imageView.setOnLongClickListener {
                if (mDataNew.file?.status == "1" || mDataNew.file?.status == "3") {
                    chatOptionsDialog?.chatOptionsDialog(adapterPosition, mDataNew.timestamp)
                }
                return@setOnLongClickListener true
            }
        }
    }

    inner class MessageRightText(val binding: MessageRightItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(mData: Any?) {
            val mDataNew = mData as MsgList
            binding.chatData = mDataNew
            binding.executePendingBindings()

            if (mDataNew.status != null) {
                if (mDataNew.status == "-1") {
                    binding.sendingProgressBar.visibility = View.VISIBLE
                    binding.sendingProgressBar.bringToFront()
                }
                if (mDataNew.status == "1") {
                    isVisible = false
                    binding.messageSeen.visibility = View.GONE
                    binding.sendingProgressBar.visibility = View.GONE
                }
                if (mDataNew.status == "2") {
                    isVisible = false
                    binding.messageSeen.visibility = View.GONE
                    binding.sendingProgressBar.visibility = View.GONE
                }
                if (mDataNew.status == "3") {
                    if (!isVisible && adapterPosition == 0) {
                        binding.messageSeen.visibility = View.VISIBLE
                        isVisible = true
                    } else {
                        isVisible = false
                        binding.messageSeen.visibility = View.GONE
                    }
                    binding.sendingProgressBar.visibility = View.GONE
                }
            }

            when (mDataNew.msg_type) {
                "text" -> {
                    binding.messageText.text = mDataNew.text?.trim()
                    binding.messageText.visibility = View.VISIBLE
                    binding.pennyImageView.visibility = View.GONE
                }
                "ping" -> {
                    binding.messageText.visibility = View.GONE
                    binding.pennyImageView.visibility = View.VISIBLE
                }
                else -> {
                    binding.messageText.visibility = View.VISIBLE
                    binding.pennyImageView.visibility = View.GONE
                    binding.messageText.text = "Unsupported message"
                }
            }
            binding.bubbleRightLayout.setOnLongClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    binding.rightEL.performLongClick()
                }
                return@setOnLongClickListener true
            }
            binding.rightEL.setOnLongClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    chatOptionsDialog?.chatOptionsDialog(adapterPosition, mDataNew.timestamp)
                }
                return@setOnLongClickListener true
            }
            binding.pennyImageView.setOnClickListener {
                val clickTime = System.currentTimeMillis()
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    if (!binding.lottieImage.isAnimating) {
                        binding.lottieImage.playAnimation()
                    }
                    lastClickTime = 0
                } else {
                    if (!binding.rightEL.isExpanded) {
                        binding.rightEL.expand()
                    } else {
                        binding.rightEL.close()
                    }
                }
                lastClickTime = clickTime
            }
        }

    }


    override fun getItemViewType(position: Int): Int {

        if (mutableMessageList[position].isSticky) {
            return TYPE_DATE
        } else {
            return when {
                mutableMessageList[position].list.message_sender?._id != null &&
                        mutableMessageList[position].list.message_sender?._id == mUserId -> {
                    return when (mutableMessageList[position].list.msg_type) {
                        "image" -> ITEM_RIGHT_IMAGE
                        "video" -> ITEM_RIGHT_VIDEO
                        "text" -> ITEM_RIGHT_TEXT
                        else -> ITEM_RIGHT_TEXT
                    }
                }
                mutableMessageList[position].list.message_sender?._id != null &&
                        mutableMessageList[position].list.message_sender?._id != mUserId -> {
                    return when (mutableMessageList[position].list.msg_type) {
                        "image" -> ITEM_LEFT_IMAGE
                        "video" -> ITEM_LEFT_VIDEO
                        "text" -> ITEM_LEFT_TEXT
                        else -> ITEM_LEFT_TEXT
                    }
                }
                mutableMessageList[position].list.message_sender?.isown == "true" -> {
                    return when (mutableMessageList[position].list.msg_type) {
                        "image" -> ITEM_RIGHT_IMAGE
                        "video" -> ITEM_RIGHT_VIDEO
                        "text" -> ITEM_RIGHT_TEXT
                        else -> ITEM_RIGHT_TEXT
                    }
                }
                mutableMessageList[position].list.msg_id == "" -> {
                    return ITEM_LOADING
                }

                else -> {
                    return when (mutableMessageList[position].list.msg_type) {
                        "image" -> ITEM_RIGHT_IMAGE
                        "video" -> ITEM_RIGHT_VIDEO
                        "text" -> ITEM_RIGHT_TEXT
                        else -> ITEM_RIGHT_TEXT
                    }
                }
            }
        }

    }

    interface ChatOptions {
        fun chatOptionsDialog(mId: Int?, mTimeStamp: String?)
    }

    override fun getItemCount() = mutableMessageList.size

    fun swap(list: MutableList<WebSocketChatActivity.RecyclerItem>) {
        val diffCallback =
            ChatDiffCallback(
                this.mutableMessageList,
                list
            )
        val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(diffCallback, true)

        this.mutableMessageList.clear()
        this.mutableMessageList.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}

class ChatDiffCallback(
    private val oldList: List<WebSocketChatActivity.RecyclerItem>?,
    private val newList: List<WebSocketChatActivity.RecyclerItem>?
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList?.size ?: 0

    override fun getNewListSize() = newList?.size ?: 0

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList?.get(oldItemPosition)?.list?.timestamp == newList?.get(newItemPosition)?.list?.timestamp

    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

        val oldItem = oldList?.get(oldItemPosition)?.list?.file?.status ?: "-1"
        val newItem = newList?.get(newItemPosition)?.list?.file?.status ?: "-1"
        return oldList?.get(oldItemPosition) == newList?.get(newItemPosition)
                && oldItem == newItem && oldList?.get(oldItemPosition)?.list?.timestamp == newList?.get(
            newItemPosition
        )?.list?.timestamp

    }


}
