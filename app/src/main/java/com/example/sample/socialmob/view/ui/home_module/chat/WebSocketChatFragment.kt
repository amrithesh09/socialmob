package com.example.sample.socialmob.view.ui.home_module.chat

import android.app.ActivityManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityOptionsCompat
import androidx.core.app.NotificationCompat
import androidx.core.util.Pair
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.WebChatFragmentBinding
import com.example.sample.socialmob.model.chat.ConvList
import com.example.sample.socialmob.model.chat.MsgPayload
import com.example.sample.socialmob.model.chat.SendMessage
import com.example.sample.socialmob.model.chat.WebSocketResponse
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.chat.model.PongData
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchTrackFragment
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.NotificationEvent
import com.example.sample.socialmob.view.utils.events.UpdateConversationEvent
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.viewmodel.feed.LastMessage
import com.example.sample.socialmob.viewmodel.feed.PingData
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class WebSocketChatFragment : Fragment(),
    ChatHistoryAdapter.MessageItemClick, ChatHistoryAdapter.DeleteConversation {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private var binding: WebChatFragmentBinding? = null
    private var chatHistoryAdapter: ChatHistoryAdapter? = null
    private var chatSuggestionAdapter: ChatSuggestionAdapter? = null

    private var mStatus: ResultResponse.Status? = null
    private var mChatHistoryList: MutableList<ConvList>? = ArrayList()
    private var mNotificationHelper: NotificationHelper? = null
    private var mGoogleApiClient: GoogleApiClient? = null

    private var mProfileIdNewChat = ""
    private var mNameNewChat = ""
    private var mImageNewChat = ""
    private var isAddedLoader: Boolean = false
    private var isNewChat: Boolean = false
    private var mLastClickTime: Long = 0
    private var setUserVisibleHint = false

    private val viewModelChat: WebSocketChatViewModel by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.web_chat_fragment, container, false
        )
        mNotificationHelper = NotificationHelper(activity)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.swipeToRefresh?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefresh?.isRefreshing = false
        binding?.swipeToRefresh?.setOnRefreshListener {
            mChatHistoryList?.clear()
            callApiCommon()
        }
        // Observe list
        observeChatHistory()
        observeChatSuggestion()

        binding?.startANewChatTextView?.setOnClickListener {
            val intent = Intent(activity, CommonFragmentActivity::class.java)
            intent.putExtra("isFrom", "CreateNewChat")
            startActivity(intent)
        }
        binding?.searchImageView?.setOnClickListener {
            val intent = Intent(activity, CommonFragmentActivity::class.java)
            intent.putExtra("isFrom", "ChatSearch")
            activity?.startActivity(intent)
        }
        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
            if (!InternetUtil.isInternetOn()) {
                if (binding?.swipeToRefresh?.isRefreshing!!) {
                    binding?.swipeToRefresh?.isRefreshing = false
                }
            }
        }
        binding?.goToOfflineMode?.setOnClickListener {
            if (activity != null) {
                (activity as HomeActivity).goToOffline()
            }

        }

        // TODO : For search
        glideRequestManager.load(R.drawable.ic_empty_penny).apply(RequestOptions().fitCenter())
            .into(binding?.noDataImageView!!)

        binding?.searchEditText?.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                val mSearchString = s.toString()
                if (mSearchString.isNotEmpty()) {

                    binding?.swipeToRefresh?.isEnabled = false
                    val mNewList = mChatHistoryList?.filter {
                        it.profile?.username?.toLowerCase()?.contains(mSearchString)!!
                    }
                    if (mNewList?.size!! > 0) {
                        chatHistoryAdapter?.swap(mNewList.toMutableList())
                    } else {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.VISIBLE
                        binding?.listLinear?.visibility = View.GONE
                    }
                } else {
                    binding?.swipeToRefresh?.isEnabled = true
                    if (mChatHistoryList?.size!! > 0) {
                        chatHistoryAdapter?.swap(mChatHistoryList!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE
                    } else {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.VISIBLE
                        binding?.listLinear?.visibility = View.GONE
                    }
                }
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {

            }
        })

        binding?.notificationRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding?.notificationRecyclerView?.isNestedScrollingEnabled = true

        val mProfileId =
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")!!
        chatHistoryAdapter =
            ChatHistoryAdapter(
                activity,
                mMessageItemClick = this,
                mProfileId = mProfileId,
                deleteConversation = this,
                isFromSearch = false,
                mNightModeStatus = SharedPrefsUtils.getBooleanPreference(
                    activity, RemoteConstant.NIGHT_MODE, false
                ), glideRequestManager = glideRequestManager
            )
        binding?.notificationRecyclerView?.adapter = chatHistoryAdapter


        //TODO : Recent Chat
        binding?.suggestionChatListRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding?.suggestionChatListRecyclerView?.isNestedScrollingEnabled = true

        chatSuggestionAdapter =
            ChatSuggestionAdapter(activity, this, glideRequestManager)
        binding?.suggestionChatListRecyclerView?.adapter = chatSuggestionAdapter

        binding?.progressLinear?.visibility = View.GONE
        binding?.noInternetLinear?.visibility = View.GONE
        binding?.noDataChatLinear?.visibility = View.GONE
        binding?.listLinear?.visibility = View.VISIBLE

        if (!viewModelChat.newMessage?.hasActiveObservers()!!) {
            viewModelChat.newMessage?.observe(viewLifecycleOwner, { newMessage ->
                if (newMessage != null && newMessage != "") {
                    if (!isNewChat) {
                        handleMessage(newMessage)
                    } else {
                        handleMessageNewUser(
                            newMessage, mProfileIdNewChat,
                            mNameNewChat,
                            mImageNewChat
                        )
                    }
                }
            })
        }

    }

    /**
     * If sending penny to new user need to navigate chat page with conversation id
     */
    private fun handleMessageNewUser(
        text: String?, mProfileId: String?, mName: String?, mImage: String?
    ) {
        val sourceType: String
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)

            sourceType = webSocketResponse?.type!!
            when (sourceType) {
                "message" -> {

                    val convId1 = webSocketResponse.msgPayload?.convId
                    val intent = Intent(activity, WebSocketChatActivity::class.java)
                    intent.putExtra("convId", convId1)
                    intent.putExtra("mProfileId", mProfileId)
                    intent.putExtra("mUserName", mName)
                    intent.putExtra("mUserImage", mImage)
                    intent.putExtra("mStatus", "")
                    intent.putExtra("mLastMessageUser", "")
                    startActivityForResult(intent, 112)

                    isNewChat = false
                    mProfileIdNewChat = ""
                    mNameNewChat = ""
                    mImageNewChat = ""

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isNewChat = false
            mProfileIdNewChat = ""
            mNameNewChat = ""
            mImageNewChat = ""
        }
    }

    /**
     *  Listening web-socket in fragment also in activity
     *  Case when same user is logined two different devices we need to update socket changes
     */
    private fun handleMessage(text: String?) {
        var sourceType = ""
        //TODO: For chat
        var convId = ""
        var mFromProfileId = ""
        val mUserName: String
        val actionTitleNew: String
        var pimage = ""
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)

            convId = webSocketResponse?.msgPayload?.convId!!

            sourceType = webSocketResponse.type!!
            when (sourceType) {
                "message" -> {
                    if (webSocketResponse.from != null) {
                        mFromProfileId = webSocketResponse.from!!
                    }
                    pimage = webSocketResponse.msgPayload?.message_sender?.pimage!!
                    mUserName = webSocketResponse.msgPayload?.message_sender?.username!!

                    viewModelChat.newMessage?.removeObservers(this)
                    val convId1 = webSocketResponse.msgPayload?.convId
                    if (isNewChat) {
                        val intent = Intent(activity, WebSocketChatActivity::class.java)
                        intent.putExtra("convId", convId1)
                        intent.putExtra("mProfileId", mFromProfileId)
                        intent.putExtra("mUserName", mUserName)
                        intent.putExtra("mUserImage", pimage)
                        intent.putExtra("mStatus", "")
                        intent.putExtra("mLastMessageUser", "")
                        startActivityForResult(intent, 112)
                    }
                }
                "ping" -> {
                    val mPongData =
                        PongData()
                    val mPingData = PingData()

                    mPingData.pingId = webSocketResponse.pingData?.pingId

                    mPongData.type = "pong"
                    mPongData.from = SharedPrefsUtils.getStringPreference(
                        activity, RemoteConstant.mProfileId, ""
                    )
                    mPongData.pingData = mPingData

                    val mGson = Gson()
                    val mMessage =
                        mGson.toJson(mPongData)

                    viewModelChat.sendMessageSm(mMessage)
//                    println("---->" + "pong sent - fragment")
                }
                "desktop-notification" -> {
                    mFromProfileId = webSocketResponse.from!!
                    pimage = webSocketResponse.msgPayload?.message_sender?.pimage!!
                    mUserName = webSocketResponse.msgPayload?.message_sender?.username!!
                    val msgType = webSocketResponse.msgPayload?.msgType
                    val mMessage: String
                    mMessage = if (msgType.equals("ping")) {
                        "Sent a ping"
                    } else if (webSocketResponse.msgPayload?.text != null && webSocketResponse.msgPayload?.text != "") {
                        webSocketResponse.msgPayload?.text ?: ""
                    } else {
                        "Sent an $msgType"

                    }
                    val mTitle = "$mUserName - $mMessage"
                    val sb: Spannable = SpannableString(mTitle)
                    sb.setSpan(
                        StyleSpan(Typeface.BOLD),
                        0,
                        mUserName.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )

                    actionTitleNew = sb.toString()


                    val mMyProfileId = SharedPrefsUtils.getStringPreference(
                        activity, RemoteConstant.mProfileId, ""
                    )
                    val mConnectedUser = SharedPrefsUtils.getStringPreference(
                        activity, RemoteConstant.mConnectedUser, ""
                    )
                    if (mFromProfileId != mMyProfileId) {
                        if (mFromProfileId != mConnectedUser || !MyApplication.isForeground) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                val notificationBuilder = mNotificationHelper?.getNotificationDM(
                                    "",
                                    actionTitleNew,
                                    convId,
                                    mFromProfileId,
                                    mUserName,
                                    pimage
                                )
                                if (notificationBuilder != null) {
                                    mNotificationHelper?.notify(
                                        System.currentTimeMillis().toInt(),
                                        notificationBuilder
                                    )
                                }
                            } else {
                                val mUniqueId = System.currentTimeMillis().toInt()
                                val intent = Intent(activity, WebSocketChatActivity::class.java)

                                intent.putExtra("convId", convId)
                                intent.putExtra("mProfileId", mFromProfileId)
                                intent.putExtra("mUserName", mUserName)
                                intent.putExtra("mUserImage", pimage)


                                val pendingIntent = PendingIntent.getActivity(
                                    activity, mUniqueId, intent,
                                    PendingIntent.FLAG_ONE_SHOT
                                )
                                val sound: Uri =
                                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE.toString() + "://" + activity?.packageName + "/" + R.raw.penny_sound) //Here is FILE_NAME is the name of file that you want to play


                                val notificationBuilder =
                                    NotificationCompat.Builder(
                                        requireActivity(),
                                        getString(R.string.default_notification_channel_id)
                                    )
                                        .setSmallIcon(R.mipmap.ic_custom_launcher_new_color)
                                        .setContentIntent(pendingIntent)
                                        .setContentText(actionTitleNew)
                                notificationBuilder.setSound(sound)
                                val notificationManager =
                                    activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                                    val channel = NotificationChannel(
                                        getString(R.string.default_notification_channel_id_chat),
                                        "chat",
                                        NotificationManager.IMPORTANCE_HIGH
                                    )
                                    notificationManager.createNotificationChannel(channel)
                                }
                                notificationBuilder.setAutoCancel(true)
                                notificationManager.notify(
                                    mUniqueId,
                                    notificationBuilder.build()
                                )
                            }
                            EventBus.getDefault()
                                .post(NotificationEvent("im-desktop-notification"))
                            val mJwt = SharedPrefsUtils.getStringPreference(
                                activity,
                                RemoteConstant.mJwt,
                                ""
                            )
                            if (mJwt != "") {
                                val nJwt =
                                    "Bearer " + SharedPrefsUtils.getStringPreference(
                                        activity,
                                        RemoteConstant.mJwt,
                                        ""
                                    )!!
                                viewModelChat.getChatMessageList(
                                    nJwt,
                                    RemoteConstant.mOffset,
                                    mChatHistoryList?.size!!
                                )
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            AppUtils.showCommonToast(requireActivity(), "" + e.printStackTrace())
            e.printStackTrace()
        }
        if (sourceType != "ping") {
            if (this.setUserVisibleHint) {
                callApiCommon()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        viewModelChat.newMessage?.removeObservers(this)
    }

    override fun onResume() {
        super.onResume()
        if (!this.setUserVisibleHint) {
            callApiCommon()
        }
        if (!viewModelChat.isWebSocketOpen && !viewModelChat.isWebSocketOpenCalled) {
            viewModelChat.connectToSocketSm()
        }
    }

    private fun callApiCommon() {

        if (InternetUtil.isInternetOn()) {
            callApi()

        } else {
            // TODO : Show no internet
            if (mChatHistoryList?.size!! == 0) {
                binding?.isVisibleList = false
                binding?.isVisibleLoading = false
                binding?.isVisibleNoDataPenny = false
                binding?.isVisibleNoDataNotification = false
                binding?.isVisibleNoInternet = true
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
                glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                    .into(binding?.noInternetImageView!!)
            }
        }

    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        this.setUserVisibleHint = setUserVisibleHint
    }
    /**
     * Get conversation list and chat suggestion list in top
     */
    private fun callApi() {
        // TODO : Chat List
        val mJwt = SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mJwt, "")
        if (mJwt != "") {
            val nJwt =
                "Bearer " + SharedPrefsUtils.getStringPreference(
                    activity,
                    RemoteConstant.mJwt,
                    ""
                )!!
            viewModelChat.getChatMessageList(
                nJwt,
                RemoteConstant.mOffset,
                mChatHistoryList?.size!!
            )
            viewModelChat.getChatSuggestionList(
                nJwt,
                RemoteConstant.mOffset,
                mChatHistoryList?.size!!
            )
        } else {
            if (binding?.swipeToRefresh?.isRefreshing!!) {
                binding?.swipeToRefresh?.isRefreshing = false
            }
            // TODO : Sign-out when user doesn't have JWT token
            signOut()
        }

    }

    private fun observeChatSuggestion() {

        viewModelChat.getAllChatSuggestionResponseModel?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->


                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        val mChatSuggestionList = resultResponse?.data!!

                        val mListRecent = SearchProfileResponseModelPayloadProfile(
                            0, "-1", "",
                            "",
                            "",
                            "", "",
                            "",
                            "", "",
                            "",
                            false, "",
                            "",
                            "-1"
                        )
                        mChatSuggestionList.add(0, mListRecent)
                        chatSuggestionAdapter?.swap(mChatSuggestionList)

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }

                        if (mChatHistoryList?.size!! >= 15) {
                            val mutableList: MutableList<ConvList> = ArrayList()
                            val lastMessage: LastMessage? = null
                            val profile: Profile? = null
                            val mList = ConvList(
                                System.currentTimeMillis().toInt(),
                                "-1",
                                profile,
                                lastMessage,
                                ""
                            )
                            mutableList.add(mList)
                            mChatHistoryList?.addAll(mutableList)
                            chatHistoryAdapter?.notifyDataSetChanged()
                        }
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }

                        val mutableList: MutableList<ConvList> = ArrayList()
                        val lastMessage: LastMessage? = null
                        val profile: Profile? = null
                        val mList = ConvList(
                            System.currentTimeMillis().toInt(),
                            "-1",
                            profile,
                            lastMessage,
                            ""
                        )

                        mutableList.add(mList)
                        mChatHistoryList?.addAll(mutableList)
                        chatHistoryAdapter?.swap(mChatHistoryList ?: ArrayList())

                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.ERROR -> {


                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.VISIBLE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.GONE
                        glideRequestManager.load(R.drawable.server_error).apply(
                            RequestOptions().fitCenter())
                            .into(binding?.noInternetImageView!!)
                        binding?.noInternetTextView?.text = getString(R.string.server_error)
                        binding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)

                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }

                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_empty_penny).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.VISIBLE


                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.LOADING -> {

                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.GONE
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }

                    }
                    else -> {

                    }
                }
                binding?.swipeToRefresh?.isRefreshing = false
            })
    }

    private fun observeChatHistory() {
        viewModelChat.getAllChatResponseModel.nonNull()
            .observe(viewLifecycleOwner, { resultResponse ->


                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mChatHistoryList = resultResponse?.data!!

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE
                        //                        binding?.penneyUnderConstructionLinear?.visibility = View.VISIBLE

                        chatHistoryAdapter?.swap(mChatHistoryList ?: ArrayList())

                        //                        val profileAdd: Profile? = null
                        //                        val lastMessageAdd: LastMessage? = null
                        //                        val timeStampAdd = ""
                        //                        val unreadMessageCountAdd = ""
                        //                        val mListRecent = ConvList(
                        //                            0, "-1", profileAdd,
                        //                            lastMessageAdd,
                        //                            timeStampAdd,
                        //                            unreadMessageCountAdd
                        //                        )
                        //                        mChatRecentList.add(mListRecent)
                        //                        recentAdapter?.swap(mChatRecentList.asReversed())

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }

                        if (mChatHistoryList?.size!! >= 15) {
                            val mutableList: MutableList<ConvList>? = ArrayList()
                            val lastMessage: LastMessage? = null
                            val profile: Profile? = null
                            val mList = ConvList(
                                System.currentTimeMillis().toInt(),
                                "-1",
                                profile,
                                lastMessage,
                                ""
                            )
                            mutableList?.add(mList)
                            mChatHistoryList?.addAll(mutableList!!)
                            chatHistoryAdapter?.notifyDataSetChanged()
                        }
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }

                        val mutableList: MutableList<ConvList>? = ArrayList()
                        val lastMessage: LastMessage? = null
                        val profile: Profile? = null
                        val mList = ConvList(
                            System.currentTimeMillis().toInt(),
                            "-1",
                            profile,
                            lastMessage,
                            ""
                        )

                        mutableList!!.add(mList)
                        mChatHistoryList?.addAll(mutableList)
                        chatHistoryAdapter?.swap(mChatHistoryList ?: ArrayList())

                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mChatHistoryList?.addAll(resultResponse?.data!!)
                        chatHistoryAdapter?.swap(mChatHistoryList ?: ArrayList())

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.ERROR -> {

                        if (SearchTrackFragment.dbPlayList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataChatLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }

                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_empty_penny).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.VISIBLE


                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }
                    }
                    ResultResponse.Status.LOADING -> {

                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataChatLinear?.visibility = View.GONE
                        if (binding?.swipeToRefresh?.isRefreshing!!) {
                            binding?.swipeToRefresh?.isRefreshing = false
                        }

                    }
                    else -> {

                    }
                }
                binding?.swipeToRefresh?.isRefreshing = false
            })

    }
    /**
     * Pagination remove loader
     */
    private fun removeLoaderFormList() {

        if (mChatHistoryList != null && mChatHistoryList?.size!! > 0 && mChatHistoryList?.get(
                mChatHistoryList?.size!! - 1
            ) != null && mChatHistoryList?.get(mChatHistoryList?.size!! - 1)?.conv_id == ""
        ) {
            mChatHistoryList?.removeAt(mChatHistoryList?.size!! - 1)
            chatHistoryAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }

    /**
     *  If we have conversation id navigate to chat page else send new message
     */

    override fun messageItemClick(
        convId: String?,
        mProfileId: String?,
        mUserName: String?,
        mUserImage: String?,
        mStatus: String?,
        mLastMessageUser: String?,
        imageView: ImageView?
    ) {
        if (convId != null && convId != "") {

            (activity as HomeActivity).updateMessageIcon()
            val intent = Intent(activity, WebSocketChatActivity::class.java)
            intent.putExtra("convId", convId)
            intent.putExtra("mProfileId", mProfileId)
            intent.putExtra("mUserName", mUserName)
            intent.putExtra("mUserImage", mUserImage)
            intent.putExtra("mStatus", mStatus)
            intent.putExtra("mLastMessageUser", mLastMessageUser)
            val imageViewPair =
                Pair.create<View, String>(imageView, getString(R.string.image_transition_name))
            val options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity(), imageViewPair)
            startActivity(intent, options.toBundle())


        } else {
            isNewChat = true
            // TODO : Send message
            mProfileIdNewChat = mProfileId ?: ""
            mNameNewChat = mUserName ?: ""
            mImageNewChat = mUserImage ?: ""
            pennySend(mProfileId ?: "", mUserName ?: "", mUserImage ?: "")
        }
    }
    /**
     * Connect google API client
     */
    override fun onStart() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(requireActivity())
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
        mGoogleApiClient?.connect()
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    /**
     * Call API when new notification received
     */
    // TODO : Refresh List
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UpdateConversationEvent) {
        Log.e("Log", event.mRefresh)
        callApiCommon()

    }
    /**
     * Delete conversation API
     *
     */
    override fun onDeleteConversation(position: Int, convId: String?, mProfileId: String?) {
        val nJwt =
            "Bearer " + SharedPrefsUtils.getStringPreference(
                activity,
                RemoteConstant.mJwt,
                ""
            )!!
        viewModelChat.deleteConversation(nJwt, convId, mProfileId)
        mChatHistoryList?.removeAt(position)
        viewModelChat.getAllChatResponseModel.postValue(
            ResultResponse.success(
                mChatHistoryList!!
            )
        )
    }
    /**
     * Sign out API
     */
    private fun signOut() {
        //
        viewModelChat.closeSocketSm()
        if (activity != null) {
                // TODO : To delete downloaded tracks
            (activity as HomeActivity).deleteDownloadedTracks()
        }
        if (InternetUtil.isInternetOn()) {
            GlobalScope.launch {
                var response: Response<Any>? = null
                kotlin.runCatching {
                    response = WebServiceClient.client.create(BackEndApi::class.java)
                        .deleteUUID(
                            SharedPrefsUtils.getStringPreference(
                                activity,
                                RemoteConstant.mAppId,
                                ""
                            )!!
                        )
                }.onSuccess {
                    when (response!!.code()) {
                        200 -> clearAll()
                        else -> {
                            RemoteConstant.apiErrorDetails(
                                requireActivity(), response?.code()!!, "Log out api"

                            )
                            clearAll()
                        }
                    }
                }.onFailure {
                    clearAll()
                }
            }
        } else {
            clearAll()
        }
    }


    /**
     *  Clear all data when logout
     *  Google Sign-out
     *  Stop running services
     *  Clear shared preference and local db
     */
    private fun clearAll() {
        // Clear all notification
        GlobalScope.launch(Dispatchers.Main) {
            try {
                if (mGoogleApiClient?.isConnected!!) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
                        println("" + it)
                    }
                }

                if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                    val serviceClass1 = MediaServiceRadio::class.java
                    val intent11 = Intent(activity, serviceClass1)
                    activity?.stopService(intent11)
                }
                if (isMyServiceRunning(MediaService::class.java)) {
                    val serviceClass1 = MediaService::class.java
                    val intent11 = Intent(activity, serviceClass1)
                    activity?.stopService(intent11)
                }
                if (activity?.getSystemService(Context.NOTIFICATION_SERVICE) != null) {
                    val nMgr =
                        activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    nMgr.cancelAll()
                }

                SharedPrefsUtils.setStringPreference(
                    activity,
                    RemoteConstant.IS_LOCATION_UPDATED,
                    "false"
                )
                SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mAppId, "")
                SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mApiKey, "")
                SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mJwt, "")
                SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mProfileId, "")
                SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mUserName, "")
                SharedPrefsUtils.setBooleanPreference(
                    activity,
                    RemoteConstant.mEnabledPushNotification,
                    false
                )
                SharedPrefsUtils.setBooleanPreference(
                    activity,
                    RemoteConstant.mEnabledPrivateProfile,
                    false
                )

//        try {
//            // clearing app data
//            val packageName: String = activity?.packageName!!
//            val runtime = Runtime.getRuntime()
//            runtime.exec("pm clear $packageName")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }


                val intent = Intent(activity, MainLandingActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.overridePendingTransition(0, 0)
                activity?.finishAffinity()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    /**
     * Get running services
     */
    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        if (activity != null) {
            val manager: ActivityManager =
                activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
        }
        return false
    }
    /**
     * Send an new penny to user
     */
    private fun pennySend(
        mProfileId: String,
        mName: String,
        mImage: String
    ) {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_penny_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val messageTextView: TextView = dialogView.findViewById(R.id.message_text_view)
        val pennyUserNameTextView: TextView =
            dialogView.findViewById(R.id.penny_user_name_text_view)
        val descriptionTextView: TextView = dialogView.findViewById(R.id.description_textView)
        val timeLinear: LinearLayout = dialogView.findViewById(R.id.time_linear)
        val pennyEditText: EditText = dialogView.findViewById(R.id.penny_edit_text)
        val closeButton: Button = dialogView.findViewById(R.id.close_button)
        val replayButton: Button = dialogView.findViewById(R.id.replay_button)
        val pennyProfileImageView: ImageView =
            dialogView.findViewById(R.id.penny_profile_image_view)
        val pennyOptionsImageView: ImageView =
            dialogView.findViewById(R.id.penny_options_image_view)
        replayButton.text = getString(R.string.send)

        pennyUserNameTextView.setOnClickListener {
            pennyProfileImageView.performClick()
        }
        pennyProfileImageView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(activity, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mProfileId)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        //TODO : Custom For Send Penny
        glideRequestManager.load(mImage).apply(
            RequestOptions.placeholderOf(R.drawable.emptypicture).error(R.drawable.emptypicture)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
        ).into(pennyProfileImageView)
        messageTextView.visibility = View.GONE
        timeLinear.visibility = View.GONE
        pennyOptionsImageView.visibility = View.GONE
        descriptionTextView.text = "Send Penny to $mName"

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        replayButton.setOnClickListener {
            if (pennyEditText.text.isNotEmpty()) {
                if (viewModelChat.isWebSocketOpen) {
                    replayButton.isClickable = false
                    replayButton.isEnabled = false

                    val mSendMessage = SendMessage()
                    val mMsgPayload = MsgPayload()

                    mSendMessage.user = mProfileId
                    mSendMessage.type = "message"

                    mMsgPayload.user = mProfileId
                    mMsgPayload.msgType = "text"
                    mMsgPayload.text = pennyEditText.text.toString()
                    mMsgPayload.timestamp = System.currentTimeMillis()
                    mSendMessage.msgPayload = mMsgPayload

                    val mGson = Gson()
                    val mMessage =
                        mGson.toJson(mSendMessage)


                    viewModelChat.sendMessageSm(mMessage)

                    Handler().postDelayed({
//                        AppUtils.showCommonToast(this, "Message send ")
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        AppUtils.hideKeyboard(activity, binding?.noDataImageView!!)
                    }, 500)
                } else {
                    if (!viewModelChat.isWebSocketOpen && !viewModelChat.isWebSocketOpenCalled) {
                        viewModelChat.connectToSocketSm()
                    }
                    Toast.makeText(activity, "Waiting for connection", Toast.LENGTH_SHORT).show()
                }
            } else {
                AppUtils.showCommonToast(
                    requireActivity(),
                    "You cannot send Penny with an empty note."
                )
            }
        }
        closeButton.setOnClickListener {
            isNewChat = false
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }

}
