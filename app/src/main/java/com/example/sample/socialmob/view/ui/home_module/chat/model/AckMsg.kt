package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class AckMsg {
    @SerializedName("msg_id")
    @Expose
    var msgId: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("filename")
    @Expose
    var filename: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null


}
