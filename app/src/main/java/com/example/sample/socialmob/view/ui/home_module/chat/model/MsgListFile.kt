package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class MsgListFile {
    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null

    @SerializedName("local_file_path")
    @Expose
    var local_file_path: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null
}
