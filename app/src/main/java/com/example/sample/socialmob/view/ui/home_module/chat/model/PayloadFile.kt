package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class PayloadFile {
    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("filename")
    @Expose
    var filename: String? = null

    @SerializedName("local_file_path")
    @Expose
    var local_file_path: String? = null

    @SerializedName("thumbnail_file_name")
    @Expose
    var thumbnail_file_name: String? = null

    @SerializedName("execution_id")
    @Expose
    var execution_id: String? = null

}
