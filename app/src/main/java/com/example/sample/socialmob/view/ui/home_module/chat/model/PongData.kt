package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.example.sample.socialmob.viewmodel.feed.PingData

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class PongData {
    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("from")
    @Expose
    var from: String? = null

    @SerializedName("ping_data")
    @Expose
    var pingData: PingData? = null
}
