package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PreSignedPayLoad {
    @SerializedName("payload")
    @Expose
     val payloadList: List<PreSignedPayLoadList>? = null
}
