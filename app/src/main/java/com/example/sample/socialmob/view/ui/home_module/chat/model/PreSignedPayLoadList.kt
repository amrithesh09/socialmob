package com.example.sample.socialmob.view.ui.home_module.chat.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName



@Entity(tableName = "PreSignedPayLoadList")
data class PreSignedPayLoadList(
    @PrimaryKey(autoGenerate = true) val _id: Int,

    @ColumnInfo(name = "url")
    var url: String? = null,

    @ColumnInfo(name = "filename")
    var filename: String? = null,

    @ColumnInfo(name = "type")
    var type: String? = null,

    @ColumnInfo(name = "status")
    var status: String? = "0"
)

