package com.example.sample.socialmob.view.ui.home_module.chat.model

import okhttp3.ResponseBody
import retrofit2.Call

class Requests(var mCall: Call<ResponseBody>, var mFileName: String, var mFileNameUrl: String, var mUserId: String)
