package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.example.sample.socialmob.view.ui.home_module.chat.model.AckMsg
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SeenMessagePayload {
    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("user")
    @Expose
    var user: String? = null

    @SerializedName("ack_msg")
    @Expose
    var ackMsg: AckMsg? = null
}

