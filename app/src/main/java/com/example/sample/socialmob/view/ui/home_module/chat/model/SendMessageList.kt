package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.example.sample.socialmob.model.chat.MsgPayload
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SendMessageList {
    @SerializedName("msg_payload")
    @Expose
    var msgPayload: List<MsgPayload>? = null
}
