package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class UploadModel {
    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("ack_msg")
    @Expose
    var ackMsg: UploadModelAckMsg? = null

}
