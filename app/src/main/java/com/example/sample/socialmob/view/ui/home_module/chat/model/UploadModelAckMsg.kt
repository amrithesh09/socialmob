package com.example.sample.socialmob.view.ui.home_module.chat.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UploadModelAckMsg {

    @SerializedName("filename")
    @Expose
     var filename: String? = null

    @SerializedName("status")
    @Expose
     var status: String? = null

}
