package com.example.sample.socialmob.view.ui.home_module.feed

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.ViewPagerAdapter
import com.example.sample.socialmob.view.utils.events.ChangeStatusBarColor
import com.example.sample.socialmob.view.utils.events.HideFeedTitileEvent
import com.example.sample.socialmob.view.utils.events.ScrollToTopEvent
import com.example.sample.socialmob.view.utils.events.StopVideoPlayBack
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.feed_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@AndroidEntryPoint
class FeedFragment : Fragment() {
    private var viewPagerAdapter: ViewPagerAdapter? = null
    private var mLastClickTime: Long = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mIsDarkModeEnabled =
            SharedPrefsUtils.getBooleanPreference(activity, RemoteConstant.NIGHT_MODE, false)


        // TODO : Select First Tab
        viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
        addPagerFragments()
        view_pager_feed.adapter = viewPagerAdapter
        view_pager_feed.offscreenPageLimit = 3


        // TODO : Disabled in V_151
//        music_grid_text_view.setOnClickListener {
//            selectTab(music_grid_text_view, feed_text_view, photo_grid_text_view)
//            view_pager_feed.currentItem = 1
//        }

        feed_text_view.setOnClickListener {
            feed_linear.performClick()
        }
        photo_grid_text_view.setOnClickListener {
            reelboard_linear.performClick()
        }
        if (mIsDarkModeEnabled) {
            EventBus.getDefault().post(
                ChangeStatusBarColor("black")
            )
            feed_text_view.setTextColor(Color.parseColor("#FFFFFF"))
            view_feed.setBackgroundColor(Color.parseColor("#FFFFFF"))

            photo_grid_text_view.setTextColor(Color.parseColor("#747d8c"))
            view_reelboard.setBackgroundColor(Color.parseColor("#1A1B1C"))

        } else {
            feed_text_view.setTextColor(Color.parseColor("#1e90ff"))
            view_feed.setBackgroundColor(Color.parseColor("#1e90ff"))

            photo_grid_text_view.setTextColor(Color.parseColor("#747d8c"))
            view_reelboard.setBackgroundColor(Color.parseColor("#FFFFFF"))
        }


        feed_linear.setOnClickListener {
            //            selectTab(feed_text_view, music_grid_text_view, photo_grid_text_view)


            if (mIsDarkModeEnabled) {
                EventBus.getDefault().post(
                    ChangeStatusBarColor("black")
                )
                feed_text_view.setTextColor(Color.parseColor("#FFFFFF"))
                view_feed.setBackgroundColor(Color.parseColor("#FFFFFF"))

                photo_grid_text_view.setTextColor(Color.parseColor("#747d8c"))
                view_reelboard.setBackgroundColor(Color.parseColor("#1A1B1C"))

            } else {
                EventBus.getDefault().post(ChangeStatusBarColor("white"))
                feed_text_view.setTextColor(Color.parseColor("#1e90ff"))
                view_feed.setBackgroundColor(Color.parseColor("#1e90ff"))

                photo_grid_text_view.setTextColor(Color.parseColor("#747d8c"))
                view_reelboard.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }
            view_pager_feed.currentItem = 0
        }
        reelboard_linear.setOnClickListener {

            //            selectTab(photo_grid_text_view, feed_text_view, music_grid_text_view)
            if (mIsDarkModeEnabled) {

                EventBus.getDefault().post(ChangeStatusBarColor("black"))
                feed_text_view.setTextColor(Color.parseColor("#747d8c"))
                view_feed.setBackgroundColor(Color.parseColor("#1A1B1C"))

                photo_grid_text_view.setTextColor(Color.parseColor("#FFFFFF"))
                view_reelboard.setBackgroundColor(Color.parseColor("#FFFFFF"))


            } else {
                EventBus.getDefault().post(ChangeStatusBarColor("white"))
                EventBus.getDefault().post(StopVideoPlayBack(true))

                feed_text_view.setTextColor(Color.parseColor("#747d8c"))
                view_feed.setBackgroundColor(Color.parseColor("#FFFFFF"))

                view_reelboard.setBackgroundColor(Color.parseColor("#1e90ff"))
                photo_grid_text_view.setTextColor(Color.parseColor("#1e90ff"))
            }

            view_pager_feed.currentItem = 1

        }
        notification_icon_image_view.setOnClickListener {
            val intent = Intent(activity, CommonFragmentActivity::class.java)
            intent.putExtra("isFrom", "Notification")
            startActivity(intent)
        }
        search_icon_linear.setOnClickListener {
            search_icon_image_view.performClick()
        }
        search_icon_image_view.setOnClickListener {

            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            } else {
                EventBus.getDefault().post(
                    StopVideoPlayBack(true)
                )
                val intent = Intent(activity, CommonFragmentActivity::class.java)
                intent.putExtra("isFrom", "FeedSearch")
                intent.putExtra("isLongPress", "false")
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        return layoutInflater.inflate(R.layout.feed_fragment, container, false)
    }


    private fun addPagerFragments() {
        viewPagerAdapter?.addFragments(MainFeedFragment())
        viewPagerAdapter?.addFragments(PhotoGridFragment())

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: HideFeedTitileEvent) {
        // TODO : Hide Title Linear
        if (event.mHide) {
            feed_text_view.isClickable = false
            music_grid_text_view.isClickable = false
            photo_grid_text_view.isClickable = false
            header_linear.visibility = View.GONE
        } else {
            feed_text_view.isClickable = true
            music_grid_text_view.isClickable = true
            photo_grid_text_view.isClickable = true
            header_linear.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    // TODO : Double tab scroll to top
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ScrollToTopEvent) {
        Log.e("Log", event.mScrollToTop)
        if (focus_edit_text != null) {
            focus_edit_text.requestFocus()
        }

    }
}
