package com.example.sample.socialmob.view.ui.home_module.feed

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.PopupWindow
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.FeedImageDetailsActivtyBinding
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.settings.HelpFeedBackReportProblemActivity
import com.example.sample.socialmob.view.ui.profile.feed.CommentListActivity
import com.example.sample.socialmob.view.ui.profile.feed.PostCommentAdapter
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.feed.FeedImageViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.feed_image_details_activty.*
import kotlinx.android.synthetic.main.options_menu_feed_option.view.*
import javax.inject.Inject

@AndroidEntryPoint
class FeedImageDetailsActivity : BaseCommonActivity(),
    PostCommentAdapter.OptionPostItemClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private val feedImageViewModel: FeedImageViewModel by viewModels()
    private var mPostId = ""
    private var feedImageDetailsActivtyBinding: FeedImageDetailsActivtyBinding? = null
    private var mFeedResponse: SingleFeedResponseModel? = null
    private var mLikeCount = ""
    private var mCommentCount = ""
    private var isLiked: Boolean = false
    private var mIsMyPost: Boolean = false
    private var mGlobalCommentList: MutableList<CommentResponseModelPayloadComment>? = ArrayList()
    private var mCommentAdapter: PostCommentAdapter? = null
    private var isCalledHashTgaDetailsPage: Boolean = false
    private var mStatus: ResultResponse.Status? = null
    private var isLoaderAdded: Boolean = false
    private var mLastClickTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        feedImageDetailsActivtyBinding =
            DataBindingUtil.setContentView(this, R.layout.feed_image_details_activty)

        feedImageDetailsActivtyBinding?.profileImage =
            SharedPrefsUtils.getStringPreference(
                this@FeedImageDetailsActivity,
                RemoteConstant.mUserImage,
                ""
            )!!
        feedImageDetailsActivtyBinding?.executePendingBindings()
        val i = intent
        if (i.getStringExtra("mPostId") != null) {
            mPostId = i.getStringExtra("mPostId")!!

        }
        feedImageDetailsActivtyBinding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }

        feedImageDetailsActivtyBinding?.commentLinear?.setOnClickListener {
            val intent = Intent(this, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mIsMyPost", mIsMyPost)
            intent.putExtra("mPosition", 0)
            startActivity(intent)
        }
        feedImageDetailsActivtyBinding?.likeLinear?.setOnClickListener {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(this, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        feedImageDetailsActivtyBinding?.likeMaterialButton?.setOnClickListener {

            if (!feedImageDetailsActivtyBinding?.likeMaterialButton?.isFavorite!!) {
                feedImageDetailsActivtyBinding?.likeMaterialButton?.isFavorite = false
                onFeedItemLikeClick(mPostId, "false")
            } else {
                feedImageDetailsActivtyBinding?.likeMaterialButton?.isFavorite = true
                onFeedItemLikeClick(mPostId, "true")
            }
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                feedImageDetailsActivtyBinding?.likeCountTextView?.setTextColor(Color.parseColor("#1e90ff"))
            } else {
                feedImageDetailsActivtyBinding?.likeCountTextView?.setTextColor(Color.parseColor("#747d8c"))
            }
        }
        feedImageDetailsActivtyBinding?.feedPostOptionImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View =
                LayoutInflater.from(this).inflate(R.layout.options_menu_feed_option, viewGroup)
            val mQQPop = PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(
                feedImageDetailsActivtyBinding?.feedPostOptionImageView,
                -180,
                -20
            )
            if (mFeedResponse?.payload?.post?.action?.post?.own == true) {
                view.edit_post.visibility = View.VISIBLE
                view.delete_post.visibility = View.VISIBLE
                view.share_post.visibility = View.VISIBLE
            } else {
                view.edit_post.visibility = View.GONE
                view.delete_post.visibility = View.GONE
                view.share_post.visibility = View.VISIBLE
                view.report_post.visibility = View.VISIBLE
            }
            view.report_post.setOnClickListener {
                mQQPop.dismiss()
                val intent = Intent(this, HelpFeedBackReportProblemActivity::class.java)
                intent.putExtra("mTitle", getString(R.string.report))
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
            view.share_post.setOnClickListener {
                ShareAppUtils.sharePost(
                    mFeedResponse?.payload?.post?.action?.source,
                    mFeedResponse?.payload?.post?.action?.post?.id!!, this
                )
                mQQPop.dismiss()
            }

            view.edit_post.setOnClickListener {
                val intent = Intent(this, UploadPhotoActivityMentionHashTag::class.java)
                intent.putExtra("mPostId", mPostId)
                intent.putExtra("mIsEdit", true)
                startActivity(intent)
                mQQPop.dismiss()
            }
            view.delete_post.setOnClickListener {
                val dialogBuilder = this.let { AlertDialog.Builder(this) }
                val inflater = layoutInflater
                val dialogView = inflater.inflate(R.layout.common_dialog, null)
                dialogBuilder.setView(dialogView)
                dialogBuilder.setCancelable(false)

                val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                val okButton: Button = dialogView.findViewById(R.id.ok_button)

                val b = dialogBuilder.create()
                b?.show()
                okButton.setOnClickListener {
                    feedImageViewModel.deletePost(mPostId, this)
                    mQQPop.dismiss()
                    b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    onBackPressed()
                }
                cancelButton.setOnClickListener {
                    mQQPop.dismiss()
                    b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                }
            }
        }

        feedImageDetailsActivtyBinding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
            val view =
                feedImageDetailsActivtyBinding?.nestedScrollView?.getChildAt(
                    feedImageDetailsActivtyBinding?.nestedScrollView?.childCount!! - 1
                )

            val diff =
                view?.bottom?:0  - (feedImageDetailsActivtyBinding?.nestedScrollView?.height!! + feedImageDetailsActivtyBinding?.nestedScrollView?.scrollY!!)

            if (diff == 0) {
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    && mGlobalCommentList != null
                    && mGlobalCommentList!!.isNotEmpty() && mGlobalCommentList!!.size >= 15 &&
                    !isLoaderAdded
                ) {
                    loadMoreItems(mGlobalCommentList!![mGlobalCommentList!!.size - 1].commentId!!)
                }

            }
        }

        feedImageDetailsActivtyBinding?.descriptionTextView?.setOnMentionClickListener { view, text ->
            //            AppUtils.showCommonToast(this, "" + text)
            for (mMentionItem in 0 until mFeedResponse?.payload?.post?.action?.post?.mentions?.size!!) {
                if (mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?.text?.contains(
                        text
                    )!!
                ) {
                    if (mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?._id != null) {
                        val intent = Intent(this, UserProfileActivity::class.java)
                        intent.putExtra(
                            "mProfileId",
                            mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?._id
                        )
                        startActivity(intent)
                    }
                }
            }
        }
        feedImageDetailsActivtyBinding?.descriptionTextView?.setOnHashtagClickListener { view, text ->
            for (mHashTagItem in 0 until mFeedResponse?.payload?.post?.action?.post?.hashTags?.size!!) {
                if (mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(mHashTagItem)?.name!!.equals(
                        text.toString(),
                        ignoreCase = true
                    )
                ) {
                    if (mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(mHashTagItem)?._id != null) {

                        if (!isCalledHashTgaDetailsPage) {
                            isCalledHashTgaDetailsPage = true
                            onOptionItemClickOptionHashTag(
                                mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(
                                    mHashTagItem
                                )?._id!!,
                                "",
                                "hashTagFeed",
                                mFeedResponse?.payload?.post?.action?.post?.id!!,
                                mFeedResponse?.payload?.post?.action?.post?.likesCount!!,
                                mFeedResponse?.payload?.post?.action?.post?.commentsCount!!
                            )
                            Handler().postDelayed({
                                isCalledHashTgaDetailsPage = false
                            }, 1000)
                        }

                    }
                }
            }
        }
        mCommentAdapter = PostCommentAdapter(
            this@FeedImageDetailsActivity, this, mIsMyPost, glideRequestManager
        )
        feedImageDetailsActivtyBinding?.postCommentsRecyclerView?.adapter = mCommentAdapter
        feedImageDetailsActivtyBinding?.profileFeedNameTextView?.setOnClickListener {
            feedImageDetailsActivtyBinding?.profileImageView?.performClick()
        }
        feedImageDetailsActivtyBinding?.profileImageView?.setOnClickListener {
            val intent = Intent(this, UserProfileActivity::class.java)
            intent.putExtra("mProfileId", mFeedResponse?.payload?.post?.actor?.id)
            startActivity(intent)
        }
        feedImageDetailsActivtyBinding?.imagePostBackImageView?.setOnClickListener {
            onBackPressed()
        }


        // TODO : Double TAP
        feedImageDetailsActivtyBinding?.heartAnim?.visibility = View.INVISIBLE

    }

    override fun onResume() {
        mGlobalCommentList?.clear()
        isLoaderAdded = false
        callApiCommon()
        super.onResume()
    }

    private fun onOptionItemClickOptionHashTag(
        mId: String, mPosition: String, mPath: String, mPostId: String, mLikeCount: String,
        mCommentCount: String
    ) {
        val detailIntent = Intent(this, PhotoGridDetailsActivity::class.java)
        detailIntent.putExtra("mId", mId)
        detailIntent.putExtra("mPosition", mPosition)
        detailIntent.putExtra("mPath", mPath)
        detailIntent.putExtra("mPostId", mPostId)
        detailIntent.putExtra("mLikeCount", mLikeCount)
        detailIntent.putExtra("mCommentCount", mCommentCount)
        startActivityForResult(detailIntent, 1501)
    }

    private fun callApiComments(mPostId: String, size: String) {
        val mAuth =
            RemoteConstant.getAuthCommentList(
                this@FeedImageDetailsActivity, mPostId, size, RemoteConstant.mCount.toInt()
            )
        feedImageViewModel.getFeedComment(mAuth, mPostId, size, RemoteConstant.mCount)
        observeData()
    }


    private fun observeData() {
        feedImageViewModel.commentPaginatedResponseModel?.nonNull()?.observe(this,
            { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mGlobalCommentList = resultResponse?.data!!.toMutableList()

                        mCommentAdapter?.submitList(mGlobalCommentList)
                        mCommentCount = mGlobalCommentList?.size.toString()

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mGlobalCommentList?.addAll(resultResponse?.data!!)
                        mCommentAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {

                        if (mGlobalCommentList?.size == 0) {
                            println("Empty")
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {

                    }
                    ResultResponse.Status.LOADING -> {

                    }
                    else -> {
                    }
                }
            })


        feedImageViewModel.commentPaginatedResponseModelCommentCount.nonNull().observe(this, {
            mCommentCount = it!!
        })

        feedImageDetailsActivtyBinding?.linearComment?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(this, CommentListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                intent.putExtra("mIsMyPost", mIsMyPost)
                intent.putExtra("mPosition", 0)
                intent.putExtra("needToFocus", true)

                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()

        }
        feedImageDetailsActivtyBinding?.commentProfileImageView?.setOnClickListener {
            feedImageDetailsActivtyBinding?.linearComment?.performClick()
        }
        feedImageDetailsActivtyBinding?.commentEditText?.setOnClickListener {
            feedImageDetailsActivtyBinding?.linearComment?.performClick()
        }
    }

    private fun loadMoreItems(size: String) {
        val mCommentList: MutableList<CommentResponseModelPayloadComment> = ArrayList()
        val mList = CommentResponseModelPayloadComment()
        mList.commentId = ""
        mCommentList.add(mList)
        mGlobalCommentList?.addAll(mCommentList)
        mCommentAdapter?.notifyDataSetChanged()
        isLoaderAdded = true
        callApiComments(mPostId, size)
    }

    private fun removeLoaderFormList() {
        if (mGlobalCommentList != null &&
            mGlobalCommentList?.size!! > 0 &&
            mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1) != null &&
            mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1)?.commentId == ""
        ) {
            mGlobalCommentList?.removeAt(mGlobalCommentList?.size!! - 1)
            mCommentAdapter?.notifyDataSetChanged()
            isLoaderAdded = false
        }
    }

    private fun onFeedItemLikeClick(mPostId: String, isLike: String) {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        } else {
            if (isLike == "true") {
                disLikePost(mPostId)
            } else {
                likePost(mPostId)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun likePost(postId: String) {
        isLiked = true
        val mAuth = RemoteConstant.getAuthLikePost(this, postId)
        feedImageViewModel.likePost(mAuth, postId)
        feedImageDetailsActivtyBinding?.likeMaterialButton?.isFavorite = true
        val mCount =
            Integer.parseInt(mFeedResponse?.payload?.post?.action?.post?.likesCount!!).plus(1)
        mFeedResponse?.payload?.post?.action?.post?.likesCount = mCount.toString()
        feedImageDetailsActivtyBinding?.likeCountTextView?.text = "$mCount Likes"
    }

    @SuppressLint("SetTextI18n")
    private fun disLikePost(postId: String) {
        isLiked = false
        val mAuth = RemoteConstant.getAuthDislikePost(this, postId)
        feedImageViewModel.disLikePost(mAuth, postId)
        feedImageDetailsActivtyBinding?.likeMaterialButton?.isFavorite = false
        val mCount =
            Integer.parseInt(mFeedResponse?.payload?.post?.action?.post?.likesCount!!).minus(1)
        mFeedResponse?.payload?.post?.action?.post?.likesCount = mCount.toString()
        feedImageDetailsActivtyBinding?.likeCountTextView?.text = "$mCount Likes"
    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi(mPostId)
            callApiComments(mPostId, "0")
            feedImageDetailsActivtyBinding?.isVisibleList = false
            feedImageDetailsActivtyBinding?.isVisibleLoading = true
            feedImageDetailsActivtyBinding?.isVisibleNoInternet = false
            feedImageDetailsActivtyBinding?.detailsCardView?.visibility = View.GONE

        } else {
            feedImageDetailsActivtyBinding?.detailsCardView?.visibility = View.GONE
            feedImageDetailsActivtyBinding?.isVisibleList = false
            feedImageDetailsActivtyBinding?.isVisibleLoading = false
            feedImageDetailsActivtyBinding?.isVisibleNoInternet = true
            val shake: Animation =
                AnimationUtils.loadAnimation(this@FeedImageDetailsActivity, R.anim.shake)
            feedImageDetailsActivtyBinding?.noInternetLinear?.startAnimation(shake) // starts animation
        }
    }

    private fun callApi(mPostId: String) {
        feedImageViewModel.getFeedDetails(mPostId)
        feedImageViewModel.responseModel.nonNull().observe(this, { responseModel ->
            if (responseModel?.payload?.deleted != true) {
                feedImageDetailsActivtyBinding?.detailsCardView?.visibility = View.VISIBLE
                feedImageDetailsActivtyBinding?.isVisibleList = true
                feedImageDetailsActivtyBinding?.isVisibleLoading = false
                feedImageDetailsActivtyBinding?.isVisibleNoInternet = false
                mFeedResponse = responseModel

                if (responseModel.payload?.post?.action?.post?.liked != null) {
                    isLiked = responseModel.payload?.post?.action?.post?.liked!!
                }
                if (responseModel.payload?.post?.action?.post?.likesCount != null) {
                    mLikeCount = responseModel.payload?.post?.action?.post?.likesCount!!
                }
                if (responseModel.payload?.post?.action?.post?.commentsCount != null) {
                    mCommentCount = responseModel.payload?.post?.action?.post?.commentsCount!!
                }
                if (responseModel.payload?.post?.action?.post?.commentsCount != null) {
                    mCommentCount = responseModel.payload?.post?.action?.post?.commentsCount!!
                }
                if (responseModel.payload?.post?.action?.post?.own != null) {
                    mIsMyPost = responseModel.payload?.post?.action?.post?.own!!

                    // TODO : Since we need to delete all the comment OWN Post
                    mCommentAdapter?.setIsOwn(mIsMyPost)
                }

                feedImageDetailsActivtyBinding?.singleFeedResponseModel = mFeedResponse

                if (responseModel.payload != null && responseModel.payload?.post?.action != null && responseModel.payload?.post?.action?.post != null &&
                    responseModel.payload?.post?.action?.post?.media != null && responseModel.payload?.post?.action?.post?.media?.isNotEmpty()!!
                ) {

                    // TODO : Set up view with ratio
                    when (responseModel.payload?.post?.action?.post?.media!![0].ratio) {
                        "1:1" -> {
                            feedImageDetailsActivtyBinding?.aspectRatioLayout?.setAspectRatio(
                                1F,
                                1F
                            )
                        }
                        "3:4" -> {
                            feedImageDetailsActivtyBinding?.aspectRatioLayout?.setAspectRatio(
                                3F,
                                4F
                            )
                        }
                        "4:3" -> {
                            feedImageDetailsActivtyBinding?.aspectRatioLayout?.setAspectRatio(
                                4F,
                                3F
                            )
                        }
                        "16:9" -> {
                            feedImageDetailsActivtyBinding?.aspectRatioLayout?.setAspectRatio(
                                16F,
                                9F
                            )
                        }
                        else -> {
                            feedImageDetailsActivtyBinding?.aspectRatioLayout?.setAspectRatio(
                                1F,
                                1F
                            )
                        }
                    }


                    val adapter = FeedImageListAdapterInner(
                        responseModel.payload?.post?.action?.post?.media!!,
                        this, mPostId, glideRequestManager
                    )
                    feedImageDetailsActivtyBinding?.imageListpager?.offscreenPageLimit = 5
                    feedImageDetailsActivtyBinding?.imageListpager?.adapter = adapter

                    if (responseModel.payload?.post?.action?.post?.media?.size!! > 1) {
                        feedImageDetailsActivtyBinding?.countTextView?.visibility = View.VISIBLE

                        feedImageDetailsActivtyBinding?.imagePosition = "" + 1 + "/" +
                                responseModel.payload?.post?.action?.post?.media?.size
                        feedImageDetailsActivtyBinding?.imageListpager?.addOnPageChangeListener(
                            object :
                                androidx.viewpager.widget.ViewPager.OnPageChangeListener {

                                override fun onPageScrollStateChanged(state: Int) {
                                }

                                override fun onPageScrolled(
                                    position: Int,
                                    positionOffset: Float,
                                    positionOffsetPixels: Int
                                ) {

                                }

                                override fun onPageSelected(position: Int) {
                                    feedImageDetailsActivtyBinding?.imagePosition =
                                        "" + position.plus(1) + "/" +
                                                responseModel.payload?.post?.action?.post?.media?.size
                                }

                            })
                    } else {
                        feedImageDetailsActivtyBinding?.countTextView?.visibility = View.GONE
                    }
                }
            } else {
                AppUtils.showCommonToast(this, "Post Deleted")
                finish()
            }
        })
    }

    override fun onBackPressed() {
        val intent = intent
        intent.putExtra("COMMENT_COUNT", mCommentCount)
        intent.putExtra("LIKE_COUNT", mLikeCount)
        intent.putExtra("POST_ID", mPostId)
        intent.putExtra("IS_LIKED", isLiked)
        intent.putExtra("IS_FROM_COMMENT_LIST", false)
        setResult(1501, intent)
        AppUtils.hideKeyboard(this@FeedImageDetailsActivity, image_post_back_image_view)
        super.onBackPressed()
    }

    override fun onOptionItemClickOption(
        mMethod: String, mId: String, mComment: String, mPosition: Int
    ) {
        when (mMethod) {
//            "editComment" -> editComment(mId, mComment)
            "deleteComment" -> deleteComment(mId, mPosition)
        }
    }

    /**
     * Delete post commment API
     */
    @SuppressLint("SetTextI18n")
    private fun deleteComment(mId: String, mPosition: Int) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mPostId +
                    RemoteConstant.mPathComment +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        feedImageViewModel.deleteComment(mAuth, mPostId, mId)
        feedImageViewModel.commentDeleteResponseModel.observe(this, {
            if (it?.payload?.commentsCount != null) {
                mCommentCount = it.payload.commentsCount
                feedImageDetailsActivtyBinding?.commentCount?.text = "$mCommentCount Comments"
            } else {
                mCommentCount = "0"
                feedImageDetailsActivtyBinding?.commentCount?.text = "$mCommentCount Comments"

            }
        })

        mGlobalCommentList?.removeAt(mPosition)
        mCommentAdapter?.notifyDataSetChanged()

    }

}
