package com.example.sample.socialmob.view.ui.home_module.feed

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ItemImageBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.google.gson.Gson

class FeedImageGalleryGridAdapter(
    private val clickListener: GridImageItemClickListener,
    private var glideRequestManager: RequestManager
) : ListAdapter<PersonalFeedResponseModelPayloadFeed, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PersonalFeedResponseModelPayloadFeed>() {
                override fun areItemsTheSame(
                    oldItem: PersonalFeedResponseModelPayloadFeed,
                    newItem: PersonalFeedResponseModelPayloadFeed
                ): Boolean {
                    return oldItem.action?.actionId == newItem.action?.actionId
                }

                override fun areContentsTheSame(
                    oldItem: PersonalFeedResponseModelPayloadFeed,
                    newItem: PersonalFeedResponseModelPayloadFeed
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_image, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(getItem(holder.adapterPosition))
    }

    inner class MyViewHolder(val binding: ItemImageBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            if (getItem(adapterPosition)?.action?.post?.media?.isNotEmpty()!! &&
                getItem(adapterPosition)?.action?.post?.media?.get(0)?.sourcePath != null
            ) {
                if (getItem(adapterPosition)?.action?.post?.type == "user_video_post") {
                    glideRequestManager.load(
                        getItem(adapterPosition)?.action?.post?.media?.get(0)?.thumbnail
                    )
                        .apply(RequestOptions().centerCrop().placeholder(0))
                        .thumbnail(0.1f)
                        .into(binding.gridImageView)
                } else {

                    glideRequestManager.load(
                        RemoteConstant.getImageUrlFromWidth(
                            getItem(adapterPosition)?.action?.post?.media?.get(0)?.sourcePath,
                            250
                        )
                    )
                        .apply(RequestOptions().centerCrop().placeholder(0))
                        .thumbnail(0.1f).into(binding.gridImageView)
                }
            }
            if (getItem(adapterPosition)?.action?.post?.type != null) {
                binding.isVideo =
                    getItem(adapterPosition)?.action?.post?.type == "user_video_post"
            }

            binding.root.setOnClickListener {
                if (itemCount > 0 && getItem(adapterPosition)?.action != null && getItem(
                        adapterPosition
                    )?.action?.post != null
                    && getItem(adapterPosition)?.action?.post?.media != null &&
                    getItem(adapterPosition)?.action?.post?.media?.isNotEmpty()!! &&
                    getItem(adapterPosition)?.action?.post?.media?.get(0)?.sourcePath != null
                ) {

                    if (adapterPosition != androidx.recyclerview.widget.RecyclerView.NO_POSITION) {

                        var mRecordId = "0"

                        if (itemCount > 0 && getItem(adapterPosition)?.recordId != null) {
                            mRecordId = if (adapterPosition != 0) {
                                getItem(adapterPosition - 1).recordId?:""
                            } else {
                                "0"
                            }
                        }

                        var feedJson: String? = ""
                        if (itemCount > 0) {
                            try {
                                val gson = Gson()
                                feedJson = gson.toJson(
                                    getItem(adapterPosition)
                                )
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            clickListener.onItemClick(
                                adapterPosition,
                                getItem(adapterPosition)?.action?.post?.id?:"",
                                mRecordId,
                                feedJson?:"",
                                getItem(adapterPosition)?.type?:""
                            )
                        }
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).action != null && getItem(position).action?.actionId != null) {
            if (getItem(position).action?.actionId != "") {
                ITEM_DATA
            } else {
                ITEM_LOADING
            }
        } else {
            ITEM_LOADING
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
        }
    }

    interface GridImageItemClickListener {
        fun onItemClick(
            item: Int, mPostId: String, mRecordId: String, mList: String, type: String
        )
    }
}