package com.example.sample.socialmob.view.ui.home_module.feed

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPostMedium
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.AspectRatioLayout
import com.example.sample.socialmob.view.utils.GlideImageLoder.GlideImageLoader
import com.example.sample.socialmob.view.utils.views.AspectRatioImageView


class FeedImageListAdapterInner(
    private var media: List<PersonalFeedResponseModelPayloadFeedActionPostMedium>?,
    private var mContext: Activity,
    private var id: String, private var requestManager: RequestManager
) : PagerAdapter() {

    private var mLayoutInflater: LayoutInflater =
        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return media?.size ?:0
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = mLayoutInflater.inflate(R.layout.layout_item_image_list, container, false)

        val aspectRatioLayout: AspectRatioLayout = itemView.findViewById(R.id.aspect_ratio_layout)
        val imageView: AspectRatioImageView = itemView.findViewById(R.id.image_view)
        val playImageView: ImageView = itemView.findViewById(R.id.play_image_view)
        val progressCircular: ProgressBar = itemView.findViewById(R.id.progress_circular)

        if (media?.get(0)?.sourcePath?.contains(".mp4")!!) {
            playImageView.visibility = View.VISIBLE

            val options: RequestOptions =
                RequestOptions.placeholderOf(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .error(R.drawable.white_bg)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)

            GlideImageLoader(
                imageView,
                progressCircular
            ).load(
                RemoteConstant.getImageUrlFromWidth(
                    media?.get(position)?.thumbnail,
                    RemoteConstant.getWidth(mContext) / 2
                ), options
            )
            imageView.setOnClickListener {
                if (id != "") {
                    val intent = Intent(mContext, FeedVideoDetailsActivity::class.java)
                    intent.putExtra("mPostId", id)
                    mContext.startActivityForResult(intent, 1501)
                }
            }

        } else {

            if (media?.get(position)?.ratio != null && media?.get(position)?.ratio != "") {
                val separated = media?.get(position)?.ratio?.split(":")
                separated?.get(0)
                separated?.get(1)
                imageView.setWidthRatio(separated?.get(0)?.toInt()?:1)
                imageView.setHeightRatio(separated?.get(1)?.toInt()?:1)

                when (media?.get(position)?.ratio) {
                    "1:1" -> {
                        aspectRatioLayout.setAspectRatio(1F, 1F)
                    }
                    "3:4" -> {
                        aspectRatioLayout.setAspectRatio(3F, 4F)
                    }
                    "4:3" -> {
                        aspectRatioLayout.setAspectRatio(4F, 3F)
                    }
                    "16:9" -> {
                        aspectRatioLayout.setAspectRatio(16F, 9F)
                    }
                    else -> {
                        aspectRatioLayout.setAspectRatio(1F, 1F)
                    }
                }
            } else {
                imageView.setWidthRatio(1)
                imageView.setHeightRatio(1)
            }

            requestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        media?.get(position)?.sourcePath,
                        RemoteConstant.getWidth(mContext) / 2
                    )
                )

                .apply(
                    RequestOptions.placeholderOf(R.drawable.ic_place_holder)
                        .error(R.drawable.ic_place_holder).centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                )
                .transition(withCrossFade())
                .thumbnail(0.1f)
                .into(imageView)


            playImageView.visibility = View.GONE
        }
        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as AspectRatioLayout)
    }
}