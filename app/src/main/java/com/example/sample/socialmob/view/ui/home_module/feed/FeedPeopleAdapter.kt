package com.example.sample.socialmob.view.ui.home_module.feed

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemFeedFollowBinding
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleAdapter
import com.example.sample.socialmob.view.utils.BaseViewHolder
import org.jetbrains.anko.layoutInflater

class FeedPeopleAdapter(
    private var profileItems: MutableList<SearchProfileResponseModelPayloadProfile>?,
    private val searchPeopleAdapterFollowItemClick: SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick,
    private val searchPeopleAdapterItemClick: SearchPeopleAdapter.SearchPeopleAdapterItemClick,
    private val mContext: Context,
    private val glideRequestManager: RequestManager
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_VIEW_ALL = 2
    }

    private val mInflater: LayoutInflater = LayoutInflater.from(mContext)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_VIEW_ALL -> bindViewAll(parent)
            else -> bindData(parent)
        }
    }

    private fun bindViewAll(parent: ViewGroup): BaseViewHolder<Any> {
        return ViewAllHolder(mInflater.inflate(R.layout.memmbers_item_view_all, parent, false))
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFeedFollowBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_feed_follow, parent, false
        )
        return ProfileViewHolder(binding)
    }


    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(profileItems?.get(position))

    }

    override fun getItemCount(): Int {
        return profileItems?.size ?: 0
    }

    inner class ViewAllHolder(itemView: View) : BaseViewHolder<Any>(itemView) {


        override fun bind(genre: Any) {
            itemView.setOnClickListener {
                val intent = Intent(mContext, CommonFragmentActivity::class.java)
                intent.putExtra("isFrom", "FeedSearch")
                mContext.startActivity(intent)
            }
        }
    }

    inner class ProfileViewHolder(val binding: LayoutItemFeedFollowBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.profileViewModel = profileItems?.get(adapterPosition)
            binding.executePendingBindings()
            if (profileItems?.get(adapterPosition)?.pimage != "") {
                glideRequestManager.load(
                    RemoteConstant.getImageUrlFromWidth(
                        profileItems?.get(adapterPosition)?.pimage, 200
                    )
                ).into(binding.profileImageView)
            } else {
                glideRequestManager.load(R.drawable.emptypicture)
                    .into(binding.profileImageView)
            }
            // Social-mob official id's user can't un-follow
            when (profileItems?.get(adapterPosition)?._id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding.followingTextView.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding.followingTextView.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding.followingTextView.visibility = View.GONE
                }
            }
            binding.followingTextView.setOnClickListener {

                if (profileItems?.get(adapterPosition)?.relation == "following") {
                    val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                    val inflater = mContext.layoutInflater
                    val dialogView = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            profileItems?.get(adapterPosition)?.relation ?: "",
                            profileItems?.get(adapterPosition)?._id ?: ""
                        )
                        Handler().postDelayed({
                            notify(
                                adapterPosition,
                                profileItems?.get(adapterPosition)?.privateProfile
                            )
                        }, 100)
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        profileItems?.get(adapterPosition)?.relation ?: "",
                        profileItems?.get(adapterPosition)?._id ?: ""
                    )
                    Handler().postDelayed({
                        notify(adapterPosition, profileItems?.get(adapterPosition)?.privateProfile)
                    }, 100)
                }


            }
            binding.profileImageView.setOnClickListener {
                searchPeopleAdapterItemClick.onSearchPeopleAdapterItemClick(
                    profileItems?.get(adapterPosition)?._id ?: "",
                    profileItems?.get(adapterPosition)?.own ?: ""
                )
            }
            binding.closeImageView.setOnClickListener {
                notifyDelete(adapterPosition)
            }

        }

    }

    private fun notifyDelete(itemPosition: Int) {
        if (profileItems?.size ?: 0 > 0) {
            profileItems?.removeAt(itemPosition)
            notifyItemRemoved(itemPosition)
            notifyItemRangeChanged(itemPosition, profileItems?.size ?: 0)
        }
    }

    private fun notify(adapterPosition: Int, privateProfile: String?) {
        if (privateProfile == "false") {
            if (profileItems?.get(adapterPosition)?.relation == "following") {
                profileItems?.get(adapterPosition)?.relation = "none"
            } else {
                profileItems?.get(adapterPosition)?.relation = "following"
            }
        } else {
            when (profileItems?.get(adapterPosition)?.relation) {
                "none" -> profileItems?.get(
                    adapterPosition
                )?.relation =
                    "request pending"
                "following" -> profileItems?.get(
                    adapterPosition
                )?.relation =
                    "none"
                "request pending" -> profileItems?.get(
                    adapterPosition
                )?.relation =
                    "none"
            }
        }

        notifyItemChanged(adapterPosition, profileItems?.size)
    }

    override fun getItemViewType(position: Int): Int {

        return if (profileItems?.get(position)?._id ?: "" != "") {
            ITEM_DATA
        } else {
            ITEM_VIEW_ALL
        }
    }
}