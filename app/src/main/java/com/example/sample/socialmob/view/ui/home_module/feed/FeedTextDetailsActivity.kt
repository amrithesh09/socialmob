package com.example.sample.socialmob.view.ui.home_module.feed

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.PopupWindow
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.FeedTextDetailsActivtyBinding
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.settings.HelpFeedBackReportProblemActivity
import com.example.sample.socialmob.view.ui.profile.feed.CommentListActivity
import com.example.sample.socialmob.view.ui.profile.feed.PostCommentAdapter
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedAdapter
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.WritePostActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.viewmodel.feed.FeedImageViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.feed_image_details_activty.*
import kotlinx.android.synthetic.main.options_menu_feed_option.view.*
import java.util.regex.Pattern
import javax.inject.Inject

@AndroidEntryPoint
class FeedTextDetailsActivity : BaseCommonActivity(),
        PostCommentAdapter.OptionPostItemClickListener,
        ProfileFeedAdapter.FeedItemLikeClick,
        ProfileFeedAdapter.FeedCommentClick, ProfileFeedAdapter.FeedLikeListClick,
        ProfileFeedAdapter.OptionItemClickListener, ProfileFeedAdapter.FeedItemClick,
        ProfileFeedAdapter.HashTagClickListener, ProfileFeedAdapter.AdapterFollowItemClick,
        ProfileFeedAdapter.HyperLink,
        ProgressRequestBody.UploadCallbacks, ProfileFeedAdapter.HyperLinkUrl{

    @Inject
    lateinit var glideRequestManager: RequestManager

    private val feedImageViewModel: FeedImageViewModel by viewModels()
    private var mPostId = ""
    private var feedTextDetailsActivityBinding: FeedTextDetailsActivtyBinding? = null

    private var ADD_COMMENT_RESPONSE_CODE = 1501
    private var mFeedResponse: SingleFeedResponseModel? = null
    private var mLikeCount = ""
    private var mCommentCount = ""
    private var isLiked: Boolean = false
    private var mIsMyPost: Boolean = false
    private var mGlobalCommentList: MutableList<CommentResponseModelPayloadComment>? = ArrayList()
    private var mCommentAdapter: PostCommentAdapter? = null
    private var isLoaderAdded: Boolean = false
    private var isCalledHashTgaDetailsPage: Boolean = false
    private var mStatus: ResultResponse.Status? = null
    private var mLastClickTime: Long = 0
    private var RESPONSE_CODE = 1501

    override fun onResume() {
        super.onResume()
        mGlobalCommentList?.clear()
        isLoaderAdded = false
        callApiCommon()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        feedTextDetailsActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.feed_text_details_activty)

        feedTextDetailsActivityBinding?.profileImage =
                SharedPrefsUtils.getStringPreference(
                        this@FeedTextDetailsActivity, RemoteConstant.mUserImage, ""
                ) ?: ""
        feedTextDetailsActivityBinding?.executePendingBindings()

        val i: Intent? = intent
        if (i?.getStringExtra("mPostId") != null) {
            mPostId = i.getStringExtra("mPostId") ?: ""
        }
        feedTextDetailsActivityBinding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }
        feedTextDetailsActivityBinding?.commentLinear?.setOnClickListener {
            val intent = Intent(this, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mIsMyPost", mIsMyPost)
            intent.putExtra("mPosition", 0)
            startActivityForResult(intent, ADD_COMMENT_RESPONSE_CODE)
        }
        feedTextDetailsActivityBinding?.likeLinear?.setOnClickListener {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(this, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        feedTextDetailsActivityBinding?.likeMaterialButton?.setOnClickListener {

            if (!feedTextDetailsActivityBinding?.likeMaterialButton?.isFavorite!!) {
                feedTextDetailsActivityBinding?.likeMaterialButton?.isFavorite = false
                onFeedItemLikeClick(mPostId, "false")
            } else {
                feedTextDetailsActivityBinding?.likeMaterialButton?.isFavorite = true
                onFeedItemLikeClick(mPostId, "true")
            }
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                feedTextDetailsActivityBinding?.likeCountTextView?.setTextColor(Color.parseColor("#1e90ff"))
            } else {
                feedTextDetailsActivityBinding?.likeCountTextView?.setTextColor(Color.parseColor("#747d8c"))
            }
        }

        feedTextDetailsActivityBinding?.imagePostBackImageView?.setOnClickListener {
            onBackPressed()
        }
        feedTextDetailsActivityBinding?.feedPostOptionImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View =
                    LayoutInflater.from(this).inflate(R.layout.options_menu_feed_option, viewGroup)
            val mQQPop = PopupWindow(
                    view,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(
                    feedTextDetailsActivityBinding?.feedPostOptionImageView,
                    -180,
                    -20
            )

            if (mFeedResponse?.payload?.post?.action?.post?.own == true) {
                view.edit_post.visibility = View.VISIBLE
                view.delete_post.visibility = View.VISIBLE
                view.share_post.visibility = View.VISIBLE
            } else {
                view.edit_post.visibility = View.GONE
                view.delete_post.visibility = View.GONE
                view.share_post.visibility = View.VISIBLE
                view.report_post.visibility = View.VISIBLE
            }
            view.report_post.setOnClickListener {
                mQQPop.dismiss()
                val intent = Intent(this, HelpFeedBackReportProblemActivity::class.java)
                intent.putExtra("mTitle", getString(R.string.report))
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
            view.share_post.setOnClickListener {
                ShareAppUtils.sharePost(
                        mFeedResponse?.payload?.post?.action?.source,
                        mFeedResponse?.payload?.post?.action?.post?.id ?: "", this
                )
                mQQPop.dismiss()
            }
            view.edit_post.setOnClickListener {
                val intent = Intent(this, WritePostActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                intent.putExtra("mIsEdit", true)
                startActivity(intent)
                mQQPop.dismiss()
            }
            view.delete_post.setOnClickListener {

                val dialogBuilder = this.let { AlertDialog.Builder(this) }
                val inflater = layoutInflater
                val dialogView = inflater.inflate(R.layout.common_dialog, null)
                dialogBuilder.setView(dialogView)
                dialogBuilder.setCancelable(false)

                val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                val okButton: Button = dialogView.findViewById(R.id.ok_button)

                val b = dialogBuilder.create()
                b?.show()
                okButton.setOnClickListener {
                    feedImageViewModel.deletePost(mPostId, this)

                    mQQPop.dismiss()
                    b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    onBackPressed()
                }
                cancelButton.setOnClickListener {
                    mQQPop.dismiss()
                    b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                }
            }
        }

        // Mention click listener
        feedTextDetailsActivityBinding?.descriptionTextView?.setOnMentionClickListener { view, text ->
            for (mMentionItem in mFeedResponse?.payload?.post?.action?.post?.mentions?.indices!!) {
                if (mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?.text?.contains(
                                text
                        )!!
                ) {
                    if (mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?._id != null) {
                        val intent = Intent(this, UserProfileActivity::class.java)
                        intent.putExtra(
                                "mProfileId",
                                mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?._id
                        )
                        startActivity(intent)
                    }
                }
            }
        }
        // Hash tag click listener
        feedTextDetailsActivityBinding?.descriptionTextView?.setOnHashtagClickListener { view, text ->
            for (mHashTagItem in mFeedResponse?.payload?.post?.action?.post?.hashTags?.indices!!) {
                if (mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(mHashTagItem)?.name?.equals(
                                text.toString(),
                                ignoreCase = true
                        )!!
                ) {
                    if (mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(mHashTagItem)?._id != null) {

                        if (!isCalledHashTgaDetailsPage) {
                            isCalledHashTgaDetailsPage = true
                            onOptionItemClickOptionHashTag(
                                    mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(
                                            mHashTagItem
                                    )?._id ?: "",
                                    "",
                                    "hashTagFeed",
                                    mFeedResponse?.payload?.post?.action?.post?.id ?: "",
                                    mFeedResponse?.payload?.post?.action?.post?.likesCount ?: "",
                                    mFeedResponse?.payload?.post?.action?.post?.commentsCount ?: ""
                            )
                            Handler().postDelayed({
                                isCalledHashTgaDetailsPage = false
                            }, 1000)
                        }

                    }
                }
            }

        }

        feedTextDetailsActivityBinding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
            val view =
                    feedTextDetailsActivityBinding?.nestedScrollView?.getChildAt(
                            feedTextDetailsActivityBinding?.nestedScrollView?.childCount ?: 0 - 1
                    )

            val diff =
                    view?.bottom ?: 0 - (feedTextDetailsActivityBinding?.nestedScrollView?.height
                            ?: 0 + feedTextDetailsActivityBinding?.nestedScrollView?.scrollY!!)

            if (diff == 0) {
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                        mStatus != ResultResponse.Status.LOADING
                        && mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY && mGlobalCommentList != null
                        && mGlobalCommentList?.isNotEmpty()!! && mGlobalCommentList?.size ?: 0 >= 15 &&
                        !isLoaderAdded
                ) {
                    loadMoreItems(
                            mGlobalCommentList?.get(
                                    mGlobalCommentList?.size ?: 0 - 1
                            )?.commentId ?: ""
                    )
                }
            }
        }
        mCommentAdapter = PostCommentAdapter(
                this@FeedTextDetailsActivity, this, mIsMyPost, glideRequestManager
        )
        feedTextDetailsActivityBinding?.postCommentsRecyclerView?.adapter = mCommentAdapter
        feedTextDetailsActivityBinding?.profileFeedNameTextView?.setOnClickListener {
            feedTextDetailsActivityBinding?.profileImageView?.performClick()
        }
        feedTextDetailsActivityBinding?.profileImageView?.setOnClickListener {
            val intent = Intent(this, UserProfileActivity::class.java)
            intent.putExtra("mProfileId", mFeedResponse?.payload?.post?.actor?.id)
            startActivity(intent)
        }

        feedTextDetailsActivityBinding?.linearComment?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(this, CommentListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                intent.putExtra("mIsMyPost", mIsMyPost)
                intent.putExtra("mPosition", 0)
                intent.putExtra("needToFocus", true)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }

        feedTextDetailsActivityBinding?.commentProfileImageView?.setOnClickListener {
            feedTextDetailsActivityBinding?.linearComment?.performClick()
        }
        feedTextDetailsActivityBinding?.commentEditText?.setOnClickListener {
            feedTextDetailsActivityBinding?.linearComment?.performClick()
        }

    }

    /**
     * Remove url from description
     */
    private fun removeUrl(commentstr: String): String {
        try {
            var commentstring = commentstr
            val urlPattern =
                    "((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?+-=\\\\.&]*)"
            val p = Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE)
            val m = p.matcher(commentstring)
            var i = 0
            while (m.find()) {
                commentstring = commentstring.replace(m.group(i).toRegex(), "").trim { it <= ' ' }
                i++
            }
            return commentstring
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    private fun onOptionItemClickOptionHashTag(
            mId: String, mPosition: String, mPath: String,
            mPostId: String, mLikeCount: String, mCommentCount: String
    ) {
        val detailIntent = Intent(this, PhotoGridDetailsActivity::class.java)
        detailIntent.putExtra("mId", mId)
        detailIntent.putExtra("mPosition", mPosition)
        detailIntent.putExtra("mPath", mPath)
        detailIntent.putExtra("mPostId", mPostId)
        detailIntent.putExtra("mLikeCount", mLikeCount)
        detailIntent.putExtra("mCommentCount", mCommentCount)
        startActivityForResult(detailIntent, 1501)
    }

    private fun loadMoreItems(size: String) {
        isLoaderAdded = true
        val mCommentList: MutableList<CommentResponseModelPayloadComment> = ArrayList()
        val mList = CommentResponseModelPayloadComment()
        mList.commentId = ""
        mCommentList.add(mList)
        val mListSizeNew = mGlobalCommentList?.size
        mGlobalCommentList?.addAll(mCommentList)

        mCommentAdapter?.notifyItemChanged(mListSizeNew ?: 0, mGlobalCommentList?.size)

        callApiComments(mPostId, size)
    }

    private fun callApiComments(mPostId: String, size: String) {
        val mAuth =
                RemoteConstant.getAuthCommentList(
                        this@FeedTextDetailsActivity,
                        mPostId,
                        size,
                        RemoteConstant.mCount.toInt()
                )
        feedImageViewModel.getFeedComment(mAuth, mPostId, size, RemoteConstant.mCount)
        observeDataComments()
    }


    private fun observeDataComments() {
        feedImageViewModel.commentPaginatedResponseModel?.nonNull()?.observe(this,
                { resultResponse ->
                    mStatus = resultResponse.status

                    when (resultResponse.status) {
                        ResultResponse.Status.SUCCESS -> {
                            mGlobalCommentList = resultResponse?.data?.toMutableList()

                            mCommentAdapter?.submitList(mGlobalCommentList)
                            mCommentCount = mGlobalCommentList?.size.toString()

                            mStatus = ResultResponse.Status.LOADING_COMPLETED

                        }
                        ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                            if (isLoaderAdded) {
                                removeLoaderFormList()
                            }
                            mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                        }
                        ResultResponse.Status.PAGINATED_LIST -> {

                            if (isLoaderAdded) {
                                removeLoaderFormList()
                            }
                            mGlobalCommentList?.addAll(resultResponse?.data!!)
                            mCommentAdapter?.notifyDataSetChanged()
                            mStatus = ResultResponse.Status.LOADING_COMPLETED
                        }
                        ResultResponse.Status.ERROR -> {

                            if (mGlobalCommentList?.size == 0) {
                                println("Empty")
                            }


                        }
                        ResultResponse.Status.NO_DATA -> {

                        }
                        ResultResponse.Status.LOADING -> {

                        }
                        else -> {

                        }
                    }
                })
        feedImageViewModel.commentPaginatedResponseModelCommentCount.nonNull()
                .observe(this, { commentCount ->
                    if (commentCount != null) {
                        mCommentCount = commentCount
                    }
                })

    }

    /**
     * Remove loader pagination
     */
    private fun removeLoaderFormList() {
        if (mGlobalCommentList != null && mGlobalCommentList?.size ?: 0 > 0 && mGlobalCommentList?.get(
                        mGlobalCommentList?.size ?: 0 - 1
                ) != null && mGlobalCommentList?.get(mGlobalCommentList?.size
                        ?: 0 - 1)?.commentId == ""
        ) {
            mGlobalCommentList?.removeAt(mGlobalCommentList?.size ?: 0 - 1)
            mCommentAdapter?.notifyDataSetChanged()
            isLoaderAdded = false
        }
    }

    override fun onFeedItemLikeClick(mPostId: String, isLike: String) {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        } else {
            if (isLike == "true") {
                disLikePost(mPostId)
            } else {
                likePost(mPostId)
            }
        }
    }

    private fun likePost(postId: String) {
        val mAuth = RemoteConstant.getAuthLikePost(this, postId)
        feedImageViewModel.likePost(mAuth, postId)
        callApi(mPostId)

    }

    private fun disLikePost(postId: String) {
        val mAuth = RemoteConstant.getAuthDislikePost(this, postId)
        feedImageViewModel.disLikePost(mAuth, postId)
        callApi(mPostId)
    }

    private fun callApiCommon() {

        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApiComments(mPostId, "0")
            callApi(mPostId)
            feedTextDetailsActivityBinding?.detailsCardView?.visibility = View.GONE

            feedTextDetailsActivityBinding?.isVisibleList = false
            feedTextDetailsActivityBinding?.isVisibleLoading = true
            feedTextDetailsActivityBinding?.isVisibleNoInternet = false

        } else {
            feedTextDetailsActivityBinding?.detailsCardView?.visibility = View.GONE

            feedTextDetailsActivityBinding?.isVisibleList = false
            feedTextDetailsActivityBinding?.isVisibleLoading = false
            feedTextDetailsActivityBinding?.isVisibleNoInternet = true
            val shake: Animation =
                    AnimationUtils.loadAnimation(this@FeedTextDetailsActivity, R.anim.shake)
            feedTextDetailsActivityBinding?.noInternetLinear?.startAnimation(shake) // starts animation
        }

    }

    /**
     * Get text post details
     */
    private fun callApi(mPostId: String) {
        feedImageViewModel.getFeedDetails(mPostId)
        feedImageViewModel.responseModel.nonNull().observe(this, { responseModel ->
            if (responseModel?.payload?.deleted != true) {
                feedTextDetailsActivityBinding?.detailsCardView?.visibility = View.VISIBLE
                feedTextDetailsActivityBinding?.isVisibleList = true
                feedTextDetailsActivityBinding?.isVisibleLoading = false
                feedTextDetailsActivityBinding?.isVisibleNoInternet = false

                mFeedResponse = responseModel
                feedTextDetailsActivityBinding?.singleFeedResponseModel = mFeedResponse
                val mPost = responseModel.payload?.post?.action?.post
                isLiked = mPost?.liked ?: false
                mLikeCount = mPost?.likesCount ?: ""
                mCommentCount = mPost?.commentsCount ?: ""
                mCommentCount = mPost?.commentsCount ?: ""
                mIsMyPost = mPost?.own ?: false


                val mDescription: String =
                        removeUrl(mPost?.description ?: "")
                if (mDescription != "" && mDescription.isNotEmpty()) {
                    if (responseModel.payload?.post?.action != null
                            && responseModel.payload?.post?.action?.post != null
                            && mPost?.ogTags != null
                            && mPost.ogTags?.url != null
                            && mPost.ogTags?.url != ""
                    ) {
                        feedTextDetailsActivityBinding?.descriptionTextView?.text = mDescription
                    } else {
                        feedTextDetailsActivityBinding?.descriptionTextView?.text =
                                mPost?.description ?: ""
                    }

                } else if (responseModel.payload?.post?.action != null
                        && responseModel.payload?.post?.action?.post != null
                        && mPost?.ogTags != null
                        && mPost.ogTags?.url != null
                        && mPost.ogTags?.url != ""
                ) {
                    feedTextDetailsActivityBinding?.descriptionTextView?.visibility = View.GONE
                } else {
                    feedTextDetailsActivityBinding?.descriptionTextView?.text =
                            mPost?.description ?: ""
                    feedTextDetailsActivityBinding?.descriptionTextView?.visibility =
                            View.VISIBLE
                }


                // TODO : Since we need to delete all the comment OWN Post
                mCommentAdapter?.setIsOwn(mIsMyPost)

                if (mPost?.ogTags != null
                        && mPost.ogTags?.id != null && mPost.ogTags?.id != ""
                ) {
                    feedTextDetailsActivityBinding?.textOgViewModel =
                            mPost.ogTags
                    feedTextDetailsActivityBinding?.scrapLinear?.visibility = View.VISIBLE
                } else {
                    feedTextDetailsActivityBinding?.scrapLinear?.visibility = View.GONE
                }


                feedTextDetailsActivityBinding?.descriptionTextView?.setOnClickListener {
                    feedTextDetailsActivityBinding?.scrapLinear?.performClick()
                }
                val mFeedList = responseModel.payload!!
                feedTextDetailsActivityBinding?.scrapLinear?.setOnClickListener {

                    if (mFeedList.post != null && mFeedList.post != null && mFeedList.post?.action != null &&
                            mFeedList.post?.action?.post != null && mFeedList.post?.action?.post?.ogTags != null
                            && mFeedList.post?.action?.post?.ogTags?.url != null
                            && mFeedList.post?.action?.post?.ogTags?.url != ""
                    ) {
                        if (mFeedList.post?.action?.post?.ogTags?.url?.contains("http")!!) {
                            try {
                                val url = mFeedList.post?.action?.post?.ogTags?.url
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.data = Uri.parse(url)
                                startActivity(intent)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    } else {
                        if (mFeedList.post?.action?.post != null && mFeedList.post?.action?.post?.ogTags != null
                                && mFeedList.post?.action?.post?.ogTags?.url != null
                                && mFeedList.post?.action?.post?.ogTags?.url != "" &&
                                retrieveLinks(mFeedList.post?.action?.post?.ogTags?.url ?: "") != ""
                        ) {
                            if (retrieveLinks(
                                            mFeedList.post?.action?.post?.ogTags?.url ?: ""
                                    ).contains(
                                            "http"
                                    )
                            ) {
                                try {
                                    val url =
                                            retrieveLinks(
                                                    mFeedList.post?.action?.post?.ogTags?.url ?: ""
                                            )
                                    val intent = Intent(Intent.ACTION_VIEW)
                                    intent.data = Uri.parse(url)
                                    startActivity(intent)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                }


            } else {
                AppUtils.showCommonToast(this, "Post Deleted")
                finish()
            }

        })
    }

    override fun onBackPressed() {
        val intent = intent
        intent.putExtra("COMMENT_COUNT", mCommentCount)
        intent.putExtra("LIKE_COUNT", mLikeCount)
        intent.putExtra("POST_ID", mPostId)
        intent.putExtra("IS_LIKED", isLiked)
        intent.putExtra("IS_FROM_COMMENT_LIST", false)
        setResult(1501, intent)
        super.onBackPressed()
        AppUtils.hideKeyboard(this@FeedTextDetailsActivity, image_post_back_image_view)
    }

    override fun onOptionItemClickOption(
            mMethod: String,
            mId: String,
            mComment: String,
            mPosition: Int
    ) {
        when (mMethod) {
//            "editComment" -> editComment(mId, mComment)
            "deleteComment" -> deleteComment(mId, mPosition)
        }
    }

    /**
     * Delete comment API
     */
    @SuppressLint("SetTextI18n")
    private fun deleteComment(mId: String, mPosition: Int) {
        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "") ?: "",
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "") ?: "",
                RemoteConstant.mBaseUrl +
                        RemoteConstant.mPostPathV1 +
                        RemoteConstant.mSlash + mPostId +
                        RemoteConstant.mPathComment +
                        RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mAppId,
                        ""
                ) + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        feedImageViewModel.deleteComment(mAuth, mPostId, mId)
        feedImageViewModel.commentDeleteResponseModel.observe(this, {
            if (it?.payload?.commentsCount != null) {
                mCommentCount = it.payload.commentsCount
                feedTextDetailsActivityBinding?.commentCount?.text = "$mCommentCount Comments"
            } else {
                mCommentCount = "0"
                feedTextDetailsActivityBinding?.commentCount?.text = "$mCommentCount Comments"

            }
        })
        mGlobalCommentList?.removeAt(mPosition)
        mCommentAdapter?.notifyDataSetChanged()

    }

    /**
     * Get link from description
     */
    private fun retrieveLinks(text: String): String {
        try {
            val links: MutableList<String>? = null

            val regex =
                    "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"
            val p = Pattern.compile(regex)
            val m = p.matcher(text)
            while (m.find()) {
                var urlStr = m.group()
                urlStr.toCharArray()

                if (urlStr.startsWith("(") && urlStr.endsWith(")")) {

                    val stringArray = urlStr.toCharArray()

                    val newArray = CharArray(stringArray.size - 2)
                    System.arraycopy(stringArray, 1, newArray, 0, stringArray.size - 2)
                    urlStr = String(newArray)
                    println("Finally Url =$newArray")

                }
                println("...Url...$urlStr")
                links?.add(urlStr)
            }
            return links?.get(0) ?: ""
        } catch (e: Exception) {
            try {
                val links: MutableList<String> = ArrayList()
                val m = Patterns.WEB_URL.matcher(text)
                while (m.find()) {
                    val url = m.group()
                    links.add(url)
                }
                return links[0]
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return ""
    }

    override fun onCommentClick(mPostId: String, mPosition: Int, mIsMyPost: Boolean) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            val intent = Intent(this, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mIsMyPost", mIsMyPost)
            intent.putExtra("mPosition", mPosition)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onFeedLikeListClick(mPostId: String, mLikeCount: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(this, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }
    override fun onOptionItemClickOption(mMethod: String, mId: String, mPos: Int) {
        if (mMethod == "delete") {

            feedImageViewModel.deletePost(mId, this)
            finish()
        }
    }

    override fun onFeedItemClick(mPostId: String, mType: String) {
        if (mType == "image") {
            val intent = Intent(this, FeedImageDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        if (mType == "video") {
            val intent = Intent(this, FeedVideoDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        if (mType == "text") {
            val intent = Intent(this, FeedTextDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
    }

    override fun onAdapterFollowItemClick(isFollowing: String, mId: String) {

    }

    override fun onHyperLinkClick(mUrl: String) {
    }

    override fun onProgressUpdate(percentage: Int, timestamp: String?) {
    }

    override fun onError() {
    }

    override fun onFinish() {
    }

    override fun onHyperLinkUrlClick(mUrl: String) {
    }

    override fun onOptionItemClickOption(mId: String, mPosition: String, mPath: String, mPostId: String, mLikeCount: String, mCommentCount: String) {

    }
}

