package com.example.sample.socialmob.view.ui.home_module.feed

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.PopupWindow
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.FeedVideoDetailsActivtyBinding
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.settings.HelpFeedBackReportProblemActivity
import com.example.sample.socialmob.view.ui.profile.feed.CommentListActivity
import com.example.sample.socialmob.view.ui.profile.feed.PostCommentAdapter
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.bvpkotlin.BetterVideoPlayer
import com.example.sample.socialmob.view.utils.bvpkotlin.VideoCallback
import com.example.sample.socialmob.viewmodel.feed.FeedImageViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.feed_video_details_activty.*
import kotlinx.android.synthetic.main.options_menu_feed_option.view.*
import javax.inject.Inject

@AndroidEntryPoint
class FeedVideoDetailsActivity : BaseCommonActivity(),
    PostCommentAdapter.OptionPostItemClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private val feedVideoViewModel: FeedImageViewModel by viewModels()
    private var mPostId = ""
    private var feedVideoDetailsActivtyBinding: FeedVideoDetailsActivtyBinding? = null
    private var mFeedResponse: SingleFeedResponseModel? = null
    private var isPlayed: Boolean = false
    private var mLikeCount = ""
    private var mCommentCount = ""
    private var isLiked: Boolean = false
    private var mIsMyPost: Boolean = false
    private var isonPause: Boolean = false
    private var mGlobalCommentList: MutableList<CommentResponseModelPayloadComment>? = ArrayList()
    private var mCommentAdapter: PostCommentAdapter? = null
    private var isLoaderAdded: Boolean = false

    private var isCalledHashTgaDetailsPage: Boolean = false
    private var mStatus: ResultResponse.Status? = null
    private var mLastClickTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        feedVideoDetailsActivtyBinding =
            DataBindingUtil.setContentView(this, R.layout.feed_video_details_activty)
        feedVideoDetailsActivtyBinding?.profileImage =
            SharedPrefsUtils.getStringPreference(
                this@FeedVideoDetailsActivity,
                RemoteConstant.mUserImage,
                ""
            )!!
        feedVideoDetailsActivtyBinding?.executePendingBindings()

        val i = intent
        if (i.getStringExtra("mPostId") != null) {
            mPostId = i.getStringExtra("mPostId") ?: ""
        }
        feedVideoDetailsActivtyBinding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }
        bvp.setHideControlsOnPlay(true)


        if (MyApplication.playlistManagerRadio != null) {
            MyApplication.playlistManagerRadio?.play(0, true)
        }
        if (MyApplication.playlistManager != null) {
            MyApplication.playlistManager?.play(0, true)
        }

        feedVideoDetailsActivtyBinding?.videoPostBackImageView?.setOnClickListener {
            onBackPressed()
        }
        feedVideoDetailsActivtyBinding?.commentLinear?.setOnClickListener {
            val intent = Intent(this, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mPosition", 0)
            intent.putExtra("mIsMyPost", mIsMyPost)
            startActivity(intent)
        }
        feedVideoDetailsActivtyBinding?.likeLinear?.setOnClickListener {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(this, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        feedVideoDetailsActivtyBinding?.likeMaterialButton?.setOnClickListener {

            if (mFeedResponse?.payload?.post?.action?.post?.liked != true) {
                onFeedItemLikeClick(mPostId, "false")
                feedVideoDetailsActivtyBinding?.likeMaterialButton?.isFavorite = true
            } else {
                onFeedItemLikeClick(mPostId, "true")
                feedVideoDetailsActivtyBinding?.likeMaterialButton?.isFavorite = false

            }
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                feedVideoDetailsActivtyBinding?.likeCountTextView?.setTextColor(Color.parseColor("#1e90ff"))
            } else {
                feedVideoDetailsActivtyBinding?.likeCountTextView?.setTextColor(Color.parseColor("#747d8c"))
            }
            feedVideoDetailsActivtyBinding?.singleFeedResponseModel = mFeedResponse
        }
        feedVideoDetailsActivtyBinding?.feedPostOptionImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View =
                LayoutInflater.from(this).inflate(R.layout.options_menu_feed_option, viewGroup)
            val mQQPop = PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(
                feedVideoDetailsActivtyBinding?.feedPostOptionImageView,
                -180,
                -20
            )

            if (mFeedResponse?.payload?.post?.action?.post?.own == true) {
                view.edit_post.visibility = View.VISIBLE
                view.delete_post.visibility = View.VISIBLE
                view.share_post.visibility = View.VISIBLE
            } else {
                view.edit_post.visibility = View.GONE
                view.delete_post.visibility = View.GONE
                view.share_post.visibility = View.VISIBLE
                view.report_post.visibility = View.VISIBLE
            }
            view.report_post.setOnClickListener {
                mQQPop.dismiss()
                val intent = Intent(this, HelpFeedBackReportProblemActivity::class.java)
                intent.putExtra("mTitle", getString(R.string.report))
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
            view.share_post.setOnClickListener {
                ShareAppUtils.sharePost(
                    mFeedResponse?.payload?.post?.action?.source,
                    mFeedResponse?.payload?.post?.action?.post?.id ?: "", this
                )
                mQQPop.dismiss()
            }
            view.edit_post.setOnClickListener {

                val intent = Intent(this, UploadPhotoActivityMentionHashTag::class.java)
                intent.putExtra("mPostId", mPostId)
                intent.putExtra("mIsEdit", true)
                startActivity(intent)
                mQQPop.dismiss()
            }
            view.delete_post.setOnClickListener {

                val dialogBuilder = this.let { AlertDialog.Builder(this) }
                val inflater = layoutInflater
                val dialogView = inflater.inflate(R.layout.common_dialog, null)
                dialogBuilder.setView(dialogView)
                dialogBuilder.setCancelable(false)

                val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                val okButton: Button = dialogView.findViewById(R.id.ok_button)

                val b = dialogBuilder.create()
                b.show()
                okButton.setOnClickListener {
                    feedVideoViewModel.deletePost(mPostId, this)
                    mQQPop.dismiss()
                    onBackPressed()
                    b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                }
                cancelButton.setOnClickListener {
                    mQQPop.dismiss()
                    b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                }
            }
        }
        // Mention click listner
        feedVideoDetailsActivtyBinding?.descriptionTextView?.setOnMentionClickListener { view, text ->
            //            AppUtils.showCommonToast(this, "" + text)
            for (mMentionItem in mFeedResponse?.payload?.post?.action?.post?.mentions?.indices!!) {
                if (mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?.text?.contains(
                        text
                    )!!
                ) {
                    if (mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?._id != null) {
                        val intent = Intent(this, UserProfileActivity::class.java)
                        intent.putExtra(
                            "mProfileId",
                            mFeedResponse?.payload?.post?.action?.post?.mentions?.get(mMentionItem)?._id
                        )
                        startActivity(intent)
                    }
                }
            }
        }
        // Hash tag click listner
        feedVideoDetailsActivtyBinding?.descriptionTextView?.setOnHashtagClickListener { view, text ->
            for (mHashTagItem in mFeedResponse?.payload?.post?.action?.post?.hashTags?.indices!!) {
                if (mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(mHashTagItem)?.name?.equals(
                        text.toString(),
                        ignoreCase = true
                    )!!
                ) {
                    if (mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(mHashTagItem)?._id != null) {

                        if (!isCalledHashTgaDetailsPage) {
                            isCalledHashTgaDetailsPage = true
                            onOptionItemClickOptionHashTag(
                                mFeedResponse?.payload?.post?.action?.post?.hashTags?.get(
                                    mHashTagItem
                                )?._id ?: "",
                                "",
                                "hashTagFeed",
                                mFeedResponse?.payload?.post?.action?.post?.id ?: "",
                                mFeedResponse?.payload?.post?.action?.post?.likesCount ?: "",
                                mFeedResponse?.payload?.post?.action?.post?.commentsCount ?: ""
                            )
                            Handler().postDelayed({
                                isCalledHashTgaDetailsPage = false
                            }, 1000)
                        }
                    }
                }
            }
        }

        feedVideoDetailsActivtyBinding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
            val view =
                feedVideoDetailsActivtyBinding?.nestedScrollView?.getChildAt(
                    feedVideoDetailsActivtyBinding?.nestedScrollView?.childCount ?: 0 - 1
                )

            val diff =
                view?.bottom ?: 0 - (feedVideoDetailsActivtyBinding?.nestedScrollView?.height
                    ?: 0
                    + feedVideoDetailsActivtyBinding?.nestedScrollView?.scrollY!!)

            if (diff == 0) {
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING
                    && mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                    mGlobalCommentList != null
                    && mGlobalCommentList?.isNotEmpty()!! && mGlobalCommentList?.size ?: 0 >= 15
                    && !isLoaderAdded
                ) {
                    loadMoreItems(
                        mGlobalCommentList?.get(mGlobalCommentList?.size ?: 1 - 1)?.commentId ?: ""
                    )
                }

            }
        }
        mCommentAdapter = PostCommentAdapter(
            this@FeedVideoDetailsActivity, this, mIsMyPost, glideRequestManager
        )
        feedVideoDetailsActivtyBinding?.postCommentsRecyclerView?.adapter = mCommentAdapter
        feedVideoDetailsActivtyBinding?.profileFeedNameTextView?.setOnClickListener {
            feedVideoDetailsActivtyBinding?.profileImageView?.performClick()
        }
        feedVideoDetailsActivtyBinding?.profileImageView?.setOnClickListener {
            val intent = Intent(this, UserProfileActivity::class.java)
            intent.putExtra("mProfileId", mFeedResponse?.payload?.post?.actor?.id)
            startActivity(intent)
        }
        val gd = GestureDetector(this, object : GestureDetector.SimpleOnGestureListener() {
            override fun onDown(e: MotionEvent): Boolean {
                return true
            }

            override fun onDoubleTap(e: MotionEvent): Boolean {

                feedVideoDetailsActivtyBinding?.heartAnim?.bringToFront()
                val pulseFade = AnimationUtils.loadAnimation(
                    this@FeedVideoDetailsActivity,
                    R.anim.pulse_fade_in
                )
                pulseFade.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {
                        feedVideoDetailsActivtyBinding?.heartAnim?.visibility = View.VISIBLE
                        if (!isLiked) {
                            feedVideoDetailsActivtyBinding?.likeMaterialButton?.performClick()
                        }
                    }

                    override fun onAnimationEnd(animation: Animation) {
                        feedVideoDetailsActivtyBinding?.heartAnim?.visibility = View.GONE
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }
                })
                feedVideoDetailsActivtyBinding?.heartAnim?.startAnimation(pulseFade)
                return true
            }


            override fun onDoubleTapEvent(e: MotionEvent): Boolean {
                return true
            }
        })
        feedVideoDetailsActivtyBinding?.bvp?.setOnTouchListener { v, event -> gd.onTouchEvent(event) }


        feedVideoDetailsActivtyBinding?.linearComment?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(this, CommentListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                intent.putExtra("mIsMyPost", mIsMyPost)
                intent.putExtra("mPosition", 0)
                intent.putExtra("needToFocus", true)

                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()

        }

        feedVideoDetailsActivtyBinding?.commentProfileImageView?.setOnClickListener {
            feedVideoDetailsActivtyBinding?.linearComment?.performClick()
        }
        feedVideoDetailsActivtyBinding?.commentEditText?.setOnClickListener {
            feedVideoDetailsActivtyBinding?.linearComment?.performClick()
        }

    }

    private fun onOptionItemClickOptionHashTag(
        mId: String, mPosition: String, mPath: String, mPostId: String, mLikeCount: String,
        mCommentCount: String
    ) {
        val detailIntent = Intent(this, PhotoGridDetailsActivity::class.java)
        detailIntent.putExtra("mId", mId)
        detailIntent.putExtra("mPosition", mPosition)
        detailIntent.putExtra("mPath", mPath)
        detailIntent.putExtra("mPostId", mPostId)
        detailIntent.putExtra("mLikeCount", mLikeCount)
        detailIntent.putExtra("mCommentCount", mCommentCount)
        startActivityForResult(detailIntent, 1501)
    }

    private fun onFeedItemLikeClick(mPostId: String, isLike: String) {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        } else {
            if (isLike == "true") {
                disLikePost(mPostId)
            } else {
                likePost(mPostId)
            }
        }
    }

    /**
     * Like post API
     */
    private fun likePost(postId: String) {
        val mAuth = RemoteConstant.getAuthLikePost(this, postId)
        feedVideoViewModel.likePost(mAuth, postId)
        callApi(mPostId)

    }

    /**
     * Dislike post API
     */
    private fun disLikePost(postId: String) {
        val mAuth = RemoteConstant.getAuthDislikePost(this, postId)
        feedVideoViewModel.disLikePost(mAuth, postId)
        callApi(mPostId)
    }


    private fun callApiCommon() {

        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi(mPostId)
            callApiComments(mPostId, "0")
            observeData()
            feedVideoDetailsActivtyBinding?.detailsCardView?.visibility = View.GONE
            feedVideoDetailsActivtyBinding?.isVisibleList = false
            feedVideoDetailsActivtyBinding?.isVisibleLoading = true
            feedVideoDetailsActivtyBinding?.isVisibleNoInternet = false

        } else {
            feedVideoDetailsActivtyBinding?.detailsCardView?.visibility = View.GONE
            feedVideoDetailsActivtyBinding?.isVisibleList = false
            feedVideoDetailsActivtyBinding?.isVisibleLoading = false
            feedVideoDetailsActivtyBinding?.isVisibleNoInternet = true
            val shake: Animation =
                AnimationUtils.loadAnimation(this@FeedVideoDetailsActivity, R.anim.shake)
            feedVideoDetailsActivtyBinding?.noInternetLinear?.startAnimation(shake) // starts animation
        }


    }

    /**
     * Get post details API
     */
    private fun callApi(mPostId: String) {
        feedVideoViewModel.getFeedDetails(mPostId)
        feedVideoViewModel.responseModel.nonNull().observe(this, { responseModel ->
            if (responseModel?.payload?.deleted != true) {

                feedVideoDetailsActivtyBinding?.isVisibleList = true
                feedVideoDetailsActivtyBinding?.isVisibleLoading = false
                feedVideoDetailsActivtyBinding?.isVisibleNoInternet = false
                feedVideoDetailsActivtyBinding?.detailsCardView?.visibility = View.VISIBLE
                mFeedResponse = responseModel
                feedVideoDetailsActivtyBinding?.singleFeedResponseModel = mFeedResponse

                val mPost = responseModel.payload?.post?.action?.post
                isLiked = mPost?.liked ?: false
                mIsMyPost = mPost?.own ?: false
                mLikeCount = mPost?.likesCount ?: ""
                mCommentCount = mPost?.commentsCount ?: ""

                // TODO : Since we need to delete all the comment OWN Post
                mCommentAdapter?.setIsOwn(mIsMyPost)

                if (mPost?.media?.isNotEmpty()!!) {
                    if (!isPlayed) {
                        isPlayed = true
                        val uriString = mPost.media?.get(0)?.sourcePath
                        bvp.setSource(Uri.parse(uriString))


                        // TODO : Set Video Ratio
                        if (mPost.media?.get(0)?.ratio != null) {
                            when (mPost.media?.get(0)?.ratio) {
                                "1:1" -> {
                                    feedVideoDetailsActivtyBinding?.frameLayout?.setAspectRatio(
                                        1F,
                                        1F
                                    )
                                }
                                "4:3" -> {
                                    feedVideoDetailsActivtyBinding?.frameLayout?.setAspectRatio(
                                        4F,
                                        3F
                                    )
                                }
                                "16:9" -> {
                                    feedVideoDetailsActivtyBinding?.frameLayout?.setAspectRatio(
                                        16F,
                                        9F
                                    )
                                }
                            }
                        }

                        bvp.setCallback(object : VideoCallback {
                            override fun onStarted(player: BetterVideoPlayer) {
                                println("Started")
                            }

                            override fun onPaused(player: BetterVideoPlayer) {
                                println("Paused")
                                player.pause()
                            }

                            override fun onPreparing(player: BetterVideoPlayer) {
                                println(">>>Preparing")
                            }

                            override fun onPrepared(player: BetterVideoPlayer) {
                                if (!isonPause) {
                                    bvp.setAutoPlay(true)
                                    bvp.start()
                                }
                                println(">>>Prepared")
                            }

                            override fun onBuffering(percent: Int) {
                                println(">>>Buffering $percent")
                            }

                            override fun onError(player: BetterVideoPlayer, e: Exception) {
                                println(">>>Error " + e.message)
                            }

                            override fun onCompletion(player: BetterVideoPlayer) {
                                println(">>>Completed")
                            }

                            override fun onToggleControls(
                                player: BetterVideoPlayer,
                                isShowing: Boolean
                            ) {

                            }
                        })
                    }
                    feedVideoViewModel.responseModel.value = null
                } else {
                    AppUtils.showCommonToast(this, "Post Deleted")
                    finish()
                }
            } else {
                AppUtils.showCommonToast(this, "Post Deleted")
                finish()
            }
        })
    }


    override fun onOptionItemClickOption(
        mMethod: String,
        mId: String,
        mComment: String,
        mPosition: Int
    ) {
        when (mMethod) {
//            "editComment" -> editComment(mId, mComment)
            "deleteComment" -> deleteComment(mId, mPosition)
        }
    }

    /**
     * Delete post comment API
     */
    @SuppressLint("SetTextI18n")
    private fun deleteComment(mId: String, mPosition: Int) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mPostId +
                    RemoteConstant.mPathComment +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        feedVideoViewModel.deleteComment(mAuth, mPostId, mId)
        feedVideoViewModel.commentDeleteResponseModel.observe(this, {
            if (it?.payload?.commentsCount != null) {
                mCommentCount = it.payload.commentsCount
                feedVideoDetailsActivtyBinding?.commentCount?.text = "$mCommentCount Comments"
            } else {
                mCommentCount = "0"
                feedVideoDetailsActivtyBinding?.commentCount?.text = "$mCommentCount Comments"

            }
        })
        mGlobalCommentList?.removeAt(mPosition)
        mCommentAdapter?.notifyDataSetChanged()

    }

    private fun loadMoreItems(size: String) {

        isLoaderAdded = true
        val mCommentList: MutableList<CommentResponseModelPayloadComment> = ArrayList()
        val mList = CommentResponseModelPayloadComment()
        mList.commentId = ""
        mCommentList.add(mList)
        mGlobalCommentList?.addAll(mCommentList)
        mCommentAdapter?.notifyDataSetChanged()

        callApiComments(mPostId, size)
    }

    /**
     * Get video post details API
     */
    private fun callApiComments(mPostId: String, size: String) {
        val mAuth =
            RemoteConstant.getAuthCommentList(
                this@FeedVideoDetailsActivity,
                mPostId,
                size,
                RemoteConstant.mCount.toInt()
            )
        feedVideoViewModel.getFeedComment(mAuth, mPostId, size, RemoteConstant.mCount)
    }


    private fun observeData() {

        feedVideoViewModel.commentPaginatedResponseModel?.nonNull()?.observe(this,
            { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mGlobalCommentList = resultResponse?.data?.toMutableList()

                        mCommentAdapter?.submitList(mGlobalCommentList)
                        mCommentCount = mGlobalCommentList?.size.toString()

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mGlobalCommentList?.addAll(resultResponse?.data!!)
                        mCommentAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {

                        if (mGlobalCommentList?.size == 0) {
                            println("Empty")
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {

                    }
                    ResultResponse.Status.LOADING -> {

                    }
                    else -> {

                    }
                }
            })

        feedVideoViewModel.commentPaginatedResponseModelCommentCount.nonNull()
            .observe(this, { mCommentCount = it!! })

    }

    /**
     * Remove loader from list - pagination
     */
    private fun removeLoaderFormList() {
        if (mGlobalCommentList != null && mGlobalCommentList?.size!! > 0 && mGlobalCommentList?.get(
                mGlobalCommentList?.size!! - 1
            ) != null && mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1)?.commentId == ""
        ) {
            mGlobalCommentList?.removeAt(mGlobalCommentList?.size!! - 1)
            mCommentAdapter?.notifyDataSetChanged()
            isLoaderAdded = false
        }
    }

    override fun onPause() {
        bvp.pause()
        isonPause = true
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        mGlobalCommentList?.clear()
        callApiCommon()
        isonPause = false
    }

    override fun onBackPressed() {
        val intent = intent
        intent.putExtra("COMMENT_COUNT", mCommentCount)
        intent.putExtra("LIKE_COUNT", mLikeCount)
        intent.putExtra("POST_ID", mPostId)
        intent.putExtra("IS_LIKED", isLiked)
        intent.putExtra("IS_FROM_COMMENT_LIST", false)
        setResult(1501, intent)
        super.onBackPressed()
        AppUtils.hideKeyboard(this@FeedVideoDetailsActivity, video_post_back_image_view)
    }

    override fun onDestroy() {
        super.onDestroy()
        feedVideoViewModel.commentPaginatedResponseModelCommentCount.removeObservers(this)
        feedVideoViewModel.commentPaginatedResponseModel?.removeObservers(this)
        feedVideoViewModel.commentDeleteResponseModel.removeObservers(this)
        feedVideoViewModel.responseModel.removeObservers(this)
    }
}
