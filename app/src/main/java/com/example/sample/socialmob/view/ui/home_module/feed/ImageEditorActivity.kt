package com.example.sample.socialmob.view.ui.home_module.feed

import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.viewModels
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.write_new_post.Config
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import com.example.sample.socialmob.viewmodel.profile.ProfileUploadPostViewModel
import dagger.hilt.android.AndroidEntryPoint
import ly.img.android.pesdk.assets.filter.basic.FilterPackBasic
import ly.img.android.pesdk.assets.font.basic.FontPackBasic
import ly.img.android.pesdk.backend.decoder.ImageSource
import ly.img.android.pesdk.backend.model.EditorSDKResult
import ly.img.android.pesdk.backend.model.config.CropAspectAsset
import ly.img.android.pesdk.backend.model.state.AssetConfig
import ly.img.android.pesdk.backend.model.state.LoadSettings
import ly.img.android.pesdk.backend.model.state.TransformSettings
import ly.img.android.pesdk.backend.model.state.manager.SettingsList
import ly.img.android.pesdk.backend.model.state.manager.configure
import ly.img.android.pesdk.ui.activity.ImgLyIntent
import ly.img.android.pesdk.ui.activity.PhotoEditorBuilder
import ly.img.android.pesdk.ui.model.state.UiConfigAspect
import ly.img.android.pesdk.ui.model.state.UiConfigFilter
import ly.img.android.pesdk.ui.model.state.UiConfigMainMenu
import ly.img.android.pesdk.ui.model.state.UiConfigText
import ly.img.android.pesdk.ui.panels.item.CropAspectItem
import ly.img.android.pesdk.ui.panels.item.ToolItem
import ly.img.android.pesdk.ui.utils.PermissionRequest
import ly.img.android.serializer._3._0._0.PESDKFileWriter
import java.io.File

@AndroidEntryPoint
class ImageEditorActivity : BaseCommonActivity(), PermissionRequest.Response {

    private var path: String? = null
    private var isFromProfilePic = false

    private var mImageList: ArrayList<GalleryData> = ArrayList()

    private var customProgressDialog: CustomProgressDialog? = null
    private val profileUploadViewModel: ProfileUploadPostViewModel by viewModels()

    companion object {
        var PESDK_RESULT = 1
        var GALLERY_RESULT = 2
    }

    // Important permission request for Android 6.0 and above, don't forget to add this!
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        PermissionRequest.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun permissionGranted() {}

    override fun permissionDenied() {
        // The Permission was rejected by the user. The Editor was not opened, as it could not save the result image.
        // TODO: Show a hint to the user and try again.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_editor)

        customProgressDialog = CustomProgressDialog(this)
        mImageList = intent.getParcelableArrayListExtra(Config.KeyName.IMAGE_LIST)!!
        isFromProfilePic = intent.getBooleanExtra("IS_FROM_CREATE_PROFILE", false)

        try {
            if (mImageList.size > 0) {
                path = mImageList[0].photoUri
                val mFile = Uri.fromFile(File(path!!))
                openEditor(mFile)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun createPesdkSettingsList() = SettingsList().apply {

        // If you include our asset Packs and you use our UI you also need to add them to the UI,
        // otherwise they are only available for the backend
        // See the specific feature sections of our guides if you want to know how to add our own Assets.

        getSettingsModel(UiConfigFilter::class.java).apply {
            setFilterList(FilterPackBasic.getFilterPack())
        }

        getSettingsModel(UiConfigText::class.java).apply {
            setFontList(FontPackBasic.getFontPack())
        }

        // Custom editor tools
        getSettingsModel(UiConfigMainMenu::class.java).apply {
            // Set the tools you want keep sure you licence is cover the feature and do not forget to include the correct modules in your build.gradle
            setToolList(
                ToolItem(
                    "imgly_tool_transform",
                    R.string.pesdk_transform_title_name,
                    ImageSource.create(R.drawable.imgly_icon_tool_transform)
                ),
//                ToolItem(
//                    "imgly_tool_filter",
//                    R.string.pesdk_filter_title_name,
//                    ImageSource.create(R.drawable.imgly_icon_tool_filters)
//                )
            )
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_RESULT) {
            // Open Editor with some uri in this case with an image selected from the system gallery.
            data?.data?.let { selectedImage ->
                openEditor(selectedImage)
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == PESDK_RESULT) {
            // Editor has saved an Image.
            val resultURI = data?.getParcelableExtra<Uri>(ImgLyIntent.RESULT_IMAGE_URI).also {
                sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).setData(it))
            }
            val sourceURI = data?.getParcelableExtra<Uri>(ImgLyIntent.SOURCE_IMAGE_URI).also {
                sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).setData(it))
            }

            // TODO: Do something with the result image

            // OPTIONAL: read the latest state to save it as a serialisation
            val lastState = data?.getParcelableExtra<SettingsList>(ImgLyIntent.SETTINGS_LIST)
            try {
                val pesdkFileWriter = PESDKFileWriter(lastState!!)
                pesdkFileWriter.writeJson(
                    File(
                        Environment.getExternalStorageDirectory(),
                        "serialisationReadyToReadWithPESDKFileReader.json"
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(this, "" + e.printStackTrace(), Toast.LENGTH_SHORT).show()
            }

            val mImageListNew: ArrayList<GalleryData> = ArrayList()
            val dataAdded = GalleryData()
            val mPath = getFilePath(this, Uri.parse(resultURI.toString()))
            dataAdded.photoUri = mPath!!
            mImageListNew.add(dataAdded)


            if (isFromProfilePic) {
                if (InternetUtil.isInternetOn()) {
                    if (mImageList.size > 0) {
                        customProgressDialog?.show()

                        // TODO : Upload Profile Image to Server
                        val fullData = RemoteConstant.getEncryptedString(
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                            SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                            )!!,
                            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mPresignedurl +
                                    RemoteConstant.mSlash + "image" + RemoteConstant.mSlash + "1",
                            RemoteConstant.mGetMethod
                        )
                        val mAuth =
                            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                        profileUploadViewModel.getPreSignedUrlProfile(
                            mAuth,
                            "image",
                            "1",
                            mImageListNew[0].photoUri,
                            ""
                        )

                        profileUploadViewModel.isUploaded.nonNull()
                            .observe(this, androidx.lifecycle.Observer {
                                if (it == true) {
                                    finish()
                                } else {
                                    AppUtils.showCommonToast(
                                        this,
                                        "Error occur please try again later"
                                    )
                                }
                                customProgressDialog?.dismiss()
                            })
                    }

                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))
                    customProgressDialog?.dismiss()
                }
            } else {


                val dataSdk = EditorSDKResult(data!!)

                val settingsList = dataSdk.settingsList
                val ts = settingsList.getSettingsModel(
                    TransformSettings::class.java
                )
                val mRatio = ts.getAspectRation() // Returns the aspect ratio

                var mImageRatio = "1:1"
                when {
                    mRatio.toString() == "1.333333" -> {
                        mImageRatio = "4:3"
                    }
                    mRatio.toString() == "1.777778" -> {
                        mImageRatio = "16:9"
                    }
                    mRatio.toString() == "1" -> {
                        mImageRatio = "1:1"
                    }
                    mRatio.toString() == "0.75" -> {
                        mImageRatio = "3:4"
                    }
                }


                val intent = Intent(this, UploadPhotoActivityMentionHashTag::class.java)
                intent.putExtra("title", "UPLOAD PHOTO")
                intent.putExtra("type", "image")
                intent.putExtra("mImageList", mImageListNew)
                intent.putExtra("IS_FROM_CREATE_PROFILE", isFromProfilePic)
                intent.putExtra("RATIO", mImageRatio)
                startActivity(intent)
                overridePendingTransition(0, 0)
                this.finish()
                if (PickerActivity.mContext != null) {
                    PickerActivity.mContext?.finish()
                }
            }

        } else if (resultCode == Activity.RESULT_CANCELED && requestCode == PESDK_RESULT) {
            // Editor was canceled
            val sourceURI = data?.getParcelableExtra<Uri>(ImgLyIntent.SOURCE_IMAGE_URI)
            // TODO: Do something with the source...
            finish()
        }
    }

    private fun openEditor(inputImage: Uri) {
        val settingsList = createPesdkSettingsList()

        settingsList.configure<LoadSettings> {
            it.source = inputImage
        }


        settingsList.getSettingsModel(AssetConfig::class.java).apply {
            getAssetMap(CropAspectAsset::class.java)
                .clear().add(
                    CropAspectAsset("aspect_1_1", 1, 1, false),
                    CropAspectAsset("aspect_3_4", 3, 4, false),
                    CropAspectAsset("aspect_4_3", 4, 3, false),
                    CropAspectAsset("aspect_16_9", 16, 9, false)
                )
        }

        // Add your own Asset to UI config and select the Force crop Mode.
        settingsList.getSettingsModel(UiConfigAspect::class.java).apply {
            setAspectList(
                CropAspectItem("aspect_1_1"),
                CropAspectItem("aspect_3_4"),
                CropAspectItem("aspect_4_3"),
                CropAspectItem("aspect_16_9")
            )
            forceCropMode = UiConfigAspect.ForceCrop.SHOW_TOOL_NEVER
        }
        val file = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
        if (!file.exists()) {
            file.mkdir()
        }
//        settingsList.getSettingsModel(SaveSettings::class.java)
//            .setExportDir(file.absolutePath)
//            .setExportPrefix("result_")

        settingsList[LoadSettings::class].source = inputImage

        PhotoEditorBuilder(this)
            .setSettingsList(settingsList)
            .startActivityForResult(this, PESDK_RESULT)
    }

    private fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                context.applicationContext,
                uri
            )
        ) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {


            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor?
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

}
