package com.example.sample.socialmob.view.ui.home_module.feed

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.FeedLikeActivityBinding
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.viewmodel.profile.PostCommentLikeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LikeListActivity : BaseCommonActivity(), PostLikeAdapter.SearchPeopleAdapterFollowItemClick,
    PostLikeAdapter.SearchPeopleAdapterItemClick {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mPostId = ""
    private var isAddedLoader: Boolean = false

    private var binding: FeedLikeActivityBinding? = null
    private val postCommentLikeViewModel: PostCommentLikeViewModel by viewModels()
    private var mPostLikeAdapter: PostLikeAdapter? = null
    private var mStatus: ResultResponse.Status? = null

    private var globalLikeList: MutableList<SearchProfileResponseModelPayloadProfile>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.feed_like_activity)

        val i = intent
        if (i.hasExtra("mPostId") && i.getStringExtra("mPostId") != null) {
            mPostId = i.getStringExtra("mPostId") ?: ""
            callApiCommon()
            observeLikeLIst()
        }

        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
            observeLikeLIst()
        }

        binding?.postLikeRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        binding?.postLikeRecyclerView?.addOnScrollListener(CustomScrollListener())

        mPostLikeAdapter = PostLikeAdapter(
            this, this,
            this@LikeListActivity, glideRequestManager
        )

        binding?.postLikeRecyclerView?.adapter = mPostLikeAdapter

    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()


            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 &&
                mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                mStatus != ResultResponse.Status.LOADING && !isAddedLoader
            ) {

                if (InternetUtil.isInternetOn()) {
                    loadMoreItems(globalLikeList?.size!!)
                }
            }

        }
    }

    private fun loadMoreItems(size: Int) {

        val mItemList: MutableList<SearchProfileResponseModelPayloadProfile> = ArrayList()
        val mList = SearchProfileResponseModelPayloadProfile(
            0, "", "",
            "", "", "", "", "", "", "",
            "", false, "", ""
        )
        mItemList.add(mList)
        globalLikeList?.addAll(mItemList)
        mPostLikeAdapter?.notifyDataSetChanged()
        isAddedLoader = true
        callApi(mPostId, size)


    }
    /**
     * Remove loader from list Pagimation
     */
    private fun removeLoaderFormList() {

        if (globalLikeList != null && globalLikeList?.size!! > 0 && globalLikeList?.get(
                globalLikeList?.size!! - 1
            ) != null && globalLikeList?.get(globalLikeList?.size!! - 1)?._id == ""
        ) {
            globalLikeList?.removeAt(globalLikeList?.size!! - 1)
            mPostLikeAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi(mPostId, globalLikeList?.size!!)
        } else {
            postCommentLikeViewModel.responsePaginatedListLikeModel?.postValue(
                ResultResponse(
                    ResultResponse.Status.NO_INTERNET,
                    ArrayList(),
                    getString(R.string.no_internet)
                )
            )
            val shake: Animation = AnimationUtils.loadAnimation(this@LikeListActivity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        }

    }

    private fun observeLikeLIst() {
        postCommentLikeViewModel.responsePaginatedListLikeModel?.nonNull()
            ?.observe(this, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        globalLikeList = resultResponse?.data!!.toMutableList()
                        mPostLikeAdapter?.submitList(globalLikeList)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.VISIBLE
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        globalLikeList?.addAll(resultResponse?.data!!)
                        mPostLikeAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {
                        if (globalLikeList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.dataLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })
    }

    private fun callApi(mPostId: String, size: Int) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mLikeListPost +
                    RemoteConstant.mSlash + mPostId +
                    RemoteConstant.mSlash + size +
                    RemoteConstant.mSlash + RemoteConstant.mCount, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(
                    this,
                    RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        postCommentLikeViewModel.getFeedLikes(
            mAuth,
            mPostId,
            size.toString(),
            RemoteConstant.mCount
        )
    }

    override fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    override fun onSearchPeopleAdapterItemClick(mProfileId: String, mIsOwn: String) {
        if (mIsOwn == "false") {
            val intent = Intent(this, UserProfileActivity::class.java)
            intent.putExtra("mProfileId", mProfileId)
            intent.putExtra("mIsOwn", mIsOwn)
            startActivity(intent)
        } else {
            val intent = Intent(this, MyProfileActivity::class.java)
            startActivity(intent)
        }
    }
    /**
     * Unfollow API
     */
    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        postCommentLikeViewModel.unFollowProfile(mAuth, mId)
    }
    /**
     * Follow API
     */
    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        postCommentLikeViewModel.followProfile(mAuth, mId)
    }
    /**
     * Follow cancel request API
     */
    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        postCommentLikeViewModel.cancelRequest(mAuth, mId)

    }

    fun likeActivityBack(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@LikeListActivity, binding?.noInternetImageView!!)
    }

}