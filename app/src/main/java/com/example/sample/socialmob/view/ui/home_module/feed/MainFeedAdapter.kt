package com.example.sample.socialmob.view.ui.home_module.feed

import android.animation.Animator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Handler
import android.os.SystemClock
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.*
import com.example.sample.socialmob.model.feed.PostFeedDataMention
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleAdapter
import com.example.sample.socialmob.view.ui.home_module.settings.HelpFeedBackReportProblemActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.ui.write_new_post.WritePostActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.cardview.CardView
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroUtil.visibleAreaOffset
import im.ene.toro.exoplayer.ExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.media.VolumeInfo
import im.ene.toro.widget.Container
import kotlinx.android.synthetic.main.detailedimage_view_pager.*
import kotlinx.android.synthetic.main.options_menu_feed_option.view.*
import ly.img.android.pesdk.backend.decoder.ImageSource.getResources
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import java.util.regex.Pattern


class MainFeedAdapter(
    private val mContext: Activity,
    private val feedItemClick: FeedItemClick,
    private val feedItemLikeClick: FeedItemLikeClick,
    private val feedCommentClick: FeedCommentClick,
    private val feedLikeListClick: FeedLikeListClick,
    private val optionItemClickListener: OptionItemClickListener,
    private val hashTagClickListener: HashTagClickListener,
    private val searchPeopleAdapterFollowItemClick: SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick,
    private val searchPeopleAdapterItemClick: SearchPeopleAdapter.SearchPeopleAdapterItemClick,
    private val glideRequestManager: RequestManager,
) : ListAdapter<PersonalFeedResponseModelPayloadFeed, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }


    private var isCalledHashTgaDetailsPage: Boolean = false
    private var mSuggestedMembers: MutableList<SearchProfileResponseModelPayloadProfile>? =
        ArrayList()
    private var mLastClickTime: Long = 0
    private var progressBar: ProgressBar? = null
    private var uploadTextView: TextView? = null

    companion object {
        const val ITEM_TEXT = 1
        const val ITEM_IMAGE = 2
        const val ITEM_VIDEO = 3
        const val ITEM_TRACK = 4
        const val ITEM_LOADING = 5
        const val ITEM_UPLOAD = 6
        const val ITEM_SUGGESTED_MEMBERS = 7
        const val ITEM_AD = 8
        const val ITEM_MUSIC = 9
        const val ITEM_ARTIST = 10
        const val ITEM_PLAYLIST = 11
        const val ITEM_PROFILE = 12
        const val ITEM_MUSIC_VIDEO = 13
        var isMute = true
        var helper: ExoPlayerViewHelper? = null

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PersonalFeedResponseModelPayloadFeed>() {
                override fun areItemsTheSame(
                    oldItem: PersonalFeedResponseModelPayloadFeed,
                    newItem: PersonalFeedResponseModelPayloadFeed,
                ): Boolean {
                    return oldItem.recordId == newItem.recordId || oldItem.action?.post?.description == newItem.action?.post?.description
                }

                override fun areContentsTheSame(
                    oldItem: PersonalFeedResponseModelPayloadFeed,
                    newItem: PersonalFeedResponseModelPayloadFeed,
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_TEXT -> bindText(parent)
            ITEM_IMAGE -> bindImage(parent)
            ITEM_TRACK -> bindTrack(parent)
            ITEM_VIDEO -> bindVideo(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_UPLOAD -> bindUploadLoader(parent)
            ITEM_SUGGESTED_MEMBERS -> bindMembers(parent)
            ITEM_AD -> bindAd(parent)
            ITEM_MUSIC -> bindMusic(parent)
            ITEM_ARTIST -> bindArtist(parent)
            ITEM_PLAYLIST -> bindPlaylist(parent)
            ITEM_PROFILE -> bindProfile(parent)
            ITEM_MUSIC_VIDEO -> bindMusicVideo(parent)
            else -> bindText(parent)
        }

    }


    inner class UnifiedNativeAdViewHolder internal constructor(view: View) :
        BaseViewHolder<Any>(view) {
        override fun bind(`object`: Any?) {
            try {
                if (
                    (mContext as HomeActivity).getLoadedAds() != null &&
                    mContext.getLoadedAds()?.size!! > 0
                ) {
                    cardView.visibility = View.VISIBLE
                    val random = ThreadLocalRandom.current()
                        .nextInt(0, mContext.getLoadedAds()?.size!!)
                    val nativeAd = mContext.getLoadedAds()?.get(random)
                    populateNativeAdView(nativeAd!!, adView)
                } else {
                    cardView.visibility = View.GONE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private val adView: UnifiedNativeAdView =
            view.findViewById<View>(R.id.ad_view) as UnifiedNativeAdView
        private val cardView: CardView =
            view.findViewById<View>(R.id.card_view) as CardView

        init {

            // The MediaView will display a video asset if one is present in the ad, and the
            // first image asset otherwise.
            adView.mediaView = adView.findViewById<View>(R.id.ad_media) as MediaView

            // Register the view used for                                                                                                         each individual asset.
            adView.headlineView = adView.findViewById(R.id.ad_headline)
            adView.bodyView = adView.findViewById(R.id.ad_body)
            adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
            adView.iconView = adView.findViewById(R.id.ad_icon)
            adView.priceView = adView.findViewById(R.id.ad_price)
            adView.starRatingView = adView.findViewById(R.id.ad_stars)
            adView.storeView = adView.findViewById(R.id.ad_store)
            adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        }


    }

    private fun populateNativeAdView(
        nativeAd: UnifiedNativeAd,
        adView: UnifiedNativeAdView,
    ) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        (adView.headlineView as TextView).text = nativeAd.headline
        (adView.bodyView as TextView).text = nativeAd.body
        (adView.callToActionView as Button).text = nativeAd.callToAction

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        val icon = nativeAd.icon

        if (icon == null) {
            adView.iconView.visibility = View.INVISIBLE
        } else {
            (adView.iconView as ImageView).setImageDrawable(icon.drawable)
            adView.iconView.visibility = View.VISIBLE
        }

        if (nativeAd.price == null) {
            adView.priceView.visibility = View.INVISIBLE
        } else {
            adView.priceView.visibility = View.VISIBLE
            (adView.priceView as TextView).text = nativeAd.price
        }

        if (nativeAd.store == null) {
            adView.storeView.visibility = View.INVISIBLE
        } else {
            adView.storeView.visibility = View.VISIBLE
            (adView.storeView as TextView).text = nativeAd.store
        }

        if (nativeAd.starRating == null) {
            adView.starRatingView.visibility = View.INVISIBLE
        } else {
            (adView.starRatingView as RatingBar).rating = nativeAd.starRating!!.toFloat()
            adView.starRatingView.visibility = View.VISIBLE
        }

        if (nativeAd.advertiser == null) {
            adView.advertiserView.visibility = View.INVISIBLE
        } else {
            (adView.advertiserView as TextView).text = nativeAd.advertiser
            adView.advertiserView.visibility = View.VISIBLE
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd)
    }


    override fun getItemViewType(position: Int): Int {
        if (getItem(position).action != null && getItem(position).action?.post != null && getItem(
                position
            ).action?.post?.type != null
        ) {
            return when (getItem(position).action?.post?.type) {
                "user_text_post" -> {
                    when (getItem(position).action?.post?.ogTags?.meta?.socialmobMeta?.pageType) {
                        "music" -> {
                            ITEM_MUSIC
                        }
                        "artist" -> {
                            ITEM_ARTIST
                        }
                        "playlist" -> {
                            ITEM_PLAYLIST
                        }
                        "profile" -> {
                            ITEM_PROFILE
                        }
                        "track-video" -> {
                            ITEM_MUSIC_VIDEO
                        }
                        else -> {
                            ITEM_TEXT
                        }
                    }
                }
                "user_image_post" -> {
                    ITEM_IMAGE
                }
                "user_video_post" -> {
                    ITEM_VIDEO
                }
                "user_track" -> {
                    //                    ITEM_TRACK
                    ITEM_SUGGESTED_MEMBERS
                }
                "suggested_mobbers" -> {
                    ITEM_SUGGESTED_MEMBERS
                }
                else -> {
                    ITEM_TEXT
                }
            }
        } else {
            return when {
                getItem(position).isAd != null && getItem(position).isAd == "ad" -> ITEM_AD
                getItem(position).isLoader != null && getItem(position).isLoader == "upload" -> ITEM_UPLOAD
                else -> ITEM_LOADING
            }
        }

    }


    private fun bindMembers(parent: ViewGroup): BaseViewHolder<Any> {

        val binding: LayoutItemMobbersBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_mobbers, parent, false
        )
        return ViewHolderItemFeedMembers(binding)
    }

    private fun bindUploadLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: UploadProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.upload_progress_item, parent, false
        )
        return UploadProgressViewHolder(binding)

    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindTrack(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemTrackBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_track, parent, false
        )
        return ViewHolderItemFeedTrack(binding)

    }

    private fun bindVideo(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemVideoExoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_video_exo, parent, false
        )
        return VideoViewHolder(binding)
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {
        val unifiedNativeLayoutView =
            LayoutInflater.from(parent.context).inflate(R.layout.ad_unified, parent, false)
        return UnifiedNativeAdViewHolder(unifiedNativeLayoutView)
    }

    private fun bindImage(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_image, parent, false
        )
        return ViewHolderItemImage(binding)
    }

    private fun bindText(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemTextBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_text, parent, false
        )
        return ViewHolderItemText(binding)
    }

    private fun bindArtist(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemArtistBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_artist, parent, false
        )
        return ViewHolderItemArtist(binding)
    }

    private fun bindPlaylist(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemPlaylistBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_playlist, parent, false
        )
        return ViewHolderItemPlayList(binding)
    }

    private fun bindMusic(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemMusicBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_music, parent, false
        )
        return ViewHolderItemMusic(binding)
    }

    private fun bindMusicVideo(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemMusicVideoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_music_video, parent, false
        )
        return ViewHolderItemMusicVideo(binding)
    }

    private fun bindProfile(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemProfileBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_profile, parent, false
        )
        return ViewHolderItemProfile(binding)

    }

    inner class VideoViewHolder(val binding: LayoutItemVideoExoBinding) :
        BaseViewHolder<Any>(binding.root),
        ToroPlayer {
        private val player = binding.player
        private var videoUri: Uri? = null
        private var helperExo: ExoPlayerViewHelper? = null
        private var listener: ToroPlayer.EventListener? = null

        @SuppressLint("SetTextI18n")
        override fun bind(item: Any?) {
            val videoUrl = getItem(adapterPosition).action?.post?.media?.get(0)?.sourcePath

            if (videoUrl !== null) videoUri = Uri.parse(videoUrl)
            binding.videoViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()


            if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                    var likedCollection: String = ""
                    for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                        if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                            binding.feedpostLikedby.cvFeedPostLiker1?.visibility =
                                View.VISIBLE

                            binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                View.GONE
                            binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                View.GONE
                            if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                    i
                                )?.pimage != ""
                            ) {
                                glideRequestManager.load(
                                    getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage
                                ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                            } else {
                                glideRequestManager.load(R.drawable.emptypicture)
                                    .into(binding.feedpostLikedby.ivFeedPostLiker1)
                            }

                            likedCollection =
                                "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                    i
                                )?.Name + "."
                            if (likedCollection.length >= 20) {
                                likedCollection.substring(
                                    0,
                                    20
                                ) + "..."
                            } else {
                                likedCollection
                            }
                            binding.feedpostLikedby.textViewLikedby.text =
                                likedCollection
                        } else {
                            if (i == 0) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name

                            } else {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                }

                                if (i > 2) {
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.VISIBLE
                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    }
                                }
                            }

                            if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                val mlikedCollection: String =
                                    if (likedCollection.length >= 20) {
                                        likedCollection.substring(
                                            0,
                                            20
                                        ) + "..."
                                    } else {
                                        likedCollection
                                    }
                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " others."
                                } else
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " other."
                            } else {
                                binding.feedpostLikedby.textViewLikedby.text = likedCollection
                            }
                        }
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }
            } else {
                binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
            }

            if (getItem(adapterPosition).action?.post?.lastComment != null) {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                binding.feedpostLastComment.textviewLastcommentUsername.text =
                    getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
            } else {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
            }

            when (getItem(adapterPosition).actor?.id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
            }
            binding?.followingTextView?.setOnClickListener {

                if (getItem(adapterPosition).actor?.relation == "following") {
                    val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                    val inflater = mContext.layoutInflater
                    val dialogView = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        getItem(adapterPosition).actor?.relation ?: "",
                        getItem(adapterPosition).actor?.id ?: ""
                    )
                    Handler().postDelayed({
                        notifyFollowing(
                            adapterPosition,
                            getItem(adapterPosition).actor?.privateProfile
                        )
                    }, 100)
                }


            }

            glideRequestManager
                .load(getItem(adapterPosition).action?.post?.media!![0].thumbnail)
                .thumbnail(0.1f)
                .into(binding.mThumnail)

            binding.heartAnim.visibility = View.GONE

            binding.imgVol.setOnClickListener {
                isMute = if (isMute) {
                    stopMusic()
                    helperExo?.volumeInfo = VolumeInfo(false, 1.0f)
                    binding.imgVol.setImageResource(R.drawable.ic_volume_up_grey_24dp)
                    false
                } else {
                    helperExo?.volumeInfo = VolumeInfo(false, 0.0f)
                    binding.imgVol.setImageResource(R.drawable.ic_volume_off_grey_24dp)
                    true
                }

            }
            // TODO : Double TAP
            val gd = GestureDetector(mContext, object : GestureDetector.SimpleOnGestureListener() {
                override fun onDoubleTap(e: MotionEvent): Boolean {
                    if (adapterPosition != RecyclerView.NO_POSITION) /*{
                        feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "video")
                   } */ {
                        binding.heartAnim.bringToFront()
                        binding.heartAnim.visibility = View.VISIBLE
                        binding.heartAnim.playAnimation()
                        if (getItem(adapterPosition).action?.post?.liked == false) {
                            binding.feedpostReview.likeMaterialButton.performClick()
                        }
                        binding.heartAnim.addAnimatorListener(object : Animator.AnimatorListener {
                            override fun onAnimationStart(animation: Animator) {
                                Log.e("Animation:", "start")

                            }

                            override fun onAnimationEnd(animation: Animator) {
                                Log.e("Animation:", "end")
//                            binding.backgroundDimmer.setBackgroundResource(android.R.color.transparent)
                                binding.heartAnim.visibility = View.GONE

                            }

                            override fun onAnimationCancel(animation: Animator) {
                                Log.e("Animation:", "cancel")
                            }

                            override fun onAnimationRepeat(animation: Animator) {
                                Log.e("Animation:", "repeat")
                                binding.heartAnim.pauseAnimation()
                                binding.heartAnim.visibility = View.GONE
                            }
                        })
                    }

                    return true
                }
            })
            if (getItem(adapterPosition).action?.post?.media?.isNotEmpty()!!) {
                if (getItem(adapterPosition).action?.post?.media!![0].ratio != null) {
                    when (getItem(adapterPosition).action?.post?.media!![0].ratio) {
                        "1:1" -> {
                            binding.mediaContainer.setAspectRatio(1F, 1F)
                        }
                        "4:3" -> {
                            binding.mediaContainer.setAspectRatio(4F, 3F)
                        }
                        "16:9" -> {
                            binding.mediaContainer.setAspectRatio(16F, 9F)
                        }
                        "9:16" -> {
                            val valString = getScreenResolution(mContext)

                            if (valString!! < 1800) {
                                binding.mediaContainer.setAspectRatio(4F, 4F)
                            } else {
                                binding.mediaContainer.setAspectRatio(4F, 5F)
                            }
                        }
                        else -> {
                            binding.mediaContainer.setAspectRatio(1F, 1F)
                        }
                    }
                }
            }

            itemView.setOnTouchListener { v, event ->
                gd.onTouchEvent(event)

            }

            binding.feedpostReview.likeMaterialButton.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION &&
                    getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.liked != null
                ) {
                    var mCount: Int
                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                        binding.feedpostReview.likeMaterialButton.isFavorite = false
                        mCount =
                            Integer.parseInt(getItem(adapterPosition).action?.post?.likesCount!!)
                                .minus(1)
                        getItem(adapterPosition).action?.post?.likesCount = mCount.toString()
                        getItem(adapterPosition).action?.post?.liked = false
                        binding.feedpostReview.likeCountTextView.text = "$mCount Likes"
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                        binding.feedpostReview.likeMaterialButton.isFavorite = true
                        mCount =
                            Integer.parseInt(getItem(adapterPosition).action?.post?.likesCount!!)
                                .plus(1)
                        getItem(adapterPosition).action?.post?.likesCount = mCount.toString()
                        getItem(adapterPosition).action?.post?.liked = true
                        binding.feedpostReview.likeCountTextView.text = "$mCount Likes"
                    }
                    if (mCount > 0) {
                        binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#1e90ff"))
                    } else {
                        binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#747d8c"))
                    }
                }

            }
            binding.feedpostReview.commentLinear.setOnClickListener {
                feedCommentClick.onCommentClick(
                    getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                    getItem(adapterPosition).action?.post?.own!!
                )


                //feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "video")

            }

            binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                if (
                    getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.id != null &&
                    getItem(adapterPosition).action?.post?.own != null
                ) {
                    feedCommentClick.onCommentClick(
                        getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                        getItem(adapterPosition).action?.post?.own!!
                    )
                    //feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "video")
                }
            }

            binding.tvViewAllComments.setOnClickListener {
                feedCommentClick.onCommentClick(
                    getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                    getItem(adapterPosition).action?.post?.own!!
                )
                //feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "video")

            }

            binding.feedpostReview.likeLinear.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition)?.action?.post?.likesCount!!
                )
            }
            binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition)?.action?.post?.likesCount!!
                )
            }

            binding.feedpostReview.llshare.setOnClickListener {
                ShareAppUtils.sharePost(
                    getItem(adapterPosition)?.action?.post?.type,
                    getItem(adapterPosition)?.action?.post?.id!!, mContext
                )
            }

            binding.feedPostOptionImageView.setOnClickListener {
                feedOptions(
                    binding.feedPostOptionImageView, adapterPosition,
                    getItem(adapterPosition)
                )
            }
            binding.descriptionTextView.setOnMentionClickListener { view, text ->
                mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
            }
            binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                hashTagClick(
                    getItem(adapterPosition).action?.post?.hashTags!!, text,
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition).action?.post?.likesCount!!,
                    getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                )
            }
            binding.profileFeedNameTextView.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding.profileImageView.setOnClickListener {
                goToProfile(getItem(adapterPosition).actor?.id!!)
            }
        }

        override fun getPlayerView(): PlayerView = player

        override fun getCurrentPlaybackInfo() = helperExo?.latestPlaybackInfo ?: PlaybackInfo()

        override fun initialize(container: Container, playbackInfo: PlaybackInfo) {
            if (helperExo == null) helperExo =
                ExoPlayerViewHelper(this, videoUri!!, null, MyApplication.config!!)
            if (listener == null) {
                listener = object : ToroPlayer.EventListener {
                    override fun onFirstFrameRendered() {
                        println(">>onFirstFrameRendered")
                        helper = helperExo
                        binding.player.post {
                            binding.player.visibility = View.VISIBLE
                            binding.player.bringToFront()
                            binding.imgVol.bringToFront()
                        }
                        binding.mThumnail.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE

                        if (!isMute) {
                            helperExo?.volumeInfo = VolumeInfo(false, 1.0f)
                            binding.imgVol.setImageResource(R.drawable.ic_volume_up_grey_24dp)
                        } else {
                            helperExo?.volumeInfo = VolumeInfo(false, 0.0f)
                            binding.imgVol.setImageResource(R.drawable.ic_volume_off_grey_24dp)
                        }
                    }

                    override fun onBuffering() {
                        println(">>onBuffering")
                        binding.mThumnail.post {
                            binding.mThumnail.visibility = View.VISIBLE
                            binding.mThumnail.bringToFront()
                        }
                        binding.mThumnail.post {
                            binding.progressBar.visibility = View.VISIBLE
                            binding.progressBar.bringToFront()
                        }
                        binding.player.visibility = View.GONE
                    }

                    override fun onPlaying() {
                        println(">>onPlaying")
                        binding.mThumnail.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        binding.player.visibility = View.VISIBLE
                    }

                    override fun onPaused() {
                        println(">>onPaused")
                        binding.progressBar.visibility = View.GONE
                        binding.player.visibility = View.GONE
                        binding.mThumnail.post {
                            binding.mThumnail.visibility = View.VISIBLE
                            binding.mThumnail.bringToFront()
                        }
                    }

                    override fun onCompleted() {
                        println(">>onCompleted")
                        binding.mThumnail.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE
                        binding.player.visibility = View.VISIBLE
                    }

                }
                helperExo!!.addPlayerEventListener(listener!!)
            }
            helperExo!!.initialize(container, playbackInfo)
        }

        override fun play() {
            helperExo?.play()
        }

        override fun pause() {
            helperExo?.pause()
        }

        override fun isPlaying() = helperExo?.isPlaying ?: false

        override fun release() {
            if (listener != null) {
                helperExo?.removePlayerEventListener(listener)
                listener = null
            }
            helperExo?.release()
            helperExo = null
        }

        override fun wantsToPlay() = visibleAreaOffset(this, itemView.parent) >= 0.65

        override fun getPlayerOrder() = adapterPosition
    }

    private fun mentionClick(
        mentions: List<PostFeedDataMention>?,
        text: CharSequence,
    ) {
        for (i in mentions?.indices!!) {
            if (mentions[i].text!!.contains(text)) {
                if (mentions[i]._id != null) {
                    val intent = Intent(mContext, UserProfileActivity::class.java)
                    intent.putExtra(
                        "mProfileId",
                        mentions[i]._id
                    )
                    mContext.startActivity(intent)
                }
            }
        }
    }

    private fun goToProfile(id: String) {
        val intent = Intent(mContext, UserProfileActivity::class.java)
        intent.putExtra("mProfileId", id)
        mContext.startActivity(intent)
    }


    private fun stopMusic() {
        (mContext as HomeActivity).stopMusicPlay()
    }

    private fun removeUrl(commentstr: String): String {
        var commentstr = commentstr
        try {
            val urlPattern =
                "((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?+-=\\\\.&]*)"
            val p = Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE)
            val m = p.matcher(commentstr)
            var i = 0
            while (m.find()) {
                commentstr = commentstr.replace(m.group(i).toRegex(), "").trim { it <= ' ' }
                i++
            }
            return commentstr
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    inner class ViewHolderItemImage(val binding: LayoutItemImageBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(`object`: Any?) {

            val feedImageListAdapterInner: FeedImageListAdapterInner?
            binding?.imageViewProfileModel = getItem(adapterPosition)
            binding?.executePendingBindings()

            if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                    binding?.feedpostLikedby?.feedpostLikedby?.visibility = View.VISIBLE
                    var likedCollection: String = ""
                    for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                        if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                            binding?.feedpostLikedby?.cvFeedPostLiker1?.visibility =
                                View.VISIBLE

                            binding?.feedpostLikedby?.cvFeedPostLiker2?.visibility =
                                View.GONE
                            binding?.feedpostLikedby?.cvFeedPostLiker3?.visibility =
                                View.GONE
                            if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                    i
                                )?.pimage != ""
                            ) {
                                glideRequestManager.load(
                                    getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage
                                ).into(binding?.feedpostLikedby?.ivFeedPostLiker1!!)
                            } else {
                                glideRequestManager.load(R.drawable.emptypicture)
                                    .into(binding?.feedpostLikedby?.ivFeedPostLiker1!!)
                            }

                            likedCollection =
                                "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                    i
                                )?.Name + "."
                            if (likedCollection.length >= 20) {
                                likedCollection.substring(
                                    0,
                                    20
                                ) + "..."
                            } else {
                                likedCollection
                            }
                            binding.feedpostLikedby.textViewLikedby.text =
                                likedCollection
                        } else {
                            if (i == 0) {
                                binding?.feedpostLikedby?.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE
                                binding?.feedpostLikedby?.cvFeedPostLiker2?.visibility =
                                    View.GONE
                                binding?.feedpostLikedby?.cvFeedPostLiker3?.visibility =
                                    View.GONE

                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding?.feedpostLikedby?.ivFeedPostLiker1!!)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding?.feedpostLikedby?.ivFeedPostLiker1!!)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name

                            } else {
                                binding?.feedpostLikedby?.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE
                                binding?.feedpostLikedby?.cvFeedPostLiker2?.visibility =
                                    View.VISIBLE
                                binding?.feedpostLikedby?.cvFeedPostLiker3?.visibility =
                                    View.GONE

                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding?.feedpostLikedby?.ivFeedPostLiker2!!)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding?.feedpostLikedby?.ivFeedPostLiker2!!)
                                }

                                if (i > 2) {
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.VISIBLE
                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    }
                                }
                            }

                            if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                val mlikedCollection: String =
                                    if (likedCollection.length >= 20) {
                                        likedCollection.substring(
                                            0,
                                            20
                                        ) + "..."
                                    } else {
                                        likedCollection
                                    }
                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " others."
                                } else
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " other."
                            } else {
                                binding.feedpostLikedby.textViewLikedby.text = likedCollection
                            }
                        }
                    }
                } else {
                    binding?.feedpostLikedby?.feedpostLikedby?.visibility = View.GONE
                }
            } else {
                binding?.feedpostLikedby?.feedpostLikedby?.visibility = View.GONE
            }

            if (getItem(adapterPosition).action?.post?.lastComment != null) {
                binding?.feedpostLastComment?.clfeedPostComments?.visibility = View.VISIBLE
                binding?.feedpostLastComment?.textviewLastcommentUsername?.text =
                    getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                settextMarquee(binding?.feedpostLastComment?.textviewLastcomment!!, getItem(adapterPosition).action?.post?.lastComment?.test)
            } else {
                binding?.feedpostLastComment?.clfeedPostComments?.visibility = View.GONE
            }

            when (getItem(adapterPosition).actor?.id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
            }
            binding?.followingTextView?.setOnClickListener {

                if (getItem(adapterPosition).actor?.relation == "following") {
                    val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                    val inflater = mContext.layoutInflater
                    val dialogView = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        getItem(adapterPosition).actor?.relation ?: "",
                        getItem(adapterPosition).actor?.id ?: ""
                    )
                    Handler().postDelayed({
                        notifyFollowing(
                            adapterPosition,
                            getItem(adapterPosition).actor?.privateProfile
                        )
                    }, 100)
                }


            }

            // TODO : Double TAP
            val gd = GestureDetector(mContext, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (itemCount > 0 && getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null &&
                            getItem(adapterPosition).action?.post?.media != null &&
                            getItem(adapterPosition).action?.post?.media!!.isNotEmpty()
                        ) {
                            feedItemClick.onFeedItemClick(
                                getItem(adapterPosition).action?.post?.id!!,
                                "image"
                            )
                        }
                        /*{
                                val dialog: Dialog?
                                dialog = Dialog(mContext, R.style.You_Dialog)
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                dialog.setContentView(R.layout.detailedimage_view_pager)
                                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                val window = dialog.window
                                window?.setLayout(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT
                                )

                                val mMedia = getItem(adapterPosition).action?.post?.media

                                dialog.view_pager.adapter = ImageDetailsAdapter(mContext, mMedia!!,glideRequestManager)
                                dialog.view_pager.currentItem = binding?.imageListpager?.currentItem!!

                                val url = getItem(adapterPosition).actor?.pimage
                                try {
                                    if (url != null && url != "" && !url.contains("googleusercontent")) {
                                        glideRequestManager.load(getImageUrlFromWidth(url, 500))
                                            .thumbnail(0.1f).apply(RequestOptions().fitCenter())
                                            .into(dialog.profile_image_view)
                                    } else {
                                        if (url != null && url != "") {
                                            if (url.contains(".jpg")) {
                                                val str = url.replace("s96-c/photo.jpg", "")
                                                val str2 = str + "s700-c/photo.jpg"
                                                glideRequestManager.load(str2).thumbnail(0.1f)
                                                    .into(dialog.profile_image_view)
                                            } else {
                                                val str = url.replace("s96-c", "")
                                                val str2 = str + "s700-c"
                                                glideRequestManager.load(str2).thumbnail(0.1f)
                                                    .into(dialog.profile_image_view)
                                            }
                                        }
                                    }


                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                if (getItem(adapterPosition).actor?.username != null && getItem(
                                        adapterPosition
                                    ).actor?.username != ""
                                ) {
                                    val mTitle: String =
                                        if (getItem(adapterPosition).actor?.username?.length!! >= 10) {
                                            getItem(adapterPosition).actor?.username?.substring(
                                                0,
                                                10
                                            ) + "..."
                                        } else {
                                            getItem(adapterPosition).actor?.username!!
                                        }
                                    dialog.profile_feed_name_text_view.text = mTitle
                                }

                                dialog.close_image_view_alert_view_pager?.bringToFront()

                                dialog.close_image_view_alert_view_pager?.setOnClickListener {
                                    dialog.dismiss()
                                }
                                dialog.show()
                            }*/
                    }
                    return super.onSingleTapConfirmed(e)
                }

                override fun onDown(e: MotionEvent): Boolean {
                    return true
                }

                override fun onDoubleTap(e: MotionEvent): Boolean {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        // TODO : Animation Heart
                        binding?.heartAnim?.bringToFront()
                        binding?.heartAnim?.visibility = View.VISIBLE
                        binding?.heartAnim?.playAnimation()
                        if (getItem(adapterPosition).action?.post?.liked == false) {
                            binding?.feedpostReview?.likeMaterialButton?.performClick()
                        }
                        binding?.heartAnim?.addAnimatorListener(object : Animator.AnimatorListener {
                            override fun onAnimationStart(animation: Animator) {
                                Log.e("Animation:", "start")

                            }

                            override fun onAnimationEnd(animation: Animator) {
                                Log.e("Animation:", "end")
                                binding.heartAnim.visibility = View.GONE

                            }

                            override fun onAnimationCancel(animation: Animator) {
                                Log.e("Animation:", "cancel")
                            }

                            override fun onAnimationRepeat(animation: Animator) {
                                Log.e("Animation:", "repeat")
                                binding.heartAnim.pauseAnimation()
                                binding.heartAnim.visibility = View.GONE
                            }
                        })


                        if (getItem(adapterPosition).action?.post?.liked == false) {
                            binding?.feedpostReview?.likeMaterialButton?.performClick()
                        }
                    }
                    return true
                }

                override fun onDoubleTapEvent(e: MotionEvent): Boolean {
                    return true
                }
            })

            binding?.imageListpager?.setOnTouchListener { v, event -> gd.onTouchEvent(event) }

            if (getItem(adapterPosition).action?.post?.media!!.isNotEmpty()) {

                feedImageListAdapterInner = FeedImageListAdapterInner(
                    getItem(adapterPosition).action?.post?.media,
                    mContext, "", glideRequestManager
                )
                when (getItem(adapterPosition).action?.post?.media!![0].ratio) {
                    "1:1" -> {
                        binding?.aspectRatioLayout?.setAspectRatio(1F, 1F)
                    }
                    "3:4" -> {
                        binding?.aspectRatioLayout?.setAspectRatio(3F, 4F)
                    }
                    "4:3" -> {
                        binding?.aspectRatioLayout?.setAspectRatio(4F, 3F)
                    }
                    "16:9" -> {
                        binding?.aspectRatioLayout?.setAspectRatio(16F, 9F)
                    }
                    "9:16" -> {
                        val valString = getScreenResolution(mContext)
                        Log.d("ViewHolderItemImage1:- ", "$valString")

                        if (valString!! < 1800) {
                            binding?.aspectRatioLayout?.setAspectRatio(4F, 4F)
                        } else {
                            binding?.aspectRatioLayout?.setAspectRatio(4F, 5F)
                        }
                    }
                    else -> {
                        binding?.aspectRatioLayout?.setAspectRatio(1F, 1F)
                    }
                }
                binding?.imageListpager?.offscreenPageLimit = 5
                binding?.imageListpager?.adapter = feedImageListAdapterInner

                if (getItem(adapterPosition).action?.post?.media!!.size > 1) {
                    binding?.countTextView?.visibility = View.VISIBLE

                    binding?.imagePosition = "" + 1 + "/" +
                            getItem(adapterPosition).action?.post?.media?.size
                    binding?.imageListpager?.addOnPageChangeListener(object :
                        androidx.viewpager.widget.ViewPager.OnPageChangeListener {

                        override fun onPageScrollStateChanged(state: Int) {
                        }

                        override fun onPageScrolled(
                            position: Int, positionOffset: Float,
                            positionOffsetPixels: Int,
                        ) {

                        }

                        override fun onPageSelected(position: Int) {
                            binding.imagePosition = "" + position.plus(1) + "/" +
                                    getItem(adapterPosition).action?.post?.media?.size
                        }

                    })
                } else {
                    binding?.countTextView?.visibility = View.GONE
                }

            }
            binding?.profileFeedNameTextView?.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding?.profileImageView?.setOnClickListener {
                if (itemCount > 0 &&
                    getItem(adapterPosition)?.actor != null &&
                    getItem(adapterPosition).actor?.id != null
                ) {
                    goToProfile(getItem(adapterPosition).actor?.id!!)
                }
            }

            binding?.feedpostReview?.likeMaterialButton?.setOnClickListener {

                // TODO : Viewpager scroll to 0
                // TODO:  Do not change it if notify it will reset adapter viewpager ( Image list - multiple item )
                var mCount = 0
                if (itemCount > 0 && getItem(adapterPosition).action != null && getItem(
                        adapterPosition
                    ).action?.post != null &&
                    getItem(adapterPosition).action?.post?.liked != null
                ) {
                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                        binding?.feedpostReview.likeMaterialButton.isFavorite = false
                        mCount =
                            Integer.parseInt(getItem(adapterPosition).action?.post?.likesCount!!)
                                .minus(1)
                        getItem(adapterPosition).action?.post?.likesCount = mCount.toString()
                        getItem(adapterPosition).action?.post?.liked = false
                        binding?.feedpostReview.likeCountTextView.text = "$mCount Likes"

                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                        binding.feedpostReview.likeMaterialButton.isFavorite = true
                        mCount =
                            Integer.parseInt(getItem(adapterPosition).action?.post?.likesCount!!)
                                .plus(1)
                        getItem(adapterPosition).action?.post?.likesCount = mCount.toString()
                        getItem(adapterPosition).action?.post?.liked = true
                        binding?.feedpostReview.likeCountTextView.text = "$mCount Likes"

                    }

                }
                if (mCount > 0) {
                    binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#1e90ff"))
                } else {
                    binding?.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#747d8c"))
                }

            }
            binding?.feedpostReview?.commentLinear?.setOnClickListener {
                /*feedCommentClick.onCommentClick(
                    getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                    getItem(adapterPosition).action?.post?.own!!
                )*/
                feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "image")
            }

            binding?.feedpostLastComment?.clfeedPostComments?.setOnClickListener {
                if (
                    getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.id != null &&
                    getItem(adapterPosition).action?.post?.own != null
                ) {
                    feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!,
                        "image")
                }
            }

            binding?.tvViewAllComments?.setOnClickListener {
                /*feedCommentClick.onCommentClick(
                    getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                    getItem(adapterPosition).action?.post?.own!!
                )*/

                feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "image")

            }

            binding?.feedpostReview?.likeLinear?.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition)?.action?.post?.likesCount!!
                )
            }

            binding?.feedpostLikedby?.feedpostLikedby?.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition)?.action?.post?.likesCount!!
                )
            }

            binding?.feedpostReview?.llshare?.setOnClickListener {
                ShareAppUtils.sharePost(
                    getItem(adapterPosition)?.action?.post?.type,
                    getItem(adapterPosition)?.action?.post?.id!!, mContext
                )
            }

            binding?.feedImageLinear?.setOnClickListener {
                feedItemClick.onFeedItemClick(getItem(adapterPosition).action?.post?.id!!, "image")
            }

            binding?.feedPostOptionImageView?.setOnClickListener {
                feedOptions(
                    binding.feedPostOptionImageView, adapterPosition,
                    getItem(adapterPosition)
                )
            }
            binding?.descriptionTextView?.setOnMentionClickListener { view, text ->
                mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
            }
            binding?.descriptionTextView?.setOnHashtagClickListener { view, text ->
                hashTagClick(
                    getItem(adapterPosition).action?.post?.hashTags!!, text,
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition).action?.post?.likesCount!!,
                    getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                )
            }
        }


    }

    private fun deletePost(adapterPosition: Int, mQQPop: PopupWindow, id: String) {
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(mContext) }
        val inflater: LayoutInflater = mContext.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val okButton: Button = dialogView.findViewById(R.id.ok_button)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        okButton.setOnClickListener {
            optionItemClickListener.onOptionItemClickOption(
                "delete", id, adapterPosition
            )
            mQQPop.dismiss()
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        cancelButton.setOnClickListener {
            mQQPop.dismiss()
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }


    inner class ViewHolderItemProfile(val binding: LayoutItemProfileBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {
            binding.textViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (getItem(adapterPosition).action?.post?.ogTags != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = getItem(adapterPosition).action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(getItem(adapterPosition).action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }
                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                    if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                            if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."
                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }
                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.Name

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (getItem(adapterPosition).action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }
            }

            when (getItem(adapterPosition).actor?.id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
            }
            binding?.followingTextView?.setOnClickListener {

                if (getItem(adapterPosition).actor?.relation == "following") {
                    val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                    val inflater = mContext.layoutInflater
                    val dialogView = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        getItem(adapterPosition).actor?.relation ?: "",
                        getItem(adapterPosition).actor?.id ?: ""
                    )
                    Handler().postDelayed({
                        notifyFollowing(
                            adapterPosition,
                            getItem(adapterPosition).actor?.privateProfile
                        )
                    }, 100)
                }


            }

            binding.feedpostReview.likeMaterialButton.setOnClickListener {

                if (getItem(adapterPosition).action?.post?.liked == true) {
                    feedItemLikeClick.onFeedItemLikeClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        "false"
                    )
                } else {
                    feedItemLikeClick.onFeedItemLikeClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        "true"
                    )
                }
                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }
            binding.feedpostReview.commentLinear.setOnClickListener {
                if (
                    getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.id != null &&
                    getItem(adapterPosition).action?.post?.own != null
                ) {
                    feedCommentClick.onCommentClick(
                        getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                        getItem(adapterPosition).action?.post?.own!!
                    )
                }
            }

            binding.tvViewAllComments.setOnClickListener {
                if (
                    getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.id != null &&
                    getItem(adapterPosition).action?.post?.own != null
                ) {
                    feedCommentClick.onCommentClick(
                        getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                        getItem(adapterPosition).action?.post?.own!!
                    )
                }
            }
            binding.feedpostReview.likeLinear.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition)?.action?.post?.likesCount!!
                )
            }
            binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    getItem(adapterPosition).action?.post?.id!!,
                    getItem(adapterPosition)?.action?.post?.likesCount!!
                )
            }
            binding.feedpostReview.llshare.setOnClickListener {
                ShareAppUtils.sharePost(
                    getItem(adapterPosition)?.action?.post?.type,
                    getItem(adapterPosition)?.action?.post?.id!!, mContext
                )
            }

            binding.scrapLinear.setOnClickListener {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                    return@setOnClickListener
                }
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (getItem(adapterPosition).action != null
                        && getItem(adapterPosition).action?.post != null
                        && getItem(adapterPosition).action?.post?.ogTags != null
                        && getItem(adapterPosition).action?.post?.ogTags?.url != null
                        && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                    ) {
                        if (getItem(adapterPosition).action?.post?.ogTags?.url!!.contains("http")) {

                            val url = getItem(adapterPosition).action?.post?.ogTags?.url
                            (mContext as HomeActivity).hyperLinkLinkClick(url)
                        }
                    } else {
                        if (retrieveLinks(getItem(adapterPosition).action?.post?.description!!) != "") {
                            try {
                                goToLink(getItem(adapterPosition).action?.post?.description!!)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
                mLastClickTime = SystemClock.elapsedRealtime()
            }

            binding.descriptionTextView.setOnClickListener {
                binding.scrapLinear.performClick()
            }
            binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    hashTagClick(
                        getItem(adapterPosition).action?.post?.hashTags!!, text,
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition).action?.post?.likesCount!!,
                        getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                    )
                }

            }
            binding.descriptionTextView.setOnMentionClickListener { view, text ->
                mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
            }
            binding.profileFeedNameTextView.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding.profileImageView.setOnClickListener {
                goToProfile(getItem(adapterPosition).actor?.id!!)
            }

            binding.feedPostOptionImageView.setOnClickListener {
                feedOptions(
                    binding.feedPostOptionImageView, adapterPosition,
                    getItem(adapterPosition)
                )
            }
        }
    }

    inner class ViewHolderItemMusicVideo(val binding: LayoutItemMusicVideoBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {
            binding.textViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (getItem(adapterPosition).action?.post?.ogTags != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = getItem(adapterPosition).action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }


                if (getItem(adapterPosition)?.action?.post?.ogTags?.meta?.socialmobMeta?.trackVideo?.artist != null) {
                    var mArtistName = ""
                    val artist =
                        getItem(adapterPosition)?.action?.post?.ogTags?.meta?.socialmobMeta?.trackVideo?.artist
                    for (i in 0 until artist?.size!!) {
                        mArtistName = if (artist.size == 1) {
                            artist[i].artistName!!
                        } else {
                            if (mArtistName != "") {
                                mArtistName + " , " + artist[i].artistName
                            } else {
                                artist[i].artistName!!
                            }
                        }
                    }
                    binding.artistDetailsTextView.text =
                        mArtistName

                }

                if (getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(getItem(adapterPosition).action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }

//                    //TODO : Check description contains url
//                    binding.typeTextTrackLinear.visibility = View.VISIBLE
//                    binding.playMageView.visibility = View.VISIBLE
//                    binding.trackImageView.visibility = View.VISIBLE

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                    if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                            if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."
                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }
                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.Name

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (getItem(adapterPosition).action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (getItem(adapterPosition).actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding?.followingTextView?.setOnClickListener {

                    if (getItem(adapterPosition).actor?.relation == "following") {
                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                getItem(adapterPosition).actor?.relation ?: "",
                                getItem(adapterPosition).actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    getItem(adapterPosition).actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        getItem(adapterPosition)?.action?.post?.type,
                        getItem(adapterPosition)?.action?.post?.id!!, mContext
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            if (getItem(adapterPosition).action?.post?.ogTags?.url!!.contains("http")) {

                                val url = getItem(adapterPosition).action?.post?.ogTags?.url
                                (mContext as HomeActivity).hyperLinkLinkClick(url)
                            }
                        } else {
                            if (retrieveLinks(getItem(adapterPosition).action?.post?.description!!) != "") {
                                try {
                                    goToLink(getItem(adapterPosition).action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            getItem(adapterPosition).action?.post?.hashTags!!, text,
                            getItem(adapterPosition).action?.post?.id!!,
                            getItem(adapterPosition).action?.post?.likesCount!!,
                            getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(getItem(adapterPosition).actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition,
                        getItem(adapterPosition)
                    )
                }
            }
        }
    }

    inner class ViewHolderItemMusic(val binding: LayoutItemMusicBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {
            binding.textViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (getItem(adapterPosition).action?.post?.ogTags != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = getItem(adapterPosition).action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(getItem(adapterPosition).action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }

                    //TODO : Check description contains url
                    binding.typeTextTrackLinear.visibility = View.VISIBLE
                    binding.playMageView.visibility = View.VISIBLE
                    binding.trackImageView.visibility = View.VISIBLE

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                    if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                            if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."
                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }
                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.Name

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (getItem(adapterPosition).action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (getItem(adapterPosition).actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding?.followingTextView?.setOnClickListener {

                    if (getItem(adapterPosition).actor?.relation == "following") {
                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                getItem(adapterPosition).actor?.relation ?: "",
                                getItem(adapterPosition).actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    getItem(adapterPosition).actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        getItem(adapterPosition)?.action?.post?.type,
                        getItem(adapterPosition)?.action?.post?.id!!, mContext
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            if (getItem(adapterPosition).action?.post?.ogTags?.url!!.contains("http")) {

                                val url = getItem(adapterPosition).action?.post?.ogTags?.url
                                (mContext as HomeActivity).hyperLinkLinkClick(url)
                            }
                        } else {
                            if (retrieveLinks(getItem(adapterPosition).action?.post?.description!!) != "") {
                                try {
                                    goToLink(getItem(adapterPosition).action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            getItem(adapterPosition).action?.post?.hashTags!!, text,
                            getItem(adapterPosition).action?.post?.id!!,
                            getItem(adapterPosition).action?.post?.likesCount!!,
                            getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(getItem(adapterPosition).actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition,
                        getItem(adapterPosition)
                    )
                }
            }
        }
    }

    inner class ViewHolderItemPlayList(val binding: LayoutItemPlaylistBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {
            binding.textViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (getItem(adapterPosition).action?.post?.ogTags != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = getItem(adapterPosition).action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(getItem(adapterPosition).action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }
                    binding.playMageView.setImageResource(R.drawable.ic_play)
                    binding.trackNameTextView.textSize = 20F
                    binding.authorTextView.textSize = 13F
                    binding.playListImageView.visibility = View.VISIBLE

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                    if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                            if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."
                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }
                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.Name

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (getItem(adapterPosition).action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (getItem(adapterPosition).actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding?.followingTextView?.setOnClickListener {

                    if (getItem(adapterPosition).actor?.relation == "following") {
                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                getItem(adapterPosition).actor?.relation ?: "",
                                getItem(adapterPosition).actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    getItem(adapterPosition).actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        getItem(adapterPosition)?.action?.post?.type,
                        getItem(adapterPosition)?.action?.post?.id!!, mContext
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            if (getItem(adapterPosition).action?.post?.ogTags?.url!!.contains("http")) {

                                val url = getItem(adapterPosition).action?.post?.ogTags?.url
                                (mContext as HomeActivity).hyperLinkLinkClick(url)
                            }
                        } else {
                            if (retrieveLinks(getItem(adapterPosition).action?.post?.description!!) != "") {
                                try {
                                    goToLink(getItem(adapterPosition).action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            getItem(adapterPosition).action?.post?.hashTags!!, text,
                            getItem(adapterPosition).action?.post?.id!!,
                            getItem(adapterPosition).action?.post?.likesCount!!,
                            getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(getItem(adapterPosition).actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition,
                        getItem(adapterPosition)
                    )
                }
            }
        }
    }

    inner class ViewHolderItemArtist(val binding: LayoutItemArtistBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {
            binding.textViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (getItem(adapterPosition).action?.post?.ogTags != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = getItem(adapterPosition).action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(getItem(adapterPosition).action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }


                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                    if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                            if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1?.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."
                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }
                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.Name

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (getItem(adapterPosition).action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (getItem(adapterPosition).actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding?.followingTextView?.setOnClickListener {

                    if (getItem(adapterPosition).actor?.relation == "following") {
                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                getItem(adapterPosition).actor?.relation ?: "",
                                getItem(adapterPosition).actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    getItem(adapterPosition).actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }
                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        getItem(adapterPosition)?.action?.post?.type,
                        getItem(adapterPosition)?.action?.post?.id!!, mContext
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            if (getItem(adapterPosition).action?.post?.ogTags?.url!!.contains("http")) {

                                val url = getItem(adapterPosition).action?.post?.ogTags?.url
                                (mContext as HomeActivity).hyperLinkLinkClick(url)
                            }
                        } else {
                            if (retrieveLinks(getItem(adapterPosition).action?.post?.description!!) != "") {
                                try {
                                    goToLink(getItem(adapterPosition).action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            getItem(adapterPosition).action?.post?.hashTags!!, text,
                            getItem(adapterPosition).action?.post?.id!!,
                            getItem(adapterPosition).action?.post?.likesCount!!,
                            getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(getItem(adapterPosition).actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition,
                        getItem(adapterPosition)
                    )
                }
            }
        }
    }

    inner class ViewHolderItemText(val binding: LayoutItemTextBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {
            binding.textViewProfileModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (getItem(adapterPosition).action?.post?.ogTags != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != null &&
                    getItem(adapterPosition).action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = getItem(adapterPosition).action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (getItem(adapterPosition).action != null &&
                    getItem(adapterPosition).action?.post != null &&
                    getItem(adapterPosition).action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(getItem(adapterPosition).action?.post?.description!!)
                    if (mDescription != "") {

                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            binding.descriptionTextView.text = removeUrl(mDescription)
                        } else {
                            binding.descriptionTextView.text =
                                getItem(adapterPosition).action?.post?.description!!
                        }
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            binding.descriptionTextView.visibility = View.GONE
                        } else {
                            binding.descriptionTextView.text =
                                getItem(adapterPosition).action?.post?.description!!
                            binding.descriptionTextView.visibility = View.VISIBLE
                        }
                    }

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (getItem(adapterPosition).action?.post?.likedConnections != null) {
                    if (getItem(adapterPosition).action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in getItem(adapterPosition).action?.post?.likedConnections?.indices!!) {

                            if (getItem(adapterPosition).action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."
                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }
                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.Name

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                getItem(adapterPosition).action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (getItem(adapterPosition).action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (getItem(adapterPosition).action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (getItem(adapterPosition).action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        getItem(adapterPosition).action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, getItem(adapterPosition).action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (getItem(adapterPosition).actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding.followingTextView.setOnClickListener {

                    if (getItem(adapterPosition).actor?.relation == "following") {
                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                getItem(adapterPosition).actor?.relation ?: "",
                                getItem(adapterPosition).actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    getItem(adapterPosition).actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            getItem(adapterPosition).actor?.relation ?: "",
                            getItem(adapterPosition).actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                getItem(adapterPosition).actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        /*feedItemClick.onFeedItemClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "text"
                        )*/
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (getItem(adapterPosition).action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        /* feedItemClick.onFeedItemClick(
                             getItem(adapterPosition).action?.post?.id!!,
                             "text"
                         )*/

                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )
                    }
                }

                binding.tvViewAllComments.setOnClickListener {
                    if (
                        getItem(adapterPosition).action != null &&
                        getItem(adapterPosition).action?.post != null &&
                        getItem(adapterPosition).action?.post?.id != null &&
                        getItem(adapterPosition).action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            getItem(adapterPosition).action?.post?.id!!, adapterPosition,
                            getItem(adapterPosition).action?.post?.own!!
                        )

                        /*feedItemClick.onFeedItemClick(
                            getItem(adapterPosition).action?.post?.id!!,
                            "text"
                        )*/

                    }
                }

                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        getItem(adapterPosition).action?.post?.id!!,
                        getItem(adapterPosition)?.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        getItem(adapterPosition)?.action?.post?.type,
                        getItem(adapterPosition)?.action?.post?.id!!, mContext
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (getItem(adapterPosition).action != null
                            && getItem(adapterPosition).action?.post != null
                            && getItem(adapterPosition).action?.post?.ogTags != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != null
                            && getItem(adapterPosition).action?.post?.ogTags?.url != ""
                        ) {
                            /*feedItemClick.onFeedItemClick(
                                getItem(adapterPosition).action?.post?.id!!,
                                "text"
                            )

                        }*/
                            if (getItem(adapterPosition).action?.post?.ogTags?.url!!.contains("http")) {

                                val url = getItem(adapterPosition).action?.post?.ogTags?.url
                                (mContext as HomeActivity).hyperLinkLinkClick(url)
                            }
                        } else {
                            if (retrieveLinks(getItem(adapterPosition).action?.post?.description!!) != "") {
                                try {
                                    goToLink(getItem(adapterPosition).action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            getItem(adapterPosition).action?.post?.hashTags!!, text,
                            getItem(adapterPosition).action?.post?.id!!,
                            getItem(adapterPosition).action?.post?.likesCount!!,
                            getItem(adapterPosition).action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(getItem(adapterPosition).action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(getItem(adapterPosition).actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition,
                        getItem(adapterPosition)
                    )
                }
            }
        }
    }

    private fun feedOptions(
        feedPostOptionImageView: ImageView, adapterPosition: Int,
        item: PersonalFeedResponseModelPayloadFeed,
    ) {

        val viewGroup: ViewGroup? = null
        val view: View =
            LayoutInflater.from(mContext)
                .inflate(R.layout.options_menu_feed_option, viewGroup)
        val mQQPop =
            PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        mQQPop.animationStyle = R.style.RightTopPopAnim
        mQQPop.isFocusable = true
        mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mQQPop.isOutsideTouchable = true
        mQQPop.showAsDropDown(feedPostOptionImageView, -180, -20)
        if (item.action?.post?.own == true) {
            view.edit_post.visibility = View.VISIBLE
            view.delete_post.visibility = View.VISIBLE
            //view.share_post.visibility = View.VISIBLE
        } else {
            view.edit_post.visibility = View.GONE
            view.delete_post.visibility = View.GONE
            //view.share_post.visibility = View.VISIBLE
            view.report_post.visibility = View.VISIBLE
        }
        view.report_post.setOnClickListener {
            mQQPop.dismiss()
            reportActivity(item.action?.post?.id!!)
        }
        view.share_post.setOnClickListener {
            ShareAppUtils.sharePost(item.action?.post?.type, item.action?.post?.id!!, mContext)
            mQQPop.dismiss()
        }
        view.edit_post.setOnClickListener {
            editPost(adapterPosition, item.action?.post?.id!!, true)
            mQQPop.dismiss()
        }
        view.delete_post.setOnClickListener {
            deletePost(adapterPosition, mQQPop, item.action?.post?.id!!)
        }
    }

    private fun hashTagClick(
        hashTags: List<HashTag>, text: CharSequence, id: String,
        likesCount: String, commentsCount: String, adapterPosition: Int,
    ) {
        for (i in hashTags.indices) {
            if (hashTags[i].name!!.equals(text.toString(), ignoreCase = true)) {
                if (hashTags[i]._id != null) {
                    if (!isCalledHashTgaDetailsPage) {
                        isCalledHashTgaDetailsPage = true
                        hashTagClickListener.onOptionItemClickHashTag(
                            hashTags[i]._id!!, adapterPosition.toString(), "hashTagFeed",
                            id, likesCount, commentsCount
                        )
                    }
                    Handler().postDelayed({
                        isCalledHashTgaDetailsPage = false
                    }, 1000)
                }
            }

        }

    }

    private fun reportActivity(id: String) {
        val intent = Intent(mContext, HelpFeedBackReportProblemActivity::class.java)
        intent.putExtra("mTitle", mContext.getString(R.string.report))
        intent.putExtra("mPostId", id)
        mContext.startActivity(intent)

    }

    private fun editPost(adapterPosition: Int, id: String, isFromWritePost: Boolean) {
        if (isFromWritePost) {
            MainFeedFragment.idPostPosition = adapterPosition
            SharedPrefsUtils.setStringPreference(mContext, "POST_FROM", "MAIN_FEED")
            val intent = Intent(mContext, WritePostActivity::class.java)
            intent.putExtra("mPostId", id)
            intent.putExtra("mIsEdit", true)
            mContext.startActivity(intent)
        } else {
            MainFeedFragment.idPostPosition = adapterPosition
            SharedPrefsUtils.setStringPreference(mContext, "POST_FROM", "MAIN_FEED")
            val intent = Intent(mContext, UploadPhotoActivityMentionHashTag::class.java)
            intent.putExtra("mPostId", getItem(adapterPosition).action?.post?.id!!)
            intent.putExtra("mIsEdit", true)
            mContext.startActivity(intent)
        }
    }

    private fun goToLink(description: String) {
        val url =
            retrieveLinks(description)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        mContext.startActivity(intent)

    }

    inner class ViewHolderItemFeedTrack(val binding: LayoutItemTrackBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(itemText: Any?) {

//            if (trackRecommendation.isNotEmpty()) {
//                binding.feedTrackRecyclerView.visibility = View.VISIBLE
//                binding.feedNoTrackLayout.visibility = View.GONE
//                binding.feedTrackRecyclerView.adapter =
//                    TrendingTrackAdapterFeed(
//                        trackRecommendation, mContext
//                    )
//            } else {
//                binding.feedTrackRecyclerView.visibility = View.GONE
//                binding.feedNoTrackLayout.visibility = View.VISIBLE
//            }

        }
    }

    inner class ViewHolderItemFeedMembers(val binding: LayoutItemMobbersBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            if (mSuggestedMembers!!.isNotEmpty()) {
                binding.feedMobbersRecyclerView.visibility = View.VISIBLE
                binding.feedNoTrackLayout.visibility = View.GONE
                binding.feedMobbersRecyclerView.adapter =
                    FeedPeopleAdapter(
                        mSuggestedMembers!!, searchPeopleAdapterFollowItemClick,
                        searchPeopleAdapterItemClick, mContext, glideRequestManager
                    )
            } else {
                binding.feedMobbersRecyclerView.visibility = View.GONE
                binding.feedNoTrackLayout.visibility = View.VISIBLE
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    inner class UploadProgressViewHolder(val binding: UploadProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            progressBar = binding.progressBar
            uploadTextView = binding.uploadTextView
        }
    }

    fun getProgressBar(): ProgressBar? {
        return progressBar
    }

    fun getUploadTextView(): TextView? {
        return uploadTextView
    }

    private fun notify(adapterPosition: Int) {
        getItem(adapterPosition).action?.post?.liked =
            getItem(adapterPosition).action?.post?.liked != true
        if (getItem(adapterPosition).action?.post?.liked == true) {
            val mCount =
                Integer.parseInt(getItem(adapterPosition).action?.post?.likesCount!!).plus(1)
            getItem(adapterPosition).action?.post?.likesCount = mCount.toString()
            getItem(adapterPosition).action?.post?.liked = true
        } else {
            val mCount =
                Integer.parseInt(getItem(adapterPosition).action?.post?.likesCount!!).minus(1)
            getItem(adapterPosition).action?.post?.likesCount = mCount.toString()
            getItem(adapterPosition).action?.post?.liked = false
        }

        notifyItemChanged(adapterPosition, itemCount)
    }

    private fun notifyFollowing(adapterPosition: Int, privateProfile: Boolean?) {
        if (!privateProfile!!) {
            if (getItem(adapterPosition).actor?.relation == "following") {
                getItem(adapterPosition).actor?.relation = "none"
            } else {
                getItem(adapterPosition).actor?.relation = "following"
            }
        } else {
            when (getItem(adapterPosition).actor?.relation) {
                "none" -> getItem(adapterPosition).actor?.relation =
                    "request pending"
                "following" -> getItem(adapterPosition).actor?.relation =
                    "none"
                "request pending" -> getItem(adapterPosition).actor?.relation =
                    "none"
            }
        }

        notifyItemChanged(adapterPosition, itemCount)
    }


    interface FeedItemLikeClick {
        fun onFeedItemLikeClick(mPostId: String, isLike: String)
    }

    interface FeedCommentClick {
        fun onCommentClick(mPostId: String, mPosition: Int, mIsMyPost: Boolean)
    }

    interface FeedLikeListClick {
        fun onFeedLikeListClick(mPostId: String, mLikeCount: String)
    }

    interface FeedItemClick {
        fun onFeedItemClick(mPostId: String, mType: String)
    }

    interface OptionItemClickListener {
        fun onOptionItemClickOption(mMethod: String, mId: String, mPos: Int)
    }

    interface HashTagClickListener {
        fun onOptionItemClickHashTag(
            mId: String,
            mPosition: String,
            mPath: String,
            mPostId: String,
            mLikeCount: String,
            mCommentCount: String,
        )

    }

    //
    private fun retrieveLinks(text: String): String {
        try {
            val links: MutableList<String>? = null

            val regex =
                "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"
            val p = Pattern.compile(regex)
            val m = p.matcher(text)
            while (m.find()) {
                var urlStr = m.group()

                if (urlStr.startsWith("(") && urlStr.endsWith(")")) {

                    val stringArray = urlStr.toCharArray()

                    val newArray = CharArray(stringArray.size - 2)
                    System.arraycopy(stringArray, 1, newArray, 0, stringArray.size - 2)
                    urlStr = String(newArray)
                    println("Finally Url =$newArray")

                }
                println("...Url...$urlStr")
                links?.add(urlStr)
            }
            return links!![0]
        } catch (e: Exception) {
            try {
                val links: MutableList<String> = ArrayList()
                val m = Patterns.WEB_URL.matcher(text)
                while (m.find()) {
                    val url = m.group()
                    links.add(url)
                }
                return links[0]
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return ""
    }

    fun updateCommentCount(mPostPosition: Int, mCommentCount: String?) {
        if (itemCount > 0 && getItem(mPostPosition)?.action != null &&
            getItem(mPostPosition).action?.post != null && getItem(mPostPosition).action?.post?.commentsCount != null
        ) {
            getItem(mPostPosition).action?.post?.commentsCount = mCommentCount
            notifyItemChanged(mPostPosition)
        }
    }

    fun addListMembers(suggestedMembers: MutableList<SearchProfileResponseModelPayloadProfile>) {
        mSuggestedMembers = suggestedMembers

    }

    private fun settextMarquee(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length >= 30) {
                value.substring(0, 30) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    /**
     * Screen Ratio
     */
    private fun getScreenResolution(context: Context): Int? {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val width: Int = metrics.widthPixels
        val height: Int = metrics.heightPixels
        return height
    }


    private fun screenType(): String? {
        val screenSize = getResources().configuration.screenLayout and
                Configuration.SCREENLAYOUT_SIZE_MASK

        val toastMsg: String
        toastMsg = when (screenSize) {
            Configuration.SCREENLAYOUT_SIZE_LARGE -> "Large_screen"
            Configuration.SCREENLAYOUT_SIZE_NORMAL -> "Normal_screen"
            Configuration.SCREENLAYOUT_SIZE_SMALL -> "Small_screen"
            else -> "Screen_1"
        }
        Log.d("ViewHolderItemImage", toastMsg)

        return toastMsg
    }

    private fun screenDen(context: Context):Int {
        val density = getResources().displayMetrics.densityDpi
        when (density) {
            DisplayMetrics.DENSITY_LOW -> Toast.makeText(context, "LDPI", Toast.LENGTH_SHORT).show()
            DisplayMetrics.DENSITY_MEDIUM -> Toast.makeText(context, "MDPI", Toast.LENGTH_SHORT)
                .show()
            DisplayMetrics.DENSITY_HIGH -> Toast.makeText(context, "HDPI", Toast.LENGTH_SHORT)
                .show()
            DisplayMetrics.DENSITY_XHIGH -> Toast.makeText(context, "XHDPI", Toast.LENGTH_SHORT)
                .show()
        }
        return density
    }
}