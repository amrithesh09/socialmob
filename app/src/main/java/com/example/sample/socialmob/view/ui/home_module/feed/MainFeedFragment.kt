package com.example.sample.socialmob.view.ui.home_module.feed

import android.content.Intent
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MainFeedFragmentBinding
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedAction
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPost
import com.example.sample.socialmob.model.profile.PostFeedData
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.utils.DbPlayClick
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.BottomSheetPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleAdapter
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.feed.CommentListActivity
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.WritePostActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.*
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.viewmodel.feed.FeedViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MainFeedFragment : Fragment(), MainFeedAdapter.FeedItemLikeClick,
    MainFeedAdapter.FeedCommentClick,
    MainFeedAdapter.FeedLikeListClick,
    MainFeedAdapter.FeedItemClick,
    MainFeedAdapter.OptionItemClickListener, DbPlayClick,
    BottomSheetPlayListAdapter.PlayListItemClickListener,
    MainFeedAdapter.HashTagClickListener,
    SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick,
    SearchPeopleAdapter.SearchPeopleAdapterItemClick,
    ProgressRequestBody.UploadCallbacks {

    @Inject
    lateinit var glideRequestManager: RequestManager

    companion object {
        var mAddFeeData: PersonalFeedResponseModelPayloadFeedActionPost? = null
        var EXPORT_STARTED: Boolean = false
        var isCompletedVideoEdit: Boolean = false
        var isUploadStarted: Boolean = false
        var isAddedFeed: Boolean = false
        var isAddedFeedMobbers: Boolean = false
        var isUpdatedFeed: Boolean = false

        var idPost: String = ""
        var idPostPosition: Int? = null
        var titlePost: String = ""
        var mAuthPost: String = ""
        var mAuthPostType: String = ""
        var mImageListPost: String = ""
        var mImageListPostList: ArrayList<GalleryData> = ArrayList()
        var mThumbListPost: ArrayList<File>? = ArrayList()
        var postDataPost = PostFeedData()
        var ratio = ""

        private var instance: MainFeedFragment? = null
        fun getInstance(): MainFeedFragment? {
            return instance
        }
    }




    private var RESPONSE_CODE = 1501
    private var ADD_FEED_RESPONSE_CODE = 1601
    private var mLastClickTime: Long = 0
    private var mAdCount: Int = 0
    private var isRefreshCalled = false
    private var isVisibleToUser = false
    private var isLoaderAdded: Boolean = false
    private var mFollowingCount: String = ""
    private var mPlayListId: String = ""

    private val feedViewModel: FeedViewModel by viewModels()
    private var binding: MainFeedFragmentBinding? = null
    private var feedList: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()
    private var mainFeedAdapter: MainFeedAdapter? = null
    private var mLayoutManager: WrapContentLinearLayoutManager? = null
    private var mStatus: ResultResponse.Status? = null

    /**
     * Receive upload details in onResume
     * Showing loader in feed page
     * Editor is running in background
     */
    override fun onResume() {
        super.onResume()
        try {
            if (EXPORT_STARTED && !isCompletedVideoEdit) {
                if (isVisibleToUser) {
                    uploadTopLoader()
                    binding?.swipeToRefresh?.isEnabled = false
                    binding?.multipleActions?.visibility = View.GONE
                }
                EXPORT_STARTED = false
            }
            if (isAddedFeed && titlePost != "" && isCompletedVideoEdit) {
                binding?.swipeToRefresh?.isEnabled = false
                if (InternetUtil.isInternetOn()) {
                    isRefreshCalled = false
                    binding?.multipleActions?.visibility = View.GONE
                    uploadPost()
                } else {
                    AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
                }
                isCompletedVideoEdit = false
                isAddedFeed = false
                mAddFeeData = null
                titlePost = ""
                isUploadStarted = false
            } else if (mAddFeeData != null && !isUpdatedFeed) {
                isRefreshCalled = false
                mAddFeeData = null
                isCompletedVideoEdit = false
                isAddedFeed = false
                mAddFeeData = null
                titlePost = ""

                if (idPost != "") {
                    showDialogDoYouWantSeePost(idPost, true, mAuthPostType)
                }
                isUploadStarted = false
            } else if (isUpdatedFeed) {

                /* TODO : Update Data in List */
                if (idPostPosition != null && mAddFeeData != null) {
                    feedList?.get(idPostPosition!!)?.action?.post = mAddFeeData
                    mainFeedAdapter?.notifyItemChanged(idPostPosition!!, mAddFeeData)
                }
                mAddFeeData = null
                isAddedFeed = false
                isCompletedVideoEdit = false
                mAddFeeData = null
                titlePost = ""
                isUpdatedFeed = false
                idPostPosition = null
                isUploadStarted = false
            }
            isLoaderAdded = false

        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "onResume_MainFeed" + e.toString())
        }
    }


    private fun uploadTopLoader() {
        if (feedList != null && feedList?.size!! > 0 && feedList?.get(0)?.isLoader != "upload") {
            val mListSize = feedList?.size
            val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
            val mList = PersonalFeedResponseModelPayloadFeed()
            mList.isLoader = "upload"
            loaderItem.add(mList)
            feedList?.addAll(0, loaderItem)
            if (mListSize == 0) {
                mainFeedAdapter?.submitList(feedList)
                mainFeedAdapter?.notifyDataSetChanged()

                binding?.feedRecyclerView?.visibility = View.VISIBLE
                binding?.listLinear?.visibility = View.VISIBLE
                binding?.progressLinear?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
                binding?.noDataLinear?.visibility = View.GONE
                binding?.backgroundDimmer?.visibility = View.GONE

            } else {
                mainFeedAdapter?.notifyDataSetChanged()
            }
        }

        if (mainFeedAdapter?.getUploadTextView() != null) {
            val uploadTextView = mainFeedAdapter?.getUploadTextView()
            uploadTextView?.text = "Uploading"
        }
        if (mainFeedAdapter?.getProgressBar() != null) {
            val mProgressBar = mainFeedAdapter?.getProgressBar()
            if (mProgressBar?.isIndeterminate == false) {
                mProgressBar.isIndeterminate = true
            }
        }
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            mLayoutManager?.scrollToPositionWithOffset(0, 0)
        }, 1000)
    }

    private fun uploadPost() {

        if (feedList != null && feedList?.size!! > 0 && feedList?.get(0)?.isLoader != "upload") {
            val mListSize = feedList?.size
            val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()
            val mList = PersonalFeedResponseModelPayloadFeed()
            mList.isLoader = "upload"
            loaderItem?.add(mList)
            feedList?.addAll(0, loaderItem!!)
            if (mListSize == 0) {
                mainFeedAdapter?.submitList(feedList)
                mainFeedAdapter?.notifyDataSetChanged()

                binding?.feedRecyclerView?.visibility = View.VISIBLE
                binding?.listLinear?.visibility = View.VISIBLE
                binding?.progressLinear?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
                binding?.noDataLinear?.visibility = View.GONE
                binding?.backgroundDimmer?.visibility = View.GONE

            } else {
                mainFeedAdapter?.notifyDataSetChanged()
            }
        }

        if (mAuthPostType == "video") {
            isUploadStarted = true

            if (mainFeedAdapter?.getUploadTextView() != null) {
                val uploadTextView = mainFeedAdapter?.getUploadTextView()
                if (uploadTextView?.text != "Compressing") {
                    uploadTextView?.text = "Compressing"
                }
            }
            feedViewModel.getPreSignedUrl(
                mAuthPost,
                mAuthPostType,
                mImageListPost,
                mImageListPostList,
                mThumbListPost,
                postDataPost,
                ratio,
                this@MainFeedFragment
            )

        } else {
            if (mainFeedAdapter?.getUploadTextView() != null) {
                val uploadTextView = mainFeedAdapter?.getUploadTextView()
                uploadTextView?.text = "Uploading"
            }
            if (mainFeedAdapter?.getProgressBar() != null) {
                val mProgressBar = mainFeedAdapter?.getProgressBar()
                if (mProgressBar?.isIndeterminate == false) {
                    mProgressBar.isIndeterminate = true
                    mainFeedAdapter?.notifyItemChanged(0)
                }
            }
            feedViewModel.getPreSignedUrl(
                mAuthPost,
                mAuthPostType,
                mImageListPost,
                mImageListPostList,
                mThumbListPost,
                postDataPost,
                ratio,
                this
            )

        }


        feedViewModel.isUploaded.nonNull().observe(viewLifecycleOwner, {
            if (!feedViewModel.isUploading) {
                if (it == true) {
                    if (idPost != "") {
                        showDialogDoYouWantSeePost(idPost, false, mAuthPostType)
                    }
                    titlePost = ""
                    mAuthPost = ""
                    mAuthPostType = ""
                    mImageListPost = ""
                    mImageListPostList = ArrayList()
                    mThumbListPost = ArrayList()
                    postDataPost = PostFeedData()
                    ratio = ""
                    binding?.swipeToRefresh?.isEnabled = true

                } else {
                    if (feedList != null && feedList?.size!! > 0) {
                        feedList?.removeAt(0)
                        mainFeedAdapter?.notifyDataSetChanged()
                        AppUtils.showCommonToast(
                            requireActivity(),
                            "Error occur please try again later"
                        )
                    }
                }
                binding?.swipeToRefresh?.isEnabled = true
                binding?.multipleActions?.visibility = View.VISIBLE
            }

            feedViewModel.isUploaded.value = null
        })

    }

    /**
     * Dialog after image/video upload
     */
    private fun showDialogDoYouWantSeePost(
        idPost: String,
        isFromWritePost: Boolean,
        mAuthPostType: String
    ) {
        if (!isFromWritePost) {
            if (feedList != null && feedList?.size!! > 0) {
                feedList?.removeAt(0)
                mainFeedAdapter?.notifyItemRemoved(0)
                EXPORT_STARTED = false
                if (feedList != null && feedList?.size!! > 0) {
                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        mLayoutManager?.scrollToPositionWithOffset(0, 0)
                    }, 1000)
                }
            }
        }


        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = layoutInflater
        val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val okButton: Button = dialogView.findViewById(R.id.ok_button)
        val dialogDescription: TextView = dialogView.findViewById(R.id.dialog_description)
        dialogDescription.text = getString(R.string.want_to_see_post)

        cancelButton.text = getString(R.string.no)
        okButton.text = getString(R.string.yes)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        okButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            when {
                isFromWritePost -> {
                    val intent = Intent(activity, FeedTextDetailsActivity::class.java)
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)

                }
                mAuthPostType == "video" -> {
                    val intent = Intent(activity, FeedVideoDetailsActivity::class.java)
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)
                }
                else -> {
                    val intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    //intent.putExtra("mPostViewType", "")
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)
                }
            }
        }
        cancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESPONSE_CODE) {
            if (data != null) {
                val mCommentCount = data.getStringExtra("COMMENT_COUNT")
                val mPostPosition = data.getIntExtra("POST_POSITION", 0)
                if (mCommentCount != null && mCommentCount != "") {
                    mainFeedAdapter?.updateCommentCount(mPostPosition, mCommentCount)
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        //add this code to pause videos (when app is minimised or paused)
        EventBus.getDefault().unregister(this)
    }

    /**
     * Upload complete event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UploadStatus) {
        if (!event.isAdded) {
            if (isVisibleToUser) {
                uploadTopLoader()
            }
        }
        isCompletedVideoEdit = event.isCompleted
        if (isCompletedVideoEdit && !isUploadStarted) {
            onResume()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        this.isVisibleToUser = isVisibleToUser
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callApiCommon()
        observeData()

        binding?.goToOfflineMode?.setOnClickListener {
            if (activity != null) {
                (activity as HomeActivity).goToOffline()
            }
        }

        binding?.internetRefreshTextView?.setOnClickListener {
            if (mainFeedAdapter != null) {
                feedList?.clear()
                mainFeedAdapter?.notifyDataSetChanged()
            }
            if (MainFeedAdapter.helper != null) {
                MainFeedAdapter.helper?.pause()
            }
            feedList?.clear()
            isAddedFeedMobbers = false
            mAdCount = 0
            callApiCommon()
            observeData()
        }

        mLayoutManager = WrapContentLinearLayoutManager(
            requireActivity(), WrapContentLinearLayoutManager.VERTICAL, false
        )

        binding?.feedRecyclerView?.layoutManager = mLayoutManager
        binding?.feedRecyclerView?.addOnScrollListener(CustomScrollListener())

        // TODO : Swipe To Refresh
        binding?.swipeToRefresh?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefresh?.setOnRefreshListener {
            if (MainFeedAdapter.helper != null) {
                MainFeedAdapter.helper?.pause()
            }
            mAdCount = 0
            feedList?.clear()
            mainFeedAdapter?.notifyDataSetChanged()
            isAddedFeedMobbers = false
            callApiCommon()
            if (!InternetUtil.isInternetOn()) {
                if (binding?.swipeToRefresh?.isRefreshing!!) {
                    binding?.swipeToRefresh?.isRefreshing = false
                }
            }
        }
        binding?.backgroundDimmer?.setOnClickListener {
            collapse()
        }
        /**
         * Handle background and video play/pause
         */
        binding?.multipleActions?.setOnFloatingActionsMenuUpdateListener(object :
            FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
            override fun onMenuExpanded() {

                binding?.backgroundDimmer?.bringToFront()
                binding?.multipleActions?.bringToFront()
                binding?.backgroundDimmer?.visibility = View.VISIBLE
                binding?.feedRecyclerView?.visibility = View.INVISIBLE
                EventBus.getDefault().post(
                    HideFeedTitileEvent(
                        true
                    )
                )
                if (MainFeedAdapter.helper != null) {
                    MainFeedAdapter.helper?.pause()
                }
            }

            override fun onMenuCollapsed() {

                binding?.backgroundDimmer?.visibility = View.GONE
                binding?.feedRecyclerView?.visibility = View.VISIBLE
                EventBus.getDefault().post(
                    HideFeedTitileEvent(
                        false
                    )
                )

            }
        })

        binding?.actionWriteAPost?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                writePost()
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.actionUploadAVideo?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                uploadVideo()
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.actionUploadAPhoto?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                uploadPhoto()
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }

        // TODO : For Suggested mobbers
        feedViewModel.suggestedMobbers?.nonNull()?.observe(viewLifecycleOwner, { list ->
            if (list != null && list.isNotEmpty()) {
                val mViewAll = SearchProfileResponseModelPayloadProfile(
                    0, "", "", "", "",
                    "", "", "", "", "",
                    "", false, "", ""
                )
                list.add(mViewAll)
                if (mainFeedAdapter != null) {
                    mainFeedAdapter?.addListMembers(list)
                }
            }
        })

        feedViewModel.profile.nonNull().observe(viewLifecycleOwner, {
            mFollowingCount = it?.followingCount!!
        })

        setAdapter()
        //TODO : Preload Ads

    }

    private fun setAdapter() {
        // TODO : Set Adapter
        binding?.feedRecyclerView?.hasFixedSize()
        mainFeedAdapter =
            MainFeedAdapter(
                requireActivity(), feedItemClick = this,
                feedItemLikeClick = this,
                feedCommentClick = this,
                feedLikeListClick = this,
                optionItemClickListener = this,
                hashTagClickListener = this,
                searchPeopleAdapterFollowItemClick = this,
                searchPeopleAdapterItemClick = this, glideRequestManager
            )
        binding?.feedRecyclerView?.adapter = mainFeedAdapter

    }

    // TODO : Pagination
    private fun loadMoreItems(size: Int) {
        callApi(size)

        val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
        val mList = PersonalFeedResponseModelPayloadFeed()
        mList.isLoader = "loader"
        loaderItem.add(mList)
        feedList?.addAll(loaderItem)
        binding?.feedRecyclerView?.post {
            mainFeedAdapter?.notifyItemInserted(feedList?.size?.minus(1)!!)
        }
        isLoaderAdded = true

    }

    // TODO : TODO : Upload Video
    private fun uploadVideo() {
        collapse()
        SharedPrefsUtils.setStringPreference(requireActivity(), "POST_FROM", "MAIN_FEED")
        val intent = Intent(activity, PickerActivity::class.java)
        intent.putExtra("title", "UPLOAD VIDEO")
        intent.putExtra("IMAGES_LIMIT", 5)
        intent.putExtra("VIDEOS_LIMIT", 1)
        startActivity(intent)
    }

    // TODO : Upload Photo
    private fun uploadPhoto() {
        collapse()
        SharedPrefsUtils.setStringPreference(requireActivity(), "POST_FROM", "MAIN_FEED")
        val intent = Intent(activity, PickerActivity::class.java)
        intent.putExtra("title", "UPLOAD PHOTO")
        intent.putExtra("IMAGES_LIMIT", 5)
        intent.putExtra("VIDEOS_LIMIT", 1)
        startActivity(intent)
    }

    //TODO : Write Post
    private fun writePost() {
        collapse()
        SharedPrefsUtils.setStringPreference(requireActivity(), "POST_FROM", "MAIN_FEED")
        val intent = Intent(activity, WritePostActivity::class.java)
        startActivityForResult(intent, ADD_FEED_RESPONSE_CODE)
    }

    private fun collapse() {

        Handler().postDelayed({
            //run this method only when run is true
            binding?.multipleActions?.collapse()
        }, 100)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_feed_fragment, container, false)
        instance = this

        return binding?.root
    }

    private fun observeData() {

        if (viewLifecycleOwner != null) {
            feedViewModel.feedsList?.nonNull()
                ?.observe(viewLifecycleOwner, { resultResponse ->

                    if (resultResponse != null) {
                        mStatus = resultResponse.status

                        when (resultResponse.status) {
                            ResultResponse.Status.SUCCESS -> {
                                feedList = resultResponse.data!!.toMutableList()

                                binding?.gifProgress?.pause()
                                binding?.multipleActions?.visibility = View.VISIBLE

                                val newAdList: MutableList<PersonalFeedResponseModelPayloadFeed>? =
                                    ArrayList()
                                for (i in feedList?.indices!!) {

                                    if (!isAddedFeedMobbers) {
                                        if (i % 3 == 0) {
                                            if (i != 0) {
                                                val mList = PersonalFeedResponseModelPayloadFeed()
                                                val action =
                                                    PersonalFeedResponseModelPayloadFeedAction()
                                                val post =
                                                    PersonalFeedResponseModelPayloadFeedActionPost()
                                                mList.action = action
                                                mList.action?.post = post
                                                mList.action?.post?.type = "suggested_mobbers"
                                                newAdList?.add(mList)
                                                isAddedFeedMobbers = true
                                                mAdCount = mAdCount.plus(1)
                                            }
                                        }
                                    }
                                    if (i % 5 == 0) {
                                        if (i != 0) {
                                            val mList = PersonalFeedResponseModelPayloadFeed()
                                            mList.isAd = "ad"
                                            newAdList?.add(mList)
                                            // TODO: Google Ad count
                                            mAdCount = mAdCount.plus(1)

                                        }
                                        newAdList?.add(feedList?.get(i)!!)
                                    } else {
                                        newAdList?.add(feedList?.get(i)!!)
                                    }
                                }

                                feedList?.clear()
                                feedList?.addAll(newAdList!!)
                                mainFeedAdapter?.submitList(feedList)
                                mainFeedAdapter?.notifyDataSetChanged()
                                binding?.feedRecyclerView?.post {
                                    binding?.feedRecyclerView?.visibility = View.VISIBLE
                                    binding?.listLinear?.visibility = View.VISIBLE
                                    binding?.progressLinear?.visibility = View.GONE
                                    binding?.noInternetLinear?.visibility = View.GONE
                                    binding?.noDataLinear?.visibility = View.GONE
                                    binding?.backgroundDimmer?.visibility = View.GONE
                                }

                                mStatus = ResultResponse.Status.LOADING_COMPLETED

                            }
                            ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                                if (isLoaderAdded) {
                                    removeLoaderFormList()
                                }
                                mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            }
                            ResultResponse.Status.PAGINATED_LIST -> {

                                if (isLoaderAdded) {
                                    removeLoaderFormList()
                                }

                                val newAdList: MutableList<PersonalFeedResponseModelPayloadFeed> =
                                    ArrayList()
                                for (i in resultResponse.data?.indices!!) {

                                    if (!isAddedFeedMobbers) {
                                        if (i % 3 == 0) {
                                            if (i != 0) {
                                                val mList = PersonalFeedResponseModelPayloadFeed()
                                                val action =
                                                    PersonalFeedResponseModelPayloadFeedAction()
                                                val post =
                                                    PersonalFeedResponseModelPayloadFeedActionPost()
                                                mList.action = action
                                                mList.action?.post = post
                                                mList.action?.post?.type = "suggested_mobbers"
                                                newAdList.add(mList)
                                                isAddedFeedMobbers = true
                                                mAdCount = mAdCount.plus(1)
                                            }
                                        }
                                    }
                                    //TODO : Check if ad preloaded
                                    if ((activity as HomeActivity).getLoadedAds()?.size!! > 0) {
                                        if (i % 5 == 0) {
                                            if (i != 0) {
                                                val mList = PersonalFeedResponseModelPayloadFeed()
                                                mList.isAd = "ad"
                                                newAdList.add(mList)
                                                // TODO: Google Ad count
                                                mAdCount = mAdCount.plus(1)

                                            }
                                            newAdList.add(resultResponse.data[i])
                                        } else {
                                            newAdList.add(resultResponse.data[i])
                                        }
                                    } else {
                                        newAdList.add(resultResponse.data[i])
                                    }
                                }


                                val size = feedList?.size
                                feedList?.addAll(newAdList)
                                mainFeedAdapter?.notifyItemRangeInserted(size!!, feedList?.size!!)

                                mStatus = ResultResponse.Status.LOADING_COMPLETED




                            }
                            ResultResponse.Status.ERROR -> {

                                if (feedList?.size == 0) {
                                    binding?.progressLinear?.visibility = View.GONE
                                    binding?.noInternetLinear?.visibility = View.VISIBLE
                                    binding?.listLinear?.visibility = View.GONE
                                    binding?.noDataLinear?.visibility = View.GONE
                                    glideRequestManager.load(R.drawable.server_error)
                                        .apply(RequestOptions().fitCenter())
                                        .into(binding?.noInternetImageView!!)
                                    binding?.noInternetTextView?.text =
                                        getString(R.string.server_error)
                                    binding?.noInternetSecTextView?.text =
                                        getString(R.string.server_error_content)
                                } else {
                                    if (isLoaderAdded) {
                                        removeLoaderFormList()
                                    }
                                }

                            }
                            ResultResponse.Status.NO_DATA -> {
                                binding?.multipleActions?.visibility = View.VISIBLE
                                glideRequestManager.load(R.drawable.ic_no_search_data).apply(
                                    RequestOptions().fitCenter()
                                )
                                    .into(binding?.noDataImageView!!)
                                binding?.progressLinear?.visibility = View.GONE
                                binding?.noInternetLinear?.visibility = View.GONE
                                binding?.listLinear?.visibility = View.GONE
                                binding?.noDataLinear?.visibility = View.VISIBLE
                            }
                            ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                                mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            }
                            ResultResponse.Status.LOADING -> {

                                binding?.progressLinear?.visibility = View.VISIBLE
                                binding?.noInternetLinear?.visibility = View.GONE
                                binding?.listLinear?.visibility = View.GONE
                                binding?.noDataLinear?.visibility = View.GONE


                            }
                            ResultResponse.Status.NO_INTERNET
                            -> {
                                glideRequestManager.load(R.drawable.ic_app_offline)
                                    .apply(RequestOptions().fitCenter())
                                    .into(binding?.noInternetImageView!!)
                                binding?.progressLinear?.visibility = View.GONE
                                binding?.noInternetLinear?.visibility = View.VISIBLE
                                binding?.listLinear?.visibility = View.GONE
                                binding?.noDataLinear?.visibility = View.GONE
                                val shake: Animation =
                                    AnimationUtils.loadAnimation(activity, R.anim.shake)
                                binding?.noInternetLinear?.startAnimation(shake) // starts animation
                            }
                            else -> {

                            }
                        }
                    }
                })
        }
    }

    /**
     * Remove loader from list - pagination
     */
    private fun removeLoaderFormList() {
        if (feedList != null && feedList?.size!! > 0 &&
            feedList?.get(feedList?.size!! - 1) != null &&
            feedList?.get(feedList?.size!! - 1)?.isLoader == "loader"
        ) {
            feedList?.removeAt(feedList?.size!! - 1)
            mainFeedAdapter?.notifyItemRemoved(feedList?.size!!)
            isLoaderAdded = false
        }
    }

    private fun callApiCommon() {
        if (InternetUtil.isInternetOn()) {
            // TODO : To Load gif image
            binding?.gifProgress?.visibility = View.VISIBLE
            binding?.gifProgress?.play()
            binding?.gifProgress?.gifResource = R.drawable.loadergif
            callApi(feedList?.size!!)
            if (binding?.swipeToRefresh?.isRefreshing!!) {
                binding?.swipeToRefresh?.isRefreshing = false
            }
        } else {
            feedViewModel.feedsList?.postValue(
                ResultResponse(
                    ResultResponse.Status.NO_INTERNET,
                    ArrayList(),
                    getString(R.string.no_internet)
                )
            )
        }
    }

    /**
     * Get timeline feed API
     */
    private fun callApi(size: Int) {
        if (size == 0) {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mTimeLineFeed + RemoteConstant.mSlash +
                        size + RemoteConstant.mSlash + RemoteConstant.mFive,
                RemoteConstant.mGetMethod
            )
            val mAuth = RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            feedViewModel.getTimeLineFeed(mAuth, "0", RemoteConstant.mFive)
        } else {
            if (feedList?.get(feedList?.size?.minus(1)!!) != null &&
                feedList?.get(feedList?.size?.minus(1)!!)?.action != null &&
                feedList?.get(feedList?.size?.minus(1)!!)?.action?.actionId != null
            ) {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mTimeLineFeed + RemoteConstant.mSlash +
                            feedList?.get(feedList?.size?.minus(1)!!)?.action?.actionId!! + RemoteConstant.mSlash + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(
                            activity,
                            RemoteConstant.mAppId,
                            ""
                        )!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                feedViewModel.getTimeLineFeed(
                    mAuth,
                    feedList?.get(feedList?.size?.minus(1)!!)?.action?.actionId!!,
                    RemoteConstant.mCount
                )
            }
        }
    }

    private fun likePost(postId: String) {
        val mAuth = RemoteConstant.getAuthLikePost(activity, postId)
        feedViewModel.likePost(mAuth, postId)
    }


    private fun disLikePost(postId: String) {
        val mAuth = RemoteConstant.getAuthDislikePost(activity, postId)
        feedViewModel.disLikePost(mAuth, postId)
    }

    override fun onFeedItemLikeClick(mPostId: String, isLike: String) {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        } else {
            if (isLike == "true") {
                likePost(mPostId)
            } else {
                disLikePost(mPostId)
            }
        }
    }

    override fun onCommentClick(mPostId: String, mPosition: Int, mIsMyPost: Boolean) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            val intent = Intent(activity, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mIsMyPost", mIsMyPost)
            intent.putExtra("mPosition", mPosition)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onFeedLikeListClick(mPostId: String, mLikeCount: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(activity, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    /**
     * Navigate to detail page
     */
    override fun onFeedItemClick(mPostId: String, mType: String) {
        if (mType == "image") {
            val intent = Intent(activity, FeedImageDetailsActivity::class.java)
            //intent.putExtra("mPostViewType", "")
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        if (mType == "video") {
            val intent = Intent(activity, FeedVideoDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        if (mType == "text") {
            val intent = Intent(activity, FeedTextDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
    }

    override fun onOptionItemClickHashTag(
        mId: String, mPosition: String, mPath: String, mPostId: String, mLikeCount: String,
        mCommentCount: String
    ) {
        val detailIntent = Intent(activity, PhotoGridDetailsActivity::class.java)
        detailIntent.putExtra("mId", mId)
        detailIntent.putExtra("mPosition", mPosition)
        detailIntent.putExtra("mPath", mPath)
        detailIntent.putExtra("mPostId", mPostId)
        detailIntent.putExtra("mLikeCount", mLikeCount)
        detailIntent.putExtra("mCommentCount", mCommentCount)
        startActivityForResult(detailIntent, RESPONSE_CODE)
    }


    override fun onOptionItemClickOption(mMethod: String, mId: String, mPos: Int) {

        if (mMethod == "delete") {
            Handler().postDelayed({
                feedList?.removeAt(mPos)
                mainFeedAdapter?.notifyItemRemoved(mPos)
                mainFeedAdapter?.notifyItemRangeChanged(mPos, feedList?.size?.minus(1)!!)

                if (feedList?.size == 0) {
                    glideRequestManager.load(R.drawable.ic_no_search_data)
                        .apply(RequestOptions().fitCenter())
                        .into(binding?.noDataImageView!!)
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.listLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.VISIBLE
                }

            }, 100)
            feedViewModel.deletePost(mId, requireActivity())
        }
    }

    override fun onPlayClick(mPosition: String) {
        val detailIntent = Intent(activity, NowPlayingActivity::class.java)
        detailIntent.putExtra(RemoteConstant.mExtraIndex, Integer.parseInt(mPosition))
        detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistFeedRecommended")
        startActivityForResult(detailIntent, 1051)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: FeedClickes) {
        if (event.mMethod == "playClick") {
            val detailIntent = Intent(activity, NowPlayingActivity::class.java)
            detailIntent.putExtra(RemoteConstant.mExtraIndex, Integer.parseInt(event.mID))
            detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistFeedRecommended")
            startActivityForResult(detailIntent, 1051)
        }

    }

    override fun onPlayListItemClick(mId: String) {
        mPlayListId = mId
    }

    override fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String) {
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }
    }

    override fun onSearchPeopleAdapterItemClick(mProfileId: String, mIsOwn: String) {
        if (mIsOwn == "false") {
            val intent = Intent(activity, UserProfileActivity::class.java)
            intent.putExtra("mProfileId", mProfileId)
            intent.putExtra("mIsOwn", mIsOwn)
            startActivityForResult(intent, 1501)
        } else {
            val intent = Intent(activity, MyProfileActivity::class.java)
            startActivity(intent)
        }
    }

    /**
     * Cancel follow request - API
     */
    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        feedViewModel.cancelRequest(mAuth, mId)

    }

    /**
     * Unfollow user API
     */
    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        feedViewModel.unFollowProfile(mAuth, mId, mFollowingCount)

    }

    /**
     * Follow user API
     */
    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        feedViewModel.followProfile(mAuth, mId, mFollowingCount)

    }

    /**
     * Pagination API = Feed
     */
    inner class CustomScrollListener : RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                    visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                    && feedList?.size!! >= 5
                ) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        val mNewListSize = feedList?.size?.minus(mAdCount)
                        if (InternetUtil.isInternetOn()) {
                            if (!isLoaderAdded) {
                                loadMoreItems(mNewListSize!!)
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Pause video when track played
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: StopVideoPlayBack) {
        if (MainFeedAdapter.helper != null) {
            MainFeedAdapter.helper?.pause()
        }
    }

    /**
     * Double tap event feed item - Scroll to top
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ScrollToTopEvent) {
        if (binding != null) {
            binding?.feedRecyclerView?.smoothScrollToPosition(0)
        }
        if (feedList?.size!! > 25) {
            if (mLayoutManager != null) {
                mLayoutManager?.scrollToPositionWithOffset(0, 0)
            }
        }
    }

    override fun onProgressUpdate(percentage: Int, timestamp: String?) {
        if (mainFeedAdapter?.getUploadTextView() != null) {
            val uploadTextView = mainFeedAdapter?.getUploadTextView()
            if (uploadTextView?.text != "Uploading") {
                uploadTextView?.text = "Uploading"
            }
        }
        if (mainFeedAdapter?.getProgressBar() != null) {
            val mProgressBar = mainFeedAdapter?.getProgressBar()
            mProgressBar?.progress = percentage

            if (mProgressBar?.isIndeterminate == true) {
                mProgressBar.isIndeterminate = false
                mainFeedAdapter?.notifyItemChanged(0)
            }
        }
    }

    override fun onError() {
        feedList?.removeAt(0)
        mainFeedAdapter?.notifyDataSetChanged()
        AppUtils.showCommonToast(
            requireActivity(),
            "Error occur please try again later"
        )
    }

    override fun onFinish() {

    }

}