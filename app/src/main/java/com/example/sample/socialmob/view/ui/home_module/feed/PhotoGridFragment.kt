package com.example.sample.socialmob.view.ui.home_module.feed

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.arasthel.spannedgridlayoutmanager.SpanSize
import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.GridMusicFragmentBinding
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.SpaceItemDecorator
import com.example.sample.socialmob.view.utils.events.ScrollToTopEvent
import com.example.sample.socialmob.viewmodel.feed.MusicAndPhotoGridViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

@AndroidEntryPoint
class PhotoGridFragment : Fragment(), ReelBoardGridAdapter.GridImageItemClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: GridMusicFragmentBinding? = null
    private val musicGridViewModel: MusicAndPhotoGridViewModel by viewModels()
    private var globalFeed: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()
    private var feedImageGalleryAdapter: ReelBoardGridAdapter? = null
    private var mLayoutManager: StaggeredGridLayoutManager? = null

    private var mStatus: ResultResponse.Status? = null
    private var isAddedLoader: Boolean = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // TODO : Check Internet connection
        try {
            callApiCommon()

            observeData()

            binding?.goToOfflineMode?.setOnClickListener {
                if (activity != null) {
                    (activity as HomeActivity).goToOffline()
                }
            }

            feedImageGalleryAdapter = ReelBoardGridAdapter(this, true,glideRequestManager)
            val spannedGridLayoutManager = SpannedGridLayoutManager(
                orientation = SpannedGridLayoutManager.Orientation.VERTICAL,
                spans = 3
            )
            if (spannedGridLayoutManager != null) {
                spannedGridLayoutManager.itemOrderIsStable = true
                binding?.gridMusicRecyclerView?.layoutManager = spannedGridLayoutManager
                spannedGridLayoutManager.spanSizeLookup =
                    SpannedGridLayoutManager.SpanSizeLookup { position ->
                        //Conditions for 3x3 items
                        if (position % 12 == 0 || position % 12 == 7) {
                            SpanSize(2, 2)
                        } else {
                            SpanSize(1, 1)
                        }
                    }
            }
            binding?.gridMusicRecyclerView?.addItemDecoration(
                SpaceItemDecorator(
                    left = 3,
                    top = 3,
                    right = 3,
                    bottom = 3

                )
            )
            binding?.gridMusicRecyclerView?.adapter = feedImageGalleryAdapter
            binding?.gridMusicRecyclerView?.hasFixedSize()
            binding?.gridMusicRecyclerView?.addOnScrollListener(CustomScrollListener())

            // TODO : Swipe To Refresh
            binding?.swipeToRefresh?.setColorSchemeResources(R.color.purple_500)
            binding?.swipeToRefresh?.setOnRefreshListener {
                if (!InternetUtil.isInternetOn()) {
                    binding?.isVisibleList = false
                    binding?.isVisibleLoading = true
                    binding?.isVisibleNoData = false
                    binding?.isVisibleNoInternet = true
                } else {
                    globalFeed?.clear()
                    binding?.gridMusicRecyclerView?.post {
                        feedImageGalleryAdapter?.notifyDataSetChanged()
                    }
                    callApiCommon()
                }
                if (!InternetUtil.isInternetOn()) {
                    if (binding?.swipeToRefresh?.isRefreshing!!) {
                        binding?.swipeToRefresh?.isRefreshing = false
                    }
                }
            }
            binding?.refreshTextView?.setOnClickListener {
                globalFeed?.clear()
                binding?.gridMusicRecyclerView?.post {
                    feedImageGalleryAdapter?.notifyDataSetChanged()
                }
                callApiCommon()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "onActivityCreated_" + e.toString())
        }

    }

   /* override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO : Check Internet connection
        callApiCommon()

        observeData()


        binding?.goToOfflineMode?.setOnClickListener {
            if (activity != null) {
                (activity as HomeActivity).goToOffline()
            }
        }

        feedImageGalleryAdapter = ReelBoardGridAdapter(this, true,glideRequestManager)
        val spannedGridLayoutManager = SpannedGridLayoutManager(
            orientation = SpannedGridLayoutManager.Orientation.VERTICAL,
            spans = 3
        )
        spannedGridLayoutManager.itemOrderIsStable = true
        binding?.gridMusicRecyclerView?.layoutManager = spannedGridLayoutManager
        spannedGridLayoutManager.spanSizeLookup =
            SpannedGridLayoutManager.SpanSizeLookup { position ->
                //Conditions for 3x3 items
                if (position % 12 == 0 || position % 12 == 7) {
                    SpanSize(2, 2)
                } else {
                    SpanSize(1, 1)
                }
            }
        binding?.gridMusicRecyclerView?.addItemDecoration(
            SpaceItemDecorator(
                left = 3,
                top = 3,
                right = 3,
                bottom = 3

            )
        )
        binding?.gridMusicRecyclerView?.adapter = feedImageGalleryAdapter
        binding?.gridMusicRecyclerView?.hasFixedSize()
        binding?.gridMusicRecyclerView?.addOnScrollListener(CustomScrollListener())

        // TODO : Swipe To Refresh
        binding?.swipeToRefresh?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefresh?.setOnRefreshListener {
            if (!InternetUtil.isInternetOn()) {
                binding?.isVisibleList = false
                binding?.isVisibleLoading = true
                binding?.isVisibleNoData = false
                binding?.isVisibleNoInternet = true
            } else {
                globalFeed?.clear()
                binding?.gridMusicRecyclerView?.post {
                    feedImageGalleryAdapter?.notifyDataSetChanged()
                }
                callApiCommon()
            }
            if (!InternetUtil.isInternetOn()) {
                if (binding?.swipeToRefresh?.isRefreshing!!) {
                    binding?.swipeToRefresh?.isRefreshing = false
                }
            }
        }
        binding?.refreshTextView?.setOnClickListener {
            globalFeed?.clear()
            binding?.gridMusicRecyclerView?.post {
                feedImageGalleryAdapter?.notifyDataSetChanged()
            }
            callApiCommon()
        }
    }*/

    /**
     * Handle pagination
     */
    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int
        ) {
            try {
                if (dy > 0) {
                    val visibleItemCount = recyclerView.layoutManager?.childCount!!
                    val totalItemCount = recyclerView.layoutManager?.itemCount!!
                    if (SpannedGridLayoutManager != null) {
                        val firstVisibleItemPosition =
                            (recyclerView.layoutManager as SpannedGridLayoutManager).firstVisiblePosition

                        if (dy > 0) {
                            if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                                mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                                mStatus != ResultResponse.Status.LOADING &&
                                mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            ) {
                                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                                    if (InternetUtil.isInternetOn()) {
                                        loadMoreItems()
                                    }
                                }
                            }

                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun loadMoreItems() {
        try {
            if (globalFeed?.size!! > (globalFeed?.size!! - 1)) {
                if (globalFeed?.get(globalFeed?.size!! - 1)?.recordId != null) {
                    val mListSizeNew = globalFeed?.size!!
                    val mListSize = globalFeed?.get(globalFeed?.size!! - 1)?.recordId
                    callApi(mListSize!!, RemoteConstant.mCounttwenty)


                    val mutableList: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
                    val mList = PersonalFeedResponseModelPayloadFeed()
                    mutableList.add(mList)
                    mList.action?.actionId = ""

                    globalFeed?.addAll(mutableList)
                    isAddedLoader = true
                    binding?.gridMusicRecyclerView?.post {
                        feedImageGalleryAdapter?.notifyItemChanged(mListSizeNew, globalFeed?.size)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun callApiCommon() {
        if (!InternetUtil.isInternetOn()) {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = false
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = true
            val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(binding?.noInternetImageView!!)

        } else {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = true
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = false
            binding?.gifProgress?.visibility = View.VISIBLE
            binding?.gifProgress?.play()
            binding?.gifProgress?.gifResource = R.drawable.loadergif
            callApi(globalFeed?.size!!.toString(), RemoteConstant.mCount)
        }
    }

    private fun callApi(size: String, mCount: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mFeedmediagrid + RemoteConstant.mSlash +
                    size + RemoteConstant.mSlash + mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        musicGridViewModel.getPhotoGridmediagrid(mAuth, size, mCount)
    }


    private fun observeData() {

        musicGridViewModel.mPhotoVideoList.nonNull()
            .observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status
                if (binding?.swipeToRefresh?.isRefreshing!!) {
                    binding?.swipeToRefresh!!.isRefreshing = false
                }
                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        globalFeed = resultResponse?.data!!

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        feedImageGalleryAdapter?.submitList(globalFeed!!)

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            val mListSize = globalFeed?.size!!
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                            val globalFeedNew = resultResponse?.data!!

                            globalFeed?.addAll(globalFeedNew)
                            binding?.gridMusicRecyclerView?.post {
                                feedImageGalleryAdapter?.notifyItemChanged(
                                    mListSize,
                                    globalFeed?.size
                                )
                            }
                            mStatus = ResultResponse.Status.LOADING_COMPLETED
                        }, 2000)


                    }
                    ResultResponse.Status.ERROR -> {

                        if (globalFeed?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }


                    }
                    ResultResponse.Status.NO_DATA -> {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE

                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY

                    }
                    ResultResponse.Status.LOADING -> {

                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE


                    }
                    else -> {

                    }
                }

            })


    }

    private fun removeLoaderFormList() {

        if (globalFeed != null && globalFeed?.size!! > 0 && globalFeed?.get(
                globalFeed?.size!! - 1
            ) != null
        ) {
            val mListSizeNew = globalFeed?.size!!
            globalFeed?.removeAt(globalFeed?.size!! - 1)
            binding?.gridMusicRecyclerView?.post {
                feedImageGalleryAdapter?.notifyItemRangeRemoved(
                    mListSizeNew - 1,
                    globalFeed?.size!!
                )
            }
            isAddedLoader = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.grid_music_fragment, container, false)
        return binding?.root
    }


    override fun onGridItemClick(
        item: Int,
        mPostId: String,
        mRecordId: String,
        mList: String,
        type: String
    ) {


        if (type == "regular") {
            val detailIntent = Intent(activity, PhotoGridDetailsActivity::class.java)
            detailIntent.putExtra(
                "mId",
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")!!
            )
            detailIntent.putExtra("mPosition", item)
            detailIntent.putExtra("mRecordId", mRecordId)
            detailIntent.putExtra("mPath", "")
            detailIntent.putExtra("mList", mList)
            startActivity(detailIntent)
        } else {
            val detailIntent = Intent(activity, CommonFragmentActivity::class.java)
            detailIntent.putExtra(
                "mId",
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")!!
            )
            detailIntent.putExtra("mPosition", item)
            detailIntent.putExtra("mRecordId", mPostId)
            detailIntent.putExtra("mPath", "")
            detailIntent.putExtra("mList", mList)
            detailIntent.putExtra("isFrom", "Campaign")
            detailIntent.putExtra("mTitle", "#Campaign")
            startActivity(detailIntent)
        }
    }
    /**
     * Event scroll to top
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ScrollToTopEvent) {
        println("" + event)
        if (binding != null) {
            binding?.gridMusicRecyclerView?.smoothScrollToPosition(0)
        }
        if (mLayoutManager != null) {
            mLayoutManager?.scrollToPositionWithOffset(0, 0)
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        //add this code to pause videos (when app is minimised or paused)
        EventBus.getDefault().unregister(this)
    }
}