package com.example.sample.socialmob.view.ui.home_module.feed

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemFollowBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder

class PostLikeAdapter(
    private val searchPeopleAdapterFollowItemClick: SearchPeopleAdapterFollowItemClick,
    private val searchPeopleAdapterItemClick: SearchPeopleAdapterItemClick,
    private val mContext: Activity,
    private var glideRequestManager: RequestManager
) :
    ListAdapter<SearchProfileResponseModelPayloadProfile, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<SearchProfileResponseModelPayloadProfile>() {
                override fun areItemsTheSame(
                    oldItem: SearchProfileResponseModelPayloadProfile,
                    newItem: SearchProfileResponseModelPayloadProfile
                ): Boolean {
                    return oldItem._id == newItem._id
                }

                override fun areContentsTheSame(
                    oldItem: SearchProfileResponseModelPayloadProfile,
                    newItem: SearchProfileResponseModelPayloadProfile
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFollowBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_follow, parent, false
        )
        return ProfileViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(getItem(holder.adapterPosition))
    }
    /**
     * Update following status
     */
    private fun notify(adapterPosition: Int, privateProfile: Boolean?) {
        if (privateProfile == false) {
            if (getItem(adapterPosition)?.relation == "following") {
                getItem(adapterPosition)?.relation = "none"
            } else {
                getItem(adapterPosition)?.relation = "following"
            }
        } else {
            when (getItem(adapterPosition)?.relation) {
                "none" -> getItem(adapterPosition)?.relation =
                    "request pending"
                "following" -> getItem(adapterPosition)?.relation =
                    "none"
                "request pending" -> getItem(adapterPosition)?.relation =
                    "none"
            }
        }
        notifyItemChanged(adapterPosition, itemCount)
    }


    inner class ProfileViewHolder(val binding: LayoutItemFollowBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.profileViewModel = getItem(adapterPosition)
            binding.executePendingBindings()

            binding.followingTextView.setOnClickListener {

                if (getItem(adapterPosition)?.relation == "following") {
                    val dialogBuilder: AlertDialog.Builder =
                        this.let { AlertDialog.Builder(mContext) }
                    val inflater: LayoutInflater = mContext.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b: AlertDialog? = dialogBuilder.create()
                    b?.show()
                    okButton.setOnClickListener {
                        if (getItem(adapterPosition)?.relation != null && getItem(adapterPosition)?._id != null) {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                getItem(adapterPosition)?.relation?:"",
                                getItem(adapterPosition)?._id?:""
                            )
                        }

                        Handler().postDelayed({
                            val isPrivateProfile: Boolean =
                                getItem(adapterPosition)?.privateProfile == "true"

                            notify(adapterPosition, isPrivateProfile)
                        }, 100)
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        getItem(adapterPosition)?.relation?:"", getItem(adapterPosition)?._id?:""
                    )

                    Handler().postDelayed({
                        if (itemCount > 0 && getItem(adapterPosition)?.privateProfile != null) {
                            val isPrivateProfile: Boolean =
                                getItem(adapterPosition)?.privateProfile == "true"

                            notify(adapterPosition, isPrivateProfile)
                        }
                    }, 100)
                }

            }
            itemView.setOnClickListener {
                searchPeopleAdapterItemClick.onSearchPeopleAdapterItemClick(
                    getItem(adapterPosition)?._id?:"",
                    getItem(adapterPosition)?.own?:""
                )
            }
            binding.communityUserNameTextView.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding.profileImageView.setOnClickListener {
                val intent = Intent(mContext, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", getItem(adapterPosition)?._id)
                mContext.startActivityForResult(intent, 0)
            }
            glideRequestManager.load(getItem(adapterPosition).pimage).into(binding.profileImageView)
        }

    }

    override fun getItemViewType(position: Int): Int {

        return if (getItem(position)?._id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    interface SearchPeopleAdapterFollowItemClick {
        fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String)
    }

    interface SearchPeopleAdapterItemClick {
        fun onSearchPeopleAdapterItemClick(mProfileId: String, mIsOwn: String)
    }

}
