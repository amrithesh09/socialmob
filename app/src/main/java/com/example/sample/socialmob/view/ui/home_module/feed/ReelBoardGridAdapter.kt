package com.example.sample.socialmob.view.ui.home_module.feed

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ItemImageBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.google.gson.Gson


class ReelBoardGridAdapter(
    private val clickListener: GridImageItemClickListener,
    private val needToShowHashTag: Boolean,
    private val glideRequestManager: RequestManager
) : ListAdapter<PersonalFeedResponseModelPayloadFeed, BaseViewHolder<Any>>(
    DIFF_CALLBACK
) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PersonalFeedResponseModelPayloadFeed>() {
                override fun areItemsTheSame(
                    oldItem: PersonalFeedResponseModelPayloadFeed,
                    newItem: PersonalFeedResponseModelPayloadFeed
                ): Boolean {
                    return oldItem.recordId == newItem.recordId
                }

                override fun areContentsTheSame(
                    oldItem: PersonalFeedResponseModelPayloadFeed,
                    newItem: PersonalFeedResponseModelPayloadFeed
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ItemImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_image, parent, false
        )
        return ReelBoardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(getItem(holder.adapterPosition))
    }

    inner class ReelBoardViewHolder(val binding: ItemImageBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(`object`: Any?) {
            if (getItem(adapterPosition).action?.post?.media?.isNotEmpty()!! &&
                getItem(adapterPosition)?.action?.post?.media?.get(0)?.sourcePath != null
            ) {
                binding?.mpos?.text = adapterPosition.toString()

                if (getItem(adapterPosition)?.action?.post?.type == "user_video_post") {
                    glideRequestManager.load(
                        getItem(adapterPosition)?.action?.post?.media?.get(0)?.thumbnail
                    )
                        .apply(RequestOptions().centerCrop().placeholder(0))
                        .thumbnail(0.1f)
                        .into(binding?.gridImageView!!)
                } else {
                    glideRequestManager.load(
                        RemoteConstant.getImageUrlFromWidth(
                            getItem(adapterPosition)?.action?.post?.media?.get(0)?.sourcePath,
                            250
                        )
                    )
                        .apply(RequestOptions().centerCrop().placeholder(0))
                        .thumbnail(0.1f)
                        .into(binding?.gridImageView!!)
                }
            }
            binding?.isVideo = getItem(adapterPosition)?.action?.post?.type == "user_video_post"

            if (needToShowHashTag && getItem(adapterPosition)?.hashTagName != null && getItem(
                    adapterPosition
                )?.hashTagName != ""
            ) {
                binding?.hashtagName?.visibility = View.VISIBLE
                binding?.hashtagName?.text = getItem(adapterPosition)?.hashTagName
            } else {
                binding?.hashtagName?.visibility = View.GONE
            }

            binding?.root?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (getItem(adapterPosition) != null && getItem(adapterPosition)?.action != null && getItem(
                            adapterPosition
                        )?.action?.post != null
                        && getItem(adapterPosition)?.action?.post?.media != null &&
                        getItem(adapterPosition)?.action?.post?.media?.isNotEmpty()!! &&
                        getItem(adapterPosition)?.action?.post?.media?.get(0)?.sourcePath != null
                    ) {

                        var mRecordId = "0"

                        if (itemCount > 0 && getItem(adapterPosition)?.recordId != null) {
                            mRecordId = if (getItem(adapterPosition)?.recordId != null) {
                                getItem(adapterPosition)?.recordId ?: ""
                            } else {
                                "0"
                            }

                        }
                        // Passing click item as json string
                        val gson = Gson()
                        var feedJson: String? = ""
                        if (getItem(adapterPosition).action != null) {
                            feedJson =
                                gson.toJson(getItem(adapterPosition))
                        }
                        if (getItem(adapterPosition).type == "regular") {
                            clickListener.onGridItemClick(
                                adapterPosition,
                                getItem(adapterPosition).action?.post?.id ?: "",
                                mRecordId,
                                feedJson ?: "", getItem(adapterPosition).type ?: ""
                            )
                        } else {
                            clickListener.onGridItemClick(
                                adapterPosition,
                                getItem(adapterPosition).hashTagId ?: "",
                                mRecordId,
                                feedJson ?: "", getItem(adapterPosition).type ?: ""
                            )
                        }
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.action != null && getItem(position)?.action?.actionId != null) {
            if (getItem(position)?.action?.actionId != "") {
                ITEM_DATA
            } else {
                ITEM_LOADING
            }
        } else {
            ITEM_LOADING
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    interface GridImageItemClickListener {
        fun onGridItemClick(
            item: Int, mPostId: String, mRecordId: String, mList: String, type: String
        )
    }
}