package com.example.sample.socialmob.view.ui.home_module.feed

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.TreadingTrackItemBinding
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.events.FeedClickes
import kotlinx.android.synthetic.main.options_menu_trending_track.view.*
import org.greenrobot.eventbus.EventBus

class TrendingTrackAdapterFeed(
    private val trendingTracks: List<TrackListCommon>?,
    private val mContext: Context?
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<TrendingTrackAdapterFeed.TrendingTrackViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): TrendingTrackViewHolder {
        val binding = DataBindingUtil.inflate<TreadingTrackItemBinding>(
            LayoutInflater.from(parent.context), R.layout.treading_track_item, parent, false
        )
        return TrendingTrackViewHolder(binding)

    }

    override fun onBindViewHolder(holder: TrendingTrackViewHolder, position: Int) {

        holder.binding.trackViewModel = trendingTracks!![position]
        holder.binding.executePendingBindings()

        holder.itemView.setOnClickListener {
            //                    trackItemClickListener.onTrendingItemClick(
//                        holder.binding.treandingTrackImageView, holder.adapterPosition,
//                        trendingTracks[position].TrackImage, "TrendingTrack"
//                    )
            EventBus.getDefault()
                .post(
                    FeedClickes(
                        "playClick",
                        holder.adapterPosition.toString()
                    )
                )

        }
        holder.binding.topTrackOptionsImageView.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View = LayoutInflater.from(mContext)
                .inflate(R.layout.options_menu_trending_track, viewGroup)
            val mQQPop =
                PopupWindow(
                    view,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(holder.binding.topTrackOptionsImageView, -250, -15)


//            view..setOnClickListener {
//                mOptionItemClickListener.onHashTagItemClickOption("deletePlayList",
//                    mPlayList[position].PlaylistId!!, "")
//                mQQPop.dismiss()
//            }
            view.add_to_playlist_text_view.setOnClickListener {
                //                        addToPlayListClickListener.onAddToPlayListItemClick(trendingTracks[position].JukeBoxTrackId)
                EventBus.getDefault()
                    .post(
                        FeedClickes(
                            "addToPlayList",
                            trendingTracks[position].id!!
                        )
                    )
                mQQPop.dismiss()
            }
            view.play_next_text_view.setOnClickListener {
                AppUtils.showCommonToast(mContext!!, mContext.getString(R.string.play_next))
                mQQPop.dismiss()
            }
            view.add_to_queue_text_view.setOnClickListener {
                AppUtils.showCommonToast(
                    mContext!!,
                    mContext.getString(R.string.add_to_queue)
                )
                mQQPop.dismiss()
            }
            view.download_text_view.setOnClickListener {
                AppUtils.showCommonToast(mContext!!, mContext.getString(R.string.download))
                mQQPop.dismiss()
            }
            view.share_track_text_view.setOnClickListener {
                ShareAppUtils.shareTrack(trendingTracks[position].id!!, mContext)
                mQQPop.dismiss()
            }

            // TODO : To uncomment
            view.play_next_text_view.visibility = View.GONE
            view.add_to_queue_text_view.visibility = View.GONE
        }


    }

    override fun getItemCount(): Int {
        return trendingTracks!!.size
    }

    class TrendingTrackViewHolder(val binding: TreadingTrackItemBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root)

    interface TrendingItemClickListener {
        fun onTrendingItemClick(
            imageView: ImageView,
            itemId: Int,
            itemImage: String,
            isFrom: String
        )
    }

    interface AddToPlayListClickListener {
        fun onAddToPlayListItemClick(mTrackId: String)
    }
}


