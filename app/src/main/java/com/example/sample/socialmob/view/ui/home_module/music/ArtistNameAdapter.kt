package com.example.sample.socialmob.view.ui.home_module.music

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArtistNameItemBinding
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder

class ArtistNameAdapter(
    private val mContext: Activity?
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private val mutableList = mutableListOf<Artist>()
    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        val binding: ArtistNameItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(mContext), R.layout.artist_name_item, parent, false
        )
        return ArtistNameViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mutableList[holder.adapterPosition])
    }

    inner class ArtistNameViewHolder(val binding: ArtistNameItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.artistViewModel = mutableList[adapterPosition]
            binding.executePendingBindings()

            binding.artistNameTextView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    val detailIntent = Intent(mContext, ArtistPlayListActivity::class.java)
                    detailIntent.putExtra("mArtistId", mutableList[adapterPosition].id)
                    mContext?.startActivity(detailIntent)
                    mContext?.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
                }
            }

        }
    }


    override fun getItemCount(): Int = mutableList.size

    fun swap(list: List<Artist>) {
        val diffCallback = ArtistDiffCallback(this.mutableList, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.mutableList.clear()
        this.mutableList.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    private class ArtistDiffCallback(
        private val oldList: List<Artist>,
        private val newList: List<Artist>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }
    }
}