package com.example.sample.socialmob.view.ui.home_module.music

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ActivityArtistVideoDetailsBinding
import com.example.sample.socialmob.di.util.CacheMapperMention
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.CommentPostDataVideo
import com.example.sample.socialmob.model.music.DurationModel
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.repository.utils.*
import com.example.sample.socialmob.view.ui.profile.feed.PostCommentAdapter
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.socialview.Mention
import com.example.sample.socialmob.view.utils.socialview.MentionArrayAdapter
import com.example.sample.socialmob.view.utils.socialview.SocialAutoCompleteTextView
import com.example.sample.socialmob.viewmodel.music_menu.ArtistViewModel
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.exo_playback_control_view.*
import java.util.*
import java.util.regex.Pattern
import javax.inject.Inject
import kotlin.collections.ArrayList


@AndroidEntryPoint
class ArtistVideoDetailsActivity : BaseCommonActivity(),
        PostCommentAdapter.OptionPostItemClickListener {
    companion object {
        const val STATE_RESUME_WINDOW = "resumeWindow"
        const val STATE_RESUME_POSITION = "resumePosition"
        const val STATE_PLAYER_FULLSCREEN = "playerFullscreen"
        const val STATE_PLAYER_PLAYING = "playerOnPlay"
    }

    @Inject
    lateinit var glideRequestManager: RequestManager

    @Inject
    lateinit var cacheMapperMention: CacheMapperMention

    private var binding: ActivityArtistVideoDetailsBinding? = null
    private var mVideoAdapter: MusicVideoListAdapter? = null

    private var exoPlayer: SimpleExoPlayer? = null
    private var dataSourceFactory: DataSource.Factory? = null

    private var currentWindow = 0
    private var playbackPosition: Long = 0
    private var isFullscreen = false
    private var isPlayerPlaying = true
    private var mUrl = ""
    private var mArtistId = ""
    private var mArtistName = ""
    private var track: TrackListCommon? = null
    private val mediaItems = LinkedList<MediaItem>()
    private var playList: MutableList<TrackListCommon>? = ArrayList()
    private var mPlayPosition: Int = 0
    private val ID = 4 //Arbitrary, for the example
    private val viewModelArtist: ArtistViewModel by viewModels()
    private var mPlayTime = 0L
    private var mPlayTotalTime = 0L

    private var mLastId = "0"
    private var mStatus: ResultResponse.Status? = null
    private var mCommentAdapter: PostCommentAdapter? = null
    private var mGlobalCommentList: MutableList<CommentResponseModelPayloadComment>? = ArrayList()
    private var isHashTag: Boolean = false
    private var isMention: Boolean = false
    private var defaultMentionAdapter: ArrayAdapter<Mention>? = null
    private var mentionList: MutableList<Profile>? = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_artist_video_details)
        intent?.let {
            if (intent.extras != null) {
                val extras: Bundle = intent.extras!!
                if (extras.containsKey("mArtistId")) {
                    mArtistId = extras.getString("mArtistId", "")
                }
                if (extras.containsKey("mUrl")) {
                    mUrl = extras.getString("mUrl", "")
                }
            }

        }

        if (intent?.action != null && intent?.data != null) {
            val data: Uri? = intent?.data
            if (data?.lastPathSegment != null) {
                mArtistId = data.lastPathSegment!!
            }
        }
        // TODO : m3U8 video play
        dataSourceFactory = DefaultDataSourceFactory(
                this,
                Util.getUserAgent(this, getString(R.string.app_name))
        )

        initFullScreenButton()

        if (savedInstanceState != null) {
            currentWindow =
                    savedInstanceState.getInt(STATE_RESUME_WINDOW)
            playbackPosition =
                    savedInstanceState.getLong(STATE_RESUME_POSITION)
            isFullscreen =
                    savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN)
            isPlayerPlaying =
                    savedInstanceState.getBoolean(STATE_PLAYER_PLAYING)
        }

        binding?.videosForYouRecyclerView?.layoutManager = LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL, false
        )
        mVideoAdapter = MusicVideoListAdapter(this, ArrayList())
        binding?.videosForYouRecyclerView?.adapter = mVideoAdapter

        callApiCommon()


        binding?.shareLinear?.setOnClickListener {
            if (mArtistId != "") {
                ShareAppUtils.shareVideo(mArtistId, this)
            }
        }
        binding?.audioTrackLinear?.setOnClickListener {
            if (track != null) {

                if (isPlaying()) {
                    exoPlayer?.playWhenReady = false
                }

                mediaItems.clear()
                playList?.clear()
                track?.playListName = "Artist tracks - " + mArtistName
                playList?.add(track!!)

                for (sample in playList!!) {
                    val mediaItem = MediaItem(sample, true)
                    mediaItems.add(mediaItem)
                }
                viewModelArtist.createDBPlayListCommon(playList, "Artist tracks - " + mArtistName)
                launchMusic(track?.media?.coverFile!!)
            }
        }

        if (mUrl != "") {
            initPlayer(mUrl)
        }
//        binding?.descriptionTextView?.isHyperlinkEnabled = true


    }

    fun isPlaying(): Boolean {
        return exoPlayer?.playbackState == Player.STATE_READY && exoPlayer?.playWhenReady!!
    }

    private fun launchMusic(mImageUrlPass: String) {
        MyApplication.playlistManager?.setParameters(mediaItems, mPlayPosition)
        MyApplication.playlistManager?.id = ID.toLong()
        MyApplication.playlistManager?.currentPosition = mPlayPosition
        MyApplication.playlistManager?.play(0, false)
        val detailIntent =
                Intent(this@ArtistVideoDetailsActivity, NowPlayingActivity::class.java)
        detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
        startActivity(detailIntent)
    }

    private fun callApiCommon() {

        if (!isLoaded) {
            if (!InternetUtil.isInternetOn()) {
                val shake: android.view.animation.Animation =
                        AnimationUtils.loadAnimation(this, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            } else {
                binding?.noDataLinear?.visibility = View.GONE
                binding?.progressLinear?.visibility = View.VISIBLE
                binding?.dataLinear?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
                if (mArtistId != "") {
                    getSimilarVideo(mArtistId)
                }
            }
        }
    }

    private fun getSimilarVideo(mArtistVideoId: String) {

        binding?.noDataLinear?.visibility = View.GONE
        binding?.progressLinear?.visibility = View.GONE
        binding?.dataLinear?.visibility = View.GONE
        binding?.noInternetLinear?.visibility = View.GONE

        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + "/api/v1/trackvideo/" + mArtistVideoId + "/detail",
                RemoteConstant.mGetMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mAppId,
                ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModelArtist.getSimilarVideo(mAuth, mArtistId)
        observeSimilarVideoList()
    }

    private var mCommentsCount: String = "0"
    private var isLoaded: Boolean = false
    private fun observeSimilarVideoList() {

        viewModelArtist.mSimilarVideosPayload.nonNull()
                .observeOnce(this, { mSimilarVideosPayload ->
                    if (mSimilarVideosPayload != null) {
                        if (!isLoaded) {
                            isLoaded = true
                            binding?.noDataLinear?.visibility = View.GONE
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.dataLinear?.visibility = View.VISIBLE
                            binding?.noInternetLinear?.visibility = View.GONE

                            mVideoAdapter?.swapMusicVideo(mSimilarVideosPayload.similarVideos?.toMutableList()!!)
                            binding?.videoModel = mSimilarVideosPayload
                            track = mSimilarVideosPayload.track
                            mCommentsCount = viewModelArtist.commentsCount.toString()
                            binding?.executePendingBindings()

                            binding?.descriptionTextView?.isHyperlinkEnabled = true
                            binding?.descriptionTextView?.text = ""
                            binding?.descriptionTextView?.text = mSimilarVideosPayload.video?.description
                                    ?: ""
                            binding?.descriptionTextView?.setOnHyperlinkClickListener { view, text ->
                                try {
                                    val intent = Intent(Intent.ACTION_VIEW)
                                    intent.data = Uri.parse(text.toString())
                                    startActivity(intent)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }

                            val mVideoId = mSimilarVideosPayload.video?.id
                            showCommentsDialog(mVideoId)
                            binding?.commentsLinear?.setOnClickListener {
                                binding?.videoComments?.visibility = View.VISIBLE
                                binding?.videoMoreDetails?.visibility = View.GONE
                            }
                            binding?.likeMaterialButton?.setOnClickListener {
                                if (mSimilarVideosPayload.video?.liked == false) {
                                    mSimilarVideosPayload.video.liked = true
                                    binding?.videoModel?.video?.likesCount =
                                            binding?.videoModel?.video?.likesCount?.toInt()?.plus(1)
                                                    .toString()
                                    binding?.invalidateAll()
                                    viewModelArtist.addToFavouritesVideo(true, mVideoId)
                                } else {
                                    mSimilarVideosPayload.video?.liked = false
                                    binding?.videoModel?.video?.likesCount =
                                            binding?.videoModel?.video?.likesCount?.toInt()?.minus(1)
                                                    .toString()
                                    binding?.invalidateAll()
                                    viewModelArtist.addToFavouritesVideo(false, mVideoId)
                                }
                            }
                            if (mUrl == "") {
                                initPlayer(mSimilarVideosPayload.video?.media?.masterPlaylist ?: "")
                            }
                        }
                    }


                    mArtistName = ""
                    val artist = mSimilarVideosPayload.video?.artist

                    for (i in 0 until artist?.size!!) {
                        mArtistName = if (artist.size == 1) {
                            artist[i].artistName!!
                        } else {
                            if (mArtistName != "") {
                                mArtistName + " , " + artist[i].artistName
                            } else {
                                artist[i].artistName!!
                            }
                        }
                    }
                    binding?.artistDetailsTextView?.text = mArtistName

                    binding?.artistDetailsTextView?.setOnClickListener {
                        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
                        val inflater: LayoutInflater = this.layoutInflater
                        val dialogView: View =
                                inflater.inflate(R.layout.common_dialog_select_artist, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(true)

                        val artistNameRecyclerView: RecyclerView =
                                dialogView.findViewById(R.id.artist_name_recycler_view)
                        val releasedForYouAdapter = ArtistNameAdapter(this)
                        artistNameRecyclerView.adapter = releasedForYouAdapter
                        releasedForYouAdapter.swap(artist)

                        val b: AlertDialog = dialogBuilder.create()
                        b.show()
                    }
                })
    }

    private fun extractUrls(input: String): List<String>? {
        val result: MutableList<String> = ArrayList()
        var words = input.split("\\s+".toRegex()).toTypedArray()
        var mWebUrl = ""
        val pattern: Pattern = Patterns.WEB_URL
        for (word in words) {
            if (pattern.matcher(word).find()) {
                if (!word.toLowerCase().contains("http://") && !word.toLowerCase().contains("https://")) {
                    mWebUrl = "http://$word"
                } else {
                    mWebUrl = word
                }
                result.add(mWebUrl)
            }
        }
        return result
    }

    private fun showCommentsDialog(mVideoId: String?) {


        mCommentAdapter = PostCommentAdapter(
                this@ArtistVideoDetailsActivity, this, false, glideRequestManager
        )
        mGlobalCommentList?.clear()
        mCommentAdapter?.submitList(mGlobalCommentList)

        val commentSendImageView: ImageView =
                binding?.videoComments?.findViewById(R.id.comment_send_image_view)!!
        val commentEditText: SocialAutoCompleteTextView =
                binding?.videoComments?.findViewById(R.id.comment_edit_text)!!
        val commentProfileImageView: ImageView =
                binding?.videoComments?.findViewById(R.id.comment_profile_image_view)!!
        val postCommentsRecyclerView: RecyclerView =
                binding?.videoComments?.findViewById(R.id.post_comments_recycler_view)!!

        val commentsBackImageView: ImageView =
                binding?.videoComments?.findViewById(R.id.comments_back_image_view)!!


        commentEditText.isMentionEnabled = true
        defaultMentionAdapter = MentionArrayAdapter(this)
        commentEditText.mentionAdapter = defaultMentionAdapter

        commentEditText.setMentionTextChangedListener { view, text ->
            isHashTag = false
            isMention = true
            if (text.isNotEmpty()) {
                defaultMentionAdapter?.clear()
                viewModelArtist.getMentions(this, text.toString())
            }
        }
        observeSearchMention(commentEditText)

        commentsBackImageView.setOnClickListener {
            viewModelArtist.commentPaginatedResponseModel?.postValue(null)
            binding?.videoComments?.visibility = View.GONE
            binding?.videoMoreDetails?.visibility = View.VISIBLE
            binding?.videoMoreDetails?.post {
                binding?.videoMoreDetails?.fling(0)
                binding?.videoMoreDetails?.smoothScrollTo(0, 0)
            }
        }

        postCommentsRecyclerView.layoutManager =
                WrapContentLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        postCommentsRecyclerView.adapter = mCommentAdapter

        glideRequestManager.load(
                SharedPrefsUtils.getStringPreference(
                        this@ArtistVideoDetailsActivity, RemoteConstant.mUserImage, ""
                )!!
        ).into(commentProfileImageView)

        viewModelArtist.callApiComment(mVideoId, mLastId)

        viewModelArtist.commentPaginatedResponseModel?.nonNull()?.observe(this,
                Observer { resultResponse ->
                    mStatus = resultResponse.status

                    when (resultResponse.status) {
                        ResultResponse.Status.SUCCESS -> {
                            mGlobalCommentList?.clear()
                            mGlobalCommentList = resultResponse?.data!!.toMutableList()
                            mCommentAdapter?.submitList(mGlobalCommentList)

                            mStatus = ResultResponse.Status.LOADING_COMPLETED

                        }
                        ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                            mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                        }
                        ResultResponse.Status.PAGINATED_LIST -> {
                            val mCommentListSize = mGlobalCommentList?.size
                            mGlobalCommentList?.addAll(0, resultResponse?.data!!)
                            if (mCommentListSize == 0) {
                                mCommentsCount = "1"
                                mCommentAdapter?.submitList(mGlobalCommentList)
                                mCommentAdapter?.notifyDataSetChanged()
                            } else {
                                mCommentAdapter?.notifyDataSetChanged()
                            }
                            mStatus = ResultResponse.Status.LOADING_COMPLETED
                        }
                        else -> {
                        }
                    }
                })

        commentSendImageView.setOnClickListener {
            if (commentEditText.text?.isNotEmpty()!!) {
                // TODO : Check Internet connection
                if (!InternetUtil.isInternetOn()) {
                    AppUtils.showCommonToast(
                            this@ArtistVideoDetailsActivity,
                            getString(R.string.no_internet)
                    )
                } else {
                    val fullData = RemoteConstant.getEncryptedString(
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                            SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mApiKey,
                                    ""
                            )!!,
                            RemoteConstant.mBaseUrl +
                                    "/api/v1/trackvideo/" + mVideoId + "/comment",
                            RemoteConstant.putMethod
                    )
                    val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                            this, RemoteConstant.mAppId,
                            ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                    val commentPostData =
                            CommentPostDataVideo(commentEditText.text.toString(), "0")
                    viewModelArtist.postComment(mAuth, mVideoId, commentPostData)
                    AppUtils.hideKeyboard(this@ArtistVideoDetailsActivity, commentEditText)
                    commentEditText.setText("")
                    mCommentsCount = mCommentsCount.toInt().plus(1).toString()
                    binding?.commentsCount?.text = "$mCommentsCount  Comments"
                }
            } else {

                AppUtils.showCommonToast(this@ArtistVideoDetailsActivity, "Comment cannot be empty")
            }

        }
    }

    private fun initPlayer(mUrl: String) {
        if (mUrl != "") {
            val bandwidthMeter: BandwidthMeter =
                    DefaultBandwidthMeter()
            val videoTrackSelectionFactory: TrackSelection.Factory =
                    AdaptiveTrackSelection.Factory(
                            bandwidthMeter
                    )
            val trackSelector: TrackSelector =
                    DefaultTrackSelector(
                            videoTrackSelectionFactory
                    )
            exoPlayer = ExoPlayerFactory.newSimpleInstance(
                    this,
                    trackSelector
            )
            val videoSource = HlsMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(Uri.parse(mUrl))

            with(exoPlayer) {
                this!!.playWhenReady = isPlayerPlaying
                seekTo(currentWindow, playbackPosition)
                prepare(videoSource, false, false)
            }
            binding?.playerView?.player = exoPlayer

            binding?.playerView?.player?.addListener(PlayerEventListener())
            binding?.playerView?.player?.addListener(object : Player.DefaultEventListener() {
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    super.onPlayerStateChanged(playWhenReady, playbackState)
                    if (playWhenReady && playbackState == Player.STATE_READY) {
                        // media actually playing
                        pauseMusic()
                    }
                }
            })
            if (isFullscreen) {
                openFullscreen()
            }
        }
    }

    inner class PlayerEventListener : ExoPlayer.EventListener {
        override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
//            println("" + timeline)
        }

        override fun onTracksChanged(
                trackGroups: TrackGroupArray?,
                trackSelections: TrackSelectionArray?
        ) {
        }

        override fun onLoadingChanged(isLoading: Boolean) {}
        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            if (playbackState == 3) {
                if (playWhenReady) {
                    mPlayTime = System.currentTimeMillis()
                } else {
                    if (mPlayTime.toInt() != 0) {
                        val mTime = System.currentTimeMillis() - mPlayTime
                        mPlayTotalTime += mTime
                        mPlayTime = 0
                    }
                }
            } else {
                if (mPlayTime.toInt() != 0) {
                    val mPauseTime = System.currentTimeMillis() - mPlayTime
                    mPlayTotalTime += mPauseTime
                    mPlayTime = 0
                }
            }
        }

        override fun onPlayerError(error: ExoPlaybackException?) {}
        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {}
    }


    override fun onSaveInstanceState(outState: Bundle) {
        if (exoPlayer != null) {
            outState.putInt(
                    STATE_RESUME_WINDOW,
                    exoPlayer?.currentWindowIndex!!
            )
            outState.putLong(
                    STATE_RESUME_POSITION,
                    exoPlayer?.currentPosition!!
            )
            outState.putBoolean(
                    STATE_PLAYER_FULLSCREEN,
                    isFullscreen
            )
            outState.putBoolean(
                    STATE_PLAYER_PLAYING,
                    isPlayerPlaying
            )
            super.onSaveInstanceState(outState)
        }
    }

    override fun onStart() {
        super.onStart()
        if (binding?.playerView != null) binding?.playerView?.onResume()
    }

    override fun onResume() {
        super.onResume()
        if (binding?.playerView != null) binding?.playerView?.onResume()
    }

    private fun pauseMusic() {

        if (MyApplication.playlistManager != null && MyApplication.playlistManager?.playlistHandler != null) {
            if (MyApplication.playlistManager?.isPlayingItem(MyApplication.playlistManager?.currentItem)!!) {

                if (SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mState,
                                ""
                        ) == "PLAYING"
                ) {
                    MyApplication.playlistManager?.playlistHandler?.pause(true)
                }
            }
        }
        if (MyApplication.playlistManagerRadio != null &&
                MyApplication.playlistManagerRadio?.playlistHandler != null
        ) {
            if (MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                MyApplication.playlistManagerRadio?.playlistHandler?.pause(true)
            }
        }
    }

    override fun onPause() {
        println(">>>Pause")
        super.onPause()
//        if (Util.SDK_INT <= 23) {
        if (binding?.playerView != null) binding?.playerView?.onPause()

        if (exoPlayer != null) {
            exoPlayer?.playWhenReady = false
        }
//        releasePlayer()
//        }
    }

    override fun onStop() {
        super.onStop()
//        if (Util.SDK_INT > 23) {
        if (binding?.playerView != null) binding?.playerView?.onPause()
//        releasePlayer()
//        }
    }

    override fun onBackPressed() {
        if (isFullscreen) {
            closeFullscreen()
            return
        }
        super.onBackPressed()
    }

// FULLSCREEN PART

    private fun initFullScreenButton() {
        exo_fullscreen_button.visibility = View.VISIBLE
        exo_fullscreen_button.setOnClickListener {
            if (!isFullscreen) {
                openFullscreen()
            } else {
                closeFullscreen()
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private fun openFullscreen() {
//        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        exo_fullscreen_icon.setImageDrawable(
                ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_fullscreen_shrink
                )
        )
        binding?.playerView?.setBackgroundColor(ContextCompat.getColor(this, R.color.black))
        val params: LinearLayout.LayoutParams =
                binding?.playerView?.layoutParams as LinearLayout.LayoutParams
        params.width = LinearLayout.LayoutParams.MATCH_PARENT
        params.height = LinearLayout.LayoutParams.MATCH_PARENT
        binding?.playerView?.layoutParams = params
//        binding?.playerView?.rotation = 90f
        supportActionBar?.hide()
//        hideSystemUi()
        isFullscreen = true
    }

    private fun closeFullscreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        exo_fullscreen_icon.setImageDrawable(
                ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_fullscreen_expand
                )
        )
        binding?.playerView?.setBackgroundColor(
                ContextCompat.getColor(this, R.color.black)
        )
        val params: LinearLayout.LayoutParams =
                binding?.playerView?.layoutParams as LinearLayout.LayoutParams
        params.width = LinearLayout.LayoutParams.MATCH_PARENT
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT
        binding?.playerView?.layoutParams = params
        supportActionBar?.show()
//        binding?.playerView?.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        isFullscreen = false
    }

    override fun onOptionItemClickOption(
            mMethod: String, mId: String,
            mComment: String, mPosition: Int
    ) {
        when (mMethod) {
            "deleteComment" -> {
                mCommentsCount = mCommentsCount.toInt().minus(1).toString()
                binding?.commentsCount?.text = "$mCommentsCount  Comments"

                deleteComment(mId, mPosition)
            }
        }
    }

    private fun deleteComment(mId: String, mPosition: Int) {
        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + "/api/v1/trackvideo/" + mArtistId + "/comment/" + mId,
                RemoteConstant.deleteMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModelArtist.deleteComment(mAuth, mArtistId, mId)
        mGlobalCommentList?.removeAt(mPosition)
        mCommentAdapter?.notifyDataSetChanged()
    }

    fun goToDetailsPage(id: String?, masterPlaylist: String) {
        viewModelArtist.mSimilarVideosPayload.postValue(null)
        if (exoPlayer != null) {
            exoPlayer?.playWhenReady = false
            binding?.playerView?.player = null
            exoPlayer?.removeListener(null)
            exoPlayer?.stop()
            exoPlayer?.release()
            exoPlayer = null

        }
        val intent = Intent(this, ArtistVideoDetailsActivity::class.java)
        intent.putExtra("mArtistId", id)
        intent.putExtra("mUrl", masterPlaylist)
        intent.putExtra("isFromViewAll", "false")
        startActivityForResult(intent, 90)
        finish()
    }

    private fun observeSearchMention(commentEditText: SocialAutoCompleteTextView) {
        viewModelArtist.mentionProfileResponseModel.nonNull().observe(this, { profiles ->
            if (profiles != null) {
                var mMentionList: MutableList<Mention>
                if (profiles.isNotEmpty()) {
                    mentionList = profiles

                    val mMentionEditTextList: MutableList<String> =
                            commentEditText.mentions ?: ArrayList()

                    val mPopulateList: MutableList<Profile> =
                            mentionList?.filter {
                                it.username !in mMentionEditTextList.map { item ->
                                    item.replace("@", "")
                                }
                            } as MutableList<Profile>
                    mMentionList =
                            cacheMapperMention.mapFromEntityListMention(mPopulateList).toMutableList()
                    defaultMentionAdapter?.addAll(mMentionList)
                }

                if (defaultMentionAdapter == null) {
                    defaultMentionAdapter = MentionArrayAdapter(this)
                    commentEditText.mentionAdapter = defaultMentionAdapter
                } else {
                    commentEditText.mentionAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onDestroy() {
        if (exoPlayer != null) {
            exoPlayer?.playWhenReady = false
        }
        val mTotalDuration = (mPlayTotalTime / 1000) % 60
        val mDuration = DurationModel()
        mDuration.duration = mTotalDuration.toString()
        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl +
                        RemoteConstant.mSlash + "api/v1/trackvideo/" + mArtistId + "/analytics",
                RemoteConstant.postMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModelArtist.postVideoAnalytics(mArtistId, mDuration, mAuth)

        viewModelArtist.mSimilarVideosPayload.postValue(null)
        viewModelArtist.commentPaginatedResponseModel?.postValue(null)


        super.onDestroy()
        if (exoPlayer != null) {
            exoPlayer?.playWhenReady = false
            exoPlayer?.removeListener(null)
            exoPlayer?.release()
            binding?.playerView?.player = null
            exoPlayer = null

        }
    }

}