package com.example.sample.socialmob.view.ui.home_module.music

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.DialogPlayListItemBinding
import com.example.sample.socialmob.model.music.PlaylistCommon


class BottomSheetPlayListAdapter(
    private val playList: List<PlaylistCommon>?,
    private val mPlayListItemClickListener: PlayListItemClickListener
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BottomSheetPlayListAdapter.PlayListViewHolder>() {
    private var row_index = -1
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PlayListViewHolder {
        val binding = DataBindingUtil.inflate<DialogPlayListItemBinding>(
            LayoutInflater.from(parent.context), R.layout.dialog_play_list_item, parent, false
        )
        return PlayListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlayListViewHolder, position: Int) {
        holder.binding.bottomSheetPlayListViewModel = playList?.get(holder.adapterPosition)!!
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener {
            row_index = position
            notifyDataSetChanged()
            mPlayListItemClickListener.onPlayListItemClick(playList[position].id!!)
        }

        if (playList[position].playlistName.equals("FAVOURITES", ignoreCase = true)
            || playList[position].playlistName.equals("LISTEN LATER" ,ignoreCase = true)) {
            if (row_index == position) {
                holder.binding.bottomDialogItem.setBackgroundColor(Color.parseColor("#ffba00"))
                holder.binding.starImageOne.visibility = View.VISIBLE
                holder.binding.starImageTwo.visibility = View.VISIBLE
//            holder.tv1.setTextColor(Color.parseColor("#ffffff"))
            } else {
                holder.binding.bottomDialogItem.setBackgroundColor(Color.parseColor("#cccccc"))
                holder.binding.starImageOne.visibility = View.INVISIBLE
                holder.binding.starImageTwo.visibility = View.INVISIBLE
//            holder.tv1.setTextColor(Color.parseColor("#000000"))
            }
        } else {

            if (row_index == position) {
                holder.binding.bottomDialogItem.setBackgroundColor(Color.parseColor("#1e90ff"))
                holder.binding.starImageOne.visibility = View.INVISIBLE
                holder.binding.starImageTwo.visibility = View.INVISIBLE
//            holder.tv1.setTextColor(Color.parseColor("#ffffff"))
            } else {
                holder.binding.bottomDialogItem.setBackgroundColor(Color.parseColor("#cccccc"))
                holder.binding.starImageOne.visibility = View.INVISIBLE
                holder.binding.starImageTwo.visibility = View.INVISIBLE
//            holder.tv1.setTextColor(Color.parseColor("#000000"))
            }
        }
    }


    // Gets the number of quick lists in the playList
    override fun getItemCount(): Int {
        return playList!!.size
    }


    class PlayListViewHolder(val binding: DialogPlayListItemBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root)

    interface PlayListItemClickListener {
        fun onPlayListItemClick(mId: String)
    }
}