package com.example.sample.socialmob.view.ui.home_module.music

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.model.music.Genre
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import com.yuyakaido.android.cardstackview.CardStackLayoutManager
import com.yuyakaido.android.cardstackview.CardStackListener
import com.yuyakaido.android.cardstackview.Direction
import com.yuyakaido.android.cardstackview.SwipeableMethod
import kotlinx.android.synthetic.main.activity_card_select.*


class CardsSelectActivity : AppCompatActivity(), CardStackListener {
    private lateinit var layoutManager: CardStackLayoutManager
    private var viewModel: MusicDashBoardViewModel? = null
    private var adapter: SwipeCardAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_select)

        viewModel = ViewModelProvider(this).get(MusicDashBoardViewModel::class.java)
        callApiCommon()
        observeGenre()
        layoutManager = CardStackLayoutManager(this, this).apply {
            setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
            setOverlayInterpolator(LinearInterpolator())
        }

        stack_view.layoutManager = layoutManager
        adapter = SwipeCardAdapter(this)
        stack_view.adapter = adapter
        stack_view.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }


    }

    private fun callApiCommon() {

        if (!InternetUtil.isInternetOn()) {
            finish()
        } else {
            genreListApi()
        }

    }

    private fun genreListApi() {
        val mAuth: String =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                this@CardsSelectActivity,
                RemoteConstant.pathGenre
            )
        viewModel?.getGenreApiCall(mAuth)
    }

    private fun observeGenre() {
//        viewModel?.genresMutableLiveData?.nonNull()?.observe(this, Observer { genreList ->
//            if (genreList != null && genreList.status == ResultResponse.Status.SUCCESS) {
//                mGenreListData = genreList.data!!
//                adapter?.submitList(mGenreListData)
//
//            }
//
//        })

    }

    private var mSwipeCount = 0
    private var mGenreListData: MutableList<Genre>? = ArrayList()
    override fun onCardDisappeared(view: View?, position: Int) {
//        println(">>>>onCardDisappeared")
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
//        println(">>>>onCardDragging")

        when (direction) {
            Direction.Left -> {
                like_dislike_text_view.text = "DISLIKE"
                like_dislike_text_view.setTextColor(Color.parseColor("#eb4d4b"))
            }
            Direction.Right -> {
                like_dislike_text_view.text = "LIKE"
                like_dislike_text_view.setTextColor(Color.GREEN)
            }
            else -> {
                like_dislike_text_view.text = ""
            }
        }
    }

    override fun onCardSwiped(direction: Direction?) {
//        println(">>>>onCardSwiped")
        mSwipeCount += 1
        if (mGenreListData?.size == mSwipeCount) {
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                Toast.makeText(this, "Completed", Toast.LENGTH_SHORT).show()
                finish()
            }, 1000)
        }
        like_dislike_text_view.text = ""
    }

    override fun onCardCanceled() {
        like_dislike_text_view.text = ""
//        println(">>>>onCardCanceled")
    }

    override fun onCardAppeared(view: View?, position: Int) {
        like_dislike_text_view.text = ""
//        println(">>>>onCardAppeared")
    }

    override fun onCardRewound() {
        like_dislike_text_view.text = ""
//        println(">>>>onCardRewound")
    }
}
