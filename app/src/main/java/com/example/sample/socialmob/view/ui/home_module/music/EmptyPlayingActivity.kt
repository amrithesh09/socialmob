package com.example.sample.socialmob.view.ui.home_module.music

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.EmptyPlayingActivityBinding
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.BaseCommonActivity

class EmptyPlayingActivity : BaseCommonActivity() {
    private var emptyPlayingActivityBinding: EmptyPlayingActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
         emptyPlayingActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.empty_playing_activity)

        emptyPlayingActivityBinding?.goToMusic?.setOnClickListener {
            val intent = Intent(this, CommonFragmentActivity::class.java)
            intent.putExtra("isFrom", "Genre")
            startActivity(intent)
            finish()
        }
        emptyPlayingActivityBinding?.emptyBackImageView?.setOnClickListener {
            finish()
            overridePendingTransition(
                0,
                R.anim.play_panel_close_background
            )
        }
    }
    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }
}
