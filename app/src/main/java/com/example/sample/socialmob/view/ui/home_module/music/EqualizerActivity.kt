package com.example.sample.socialmob.view.ui.home_module.music

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bullhead.equalizer.EqualizerFragment
import com.example.sample.socialmob.R

class EqualizerActivity : AppCompatActivity() {
    private var sessionId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeStatusBarColor()
        setContentView(R.layout.activity_equalizer)

        val extras = intent.extras
        if (extras != null) {

            if (extras.containsKey("audioSessionIdNowPlaying")) {
                sessionId = extras.getInt("audioSessionIdNowPlaying")
            }
        }
        if (sessionId != 0) {
            val equalizerFragment = EqualizerFragment.newBuilder()
                .setAccentColor(Color.parseColor("#1e90ff"))
                .setAudioSessionId(sessionId)
                .build()
            supportFragmentManager.beginTransaction()
                .replace(R.id.eqFrame, equalizerFragment)
                .commit()
        }
    }

    private fun changeStatusBarColor() {
        // finally change the color
//        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

}
