package com.example.sample.socialmob.view.ui.home_module.music

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MusicVideoItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.music.MusicVideoPayloadVideo
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistVideoViewAllActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder

class MusicVideoListAdapter(
    private val mContext: Context?,
    private var list: MutableList<MusicVideoPayloadVideo>?
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MusicVideoItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.music_video_item, parent, false
        )
        return MusicVideoViewHolder(binding)
    }


    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(list?.get(holder.adapterPosition))
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class MusicVideoViewHolder(val binding: MusicVideoItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(`object`: Any?) {
            binding.artistViewModel = list?.get(adapterPosition)
            binding.executePendingBindings()

            if (list?.get(adapterPosition)?.artist != null &&
                list?.get(adapterPosition)?.artist?.size != null &&
                list?.get(adapterPosition)?.artist?.size ?: 0 > 0
            ) {
                var mArtistName = ""
                val artist = list?.get(adapterPosition)?.artist
                for (i in 0 until artist?.size!!) {
                    mArtistName = if (artist.size == 1) {
                        artist[i].artistName!!
                    } else {
                        if (mArtistName != "") {
                            mArtistName + " , " + artist[i].artistName
                        } else {
                            artist[i].artistName!!
                        }
                    }
                }
                binding.artistDetailsTextView.text =
                    mArtistName + " - " + list?.get(adapterPosition)?.viewCount + " views"

            }
            binding.playCardView.setOnClickListener {
                when (mContext) {
                    is ArtistVideoViewAllActivity -> {
                        mContext.goToDetailsPage(
                            list?.get(
                                adapterPosition
                            )?.id, list?.get(
                                adapterPosition
                            )?.media?.masterPlaylist
                        )
                    }
                    is ArtistVideoDetailsActivity -> {
                        mContext.goToDetailsPage(
                            list?.get(
                                adapterPosition
                            )?.id, list?.get(
                                adapterPosition
                            )?.media?.masterPlaylist!!
                        )
                    }
                    else -> {

                    }
                }
            }

        }

    }

    override fun getItemViewType(position: Int): Int {

        return when {
            list?.get(position)?.id == "" -> {
                ITEM_LOADING
            }
            list?.get(position)?.id != "" -> {
                ITEM_DATA
            }
            else -> {
                ITEM_LOADING
            }
        }
    }

    fun swapMusicVideo(mutableList: MutableList<MusicVideoPayloadVideo>) {
        val diffCallback = VideoListDiffUtils(this.list!!, mutableList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.list?.clear()
        this.list?.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }
}


private class VideoListDiffUtils(
    private val oldList: MutableList<MusicVideoPayloadVideo>,
    private val newList: MutableList<MusicVideoPayloadVideo>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id

    }

}