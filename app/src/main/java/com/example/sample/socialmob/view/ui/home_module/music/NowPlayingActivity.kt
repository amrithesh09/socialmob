package com.example.sample.socialmob.view.ui.home_module.music

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.*
import android.text.format.DateUtils
import android.transition.ChangeBounds
import android.transition.Transition
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.NowPlayingActivityBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.repository.utils.observe
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.MusicDashBoardFragment
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.PlayListImageActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.RecentlyPlayedActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchTrackFragment
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.TapTarget.OnSpotlightStateChangedListener
import com.example.sample.socialmob.view.utils.TapTarget.Spotlight
import com.example.sample.socialmob.view.utils.TapTarget.shape.Circle
import com.example.sample.socialmob.view.utils.TapTarget.target.SimpleTarget
import com.example.sample.socialmob.view.utils.events.NotificationSwipeClose
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DefaultPlaylistHandler
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PodCastProgress
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaProgress
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.listener.ProgressListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.NowPlayingViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.firebase.analytics.FirebaseAnalytics
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class NowPlayingActivity : BaseCommonActivity(), PlaylistListener<MediaItem>, ProgressListener {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private var mTrackId = ""
    private var mTrackName = ""
    private var mTrackGenre = ""
    private var mTrackArtist = ""
    private var mTrackPlayCount = ""
    private var mTrackImage = ""
    private var mImage: String = ""
    private var mTitle: String = ""
    private var mIsFrom: String = ""
    private var extraIsFromRadio: Boolean = false
    private var isLoaded = false
    private var shouldSetDuration: Boolean = false
    private var userInteracting: Boolean = false
    private var isPlayed: Boolean = false
    private var mLastId: String = ""
    private var mLastClickTime: Long = 0
    private var mClickCount: Int = 0

    private var nowPlayingActivityBinding: NowPlayingActivityBinding? = null
    private val nowPlayingViewModel: NowPlayingViewModel by viewModels()
    private var playlistManager: PlaylistManager? = null
    private var selectedPosition = 0
    private var mediaItems = LinkedList<MediaItem>()
    private var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
    private var newList: MutableList<PlayListDataClass>? = ArrayList()


    private var playListImageView: ImageView? = null
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"
    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private val GROUP_ID = "listGroup".hashCode()

    companion object {
        var mGlobalList: MutableList<PlayListDataClass>? = ArrayList()
        var PLAYLIST_ID = 4 //Arbitrary, for the example

        private val formatBuilder = StringBuilder()

        @SuppressLint("ConstantLocale")
        private val formatter = Formatter(formatBuilder, Locale.getDefault())

        /**
         * Formats the specified milliseconds to a human readable format
         * in the form of (Hours : Minutes : Seconds).  If the specified
         * milliseconds is less than 0 the resulting format will be
         * "--:--" to represent an unknown time
         *
         * @param milliseconds The time in milliseconds to format
         * @return The human readable time
         */
        fun formatMs(milliseconds: Long): String {
            if (milliseconds < 0) {
                return "--:--"
            }

            val seconds = milliseconds % DateUtils.MINUTE_IN_MILLIS / DateUtils.SECOND_IN_MILLIS
            val minutes = milliseconds % DateUtils.HOUR_IN_MILLIS / DateUtils.MINUTE_IN_MILLIS
            val hours = milliseconds % DateUtils.DAY_IN_MILLIS / DateUtils.HOUR_IN_MILLIS

            formatBuilder.setLength(0)
            return if (hours > 0) {
                formatter.format("%d:%02d:%02d", hours, minutes, seconds).toString()
            } else formatter.format("%02d:%02d", minutes, seconds).toString()

        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nowPlayingActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.now_playing_activity)

        if (Build.VERSION.SDK_INT >= 21) {
            window.navigationBarColor = ContextCompat.getColor(
                    this,
                    R.color.white
            ) // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            window.statusBarColor =
                    ContextCompat.getColor(this, R.color.black) //status bar or the time bar at the top
        }

        nowPlayingActivityBinding?.viewModelNowPlaying = nowPlayingViewModel


        playlistManager = MyApplication.playlistManager
        updateCurrentPlaybackInformation()

        nowPlayingActivityBinding?.nowPlayingBackImageView?.setOnClickListener {
            onBackPressed()
        }

        nowPlayingActivityBinding?.audioPlayerLoading?.play()
        nowPlayingActivityBinding?.audioPlayerLoading?.gifResource = R.drawable.loadergif

        // TODO : Transition Receive and Bundle
        retrieveExtras()
        inItElementTransition()


        val paramsCard: ViewGroup.LayoutParams =
                nowPlayingActivityBinding?.audioPlayerImage?.layoutParams!!
        // Changes the height and width to the specified *pixels*
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val screenHeight: Double = displayMetrics.heightPixels * 0.60
        paramsCard.height = screenHeight.toInt() - 400
        paramsCard.width = screenHeight.toInt() - 400
        nowPlayingActivityBinding?.audioPlayerImage?.layoutParams = paramsCard
//        cover.

//        val paramsCardcover: ViewGroup.LayoutParams = layoutParams
//        // Changes the height and width to the specified *pixels*
//        val displayMetricscover = DisplayMetrics()
//        windowManager.defaultDisplay.getMetrics(displayMetricscover)
//        val screenHeightcover: Double = displayMetricscover.heightPixels * 0.60
//        paramsCardcover.height = screenHeightcover.toInt() - 400
//        paramsCardcover.width = screenHeightcover.toInt() - 400
//
//        val params = RelativeLayout.LayoutParams(
//            RelativeLayout.LayoutParams.WRAP_CONTENT,
//            RelativeLayout.LayoutParams.WRAP_CONTENT
//        )
//        params.height = screenHeightcover.toInt() - 400
//        params.width = screenHeightcover.toInt() - 400
//        params.setMargins(200, 0, 0, 0)
//        cover.layoutParams = params

        // TODO: Share Song
        var isLiked: String

        //TODO : Add to favourite
        try {
            if (
                    MyApplication.playlistManager != null &&
                    MyApplication.playlistManager?.currentItem != null &&
                    MyApplication.playlistManager?.currentItem?.isFavorite != null &&
                    MyApplication.playlistManager?.currentItemChange != null &&
                    MyApplication.playlistManager?.currentItemChange?.currentItem != null &&
                    MyApplication.playlistManager?.currentItemChange?.currentItem?.isFavorite != null
            ) {
                nowPlayingActivityBinding?.likeMaterialButton?.visibility = View.VISIBLE
                isLiked =
                        MyApplication.playlistManager?.currentItemChange?.currentItem?.isFavorite!!
                if (isLiked == "true") {
                    nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)
                } else {
                    nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_grey)
                }
                nowPlayingActivityBinding?.likeMaterialButton?.setOnClickListener {
                    // TODO : get current track id
                    if (MyApplication.playlistManager?.currentItemChange?.currentItem?.mExtraId != null) {
                        val mTrackId: String =
                                MyApplication.playlistManager?.currentItemChange?.currentItem?.mExtraId!!
                        val mImage: String? = playlistManager?.currentItem?.thumbnailUrl
                        if (mImage != null && mImage != "" && !mImage.contains("podcast")) {


                            if (isLiked == "false") {
                                isLiked = "true"
                                // TODO : Update list ( Featured, Top , Trending )
                                EventBus.getDefault().post(UpdateTrackEvent(mTrackId, isLiked))
                                MyApplication.playlistManager?.currentItem?.isFavorite =
                                        "true"
                                nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)

                                val fullData = RemoteConstant.getEncryptedString(
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mAppId,
                                                ""
                                        )!!,
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mApiKey,
                                                ""
                                        )!!,
                                        RemoteConstant.mBaseUrl +
                                                RemoteConstant.trackData +
                                                RemoteConstant.mSlash + mTrackId +
                                                RemoteConstant.pathFavorite, RemoteConstant.putMethod
                                )
                                val mAuth =
                                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mAppId,
                                                ""
                                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                                nowPlayingViewModel.addToFavourites(mAuth, mTrackId)

                                try {
                                    when (mIsFrom) {

                                        "TopTrack" -> {
                                            MusicDashBoardFragment.topTrackModel!![playlistManager?.currentPosition!!].favorite =
                                                    "true"
                                        }
                                        "TrendingTrack" -> {
                                            MusicDashBoardFragment.trendingTrackModel!![playlistManager?.currentPosition!!].favorite =
                                                    "true"

                                        }
                                        "MusicDashBoard" -> {
                                            MusicDashBoardFragment.featuredTrackModelNew!![playlistManager?.currentPosition!!].favorite =
                                                    "true"
                                        }
                                        "dblistSearchTrack" -> {
                                            SearchTrackFragment.dbPlayList!![playlistManager?.currentPosition!!].favorite =
                                                    "true"
                                        }
                                        "dblistMusicGrid" -> {

                                        }
                                        "dblistFeedRecommended" -> {

                                        }
                                        "dblistUserProfile" -> {

                                        }
                                        "dbRecentlyPlayedSongs" -> {
                                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                                    "true"
                                        }
                                        "dbLikedsongs" -> {
                                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                                    "true"
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }


                            } else {
                                isLiked = "false"
                                // TODO : Update list ( Featured, Top , Trending )
                                EventBus.getDefault().post(UpdateTrackEvent(mTrackId, isLiked))
                                MyApplication.playlistManager?.currentItem?.isFavorite =
                                        "false"
                                nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_grey)
                                val fullData = RemoteConstant.getEncryptedString(
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mAppId,
                                                ""
                                        )!!,
                                        SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mApiKey,
                                                ""
                                        )!!,
                                        RemoteConstant.mBaseUrl +
                                                RemoteConstant.trackData +
                                                RemoteConstant.mSlash + mTrackId +
                                                RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                                )
                                val mAuth =
                                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mAppId,
                                                ""
                                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                                nowPlayingViewModel.removeFavourites(mAuth, mTrackId)
                                try {
                                    when (mIsFrom) {
                                        "TopTrack" -> {
                                            MusicDashBoardFragment.topTrackModel!![playlistManager?.currentPosition!!].favorite =
                                                    "false"
                                        }
                                        "TrendingTrack" -> {
                                            MusicDashBoardFragment.trendingTrackModel!![playlistManager?.currentPosition!!].favorite =
                                                    "false"
                                        }
                                        "MusicDashBoard" -> {
                                            MusicDashBoardFragment.featuredTrackModelNew!![playlistManager!!.currentPosition].favorite =
                                                    "false"
                                        }
                                        "dblistSearchTrack" -> {
                                            SearchTrackFragment.dbPlayList!![playlistManager?.currentPosition!!].favorite =
                                                    "false"
                                        }
                                        "dblistMusicGrid" -> {

                                        }
                                        "dblistFeedRecommended" -> {

                                        }
                                        "dblistUserProfile" -> {

                                        }
                                        "dbRecentlyPlayedSongs" -> {
                                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                                    "false"
                                        }
                                        "dbLikedsongs" -> {
                                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                                    "false"
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }

                            nowPlayingViewModel.updateFavourite(isLiked, mTrackId)
                        }
                    }

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }






        nowPlayingActivityBinding?.nowPlayingOptionsImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null

            firebaseAnalytics = FirebaseAnalytics.getInstance(this)


            val paramsNotification = Bundle()
            paramsNotification.putString(
                    "user_id",
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
            )
            paramsNotification.putString("track_id", mTrackId)
            paramsNotification.putString("event_type", "track_download")
            firebaseAnalytics.logEvent("offline_track_download", paramsNotification)


            val mBottomSheetDialog = RoundedBottomSheetDialog(this)
            val sheetView: View =
                    layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
            mBottomSheetDialog.setContentView(sheetView)
            mBottomSheetDialog.show()
            val addToPlaylistLinear: LinearLayout =
                    sheetView.findViewById(R.id.add_to_playlist_linear)
            val addPlayListImageView: ImageView =
                    sheetView.findViewById(R.id.add_play_list_image_view)
            val equalizerImageView: ImageView = sheetView.findViewById(R.id.equalizer_image_view)
            val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
                    sheetView.findViewById(R.id.play_list_recycler_view)
            val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
            val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
            val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
            val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)
            val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)
            val navLinear: LinearLayout = sheetView.findViewById(R.id.nav_linear)
            val viewSeparator: View = sheetView.findViewById(R.id.view_separator)
            val downloadButton: ImageView = sheetView.findViewById(R.id.download_button)
            val downloadTextView: TextView = sheetView.findViewById(R.id.download_text_view)
            val equalizerTextView: TextView = sheetView.findViewById(R.id.equalizer_text_view)
            equalizerLinear.visibility = View.VISIBLE
            val playListName = playlistManager?.currentItemChange?.currentItem?.playListName ?: ""
            if (playListName.contains("Music station")) {
                navLinear.visibility = View.GONE
            }

            if (playlistManager != null &&
                    playlistManager?.currentItemChange != null &&
                    playlistManager?.currentItemChange?.currentItem != null &&
                    playlistManager?.currentItemChange?.currentItem?.downloadedOffline != null
            ) {

                //TODO : Disable equalizer

                DrawableCompat.setTint(
                        equalizerImageView.drawable,
                        ContextCompat.getColor(this, R.color.selected_notification_color)
                )
                equalizerTextView.setTextColor(
                        ContextCompat.getColor(
                                this,
                                R.color.selected_notification_color
                        )
                )
                val allowOfflineDownload: String? =
                        playlistManager?.currentItemChange?.currentItem?.downloadedOffline!!
                downloadLinear.visibility = View.VISIBLE
                if (allowOfflineDownload == "1") {
//                    downloadLinear.visibility = View.VISIBLE
                } else {
                    DrawableCompat.setTint(
                            downloadButton.drawable,
                            ContextCompat.getColor(this, R.color.selected_notification_color)
                    )
                    downloadTextView.setTextColor(
                            ContextCompat.getColor(
                                    this,
                                    R.color.selected_notification_color
                            )
                    )
//                    downloadLinear.visibility = View.GONE
                }

                downloadLinear.setOnClickListener {
                    if (allowOfflineDownload == "1") {
                        mBottomSheetDialog.dismiss()
                        showRewardedVideo(false)
                    }
                }
            }


            val mImage: String? = playlistManager?.currentItemChange?.currentItem?.thumbnailUrl
            if (mImage != null && mImage != "" && mImage.contains("podcast")) {
                equalizerLinear.visibility = View.GONE
                artistLinear.visibility = View.GONE
                albumLinear.visibility = View.GONE
                viewSeparator.visibility = View.GONE
            }
            albumLinear.setOnClickListener {
                if (playlistManager != null &&
                        playlistManager?.currentItemChange?.currentItem != null &&
                        playlistManager?.currentItem != null &&
                        playlistManager?.currentItem?.albumId != null &&
                        playlistManager?.currentItem?.albumName != null
                ) {
                    val intent = Intent(this, CommonPlayListActivity::class.java)
                    intent.putExtra(
                            "mId",
                            playlistManager?.currentItemChange?.currentItem?.albumId!!
                    )
                    intent.putExtra(
                            "title",
                            playlistManager?.currentItemChange?.currentItem?.albumName!!
                    )
                    intent.putExtra("mIsFrom", "artistAlbum")
                    startActivity(intent)
                }
            }
            artistLinear.setOnClickListener {
                if (playlistManager != null &&
                        playlistManager?.currentItemChange != null &&
                        playlistManager?.currentItemChange?.currentItem != null &&
                        playlistManager?.currentItemChange?.currentItem?.artistId != null
                ) {
                    val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
                    detailIntent.putExtra(
                            "mArtistId",
                            MyApplication.playlistManager?.currentItemChange?.currentItem?.artistId!!
                    )
                    startActivity(detailIntent)
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
                }
            }

            val mAuth =
                    RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
            nowPlayingViewModel.getPlayList(mAuth)

            nowPlayingViewModel.playListModel.nonNull()
                    .observe(this, androidx.lifecycle.Observer {

                        if (it.isNotEmpty()) {
                            mPlayList = it as MutableList
                            playListRecyclerView.adapter =
                                    CommonPlayListAdapter(
                                            mPlayList,
                                            this,
                                            false, glideRequestManager
                                    )

                            // TODO : Update playlist ( Home page )
                            EventBus.getDefault().postSticky(UpdatePlaylist(mPlayList!!))
                        }

                    })


            // TODO: Add to play list
            addPlayListImageView.setOnClickListener {
                createPlayListDialogFragment()
            }


            if (mImage != null && mImage != "" && mImage.contains("podcast")) {
                addToPlaylistLinear.visibility = View.GONE
                playListRecyclerView.visibility = View.GONE
                nowPlayingActivityBinding?.shuffleButton?.isEnabled = false
                nowPlayingActivityBinding?.repeatButton?.isEnabled = false
                nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.disable_color
                        )
                )
                setPlayState(
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mRepeatStatus, "0"
                        )?.toInt()
                )
            } else {
                nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.white
                        )
                )
                setPlayState(
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mRepeatStatus, "0"
                        )?.toInt()
                )
                nowPlayingActivityBinding?.shuffleButton?.isEnabled = true
                nowPlayingActivityBinding?.repeatButton?.isEnabled = true
                // TODO : Set adapter playlist
                nowPlayingViewModel.playListModel.nonNull().observe(this) {
                    if (it != null && it.isNotEmpty()) {
                        mPlayList = it as MutableList
                        playListRecyclerView.adapter =
                                CommonPlayListAdapter(
                                        it,
                                        this,
                                        false, glideRequestManager
                                )
                        EventBus.getDefault().postSticky(UpdatePlaylist(mPlayList!!))
                    }

                }
                // TODO : Shuffle
                if (SharedPrefsUtils.getStringPreference(
                                this, RemoteConstant.mShuffleStatus,
                                "false"
                        ) == "true"
                ) {
                    setShuffleStateImage("true", false)
                }
            }
            playListRecyclerView.addOnItemTouchListener(
                    RecyclerItemClickListener(
                            this,
                            playListRecyclerView,
                            object : RecyclerItemClickListener.ClickListener {
                                override fun onItemClick(view: View, position: Int) {
                                    //TODO : Call Add to playlist API
                                    if (mPlayList != null && mPlayList?.get(position)?.id != null) {
                                        val mTrackId: String =
                                                playlistManager?.currentItemChange?.currentItem?.mExtraId
                                                        ?: ""
                                        if (mTrackId != "") {
                                            addTrackToPlayList(
                                                    mTrackId, mPlayList?.get(position)?.id
                                                    ?: ""
                                            )
                                        }
                                    }
                                    mBottomSheetDialog.dismiss()
                                }

                                override fun onLongItemClick(view: View?, position: Int) {

                                }


                            })
            )
            // TODO : Equalizer onClick
            equalizerLinear.setOnClickListener {
                try {
//                    val id = AudioApi.audioPlayer.audioSessionId
//                    val intent = Intent(this, EqualizerActivity::class.java)
//                    intent.putExtra("audioSessionIdNowPlaying", id)
//                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                mBottomSheetDialog.dismiss()
            }
            shareLinear.setOnClickListener {
                if (playlistManager != null && playlistManager?.currentItem != null) {
                    if (mImage != null && mImage != "" && mImage?.contains("podcast")!! && playlistManager?.currentItem?.album != null) {
                        ShareAppUtils.sharePodcastEpisode(
                                playlistManager?.currentItem?.mExtraId!!,
                                this,
                                playlistManager?.currentItem?.album?.replace("\\W+", " ")
                                        ?.replace(" ", "-")!!
                        )
                    } else {
                        ShareAppUtils.shareTrack(
                                playlistManager?.currentItem?.mExtraId!!,
                                this
                        )
                    }
                }
                mBottomSheetDialog.dismiss()

            }

        }
        //TODO : Visible Now Playing Layout In Navigation Drawer
        isPlayed = true


        // TODO : To Handle Share Track
        if (intent?.action != null && intent?.data != null) {
            nowPlayingActivityBinding?.musicProgress?.visibility = View.VISIBLE
            val data: Uri? = intent?.data
            if (data?.lastPathSegment != null) {

                if (data.toString().contains("podcast")) {
//                    val mIdLong = hashids.decode(data.lastPathSegment!!)
//                    val mSongId = mIdLong[0].toString()
                    val mSongId = data.lastPathSegment!!

                    val fullData = RemoteConstant.getEncryptedString(
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathPodCastEpisodeDetails
                                    + RemoteConstant.mSlash + mSongId, RemoteConstant.mGetMethod
                    )
                    val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                            this, RemoteConstant.mAppId,
                            ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                    GlobalScope.launch {
                        nowPlayingViewModel.getPodCastDetails(mAuth, mSongId)
                    }
                    nowPlayingViewModel.podcastResponseModel.observe(
                            this, {
                        if (it?.payload != null && it.payload?.episode != null) {
                            val dbPlayList: MutableList<RadioPodCastEpisode> = ArrayList()
                            dbPlayList.add(it.payload?.episode!!)
                            nowPlayingActivityBinding?.musicProgress?.visibility = View.GONE
                            nowPlayingViewModel.createDbPlayListModelNotificationPodcast(
                                    dbPlayList, "Podcast suggestion"
                            )
                            val detailIntent = Intent(this, NowPlayingActivity::class.java)
                            detailIntent.putExtra(
                                    RemoteConstant.mExtraIndex, 0
                            )
                            detailIntent.putExtra(
                                    RemoteConstant.extraIndexIsFrom,
                                    "dblistMyFavTrack"
                            )
                            startActivity(detailIntent)
                            overridePendingTransition(0, 0)
                            finish()
                            nowPlayingViewModel.podcastResponseModel.value = null
                        } else {
                            //                            AppUtils.showCommonToast(this, "No track found")
                            finish()
                        }
                    })
                } else {

                    // TODO : "source=sm_2" - New share Url , "hashids" - for older url
                    try {
                        if (data.toString().contains("source=sm_2")) {
                            val mIdLong = data.lastPathSegment!!
                            println("" + mIdLong)
                            if (mIdLong != "" && mIdLong.isNotEmpty()) {
                                val mAuth =
                                        RemoteConstant.getAuthTrackDetails(
                                                this@NowPlayingActivity,
                                                mIdLong
                                        )
                                GlobalScope.launch {
                                    nowPlayingViewModel.getTrackDetails(mAuth, mIdLong)
                                }
                            }
                        } else {
                            finish()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            nowPlayingViewModel.trackDetailsModel.nonNull()
                    .observe(this, androidx.lifecycle.Observer {
                        if (it?.payload != null && it.payload?.track != null
                        ) {
                            val dbPlayList: MutableList<TrackListCommon> = ArrayList()
                            dbPlayList.add(it.payload?.track!!)
                            nowPlayingViewModel.createDbPlayListModelNotification(
                                    dbPlayList, "Track suggestion"
                            )
                            val detailIntent = Intent(this, NowPlayingActivity::class.java)
                            detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                            detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                            startActivity(detailIntent)
                            overridePendingTransition(0, 0)
                            finish()
                            nowPlayingViewModel.trackDetailsModel.value = null
                        } else {
                            AppUtils.showCommonToast(this, "No track found")
                            finish()
                        }


                    })
        }

        // TODO : Add Equalizer Fragment ( DO not change )
        try {
//            val equalizerFragment = EqualizerFragment.newBuilder()
//                .setAccentColor(Color.parseColor("#4775e6"))
//                .setAudioSessionId(AudioApi.audioPlayer.audioSessionId)
//                .build()
//            supportFragmentManager.beginTransaction()
//                .replace(R.id.eqFrame, equalizerFragment)
//                .commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // TODO : Set default Repeat
        val playListNamePlayer = MyApplication.playlistManager?.currentItemChange?.currentItem?.playListName
                ?: ""

        if (!playListNamePlayer.contains("Music station")) {
            setPlayState(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mRepeatStatus, "0")
                            ?.toInt()
            )
        }

        // TODO : Repeat
        nowPlayingActivityBinding?.repeatButton?.setOnClickListener {
            var mRepeatMode =
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mRepeatStatus, "0")
                            ?.toInt()

            when (mRepeatMode) {
                0 -> mRepeatMode = 1
                1 -> mRepeatMode = 2
                2 -> mRepeatMode = 0
            }
            SharedPrefsUtils.setStringPreference(
                    this,
                    RemoteConstant.mRepeatStatus,
                    mRepeatMode.toString()
            )
            setPlayState(mRepeatMode)
        }


        nowPlayingViewModel.createDbPlayListModel?.nonNull()?.observe(
                this, androidx.lifecycle.Observer { playist ->
            if (playist != null && playist.isNotEmpty()) {
                newList = playist.toMutableList()

            }
        })

        // TODO : Shuffle
        val playListName = MyApplication.playlistManager?.currentItemChange?.currentItem?.playListName
                ?: ""
        if (!playListName.contains("Music station")) {
            setShuffleStateImage(
                    SharedPrefsUtils.getStringPreference(
                            this, RemoteConstant.mShuffleStatus, "false"
                    ), true
            )
        }

        // TODO : Music comments click

        nowPlayingActivityBinding?.commentListImageView?.setOnClickListener {
            if (playlistManager != null &&
                    playlistManager?.currentItem != null &&
                    playlistManager?.currentItem?.mExtraId != null
            ) {


                val mImage =
                        playlistManager?.currentItemChange?.currentItem?.thumbnailUrl.toString()

                val detailIntent = Intent(this, TrackCommentsActivity::class.java)
                if (mImage.contains("podcast")) {
                    detailIntent.putExtra(
                            RemoteConstant.mTrackId,
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.mExtraId
                    )
                    detailIntent.putExtra(RemoteConstant.mIsFrom, "fromPodcastTrack")
                } else {
                    detailIntent.putExtra(
                            RemoteConstant.mTrackId,
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.mExtraId
                    )
                    detailIntent.putExtra(RemoteConstant.mIsFrom, mIsFrom)
                }
                detailIntent.putExtra(RemoteConstant.mTrackName, mTrackName)
                detailIntent.putExtra(RemoteConstant.mTrackGenre, mTrackGenre)
                detailIntent.putExtra(RemoteConstant.mTrackPlayCount, mTrackPlayCount)
                detailIntent.putExtra(RemoteConstant.mTrackArtist, mTrackArtist)
                detailIntent.putExtra(RemoteConstant.mTrackImage, mTrackImage)




                if (playlistManager != null && playlistManager?.currentProgress != null
                        && playlistManager?.currentProgress?.position != null
                ) {
                    detailIntent.putExtra(
                            RemoteConstant.mDuration,
                            playlistManager?.currentProgress?.position.toString()
                    )
                }

                startActivity(detailIntent)
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
            }
        }

        // TODO: Shuffle button click
        nowPlayingActivityBinding?.shuffleButton?.setOnClickListener {
            if (SharedPrefsUtils.getStringPreference(
                            this,
                            RemoteConstant.mShuffleStatus,
                            "false"
                    ) == "true"
            ) {
                SharedPrefsUtils.setStringPreference(
                        this,
                        RemoteConstant.mShuffleStatus,
                        "false"
                )
                setShuffleStateImage("false", false)
            } else {
                SharedPrefsUtils.setStringPreference(
                        this,
                        RemoteConstant.mShuffleStatus,
                        "true"
                )
                setShuffleStateImage("true", false)
            }
        }

        // TODO : For track download
        try {
            val fetchConfiguration = FetchConfiguration.Builder(this)
                    .setDownloadConcurrentLimit(4)
                    .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                    .setNamespace(FETCH_NAMESPACE)
                    //.setNotificationManager(object : DefaultFetchNotificationManager(this) {
                    //override fun getFetchInstanceForNamespace(namespace: String): Fetch {
                    //return fetch!!
                    //}
                    //})
                    .build()
            fetch = Fetch.getInstance(fetchConfiguration)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        // TODO : Download offline track pending
        if (nowPlayingViewModel.downloadFalseTrackListModel != null) {
            nowPlayingViewModel.downloadFalseTrackListModel?.nonNull()
                    ?.observe(this, androidx.lifecycle.Observer { trackListCommonDbs ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            this.trackListCommonDbs = trackListCommonDbs
                        }
                    })
        }

        MobileAds.initialize(this)
        loadRewardedAd()

        if (SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.isFirstTimeInNowPlaying, "false"
                ) == "false"
        ) {
            Handler().postDelayed({
                SharedPrefsUtils.setStringPreference(
                        this, RemoteConstant.isFirstTimeInNowPlaying, "true"
                )
                tapTarget()
            }, 1000)
        }
    }

    private fun tapTarget() {
        val pennyRadius = 100F

        val likeMaterialButtonTarget = SimpleTarget.Builder(this@NowPlayingActivity)
                .setPoint(findViewById<View>(R.id.like_material_linear))
                .setShape(Circle(pennyRadius))
                .setTitle("Like")
                .setDescription("Show how much you liked this track, \nby hitting the like button.")
                .setOverlayPoint(50f, 200f)
                .build()
        val shuffleButtonTarget = SimpleTarget.Builder(this@NowPlayingActivity)
                .setPoint(findViewById<ImageView>(R.id.shuffle_button))
                .setShape(Circle(pennyRadius))
                .setTitle("Shuffle")
                .setDescription("Surprise yourself by clicking on Shuffle; \nthe next song you hear will be randomly selected.")
                .setOverlayPoint(50f, 200f)
                .build()
        val repeatButtonTarget = SimpleTarget.Builder(this@NowPlayingActivity)
                .setPoint(findViewById<ImageView>(R.id.repeat_button))
                .setShape(Circle(pennyRadius))
                .setTitle("Repeat")
                .setDescription("Listen to this song on loop \nby hitting on repeat.")
                .setOverlayPoint(50f, 200f)
                .build()
        val commentListImageViewTarget = SimpleTarget.Builder(this@NowPlayingActivity)
                .setPoint(findViewById<ImageView>(R.id.comment_list_image_view))
                .setShape(Circle(pennyRadius))
                .setTitle("Comment")
                .setDescription("Post a comment and let others know \nhow this track made you feel.")
                .setOverlayPoint(50f, 200f)
                .build()


        // create spotlight
        Spotlight.with(this)
                .setOverlayColor(R.color.background_shade)
                .setDuration(1000L)
                .setAnimation(DecelerateInterpolator(50f))
                .setTargets(
                        likeMaterialButtonTarget,
                        shuffleButtonTarget,
                        repeatButtonTarget,
                        commentListImageViewTarget
                )
                .setClosedOnTouchedOutside(true)
                .setOnSpotlightStateListener(object : OnSpotlightStateChangedListener {
                    override fun onStarted() {
                    }

                    override fun onEnded() {

                    }
                })
                .start()

    }


    private var isRewardEarned = false
    private fun showRewardedVideo(isFromNext: Boolean) {
//        show_video_button.visibility = View.INVISIBLE
        if (mRewardedAd.isLoaded) {
            mClickCount = 0
            mRewardedAd.show(
                    this,
                    object : RewardedAdCallback() {
                        override fun onUserEarnedReward(
                                rewardItem: RewardItem
                        ) {
//                        Toast.makeText(
//                            this@NowPlayingActivity,
//                            "onUserEarnedReward",
//                            Toast.LENGTH_LONG
//                        ).show()
//                        addCoins(rewardItem.amount)

                            isRewardEarned = true

                        }

                        override fun onRewardedAdClosed() {
                            isRewardEarned = if (!isRewardEarned) {
                                if (!isFromNext) {
                                    Toast.makeText(
                                            this@NowPlayingActivity,
                                            "Finish the ad to download the track",
                                            Toast.LENGTH_LONG
                                    ).show()
                                }
                                false
                            } else {
                                if (!isFromNext) {
                                    startDownload()
                                }
                                false
                            }
                            loadRewardedAd()
                        }

                        override fun onRewardedAdFailedToShow(errorCode: Int) {
                            Toast.makeText(
                                    this@NowPlayingActivity,
                                    "something went wrong please try again",
                                    Toast.LENGTH_LONG
                            ).show()
                        }

                        override fun onRewardedAdOpened() {
//                        Toast.makeText(
//                            this@NowPlayingActivity,
//                            "onRewardedAdOpened",
//                            Toast.LENGTH_LONG
//                        ).show()
                        }
                    }
            )
        } else {
            if (!isFromNext) {
                startDownload()
            }
        }
    }

    private fun startDownload() {

        try {
            val itemChange = playlistManager?.currentItemChange?.currentItem
            val trackListCommonDbModel = TrackListCommonDb(
                    itemChange?.mExtraId!!.toString(),
                    itemChange.id.toString(),
                    itemChange.title!!,
                    itemChange.thumbnailUrl!!,
                    itemChange.mediaUrl!!,
                    itemChange.artist!!,
                    itemChange.album!!,
                    "false", "false"
            )
            downloadCommon(trackListCommonDbModel)

        } catch (e: Exception) {
            Toast.makeText(
                    this@NowPlayingActivity,
                    "Error occurred please try again later",
                    Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun downloadCommon(trackListCommonDbModel: TrackListCommonDb) {

        nowPlayingViewModel.addToDownloadNowPlaying(trackListCommonDbModel)

        Toast.makeText(this, "Track added to queue", Toast.LENGTH_SHORT).show()


        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            enqueueDownloads()
        }, 1000)
    }

    private var mIsLoading = false
    private lateinit var mRewardedAd: RewardedAd

    private fun loadRewardedAd() {


        if (!(::mRewardedAd.isInitialized) || !mRewardedAd.isLoaded) {
            mIsLoading = true


            mRewardedAd = RewardedAd(this, getString(R.string.ad_unit_id_reward))
            mRewardedAd.loadAd(
                    AdRequest.Builder()
                            .addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0")
                            .build(),
                    object : RewardedAdLoadCallback() {
                        override fun onRewardedAdLoaded() {
                            mIsLoading = false
                        }

                        override fun onRewardedAdFailedToLoad(errorCode: Int) {
                            mIsLoading = false
                        }
                    }
            )
        }
    }


    /**
     * TODO : Change shuffle image
     *
     * Doing Shuffle things ( if track is playing adding playing track as first track)
     */

    private fun setShuffleStateImage(mShuffle: String?, isLaunch: Boolean) {
        try {
            if (!isLaunch) {
                if (mShuffle != null && mShuffle != "" && mShuffle == "true") {
                    if (newList != null && newList?.isNotEmpty()!!) {

                        //TODO : if track is playing adding playing track as first track

                        if (selectedPosition != -1 && newList?.get(selectedPosition) != null && newList?.get(
                                        selectedPosition
                                )?.TrackName != null
                        ) {
                            val mSelectedSong: PlayListDataClass = newList?.get(selectedPosition)!!
                            val mGlobalFirstList: MutableList<PlayListDataClass> = ArrayList()
                            val mGlobalListNew: MutableList<PlayListDataClass> = ArrayList()
                            mGlobalListNew.addAll(newList!!)

                            //TODO : if track is playing adding playing track as first track
                            mGlobalListNew.removeAt(selectedPosition)
                            mGlobalListNew.shuffle()

                            mGlobalFirstList.clear()
                            mGlobalFirstList.add(0, mSelectedSong)
                            mGlobalFirstList.addAll(mGlobalListNew)
                            selectedPosition = 0

                            mediaItems.clear()
                            for (i in mGlobalFirstList.indices) {
                                val mediaItem = MediaItem(mGlobalFirstList.get(i), true)
                                mediaItems.add(mediaItem)
                            }
                            playlistManager?.setParameters(mediaItems, selectedPosition)
                            playlistManager?.id = PLAYLIST_ID.toLong()
                            mGlobalList = mGlobalFirstList
                        }

                    }
                    nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                            ContextCompat.getColor(
                                    this,
                                    R.color.colorPrimary
                            )
                    )
                } else {
                    if (newList != null && newList?.isNotEmpty()!!) {
                        val mGlobalFirstList = newList?.toMutableList()

                        mediaItems.clear()

                        val mTrackId =
                                playlistManager?.currentItemChange?.currentItem?.id.toString()
                        for (i in mGlobalFirstList?.indices!!) {

                            if (mTrackId == mGlobalFirstList[i].JukeBoxTrackId) {
                                selectedPosition = i
                            }

                            val mediaItem = MediaItem(mGlobalFirstList[i], true)
                            mediaItems.add(mediaItem)
                        }
                        playlistManager?.setParameters(mediaItems, selectedPosition)
                        playlistManager?.id = PLAYLIST_ID.toLong()
                        mGlobalList = mGlobalFirstList

                    }
                    nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                            ContextCompat.getColor(
                                    this,
                                    R.color.white
                            )
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * TODO : Change repeat mode and drawable
     */
    private fun setPlayState(mRepeatMode: Int?) {
        when (mRepeatMode) {
            RemoteConstant.REPEAT_MODE_NONE -> {
                nowPlayingActivityBinding?.repeatButton?.setImageResource(R.drawable.repeat)
                nowPlayingActivityBinding?.repeatButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.white
                        )
                )
            }

            RemoteConstant.REPEAT_MODE_THIS -> {
                nowPlayingActivityBinding?.repeatButton?.setImageResource(R.drawable.repeat_one)
                nowPlayingActivityBinding?.repeatButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.colorPrimary
                        )
                )
            }
            RemoteConstant.REPEAT_MODE_ALL -> {
                nowPlayingActivityBinding?.repeatButton?.setImageResource(R.drawable.repeat_all)
                nowPlayingActivityBinding?.repeatButton?.setColorFilter(
                        ContextCompat.getColor(this, R.color.colorPrimary)
                )
            }
        }
    }


    /**
     * Retrieves the extra associated with the selected playlist index
     * so that we can start playing the correct item.
     */
    private fun retrieveExtras() {
        val extras: Bundle
        if (intent.extras != null) {
            extras = intent?.extras!!

            if (extras.containsKey(RemoteConstant.mExtraIndex)) {
                selectedPosition = extras.getInt(RemoteConstant.mExtraIndex, 0)
                if (selectedPosition != -1) {
                    playlistManager?.currentPosition = selectedPosition
                }
            }
            if (extras.containsKey(RemoteConstant.extraIndexImage)) {
                mImage = extras.getString(RemoteConstant.extraIndexImage, "")
            }
            if (extras.containsKey(RemoteConstant.mNeedToFilter)) {
                mTitle = extras.getString(RemoteConstant.mNeedToFilter, "")
            }
            if (extras.containsKey(RemoteConstant.extraIndexIsFrom)) {
                mIsFrom = extras.getString(RemoteConstant.extraIndexIsFrom, "")
            }
            if (extras.containsKey(RemoteConstant.extraIsFromRadio)) {
                extraIsFromRadio = extras.getBoolean(RemoteConstant.extraIsFromRadio, false)
            }
            SharedPrefsUtils.setStringPreference(
                    this@NowPlayingActivity,
                    AppUtils.SELECTED_FILTER,
                    mTitle
            )
            if (mImage.contains("podcast") || mImage == "") {
                nowPlayingActivityBinding?.musicOptionLinear?.visibility = View.VISIBLE
                nowPlayingActivityBinding?.trackGenreTextView?.visibility = View.GONE
                nowPlayingActivityBinding?.trackArtistTextView?.visibility = View.VISIBLE
                nowPlayingActivityBinding?.shuffleButton?.isEnabled = false
                nowPlayingActivityBinding?.repeatButton?.isEnabled = false
                nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.disable_color
                        )
                )
                nowPlayingActivityBinding?.repeatButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.disable_color
                        )
                )
            } else {
                nowPlayingActivityBinding?.musicOptionLinear?.visibility = View.VISIBLE
                nowPlayingActivityBinding?.trackGenreTextView?.visibility =
                        View.VISIBLE
                nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                        ContextCompat.getColor(
                                this,
                                R.color.white
                        )
                )
                setPlayState(
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mRepeatStatus, "0"
                        )?.toInt()
                )
                nowPlayingActivityBinding?.shuffleButton?.isEnabled = true
                nowPlayingActivityBinding?.repeatButton?.isEnabled = true
                // TODO : Shuffle
                if (SharedPrefsUtils.getStringPreference(
                                this, RemoteConstant.mShuffleStatus, "false"
                        ) == "true"
                ) {
                    setShuffleStateImage("true", false)
                }
            }


            init()
        }

    }


    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
        playlistManager?.unRegisterProgressListener(this)

    }

    override fun onResume() {
        super.onResume()
        isPlayed = true


        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)
        playlistManager?.registerProgressListener(this)
        updateCurrentPlaybackInformation()


        // TODO : Change if like updated from
        if (MyApplication.playlistManager != null &&
                MyApplication.playlistManager?.currentItemChange != null &&
                MyApplication.playlistManager?.currentItemChange?.currentItem != null &&
                MyApplication.playlistManager?.currentItemChange?.currentItem?.isFavorite != null
        ) {
            val isLiked =
                    MyApplication.playlistManager?.currentItemChange?.currentItem?.isFavorite!!
            if (isLiked == "true") {
                nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)
            } else {
                nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_grey)
            }
        }

    }

    override fun onPlaylistItemChanged(
            currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        shouldSetDuration = true

        if (currentItem?.playListName != null) {
            nowPlayingActivityBinding?.stationNameView?.text = currentItem.playListName
        }
        //Updates the button states
        // Disable next prev in music station
        val playListName = currentItem?.playListName ?: ""
        if (playListName.contains("Music station")) {
            nowPlayingActivityBinding?.audioPlayerNext?.isEnabled = false
            nowPlayingActivityBinding?.audioPlayerPrevious?.isEnabled = false
            DrawableCompat.setTint(
                    nowPlayingActivityBinding?.audioPlayerPrevious?.drawable!!,
                    ContextCompat.getColor(this, R.color.light_grey)
            )
            DrawableCompat.setTint(
                    nowPlayingActivityBinding?.audioPlayerNext?.drawable!!,
                    ContextCompat.getColor(this, R.color.light_grey)
            )
            DrawableCompat.setTint(
                    nowPlayingActivityBinding?.queueListImageView?.drawable!!,
                    ContextCompat.getColor(this, R.color.light_grey)
            )
            // To disable shuffle and repeat
            nowPlayingActivityBinding?.shuffleButton?.visibility = View.GONE
            nowPlayingActivityBinding?.repeatButton?.visibility = View.GONE
            nowPlayingActivityBinding?.commentListImageView?.visibility = View.GONE
            nowPlayingActivityBinding?.queueListImageView?.isEnabled = false
        } else {
            nowPlayingActivityBinding?.audioPlayerNext?.isEnabled = hasNext
            nowPlayingActivityBinding?.audioPlayerPrevious?.isEnabled = hasPrevious

            if (!hasPrevious) {
                DrawableCompat.setTint(
                        nowPlayingActivityBinding?.audioPlayerPrevious?.drawable!!,
                        ContextCompat.getColor(this, R.color.light_grey)
                )
            } else {
                DrawableCompat.setTint(
                        nowPlayingActivityBinding?.audioPlayerPrevious?.drawable!!,
                        ContextCompat.getColor(this, R.color.white)
                )
            }
            if (!hasNext) {
                DrawableCompat.setTint(
                        nowPlayingActivityBinding?.audioPlayerNext?.drawable!!,
                        ContextCompat.getColor(this, R.color.light_grey)
                )
            } else {
                DrawableCompat.setTint(
                        nowPlayingActivityBinding?.audioPlayerNext?.drawable!!,
                        ContextCompat.getColor(this, R.color.white)
                )
            }
        }

        if (currentItem != null) {//Loads the new image

            val isLiked = currentItem.isFavorite
            if (isLiked == "true") {
                nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)
            } else {
                nowPlayingActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_grey)
            }

            if (currentItem.thumbnailUrl != null) {
                if (InternetUtil.isInternetOn()) {
                    glideRequestManager.load(
                            RemoteConstant.getImageUrlFromWidth(
                                    currentItem.thumbnailUrl,
                                    RemoteConstant.getWidth(this) / 2
                            )
                    )
                            .apply(
                                    RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .priority(Priority.HIGH)
                            ).into(nowPlayingActivityBinding?.audioPlayerImage!!)
                    glideRequestManager.load(
                            RemoteConstant.getImageUrlFromWidth(
                                    currentItem.thumbnailUrl,
                                    200
                            )
                    ).thumbnail(0.1f)
                            .into(nowPlayingActivityBinding?.baseImage!!)
                } else {

                    // TODO : For offline tracks
                    glideRequestManager.load(
                            currentItem.thumbnailUrl
                    )
                            .apply(
                                    RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .priority(Priority.HIGH)
                            ).into(nowPlayingActivityBinding?.audioPlayerImage!!)
                    glideRequestManager.load(
                            currentItem.thumbnailUrl
                    ).thumbnail(0.1f)
                            .into(nowPlayingActivityBinding?.baseImage!!)
                }

                // Updates the title, subtitle, and description
                if (currentItem.title != null) {
                    nowPlayingActivityBinding?.trackTitleTextView?.text = currentItem.title
                }
                if (currentItem.artist != null) {
                    nowPlayingActivityBinding?.trackArtistTextView?.text = currentItem.artist

                    //TODO : Artist click
                    nowPlayingActivityBinding?.trackArtistTextView?.setOnClickListener {
                        if (playlistManager != null &&
                                playlistManager?.currentItemChange != null &&
                                playlistManager?.currentItemChange?.currentItem != null &&
                                playlistManager?.currentItemChange?.currentItem?.artistId != null &&
                                playlistManager?.currentItemChange?.currentItem?.artistId != ""
                        ) {
                            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
                            detailIntent.putExtra(
                                    "mArtistId",
                                    MyApplication.playlistManager?.currentItemChange?.currentItem?.artistId!!
                            )
                            startActivity(detailIntent)
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
                        }
                    }

                }
                var mAlbumName = ""
                if (currentItem.album != null && currentItem.album != "") {
                    mAlbumName = currentItem.album!! + " - "
                }
                if (currentItem.playCount != "" && currentItem.playCount != "null") {
                    if (currentItem.playCount != "0") {
                        nowPlayingActivityBinding?.trackGenreTextView?.text =
                                mAlbumName.plus(currentItem.playCount + " Plays")
                        nowPlayingActivityBinding?.trackGenreTextView?.visibility =
                                View.VISIBLE
                    } else {
                        nowPlayingActivityBinding?.trackGenreTextView?.text = currentItem.album
                        nowPlayingActivityBinding?.trackGenreTextView?.visibility =
                                View.VISIBLE
                    }
                } else {
                    nowPlayingActivityBinding?.trackGenreTextView?.text = currentItem.album
                    nowPlayingActivityBinding?.trackGenreTextView?.visibility =
                            View.VISIBLE
                }

                if (currentItem.album != null) {
                    if (!currentItem.thumbnailUrl?.contains("podcast")!! ||
                            playListName.contains("Music station")) {
//                        track_category_text_view!!.text = currentItem.album

                        nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                                ContextCompat.getColor(
                                        this,
                                        R.color.white
                                )
                        )
                        setPlayState(
                                SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mRepeatStatus, "0"
                                )?.toInt()
                        )

                        nowPlayingActivityBinding?.shuffleButton?.isEnabled = true
                        nowPlayingActivityBinding?.repeatButton?.isEnabled = true

                        // TODO : Shuffle
                        if (SharedPrefsUtils.getStringPreference(
                                        this, RemoteConstant.mShuffleStatus, "false"
                                ) == "true"
                        ) {
                            setShuffleStateImage("true", false)
                        }
                        nowPlayingActivityBinding?.musicOptionLinear?.visibility =
                                View.VISIBLE
                    } else {
                        //TODO: Podcast
                        nowPlayingActivityBinding?.trackArtistTextView?.visibility = View.GONE
                        nowPlayingActivityBinding?.musicOptionLinear?.visibility =
                                View.VISIBLE
                        nowPlayingActivityBinding?.shuffleButton?.isEnabled = false
                        nowPlayingActivityBinding?.repeatButton?.isEnabled = false
                        nowPlayingActivityBinding?.shuffleButton?.setColorFilter(
                                ContextCompat.getColor(
                                        this,
                                        R.color.disable_color
                                )
                        )
                        nowPlayingActivityBinding?.repeatButton?.setColorFilter(
                                ContextCompat.getColor(
                                        this,
                                        R.color.disable_color
                                )
                        )
                    }


                }

                //TODO : Pass Track Id
                if (currentItem.id != 0.toLong()) {
                    mTrackId = currentItem.id.toString()
                }
                if (currentItem.title != null) {
                    mTrackName = currentItem.title!!
                }
                if (currentItem.album != null) {
                    mTrackGenre = currentItem.album!!
                }
                if (currentItem.artist != null) {
                    mTrackArtist = currentItem.artist!!
                }
                mTrackPlayCount = "0"
                mTrackImage =
                        RemoteConstant.getImageUrlFromWidth(currentItem.thumbnailUrl, 200)
                // TODO : Updating now playing
//                EventBus.getDefault().post(UpdateTrackPlayingEvent(mTrackId))

                // TODO : For offline track disable play count
                if (currentItem.mediaUrl?.contains("/storage/emulated/")!!) {
                    nowPlayingActivityBinding?.musicOptionLinear?.visibility = View.GONE
                    nowPlayingActivityBinding?.trackGenreTextView?.visibility =
                            View.GONE
                    nowPlayingActivityBinding?.nowPlayingOptionsImageView?.setImageResource(R.drawable.ic_options_grey)
                    nowPlayingActivityBinding?.nowPlayingOptionsImageView?.setOnClickListener {

                    }
                }
            }
        }

        return true
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.STOPPED -> {
                EventBus.getDefault().post(NotificationSwipeClose())
                finish()
            }
            PlaybackState.RETRIEVING, PlaybackState.PREPARING, PlaybackState.SEEKING -> restartLoading()
            PlaybackState.PLAYING -> {

                doneLoading(true)
            }
            PlaybackState.PAUSED -> {
                doneLoading(false)
            }
            else -> {
            }
        }

        return true
    }

    override fun onProgressUpdated(mediaProgress: MediaProgress): Boolean {
        if (shouldSetDuration && mediaProgress.duration > 0) {
            shouldSetDuration = false
            setDuration(mediaProgress.duration)
        }

        if (!userInteracting) {
            nowPlayingActivityBinding?.audioPlayerSeek?.secondaryProgress =
                    (mediaProgress.duration * mediaProgress.bufferPercentFloat).toInt()
            nowPlayingActivityBinding?.audioPlayerSeek?.progress = mediaProgress.position.toInt()
            nowPlayingActivityBinding?.audioPlayerPosition?.text =
                    formatMs(
                            mediaProgress.position
                    )
        }

        return true
    }

    /**
     * Makes sure to update the UI to the current playback item.
     */
    private fun updateCurrentPlaybackInformation() {
        val itemChange = playlistManager?.currentItemChange
        if (itemChange != null) {
            onPlaylistItemChanged(
                    itemChange.currentItem,
                    itemChange.hasNext,
                    itemChange.hasPrevious
            )
        }

        // TODO : Added on update ( Music play from listing - Not naviagte to NowPlaying page)
//        playlistManager?.currentPosition = itemChange?.currentItem

        val currentPlaybackState = playlistManager?.currentPlaybackState

        if (currentPlaybackState !== PlaybackState.STOPPED) {
            onPlaybackStateChanged(currentPlaybackState!!)

        }

        val mediaProgress = playlistManager?.currentProgress
        if (mediaProgress != null) {
            onProgressUpdated(mediaProgress)
        }

//        TODO : APP Resume state
//        if (currentPlaybackState == PlaybackState.STOPPED) {
//            finish()
//        }
    }


    private fun init() {

        setupListeners()
        if (mImage != "") {
            Handler().postDelayed({
                glideRequestManager.load(mImage).thumbnail(0.1f).into(nowPlayingActivityBinding?.audioPlayerImage!!)
                glideRequestManager.load(mImage).thumbnail(0.1f).into(nowPlayingActivityBinding?.baseImage!!)
            }, 100)
        }

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            nowPlayingViewModel.isInternetAvailable = true
            nowPlayingViewModel.isInternetAvailableLayout = false
            checkInternet()
        } else {
            nowPlayingViewModel.isInternetAvailable = false
            nowPlayingViewModel.isInternetAvailableLayout = true
            addPlayList()
        }



        nowPlayingActivityBinding?.queueListImageView?.setOnClickListener {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {

                val detailIntent = Intent(this, QueueListActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, selectedPosition)
                detailIntent.putExtra(RemoteConstant.mNeedToFilter, mTitle)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImage)
                detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, mIsFrom)
                startActivity(detailIntent)
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
            }
            mLastClickTime = SystemClock.elapsedRealtime()

        }


    }

    private fun checkInternet() {
        InternetUtil.observe(this, {
            if (it == true) {
                if (!isLoaded) {
                    addPlayList()
                }
                nowPlayingViewModel.isInternetAvailable = false
                nowPlayingViewModel.isInternetAvailableLayout = true
            } else {
                nowPlayingViewModel.isInternetAvailable = true
                nowPlayingViewModel.isInternetAvailableLayout = false
                AppUtils.showCommonToast(this, "No Internet connection")
            }
        })
    }

    @SuppressLint("RestrictedApi")
    private fun addPlayList() {
        // TODO : To check playlist already added
        setupPlaylistManagerNew()

        //TODO: Disable add to playlist
        if (mIsFrom == "RadioPodCast" || mIsFrom == "RadioRecommended" || mIsFrom == "QuickPlayList")
            nowPlayingActivityBinding?.fabAddToPlayList?.visibility = View.GONE
    }

    private fun inItElementTransition() {
        val bounds = ChangeBounds()
        bounds.duration = 300
        window.sharedElementEnterTransition = bounds

        //add transition listener to load full-size image on transition end
        window.sharedElementEnterTransition.addListener(object :
                Transition.TransitionListener {
            override fun onTransitionEnd(transition: Transition?) {
                //load full-size
                transition?.removeListener(this)
            }

            override fun onTransitionResume(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTransitionPause(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTransitionCancel(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTransitionStart(transition: Transition?) {
                //To change body of created functions use File | Settings | File Templates.

//
                //TODO: Disable add to playlist
                if (mImage.contains("podcast"))
                    nowPlayingActivityBinding?.fabAddToPlayList?.hide()

            }
        })
    }

    /**
     * Called when we receive a notification that the current item is
     * done loading.  This will then update the view visibilities and
     * states accordingly.
     *
     * @param isPlaying True if the audio item is currently playing
     */
    private fun doneLoading(isPlaying: Boolean) {
        // TODO : Adding playing category to check already added
        if (mIsFrom == "Genre" || mIsFrom == "RadioPodCast") {

            /**
             * One genre and radio podcast we have multiple categories
             */
            SharedPrefsUtils.setStringPreference(
                    this@NowPlayingActivity,
                    AppUtils.SELECTED_FILTER,
                    mTitle
            )
            SharedPrefsUtils.setStringPreference(
                    this@NowPlayingActivity,
                    AppUtils.SELECTED_CATEGORY,
                    mIsFrom
            )
            // TODO : To Handle Navigation Drawer play UI
            SharedPrefsUtils.setStringPreference(
                    this@NowPlayingActivity,
                    AppUtils.SELECTED_IS_FROM_PODCAST_OR_GENRE,
                    mIsFrom
            )

        } else {
            SharedPrefsUtils.setStringPreference(
                    this@NowPlayingActivity,
                    AppUtils.SELECTED_CATEGORY,
                    mIsFrom
            )
        }


        SharedPrefsUtils.setStringPreference(
                this@NowPlayingActivity,
                AppUtils.SELECTED_CATEGORY,
                mIsFrom
        )
        loadCompleted()
        updatePlayPauseImage(isPlaying)
    }

    /**
     * Updates the Play/Pause image to represent the correct playback state
     *
     * @param isPlaying True if the audio item is currently playing
     */
    private fun updatePlayPauseImage(isPlaying: Boolean) {
        if (isPlaying) {
            nowPlayingActivityBinding?.audioPlayerPlayPause?.setImageResource(R.drawable.ic_baseline_pause)
        } else {
            nowPlayingActivityBinding?.audioPlayerPlayPause?.setImageResource(R.drawable.ic_baseline_play)
        }
    }

    /**
     * Used to inform the controls to finalize their setup.  This
     * means replacing the loading animation with the PlayPause button
     */
    private fun loadCompleted() {
        nowPlayingActivityBinding?.audioPlayerPlayPause?.visibility = View.VISIBLE
        nowPlayingActivityBinding?.audioPlayerPrevious?.visibility = View.VISIBLE
        nowPlayingActivityBinding?.audioPlayerNext?.visibility = View.VISIBLE

        nowPlayingActivityBinding?.audioPlayerLoading?.visibility = View.INVISIBLE
        nowPlayingActivityBinding?.audioPlayerSeek?.visibility = View.VISIBLE

    }

    /**
     * Used to inform the controls to return to the loading stage.
     * This is the opposite of [.loadCompleted]
     */
    private fun restartLoading() {
        nowPlayingActivityBinding?.audioPlayerPlayPause?.visibility = View.INVISIBLE
        nowPlayingActivityBinding?.audioPlayerPrevious?.visibility = View.INVISIBLE
        nowPlayingActivityBinding?.audioPlayerNext?.visibility = View.INVISIBLE

        nowPlayingActivityBinding?.audioPlayerLoading?.visibility = View.VISIBLE
        nowPlayingActivityBinding?.audioPlayerSeek?.visibility = View.INVISIBLE
    }

    /**
     * Sets the [.seekBar]s max and updates the duration text
     *
     * @param duration The duration of the media item in milliseconds
     */
    private fun setDuration(duration: Long) {
        nowPlayingActivityBinding?.audioPlayerSeek?.max = duration.toInt()
        nowPlayingActivityBinding?.audioPlayerDuration?.text =
                formatMs(
                        duration
                )
    }


    private fun setupPlaylistManagerNew() {
        playlistManager = MyApplication.playlistManager

        when (mIsFrom) {
            "dblistUserProfile" -> playTrack(true)
            "dblistMyFavTrack" -> playTrack(true)
            else -> {
//                playTrack(false)
            }
        }

    }

    private fun playTrack(needToPlay: Boolean) {

        nowPlayingViewModel.createDbPlayListModel?.nonNull()?.observe(
                this, androidx.lifecycle.Observer {
            for (sample in it!!) {
                val mediaItem = MediaItem(sample, true)
                mediaItems.add(mediaItem)
            }
            playlistManager?.setParameters(mediaItems, selectedPosition)
            playlistManager?.id = PLAYLIST_ID.toLong()
            mGlobalList = it as MutableList<PlayListDataClass>
            startPlayback(needToPlay)
        })
    }


    /**
     * Links the SeekBarChanged to the [.seekBar] and
     * onClickListeners to the media buttons that call the appropriate
     * invoke methods in the [.playlistManager]
     */
    private fun setupListeners() {
        nowPlayingActivityBinding?.audioPlayerSeek?.setOnSeekBarChangeListener(SeekBarChanged())
        nowPlayingActivityBinding?.audioPlayerPrevious?.setOnClickListener {
            if (mClickCount == 2) {
//                mClickCount = 0
                showRewardedVideo(true)
            } else {
                mClickCount = mClickCount.plus(1)
            }


            // TODO : Check if current position is more than 7-sec ( 7000 millisecond ) seek to zero
            try {
                val mediaProgressLong = playlistManager?.currentProgress?.position!!
                if (mediaProgressLong > 7000) {
                    playlistManager?.play(0, false)

                } else {


                    // TODO : If repeat enabled - disable
                    if (SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mRepeatStatus, "0"
                            ) == "2"
                    ) {
                        SharedPrefsUtils.setStringPreference(
                                this, RemoteConstant.mRepeatStatus, "0"
                        )
                    }
                    setPlayState(
                            SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mRepeatStatus, "0"
                            )?.toInt()
                    )
                    playlistManager?.invokePrevious()
                }
            } catch (e: Exception) {
                playlistManager?.invokePrevious()
                e.printStackTrace()
            }
        }

        nowPlayingActivityBinding?.audioPlayerPlayPause?.setOnClickListener {
            val currentPlaybackState = playlistManager?.currentPlaybackState
            if (currentPlaybackState == PlaybackState.STOPPED) {
                addPlayList()
            } else {
                playlistManager?.invokePausePlay()
            }
        }

        nowPlayingActivityBinding?.audioPlayerNext?.setOnClickListener {


            if (mClickCount == 2) {
                showRewardedVideo(true)
            } else {
                mClickCount = mClickCount.plus(1)
            }
            // TODO : If repeat enabled - disable
            if (SharedPrefsUtils.getStringPreference(
                            this, RemoteConstant.mRepeatStatus, "0"
                    ) == "2"
            ) {
                SharedPrefsUtils.setStringPreference(
                        this, RemoteConstant.mRepeatStatus, "0"
                )

            }

            setPlayState(
                    SharedPrefsUtils.getStringPreference(
                            this, RemoteConstant.mRepeatStatus, "0"
                    )?.toInt()
            )

            playlistManager?.invokeNext()

        }
    }

    /**
     * Starts the audio playback if necessary.
     *
     * @param forceStart True if the audio should be started from the beginning even if it is currently playing
     */
    private fun startPlayback(forceStart: Boolean) {

        //TODO : When track play (Radio need to be paused)
        pauseRadio()

        //If we are changing audio files, or we haven't played before then start the playback
        ///TODO : if playing Radio podcast seek to  millisecond
        if (forceStart || playlistManager?.currentPosition != selectedPosition) {
            playlistManager?.currentPosition = selectedPosition
            if ((mGlobalList != null && mGlobalList?.isNotEmpty()!! && mGlobalList?.size!! > 0) &&
                    selectedPosition != -1 && mGlobalList?.size!! >= selectedPosition &&
                    mGlobalList?.get(selectedPosition) != null &&
                    mGlobalList?.get(selectedPosition)?.Millisecond != null && mGlobalList!![selectedPosition].Millisecond != ""
            ) {
                if (Integer.parseInt(mGlobalList!![selectedPosition].percentage) < 96) {
                    if (mGlobalList?.get(selectedPosition)?.Millisecond != null && mGlobalList?.get(
                                    selectedPosition
                            )?.Millisecond != ""
                    ) {
                        try {
                            playlistManager?.play(
                                    mGlobalList!![selectedPosition].Millisecond.toLong(),
                                    false
                            )
                        } catch (e: Exception) {
                            playlistManager?.play(0, false)
                        }
                    } else {
                        playlistManager?.play(0, false)
                    }
                } else {
                    // TODO : If radio play percentage <96 set percentage as 100 ( Track analytics )
                    doAsync {
                        if (mGlobalList != null &&
                                mGlobalList?.get(playlistManager?.currentPosition!!) != null &&
                                mGlobalList?.get(playlistManager?.currentPosition!!)?.JukeBoxTrackId != null
                        ) {
                            val podcastProgress: MutableList<PodCastProgress> =
                                    java.util.ArrayList()
                            val mAnalytics = PodCastProgress(
                                    mGlobalList!![playlistManager?.currentPosition!!].JukeBoxTrackId,
                                    "100",
                                    "0"
                            )
                            podcastProgress.add(mAnalytics)
                            DefaultPlaylistHandler.dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                    podcastProgress
                            )
                        }

                    }
                    playlistManager?.play(0, false)
                }
            } else {
                playlistManager?.play(0, false)
            }
        }

    }

    /**
     * Listens to the seek bar change events and correctly handles the changes
     */
    private inner class SeekBarChanged : SeekBar.OnSeekBarChangeListener {
        private var seekPosition = -1

        override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
        ) {
            if (!fromUser) {
                return
            }

            seekPosition = progress
            nowPlayingActivityBinding?.audioPlayerPosition?.text =
                    formatMs(
                            progress.toLong()
                    )
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {
            userInteracting = true
            seekPosition = seekBar.progress
            playlistManager?.invokeSeekStarted()
        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {
            userInteracting = false
            playlistManager?.invokeSeekEnded(seekPosition.toLong())
            seekPosition = -1
        }
    }

    /**
     * Add track to playlist API
     */
    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {

        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                        mPlayListId + RemoteConstant.mSlash + mTrackId,
                RemoteConstant.putMethod
        )
        val mAuth: String =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mAppId,
                        ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        nowPlayingViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View =
                inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val createPlayListTextView: TextView =
                dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
                dialogView.findViewById(R.id.play_list_name_edit_text)

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        val switchButtonPlayList: SwitchButton =
                dialogView.findViewById(R.id.switch_button_play_list)
        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                            && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                                "0", playListNameEditText.text.toString(),
                                isPrivate,
                                mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }
                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }

    }

    /**
     * Create playlist API
     */
    private fun createPlayListApi(
            playListId: String, playListName: String, private: String, icon_id: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody =
                    CreatePlayListBody(playListId, playListName, icon_id, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            nowPlayingViewModel.createPlayList(mAuth, createPlayListBody)
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("isPlayed", isPlayed)
        setResult(1051, intent)
        super.onBackPressed()
        overridePendingTransition(
                0,
                R.anim.play_panel_close_background
        )
        AppUtils.hideKeyboard(
                this@NowPlayingActivity,
                nowPlayingActivityBinding?.nowPlayingBackImageView
        )
        if (mIsFrom == "dblistMusicGrid") {
            this.finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }
                    if (playListImageView != null) {
                        glideRequestManager
                                .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                                .into(playListImageView!!)
                    }
                }
            }
        }
    }

    private fun pauseRadio() {
        if (MyApplication.playlistManagerRadio != null &&
                MyApplication.playlistManagerRadio?.playlistHandler != null &&
                MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer != null &&
                MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying != null
        ) {
            if (MyApplication.playlistManagerRadio?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                MyApplication.playlistManagerRadio?.invokePausePlay()
            }
        }
    }

    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size!! > 0) {
            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }
    }
}
