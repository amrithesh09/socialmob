package com.example.sample.socialmob.view.ui.home_module.music

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.QueueListActivityBinding
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class QueueListActivity : BaseCommonActivity(), QueueListAdapter.QueueItemClickListener,
    PlaylistListener<MediaItem> {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mIsFrom: String = ""
    private var mTitle: String = ""
    private var mImage: String = ""
    private var selectedPosition = 0
    private var mPlayPosition = -1
    private var mQueueListAdapter: QueueListAdapter? = null
    private var queueListActivityBinding: QueueListActivityBinding? = null

    private var mPlayListManager: PlaylistManager? = null
    private var items: MutableList<MediaItem>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        queueListActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.queue_list_activity)

        queueListActivityBinding?.queueBackImageView?.setOnClickListener {
            onBackPressed()
        }

        observeData()
    }
    /**
     * get extras from intent
     */
    private fun observeData() {
        intent?.let {
            val extras: Bundle = intent.extras!!
            selectedPosition = extras.getInt(RemoteConstant.mExtraIndex, 0)
            mImage = extras.getString(RemoteConstant.extraIndexImage, "")
            mTitle = extras.getString(RemoteConstant.mNeedToFilter, "")
            mIsFrom = extras.getString(RemoteConstant.extraIndexIsFrom, "")
        }

        mQueueListAdapter = QueueListAdapter(
            this, this, glideRequestManager
        )
        queueListActivityBinding?.queueListRecyclerView?.adapter = mQueueListAdapter
        mQueueListAdapter?.swap(items!!)
        updatePlaying()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, R.anim.play_panel_close_background)
        AppUtils.hideKeyboard(
            this@QueueListActivity, queueListActivityBinding?.queueListRecyclerView!!
        )
    }

    override fun onQueueItemClick(mPosition: Int) {
        mPlayPosition = mPosition
        mPlayListManager?.currentPosition = mPosition
        mPlayListManager?.play(0, false)
    }


    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {

        when (playbackState) {
            PlaybackState.STOPPED -> {
            }
            PlaybackState.RETRIEVING -> {

            }
            PlaybackState.PREPARING -> {

            }
            PlaybackState.PLAYING -> {
                updatePlaying()
            }
            PlaybackState.PAUSED -> {
                updatePlaying()
            }
            PlaybackState.SEEKING -> {

            }
            PlaybackState.ERROR -> {

            }
        }

        return true
    }


    override fun onPause() {
        super.onPause()
        mPlayListManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        mPlayListManager = MyApplication.playlistManager
        mPlayListManager?.registerPlaylistListener(this)
    }

    private fun updatePlaying() {
        if (MyApplication.playlistManager?.getPlayList() != null && MyApplication.playlistManager?.getPlayList()?.size!! > 0) {
            mQueueListAdapter?.swap(MyApplication.playlistManager?.getPlayList()?.toMutableList()!!)
        }

    }

}