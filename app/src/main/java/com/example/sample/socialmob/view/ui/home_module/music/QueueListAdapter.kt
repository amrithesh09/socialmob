package com.example.sample.socialmob.view.ui.home_module.music

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import kotlinx.android.synthetic.main.options_menu_trending_track.view.*
import kotlinx.android.synthetic.main.queue_list_item.view.*

class QueueListAdapter(
    private val context: FragmentActivity?,
    private val queueItemClickListener: QueueItemClickListener,
    private var glideRequestManager: RequestManager
) :
    RecyclerView.Adapter<QueueListAdapter.QueueListViewHolder>() {

    private var items: MutableList<MediaItem>? = ArrayList()
    private var rowIndex = -1
    private var isLauched: Boolean = false
    private var isPlaying: Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): QueueListViewHolder {
        return QueueListViewHolder(
            LayoutInflater.from(context!!).inflate(R.layout.queue_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: QueueListViewHolder, position: Int) {
        items?.let {
            bindItemSinglePlayList(holder, items as List<MediaItem>)
        }

        try {
            if (!isLauched) {
                if (MyApplication.playlistManager?.playlistHandler != null
                    && MyApplication.playlistManager?.currentItem != null
                    && MyApplication.playlistManager?.currentItem?.id != null
                ) {
                    val mItems: List<MediaItem> = items as List<MediaItem>
                    if (mItems[holder.adapterPosition].id.toString()
                        == MyApplication.playlistManager?.currentItemChange?.currentItem?.id?.toString()
                    ) {

                        try {
                            isPlaying = MyApplication.playlistManager?.playlistHandler != null &&
                                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                            isLauched = true
                        } catch (e: Exception) {
                            e.printStackTrace()
                            false
                        }
                        rowIndex = if (isPlaying) {
                            holder.adapterPosition
                        } else {
                            -1
                        }


                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


        holder.itemView.setOnClickListener {
            rowIndex = -1
            notifyDataSetChanged()
            Handler().postDelayed({
                if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                    rowIndex = holder.adapterPosition
                    queueItemClickListener.onQueueItemClick(rowIndex)
                    notifyDataSetChanged()
                }
            }, 1000)


        }


        if (rowIndex == holder.adapterPosition) {


            if (isPlaying) {
                holder.queueListItemPlayImageView.visibility = View.VISIBLE
                holder.isPlayingImageView.visibility = View.VISIBLE
                holder.gifProgress.visibility = View.VISIBLE
                holder.gifProgress.playAnimation()
            } else {
                holder.queueListItemPlayImageView.visibility = View.INVISIBLE
                holder.isPlayingImageView.visibility = View.INVISIBLE
                holder.gifProgress.visibility = View.INVISIBLE
                holder.gifProgress.pauseAnimation()
            }
        } else {
            holder.queueListItemPlayImageView.visibility = View.INVISIBLE
            holder.isPlayingImageView.visibility = View.INVISIBLE
            holder.gifProgress.visibility = View.INVISIBLE
            holder.gifProgress.pauseAnimation()

        }
        holder.trackOptionsImageView.visibility = View.INVISIBLE
        holder.trackOptionsImageView.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View = LayoutInflater.from(context)
                .inflate(R.layout.options_menu_trending_track, viewGroup)
            val mQQPop =
                PopupWindow(
                    view,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(holder.trackOptionsImageView, -250, -15)

            view.add_to_playlist_text_view.setOnClickListener {
                mQQPop.dismiss()
            }
            view.play_next_text_view.setOnClickListener {
                AppUtils.showCommonToast(context!!, context.getString(R.string.play_next))
                mQQPop.dismiss()
            }
            view.add_to_queue_text_view.setOnClickListener {
                AppUtils.showCommonToast(context!!, context.getString(R.string.add_to_queue))
                mQQPop.dismiss()
            }
            view.download_text_view.setOnClickListener {
                AppUtils.showCommonToast(context!!, context.getString(R.string.download))
                mQQPop.dismiss()
            }
            view.share_track_text_view.setOnClickListener {
                AppUtils.showCommonToast(context!!, context.getString(R.string.share_track))
                mQQPop.dismiss()
            }
        }

    }

    private fun bindItemSinglePlayList(holder: QueueListViewHolder, list: List<MediaItem>) {
        commonLoad(
            list[holder.adapterPosition].thumbnailUrl,
            list[holder.adapterPosition].title,
            list[holder.adapterPosition].album,
            list[holder.adapterPosition].artist,
            holder
        )
    }

    private fun commonLoad(
        trackImage: String?, trackName: String?, jukeBoxCategoryName: String?, author: String?,
        holder: QueueListViewHolder
    ) {
        context?.let {
            glideRequestManager
                .load(trackImage)
                .into(holder.queueListItemImageView)
        }

        if (trackName != null && trackName != "") {
            val mTitle: String = if (trackName.length >= 25) {
                trackName.substring(0, 25) + "..."
            } else {
                trackName
            }
            holder.queueListItemTitleTextView.text = mTitle
        }
        holder.queueListItemNameTextView.text = jukeBoxCategoryName
        holder.queueListItemCategoryTextView.text = author

    }

    override fun getItemCount(): Int {
        return items?.size!!
    }

    class QueueListViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        val queueListItemPlayImageView = view.queue_list_item_play_image_view!!
        val queueListItemImageView = view.queue_list_item_image_view!!
        val queueListItemTitleTextView = view.queue_list_item_title_text_view!!
        val queueListItemNameTextView = view.queue_list_item_name_text_view!!
        val queueListItemCategoryTextView = view.queue_list_item_category_text_view!!
        val trackOptionsImageView = view.track_options_image_view!!
        val isPlayingImageView = view.is_playing_image_view!!
        val gifProgress = view.gif_progress!!

    }

    interface QueueItemClickListener {
        fun onQueueItemClick(mPosition: Int)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun swap(mutableList: MutableList<MediaItem>) {
        val diffCallback =
            RecentConversationListDiffUtils(
                this.items!!,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.items?.clear()
        this.items?.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }


    class RecentConversationListDiffUtils(
        private val oldList: MutableList<MediaItem>,
        private val newList: MutableList<MediaItem>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].albumName == newList[newItemPosition].albumName

        }

    }

}
