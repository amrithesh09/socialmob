package com.example.sample.socialmob.view.ui.home_module.music

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SimpleSpinnerDropdownItemCustomBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.music.Genre

class SwipeCardAdapter(
    private val mContext: Context
) :
    ListAdapter<Genre, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    companion object {
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<Genre>() {
                override fun areItemsTheSame(
                    oldItem: Genre,
                    newItem: Genre
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Genre,
                    newItem: Genre
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        val binding: SimpleSpinnerDropdownItemCustomBinding = DataBindingUtil.inflate(
            LayoutInflater.from(mContext),
            R.layout.simple_spinner_dropdown_item_custom,
            parent,
            false
        )
        return TopTrackViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(position))
    }

    inner class TopTrackViewHolder(val binding: SimpleSpinnerDropdownItemCustomBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            Glide.with(mContext)
                ?.load(getItem(adapterPosition).media?.squareImage)
                ?.thumbnail(0.1f)
                ?.into(binding.helloImage)
            binding.text1.text = getItem(adapterPosition).genreName
        }
    }

}
