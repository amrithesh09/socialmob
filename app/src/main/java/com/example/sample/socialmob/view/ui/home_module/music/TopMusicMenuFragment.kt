package com.example.sample.socialmob.view.ui.home_module.music

import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.PorterDuff
import android.graphics.Shader
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.ShowTapTaget
import com.example.sample.socialmob.view.utils.TapTarget.OnSpotlightStateChangedListener
import com.example.sample.socialmob.view.utils.TapTarget.Spotlight
import com.example.sample.socialmob.view.utils.TapTarget.shape.RoundedRectangle
import com.example.sample.socialmob.view.utils.TapTarget.target.SimpleTarget
import com.example.sample.socialmob.view.utils.events.ChangeStatusBarColor
import com.example.sample.socialmob.view.utils.events.OpenDrawerEvent
import com.example.sample.socialmob.view.utils.events.SelectFragmentEvent
import com.example.sample.socialmob.view.utils.events.SelectFragmentEventRadio
import kotlinx.android.synthetic.main.top_menu_music.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class TopMusicMenuFragment : Fragment() {
    private var mLastClickTime: Long = 0
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.top_menu_music, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //view_music.setBackgroundColor(Color.parseColor("#FFFFFF"))
        textColorChange(dash_board_text_view, "#9796f0", "#fbc7d4")
        textColorChange(radio_board_text_view, "#9796f0", "#fbc7d4")
        textColorChange(radio_text_view, "#9796f0", "#fbc7d4")
        viewColorChange(view_music, "#9796f0", "#fbc7d4")
        view_podcasts.setBackgroundColor(Color.parseColor("#000000"))
        view_radio.setBackgroundColor(Color.parseColor("#000000"))

        top_linear.setBackgroundColor(Color.parseColor("#000000"))

        selectTab(0)

        dash_board_linear.setOnClickListener {
            dash_board_text_view.performClick()
        }
        radio_board_linear.setOnClickListener {
            radio_board_text_view.performClick()
        }
        radio_linear.setOnClickListener {
            radio_text_view.performClick()
        }



        search_icon_image_view.setOnClickListener {
            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            music_search.performClick()
        }

        music_search.setOnClickListener {
            val intent = Intent(activity, CommonFragmentActivity::class.java)
            intent.putExtra("isFrom", "MusicSearch")
            intent.putExtra("isLongPress", "false")
            startActivity(intent)
        }
//        music_search.setOnLongClickListener {
//            AppUtils.showCommonToast(activity!!,"Long click")
//            val intent = Intent(activity, CommonFragmentActivity::class.java)
//            intent.putExtra("isFrom", "MusicSearch")
//            intent.putExtra("isLongPress", "true")
//            startActivity(intent)
//            return@setOnLongClickListener true
//        }
        dash_board_text_view.setOnClickListener {
            EventBus.getDefault().post(SelectFragmentEvent(getString(R.string.dashboard)))
            EventBus.getDefault().post(ChangeStatusBarColor("black"))
            selectTab(0)
            music_drawer_image_view.setColorFilter(
                ContextCompat.getColor(requireActivity(), android.R.color.white),
                PorterDuff.Mode.SRC_IN
            )
            //view_music.setBackgroundColor(Color.parseColor("#FFFFFF"))
            viewColorChange(view_music, "#9796f0", "#fbc7d4")
            view_podcasts.setBackgroundColor(Color.parseColor("#000000"))
            view_radio.setBackgroundColor(Color.parseColor("#000000"))

            top_linear.setBackgroundColor(Color.parseColor("#000000"))
        }
        radio_board_text_view.setOnClickListener {
            EventBus.getDefault().post(
                SelectFragmentEvent(
                    getString(R.string.music_video)
                )
            )
            EventBus.getDefault().post(
                ChangeStatusBarColor(
                    "black"
                )
            )
            selectTab(2)
            music_drawer_image_view.setColorFilter(
                ContextCompat.getColor(requireActivity(), R.color.white),
                PorterDuff.Mode.SRC_IN
            )
            view_music.setBackgroundColor(Color.parseColor("#000000"))
           // view_podcasts.setBackgroundColor(Color.parseColor("#FFFFFF"))
            viewColorChange(view_podcasts, "#9796f0", "#fbc7d4")
            view_radio.setBackgroundColor(Color.parseColor("#000000"))

            top_linear.setBackgroundColor(Color.parseColor("#000000"))
        }
        radio_text_view.setOnClickListener {
            EventBus.getDefault().post(
                SelectFragmentEvent(
                    getString(R.string.genre)
                )
            )
            EventBus.getDefault().post(
                ChangeStatusBarColor(
                    "black"
                )
            )
            selectTab(1)
            music_drawer_image_view.setColorFilter(
                ContextCompat.getColor(requireActivity(), R.color.white),
                PorterDuff.Mode.SRC_IN
            )
            view_music.setBackgroundColor(Color.parseColor("#00000000"))
            view_podcasts.setBackgroundColor(Color.parseColor("#00000000"))
           // view_radio.setBackgroundColor(Color.parseColor("#FFFFFF"))
            viewColorChange(view_radio, "#9796f0", "#fbc7d4")
//            val handler = Handler(Looper.getMainLooper())
//            handler.postDelayed({
//            top_linear.setBackgroundColor(Color.parseColor("#00000000"))
            top_linear.setBackgroundColor(Color.parseColor("#000000"))
//            }, 100)

        }

        music_drawer_image_view.setOnClickListener {

            EventBus.getDefault().post(OpenDrawerEvent())
        }
    }

    private fun selectTabColorBlack(
        textView: TextView?, textView1: TextView?, textView2: TextView?
    ) {
        textView?.setTextColor(Color.parseColor("#1e90ff"))
        textView1?.setTextColor(Color.parseColor("#747d8c"))
        textView2?.setTextColor(Color.parseColor("#747d8c"))

    }

    private fun selectTab(position: Int) {

        if (position == 0) {
            dash_board_text_view.visibility = View.VISIBLE
            dash_board_text_view_gray.visibility = View.GONE

            radio_board_text_view.visibility = View.GONE
            radio_board_text_view_gray.visibility = View.VISIBLE

            radio_text_view.visibility = View.GONE
            radio_text_view_gray.visibility = View.VISIBLE

        } else if (position == 1) {
            dash_board_text_view.visibility = View.GONE
            dash_board_text_view_gray.visibility = View.VISIBLE

            radio_board_text_view.visibility = View.GONE
            radio_board_text_view_gray.visibility = View.VISIBLE

            radio_text_view.visibility = View.VISIBLE
            radio_text_view_gray.visibility = View.GONE
        } else {
            dash_board_text_view.visibility = View.GONE
            dash_board_text_view_gray.visibility = View.VISIBLE

            radio_board_text_view.visibility = View.VISIBLE
            radio_board_text_view_gray.visibility = View.GONE

            radio_text_view.visibility = View.GONE
            radio_text_view_gray.visibility = View.VISIBLE
        }
    }

    private fun textColorChange(textView: TextView, colorCode1: String, colorCode2: String) {
        val width =  textView.paint.measureText(textView.text.toString())
        val textShader: Shader = LinearGradient(0f, 0f, width, textView.textSize, intArrayOf(
            Color.parseColor(colorCode1),
            Color.parseColor(colorCode2)
        ), null, Shader.TileMode.REPEAT)

        textView.paint.setShader(textShader)
    }

    private fun viewColorChange(view: View, colorCode1: String, colorCode2: String) {
        val gd = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM, intArrayOf( Color.parseColor(colorCode1),
                Color.parseColor(colorCode2))
        )
        gd.cornerRadius = 0f
        view.setBackgroundDrawable(gd)
    }

    private fun tapTargetTopMusic() {
        val threeWidth = top_menu_linear.height.toFloat()
        val threeHeight = top_menu_linear.width.toFloat()

        val location = IntArray(2)
        top_menu_linear.getLocationInWindow(location)
        location[1] + top_menu_linear.height / 2

        // third target
        val thirdTarget = SimpleTarget.Builder(requireActivity())
            .setPoint(top_menu_linear)
            .setShape(RoundedRectangle(threeWidth, threeHeight, 25f))
            .setTitle("Explore these sub-menus for more")
            .setDescription("")
            .setOverlayPoint(10f, threeHeight)
            .build()

        // create spotlight
        Spotlight.with(requireActivity())
            .setOverlayColor(R.color.background_shade)
            .setDuration(1000L)
            .setAnimation(DecelerateInterpolator(50f))
            .setTargets(thirdTarget)
            .setClosedOnTouchedOutside(true)
            .setOnSpotlightStateListener(object : OnSpotlightStateChangedListener {
                override fun onStarted() {
                }

                override fun onEnded() {
                }
            })
            .start()

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SelectFragmentEvent) {
        when {

            event.gemSelectFragment() == "TopGenre" -> //fragmentTransation(fragmentGenre)
                radio_text_view.performClick()
            event.gemSelectFragment() == "Radio" -> {
                //fragmentTransation(fragmentGenre)
                selectTabColorBlack(radio_text_view, radio_board_text_view, dash_board_text_view)
                music_drawer_image_view.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.white),
                    PorterDuff.Mode.SRC_IN
                )
            }
            event.gemSelectFragment() == getString(R.string.genre) -> {
//                EventBus.getDefault()
//                    .post(SelectFragmentEventRadio(getString(R.string.radio)))
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SelectFragmentEventRadio) {
        println(">>SelectFragmentEventRadio")
        radio_text_view?.performClick()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ShowTapTaget) {
        tapTargetTopMusic()
    }
}

