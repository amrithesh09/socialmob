package com.example.sample.socialmob.view.ui.home_module.music

import android.annotation.SuppressLint
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.CommentActivityBinding
import com.example.sample.socialmob.di.util.CacheMapperMention
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.CommentPostDataTrack
import com.example.sample.socialmob.model.music.TrackCommentModelPayloadComment
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.MusicDashBoardFragment
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.RecentlyPlayedActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchTrackFragment
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.socialview.Mention
import com.example.sample.socialmob.view.utils.socialview.MentionArrayAdapter
import com.example.sample.socialmob.viewmodel.profile.PostCommentLikeViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Handled track , pod-cast , pod-cast episode track comments
 */
@AndroidEntryPoint
class TrackCommentsActivity : BaseCommonActivity(), MusicCommentAdapter.OptionPostItemClickListener,
    PlaylistListener<MediaItem> {
    @Inject
    lateinit var glideRequestManager: RequestManager

    @Inject
    lateinit var cacheMapperMention: CacheMapperMention

    private val postCommentLikeViewModel: PostCommentLikeViewModel by viewModels()
    private var commentActivityBinding: CommentActivityBinding? = null
    private var mCommentAdapter: MusicCommentAdapter? = null
    private var mGlobalCommentList: MutableList<TrackCommentModelPayloadComment>? = ArrayList()
    private var mStatus: ResultResponse.Status? = null
    private var playlistManager: PlaylistManager? = null

    private var mTrackId = ""
    private var mTrackName = ""
    private var mTrackGenre = ""
    private var mTrackPlayCount = ""
    private var mTrackArtist = ""
    private var mTrackImageUrl = ""
    private var mIsFrom = ""
    private var mDuration = ""
    private var isLoaderAdded: Boolean = false
    private var isLiked: String = "false"
    private var likeCount: String = "0"
    private var commentsCount: String = "0"

    private var defaultMentionAdapter: ArrayAdapter<Mention>? = null
    private var isMention: Boolean = false

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        commentActivityBinding = DataBindingUtil.setContentView(this, R.layout.comment_activity)
        //TODO : get intent extras
        getExtras()

        commentActivityBinding?.trackName = mTrackName
        commentActivityBinding?.trackGenre = mTrackGenre
        commentActivityBinding?.trackArtist = mTrackArtist
        commentActivityBinding?.trackPlayCount = mTrackPlayCount
        commentActivityBinding?.trackImageUrl = mTrackImageUrl
        commentActivityBinding?.executePendingBindings()
        val profileImage = SharedPrefsUtils.getStringPreference(
            this@TrackCommentsActivity,
            RemoteConstant.mUserImage,
            ""
        )!!

        if (profileImage != "") {
            glideRequestManager.load(RemoteConstant.getImageUrlFromWidth(profileImage, 100))
                .into(commentActivityBinding?.commentProfileImageView!!)
        } else {
            glideRequestManager.load(R.drawable.emptypicture)
                .into(commentActivityBinding?.commentProfileImageView!!)
        }

        commentActivityBinding?.musicDetails?.visibility = View.VISIBLE


        setUpClickListeners()

        //TODO : Set comments adapter
        setAdapter()

        // TODO : Init API call
        callApiCommon()
        observeCommentList()


        commentActivityBinding?.likeMaterialButton?.setOnClickListener {
            if (isLiked == "false") {

                //TODO : Update like count
                if (likeCount != "") {
                    likeCount = likeCount.toInt().plus(1).toString()
                }
                commentActivityBinding?.likes = likeCount

                isLiked = "true"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(UpdateTrackEvent(mTrackId, isLiked))
                MyApplication.playlistManager?.currentItem?.isFavorite = "true"
                commentActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                postCommentLikeViewModel.addToFavourites(mAuth, mTrackId)

                try {
                    when (mIsFrom) {

                        "TopTrack" -> {
                            MusicDashBoardFragment.topTrackModel!![playlistManager?.currentPosition!!].favorite =
                                "true"
                        }
                        "TrendingTrack" -> {
                            MusicDashBoardFragment.trendingTrackModel!![playlistManager?.currentPosition!!].favorite =
                                "true"

                        }
                        "MusicDashBoard" -> {
                            MusicDashBoardFragment.featuredTrackModelNew!![playlistManager?.currentPosition!!].favorite =
                                "true"
                        }
                        "dblistSearchTrack" -> {
                            SearchTrackFragment.dbPlayList!![playlistManager?.currentPosition!!].favorite =
                                "true"
                        }
                        "dblistMusicGrid" -> {

                        }
                        "dblistFeedRecommended" -> {

                        }
                        "dblistUserProfile" -> {

                        }
                        "dbRecentlyPlayedSongs" -> {
                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                "true"
                        }
                        "dbLikedsongs" -> {
                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                "true"
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }


            } else {
                isLiked = "false"

                //TODO : Update like count
                likeCount = likeCount.toInt().minus(1).toString()
                commentActivityBinding?.likes = likeCount

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(UpdateTrackEvent(mTrackId, isLiked))
                MyApplication.playlistManager?.currentItem?.isFavorite = "false"
                commentActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                postCommentLikeViewModel.removeFavourites(mAuth, mTrackId)
                try {
                    when (mIsFrom) {
                        "TopTrack" -> {
                            MusicDashBoardFragment.topTrackModel!![playlistManager?.currentPosition!!].favorite =
                                "false"
                        }
                        "TrendingTrack" -> {
                            MusicDashBoardFragment.trendingTrackModel!![playlistManager?.currentPosition!!].favorite =
                                "false"
                        }
                        "MusicDashBoard" -> {
                            MusicDashBoardFragment.featuredTrackModelNew!![playlistManager!!.currentPosition].favorite =
                                "false"
                        }
                        "dblistSearchTrack" -> {
                            SearchTrackFragment.dbPlayList!![playlistManager?.currentPosition!!].favorite =
                                "false"
                        }
                        "dblistMusicGrid" -> {

                        }
                        "dblistFeedRecommended" -> {

                        }
                        "dblistUserProfile" -> {

                        }
                        "dbRecentlyPlayedSongs" -> {
                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                "false"
                        }
                        "dbLikedsongs" -> {
                            RecentlyPlayedActivity.singlePlayListTrack!![playlistManager?.currentPosition!!].favorite =
                                "false"
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            postCommentLikeViewModel.updateFavourite(isLiked, mTrackId)
        }


        // TODO : Metion in track comment

        commentActivityBinding?.commentEditText?.isMentionEnabled = true
        defaultMentionAdapter = MentionArrayAdapter(this)
        commentActivityBinding?.commentEditText?.mentionAdapter = defaultMentionAdapter

        commentActivityBinding?.commentEditText?.setMentionTextChangedListener { view, text ->
            isMention = true
            if (text.isNotEmpty()) {
                defaultMentionAdapter?.clear()
                postCommentLikeViewModel.getMentions(this, text.toString())
            }
        }
        observeSearchMention()

    }

    private fun setAdapter() {
        commentActivityBinding?.postCommentsRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mCommentAdapter = MusicCommentAdapter(
            this@TrackCommentsActivity,
            this, glideRequestManager
        )
        commentActivityBinding?.postCommentsRecyclerView?.adapter = mCommentAdapter
        commentActivityBinding?.postCommentsRecyclerView?.addOnScrollListener(CustomScrollListener())

    }

    private fun setUpClickListeners() {
        commentActivityBinding?.commentsBackImageView?.setOnClickListener {
            onBackPressed()
        }
        commentActivityBinding?.commentSendImageView?.setOnClickListener {
            if (commentActivityBinding?.commentEditText?.text?.isNotEmpty()!!) {

                // TODO : Check Internet connection
                if (!InternetUtil.isInternetOn()) {
                    AppUtils.showCommonToast(
                        this@TrackCommentsActivity, getString(R.string.no_internet)
                    )
                } else {
                    when (mIsFrom) {
                        "fromPodcast" -> {
                            val fullData = RemoteConstant.getEncryptedString(
                                SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mAppId,
                                    ""
                                )!!,
                                SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mApiKey,
                                    ""
                                )!!, RemoteConstant.mBaseUrl +
                                        RemoteConstant.mSlash + "api/v1/podcast/comment?target=podcast&source=" + mTrackId,
                                RemoteConstant.postMethod
                            )
                            val mAuth =
                                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mAppId,
                                    ""
                                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                            val commentPostData =
                                CommentPostDataTrack(
                                    commentActivityBinding?.commentEditText?.text.toString(),
                                    mDuration
                                )
                            postCommentLikeViewModel.postCommentPodCast(
                                mAuth,
                                mTrackId,
                                commentPostData, "podcast"
                            )

                        }
                        "fromPodcastTrack" -> {

                            val fullData = RemoteConstant.getEncryptedString(
                                SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mAppId,
                                    ""
                                )!!,
                                SharedPrefsUtils.getStringPreference(
                                    this,
                                    RemoteConstant.mApiKey,
                                    ""
                                )!!, RemoteConstant.mBaseUrl +
                                        RemoteConstant.mSlash + "api/v1/podcast/comment?target=episode&source=" + mTrackId,
                                RemoteConstant.postMethod
                            )
                            val mAuth =
                                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                    this, RemoteConstant.mAppId,
                                    ""
                                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                            val commentPostData =
                                CommentPostDataTrack(
                                    commentActivityBinding?.commentEditText?.text.toString(),
                                    mDuration
                                )
                            postCommentLikeViewModel.postCommentPodCast(
                                mAuth,
                                mTrackId,
                                commentPostData, "episode"
                            )


                        }
                        else -> {
                            val mAuth: String = RemoteConstant.getAuthAddTackComment(
                                mContext = this, mTrackId = mTrackId
                            )
                            val commentPostData =
                                CommentPostDataTrack(
                                    commentActivityBinding?.commentEditText?.text.toString(),
                                    mDuration
                                )
                            postCommentLikeViewModel.postCommentTrack(
                                mAuth,
                                mTrackId,
                                commentPostData
                            )
                        }
                    }

                    AppUtils.hideKeyboard(
                        this@TrackCommentsActivity, commentActivityBinding?.commentEditText!!
                    )
                    commentActivityBinding?.commentEditText?.setText("")
                }
            } else
                AppUtils.showCommonToast(this@TrackCommentsActivity, "Comment cannot be empty")

        }


        // TODO : Disable swipe when keyboard is visible
        commentActivityBinding?.topLinear?.viewTreeObserver?.addOnGlobalLayoutListener {
            val r = Rect()
            commentActivityBinding?.topLinear?.getWindowVisibleDisplayFrame(r)
            val screenHeight = commentActivityBinding?.topLinear?.rootView?.height
            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            val keypadHeight = screenHeight!! - r.bottom
            commentActivityBinding?.swipeBackLayout?.isSwipeFromEdge =
                keypadHeight > screenHeight * 0.15
        }

    }

    @SuppressLint("SetTextI18n")
    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (mIsFrom) {
                "fromPodcast" -> {
                    callApiPodcastComments(mTrackId, "0")
                    commentActivityBinding?.likeLinear?.visibility = View.GONE
                    commentActivityBinding?.podcastName?.visibility = View.VISIBLE
                    commentActivityBinding?.trackDetails?.visibility = View.GONE
                    commentActivityBinding?.toolBarTextView?.text = mTrackName

                }
                "fromPodcastTrack" -> {
                    callApiPodcastEpisodeComments(mTrackId, "0")
                    commentActivityBinding?.likeLinear?.visibility = View.GONE
                    commentActivityBinding?.toolBarTextView?.text = "EPISODE COMMENTS"
                    //                commentActivityBinding?.podcastName?.visibility = View.VISIBLE
                    //                commentActivityBinding?.trackDetails?.visibility = View.GONE

                }
                else -> {
                    commentActivityBinding?.toolBarTextView?.text =
                        getString(R.string.track_coments)
                    callApiTrackComments(mTrackId, "0")
                }
            }

        } else {
            postCommentLikeViewModel.commentPaginatedResponseModel?.postValue(
                ResultResponse(
                    ResultResponse.Status.NO_INTERNET,
                    ArrayList(),
                    getString(R.string.no_internet)
                )
            )
            val shake: Animation =
                AnimationUtils.loadAnimation(this@TrackCommentsActivity, R.anim.shake)
            commentActivityBinding?.noInternetLinear?.startAnimation(shake) // starts animation

            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(commentActivityBinding?.noInternetImageView!!)
        }

    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager?.childCount!!
            val totalItemCount = recyclerView.layoutManager?.itemCount!!
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                    !isLoaderAdded && mGlobalCommentList != null
                    && mGlobalCommentList?.isNotEmpty()!! && mGlobalCommentList?.size!! >= 15
                ) {
                    if (InternetUtil.isInternetOn()) {
                        loadMoreItems(mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1)?.commentId!!)
                    }
                }
            }
        }
    }

    private fun loadMoreItems(mCommentId: String) {
        val mCommentList: MutableList<TrackCommentModelPayloadComment> = ArrayList()
        val mList = TrackCommentModelPayloadComment()
        mList.commentId = ""
        mCommentList.add(mList)
        mGlobalCommentList?.addAll(mCommentList)
        mCommentAdapter?.notifyDataSetChanged()

        isLoaderAdded = true

        when (mIsFrom) {
            "fromPodcast" -> {
                callApiPodcastComments(mTrackId, mCommentId)
            }
            "fromPodcastTrack" -> {
                callApiPodcastEpisodeComments(mTrackId, mCommentId)
            }
            else -> {
                callApiTrackComments(mTrackId, mCommentId)
            }
        }

    }

    private fun observeCommentList() {
        postCommentLikeViewModel.trackCommentPaginatedResponseModel?.nonNull()
            ?.observe(this, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        if (resultResponse?.data?.comments?.size!! > 0) {
                            mGlobalCommentList = resultResponse.data.comments.toMutableList()

                            mCommentAdapter?.submitList(mGlobalCommentList)
                        }

                        commentActivityBinding?.progressLinear?.visibility = View.GONE
                        commentActivityBinding?.noInternetLinear?.visibility = View.GONE
                        commentActivityBinding?.noDataLinear?.visibility = View.GONE
                        commentActivityBinding?.dataLinear?.visibility = View.VISIBLE

                        if (resultResponse.data.track?.playCount != null) {
                            commentActivityBinding?.trackPlayCount =
                                resultResponse.data.track.playCount
                        }

                        if (resultResponse.data.commentsCount != null) {
                            commentsCount = resultResponse.data.commentsCount
                            commentActivityBinding?.comments = commentsCount
                        }
                        if (resultResponse.data.track?.likeCount != null) {
                            commentActivityBinding?.likes = resultResponse.data.track.likeCount
                        }

                        if (resultResponse.data.track?.likeCount != null) {
                            likeCount = resultResponse.data.track.likeCount
                        }

                        if (resultResponse.data.track?.favorite != null) {
                            isLiked = resultResponse.data.track.favorite!!

                            if (resultResponse.data.track.favorite == "true") {
                                commentActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)
                            } else {
                                commentActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                            }
                        }


                        //TODO : For podcast

                        if (postCommentLikeViewModel.mTrackName != "") {
                            commentActivityBinding?.trackName = postCommentLikeViewModel.mTrackName
                        }
                        if (postCommentLikeViewModel.mTrackImageUrl != null) {
                            commentActivityBinding?.trackImageUrl =
                                postCommentLikeViewModel.mTrackImageUrl
                        }


                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mGlobalCommentList?.addAll(resultResponse?.data?.comments!!)
                        mCommentAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {

                        if (mGlobalCommentList?.size == 0) {
                            commentActivityBinding?.progressLinear?.visibility = View.GONE
                            commentActivityBinding?.noInternetLinear?.visibility = View.VISIBLE
                            commentActivityBinding?.dataLinear?.visibility = View.GONE
                            commentActivityBinding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(commentActivityBinding?.noInternetImageView!!)
                            commentActivityBinding?.noInternetTextView?.text =
                                getString(R.string.server_error)
                            commentActivityBinding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        }


                    }
                    ResultResponse.Status.NO_DATA -> {

                        glideRequestManager.load(R.drawable.ic_empty_comments)
                            .into(commentActivityBinding?.noCommentsImageView!!)

                        commentActivityBinding?.progressLinear?.visibility = View.GONE
                        commentActivityBinding?.noInternetLinear?.visibility = View.GONE
                        commentActivityBinding?.dataLinear?.visibility = View.VISIBLE
                        commentActivityBinding?.noDataLinear?.visibility = View.VISIBLE

                        commentActivityBinding?.trackPlayCount = postCommentLikeViewModel.playCount
                        commentsCount = postCommentLikeViewModel.commentsCount
                        commentActivityBinding?.comments = postCommentLikeViewModel.commentsCount
                        commentActivityBinding?.likes = postCommentLikeViewModel.likeCount
                        isLiked = postCommentLikeViewModel.favorite
                        likeCount = postCommentLikeViewModel.likeCount

                        if (postCommentLikeViewModel.favorite == "true") {
                            commentActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_blue_24dp)
                        } else {
                            commentActivityBinding?.likeMaterialButton?.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                        }

                        //TODO : For podcast
                        if (postCommentLikeViewModel.mTrackName != "") {
                            commentActivityBinding?.trackName = postCommentLikeViewModel.mTrackName
                        }
                        if (postCommentLikeViewModel.mTrackImageUrl != null) {
                            commentActivityBinding?.trackImageUrl =
                                postCommentLikeViewModel.mTrackImageUrl
                        }

                    }
                    ResultResponse.Status.NO_INTERNET -> {
                        glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                            .into(commentActivityBinding?.noInternetImageView!!)

                    }
                    ResultResponse.Status.LOADING -> {

                        commentActivityBinding?.progressLinear?.visibility = View.VISIBLE
                        commentActivityBinding?.noInternetLinear?.visibility = View.GONE
                        commentActivityBinding?.dataLinear?.visibility = View.GONE
                        commentActivityBinding?.noDataLinear?.visibility = View.GONE


                    }
                    else -> {

                    }
                }


            })

        postCommentLikeViewModel.addedTrackCommentResponse?.nonNull()
            ?.observe(this, { addedTrackCommentResponse ->
                if (addedTrackCommentResponse != null) {
                    mGlobalCommentList?.add(0, addedTrackCommentResponse)

                    if (mGlobalCommentList?.size == 1) {
                        mCommentAdapter?.submitList(mGlobalCommentList)
                    }
                    mCommentAdapter?.notifyDataSetChanged()

                    if (commentActivityBinding?.dataLinear?.visibility == View.GONE) {
                        commentActivityBinding?.progressLinear?.visibility = View.GONE
                        commentActivityBinding?.noInternetLinear?.visibility = View.GONE
                        commentActivityBinding?.noDataLinear?.visibility = View.GONE
                        commentActivityBinding?.dataLinear?.visibility = View.VISIBLE
                    }

                    commentsCount = commentsCount.toInt().plus(1).toString()
                    commentActivityBinding?.comments = commentsCount

                }
            })
    }


    private fun getExtras() {
        val extras: Bundle
        if (intent.extras != null) {
            extras = intent?.extras!!

            if (extras.containsKey(RemoteConstant.mTrackId)) {
                mTrackId = extras.getString(RemoteConstant.mTrackId, "")
            }
            if (extras.containsKey(RemoteConstant.mTrackName)) {
                mTrackName = extras.getString(RemoteConstant.mTrackName, "")
            }
            if (extras.containsKey(RemoteConstant.mTrackGenre)) {
                mTrackGenre = extras.getString(RemoteConstant.mTrackGenre, "")
            }
            if (extras.containsKey(RemoteConstant.mTrackArtist)) {
                mTrackArtist = extras.getString(RemoteConstant.mTrackArtist, "")
            }
            if (extras.containsKey(RemoteConstant.mTrackPlayCount)) {
                mTrackPlayCount = extras.getString(RemoteConstant.mTrackPlayCount, "")
            }
            if (extras.containsKey(RemoteConstant.mTrackImage)) {
                mTrackImageUrl = extras.getString(RemoteConstant.mTrackImage, "")
            }
            if (extras.containsKey(RemoteConstant.mIsFrom)) {
                mIsFrom = extras.getString(RemoteConstant.mIsFrom, "")
            }
            if (extras.containsKey(RemoteConstant.mDuration)) {
                mDuration = extras.getString(RemoteConstant.mDuration, "")
            }
        }
    }

    private fun callApiPodcastComments(mTrackId: String, size: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mSlash + "api/v1/podcast/comment?target=podcast&source=" + mTrackId + "&offset=" + size + "&count=15",
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        postCommentLikeViewModel.getPodcastComment(mAuth, mTrackId, size, RemoteConstant.mCount)

    }

    private fun callApiPodcastEpisodeComments(mTrackId: String, size: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mSlash + "api/v1/podcast/comment?target=episode&source=" + mTrackId + "&offset=" + size + "&count=15",
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        postCommentLikeViewModel.getPodcastEpisodeComment(
            mAuth,
            mTrackId,
            size,
            RemoteConstant.mCount
        )
    }


    private fun callApiTrackComments(mTrackId: String, size: String) {
        val mAuth = RemoteConstant.getAuthTrack(this, mTrackId, size)
        postCommentLikeViewModel.getTrackComment(mAuth, mTrackId, size, RemoteConstant.mCount)
    }


    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)

    }

    override fun onResume() {
        super.onResume()

        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)
    }


    override fun onOptionItemClickOption(
        mMethod: String, mId: String, mComment: String, mPosition: Int
    ) {
        when (mMethod) {
            "deleteComment" -> deleteComment(mId, mPosition)
        }
    }

    private fun deleteComment(mId: String, mPosition: Int) {

        when (mIsFrom) {
            "fromPodcast" -> {

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl +
                            "/api/v1/podcast/comment?target=podcast&source=" + mTrackId + "&comment=" + mId,
                    RemoteConstant.deleteMethod
                )

                val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    this, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                postCommentLikeViewModel.deletePodCastComment(mAuth, mTrackId, mId, "podcast")


            }
            "fromPodcastTrack" -> {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl +
                            "/api/v1/podcast/comment?target=episode&source=" + mTrackId + "&comment=" + mId,
                    RemoteConstant.deleteMethod
                )

                val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    this, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                postCommentLikeViewModel.deletePodCastComment(mAuth, mTrackId, mId, "episode")

            }
            else -> {
                val mAuth = RemoteConstant.getAuthDeleteTrackComment(this, mTrackId, mId)
                postCommentLikeViewModel.deleteTrackComment(mAuth, mTrackId, mId)
            }
        }

        mGlobalCommentList?.removeAt(mPosition)
        commentsCount = commentsCount.toInt().minus(1).toString()
        commentActivityBinding?.comments = commentsCount
        mCommentAdapter?.notifyItemRemoved(mPosition)


    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, R.anim.play_panel_close_background)
    }


    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        return false
    }


    private fun removeLoaderFormList() {
        if (mGlobalCommentList != null && mGlobalCommentList?.size!! > 0 && mGlobalCommentList?.get(
                mGlobalCommentList?.size!! - 1
            ) != null && mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1)?.commentId == ""
        ) {
            mGlobalCommentList?.removeAt(mGlobalCommentList?.size!! - 1)
            mCommentAdapter?.notifyDataSetChanged()
            isLoaderAdded = false
        }
    }

    private fun observeSearchMention() {
        postCommentLikeViewModel.mentionProfileResponseModel.nonNull().observe(this, { profiles ->
            if (profiles != null) {
                val mMentionList: MutableList<Mention>
                if (profiles.isNotEmpty()) {


                    val mMentionEditTextList: MutableList<String> =
                        commentActivityBinding?.commentEditText?.mentions ?: ArrayList()

                    val mPopulateList: MutableList<Profile> =
                        profiles.filter {
                            it.username !in mMentionEditTextList.map { item ->
                                item.replace("@", "")
                            }
                        } as MutableList<Profile>
                    mMentionList =
                        cacheMapperMention.mapFromEntityListMention(mPopulateList).toMutableList()
                    defaultMentionAdapter?.addAll(mMentionList)
                }

                if (defaultMentionAdapter == null) {
                    defaultMentionAdapter = MentionArrayAdapter(this)
                    commentActivityBinding?.commentEditText?.mentionAdapter = defaultMentionAdapter
                } else {
                    commentActivityBinding?.commentEditText?.mentionAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

}
