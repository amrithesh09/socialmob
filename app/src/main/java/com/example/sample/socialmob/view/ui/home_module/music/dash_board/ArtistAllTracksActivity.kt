package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArtistAllTrackActivtyBinding
import com.example.sample.socialmob.model.music.ArtistDetailsPayload
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.radio.GenrePlayListAdapter
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.ArtistViewModel
import com.suke.widget.SwitchButton
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class ArtistAllTracksActivity : BaseCommonActivity(),
    GenrePlayListAdapter.GenrePlayListItemClickListener,
    GenrePlayListAdapter.PlayListItemClickListener,
    PlaylistListener<MediaItem> {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mArtistId: String = ""
    private var mArtistName: String = ""
    private var artistAllTrackActivtyBinding: ArtistAllTrackActivtyBinding? = null
    private val artistViewModel: ArtistViewModel by viewModels()
    private var mGlobalGenreList: MutableList<TrackListCommon>? = ArrayList()
    private var mStatus: ResultResponse.Status? = null
    private var mGenreAdapter: GenrePlayListAdapter? = null
    private var isLoaderAdded: Boolean = false
    private var mLastId: String = ""
    private var playListImageView: ImageView? = null
    private var mLastClickTime: Long = 0
    private var mTrackIdGlobal: String = ""

    private val mediaItems = LinkedList<MediaItem>()
    private var mPlayPostion = -1
    private var PLAYLIST_ID = 4 //Arbitrary, for the example
    private var playlistManager: PlaylistManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        artistAllTrackActivtyBinding =
            DataBindingUtil.setContentView(this, R.layout.artist_all_track_activty)

        val i: Intent? = intent
        mArtistId = i?.getStringExtra("mArtistId")?:""
        mArtistName = i?.getStringExtra("mArtistName")?:""

        callApiCommon()

        artistAllTrackActivtyBinding?.genreBackImageView?.setOnClickListener {
            finish()
        }

        artistAllTrackActivtyBinding?.allTrackRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
            )

        val mListAdUitId: MutableList<String>? = ArrayList()
        mListAdUitId?.add(getString(R.string.ad_unit_id))
        mListAdUitId?.add(getString(R.string.ad_unit_id_2))

        mGenreAdapter = GenrePlayListAdapter(
            this,
            this,
            this@ArtistAllTracksActivity
        )
        artistAllTrackActivtyBinding?.allTrackRecyclerView?.adapter = mGenreAdapter
        artistAllTrackActivtyBinding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
            val view =
                artistAllTrackActivtyBinding?.nestedScrollView?.getChildAt(
                    artistAllTrackActivtyBinding?.nestedScrollView?.childCount!! - 1
                )

            val diff =
                view?.bottom?:0  - (artistAllTrackActivtyBinding?.nestedScrollView?.height!! + artistAllTrackActivtyBinding?.nestedScrollView?.scrollY!!)

            if (diff == 0) {
                //your api call to fetch data
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                    mGlobalGenreList?.size!! >= 15
                ) {
                    callApi(mArtistId, mGlobalGenreList?.size.toString())
                }
            }
        }
        artistAllTrackActivtyBinding?.gifProgress?.setOnClickListener { view: View? -> artistAllTrackActivtyBinding?.fab?.performClick() }
        artistAllTrackActivtyBinding?.fab?.setOnClickListener {
                if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    var mImageUrlPass = ""
                    if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                        Intent(this@ArtistAllTracksActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                } else if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null
                ) {
                    var mImageUrlPass = ""
                    if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                        Intent(this@ArtistAllTracksActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                }
        }
    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            artistViewModel.artistModel?.postValue(ResultResponse.noInternet(ArtistDetailsPayload()))
            val shake: android.view.animation.Animation =
                AnimationUtils.loadAnimation(this, R.anim.shake)
            artistAllTrackActivtyBinding?.noInternetLinear?.startAnimation(shake) // starts animation
        } else {
            callApi(mArtistId, mGlobalGenreList?.size.toString())
        }
        observeData()
    }

    private fun callApi(mArtistId: String, mOffset: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + "/api/v1/artist/" + mArtistId + "/toptracks/" + mOffset +
                    RemoteConstant.mSlash + RemoteConstant.mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        artistViewModel.artistAllTracks(mAuth, mArtistId, mOffset)
    }

    private fun observeData() {
        artistViewModel.genreTrackListModel?.nonNull()?.observe(this, { genreTrackList ->
            mStatus = genreTrackList.status
            if (genreTrackList.status == ResultResponse.Status.SUCCESS) {
                mGlobalGenreList =
                    genreTrackList.data as MutableList<TrackListCommon>
                if (mGlobalGenreList!!.isNotEmpty()) {

                    artistAllTrackActivtyBinding?.noDataLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.progressLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.artistTrackLinear?.visibility = View.VISIBLE
                    artistAllTrackActivtyBinding?.noInternetLinear?.visibility = View.GONE
                    if (isLoaderAdded) {
                        removeLoaderFormList()
                    }
                    mGenreAdapter?.swap(mGlobalGenreList!!)
//                    mGenreAdapter?.notifyItemInserted(mGlobalGenreList?.size!!)
                    checkTrackPlayStatus()
                    mStatus = ResultResponse.Status.LOADING_COMPLETED
                }
            } else if (genreTrackList.status == ResultResponse.Status.PAGINATED_LIST) {

                removeLoaderFormList()
                val mGlobalGenreListNew: MutableList<TrackListCommon> =
                    genreTrackList.data as MutableList<TrackListCommon>
                mGlobalGenreList?.addAll(mGlobalGenreListNew)
                mGenreAdapter?.swap(mGlobalGenreList!!)
                checkTrackPlayStatus()

                mStatus = ResultResponse.Status.LOADING_COMPLETED
            } else if (genreTrackList.status == ResultResponse.Status.EMPTY_PAGINATED_LIST) {
                removeLoaderFormList()

            } else if (genreTrackList.status == ResultResponse.Status.ERROR) {
                if (mGlobalGenreList?.isEmpty()!!) {
                    artistAllTrackActivtyBinding?.noDataLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.progressLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.artistTrackLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.noInternetLinear?.visibility = View.VISIBLE
                    glideRequestManager.load(R.drawable.server_error).apply(
                        RequestOptions().fitCenter())
                        .into(artistAllTrackActivtyBinding?.noInternetImageView!!)
                    artistAllTrackActivtyBinding?.noInternetTextView?.text =
                        getString(R.string.server_error)
                    artistAllTrackActivtyBinding?.noInternetSecTextView?.text =
                        getString(R.string.server_error_content)
                } else {
                    removeLoaderFormList()
                }
            } else if (genreTrackList.status == ResultResponse.Status.NO_DATA) {
                artistAllTrackActivtyBinding?.noDataLinear?.visibility = View.VISIBLE
                artistAllTrackActivtyBinding?.progressLinear?.visibility = View.GONE
                artistAllTrackActivtyBinding?.artistTrackLinear?.visibility = View.GONE
                artistAllTrackActivtyBinding?.noInternetLinear?.visibility = View.GONE
            } else if (genreTrackList.status == ResultResponse.Status.LOADING) {
                if (genreTrackList.message == "0") {
                    artistAllTrackActivtyBinding?.noDataLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.progressLinear?.visibility = View.VISIBLE
                    artistAllTrackActivtyBinding?.artistTrackLinear?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.noInternetLinear?.visibility = View.GONE
                }
            }
        })
    }

    override fun onRecommendedItemClick(itemId: Int, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (itemId == playlistManager?.currentItemChange?.currentItem?.id?.toInt() ?: 0) {
                if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    startActivityNowPlaying()
                } else {
                    createLocalPlayList(itemId)
                }
            } else {
                createLocalPlayList(itemId)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    private fun startActivityNowPlaying() {
        var mImageUrlPass = ""
        if (
            playlistManager?.playlistHandler?.currentItemChange != null &&
            playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
        ) {
            mImageUrlPass =
                playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

        }
        val detailIntent =
            Intent(this, NowPlayingActivity::class.java)
        detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
        startActivity(detailIntent)
    }

    private fun createLocalPlayList(itemId: Int) {
        // TODO : Check if playing older position
        if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 && mPlayPostion != -1) {
            mGlobalGenreList?.get(mPlayPostion)?.isPlaying = false
            mGenreAdapter?.notifyItemChanged(mPlayPostion)
        }
        mGlobalGenreList?.map {
            if (it.numberId == itemId.toString()) {
                it.isPlaying = true
                mPlayPostion = mGlobalGenreList?.indexOf(it)!!
            }
        }
        mGenreAdapter?.notifyItemChanged(mPlayPostion)
        artistViewModel.createPlayList(mGlobalGenreList, "Artist tracks - "+ mArtistName) { mPlayList ->
            mediaItems.clear()
            for (sample in mPlayList) {
                val mediaItem = MediaItem(sample, true)
                mediaItems.add(mediaItem)
            }
            launchMusic(mPlayPostion)
        }
    }

    private fun launchMusic(itemId: Int) {
        try {
            MyApplication.playlistManager?.setParameters(mediaItems, itemId)
            MyApplication.playlistManager?.id = PLAYLIST_ID.toLong()
            MyApplication.playlistManager?.currentPosition = itemId
            MyApplication.playlistManager?.play(0, false)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "launchMusic_" + e.toString())
        }
    }

    override fun musicOptionsDialog(
        mTrackCommon: TrackListCommon, isDeleteEnabled: Boolean, mAdapterPosition: Int
    ) {

        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
            sheetView.findViewById(R.id.add_play_list_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
            sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE

        val mTrackId: String? = mTrackCommon.id
        var isLiked: String? = mTrackCommon.favorite
        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }
        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                artistViewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "false"
                    }
                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                artistViewModel.removeFavourites(mAuth, mTrackId)
            }
        }

        val mAuth =
            RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        artistViewModel.getPlayListApi(mAuth)

        artistViewModel.playListModel.nonNull().observe(this, {
            if (it.isNotEmpty()) {
                mPlayList = it as MutableList
                playListRecyclerView.adapter = CommonPlayListAdapter(
                    mPlayList, this,
                    false, glideRequestManager
                )

                // TODO : Update playlist ( Home page )
                EventBus.getDefault().postSticky(UpdatePlaylist(mPlayList!!))
            }

        })
        albumLinear.setOnClickListener {
            val intent = Intent(this, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackCommon.album?.id)
            intent.putExtra("title", mTrackCommon.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivityForResult(intent, 565)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackCommon.artist?.id)
            startActivity(detailIntent)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                context = this,
                recyclerView = playListRecyclerView,
                mListener = object : RecyclerItemClickListener.ClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //TODO : Call Add to playlist API
                        if (mPlayList != null && mPlayList!![position].id != null) {
                            addTrackToPlayList(mTrackCommon.id!!, mPlayList!![position].id!!)
                        }
                        mBottomSheetDialog.dismiss()
                    }

                    override fun onLongItemClick(view: View?, position: Int) {

                    }
                })
        )
        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(mTrackCommon.id!!, this)
            mBottomSheetDialog.dismiss()
        }
    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)

        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                        && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                            "0", playListNameEditText.text.toString(), isPrivate,
                            mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }

                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }
    }

    private fun createPlayListApi(
        playListId: String, playListName: String, private: String, mLastId: String
    ) {
        if (InternetUtil.isInternetOn()) {

            val createPlayListBody =
                CreatePlayListBody(playListId, playListName, mLastId, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            artistViewModel.createPlayListApi(mAuth, createPlayListBody)
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                    mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        artistViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            if (resultCode == 1001) {
                if (data != null) {
                    if (data.getStringExtra("mFileUrl") != null) {
                        val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                        if (data.getStringExtra("mLastId") != null) {
                            mLastId = data.getStringExtra("mLastId")!!
                        }
                        if (playListImageView != null) {
                            glideRequestManager
                                .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                                .into(playListImageView!!)
                        }
                    }
                }
            }
        }
    }

    private fun removeLoaderFormList() {
        if (mGlobalGenreList != null &&
            mGlobalGenreList?.size!! > 0 &&
            mGlobalGenreList?.get(mGlobalGenreList?.size!! - 1) != null &&
            mGlobalGenreList?.get(mGlobalGenreList?.size!! - 1)?.id == "0"
        ) {
            val mRemovedPosition = mGlobalGenreList?.size!! - 1
            mGlobalGenreList?.removeAt(mGlobalGenreList?.size!! - 1)
            mGenreAdapter?.notifyItemRemoved(mRemovedPosition)
            isLoaderAdded = false
        }
    }

    private fun checkTrackPlayStatus() {
        // TODO : If track is playing change status ( isPlaying )
        try {
            if (MyApplication.playlistManager != null &&
                MyApplication.playlistManager?.getPlayList() != null &&
                MyApplication.playlistManager?.getPlayList()?.size!! > 0 &&
                MyApplication.playlistManager?.currentPosition != null &&
                MyApplication.playlistManager?.currentPosition != -1 &&
                MyApplication.playlistManager?.playlistHandler != null &&
                mGlobalGenreList != null && mGlobalGenreList?.isNotEmpty()!!
            ) {

                if (MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    // TODO : Check if playing older position
                    if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 && mPlayPostion != -1) {
                        mGlobalGenreList?.get(mPlayPostion)?.isPlaying = false
                        mGenreAdapter?.notifyItemChanged(mPlayPostion)

                    }

                    // TODO : Checking if the current list track is playing
                    mGlobalGenreList?.map {
                        if (it.numberId == MyApplication.playlistManager?.getPlayList()?.get(
                                MyApplication.playlistManager?.currentPosition!!
                            )?.id.toString()
                        ) {
                            it.isPlaying = true
                            mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                        }
                    }

                    //TODO : Notify play animation
                    if (mPlayPostion != -1) {
                        mGenreAdapter?.notifyItemChanged(mPlayPostion)
                    }
                } else {
                    mGlobalGenreList?.map {
                        if (it.numberId == MyApplication.playlistManager?.getPlayList()?.get(
                                MyApplication.playlistManager?.currentPosition!!
                            )?.id.toString()
                        ) {
                            it.isPlaying = false
                        }
                    }
                    if (mPlayPostion != -1) {
                        mGenreAdapter?.notifyItemChanged(mPlayPostion)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR", "checkTrackPlayStatus_" + e.toString())
        }
    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        if (mTrackIdGlobal != currentItem?.id.toString() || mTrackIdGlobal == currentItem?.id.toString()) {
            checkTrackPlayStatus()
            mTrackIdGlobal = currentItem?.id.toString()
        }
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.PLAYING -> {
                val itemChange = playlistManager?.currentItemChange
                if (itemChange != null) {
                    onPlaylistItemChanged(
                        itemChange.currentItem,
                        itemChange.hasNext,
                        itemChange.hasPrevious
                    )
                }
                try {
                    artistAllTrackActivtyBinding?.fab?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.gifProgress?.visibility = View.VISIBLE
                    artistAllTrackActivtyBinding?.gifProgress?.playAnimation()
                    artistAllTrackActivtyBinding?.gifProgress?.bringToFront()
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d("AmR_", "PlaybackState.PLAYING" + e.toString())
                }
            }
            PlaybackState.ERROR -> {
            }
            PlaybackState.PAUSED -> {
                try {
                    artistAllTrackActivtyBinding?.gifProgress?.visibility = View.GONE
                    artistAllTrackActivtyBinding?.fab?.visibility = View.VISIBLE
                    artistAllTrackActivtyBinding?.gifProgress?.pauseAnimation()
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d("AmR_", "PlaybackState.PAUSED" + e.toString())
                }
                updatePause()
            }
            PlaybackState.RETRIEVING -> {

            }
            PlaybackState.PREPARING -> {

            }
            PlaybackState.SEEKING -> {

            }
            PlaybackState.STOPPED -> {

            }
        }
        return true
    }

    private fun updatePause() {
        try {
            if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0) {
                if (MyApplication.playlistManager?.getPlayList()?.size!! > MyApplication.playlistManager?.currentPosition!!) {
                    mGlobalGenreList?.map {
                        if (it.numberId == MyApplication.playlistManager?.getPlayList()?.get(
                                MyApplication.playlistManager?.currentPosition!!
                            )?.id.toString()
                        ) {
                            it.isPlaying = false
                            mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                        }
                    }
                }
            }
            if (mPlayPostion != -1) {
                mGenreAdapter?.notifyItemChanged(mPlayPostion)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "updatePause_" + e.toString())
        }
    }

    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        try {
            playlistManager = MyApplication.playlistManager
            playlistManager?.registerPlaylistListener(this)

            if (playlistManager != null && playlistManager?.playlistHandler != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                artistAllTrackActivtyBinding?.fab?.visibility = View.GONE
                artistAllTrackActivtyBinding?.gifProgress?.visibility = View.VISIBLE
                artistAllTrackActivtyBinding?.gifProgress?.playAnimation()
                artistAllTrackActivtyBinding?.gifProgress?.bringToFront()
                checkTrackPlayStatus()
            } else {
                artistAllTrackActivtyBinding?.gifProgress?.visibility = View.GONE
                artistAllTrackActivtyBinding?.fab?.visibility = View.VISIBLE
                artistAllTrackActivtyBinding?.gifProgress?.pauseAnimation()
                updatePause()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR", "onResume" + e.toString())
        }
    }
}
