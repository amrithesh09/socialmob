package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArtistDetailsActivtyBinding
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.ShareAppUtils


class ArtistDetailsActivity : BaseCommonActivity() {
    private var artistPlayListActivityBinding: ArtistDetailsActivtyBinding? = null
    private var mArtistId: String = ""
    private var mArtistProfileId: String = ""
    private var mArtistPath: String = ""

    companion object {
        var mArtist = Artist()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        artistPlayListActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.artist_details_activty)

        val i: Intent? = intent
        mArtistId = i?.getStringExtra("mArtistId")!!
        mArtistProfileId = i.getStringExtra("mArtistProfileId")!!
        mArtistPath = i.getStringExtra("mArtistPath")!!

        if (mArtistProfileId == "") {
            artistPlayListActivityBinding?.goToProfile?.visibility = View.GONE
        }

        if (mArtistPath != null) {
            if (mArtistPath.equals("ArtistPlay")) {
                if (ArtistPlayListActivity.getViewModel() != null) {
                    val mArtistData = ArtistPlayListActivity.getViewModel()
                    artistPlayListActivityBinding?.artistViewModel = mArtistData?.artist
                    artistPlayListActivityBinding?.executePendingBindings()
                }
            } else {
                if (mArtist.artistName != null) {
                    val mArtistData = mArtist
                    artistPlayListActivityBinding?.artistViewModel = mArtistData
                    artistPlayListActivityBinding?.executePendingBindings()
                }
            }
        }


        artistPlayListActivityBinding?.goToProfile?.setOnClickListener {
            if (mArtistProfileId != "") {
                val intent = Intent(this, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mArtistProfileId)
                startActivity(intent)
            } else {
                AppUtils.showCommonToast(this, "No profile id")
            }
        }
        artistPlayListActivityBinding?.shareArtist?.setOnClickListener {
            ShareAppUtils.shareArtist(this, mArtistId)
        }
        artistPlayListActivityBinding?.artistBackImageView?.setOnClickListener {
            finish()
        }
    }
}
