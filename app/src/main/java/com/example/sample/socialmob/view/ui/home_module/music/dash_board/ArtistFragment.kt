package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArtistFragmentBinding
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.model.music.ArtistDetails
import com.example.sample.socialmob.model.music.ArtistDetailsPayload
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.music.ArtistVideoDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.viewmodel.music_menu.ArtistViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


@AndroidEntryPoint
class ArtistFragment : Fragment(), TopTrackArtistAdapter.TopTrackItemClickListener,
    TopTrackArtistAdapter.AddToPlayListClickListener, ArtistsAdapter.ArtistClickListener,
    ArtistsAlbumAdapter.ArtistAlbumClickListener, TopTrackArtistAdapter.MusicOptionsDialog {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mArtistProfileId = ""
    private var mArtistId = ""
    private var mArtistName = ""
    private var mArtistVideoId = ""
    private var mArtist = Artist()
    private var resultResponse: ArtistDetailsPayload? = null
    private var binding: ArtistFragmentBinding? = null
    private val artistViewModel: ArtistViewModel by viewModels()
    private var mGlobalGenreList: MutableList<TrackListCommon>? = ArrayList()
    private var mLastClickTime: Long = 0
    private val mediaItems = LinkedList<MediaItem>()
    private var mTrackAdapter: TopTrackArtistAdapter? = null
    private var mPlayPostion = -1
    private var PLAYLIST_ID = 4 //Arbitrary, for the example
    private var mArtistAdapter: ArtistsAdapter? = null
    private var mUrl = ""
    private var isApiCalled = false
    fun newInstance(mArtistId: String): ArtistFragment {
        val args = Bundle()
        args.putString("mArtistId", mArtistId)
        val fragment = ArtistFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.artist_fragment, container, false
        )
        return binding?.root
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (mArtist.profileId == null || mArtist.profileId == "") {
                callApiCommon()
                binding?.focusEditText?.requestFocus()
                binding?.nestedScrollView?.post {
                    binding?.nestedScrollView?.fling(0)
                    binding?.nestedScrollView?.smoothScrollTo(0, 0)
                }
                updatePlayPause()
            } else {
                executeBinding()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val args = arguments
        if (args?.containsKey("mArtistId")!!) {
            mArtistId = args.getString("mArtistId", "")!!
        }

        if (activity != null) {
            mTrackAdapter = TopTrackArtistAdapter(
                topTrackItemClickListener = this,
                mMusicOptionsDialog = this
            )
            binding?.topTrackRecyclerView?.adapter = mTrackAdapter

            mArtistAdapter = ArtistsAdapter(
                mArtistClickListener = this,
                isFrom = "artistDetails",
                mContext = requireActivity(), requestManager = glideRequestManager
            )
            binding?.artistRecyclerView?.adapter = mArtistAdapter

            binding?.roundImageView?.setOnClickListener {
                binding?.artistInfoImageView?.performClick()
            }
            binding?.artistInfoImageView?.setOnClickListener {
                (activity as ArtistViewPagerActivity).gotoArtistInfo(
                    mArtistId,
                    mArtistProfileId, mArtist
                )
            }
            binding?.featuredVideoCard?.setOnClickListener {
                if (mArtistId != "") {
                    val intent = Intent(activity, ArtistVideoDetailsActivity::class.java)
                    intent.putExtra("mUrl", mUrl)
                    intent.putExtra("mArtistId", mArtistVideoId)
                    intent.putExtra("isFromViewAll", "false")
                    startActivityForResult(intent, 10)
                }
            }
            binding?.featuredVideoViewAll?.setOnClickListener {
                if (mArtistId != "") {
                    val intent = Intent(activity, ArtistVideoViewAllActivity::class.java)
                    intent.putExtra("mArtistId", mArtistId)
                    startActivity(intent)
                }
            }
            binding?.topTrackViewAll?.setOnClickListener {
                if (mArtistId != "") {
                    val intent = Intent(activity, ArtistAllTracksActivity::class.java)
                    intent.putExtra("mArtistId", mArtistId)
                    intent.putExtra("mArtistName", mArtistName)
                    startActivity(intent)
                }
            }

            binding?.shareArtist?.setOnClickListener {
                ShareAppUtils.shareArtist(requireActivity(), mArtistId)
            }

            binding?.followingTextView?.setOnClickListener {
                if (InternetUtil.isInternetOn()) {
                    if (mArtist.relation == "none") {
                        artistViewModel.followArtist(true, mArtistId, requireActivity())
                        mArtist.relation = "following"
                    } else {
                        artistViewModel.followArtist(false, mArtistId, requireActivity())
                        mArtist.relation = "none"
                    }
                    binding?.invalidateAll()
                } else {
                    Toast.makeText(activity, getString(R.string.no_internet), Toast.LENGTH_SHORT)
                        .show()
                }
            }
            binding?.artistProfileImageView?.setOnClickListener {
                if (mArtistProfileId != "") {
                    val intent =
                        Intent(activity, UserProfileActivity::class.java)
                    intent.putExtra("mProfileId", mArtistProfileId)
                    startActivity(intent)
                } else {
                    AppUtils.showCommonToast(requireActivity(), "No profile id")
                }
            }
        }
        if (mArtist.profileId == null || mArtist.profileId == "") {
            callApiCommon()
        } else {
            executeBinding()
        }

    }

    /*override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments
        if (args?.containsKey("mArtistId")!!) {
            mArtistId = args.getString("mArtistId", "")!!
        }

        if (activity != null) {
            mTrackAdapter = TopTrackArtistAdapter(
                topTrackItemClickListener = this,
                mMusicOptionsDialog = this
            )
            binding?.topTrackRecyclerView?.adapter = mTrackAdapter

            mArtistAdapter = ArtistsAdapter(
                mArtistClickListener = this,
                isFrom = "artistDetails",
                mContext = requireActivity(), requestManager = glideRequestManager
            )
            binding?.artistRecyclerView?.adapter = mArtistAdapter

            binding?.roundImageView?.setOnClickListener {
                binding?.artistInfoImageView?.performClick()
            }
            binding?.artistInfoImageView?.setOnClickListener {
                (activity as ArtistViewPagerActivity).gotoArtistInfo(
                    mArtistId,
                    mArtistProfileId, mArtist
                )
            }
            binding?.featuredVideoCard?.setOnClickListener {
                if (mArtistId != "") {
                    val intent = Intent(activity, ArtistVideoDetailsActivity::class.java)
                    intent.putExtra("mUrl", mUrl)
                    intent.putExtra("mArtistId", mArtistVideoId)
                    intent.putExtra("isFromViewAll", "false")
                    startActivityForResult(intent, 10)
                }
            }
            binding?.featuredVideoViewAll?.setOnClickListener {
                if (mArtistId != "") {
                    val intent = Intent(activity, ArtistVideoViewAllActivity::class.java)
                    intent.putExtra("mArtistId", mArtistId)
                    startActivity(intent)
                }
            }
            binding?.topTrackViewAll?.setOnClickListener {
                if (mArtistId != "") {
                    val intent = Intent(activity, ArtistAllTracksActivity::class.java)
                    intent.putExtra("mArtistId", mArtistId)
                    intent.putExtra("mArtistName", mArtistName)
                    startActivity(intent)
                }
            }

            binding?.shareArtist?.setOnClickListener {
                ShareAppUtils.shareArtist(requireActivity(), mArtistId)
            }

            binding?.followingTextView?.setOnClickListener {
                if (InternetUtil.isInternetOn()) {
                    if (mArtist.relation == "none") {
                        artistViewModel.followArtist(true, mArtistId, requireActivity())
                        mArtist.relation = "following"
                    } else {
                        artistViewModel.followArtist(false, mArtistId, requireActivity())
                        mArtist.relation = "none"
                    }
                    binding?.invalidateAll()
                } else {
                    Toast.makeText(activity, getString(R.string.no_internet), Toast.LENGTH_SHORT)
                        .show()
                }
            }
            binding?.artistProfileImageView?.setOnClickListener {
                if (mArtistProfileId != "") {
                    val intent =
                        Intent(activity, UserProfileActivity::class.java)
                    intent.putExtra("mProfileId", mArtistProfileId)
                    startActivity(intent)
                } else {
                    AppUtils.showCommonToast(requireActivity(), "No profile id")
                }
            }
        }
        if (mArtist.profileId == null || mArtist.profileId == "") {
            callApiCommon()
        } else {
            executeBinding()
        }
    }*/

    private fun callApiCommon() {
        // TODO : Check Internet connection
        try {
            if (!InternetUtil.isInternetOn()) {
                val shake: android.view.animation.Animation =
                    AnimationUtils.loadAnimation(context, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            } else {
                binding?.noDataLinear?.visibility = View.GONE
                binding?.progressLinear?.visibility = View.VISIBLE
                binding?.artistLinear?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
                if (mArtistId != "") {
                    callApi(mArtistId)
                } else {
                    isApiCalled = false
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "callApiCommon_" + e.toString())
        }

    }

    private fun callApi(mArtistId: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + "/api/v2/artist/" + mArtistId + "/detail",
            RemoteConstant.mGetMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        artistDetails(mAuth, mArtistId)

    }

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    private fun artistDetails(mAuth: String, mPlayListId: String) {
        try {
            val serviceApi = WebServiceClient.client.create(BackEndApi::class.java)// get instance
            var response: Response<ArtistDetails>? = null
            GlobalScope.launch(Dispatchers.IO) {
                kotlin.runCatching {
                    response = serviceApi.getArtistDetails(mAuth, mPlayListId)

                }.onSuccess {
                    when (response?.code()) {
                        200 ->
                            if (response?.body() != null) {
                                resultResponse = response?.body()?.payload!!
                                mArtistProfileId = resultResponse?.artist?.profileId!!
                                mArtist = resultResponse?.artist!!

                                executeBinding()
                                GlobalScope.launch(Dispatchers.Main) {
                                    binding?.noDataLinear?.visibility = View.GONE
                                    binding?.progressLinear?.visibility = View.GONE
                                    binding?.artistLinear?.visibility = View.VISIBLE
                                    binding?.noInternetLinear?.visibility = View.GONE
                                }
                            } else {
                                binding?.noDataLinear?.visibility = View.VISIBLE
                                binding?.progressLinear?.visibility = View.GONE
                                binding?.artistLinear?.visibility = View.GONE
                                binding?.noInternetLinear?.visibility = View.GONE
                            }
                        502, 522, 523, 500 -> {
                        }
                        else -> {
                            RemoteConstant.apiErrorDetails(
                                activity, response?.code()!!, "artistDetails api"
                            )
                        }
                    }
                }.onFailure {
                    activity?.runOnUiThread(
                        Runnable {
                            binding?.noDataLinear?.visibility = View.VISIBLE
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.artistLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.GONE
                        }
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "artistDetails_" + e.toString())
        }

    }

    private fun executeBinding() {
        GlobalScope.launch(Dispatchers.Main) {
            binding?.artistViewModel = resultResponse
            if (mArtistProfileId == "") {
                binding?.artistProfileImageView?.visibility = View.GONE
            }

            val mArtistNewList: MutableList<Artist> =
                resultResponse?.similarArtists?.toMutableList()!!
            mArtistAdapter?.submitList(mArtistNewList)

            binding?.albumRecyclerView?.layoutManager =
                WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            binding?.albumRecyclerView?.adapter = ArtistsAlbumAdapter(
                mContext = activity,
                albums = resultResponse?.albums!!,
                mArtistClickListener = this@ArtistFragment, glideRequestManager
            )
            binding?.noDataLinear?.visibility = View.GONE
            binding?.progressLinear?.visibility = View.GONE
            binding?.artistLinear?.visibility = View.VISIBLE
            binding?.noInternetLinear?.visibility = View.GONE

            mGlobalGenreList =
                resultResponse?.topTracks as MutableList<TrackListCommon>
            binding?.topTrackRecyclerView?.post {
                mTrackAdapter?.submitList(mGlobalGenreList)
            }

            mUrl = resultResponse?.artist?.featuredVideo?.media?.master_playlist ?: ""
            if (resultResponse != null &&
                resultResponse?.artist != null &&
                resultResponse?.artist?.featuredVideo != null &&
                resultResponse?.artist?.featuredVideo?.artist != null &&
                resultResponse?.artist?.featuredVideo?.artist?.size ?: 0 > 0

            ) {
                 mArtistName = ""
                val artist = resultResponse?.artist?.featuredVideo?.artist
                for (i in 0 until artist?.size!!) {
                    mArtistName = if (artist.size == 1) {
                        artist[i].artistName!!
                    } else {
                        if (mArtistName != "") {
                            mArtistName + " , " + artist[i].artistName
                        } else {
                            artist[i].artistName!!
                        }
                    }
                }
                binding?.artistDetailsTextView?.text =
                    mArtistName + " - " + resultResponse?.artist?.featuredVideo?.viewCount + " views"

            }


            if (resultResponse?.artist != null &&
                resultResponse?.artist?.featured_video_id != null
            ) {
                mArtistVideoId = resultResponse?.artist?.featured_video_id!!
            }
            when {
                resultResponse?.artist?.video_count?.toInt() == 0 -> {
                    binding?.featuredVideoLinear?.visibility = View.GONE
                }
                resultResponse?.artist?.video_count?.toInt()!! > 0 -> {
                    binding?.featuredVideoViewAll?.visibility = View.VISIBLE
                }
                else -> {
                    binding?.featuredVideoViewAll?.visibility = View.GONE
                }
            }

            if (resultResponse?.artist?.featuredVideo != null) {
                binding?.featuredVideoLinear?.visibility = View.VISIBLE
            } else {
                binding?.featuredVideoLinear?.visibility = View.GONE
            }
            binding?.focusEditText?.clearFocus()
            binding?.focusEditText?.requestFocus()
            binding?.nestedScrollView?.post {
                binding?.nestedScrollView?.fling(0)
                binding?.nestedScrollView?.smoothScrollTo(0, 0)
            }
            isApiCalled = true
            updatePlayPause()
        }
    }


    override fun onTopTrackItemClick(itemId: String, itemImage: String, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (itemId == MyApplication.playlistManager?.currentItemChange?.currentItem?.id?.toString() ?: 0) {
                if (MyApplication.playlistManager != null &&
                    MyApplication.playlistManager?.playlistHandler != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    startActivityNowPlaying()
                } else {
                    createLocalPlayList(itemId,"Artist tracks - "+ mArtist.artistName)
                }
            } else {
                createLocalPlayList(itemId,"Artist tracks - "+ mArtist.artistName)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    private fun startActivityNowPlaying() {

        var mImageUrlPass = ""
        if (
            playlistManager?.playlistHandler?.currentItemChange != null &&
            playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
        ) {
            mImageUrlPass =
                playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

        }
        val detailIntent =
            Intent(activity, NowPlayingActivity::class.java)
        detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
        startActivity(detailIntent)
    }

    private fun createLocalPlayList(itemId: String, playListName: String) {
        // TODO : Check if playing older position
        if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 && mPlayPostion != -1) {
            mGlobalGenreList?.get(mPlayPostion)?.isPlaying = false
            mTrackAdapter?.notifyItemChanged(mPlayPostion)
        }

        mGlobalGenreList?.map {
            if (it.numberId == itemId) {
                it.isPlaying = true
                mPlayPostion = mGlobalGenreList?.indexOf(it)!!
            }
        }
        mTrackAdapter?.notifyItemChanged(mPlayPostion)

        artistViewModel.createPlayList(mGlobalGenreList, playListName){mPlayList->
            mediaItems.clear()
            for (sample in mPlayList) {
                val mediaItem = MediaItem(sample, true)
                mediaItems.add(mediaItem)
            }
            launchMusic(mPlayPostion)
        }
    }

    private fun launchMusic(itemId: Int) {
        MyApplication.playlistManager?.setParameters(mediaItems, itemId)
        MyApplication.playlistManager?.id = PLAYLIST_ID.toLong()
        MyApplication.playlistManager?.currentPosition = itemId
        MyApplication.playlistManager?.play(0, false)
        (activity as ArtistViewPagerActivity).onPlaybackStateChanged(PlaybackState.PLAYING)
    }

    override fun onAddToPlayListItemClick(mTrackId: String) {

    }

    override fun onArtistClickListener(
        mArtistId: String, mArtistPosition: String, mIsFrom: String
    ) {
        val detailIntent = Intent(activity, ArtistPlayListActivity::class.java)
        detailIntent.putExtra("mArtistId", mArtistId)
        detailIntent.putExtra("mArtistPosition", mArtistPosition)
        startActivityForResult(detailIntent, 500)
        activity?.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    override fun onArtistAlbumClickListener(
        mArtistId: String, mTitle: String,
        imageUrl: String, mDescription: String
    ) {
        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", mArtistId)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", imageUrl)
        intent.putExtra("mDescription", mDescription)
        intent.putExtra("mIsFrom", "artistAlbum")
        startActivity(intent)
    }

    override fun musicOptionsDialog(mTrackListCommon: TrackListCommon) {
        if (activity != null) {
            (activity as ArtistViewPagerActivity).musicOptionsDialog(mTrackListCommon)
        }
    }

    private var playlistManager: PlaylistManager? = null
    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        updatePlayPause()
    }

    private fun updatePlayPause() {
        if (playlistManager != null
            && playlistManager?.playlistHandler != null
            && playlistManager?.playlistHandler?.currentMediaPlayer != null
            && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
            && playlistManager?.currentItem != null
            && playlistManager?.currentItem?.id != null
        ) {
            if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                val mTrackId =
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id?.toString()

                mGlobalGenreList?.map {
                    if (it.numberId == mTrackId) {
                        mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                    }
                }
                updatePLaying(mTrackId)
            } else {
                updatePause()
            }
        }
    }

    private fun updatePause() {
        mGlobalGenreList?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
            }
        }
        mTrackAdapter?.notifyDataSetChanged()

    }

    private fun updatePLaying(mTrackId: String?) {
        mGlobalGenreList?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                println("" + true)
            } else {
                it.isPlaying = false
                println("" + false)
            }
        }
        mTrackAdapter?.notifyDataSetChanged()
    }
}