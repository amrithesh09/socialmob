package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArtistPlayListActivtyBinding
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.model.music.ArtistDetailsPayload
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.ArtistVideoDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.ArtistViewModel
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.artist_play_list_activty.*
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class ArtistPlayListActivity : BaseCommonActivity(),
        TopTrackArtistAdapter.TopTrackItemClickListener,
        TopTrackArtistAdapter.AddToPlayListClickListener, ArtistsAdapter.ArtistClickListener,
        ArtistsAlbumAdapter.ArtistAlbumClickListener, PlaylistListener<MediaItem>,
        TopTrackArtistAdapter.MusicOptionsDialog {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private var artistPlayListActivityBinding: ArtistPlayListActivtyBinding? = null
    private var mArtistId: String = ""
    private var mArtistName: String = ""
    private var mArtistNamePlayList: String = ""
    private var mArtistProfileId: String = ""
    private var mGlobalGenreList: MutableList<TrackListCommon>? = ArrayList()
    private var playListImageView: ImageView? = null
    private var mLastId: String = ""
    private var mLastClickTime: Long = 0
    private val mediaItems = LinkedList<MediaItem>()
    private var mTrackAdapter: TopTrackArtistAdapter? = null
    private var mPlayPostion = -1
    private var PLAYLIST_ID = 4 //Arbitrary, for the example
    private var mArtistVideoId = ""
    private var mUrl = ""
    private val artistViewModel: ArtistViewModel by viewModels()
    private val searchViewModel: SearchViewModel by viewModels()

    // Track Download
    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"

    companion object {
        private var mArtistData: ArtistDetailsPayload? = null
        fun getViewModel(): ArtistDetailsPayload? {
            return mArtistData
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        artistPlayListActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.artist_play_list_activty)

        if (intent != null && intent.data != null) {

            // TODO : Share play list Track
            val data: Uri? = intent?.data
            if (data?.lastPathSegment != null) {
                mArtistId = data.lastPathSegment!!
            }
        }


        if (intent != null) {
            val i: Intent? = intent
            if (i != null && i.hasExtra("mArtistName")) {
                mArtistName = i.getStringExtra("mArtistName")!!
            }
            if (i != null && i.hasExtra("mArtistId")) {
                mArtistId = i.getStringExtra("mArtistId")!!
            }
        }

        // TODO : Track download

        val fetchConfiguration = FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(4)
                .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE)
//            .setNotificationManager(object : DefaultFetchNotificationManager(requireContext()) {
//                override fun getFetchInstanceForNamespace(namespace: String): Fetch {
//                    return fetch!!
//                }
//            })
                .build()
        fetch = Fetch.getInstance(fetchConfiguration)


        // TODO : Download offlie track pending
        if (searchViewModel.downloadFalseTrackListModel != null) {
            searchViewModel.downloadFalseTrackListModel?.nonNull()
                    ?.observe(this, { trackListCommonDbs ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            this.trackListCommonDbs = trackListCommonDbs

                        }
                    })
        }

        callApiCommon()


        artistPlayListActivityBinding?.featuredVideoCard?.setOnClickListener {
            if (mArtistId != "") {
                val intent = Intent(this, ArtistVideoDetailsActivity::class.java)
                intent.putExtra("mArtistId", mArtistVideoId)
                intent.putExtra("mUrl", mUrl)
                intent.putExtra("isFromViewAll", "false")
                startActivityForResult(intent, 10)
            }
        }
        artistPlayListActivityBinding?.featuredVideoLinear?.setOnClickListener {
            artistPlayListActivityBinding?.featuredVideoCard?.performClick()
        }
        artistPlayListActivityBinding?.featuredVideoViewAll?.setOnClickListener {
            if (mArtistId != "") {
                val intent = Intent(this, ArtistVideoViewAllActivity::class.java)
                intent.putExtra("mArtistId", mArtistId)
                startActivity(intent)
            }
        }

        artistPlayListActivityBinding?.followingTextView?.setOnClickListener {
            if (InternetUtil.isInternetOn()) {
                if (artistPlayListActivityBinding?.relation == "none") {
                    artistViewModel.followArtist(true, mArtistId, this)
                    artistPlayListActivityBinding?.relation = "following"
                    mArtistData?.artist?.relation = "following"
                } else {
                    artistViewModel.followArtist(false, mArtistId, this)
                    artistPlayListActivityBinding?.relation = "none"
                    mArtistData?.artist?.relation = "none"
                }
                artistPlayListActivityBinding?.invalidateAll()
            } else {
                AppUtils.showCommonToast(this, getString(R.string.no_internet))
            }
        }

        artistPlayListActivityBinding?.artistProfileImageView?.setOnClickListener {
            if (mArtistProfileId != "") {
                val intent = Intent(this, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mArtistProfileId)
                startActivity(intent)
            } else {
                AppUtils.showCommonToast(this, "No profile id")
            }
        }
        artistPlayListActivityBinding?.shareArtist?.setOnClickListener {
            ShareAppUtils.shareArtist(this, mArtistId)
        }
        artistPlayListActivityBinding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }
        artistPlayListActivityBinding?.artistBackImageView?.setOnClickListener {
            onBackPressed()
        }

        artistPlayListActivityBinding?.topTrackViewAll?.setOnClickListener {
            val intent = Intent(this, ArtistAllTracksActivity::class.java)
            intent.putExtra("mArtistId", mArtistId)
            intent.putExtra("mArtistName", mArtistName)
            startActivity(intent)
        }

        artistPlayListActivityBinding?.roundImageView?.setOnClickListener {
            artistPlayListActivityBinding?.artistInfoImageView?.performClick()
        }
        artistPlayListActivityBinding?.artistInfoImageView?.setOnClickListener {

            val intent = Intent(this, ArtistDetailsActivity::class.java)
            intent.putExtra("mArtistId", mArtistId)
            intent.putExtra("mArtistPath", "ArtistPlay")
            intent.putExtra("mArtistProfileId", mArtistProfileId)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this,
                    round_image_view,
                    getString(R.string.image_transition_name)
            )
            startActivity(intent, options.toBundle())

        }
        artistPlayListActivityBinding?.gifProgress?.setOnClickListener { view: View? -> artistPlayListActivityBinding?.fab?.performClick() }
        artistPlayListActivityBinding?.fab?.setOnClickListener {
            if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                var mImageUrlPass = ""
                if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                }
                val detailIntent =
                        Intent(this@ArtistPlayListActivity, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                startActivity(detailIntent)
            } else if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null
            ) {
                var mImageUrlPass = ""
                if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                }
                val detailIntent =
                        Intent(this@ArtistPlayListActivity, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                startActivity(detailIntent)
            }
        }
    }

    private fun callApiCommon() {

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            artistViewModel.artistModel?.postValue(ResultResponse.noInternet(ArtistDetailsPayload()))
            val shake: android.view.animation.Animation =
                    AnimationUtils.loadAnimation(this, R.anim.shake)
            artistPlayListActivityBinding?.noInternetLinear?.startAnimation(shake) // starts animation
        } else {
            callApi(mArtistId)
        }
        observeData()
    }

    private fun callApi(mArtistId: String) {

        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + "/api/v2/artist/" + mArtistId + "/detail",
                RemoteConstant.mGetMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mAppId,
                ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        artistViewModel.artistDetails(mAuth, mArtistId)

    }


    private fun observeData() {

        artistViewModel.artistModel?.nonNull()?.observe(this, { resultResponse ->
            when (resultResponse.status) {
                ResultResponse.Status.SUCCESS -> {
                    mArtistData = resultResponse?.data
                    artistPlayListActivityBinding?.artistViewModel = mArtistData

                    mArtistProfileId = resultResponse?.data?.artist?.profileId!!
                    mArtistNamePlayList = resultResponse.data.artist.artistName ?: ""
                    artistPlayListActivityBinding?.relation =
                            resultResponse.data.artist.relation!!
                    if (mArtistProfileId == "") {
                        artistPlayListActivityBinding?.artistProfileImageView?.visibility =
                                View.GONE
                    }

                    mGlobalGenreList =
                            resultResponse.data.topTracks as MutableList<TrackListCommon>
                    mTrackAdapter = TopTrackArtistAdapter(
                            topTrackItemClickListener = this,
                            mMusicOptionsDialog = this
                    )
                    artistPlayListActivityBinding?.topTrackRecyclerView?.adapter = mTrackAdapter
                    mTrackAdapter?.submitList(mGlobalGenreList)

                    val mArtistAdapter = ArtistsAdapter(
                            mArtistClickListener = this,
                            isFrom = "artistDetails",
                            mContext = this, requestManager = glideRequestManager
                    )
                    artistPlayListActivityBinding?.artistRecyclerView?.adapter = mArtistAdapter
                    val mArtistNewList: MutableList<Artist> =
                            resultResponse.data.similarArtists?.toMutableList()!!
                    mArtistAdapter.submitList(mArtistNewList)

                    artistPlayListActivityBinding?.albumRecyclerView?.adapter = ArtistsAlbumAdapter(
                            mContext = this,
                            albums = resultResponse.data.albums!!,
                            mArtistClickListener = this,
                            glideRequestManager = glideRequestManager
                    )
                    mUrl = resultResponse.data.artist.featuredVideo?.media?.master_playlist ?: ""
                    if (resultResponse.data.artist.featuredVideo?.artist != null &&
                            resultResponse.data.artist.featuredVideo.artist.isNotEmpty()

                    ) {
                        mArtistName = ""
                        val artist = resultResponse.data.artist.featuredVideo.artist
                        for (i in artist.indices) {
                            mArtistName = if (artist.size == 1) {
                                artist[i].artistName!!
                            } else {
                                if (mArtistName != "") {
                                    mArtistName + " , " + artist[i].artistName
                                } else {
                                    artist[i].artistName!!
                                }
                            }
                        }
                        artistPlayListActivityBinding?.artistDetailsTextView?.text =
                                mArtistName + " - " + resultResponse.data.artist.featuredVideo.viewCount + " views"

                    }


                    if (resultResponse.data.artist.featured_video_id != null
                    ) {
                        mArtistVideoId = resultResponse.data.artist.featured_video_id
                    }
                    when {
                        resultResponse.data.artist.video_count?.toInt() == 0 -> {
                            artistPlayListActivityBinding?.featuredVideoLinear?.visibility =
                                    View.GONE
                        }
                        resultResponse.data.artist.video_count?.toInt()!! > 0 -> {
                            artistPlayListActivityBinding?.featuredVideoViewAll?.visibility =
                                    View.VISIBLE
                        }
                        else -> {
                            artistPlayListActivityBinding?.featuredVideoViewAll?.visibility =
                                    View.GONE
                        }
                    }
                    if (resultResponse.data.artist.featuredVideo != null) {
                        artistPlayListActivityBinding?.featuredVideoLinear?.visibility =
                                View.VISIBLE
                    } else {
                        artistPlayListActivityBinding?.featuredVideoLinear?.visibility = View.GONE
                    }
                    checkTrackPlayStatus()
                    artistPlayListActivityBinding?.noDataLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.progressLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.artistLinear?.visibility = View.VISIBLE
                    artistPlayListActivityBinding?.noInternetLinear?.visibility = View.GONE

                }
                ResultResponse.Status.ERROR -> {
                    artistPlayListActivityBinding?.noDataLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.progressLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.artistLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.noInternetLinear?.visibility = View.VISIBLE
                    glideRequestManager.load(R.drawable.server_error).apply(
                            RequestOptions().fitCenter())
                            .into(artistPlayListActivityBinding?.noInternetImageView!!)
                    artistPlayListActivityBinding?.noInternetTextView?.text =
                            getString(R.string.server_error)
                    artistPlayListActivityBinding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)
                }
                ResultResponse.Status.NO_DATA -> {
                    artistPlayListActivityBinding?.noDataLinear?.visibility = View.VISIBLE
                    artistPlayListActivityBinding?.progressLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.artistLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.noInternetLinear?.visibility = View.GONE
                }
                ResultResponse.Status.LOADING -> {
                    artistPlayListActivityBinding?.noDataLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.progressLinear?.visibility = View.VISIBLE
                    artistPlayListActivityBinding?.artistLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.noInternetLinear?.visibility = View.GONE
                }
                ResultResponse.Status.NO_INTERNET -> {
                    artistPlayListActivityBinding?.noDataLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.progressLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.artistLinear?.visibility = View.GONE
                    artistPlayListActivityBinding?.noInternetLinear?.visibility = View.VISIBLE
                }
                else -> {

                }
            }
        })
    }


    override fun onTopTrackItemClick(
            itemId: String, itemImage: String, isFrom: String
    ) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {

            if (itemId == playlistManager?.currentItemChange?.currentItem?.id?.toString() ?: 0) {
                if (playlistManager != null &&
                        playlistManager?.playlistHandler != null &&
                        playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                        playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    startActivityNowPlaying()
                } else {
                    createLocalPlayList(itemId, "Artist tracks - " + mArtistNamePlayList)
                }
            } else {
                createLocalPlayList(itemId, "Artist tracks - " + mArtistNamePlayList)
            }

        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    private fun startActivityNowPlaying() {

        var mImageUrlPass = ""
        if (
                playlistManager?.playlistHandler?.currentItemChange != null &&
                playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
        ) {
            mImageUrlPass =
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

        }
        val detailIntent =
                Intent(this, NowPlayingActivity::class.java)
        detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
        startActivity(detailIntent)
    }

    private fun createLocalPlayList(itemId: String, playListName: String) {
        // TODO : Check if playing older position
        if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 && mPlayPostion != -1) {
            mGlobalGenreList?.get(mPlayPostion)?.isPlaying = false
            mTrackAdapter?.notifyItemChanged(mPlayPostion)
        }

        mGlobalGenreList?.map {
            if (it.numberId == itemId) {
                it.isPlaying = true
                mPlayPostion = mGlobalGenreList?.indexOf(it)!!
            }
        }
        mTrackAdapter?.notifyItemChanged(mPlayPostion)

        artistViewModel.createPlayList(mGlobalGenreList, playListName) { mPlayList ->
            mediaItems.clear()
            for (sample in mPlayList) {
                val mediaItem = MediaItem(sample, true)
                mediaItems.add(mediaItem)
            }
            launchMusic(mPlayPostion)
        }
    }

    private fun launchMusic(itemId: Int) {

        MyApplication.playlistManager?.setParameters(mediaItems, itemId)
        MyApplication.playlistManager?.id = PLAYLIST_ID.toLong()
        MyApplication.playlistManager?.currentPosition = itemId
        MyApplication.playlistManager?.play(0, false)

    }

    override fun onAddToPlayListItemClick(mTrackId: String) {

    }

    override fun onArtistClickListener(
            mArtistId: String,
            mArtistPosition: String,
            mIsFrom: String
    ) {

        val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
        detailIntent.putExtra("mArtistId", mArtistId)
        detailIntent.putExtra("mArtistPosition", mArtistPosition)
        startActivityForResult(detailIntent, 500)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    override fun onArtistAlbumClickListener(
            mArtistId: String,
            mTitle: String,
            imageUrl: String,
            mDescription: String
    ) {
        val intent = Intent(this, CommonPlayListActivity::class.java)
        intent.putExtra("mId", mArtistId)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", imageUrl)
        intent.putExtra("mDescription", mDescription)
        intent.putExtra("mIsFrom", "artistAlbum")
        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, R.anim.play_panel_close_background)
        AppUtils.hideKeyboard(
                this@ArtistPlayListActivity, artistPlayListActivityBinding?.topTrackRecyclerView!!
        )
    }

    override fun musicOptionsDialog(mTrackListCommon: TrackListCommon) {
        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView: View = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
                sheetView.findViewById(R.id.add_play_list_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
                sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE


        val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)


        val allowOfflineDownload: String? = mTrackListCommon.allowOfflineDownload
        if (allowOfflineDownload == "1") {
            downloadLinear.visibility = View.VISIBLE
        } else {
            downloadLinear.visibility = View.GONE
        }

        downloadLinear.setOnClickListener {
            startDownload(mTrackListCommon)
            mBottomSheetDialog.dismiss()
        }
        val mTrackId: String? = mTrackListCommon.id
        var isLiked: String? = mTrackListCommon.favorite
        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }


        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                        UpdateTrackEvent(
                                mTrackId!!,
                                isLiked!!
                        )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                        )!!,
                        RemoteConstant.mBaseUrl +
                                RemoteConstant.trackData +
                                RemoteConstant.mSlash + mTrackId +
                                RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                artistViewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                        UpdateTrackEvent(
                                mTrackId!!,
                                isLiked!!
                        )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "false"
                    }

                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                        SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                        )!!,
                        RemoteConstant.mBaseUrl +
                                RemoteConstant.trackData +
                                RemoteConstant.mSlash + mTrackId +
                                RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                artistViewModel.removeFavourites(mAuth, mTrackId)

            }

        }


        val mAuth =
                RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        artistViewModel.getPlayListApi(mAuth)

        artistViewModel.playListModel.nonNull().observe(this, {
            if (it.isNotEmpty()) {
                mPlayList = it as MutableList
                playListRecyclerView.adapter = CommonPlayListAdapter(
                        mPlayList, this,
                        false, glideRequestManager
                )

                // TODO : Update playlist ( Home page )
                EventBus.getDefault().postSticky(UpdatePlaylist(mPlayList!!))
            }

        })
        albumLinear.setOnClickListener {
            val intent = Intent(this, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackListCommon.album?.id)
            intent.putExtra("title", mTrackListCommon.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivity(intent)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackListCommon.artist?.id)
            detailIntent.putExtra("mArtistName", mTrackListCommon.artist?.artistName)
            startActivity(detailIntent)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
                RecyclerItemClickListener(
                        context = this,
                        recyclerView = playListRecyclerView,
                        mListener = object : RecyclerItemClickListener.ClickListener {
                            override fun onItemClick(view: View, position: Int) {
                                //TODO : Call Add to playlist API
                                if (mPlayList != null && mPlayList!![position].id != null) {
                                    addTrackToPlayList(mTrackListCommon.id!!, mPlayList!![position].id!!)
                                }
                                mBottomSheetDialog.dismiss()
                            }

                            override fun onLongItemClick(view: View?, position: Int) {

                            }


                        })
        )

        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(
                    mTrackListCommon.id!!,
                    this
            )
            mBottomSheetDialog.dismiss()
        }
    }

    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {

        val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                        mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        artistViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val switchButtonPlayList: SwitchButton =
                dialogView.findViewById(R.id.switch_button_play_list)
        val createPlayListTextView: TextView =
                dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
                dialogView.findViewById(R.id.play_list_name_edit_text)

        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                            && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                                "0", playListNameEditText.text.toString(), isPrivate,
                                mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }

                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }

    }

    private fun createPlayListApi(
            playListId: String,
            playListName: String,
            private: String,
            mLastId: String
    ) {
        if (InternetUtil.isInternetOn()) {

            val createPlayListBody =
                    CreatePlayListBody(playListId, playListName, mLastId, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            artistViewModel.createPlayListApi(mAuth, createPlayListBody)


        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            if (resultCode == 1001) {
                if (data != null) {
                    if (data.getStringExtra("mFileUrl") != null) {
                        val mFileUrl: String = data.getStringExtra("mFileUrl") ?: ""
                        if (data.getStringExtra("mLastId") != null) {
                            mLastId = data.getStringExtra("mLastId") ?: ""
                        }

                        if (playListImageView != null) {
                            glideRequestManager
                                    .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                                    .into(playListImageView!!)
                        }
                    }

                }
            }
        }
    }

    private fun checkTrackPlayStatus() {

        // TODO : If track is playing change status ( isPlaying )
        if (MyApplication.playlistManager != null &&
                MyApplication.playlistManager?.getPlayList() != null &&
                MyApplication.playlistManager?.getPlayList()?.size!! > 0 &&
                MyApplication.playlistManager?.currentPosition != null &&
                MyApplication.playlistManager?.currentPosition != -1 &&
                MyApplication.playlistManager?.playlistHandler != null &&
                mGlobalGenreList != null && mGlobalGenreList?.isNotEmpty()!!
        ) {

            if (MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                // TODO : Check if playing older position
                if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 && mPlayPostion != -1) {
                    mGlobalGenreList?.get(mPlayPostion)?.isPlaying = false
                    mTrackAdapter?.notifyItemChanged(mPlayPostion)

                }

                // TODO : Checking if the current list track is playing
                mGlobalGenreList?.map {
                    if (it.numberId == MyApplication.playlistManager?.getPlayList()?.get(
                                    MyApplication.playlistManager?.currentPosition!!
                            )?.id.toString()
                    ) {
                        it.isPlaying = true
                        mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                    }
                }

                //TODO : Notify play animation
                if (mPlayPostion != -1) {
                    mTrackAdapter?.notifyItemChanged(mPlayPostion)
                }
            } else {
                mGlobalGenreList?.map {
                    if (it.numberId == MyApplication.playlistManager?.getPlayList()?.get(
                                    MyApplication.playlistManager?.currentPosition!!
                            )?.id.toString()
                    ) {
                        it.isPlaying = false
//                        mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                    }
                }
                if (mPlayPostion != -1) {
                    mTrackAdapter?.notifyItemChanged(mPlayPostion)
                }
            }
        }

    }

    override fun onPlaylistItemChanged(
            currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        checkTrackPlayStatus()
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.PLAYING -> {
                val itemChange = playlistManager?.currentItemChange
                if (itemChange != null) {
                    onPlaylistItemChanged(
                            itemChange.currentItem,
                            itemChange.hasNext,
                            itemChange.hasPrevious
                    )
                }
                artistPlayListActivityBinding?.fab?.visibility = View.GONE
                artistPlayListActivityBinding?.gifProgress?.visibility = View.VISIBLE
                artistPlayListActivityBinding?.gifProgress?.playAnimation()
                artistPlayListActivityBinding?.gifProgress?.bringToFront()
            }
            PlaybackState.ERROR -> {
            }
            PlaybackState.PAUSED -> {
                artistPlayListActivityBinding?.gifProgress?.visibility = View.GONE
                artistPlayListActivityBinding?.fab?.visibility = View.VISIBLE
                artistPlayListActivityBinding?.gifProgress?.pauseAnimation()
                updatePause()
            }
            PlaybackState.RETRIEVING -> {

            }
            PlaybackState.PREPARING -> {

            }
            PlaybackState.SEEKING -> {

            }
            PlaybackState.STOPPED -> {

            }
        }
        return true
    }

    private fun updatePause() {

        if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 &&
                playlistManager != null &&
                playlistManager?.currentPosition != null &&
                playlistManager?.currentPosition != -1
        ) {
            mGlobalGenreList?.map {
                if (it.numberId == MyApplication.playlistManager?.getPlayList()?.get(
                                MyApplication.playlistManager?.currentPosition!!
                        )?.id.toString()
                ) {
                    it.isPlaying = false
                    mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                }
            }
            if (mPlayPostion != -1) {
                mTrackAdapter?.notifyItemChanged(mPlayPostion)
            }
        }
    }

    private var playlistManager: PlaylistManager? = null
    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)

        updatePlayPause()
        updateCurrentPlaybackInformation()
    }

    private fun updateCurrentPlaybackInformation() {

        val currentPlaybackState = playlistManager?.currentPlaybackState
        onPlaybackStateChanged(currentPlaybackState!!)
    }

    private fun updatePlayPause() {
        if (playlistManager != null
                && playlistManager?.playlistHandler != null
                && playlistManager?.playlistHandler?.currentMediaPlayer != null
                && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
                && playlistManager?.currentItem != null
                && playlistManager?.currentItem?.id != null
        ) {
            if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                val mTrackId =
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id?.toString()

                mGlobalGenreList?.map {
                    if (it.numberId == mTrackId) {
                        mPlayPostion = mGlobalGenreList?.indexOf(it)!!
                    }
                }
                updatePLaying(mTrackId)
            } else {
                updatePause()
            }
        }

    }

    private fun updatePLaying(mTrackId: String?) {
        mGlobalGenreList?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                println("" + true)
            } else {
                it.isPlaying = false
                println("" + false)
            }
        }

        mTrackAdapter?.notifyDataSetChanged()
    }

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private fun startDownload(mTrackDetails: TrackListCommon) {

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val paramsNotification = Bundle()
        paramsNotification.putString(
                "user_id",
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        )
        paramsNotification.putString("track_id", mTrackDetails.id)
        paramsNotification.putString("event_type", "track_download")
        firebaseAnalytics.logEvent("offline_track_download", paramsNotification)
        searchViewModel.addToDownload(mTrackDetails)
        Toast.makeText(this, "Track added to queue", Toast.LENGTH_SHORT).show()


        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            enqueueDownloads()
        }, 1000)
    }


    private val GROUP_ID = "listGroup".hashCode()
    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size!! > 0) {

            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests != null && requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }
    }

}
