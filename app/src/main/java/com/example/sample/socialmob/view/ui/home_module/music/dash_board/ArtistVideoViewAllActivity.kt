package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ActivityArtistVideoDetailsBinding
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.ArtistVideoDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.music.MusicVideoListAdapter
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.music_menu.ArtistViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArtistVideoViewAllActivity : BaseCommonActivity() {

    private var binding: ActivityArtistVideoDetailsBinding? = null
    private var mVideoAdapter: MusicVideoListAdapter? = null
    private var mArtistId = ""
    private val viewModelArtist: ArtistViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_artist_video_details)

        binding?.playerView?.visibility = View.GONE
        binding?.videoDetailsCardView?.visibility = View.GONE
        binding?.videoYouMayLikeTextView?.visibility = View.GONE
        binding?.cardLayoutToolbar?.visibility = View.VISIBLE

        intent?.let {
            val extras: Bundle = intent.extras!!
            if (extras.containsKey("mArtistId")) {
                mArtistId = extras.getString("mArtistId", "")
            }
        }

        binding?.videosForYouRecyclerView?.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )
        mVideoAdapter = MusicVideoListAdapter(this, ArrayList())
        binding?.videosForYouRecyclerView?.adapter = mVideoAdapter

        if (mArtistId != "") {
            if (!InternetUtil.isInternetOn()) {
                val shake: Animation =
                    AnimationUtils.loadAnimation(this, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            } else {
                getVideoList(mArtistId)
            }
        }
    }

    private fun getVideoList(mArtistId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + "/api/v1/trackvideo/artist/list?artist=" + mArtistId,
            RemoteConstant.mGetMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModelArtist.getVideoList(mAuth, mArtistId)
        observeVideoList()
    }

    private fun observeVideoList() {
        viewModelArtist.videoListModel.nonNull().observe(this, { videoListModel ->
            if (videoListModel != null && videoListModel.size > 0) {
                mVideoAdapter?.swapMusicVideo(videoListModel)
            }
        })
    }

    fun goToDetailsPage(id: String?, mUrl: String?) {
        viewModelArtist.mSimilarVideosPayload.postValue(null)
        val intent = Intent(this, ArtistVideoDetailsActivity::class.java)
        intent.putExtra("mArtistId", id)
        intent.putExtra("mUrl", mUrl)
        intent.putExtra("isFromViewAll", "false")
        startActivityForResult(intent, 10)
        finish()
    }
}
