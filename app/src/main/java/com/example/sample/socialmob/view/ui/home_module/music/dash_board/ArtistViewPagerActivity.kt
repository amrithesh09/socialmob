package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ArtistViewpagerActivtyBinding
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.HomeViewPagerAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.ArtistViewModel
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

@AndroidEntryPoint
class ArtistViewPagerActivity : BaseCommonActivity(), PlaylistListener<MediaItem> {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var artistViewpagerActivityBinding: ArtistViewpagerActivtyBinding? = null
    private val artistViewModel: ArtistViewModel by viewModels()
    private var playListImageView: ImageView? = null
    private var mLastId: String = ""
    private var mIsLoadingFragment: Boolean = false
    private var handler: Handler? = null
    private var handlerViewVisible: Handler? = null

    private val GROUP_ID = "listGroup".hashCode()
    // Track Download
    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"
    private val searchViewModel: SearchViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        // TODO : Dark mode
        super.onCreate(savedInstanceState)
        artistViewpagerActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.artist_viewpager_activty)
        // TODO : Track download

        val fetchConfiguration = FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(4)
                .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE)
//            .setNotificationManager(object : DefaultFetchNotificationManager(requireContext()) {
//                override fun getFetchInstanceForNamespace(namespace: String): Fetch {
//                    return fetch!!
//                }
//            })
                .build()
        fetch = Fetch.getInstance(fetchConfiguration)


        // TODO : Download offlie track pending
        if (searchViewModel.downloadFalseTrackListModel != null) {
            searchViewModel.downloadFalseTrackListModel?.nonNull()
                    ?.observe(this, { trackListCommonDbs ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            this.trackListCommonDbs = trackListCommonDbs

                        }
                    })
        }


        // TODO : get list
        mIsLoadingFragment = true
        if (intent?.hasExtra("mArtistList")!!) {
            val mList = intent.getParcelableArrayListExtra<Parcelable>("mArtistList")
            val mArtistPosition = intent.getStringExtra("mArtistPosition")
            if (mList?.isNotEmpty()!!) {

                try {
                    artistViewpagerActivityBinding?.gifProgress?.visibility = View.GONE
                    artistViewpagerActivityBinding?.fab?.visibility = View.GONE
                    artistViewpagerActivityBinding?.progressBar?.visibility = View.VISIBLE
                    artistViewpagerActivityBinding?.progressBar?.bringToFront()
                    artistViewpagerActivityBinding?.artistViewpager?.visibility = View.INVISIBLE
                    val mArtistList: List<Artist>? =
                        intent.getParcelableArrayListExtra<Parcelable>("mArtistList") as List<Artist>

                    val fragmentAdapter = HomeViewPagerAdapter(supportFragmentManager)

                    for (i in 0 until mArtistList?.size!!) {
                        fragmentAdapter.addFragments(
                            ArtistFragment().newInstance(mArtistList[i].id!!)
                        )
                    }
                    artistViewpagerActivityBinding?.artistViewpager?.adapter = fragmentAdapter
                    if (mArtistPosition != null && mArtistPosition != "") {
                        handler = Handler(Looper.getMainLooper())
                        handler?.postDelayed({
                            artistViewpagerActivityBinding?.artistViewpager?.currentItem =
                                mArtistPosition.toInt()

                        }, 1000)
                        handlerViewVisible = Handler(Looper.getMainLooper())
                        handlerViewVisible?.postDelayed({
                            artistViewpagerActivityBinding?.progressBar?.visibility = View.GONE
                            artistViewpagerActivityBinding?.artistViewpager?.visibility =
                                View.VISIBLE

                            artistViewpagerActivityBinding?.gifProgress?.visibility = View.VISIBLE
                            artistViewpagerActivityBinding?.fab?.visibility = View.VISIBLE
                            mIsLoadingFragment = false
                            onResume()
                        }, 3000)

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        artistViewpagerActivityBinding?.artistBackImageView?.setOnClickListener {
            finish()
        }
        artistViewpagerActivityBinding?.gifProgress?.setOnClickListener {
            artistViewpagerActivityBinding?.fab?.performClick()
        }
        artistViewpagerActivityBinding?.fab?.setOnClickListener {

            if (MyApplication.playlistManager != null &&
                MyApplication.playlistManager?.playlistHandler != null &&
                MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                var mImageUrlPass = ""
                if (
                    MyApplication.playlistManager?.playlistHandler?.currentItemChange != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                        MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                }
                val detailIntent =
                    Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                startActivity(detailIntent)
            } else if (MyApplication.playlistManager != null &&
                MyApplication.playlistManager?.playlistHandler != null &&
                MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null
            ) {
                var mImageUrlPass = ""
                if (
                    MyApplication.playlistManager?.playlistHandler?.currentItemChange != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                    MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                        MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                }
                val detailIntent =
                    Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                startActivity(detailIntent)
            }

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, R.anim.play_panel_close_background)
        AppUtils.hideKeyboard(
            this@ArtistViewPagerActivity, artistViewpagerActivityBinding?.artistViewpager!!
        )
    }

    fun gotoArtistInfo(
        mArtistId: String, mArtistProfileId: String, mArtist: Artist
    ) {
        ArtistDetailsActivity.mArtist = mArtist
        val intent = Intent(this, ArtistDetailsActivity::class.java)
        intent.putExtra("mArtistId", mArtistId)
        intent.putExtra("mArtistPath", "ArtistView")
        intent.putExtra("mArtistProfileId", mArtistProfileId)
        intent.putExtra("mArtist", mArtist)
        startActivity(intent)
    }

    fun musicOptionsDialog(mTrackDetails: TrackListCommon) {
        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView: View = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
            sheetView.findViewById(R.id.add_play_list_image_view)
        val equalizerImageView: ImageView = sheetView.findViewById(R.id.equalizer_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
            sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE
        val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)


        val allowOfflineDownload: String? = mTrackDetails?.allowOfflineDownload
        if (allowOfflineDownload == "1") {
            downloadLinear.visibility = View.VISIBLE
        } else {
            downloadLinear.visibility = View.GONE
        }
        downloadLinear.setOnClickListener {
            startDownload(mTrackDetails)
            mBottomSheetDialog.dismiss()
        }
        val mTrackId: String? = mTrackDetails.id
        var isLiked: String? = mTrackDetails.favorite
        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }


        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                artistViewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "false"
                    }

                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                artistViewModel.removeFavourites(mAuth, mTrackId)

            }

        }

        val mAuth =
            RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        artistViewModel.getPlayListApi(mAuth)

        artistViewModel.playListModel.nonNull().observe(this, {
            if (it.isNotEmpty()) {
                mPlayList = it as MutableList
                playListRecyclerView.adapter = CommonPlayListAdapter(
                    mPlayList, this,
                    false, glideRequestManager
                )

                // TODO : Update playlist ( Home page )
                EventBus.getDefault().postSticky(UpdatePlaylist(mPlayList!!))
            }

        })
        albumLinear.setOnClickListener {
            val intent = Intent(this, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackDetails.album?.id)
            intent.putExtra("title", mTrackDetails.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivity(intent)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackDetails.artist?.id)
            detailIntent.putExtra("mArtistName", mTrackDetails.artist?.artistName)
            startActivity(detailIntent)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                context = this,
                recyclerView = playListRecyclerView,
                mListener = object : RecyclerItemClickListener.ClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //TODO : Call Add to playlist API
                        if (mPlayList != null && mPlayList!![position].id != null) {
                            addTrackToPlayList(mTrackDetails.id!!, mPlayList!![position].id!!)
                        }
                        mBottomSheetDialog.dismiss()
                    }

                    override fun onLongItemClick(view: View?, position: Int) {

                    }


                })
        )

        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(
                mTrackDetails.id!!,
                this
            )
            mBottomSheetDialog.dismiss()
        }
    }

    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                    mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        artistViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)

        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                        && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                            "0", playListNameEditText.text.toString(), isPrivate,
                            mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }
                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }
    }

    private fun createPlayListApi(
        playListId: String, playListName: String, private: String, mLastId: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody =
                CreatePlayListBody(playListId, playListName, mLastId, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            artistViewModel.createPlayListApi(mAuth, createPlayListBody)
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.PLAYING -> {
                if (!mIsLoadingFragment) {
                    artistViewpagerActivityBinding?.fab?.visibility = View.GONE
                    artistViewpagerActivityBinding?.gifProgress?.visibility = View.VISIBLE
                    artistViewpagerActivityBinding?.gifProgress?.playAnimation()
                    artistViewpagerActivityBinding?.gifProgress?.bringToFront()
                }
            }
            PlaybackState.ERROR -> {
            }
            PlaybackState.PAUSED -> {
                if (!mIsLoadingFragment) {
                    artistViewpagerActivityBinding?.gifProgress?.visibility = View.GONE
                    artistViewpagerActivityBinding?.fab?.visibility = View.VISIBLE
                    artistViewpagerActivityBinding?.gifProgress?.pauseAnimation()
                }
            }
            PlaybackState.RETRIEVING -> {

            }
            PlaybackState.PREPARING -> {

            }
            PlaybackState.SEEKING -> {

            }
            PlaybackState.STOPPED -> {

            }
        }
        return true
    }

    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size!! > 0) {

            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests != null && requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }
    }
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private fun startDownload(mTrackDetails: TrackListCommon) {

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val paramsNotification = Bundle()
        paramsNotification.putString(
                "user_id",
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        )
        paramsNotification.putString("track_id", mTrackDetails.id)
        paramsNotification.putString("event_type", "track_download")
        firebaseAnalytics.logEvent("offline_track_download", paramsNotification)
        searchViewModel.addToDownload(mTrackDetails)
        Toast.makeText(this, "Track added to queue", Toast.LENGTH_SHORT).show()


        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            enqueueDownloads()
        }, 1000)
    }
    override fun onResume() {
        super.onResume()
        if (MyApplication.playlistManager != null &&
            MyApplication.playlistManager?.currentPlaybackState != null
        ) {
            val itemChange = MyApplication.playlistManager?.currentPlaybackState
            if (itemChange != null) {
                onPlaybackStateChanged(itemChange)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler?.removeCallbacksAndMessages(null)
        handlerViewVisible?.removeCallbacksAndMessages(null)
    }


}