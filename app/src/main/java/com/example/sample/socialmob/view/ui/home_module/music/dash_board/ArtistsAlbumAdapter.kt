package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AlbumListItemBinding
import com.example.sample.socialmob.model.music.ArtistDetailsPayloadAlbum
import com.example.sample.socialmob.repository.utils.RemoteConstant.getImageUrlFromWidth

class ArtistsAlbumAdapter(
    private val mContext: Context?,
    private var albums: List<ArtistDetailsPayloadAlbum>?,
    private var mArtistClickListener: ArtistAlbumClickListener,
    private var glideRequestManager: RequestManager

) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ArtistsAlbumAdapter.ViewHolder>() {
    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding: AlbumListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.album_list_item, parent, false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.albumViewModel = albums?.get(holder.adapterPosition)
        holder.binding.executePendingBindings()

       val mArtistAlbumImage = if (albums?.get(holder.adapterPosition)?.isSingles == "false") {
            albums?.get(holder.adapterPosition)?.albumMedia?.coverImage!!
        } else {
            albums?.get(holder.adapterPosition)?.singlesMedia?.coverImage!!
        }
        glideRequestManager.load(getImageUrlFromWidth(mArtistAlbumImage, 500)).thumbnail(0.1f)
            .apply(
                RequestOptions.placeholderOf(R.drawable.default_list_icon).error(R.drawable.default_list_icon)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
            )
            .into(holder.binding.playListImageView)

        holder.itemView.setOnClickListener {
            var coverImage = ""
            if (albums?.get(holder.adapterPosition)?.singlesMedia?.coverImage != null) {
                coverImage = albums?.get(holder.adapterPosition)?.singlesMedia?.coverImage!!
            }

            mArtistClickListener.onArtistAlbumClickListener(
                albums?.get(holder.adapterPosition)?.id!!,
                albums?.get(holder.adapterPosition)?.albumName!!,
                coverImage,
                albums?.get(holder.adapterPosition)?.artist?.artistName!!
            )
        }
    }

    override fun getItemCount(): Int {
        return albums?.size!!
    }

    class ViewHolder(val binding: AlbumListItemBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root)


    interface ArtistAlbumClickListener {
        fun onArtistAlbumClickListener(
            mArtistId: String, mTitle: String,
            imageUrl: String,
            mDescription: String
        )
    }
}

