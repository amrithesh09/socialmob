package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.music.genre.GenreFragment
import com.example.sample.socialmob.view.ui.home_module.music.radio.RadioMainFragment
import com.example.sample.socialmob.view.ui.home_module.notification.NotificationInnerFragment
import com.example.sample.socialmob.view.ui.home_module.search.PlayListFragment
import com.example.sample.socialmob.view.ui.home_module.search.SearchArtistFragment
import com.example.sample.socialmob.view.ui.home_module.search.SearchFragment
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.common_activity.*

@AndroidEntryPoint
class CommonFragmentActivity : BaseCommonActivity() {

    private var isFrom: String = ""
    private var isLongPress: String = ""
    private var mTitle: String = ""
    private var mRecordId: String = ""
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    companion object {
        var isUploading: Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_activity)

        intent?.let {
            val extras: Bundle = intent.extras!!
            if (extras.containsKey("isFrom")) {
                isFrom = extras.getString("isFrom", "")
            }
            if (extras.containsKey("isLongPress")) {
                isLongPress = extras.getString("isLongPress", "")
            }
            if (extras.containsKey("mTitle")) {
                mTitle = extras.getString("mTitle", "")
            }
            if (extras.containsKey("mRecordId")) {
                mRecordId = extras.getString("mRecordId", "")
            }
        }

        isUploading = false
        when (isFrom) {
          "RadioStation" -> {
              val fragmentManager: FragmentManager = supportFragmentManager
              val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
              fragmentTransaction.replace(R.id.genre_frame_layout, RadioMainFragment())
              fragmentTransaction.commit()
//              genre_title_text_view.text = getString(R.string.discover)
              cardLayout.visibility = View.GONE
          }
          "Discover" -> {
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.genre_frame_layout, DiscoverFragment())
                fragmentTransaction.commit()
                genre_title_text_view.text = getString(R.string.discover)
                cardLayout.visibility = View.GONE
            }
            "Genre" -> {
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.genre_frame_layout, GenreFragment())
                fragmentTransaction.commit()
                genre_title_text_view.text = getString(R.string.genre)
            }
            "Notification" -> {
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    NotificationInnerFragment().newInstance(getString(R.string.settings_))
                )
                fragmentTransaction.commit()
                genre_title_text_view.text = getString(R.string.notification)
            }
            "FeedSearch" -> {
                cardLayout.visibility = View.GONE
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    SearchFragment().newInstance("FeedSearch", isLongPress)
                )
                fragmentTransaction.commit()
            }
            "ChatSearch" -> {
                cardLayout.visibility = View.GONE
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    SearchFragment().newInstance("ChatSearch", isLongPress)
                )
                fragmentTransaction.commit()
            }
            "CreateNewChat" -> {
                cardLayout.visibility = View.GONE
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    SearchFragment().newInstance("CreateNewChat", isLongPress)
                )
                fragmentTransaction.commit()
            }
            "MusicSearch" -> {
                cardLayout.visibility = View.GONE
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    SearchFragment().newInstance("MusicSearch", isLongPress)
                )
                fragmentTransaction.commit()
            }
            "AllArtist" -> {
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    SearchArtistFragment.newInstance("AllArtist")
                )
                fragmentTransaction.commit()
                genre_title_text_view.text = getString(R.string.all_artist)
            }
            "PlayListForYouViewAll" -> {
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    PlayListFragment.newInstance("PlayListForYouViewAll")
                )
                fragmentTransaction.commit()
                genre_title_text_view.text = getString(R.string.all_playlists)
            }
            "Campaign" -> {
                val fragmentManager: FragmentManager = supportFragmentManager
                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.genre_frame_layout,
                    ProfileMediaGridFragment.newInstance(mRecordId, "Campaign")
                )
                fragmentTransaction.commit()
                genre_title_text_view.text = ""

                firebaseAnalytics = FirebaseAnalytics.getInstance(this)


                val paramsNotification = Bundle()
                paramsNotification.putString(
                    "user_id",
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
                )
                paramsNotification.putString("event_type", "campaign-Mathrubhumi Sing N Win")
                firebaseAnalytics.logEvent("campaign", paramsNotification)


            }
        }

        genre_back_image_view.setOnClickListener {
            AppUtils.hideKeyboard(this@CommonFragmentActivity, genre_back_image_view)
            onBackPressed()
        }

    }

    fun setTitleToolbar(name: String) {

        genre_title_text_view.text = name
    }


    var mAlertDialog: AlertDialog? = null
    override fun onBackPressed() {
        if (!isUploading) {
            super.onBackPressed()
            AppUtils.hideKeyboard(this@CommonFragmentActivity, genre_back_image_view)
        } else {
            val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
            val inflater: LayoutInflater = layoutInflater
            val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
            val okButton: Button = dialogView.findViewById(R.id.ok_button)
            val dialogDescription: TextView = dialogView.findViewById(R.id.dialog_description)
            dialogDescription.text = getString(R.string.want_to_see_post)

            dialogDescription.text = "Upload on progress do you want to cancel"

            cancelButton.text = getString(R.string.no)
            okButton.text = getString(R.string.yes)

            mAlertDialog = dialogBuilder.create()
            mAlertDialog?.show()
            okButton.setOnClickListener {
                mAlertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                super.onBackPressed()
                AppUtils.hideKeyboard(this@CommonFragmentActivity, genre_back_image_view)
                isUploading = false
            }
            cancelButton.setOnClickListener {
                mAlertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                isUploading = false
            }
        }

    }
}
