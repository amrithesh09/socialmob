package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.DiscoverFragmentBinding
import com.example.sample.socialmob.view.ui.home_module.search.SearchPageAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DiscoverFragment : Fragment() {
    private var discoverFragmentBinding: DiscoverFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        discoverFragmentBinding?.discoverBackImageView?.setOnClickListener {
            if (activity != null) {
                activity?.finish()
            }
        }
        setAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        discoverFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.discover_fragment, container, false)

        return discoverFragmentBinding?.root
    }

    private fun setAdapter() {

        val fragmentAdapter = SearchPageAdapter(requireFragmentManager(), "Discover")
        discoverFragmentBinding?.searchViewpager?.adapter = null
        discoverFragmentBinding?.searchViewpager?.adapter = fragmentAdapter
        discoverFragmentBinding?.tabsMain?.setupWithViewPager(discoverFragmentBinding?.searchViewpager)

    }

}

