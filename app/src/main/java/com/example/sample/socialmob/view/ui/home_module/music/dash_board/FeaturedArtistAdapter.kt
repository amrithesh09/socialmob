package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.*
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder

class FeaturedArtistAdapter (
    private val mArtistClickListener: ArtistsAdapter.ArtistClickListener?,
    private var isFrom: String,
    private var mContext: FragmentActivity,
    private var requestManager: RequestManager
) :
    ListAdapter<Artist, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_VIEW_ALL = 3

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<Artist>() {
                override fun areItemsTheSame(
                    oldItem: Artist,
                    newItem: Artist
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Artist,
                    newItem: Artist
                ): Boolean {
                    return oldItem.id?.equals(newItem.id)!!
                }
            }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)?.id) {
            "0" -> ITEM_LOADING
            "" -> ITEM_VIEW_ALL
            else -> ITEM_DATA
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        if (isFrom == "searchArtist") {
            return when (viewType) {
                ITEM_DATA -> bindData(parent)
                ITEM_LOADING -> bindLoader(parent)
                ITEM_VIEW_ALL -> bindViewAll(parent)
                else -> bindData(parent)
            }

        } else {

            return when (viewType) {
                ITEM_DATA -> bindDataArtist(parent)
                ITEM_VIEW_ALL -> bindViewAll(parent)
                else -> bindData(parent)
            }
        }

    }

    private fun bindDataArtist(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: FeaturedArtistItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.featured_artist_item, parent, false
        )
        return ArtistViewHolder(binding)
    }

    private fun bindViewAll(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MemmbersItemViewAllBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.memmbers_item_view_all, parent, false
        )
        return ViewAllHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ArtistItemSearchBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.artist_item_search, parent, false
        )
        return ArtistViewHolderSearch(binding)
    }


    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }


    inner class ArtistViewHolder(val binding: FeaturedArtistItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(mArtist: Any?) {
            if (mArtist != null) {
                binding?.similarArtistViewModel = mArtist as Artist
                binding?.executePendingBindings()

                requestManager
                    .load(
                        RemoteConstant.getImageUrlFromWidth(
                            getItem(adapterPosition).media?.coverImage,
                            RemoteConstant.getWidth(mContext) / 2
                        )
                    )
                    .apply(
                        RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                            .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                                Priority.HIGH
                            )
                    )
                    .thumbnail(0.1f)
                    .into(binding?.artistImageView!!)

                binding.artistImageView.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        mArtistClickListener?.onArtistClickListener(
                            mArtist.id!!,
                            adapterPosition.toString(),"featuredArtist"
                        )
                    }
                }
            }
        }
    }

    inner class ArtistViewHolderSearch(val binding: ArtistItemSearchBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(mArtist: Any?) {
            binding?.similarArtistViewModel = mArtist as Artist
            binding?.executePendingBindings()

            binding?.artistImageView?.setOnClickListener {
                mArtistClickListener?.onArtistClickListener(
                    getItem(adapterPosition).id!!,
                    adapterPosition.toString(),"featuredArtist"
                )
            }
        }
    }

    inner class ViewAllHolder(val binding: MemmbersItemViewAllBinding?) :
        BaseViewHolder<Any>(binding?.root) {


        override fun bind(genre: Any) {

            itemView.setOnClickListener {
                val intent = Intent(mContext, CommonFragmentActivity::class.java)
                intent.putExtra("isFrom", "AllArtist")
                mContext.startActivity(intent)

            }
        }

    }

}