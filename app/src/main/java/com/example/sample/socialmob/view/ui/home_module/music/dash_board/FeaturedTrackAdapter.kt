package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MusicDashboardItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.MyApplication

class FeaturedTrackAdapter(
    private var mContext: Context,
    private var requestManager: RequestManager,
    private val musicDashboardItemClickListener: MusicDashboardItemClickListener
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var playlistManager = MyApplication.playlistManager
    private val mFeaturedTrack = mutableListOf<TrackListCommon>()
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        val binding: MusicDashboardItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.music_dashboard_item, parent, false
        )
        return WhatsNewViewHolder(binding)

    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mFeaturedTrack[holder.adapterPosition])
    }


    inner class WhatsNewViewHolder(val binding: MusicDashboardItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(mTrackItem: Any?) {
            binding?.trackViewModel = mTrackItem as TrackListCommon
            binding?.executePendingBindings()


            requestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        mFeaturedTrack[adapterPosition].media?.coverFile,
                        RemoteConstant.getWidth(mContext) / 2
                    )
                )
                .apply(
                    RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                            Priority.HIGH
                        )
                )
                .thumbnail(0.1f)
                .into(binding?.musicDashboardItemImageView!!)


            binding.`whatsNewLinear`.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    musicDashboardItemClickListener.onMusicDashboardItemClick(
                        mFeaturedTrack[adapterPosition].numberId?.toInt()!!,
                        mFeaturedTrack[adapterPosition].media?.coverFile!!, "MusicDashBoard"
                    )
                }
            }

            // TODO : Playing Gif
            /*when {
                mFeaturedTrack[adapterPosition].isPlaying == true -> {
                    binding.fabPlayDashboardImageView.visibility = View.GONE
                    binding.featuredTrackGifProgress.visibility = View.VISIBLE
                    binding.featuredTrackGifProgress.post {
                        binding.featuredTrackGifProgress.playAnimation()
                    }
                }
                mFeaturedTrack[adapterPosition].numberId == playlistManager?.currentItemChange?.currentItem?.id?.toString() ?: 0 &&
                        mFeaturedTrack[adapterPosition].isPlaying == false -> {

                    binding.fabPlayDashboardImageView.visibility = View.GONE
                    binding.featuredTrackGifProgress.visibility = View.VISIBLE
                    binding.featuredTrackGifProgress.post {
                        binding.featuredTrackGifProgress.pauseAnimation()
                    }
                }
                else -> {
                    binding.fabPlayDashboardImageView.visibility = View.VISIBLE
                    binding.featuredTrackGifProgress.visibility = View.INVISIBLE
                    binding.featuredTrackGifProgress.post {
                        binding.featuredTrackGifProgress.pauseAnimation()
                    }
                }
            }*/
        }
    }

    interface MusicDashboardItemClickListener {
        fun onMusicDashboardItemClick(itemId: Int, itemImage: String, isFrom: String)
    }

    override fun getItemCount(): Int = mFeaturedTrack.size

    fun swap(list: List<TrackListCommon>) {
        val diffCallback = TrackDiffCallbackFeaturedTrack(this.mFeaturedTrack, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback, true)

        this.mFeaturedTrack.clear()
        this.mFeaturedTrack.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    private class TrackDiffCallbackFeaturedTrack(
        private val oldList: List<TrackListCommon>,
        private val newList: List<TrackListCommon>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].isPlaying == newList[newItemPosition].isPlaying

        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
                    || oldList[oldItemPosition].isPlaying == newList[newItemPosition].isPlaying
        }
    }
}