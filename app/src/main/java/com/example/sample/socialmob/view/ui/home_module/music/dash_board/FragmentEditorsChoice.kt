package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.FragmentEditorsBinding
import com.example.sample.socialmob.model.music.EditorsChoicePlaylist
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.events.UpdateViewpagerList
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class FragmentEditorsChoice : Fragment(), TopTrackAdapter.TopTrackItemClickListener {
    private var title: String? = null
    private var mEditorsChoicePlaylist: EditorsChoicePlaylist? = EditorsChoicePlaylist()
    private var binding: FragmentEditorsBinding? = null
    private var mLastClickTime: Long = 0
    private var itemTrackId: Int = 0
    private var mPlayPosition: Int = -1
    private val mediaItems = LinkedList<MediaItem>()
    private val ID = 4 //Arbitrary, for the example
    private var mTrackIdGlobal: String = ""
    private var topAdapter: TopTrackAdapter? = null
    private var playlistManager: PlaylistManager? = null


    @Inject
    lateinit var glideRequestManager: RequestManager
    private var mPlayPositionTopTrack = -1
    private val viewModel: MusicDashBoardViewModel by viewModels()


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (playlistManager?.playlistHandler != null
                && playlistManager?.playlistHandler?.currentMediaPlayer != null
                && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
                && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying == true
            ) {
                updatePLaying(playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id?.toString())
            } else {
                updatePause()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        userVisibleHint = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mEditorsChoicePlaylist != null) {

            binding?.topTrackRecyclerView?.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            topAdapter = TopTrackAdapter(
                topTrackItemClickListener = this,
                mContext = activity,
                isFrom = title, requestManager = glideRequestManager
            )
            binding?.topTrackRecyclerView?.adapter = topAdapter
            if (mEditorsChoicePlaylist?.tracks != null) {
                topAdapter?.swap(mEditorsChoicePlaylist?.tracks ?: ArrayList())
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            title = arguments?.getString("mTitle")!!
            mEditorsChoicePlaylist = arguments?.getSerializable("mDetails") as EditorsChoicePlaylist
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_editors, container, false
        )
        return binding?.root
    }

    companion object {
        // newInstance constructor for creating fragment with arguments
        var requireActivity: FragmentActivity? = null
        fun newInstance(
            title: String?,
            editorsChoicePlaylist: EditorsChoicePlaylist,
            requireActivity: FragmentActivity
        ): FragmentEditorsChoice {
            val fragmentFirst = FragmentEditorsChoice()
            val args = Bundle()
            args.putString("mTitle", title)
            args.putSerializable("mDetails", editorsChoicePlaylist)
            fragmentFirst.arguments = args
            this.requireActivity = requireActivity
            return fragmentFirst
        }
    }

    override fun onTopTrackItemClick(itemId: Int, itemImage: String, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            itemTrackId = itemId
            if (mEditorsChoicePlaylist != null && mEditorsChoicePlaylist?.tracks != null
            ) {
                playTrackCommon(
                    itemId, mEditorsChoicePlaylist?.tracks?.toMutableList() ?: ArrayList(), isFrom
                )
            }
        }
        (requireActivity as HomeActivity).removeStationAnimation()
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    private fun playTrackCommon(itemId: Int, newTrack: List<TrackListCommon>, isFrom: String) {
        if (itemId == playlistManager?.currentItemChange?.currentItem?.id?.toInt() ?: 0) {

            if (playlistManager != null &&
                playlistManager?.playlistHandler != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                var mImageUrlPass = ""
                if (
                    playlistManager?.playlistHandler?.currentItemChange != null &&
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl
                            ?: ""

                }
                val detailIntent =
                    Intent(activity, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                startActivity(detailIntent)

            } else if (!playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                playlistManager?.play(
                    playlistManager?.currentProgress?.position ?: 0,
                    false
                )
                updatePlayPause(itemId.toString())
            } else {
                itemTrackId = itemId
                createLocalPlayList(newTrack.toMutableList(), itemId, isFrom)
            }
        } else {
            itemTrackId = itemId
            createLocalPlayList(newTrack.toMutableList(), itemId, isFrom)
        }
        mTrackIdGlobal = itemId.toString()
    }


    private fun createLocalPlayList(
        playListTrack: MutableList<TrackListCommon>,
        itemId: Int,
        isFrom: String
    ) {

        val mShuffleStatus =
            SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mShuffleStatus, "false"
            )
        viewModel.createDBPlayListCommon(playListTrack, isFrom) { playList ->
            if (mShuffleStatus == "true") {
                var selectedPosition = 0
                playList.map {
                    if (it.JukeBoxTrackId == itemId.toString()) {
                        selectedPosition = playList.indexOf(it)
                    }
                }
                val mSelectedSong: PlayListDataClass = playList[selectedPosition]
                val mGlobalFirstList: MutableList<PlayListDataClass> = ArrayList()
                val mGlobalListNew: MutableList<PlayListDataClass> = ArrayList()
                mGlobalListNew.addAll(playList)

                //TODO : if track is playing adding playing track as first track
                mGlobalListNew.removeAt(selectedPosition)
                mGlobalListNew.shuffle()

                mGlobalFirstList.clear()
                mGlobalFirstList.add(0, mSelectedSong)
                mGlobalFirstList.addAll(mGlobalListNew)
                if (mGlobalFirstList.isNotEmpty()) {
                    mediaItems.clear()
                    for (sample in mGlobalFirstList) {
                        val mediaItem = MediaItem(sample, true)
                        mediaItems.add(mediaItem)
                    }
                }
                mGlobalFirstList.map {
                    if (it.JukeBoxTrackId == itemId.toString()) {
                        mPlayPosition = mGlobalFirstList.indexOf(it)
                    }
                }
            } else {

                if (playList.isNotEmpty()) {
                    mediaItems.clear()
                    for (sample in playList) {
                        val mediaItem = MediaItem(sample, true)
                        mediaItems.add(mediaItem)
                    }
                }

                playList.map {
                    if (it.JukeBoxTrackId == itemId.toString()) {
                        mPlayPosition = playList.indexOf(it)
                    }
                }
            }
            if (mTrackIdGlobal == "") {
                mTrackIdGlobal = mPlayPosition.toString()
            }
            launchMusic()
            updatePlayPause(itemId.toString())
        }
    }

    private fun launchMusic() {
        playlistManager?.setParameters(mediaItems, mPlayPosition)
        playlistManager?.id = ID.toLong()
        playlistManager?.currentPosition = mPlayPosition
        playlistManager?.play(0, false)
    }

    private fun updatePlayPause(mTrackId: String) {
        try {

            if (activity != null) {
                if (playlistManager?.playlistHandler != null
                    && playlistManager?.playlistHandler?.currentMediaPlayer != null
                    && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
                ) {
                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        if (playlistManager != null &&
                            playlistManager?.playlistHandler != null &&
                            playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                            playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
                        ) {
                            if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                                updatePLaying(mTrackId)
                            } else {
                                if (mTrackIdGlobal != "") {
                                    if (mTrackIdGlobal != mTrackId) {
                                        updatePLaying(mTrackId)
                                    } else {
                                        updatePause()
                                    }
                                } else {
                                    updatePause()
                                }
                            }
                        }
                    }, 1000)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            println("" + e.printStackTrace())
            println("" + e.printStackTrace())
        }
    }


    private fun updatePLaying(mTrackId: String?) {
        mTrackIdGlobal = mTrackId!!

        mEditorsChoicePlaylist?.tracks?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                mPlayPositionTopTrack = mEditorsChoicePlaylist?.tracks?.indexOf(it)!!
            } else {
                it.isPlaying = false
            }
        }
        if (topAdapter != null && binding?.topTrackRecyclerView != null) {
            binding?.topTrackRecyclerView?.post {
                topAdapter?.swap(mEditorsChoicePlaylist?.tracks ?: ArrayList())
                topAdapter?.notifyDataSetChanged()
            }
        }
    }

    private fun updatePause() {
        try {
            if (mEditorsChoicePlaylist != null) {
                mEditorsChoicePlaylist?.tracks?.map {
                    if (it.isPlaying == true) {
                        it.isPlaying = false
                        mPlayPositionTopTrack = mEditorsChoicePlaylist?.tracks?.indexOf(it)!!
                    }
                }
            }
            if (topAdapter != null && binding?.topTrackRecyclerView != null) {
                binding?.topTrackRecyclerView?.post {
                    topAdapter?.swap(mEditorsChoicePlaylist?.tracks ?: ArrayList())
                    topAdapter?.notifyDataSetChanged()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UpdateViewpagerList) {
        updatePlayPause(event.mTrackId!!)
    }
}