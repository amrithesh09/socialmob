package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.PlaylistDataIcon
import com.example.sample.socialmob.view.utils.BaseViewHolder


class HomePlayListAdapter internal constructor(
    private val mCallBack: PlayListAdapterItemClickListener,
    private val mCallBackIntent: PlayListAdapterViewAllItemClickListener,
    private val isFrom: String,
    private var editPlayListDialog: EditPlayListDialog,
    private var mPlayListForYouViewAllItemClickListener: PlayListForYouViewAllItemClickListener,
    private var requestManager: RequestManager

) : ListAdapter<PlaylistCommon, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    companion object {
        const val ITEM_VIEW_ALL = 1
        const val ITEM_VIEW_ALL_PLAY_LIST_FOR_YOU = 2
        const val ITEM_PLAYLIST = 3
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PlaylistCommon>() {
                override fun areItemsTheSame(
                    @NonNull oldItem: PlaylistCommon,
                    @NonNull newItem: PlaylistCommon
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    @NonNull oldItem: PlaylistCommon,
                    @NonNull newItem: PlaylistCommon
                ): Boolean {
                    return oldItem.equals(newItem)
                }

            }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        val mInflater: LayoutInflater = LayoutInflater.from(parent.context)


        when (viewType) {
            ITEM_PLAYLIST ->
                return PlayListHolder(mInflater.inflate(R.layout.play_list_item, parent, false))
            ITEM_VIEW_ALL ->
                return ViewAllHolder(
                    mInflater.inflate(R.layout.play_list_item_view_all, parent, false)
                )
            ITEM_VIEW_ALL_PLAY_LIST_FOR_YOU ->
                return ViewAllHolderPlayListForYou(
                    mInflater.inflate(R.layout.play_list_item_view_all, parent, false)
                )
        }
        return PlayListHolder(mInflater.inflate(R.layout.play_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(position))
    }


    override fun getItemViewType(position: Int): Int {
        return when {
            getItem(position).playlistName.equals("") -> ITEM_VIEW_ALL
            getItem(position).playlistName.equals("playListForYouViewAllItem") -> ITEM_VIEW_ALL_PLAY_LIST_FOR_YOU
            else -> ITEM_PLAYLIST
        }

    }


    inner class ViewAllHolder(itemView: View) : BaseViewHolder<Any>(itemView) {

        private val mItemViewAll: ConstraintLayout = itemView.findViewById(R.id.view_all_layout)
        override fun bind(genre: Any) {

            mItemViewAll.setOnClickListener {
                mCallBackIntent.onItemClickViewAllUserPlayList()
            }
        }

    }

    inner class ViewAllHolderPlayListForYou(itemView: View) : BaseViewHolder<Any>(itemView) {

        private val mItemViewAll: ConstraintLayout = itemView.findViewById(R.id.view_all_layout)
        override fun bind(genre: Any) {

            mItemViewAll.setOnClickListener {
                mPlayListForYouViewAllItemClickListener.onItemClickPlayListForYou()
            }
        }

    }


    inner class PlayListHolder(itemView: View?) : BaseViewHolder<Any>(itemView) {
        private val mPlayListName: TextView =
            itemView?.findViewById(R.id.quick_play_list_title_text_view)!!
        private val mPlayListImage: ImageView = itemView?.findViewById(R.id.play_list_image_view)!!
        private val privateImageView: ImageView = itemView?.findViewById(R.id.private_image_view)!!
        private val gradientImageView: ImageView =
            itemView?.findViewById(R.id.gradient_image_view)!!
        private val playListOptionImageView: ImageView =
            itemView?.findViewById(R.id.play_list_option_image_view)!!

        override fun bind(mPlaytListData: Any) {

            val mPlayList: PlaylistCommon? = mPlaytListData as PlaylistCommon


            if (isFrom == "forYouPlayList") {
                mPlayListName.visibility = View.GONE
                gradientImageView.setImageResource(R.drawable.default_play_list_gradient_new)
            }

            val mPlayListNameText = mPlayList?.playlistName
            if (mPlayListNameText != null && mPlayListNameText != "") {
                val mTitle: String = if (mPlayListNameText.length >= 10) {
                    mPlayListNameText.substring(0, 10) + "..."
                } else {
                    mPlayListNameText
                }
                mPlayListName.text = mTitle
            }



            if (isFrom == "myPlayList") {
                if (mPlayList?.playlistType == "system-generated" || mPlayList?.playlistType == "admin-generated") {
                    requestManager
                        .load(mPlayList.media?.coverFile)
                        .apply(
                            RequestOptions.placeholderOf(R.drawable.blur)
                                .error(R.drawable.default_list_icon)
                        )
                        .into(mPlayListImage)
                } else {
                    requestManager
                        .load(mPlayList?.icon?.fileUrl)
                        .apply(
                            RequestOptions.placeholderOf(R.drawable.blur)
                                .error(R.drawable.default_list_icon)
                        )
                        .into(mPlayListImage)
                }
            } else {
                requestManager
                    .load(mPlayList?.media?.coverImage)
                    .apply(
                        RequestOptions.placeholderOf(R.drawable.blur)
                            .error(R.drawable.default_list_icon)
                    )
                    .into(mPlayListImage)
            }



            mPlayListImage.setOnClickListener {


                if (isFrom == "myPlayList") {
                    var mUrl = ""
                    if (mPlayList?.playlistType == "system-generated" || mPlayList?.playlistType == "admin-generated") {
                        if (mPlayList.media?.coverFile != null &&
                            mPlayList.media.coverFile != ""
                        ) {
                            mUrl = mPlayList.media.coverFile!!
                        }
                    } else {
                        if (mPlayList?.icon?.fileUrl != null &&
                            mPlayList.icon.fileUrl != ""
                        ) {
                            mUrl = mPlayList.icon.fileUrl
                        }
                    }

                    mCallBack.onItemClickUserPlayList(
                        mPlayList?.id!!,
                        mPlayList.playlistName!!,
                        mUrl, isFrom
                    )
                } else {
                    val mUrl = mPlayList?.media?.coverImage
                    mCallBack.onItemClickUserPlayList(
                        mPlayList?.id!!,
                        mPlayList.playlistName!!,
                        mUrl!!, isFrom
                    )
                }
            }

            playListOptionImageView.setOnClickListener {
                if (isFrom == "myPlayList") {
                    if (mPlayList?.id != null &&
                        mPlayList.playlistName != null && mPlayList.isPrivate != null &&
                        mPlayList.icon != null
                    ) {
                        editPlayListDialog.optionDialog(
                            mPlayList.id, mPlayList.playlistName,
                            mPlayList.isPrivate, mPlayList.icon, adapterPosition, isFrom
                        )
                    }
                } else {
                    editPlayListDialog.optionDialog(
                        mPlayList?.id,
                        mPlayList?.playlistName,
                        "",
                        PlaylistDataIcon(),
                        adapterPosition, isFrom
                    )
                }
            }

            if (mPlayList?.isPrivate?.equals("1")!!) {
                privateImageView.visibility = View.VISIBLE
            } else {
                privateImageView.visibility = View.GONE
            }
            if (mPlayList.playlistName == "Listen Later") {
                requestManager
                    .load(R.drawable.listen_later_new_one)
                    .into(mPlayListImage)
                playListOptionImageView.visibility = View.GONE
                privateImageView.visibility = View.GONE
            }
        }


    }

    interface PlayListAdapterItemClickListener {
        fun onItemClickUserPlayList(item: String, mTitle: String, mImageUrl: String, isFrom: String)
    }

    interface PlayListAdapterViewAllItemClickListener {
        fun onItemClickViewAllUserPlayList()
    }

    interface PlayListForYouViewAllItemClickListener {
        fun onItemClickPlayListForYou()
    }

    interface EditPlayListDialog {
        fun optionDialog(
            playlistId: String?, playlistName: String?, private: String,
            icon: PlaylistDataIcon, adapterPosition: Int,
            isFrom: String
        )
    }
}