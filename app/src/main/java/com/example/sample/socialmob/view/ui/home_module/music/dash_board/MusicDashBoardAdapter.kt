package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.RemoteConstant

class MusicDashBoardAdapter(
    private var mContext: Context?,
    private var featuredTrackList: List<TrackListCommon>,
    private val musicDashboardItemClickListener: MusicDashboardItemClickListener,
    private var requestManager: RequestManager
) :
    PagerAdapter() {

    private var mLayoutInflater: LayoutInflater =
        mContext?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return featuredTrackList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val itemView: View =
            mLayoutInflater.inflate(R.layout.music_dashboard_item, container, false)

        //val gifProgress: LottieAnimationView = itemView.findViewById(R.id.featured_track_gif_progress)
        val imageView: ImageView = itemView.findViewById(R.id.music_dashboard_item_image_view)
        val trackTitleTextView: TextView = itemView.findViewById(R.id.track_title_text_view)
        val trackNameTextView: TextView = itemView.findViewById(R.id.track_name_text_view)
        //val trackCategoryTextView: TextView = itemView.findViewById(R.id.track_category_text_view)
       // val playDashboardImageView: ImageView = itemView.findViewById(R.id.play_dashboard_image_view)

        requestManager
            .load(
                RemoteConstant.getImageUrlFromWidth(
                    featuredTrackList[position].media?.coverFile,
                    RemoteConstant.getWidth(mContext) / 2
                )
            )
            .apply(
                RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                        Priority.HIGH
                    )
            )
            .thumbnail(0.1f)
            .into(imageView)


        trackTitleTextView.text = featuredTrackList[position].trackName
        trackNameTextView.text = featuredTrackList[position].artist?.artistName
        //trackCategoryTextView.text = featuredTrackList[position].genre?.genreName


       /* playDashboardImageView.setOnClickListener {
            musicDashboardItemClickListener.onMusicDashboardItemClick(
                featuredTrackList[position].numberId?.toInt()!!,
                featuredTrackList[position].media?.coverFile!!, "MusicDashBoard"
            )
        }*/


        // TODO : Playing Gif
        /*if (featuredTrackList[position].isPlaying!!) {
            gifProgress.visibility = View.VISIBLE
            gifProgress.playAnimation()
        } else {
            gifProgress.visibility = View.INVISIBLE
            gifProgress.pauseAnimation()

        }*/


        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    fun update(featuredTrackModelNew: List<TrackListCommon>) {
        val refresh = Handler(Looper.getMainLooper())
        refresh.post {
            featuredTrackList = featuredTrackModelNew
            notifyDataSetChanged()
        }


    }

    interface MusicDashboardItemClickListener {
        fun onMusicDashboardItemClick(itemId: Int, itemImage: String, isFrom: String)
    }

    interface MusicDashboardPlayListClickListener {
        fun onMusicDashboardPlayListClick(playListId: String, playListName: String, isFrom: String)
    }
}