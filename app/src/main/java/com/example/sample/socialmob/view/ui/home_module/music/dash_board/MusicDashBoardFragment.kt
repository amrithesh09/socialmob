package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.*
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.DashboardFragment1Binding
import com.example.sample.socialmob.databinding.DashboardFragmentBinding
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.BottomSheetPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.HomeViewPagerAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.RemoveStationAnimationEvent
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.events.UpdateTrackPlayingEvent
import com.example.sample.socialmob.view.utils.events.UpdateViewpagerList
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.suke.widget.SwitchButton
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class MusicDashBoardFragment : Fragment(), TopTrackAdapter.TopTrackItemClickListener,
        TrendingTrackAdapter.TrendingItemClickListener,
        FeaturedTrackAdapter.MusicDashboardItemClickListener,
        HomePlayListAdapter.PlayListAdapterItemClickListener,
        HomePlayListAdapter.PlayListAdapterViewAllItemClickListener,
        MusicDashBoardAdapter.MusicDashboardPlayListClickListener,
        HomePlayListAdapter.EditPlayListDialog,
        BottomSheetPlayListAdapter.PlayListItemClickListener,
        ArtistsAdapter.ArtistClickListener, HomePlayListAdapter.PlayListForYouViewAllItemClickListener,
        PlaylistListener<MediaItem>, ArtistsAlbumAdapter.ArtistAlbumClickListener,
        StationAdapter.MusicStationClickListener {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var isMusicStationCallCompleted = false
    private var isPlayedTrack = false
    private var isUpdatedPlayLIst: Boolean = false
    private var mPlayListId: String = ""
    private var mLastId: String = ""
    private var itemTrackId: Int = 0
    private var mPlayPosition: Int = -1
    private var mPlayPositionTopTrack: Int = -1
    private var mPlayPositionweeklyNewTracks: Int = -1
    private var mPlayPositionTrendingTrack: Int = -1
    private var mPlayPositionFeaturedTrack: Int = -1


    private var mTrackIdGlobal: String = ""
    private var mLastClickTime: Long = 0
    private var playList: MutableList<PlaylistCommon>? = ArrayList()
    private var playListImageView: ImageView? = null

    private var dashboardFragmentBinding: DashboardFragment1Binding? = null

    private val musicDashBoardViewModel: MusicDashBoardViewModel by viewModels()

    private var newTrackOfTheWeekAdapter: TrendingTrackAdapter? = null
    private var releasedForYouAdapter: TopTrackAdapter? = null
    private var featuredArtistAdapter: FeaturedArtistAdapter? = null
    private var trendingAdapter: TrendingTrackAdapter? = null

    private var mFeaturedTrackAdapter: FeaturedTrackAdapter? = null
    private var mPlayListForYouAdapter: HomePlayListAdapter? = null
    private var mMusicStationsForYouAdapter: StationAdapter? = null
    private var mResultMusicMetaResponse: ResultResponse<MusicMetaResponse>? = null
    private var stationPlayList: MutableList<MusicStation>? = ArrayList()
    private val mediaItems = LinkedList<MediaItem>()
    private val ID = 4 //Arbitrary, for the example
    private var mArtistNewList: MutableList<Artist>? = ArrayList()
    private var featuredArtists: MutableList<Artist>? = ArrayList()
    private var popularPlayListAdapter: ArtistsAlbumAdapter? = null
    private var editorsChoiceViewPagerAdapter: HomeViewPagerAdapter? = null
    private var releasedForYouViewPagerAdapter: HomeViewPagerAdapter? = null
    private var playlistManager: PlaylistManager? = null
    private var mPlayListAdapter: HomePlayListAdapter? = null
    private var PLAYLIST_ID = 401 //Arbitrary, for the example

    /**
     * Do not edit need to update like track all common list
     */
    companion object {
        var releasedForYouTrack: MutableList<TrackListCommon>? = ArrayList()
        var topTrackModel: MutableList<TrackListCommon>? = ArrayList()
        var weeklyNewTracks: MutableList<TrackListCommon>? = ArrayList()
        var trendingTrackModel: MutableList<TrackListCommon>? = ArrayList()
        var featuredTrackModelNew: List<TrackListCommon>? = ArrayList()
        var mEditorPlayList: List<EditorsChoicePlaylist>? = null
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        dashboardFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.dashboard_fragment1, container, false
        )
        return dashboardFragmentBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val intent = Intent(activity, Slide::class.java)
//        startActivity(intent)

        //TODO : Viewpager need to be 60% of screen height
        val parameterView: ViewGroup.LayoutParams =
                dashboardFragmentBinding?.viewpagerView?.layoutParams!!
        // Changes the height and width to the specified *pixels*
        val displayMetricsViewPagerView = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetricsViewPagerView)
        val screenHeightenedView: Double = displayMetricsViewPagerView.heightPixels * 0.60
        parameterView.height = screenHeightenedView.toInt()
        dashboardFragmentBinding?.viewpagerView?.layoutParams = parameterView

        // TODO : Whats new UI need to be 60% of the screen
        // Gets the layout params that will allow you to resize the layout
        if (dashboardFragmentBinding?.whatsNewRecyclerView != null) {
            val params: ViewGroup.LayoutParams =
                    dashboardFragmentBinding?.whatsNewRecyclerView?.layoutParams!!
            // Changes the height and width to the specified *pixels*
            val displayMetrics = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
            val screenHeight: Double = displayMetrics.heightPixels * 0.60
            params.height = screenHeight.toInt()
            dashboardFragmentBinding?.whatsNewRecyclerView?.layoutParams = params
            //dashboardFragmentBinding?.nestedScrollView?.scrollTo(0, 0)
        }
        if (dashboardFragmentBinding?.featuredArtistsRecyclerView != null) {
            val params: ViewGroup.LayoutParams =
                    dashboardFragmentBinding?.featuredArtistsRecyclerView?.layoutParams!!
            // Changes the height and width to the specified *pixels*
            val displayMetricsFeatured = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetricsFeatured)
            val screenHeightFeatured: Double = displayMetricsFeatured.heightPixels * 0.60
            params.height = screenHeightFeatured.toInt()
            dashboardFragmentBinding?.featuredArtistsRecyclerView?.layoutParams = params
        }

        // TODO : Google Ads for music dashboard
        // Preload google ads
        MobileAds.initialize(activity)

        //AmR Cmt
        /*dashboardFragmentBinding?.adViewTrack?.adListener = object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                dashboardFragmentBinding?.adViewTrack?.visibility = View.GONE
            }

        }
        dashboardFragmentBinding?.adViewTopTrack?.adListener = object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                dashboardFragmentBinding?.adViewTopTrackLinear?.visibility = View.GONE
                dashboardFragmentBinding?.adViewTopTrack?.visibility = View.GONE
            }
        }*/


        mFeaturedTrackAdapter = FeaturedTrackAdapter(
                requireActivity(), glideRequestManager,
                this
        )
        mPlayListAdapter = HomePlayListAdapter(
                mCallBack = this,
                mCallBackIntent = this,
                isFrom = "myPlayList",
                editPlayListDialog = this,
                mPlayListForYouViewAllItemClickListener = this, requestManager = glideRequestManager
        )
        mPlayListForYouAdapter = HomePlayListAdapter(
                mCallBack = this, mCallBackIntent = this,
                isFrom = "forYouPlayList", editPlayListDialog = this,
                mPlayListForYouViewAllItemClickListener = this, requestManager = glideRequestManager
        )
        trendingAdapter =
                TrendingTrackAdapter(
                        trackItemClickListener = this, mContext = activity, isNewTrack = false,
                        requestManager = glideRequestManager
                )

        newTrackOfTheWeekAdapter =
                TrendingTrackAdapter(
                        trackItemClickListener = this, mContext = activity, isNewTrack = true,
                        requestManager = glideRequestManager
                )
        releasedForYouAdapter =
                TopTrackAdapter(
                        topTrackItemClickListener = this, mContext = activity, isFrom = "TopTrack",
                        requestManager = glideRequestManager
                )
        //TODO:
        featuredArtistAdapter = FeaturedArtistAdapter(
                mArtistClickListener = this,
                isFrom = "", mContext = requireActivity(),
                requestManager = glideRequestManager
        )

        dashboardFragmentBinding?.goToOfflineMode?.setOnClickListener {
            if (activity != null) {
                (activity as HomeActivity).goToOffline()
            }
        }
        dashboardFragmentBinding?.refreshTextView?.setOnClickListener {
            apiCommon()
        }

        //AmR Cmt
        /*dashboardFragmentBinding?.goToMusic?.setOnClickListener {
            dashboardFragmentBinding?.genreRelative?.performClick()
        }
        dashboardFragmentBinding?.genreRelative?.setOnClickListener {
            val intent = Intent(activity, CommonFragmentActivity::class.java)
            intent.putExtra("isFrom", "Discover")
            startActivity(intent)
//            val intent = Intent(activity, ReelsVideoActivity::class.java)
//            val intent = Intent(activity, CardsSelectActivity::class.java)
//            intent.putExtra("isFrom", "Genre")
//            startActivity(intent)
        }
        dashboardFragmentBinding?.recentlyPlayedRelative?.setOnClickListener {
            val intent = Intent(activity, RecentlyPlayedActivity::class.java)
            intent.putExtra("isFrom", "recently_played_songs")
            startActivity(intent)
        }
        dashboardFragmentBinding?.likedSongsRelative?.setOnClickListener {
            val intent = Intent(activity, RecentlyPlayedActivity::class.java)
            intent.putExtra("isFrom", "liked_songs")
            startActivity(intent)
        }*/

        dashboardFragmentBinding?.newTrackOfTheWeekRecyclerView?.layoutManager =
                WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        //dashboardFragmentBinding?.popularAlbumRecyclerView?.layoutManager = WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        dashboardFragmentBinding?.featuredArtistsRecyclerView?.layoutManager =
                WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        dashboardFragmentBinding?.trendingTrackRecyclerView?.layoutManager =
                WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        dashboardFragmentBinding?.whatsNewRecyclerView?.layoutManager =
                WrapContentLinearLayoutManager(
                        activity, LinearLayoutManager.HORIZONTAL, false
                )

        dashboardFragmentBinding?.whatsNewRecyclerView?.adapter = mFeaturedTrackAdapter
        //dashboardFragmentBinding?.quickPlayListRecyclerView?.adapter = mPlayListAdapter
        dashboardFragmentBinding?.playListForYouRecyclerView?.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        dashboardFragmentBinding?.playListForYouRecyclerView?.adapter = mPlayListForYouAdapter
        dashboardFragmentBinding?.trendingTrackRecyclerView?.adapter = trendingAdapter
        dashboardFragmentBinding?.newTrackOfTheWeekRecyclerView?.adapter = newTrackOfTheWeekAdapter

        dashboardFragmentBinding?.swipeToRefresh?.setColorSchemeResources(R.color.purple_500)
        dashboardFragmentBinding?.swipeToRefresh?.isRefreshing = false
        dashboardFragmentBinding?.swipeToRefresh?.setOnRefreshListener {
            musicDashBoardViewModel.musicMetaLiveData?.value = null
            apiCommon()
            observeMusicMetaData()
        }

        // TODO : Call api and observe data
        apiCommon()
        observeMusicMetaData()
    }

    private fun createLocalPlayList(
            playListTrack: MutableList<TrackListCommon>, itemId: Int,
            playListName: String, needToPlay: Boolean
    ) {

        val mShuffleStatus =
                SharedPrefsUtils.getStringPreference(
                        activity, RemoteConstant.mShuffleStatus, "false"
                )
        musicDashBoardViewModel.createDBPlayListCommon(playListTrack, playListName) { playList ->
            if (mShuffleStatus == "true") {
                var selectedPosition = 0
                playList.map {
                    if (it.JukeBoxTrackId == itemId.toString()) {
                        selectedPosition = playList.indexOf(it)
                    }
                }
                val mSelectedSong: PlayListDataClass = playList[selectedPosition]
                val mGlobalFirstList: MutableList<PlayListDataClass> = ArrayList()
                val mGlobalListNew: MutableList<PlayListDataClass> = ArrayList()
                mGlobalListNew.addAll(playList)

                //TODO : if track is playing adding playing track as first track
                mGlobalListNew.removeAt(selectedPosition)
                mGlobalListNew.shuffle()

                mGlobalFirstList.clear()
                mGlobalFirstList.add(0, mSelectedSong)
                mGlobalFirstList.addAll(mGlobalListNew)
                if (mGlobalFirstList.isNotEmpty()) {
                    mediaItems.clear()
                    for (sample in mGlobalFirstList) {
                        val mediaItem = MediaItem(sample, true)
                        mediaItems.add(mediaItem)
                    }
                }
                mGlobalFirstList.map {
                    if (it.JukeBoxTrackId == itemId.toString()) {
                        mPlayPosition = mGlobalFirstList.indexOf(it)
                    }
                }
            } else {
                if (playList.isNotEmpty()) {
                    mediaItems.clear()
                    for (sample in playList) {
                        val mediaItem = MediaItem(sample, true)
                        mediaItems.add(mediaItem)
                    }
                }

                playList.map {
                    if (it.JukeBoxTrackId == itemId.toString()) {
                        mPlayPosition = playList.indexOf(it)
                    }
                }

            }
            if (needToPlay) {
                launchMusic()
            }
        }
        if (mTrackIdGlobal == "") {
            mTrackIdGlobal = mPlayPosition.toString()
        }
    }

    /**
     * Play selected track
     */
    private fun launchMusic() {
        playlistManager?.setParameters(mediaItems, mPlayPosition)
        playlistManager?.id = ID.toLong()
        playlistManager?.currentPosition = mPlayPosition
        playlistManager?.play(0, false)
    }

    /**
     * Music dashboard API call
     */
    private fun apiCommon() {
        if (!InternetUtil.isInternetOn()) {
            dashboardFragmentBinding?.progressLinear?.visibility = View.GONE
            dashboardFragmentBinding?.nsMusicDashbord?.visibility = View.GONE
            dashboardFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
            val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
            dashboardFragmentBinding?.noInternetLinear?.startAnimation(shake) // starts animation

            musicDashBoardViewModel.musicMetaLiveData?.postValue(
                    ResultResponse.noInternet(
                            MusicMetaResponse()
                    )
            )
        } else {
            callApi()
        }
    }

    /**
     * Music dashboard API call
     */
    private fun callApi() {
        val mAuth: String =
                RemoteConstant.getAuthWithoutOffsetAndCount(
                        requireActivity(),
                        RemoteConstant.musicmetadata
                )
        musicDashBoardViewModel.getMetaData(mAuth)

    }

    @SuppressLint("SetTextI18n")
    private fun observeMusicMetaData() {
        // TODO : Collect station data
        var isPlayListIsEmpty = true
        musicDashBoardViewModel.createDbPlayListModel?.nonNull()
                ?.observe(requireActivity(), { dbPlayList ->
                    if (dbPlayList != null && dbPlayList.isNotEmpty()) {
                        isPlayListIsEmpty = false
                    }

                })

        /**
         * Handle user playlist data
         */
        musicDashBoardViewModel.playListModel.nonNull().observe(requireActivity(), {
            if (it?.isNotEmpty()!!) {

                val playListTrack: List<PlaylistCommon> = it

                //Amr Cmt
                /*if (playListTrack.isNotEmpty()) {
                    dashboardFragmentBinding?.goToMusicLinear?.visibility = View.GONE
                    dashboardFragmentBinding?.quickPlayListRecyclerView?.visibility = View.VISIBLE

                } else {
                    dashboardFragmentBinding?.goToMusicLinear?.visibility = View.VISIBLE
                    dashboardFragmentBinding?.quickPlayListRecyclerView?.visibility = View.GONE
                }*/
                if (playListTrack.isNotEmpty()) {
                    val playListViewAll: MutableList<PlaylistCommon> = ArrayList()
                    val mList = PlaylistCommon()
                    mList.playlistName = ""
                    playListViewAll.add(mList)

                    playList = playListTrack.toMutableList()
                    if (playList?.size!! > 3) {
                        val subItems: MutableList<PlaylistCommon> = playList?.subList(0, 3)!!
                        subItems += playListViewAll
                        mPlayListAdapter?.submitList(subItems)
                    } else {
                        playList!! += playListViewAll
                        mPlayListAdapter?.submitList(playList)
                    }
                }

            }
        })

        /**
         * Handle music dashboard API data
         */
        musicDashBoardViewModel.musicMetaLiveData?.nonNull()
                ?.observe(requireActivity(), { resultResponse ->

                    if (dashboardFragmentBinding?.swipeToRefresh?.isRefreshing!!) {
                        dashboardFragmentBinding?.swipeToRefresh?.isRefreshing = false
                    }
                    if (resultResponse != null && resultResponse.status == ResultResponse.Status.SUCCESS) {


                        mResultMusicMetaResponse = resultResponse

                        dashboardFragmentBinding?.nsMusicDashbord?.visibility = View.VISIBLE
                        dashboardFragmentBinding?.progressLinear?.visibility = View.GONE
                        dashboardFragmentBinding?.noInternetLinear?.visibility = View.GONE

                        // TODO : Trending tracks
                        val trendingTrackList: List<TrackListCommon> =
                                resultResponse.data?.payload?.trendingTracks ?: ArrayList()
                        if (trendingTrackList.isNotEmpty()) {
                            trendingTrackModel = trendingTrackList.toMutableList()
                            trendingAdapter?.swap(trendingTrackModel ?: ArrayList())
                        }
                        // TODO : Artists

                        mArtistNewList =
                                resultResponse.data?.payload?.latestArtists

                        dashboardFragmentBinding?.artistRecyclerView?.layoutManager =
                            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        val mArtistAdapter = ArtistsAdapter(
                                mArtistClickListener = this,
                                isFrom = "",
                                mContext = requireActivity(),
                                requestManager = glideRequestManager
                        )
                        dashboardFragmentBinding?.artistRecyclerView?.adapter = mArtistAdapter
                        mArtistAdapter.submitList(mArtistNewList)


                        // TODO : releasesForYou Tracks
                        releasedForYouTrack =
                                resultResponse.data?.payload?.releasesForYou ?: ArrayList()
                        releasedForYouViewPagerAdapter = HomeViewPagerAdapter(childFragmentManager)
                        if (releasedForYouTrack?.isNotEmpty()!!) {

                            if (releasedForYouTrack?.size!! >= 5) {
                                val mEditorsChoicePlaylist =
                                        EditorsChoicePlaylist()
                                val mSubList = releasedForYouTrack?.subList(0, 5)
                                mEditorsChoicePlaylist.tracks = mSubList
                                mEditorsChoicePlaylist.id = System.currentTimeMillis().toString()
                                mEditorsChoicePlaylist.playlistName = ""
                                releasedForYouViewPagerAdapter?.addFragments(
                                        FragmentEditorsChoice.newInstance(
                                                "Release for You",
                                                mEditorsChoicePlaylist, requireActivity()
                                        )
                                )
                            }
                            if (releasedForYouTrack?.size!! >= 10) {
                                val mEditorsChoicePlaylist =
                                        EditorsChoicePlaylist()
                                val mSubList = releasedForYouTrack?.subList(5, 10)
                                mEditorsChoicePlaylist.tracks = mSubList
                                mEditorsChoicePlaylist.id = System.currentTimeMillis().toString()
                                mEditorsChoicePlaylist.playlistName = ""
                                releasedForYouViewPagerAdapter?.addFragments(
                                        FragmentEditorsChoice.newInstance(
                                                "Release for You",
                                                mEditorsChoicePlaylist,
                                                requireActivity()
                                        )
                                )
                            }
                            if (releasedForYouTrack?.size!! >= 15) {
                                val mEditorsChoicePlaylist =
                                        EditorsChoicePlaylist()
                                val mSubList = releasedForYouTrack?.subList(10, 15)
                                mEditorsChoicePlaylist.tracks = mSubList
                                mEditorsChoicePlaylist.id = System.currentTimeMillis().toString()
                                mEditorsChoicePlaylist.playlistName = ""
                                releasedForYouViewPagerAdapter?.addFragments(
                                        FragmentEditorsChoice.newInstance(
                                                "Release for You",
                                                mEditorsChoicePlaylist,
                                                requireActivity()
                                        )
                                )
                            }

                            //AmR Cmt
                            /*if (releasedForYouTrack?.size!! >= 5) {
                                dashboardFragmentBinding?.releasesForYouLinear?.visibility =
                                        View.VISIBLE
                                dashboardFragmentBinding?.releasesForYouChoiceViewPager?.offscreenPageLimit =
                                        3
                                dashboardFragmentBinding?.releasesForYouChoiceViewPager?.adapter =
                                        releasedForYouViewPagerAdapter
                                dashboardFragmentBinding?.releasesForYouDotsIndicator?.attachToPager(
                                        dashboardFragmentBinding?.releasesForYouChoiceViewPager!!
                                )
                            } else {
                                dashboardFragmentBinding?.releasesForYouLinear?.visibility = View.GONE
                            }*/


                        } else {
                            //AmR Cmt
                            //dashboardFragmentBinding?.releasesForYouLinear?.visibility = View.GONE

                        }

                        // TODO : New Track of the week
                        weeklyNewTracks =
                                resultResponse.data?.payload?.weeklyNewTracks ?: ArrayList()
                        if (weeklyNewTracks?.isNotEmpty()!!) {
                            newTrackOfTheWeekAdapter?.swap(weeklyNewTracks ?: ArrayList())
                        }


                        //TODO: Editor choice
                        mEditorPlayList =
                                resultResponse.data?.payload?.editorsChoicePlaylist ?: ArrayList()

                        editorsChoiceViewPagerAdapter = HomeViewPagerAdapter(childFragmentManager)
                        for (i in mEditorPlayList?.indices!!) {
                            editorsChoiceViewPagerAdapter?.addFragments(
                                    FragmentEditorsChoice.newInstance(
                                            mEditorPlayList?.get(i)?.playlistName,
                                            mEditorPlayList?.get(i)!!,
                                            requireActivity()
                                    )
                            )
                        }
                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            dashboardFragmentBinding?.editorChoiceViewPager?.offscreenPageLimit =
                                    mEditorPlayList?.size!!
                            dashboardFragmentBinding?.editorChoiceViewPager?.adapter =
                                    editorsChoiceViewPagerAdapter
                            //dashboardFragmentBinding?.editorChoiceDotsIndicator?.attachToPager(dashboardFragmentBinding?.editorChoiceViewPager!!)
                            dashboardFragmentBinding?.editorChoiceViewPager?.currentItem = 0
                            if (mEditorPlayList?.size ?: 0 > 0) {
                                //dashboardFragmentBinding?.titleTextViewEditorsChoice?.text = mEditorPlayList?.get(0)?.playlistName
                            }
                            dashboardFragmentBinding?.editorChoiceViewPager?.addOnPageChangeListener(
                                    object :
                                            androidx.viewpager.widget.ViewPager.OnPageChangeListener {

                                        override fun onPageScrollStateChanged(state: Int) {
                                        }

                                        override fun onPageScrolled(
                                                position: Int,
                                                positionOffset: Float,
                                                positionOffsetPixels: Int
                                        ) {

                                        }

                                        override fun onPageSelected(position: Int) {
                                            //dashboardFragmentBinding?.titleTextViewEditorsChoice?.text = mEditorPlayList?.get(position)?.playlistName
                                        }
                                    })

                        }, 2000)

                        // TODO : Featured Tracks
                        featuredTrackModelNew = resultResponse.data?.payload?.featuredTracks!!
                        featuredArtists = resultResponse.data.payload.featuredArtists
                        if (featuredTrackModelNew != null && featuredTrackModelNew?.isNotEmpty()!!) {
                            //dashboardFragmentBinding?.nestedScrollView?.scrollTo(0, 0)
                            setAdapterViewPager()
                        }

                        //AmR Cmt
                       /* val mRemoteImages: RemoteImages =
                                resultResponse.data.payload.remoteImages ?: RemoteImages()
                        dashboardFragmentBinding?.genreTextView?.text = "Artists & Genres"
                        dashboardFragmentBinding?.likedSongsTextView?.text =
                            mRemoteImages.likedTracks?.text?.replace(" ", "\n")
                        dashboardFragmentBinding?.recentlyPlayedTextView?.text =
                                mRemoteImages.lastPlayed?.text?.replace(" ", "\n")


                        glideRequestManager.load(mRemoteImages.genre?.coverImage).apply(
                                RequestOptions.placeholderOf(R.drawable.genre_bg).error(R.drawable.genre_bg)
                                        .error(R.drawable.genre_bg)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                        ).into(dashboardFragmentBinding?.genreImageView!!)

                        glideRequestManager.load(mRemoteImages.likedTracks?.coverImage).apply(
                                RequestOptions.placeholderOf(R.drawable.liked_track_bg)
                                        .error(R.drawable.liked_track_bg)
                                        .error(R.drawable.liked_track_bg)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                        ).into(dashboardFragmentBinding?.likedSongsImageView!!)

                        glideRequestManager.load(mRemoteImages.lastPlayed?.coverImage).apply(
                                RequestOptions.placeholderOf(R.drawable.recently_played_bg)
                                        .error(R.drawable.recently_played_bg)
                                        .error(R.drawable.recently_played_bg)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                        ).into(dashboardFragmentBinding?.recentlyPlayedImageView!!)*/


                        // TODO : My PlayList

                        val playListTrack: List<PlaylistCommon>? =
                                resultResponse.data.payload.myPlaylists

                        //AmR Cmt
                       /* if (playListTrack?.isNotEmpty()!!) {
                            dashboardFragmentBinding?.goToMusicLinear?.visibility = View.GONE
                            dashboardFragmentBinding?.quickPlayListRecyclerView?.visibility =
                                    View.VISIBLE

                        } else {
                            dashboardFragmentBinding?.goToMusicLinear?.visibility = View.VISIBLE
                            dashboardFragmentBinding?.quickPlayListRecyclerView?.visibility = View.GONE
                        }*/
                        if (playListTrack!!.isNotEmpty()) {
                            val playListViewAll: MutableList<PlaylistCommon> = ArrayList()
                            val mList = PlaylistCommon()
                            mList.playlistName = ""
                            playListViewAll.add(mList)

                            playList = playListTrack.toMutableList()
                            if (playList?.size!! > 3) {
                                val subItems: MutableList<PlaylistCommon> = playList?.subList(0, 3)!!
                                subItems += playListViewAll
                                mPlayListAdapter?.submitList(subItems)
                            } else {
                                playList!! += playListViewAll
                                mPlayListAdapter?.submitList(playList)
                            }
                        }


                        val playlistForYou: MutableList<PlaylistCommon>? =
                                resultResponse.data.payload.playlistsForYou?.toMutableList()

                        val playListForYouViewAll: MutableList<PlaylistCommon> = ArrayList()
                        val mList = PlaylistCommon()
                        mList.playlistName = "playListForYouViewAllItem"
                        playListForYouViewAll.add(mList)
                        playlistForYou?.addAll(playListForYouViewAll)
                        mPlayListForYouAdapter?.submitList(playlistForYou)


                        // TODO : Popular playlist
                        val popularPlaylist: MutableList<ArtistDetailsPayloadAlbum> =
                                resultResponse.data.payload.popularAlbums ?: ArrayList()
                        if (popularPlaylist != null && popularPlaylist.size > 0) {
                            popularPlayListAdapter = ArtistsAlbumAdapter(
                                    mContext = activity,
                                    albums = popularPlaylist,
                                    mArtistClickListener = this,
                                    glideRequestManager = glideRequestManager
                            )
                            //dashboardFragmentBinding?.popularAlbumRecyclerView?.adapter = popularPlayListAdapter
                        }
                        // TODO : Station List adapter
                        if (stationPlayList?.isNotEmpty()!!) {
                            stationPlayList?.clear()
                        }
                        stationPlayList = resultResponse.data.payload.stations?.toMutableList()

                        val adapterstations = ViewPagerAdapter(childFragmentManager)
                        /*for (i in 0 until 5) {
                            stationPlayList?.get(i)?.genreName?.let {
                                adapterstations.addFragments(
                                    StationsDetailsFragment(stationsList), it
                                )
                            }
                        }*/
                        dashboardFragmentBinding?.viewpagerStations?.setAdapter(adapterstations)
                        dashboardFragmentBinding?.dotsIndicatorStations?.setViewPager(dashboardFragmentBinding?.viewpagerStations!!)

                        // TODO : Add first item global radio
//                    val mMusicStation = MusicStation()
//                    mMusicStation.id = "0"
//                    stationPlayList?.add(0, mMusicStation)
                        mMusicStationsForYouAdapter = StationAdapter(
                                mContext = activity,
                                mMusicStationClickListener = this, requestManager = glideRequestManager
                        )


                        //AmR Cmt
                       /* dashboardFragmentBinding?.musicStationsForYouRecyclerView?.adapter =
                                mMusicStationsForYouAdapter
                        mMusicStationsForYouAdapter?.swap(stationPlayList ?: ArrayList())
                        if (stationPlayList?.size == 0) {
                            dashboardFragmentBinding?.musicStationsForYouRecyclerView?.visibility =
                                    View.GONE
                            dashboardFragmentBinding?.stationLinear?.visibility = View.GONE
                        }*/
                        //TODO : Create playlist if playlist is empty
                        if (isPlayListIsEmpty) {
                            createLocalPlayList(
                                    featuredTrackModelNew?.toMutableList() ?: ArrayList(),
                                    featuredTrackModelNew?.get(0)?.numberId?.toInt() ?: 0,
                                    "Featured tracks", false
                            )
                        }
                    } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.ERROR) {
                        dashboardFragmentBinding?.progressLinear?.visibility = View.GONE
                        dashboardFragmentBinding?.nsMusicDashbord?.visibility = View.GONE
                        dashboardFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
                        glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter()
                        )
                                .into(dashboardFragmentBinding?.noInternetImageView!!)
                        dashboardFragmentBinding?.noInternetTextView?.text =
                                getString(R.string.server_error)
                        dashboardFragmentBinding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                    } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.LOADING) {

                        dashboardFragmentBinding?.progressLinear?.visibility = View.VISIBLE
                        dashboardFragmentBinding?.nsMusicDashbord?.visibility = View.GONE
                        dashboardFragmentBinding?.noInternetLinear?.visibility = View.GONE

                    } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.NO_INTERNET) {
                        glideRequestManager.load(R.drawable.ic_app_offline)
                                .apply(RequestOptions().fitCenter())
                                .into(dashboardFragmentBinding?.noInternetImageView!!)
                        dashboardFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
                        dashboardFragmentBinding?.progressLinear?.visibility = View.GONE
                        dashboardFragmentBinding?.nsMusicDashbord?.visibility = View.GONE
                        val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                        dashboardFragmentBinding?.noInternetLinear?.startAnimation(shake) // starts animation
                    }

                    val adRequest =
                            AdRequest.Builder()
                                    .addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0")
                                    .build()

                    //AmR Cmt
                    /*dashboardFragmentBinding?.adViewTrack?.loadAd(adRequest)
                    dashboardFragmentBinding?.adViewTopTrack?.loadAd(adRequest)*/

                })


    }

    /**
     * Featured artist viewpger
     */
    private fun setAdapterViewPager() {
        if (featuredTrackModelNew?.isNotEmpty()!!) {
            mFeaturedTrackAdapter?.swap(featuredTrackModelNew ?: ArrayList())
            val startSnapHelperWhatsNew = StartSnapHelper()
            dashboardFragmentBinding?.whatsNewRecyclerView?.onFlingListener = null
            startSnapHelperWhatsNew.attachToRecyclerView(dashboardFragmentBinding?.whatsNewRecyclerView!!)
          dashboardFragmentBinding?.dotsIndicatorFeaturedTracks?.attachToRecyclerView(dashboardFragmentBinding?.whatsNewRecyclerView!! )


            val layoutManagerFeaturedArtistsRecyclerView = WrapContentLinearLayoutManager(
                    activity,
                    LinearLayoutManager.HORIZONTAL, false
            )
            dashboardFragmentBinding?.featuredArtistsRecyclerView?.layoutManager =
                    layoutManagerFeaturedArtistsRecyclerView
            dashboardFragmentBinding?.featuredArtistsRecyclerView?.adapter =
                    featuredArtistAdapter
            featuredArtistAdapter?.submitList(featuredArtists)
            dashboardFragmentBinding?.featuredArtistsRecyclerView?.onFlingListener = null
            val startSnapHelper = StartSnapHelper()
            startSnapHelper.attachToRecyclerView(dashboardFragmentBinding?.featuredArtistsRecyclerView!!)
            //dashboardFragmentBinding?.featuredArtistsDotsIndicator?.attachToRecyclerView(dashboardFragmentBinding?.featuredArtistsRecyclerView!! )

        }

        // TODO : tap naviation
        if (activity != null) {
            if (!SharedPrefsUtils.getBooleanPreference(
                            requireActivity(), RemoteConstant.isTapTargetPlayedHome, false
                    )
            ) {
                SharedPrefsUtils.setBooleanPreference(
                        requireActivity(), RemoteConstant.isTapTargetPlayedHome, true
                )
                //TODO : Tap target in music dashboard page
                // https://github.com/TakuSemba/Spotlight
                (activity as HomeActivity).tapTarget()
            }
        }
    }

    /**
     * Top track play
     */
    override fun onTopTrackItemClick(itemId: Int, itemImage: String, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            removeStationAnimation()
            itemTrackId = itemId
            playTrackCommon(itemId, topTrackModel ?: ArrayList(), "Top tracks")
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    /**
     * Trending and weekly track play
     */
    override fun onTrendingItemClick(itemId: Int, itemImage: String, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            removeStationAnimation()
            itemTrackId = itemId
            if (isFrom == "TrendingTrack") {
                playTrackCommon(itemId, trendingTrackModel ?: ArrayList(), "Trending tracks")
            } else {
                playTrackCommon(itemId, weeklyNewTracks ?: ArrayList(), "New tracks this week")
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    /**
     * Whats new track play
     */
    override fun onMusicDashboardItemClick(itemId: Int, itemImage: String, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            removeStationAnimation()
            playTrackCommon(itemId, featuredTrackModelNew ?: ArrayList(), "Featured tracks")
        }

        mLastClickTime = SystemClock.elapsedRealtime()
    }

    /**
     * Navigation to now playing page
     * If user click already playing track we need to navigate to Now playing page
     */
    private fun playTrackCommon(
            itemId: Int,
            newTrack: List<TrackListCommon>,
            playListName: String
    ) {
        if (itemId == playlistManager?.currentItemChange?.currentItem?.id?.toInt() ?: 0) {

            if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                var mImageUrlPass = ""
                if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                ) {
                    mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl
                                    ?: ""
                }
                val detailIntent =
                        Intent(activity, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                startActivity(detailIntent)
            } else if (!playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                playlistManager?.play(
                        playlistManager?.currentProgress?.position ?: 0,
                        false
                )
            } else {
                itemTrackId = itemId
                createLocalPlayList(newTrack.toMutableList(), itemId, playListName, true)
            }
        } else {
            itemTrackId = itemId
            createLocalPlayList(newTrack.toMutableList(), itemId, playListName, true)
        }
        mTrackIdGlobal = itemId.toString()
    }


    override fun onItemClickViewAllUserPlayList() {
        val intent = Intent(activity, PlayListAllActivity::class.java)
        startActivityForResult(intent, 1001)
    }

    /**
     * User Playlist navigation
     */
    override fun onItemClickUserPlayList(
            item: String, mTitle: String, mImageUrl: String, isFrom: String
    ) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            val intent = Intent(activity, CommonPlayListActivity::class.java)
            intent.putExtra("mId", item)
            intent.putExtra("title", mTitle)
            intent.putExtra("imageUrl", mImageUrl)
            intent.putExtra("mIsFrom", isFrom)
            startActivity(intent)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    /**
     * Playlist navigation
     */
    override fun onMusicDashboardPlayListClick(
            playListId: String, playListName: String, isFrom: String
    ) {
        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", playListId)
        intent.putExtra("title", playListName)
        intent.putExtra("mIsFrom", "fromPlayList")
        startActivity(intent)
    }

    /**
     * Create an new playlist API
     */
    private fun createPlayListApiNew(
            playListId: String, playListName: String, private: String, icon_id: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody =
                    CreatePlayListBody(playListId, playListName, icon_id, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(requireActivity())
            musicDashBoardViewModel.createPlayList(mAuth, createPlayListBody)
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }
    }

    override fun onPlayListItemClick(mId: String) {
        mPlayListId = mId
    }

    /**
     * Navigation to Artist details page
     * We have two list from API Top 5 and featured Artist
     * Passing artist details as parsable array list
     */
    override fun onArtistClickListener(
            mArtistId: String, mArtistPosition: String, mIsFrom: String
    ) {

        val detailIntent = Intent(activity, ArtistViewPagerActivity::class.java)
        detailIntent.putExtra("mArtistPosition", mArtistPosition)
        if (mIsFrom == "TopFive") {
            detailIntent.putParcelableArrayListExtra(
                    "mArtistList",
                    mArtistNewList as java.util.ArrayList<out Parcelable>
            )
        } else {
            detailIntent.putParcelableArrayListExtra(
                    "mArtistList",
                    featuredArtists as java.util.ArrayList<out Parcelable>
            )
        }
        startActivity(detailIntent)
        activity?.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)

    }

    /**
     * Playlist option Ui and functionality
     */
    override fun optionDialog(
            playlistId: String?, playlistName: String?, private: String,
            icon: PlaylistDataIcon, adapterPosition: Int, isFrom: String
    ) {
        mLastId = ""
        val viewGroup: ViewGroup? = null
        val mBottomSheetDialog = RoundedBottomSheetDialog(requireActivity())
        val sheetView: View = layoutInflater.inflate(R.layout.play_list_options, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val editLinear: LinearLayout = sheetView.findViewById(R.id.edit_linear)
        val deleteLinear: LinearLayout = sheetView.findViewById(R.id.delete_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)

        if (isFrom == "forYouPlayList") {
            editLinear.visibility = View.GONE
            deleteLinear.visibility = View.GONE
        }
        if (private == "1") {
            shareLinear.visibility = View.GONE
        }


        editLinear.setOnClickListener {
            mBottomSheetDialog.dismiss()
            onOptionItemClickOption(
                    "editPlayList",
                    playlistId!!, playlistName!!, private, icon
            )
        }
        deleteLinear.setOnClickListener {
            val viewGroupNew: ViewGroup? = null
            val dialogBuilder: AlertDialog.Builder =
                    this.let { AlertDialog.Builder(requireActivity()) }
            val inflater: LayoutInflater = layoutInflater
            val dialogView: View = inflater.inflate(R.layout.common_dialog, viewGroupNew)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
            val okButton: Button = dialogView.findViewById(R.id.ok_button)

            val b: AlertDialog = dialogBuilder.create()
            b.show()
            okButton.setOnClickListener {

                mBottomSheetDialog.dismiss()
                onOptionItemClickOption(
                        "deletePlayList",
                        playlistId!!, "", private, PlaylistDataIcon()
                )
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

            }
            cancelButton.setOnClickListener {
                mBottomSheetDialog.dismiss()
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

            }
        }
        shareLinear.setOnClickListener {
            ShareAppUtils.sharePlayList(playlistId, requireActivity())
            mBottomSheetDialog.dismiss()

        }
    }

    /**
     * Common track option ( Delete and edit )
     */
    fun onOptionItemClickOption(
            mMethod: String, mId: String, mPlayListName: String, private: String, icon: PlaylistDataIcon
    ) {
        when (mMethod) {
            "deletePlayList" -> deletePlayList(mId)
            "editPlayList" -> editPlayList(mId, mPlayListName, private, icon)
        }
    }

    /**
     * Edit user playlist Ui and functionality
     */
    private fun editPlayList(
            mId: String, mPlayListName: String, private: String, icon: PlaylistDataIcon
    ) {
        var iconFileUrl = ""
        if (icon.id != null) {
            iconFileUrl = icon.fileUrl!!
        }

        if (icon.id != null) {
            mLastId = icon.id
        }

        var isPrivate: String = private
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val createPlayListTextView: TextView =
                dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
                dialogView.findViewById(R.id.play_list_name_edit_text)
        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListNameEditText.setText(mPlayListName)
        createPlayListTextView.isAllCaps = true
        createPlayListTextView.text = getString(R.string.save)


        val switchButtonPlayList: SwitchButton =
                dialogView.findViewById(R.id.switch_button_play_list)

        switchButtonPlayList.isChecked = private != "0"


        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        val alertDialog: AlertDialog = dialogBuilder.create()
        alertDialog.show()


        if (icon.fileUrl != null) {
            glideRequestManager.load(icon.fileUrl).into(playListImageView!!)
        }

        playListImageView?.setOnClickListener {
            val intent = Intent(activity, PlayListImageActivity::class.java)
            intent.putExtra("iconId", mLastId)
            intent.putExtra("iconFileUrl", iconFileUrl)
            startActivityForResult(intent, 1001)
        }

        closeButton.setOnClickListener {
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        createPlayListApiNew(
                                mId,
                                playListNameEditText.text.toString(),
                                isPrivate,
                                mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(
                                requireActivity(),
                                getString(R.string.have_play_list)
                        )
                    }
                } else
                    AppUtils.showCommonToast(requireActivity(), "Playlist name too short")
            } else
                AppUtils.showCommonToast(requireActivity(), "Playlist name cannot be empty")
            //To close alert
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    /**
     * Case when user updating playlist image
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }
                    if (playListImageView != null) {
                        glideRequestManager
                                .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                                .into(playListImageView!!)
                    }
                }

            }
        }

    }

    /**
     * Delete user playlist API
     */
    private fun deletePlayList(mId: String) {
        val mAuth: String = RemoteConstant.getAuthDeletePlayList(requireActivity(), mId)
        musicDashBoardViewModel.deletePlayList(mAuth, mId)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    // TODO : Update like event
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onMessageEvent(event: UpdateTrackEvent) {
        updateLike(event.mTrackId, event.mFavourite)
    }

    // TODO : Update like event
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onMessageEvent(event: RemoveStationAnimationEvent) {
        removeStationAnimation()
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onMessageEvent(event: PauseEvent) {
//        updatePause()
//    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onMessageEvent(event: UpdateTrackPlayingEvent) {
        if (event.mTrackId == "") {
            // TODO : isPlayedTrack status for ( first time we have launching recently played track)
            if (isPlayedTrack) {
                updatePause()
            }
        } else {
            isPlayedTrack = true
            updatePLaying(event.mTrackId)
        }
    }

    /**
     * To update track list locally
     */
    private fun updateLike(mTrackId: String?, liked: String) {

        for (i in 0 until topTrackModel?.size!!) {
            if (topTrackModel?.get(i)?.id == mTrackId) {
                topTrackModel?.get(i)?.favorite = liked
                isUpdatedPlayLIst = true
            }
        }
        for (i in 0 until trendingTrackModel?.size!!) {
            if (trendingTrackModel?.get(i)?.id == mTrackId) {
                trendingTrackModel?.get(i)?.favorite = liked
                isUpdatedPlayLIst = true
            }
        }
        for (i in 0 until featuredTrackModelNew?.size!!) {
            if (featuredTrackModelNew?.get(i)?.id == mTrackId) {
                featuredTrackModelNew?.get(i)?.favorite = liked
                isUpdatedPlayLIst = true
            }
        }
        for (i in 0 until weeklyNewTracks?.size!!) {
            if (weeklyNewTracks?.get(i)?.id == mTrackId) {
                weeklyNewTracks?.get(i)?.favorite = liked
                isUpdatedPlayLIst = true
            }
        }

        if (isUpdatedPlayLIst) {
            if (trendingTrackModel?.isNotEmpty()!!) {
                trendingAdapter?.swap(trendingTrackModel ?: ArrayList())
            }
            if (featuredTrackModelNew?.isNotEmpty()!!) {
                mFeaturedTrackAdapter?.swap(featuredTrackModelNew ?: ArrayList())
            }
            if (weeklyNewTracks?.isNotEmpty()!!) {
                newTrackOfTheWeekAdapter?.swap(weeklyNewTracks ?: ArrayList())
            }
            isUpdatedPlayLIst = false
        }
    }

    /**
     * Play status update for music dashboard page
     */
    private fun updatePLaying(mTrackId: String?) {

        //TODO : For Editors choice viewpager
        EventBus.getDefault().post(UpdateViewpagerList(mTrackId))
        mTrackIdGlobal = mTrackId!!

        if (topTrackModel != null && topTrackModel?.size!! > 0 && mPlayPositionTopTrack != -1) {
            topTrackModel?.get(mPlayPositionTopTrack)?.isPlaying = false
            releasedForYouAdapter?.notifyItemChanged(mPlayPositionTopTrack)
        }

        if (trendingTrackModel != null && trendingTrackModel?.size!! > 0 && mPlayPositionTrendingTrack != -1) {
            trendingTrackModel?.get(mPlayPositionTrendingTrack)?.isPlaying = false
            trendingAdapter?.notifyItemChanged(mPlayPositionTrendingTrack)
        }
        if (weeklyNewTracks != null && weeklyNewTracks?.size!! > 0 && mPlayPositionweeklyNewTracks != -1) {
            weeklyNewTracks?.get(mPlayPositionweeklyNewTracks)?.isPlaying = false
            newTrackOfTheWeekAdapter?.notifyItemChanged(mPlayPositionweeklyNewTracks)
        }

        topTrackModel?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                mPlayPositionTopTrack = topTrackModel?.indexOf(it)!!
            } else {
                it.isPlaying = false
            }
        }
        weeklyNewTracks?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                mPlayPositionweeklyNewTracks = weeklyNewTracks?.indexOf(it)!!
            } else {
                it.isPlaying = false
            }
        }
        trendingTrackModel?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                mPlayPositionTrendingTrack = trendingTrackModel?.indexOf(it)!!
            } else {
                it.isPlaying = false
            }
        }
        featuredTrackModelNew?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                mPlayPositionFeaturedTrack = featuredTrackModelNew?.indexOf(it)!!
            } else {
                it.isPlaying = false
            }
        }

        if (mPlayPositionTrendingTrack != -1) {
            trendingAdapter?.notifyItemChanged(mPlayPositionTrendingTrack)
        }

        dashboardFragmentBinding?.whatsNewRecyclerView?.post {
            mFeaturedTrackAdapter?.swap(featuredTrackModelNew ?: ArrayList())
            mFeaturedTrackAdapter?.notifyDataSetChanged()
        }
        dashboardFragmentBinding?.newTrackOfTheWeekRecyclerView?.post {
            newTrackOfTheWeekAdapter?.swap(weeklyNewTracks ?: ArrayList())
            newTrackOfTheWeekAdapter?.notifyDataSetChanged()
        }

        isPlayedTrack = true
    }

    /**
     * Remove station animation when user playing an track from list
     */
    fun removeStationAnimation() {
        //TODO : Need to update playlist animation

        val mMusicStation: MusicStation? = stationPlayList?.find { it.isPlaying == true }
        if (mMusicStation != null && mMusicStation.isPlaying == true) {
            stationPlayList?.map {
                it.isPlaying = false
            }
            mMusicStationsForYouAdapter?.swap(stationPlayList ?: ArrayList())
            mMusicStationsForYouAdapter?.notifyDataSetChanged()
        }
    }

    /**
     * Pause status update for music dashboard page
     */
    private fun updatePause() {

        //TODO : For Editors choice
        EventBus.getDefault().post(UpdateViewpagerList(""))
        if (topTrackModel != null && topTrackModel?.size!! > 0 && mPlayPositionTopTrack != -1) {
            topTrackModel?.get(mPlayPositionTopTrack)?.isPlaying = false
            releasedForYouAdapter?.notifyItemChanged(mPlayPositionTopTrack)
        }

        if (trendingTrackModel != null && trendingTrackModel?.size!! > 0 && mPlayPositionTrendingTrack != -1) {
            trendingTrackModel?.get(mPlayPositionTrendingTrack)?.isPlaying = false
            trendingAdapter?.notifyItemChanged(mPlayPositionTrendingTrack)
        }
        if (weeklyNewTracks != null && weeklyNewTracks?.size!! > 0 && mPlayPositionweeklyNewTracks != -1) {
            weeklyNewTracks?.get(mPlayPositionweeklyNewTracks)?.isPlaying = false
            newTrackOfTheWeekAdapter?.notifyItemChanged(mPlayPositionweeklyNewTracks)
        }

        topTrackModel?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
                mPlayPositionTopTrack = topTrackModel?.indexOf(it)!!
            }
        }
        trendingTrackModel?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
                mPlayPositionTrendingTrack = trendingTrackModel?.indexOf(it)!!
            }
        }
        featuredTrackModelNew?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
                mPlayPositionFeaturedTrack = featuredTrackModelNew?.indexOf(it)!!
            }
        }
        weeklyNewTracks?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
                mPlayPositionFeaturedTrack = weeklyNewTracks?.indexOf(it)!!
            }
        }

        if (mPlayPositionweeklyNewTracks != -1) {
            newTrackOfTheWeekAdapter?.swap(weeklyNewTracks ?: ArrayList())
        }

        if (mPlayPositionTrendingTrack != -1) {
            trendingAdapter?.notifyItemChanged(mPlayPositionTrendingTrack)
        }

        dashboardFragmentBinding?.whatsNewRecyclerView?.post {
            mFeaturedTrackAdapter?.swap(featuredTrackModelNew ?: ArrayList())
            mFeaturedTrackAdapter?.notifyDataSetChanged()
        }

    }

    /**
     * Global Play status update for music dashboard page
     */
    fun updatePlayPause(mTrackId: String) {
        try {
            if (activity != null) {
                if (playlistManager?.playlistHandler != null
                        && playlistManager?.playlistHandler?.currentMediaPlayer != null
                        && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
                ) {
                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying == true) {
                            updatePLaying(mTrackId)
                        } else {
                            if (mTrackIdGlobal != "") {
                                if (mTrackIdGlobal != mTrackId) {
                                    updatePLaying(mTrackId)
                                } else {
                                    updatePause()
                                }
                            } else {
                                updatePause()
                            }
                        }
                    }, 1000)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onItemClickPlayListForYou() {
        val intent = Intent(activity, CommonFragmentActivity::class.java)
        intent.putExtra("isFrom", "PlayListForYouViewAll")
        startActivity(intent)
    }


    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)
        onPlaybackStateChanged(playlistManager?.currentPlaybackState!!)
        val mPlayListName=playlistManager?.currentItem?.playListName?:""
        if (!mPlayListName.contains("Music station") &&
                mPlayListName != "socialmob station"
        ) {
                removeStationAnimation()
        }
    }


    override fun onPlaylistItemChanged(
            currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.PAUSED -> {
                removeStationAnimation()
            }
            else -> {
            }
        }
        return true
    }

    /**
     * Artist album track list
     */
    override fun onArtistAlbumClickListener(
            mArtistId: String, mTitle: String, imageUrl: String, mDescription: String
    ) {
        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", mArtistId)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", imageUrl)
        intent.putExtra("mDescription", mDescription)
        intent.putExtra("mIsFrom", "artistAlbum")
        startActivity(intent)
    }

    /**
     * Common method to update playlist
     */
    fun updatePlayList(playListTrack: MutableList<PlaylistCommon>) {

        if (playListTrack.isNotEmpty()) {
            val playListViewAll: MutableList<PlaylistCommon> = ArrayList()
            val mList = PlaylistCommon()
            mList.playlistName = ""
            playListViewAll.add(mList)

            val playList: MutableList<PlaylistCommon> = playListTrack.toMutableList()
            if (playList.size > 3) {
                val subItems: MutableList<PlaylistCommon> = playList.subList(0, 3)
                subItems += playListViewAll
                mPlayListAdapter?.submitList(subItems)
            } else {
                playList += playListViewAll
                mPlayListAdapter?.submitList(playList)
            }
        }
    }


    override fun onMusicStationClickListener(
            mMusicStationId: String, mTitle: String, imageUrl: String, adapterPosition: Int, mMusicStationName: String
    ) {
        isMusicStationCallCompleted = false
        updatePause()
        if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying == true) {
            playlistManager?.invokePausePlay()
        }
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({

            if (adapterPosition == 0) {
                val task =
                        CreatePlayListDialog(
                                requireActivity(),
                                "radio",
                                mMusicStationId,""
                        ) // pass the context to the constructor
                task.execute()
            } else {
                val task =
                        CreatePlayListDialog(
                                requireActivity(),
                                "station",
                                mMusicStationId,
                                mMusicStationName
                        ) // pass the context to the constructor
                task.execute()
            }
        }, 1000)
    }

    /**
     * Play playlist station
     */
    private var dialog: Dialog? = null

    @SuppressLint("StaticFieldLeak")
    inner class CreatePlayListDialog(
            var ctx: Context,
            var mType: String,
            var mMusicStationId: String,
            var mMusicStationNAme: String
    ) : AsyncTask<Void?, Void?, Void?>() {
        override fun onPreExecute() {

            val resId = resources.getIdentifier(
                    R.raw.radio_tuning_sm.toString(),
                    "raw", activity?.packageName
            )

            val mediaPlayer = MediaPlayer.create(activity, resId)
            mediaPlayer.start()

            if (dialog == null) {
                dialog = ProgressDialog(ctx)
            }
            dialog?.show()
            super.onPreExecute()
        }

        override fun onPostExecute(aVoid: Void?) {
            if (dialog != null && dialog?.isShowing!!) {
                if (mType == "radio") {
                    if (stationPlayList?.size ?: 0 > 0) {
                        mediaItems.clear()
                        val mRadioItem = stationPlayList?.get(0)

                        val trackList: MutableList<TrackListCommon> = ArrayList()
                        val track = TrackListCommon()
                        track.numberId = "0"
                        track.playListName = mRadioItem?.genreName
                        val media = TrackListCommonMedia()
                        media.trackFile = mRadioItem?.stationData?.station_url
                        media.coverImage = mRadioItem?.stationData?.coverImage
                        track.media = media
                        trackList.add(track)
                        for (sample in trackList) {
                            val mediaItem = MediaItem(sample, true)
                            mediaItems.add(mediaItem)
                        }

                        playlistManager?.setParameters(mediaItems, 0)
                        playlistManager?.id = PLAYLIST_ID.toLong()
                        mPlayPosition = 0
                        launchMusic()
                        stationPlayList?.map {
                            it.isPlaying = it.id == mMusicStationId
                        }
                        mMusicStationsForYouAdapter?.swap(stationPlayList ?: ArrayList())
                        mMusicStationsForYouAdapter?.notifyDataSetChanged()
                        musicDashBoardViewModel.genreTrackListModel?.postValue(null)
                        isMusicStationCallCompleted = true
                    }
                } else if (mType == "station") {
                    val mAuth: String =
                            RemoteConstant.getAuthStationTrack(mMusicStationId, requireActivity())
                    musicDashBoardViewModel.getStationTrackList(mAuth, mMusicStationId)
                    musicDashBoardViewModel.genreTrackListModel?.nonNull()
                            ?.observe(viewLifecycleOwner, { genreTrackList ->
                                when (genreTrackList.status) {
                                    ResultResponse.Status.SUCCESS -> {
                                        val mGlobalGenreList =
                                                genreTrackList.data as MutableList<TrackListCommon>
                                        if (mGlobalGenreList.size > 0) {
                                            playTrackCommon(
                                                    mGlobalGenreList[0].numberId?.toInt()!!,
                                                    mGlobalGenreList,
                                                    "Music station - "+  mMusicStationNAme
                                            )
                                        }
                                        stationPlayList?.map {
                                            it.isPlaying = it.id == mMusicStationId
                                        }
                                        mMusicStationsForYouAdapter?.swap(
                                                stationPlayList ?: ArrayList()
                                        )
                                        mMusicStationsForYouAdapter?.notifyDataSetChanged()
                                        musicDashBoardViewModel.genreTrackListModel?.postValue(null)
                                        isMusicStationCallCompleted = true
                                    }
                                    ResultResponse.Status.ERROR -> {
                                        dialog?.dismiss()
                                    }
                                    ResultResponse.Status.NO_DATA -> {

                                        dialog?.dismiss()
                                    }
                                    ResultResponse.Status.LOADING -> {

                                    }
                                    else -> {

                                    }
                                }
                            })
                }

                dialog?.dismiss()
                dialog = null
            }
            super.onPostExecute(aVoid)
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            try {
                Thread.sleep(4000) // waits 4 seconds
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            return null
        }
    }


}
