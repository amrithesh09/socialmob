package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.view.ui.home_module.music.HomeViewPagerAdapter
import com.example.sample.socialmob.view.ui.home_module.music.TopMusicMenuFragment
import com.example.sample.socialmob.view.ui.home_module.music.musicvideo.MusicVideoFragment
import com.example.sample.socialmob.view.ui.home_module.music.radio.RadioPodCastFragment
import com.example.sample.socialmob.view.utils.events.SelectFragmentEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.music_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@AndroidEntryPoint
class MusicFragment : Fragment() {

    private val fragmentTopMenu = TopMusicMenuFragment()
    private var adapter: HomeViewPagerAdapter? = null


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    private fun addPagerFragments() {
        adapter?.addFragments(MusicDashBoardFragment())
        adapter?.addFragments(RadioPodCastFragment())
//        adapter?.addFragments(RadioMainFragment())
        adapter?.addFragments(MusicVideoFragment())
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.music_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fragmentManager: FragmentManager = childFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.content_frame_music_header, fragmentTopMenu)
        fragmentTransaction.commit()

        adapter =
                HomeViewPagerAdapter(
                        fragmentManager
                )
        addPagerFragments()
        view_pager_music_fragment?.adapter = adapter
        view_pager_music_fragment?.offscreenPageLimit = 3

        // TODO : Setting Top Menu
        content_frame_music_header?.bringToFront()
        chooseDashBoard()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SelectFragmentEvent) {
        when {
            event.gemSelectFragment() == getString(R.string.dashboard) -> //fragmentTransation(fragmentDashBoard)
                chooseDashBoard()
            event.gemSelectFragment() == getString(R.string.music_video) -> {//fragmentTransation(fragmentRadio)
                chooserRadio()
            }
            event.gemSelectFragment() == getString(R.string.genre) -> //fragmentTransation(fragmentGenre)
                chooseRadio()

        }
    }

    private fun chooseDashBoard() {
        view_pager_music_fragment?.currentItem = 0
    }

    private fun chooserRadio() {
        view_pager_music_fragment?.currentItem = 1
    }

    private fun chooseRadio() {
        view_pager_music_fragment?.currentItem = 2
    }

    fun updatePlayPause(mTrackId: String) {
        if (adapter != null) {
            val fr: Fragment = adapter?.getItem(0)!!
            if (fr is MusicDashBoardFragment) {
                fr.updatePlayPause(mTrackId)
            }
            val fr1: Fragment = adapter?.getItem(1)!!
            if (fr1 is RadioPodCastFragment) {
                fr1.updatePlayPause(mTrackId)
            }
        }
    }

    fun updatePlayList(mutableList: MutableList<PlaylistCommon>) {
        if (adapter != null) {
            val fr: Fragment = adapter?.getItem(0)!!
            if (fr is MusicDashBoardFragment) {
                fr.updatePlayList(mutableList)
            }
        }
    }

    fun removeStationAnimation() {
        try {
            if (adapter != null) {
                val fr: Fragment = adapter?.getItem(0)!!
                if (fr is MusicDashBoardFragment) {
                    fr.removeStationAnimation()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "removeStation" + e.toString())
        }
    }
}
