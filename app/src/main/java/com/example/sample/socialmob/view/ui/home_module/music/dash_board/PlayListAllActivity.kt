package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PlayListAllActivityBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.PlaylistDataIcon
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.GridSpacingItemDecoration
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.PlayListViewModel
import com.suke.widget.SwitchButton
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

@AndroidEntryPoint
class PlayListAllActivity : BaseCommonActivity(),
    PlayListAllRecyclerListAdapter.ItemClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private val playListViewModel: PlayListViewModel by viewModels()
    private var playListAllActivityBinding: PlayListAllActivityBinding? = null
    private var isApiCalled = false
    private var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
    private var playListImageView: ImageView? = null
    private var mLastId: String = ""
    private var mAdapter: PlayListAllRecyclerListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        playListAllActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.play_list_all_activity)

        playListAllActivityBinding?.viewModel = playListViewModel

        playListAllActivityBinding?.executePendingBindings()

        observePlayList()
        playListApi()

        mAdapter = PlayListAllRecyclerListAdapter(
            mContext = this, mItemClickListener = this, glideRequestManager
        )
        playListAllActivityBinding?.playListRecyclerView?.adapter = mAdapter



        playListAllActivityBinding?.allPlayListBackImageView?.setOnClickListener {
            finish()
        }
        playListAllActivityBinding?.informationImageview?.setOnClickListener {
            addInformationDialog()
        }
        playListAllActivityBinding?.addPlayListFab?.setOnClickListener {
            mLastId = ""
            createPlayListDialog()
        }


        playListAllActivityBinding?.playListRecyclerView?.layoutManager = GridLayoutManager(this, 2)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.penny_margin_start)
        playListAllActivityBinding?.playListRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(2, spacingInPixels, true)
        )

    }

    private fun playListApi() {
        val mAuth =
            RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        playListViewModel.getPlayList(mAuth)
    }

    private fun observePlayList() {

        playListViewModel.playListModel.nonNull().observe(this, { resultResponse ->
            when (resultResponse.status) {
                ResultResponse.Status.SUCCESS -> {
                    playListAllActivityBinding?.playListRecyclerView?.visibility = View.VISIBLE
                    playListAllActivityBinding?.loadingLinear?.visibility = View.GONE
                    playListAllActivityBinding?.noInternetLinear?.visibility = View.GONE

                    mPlayList = resultResponse.data
                    mAdapter?.submitList(mPlayList)

                    EventBus.getDefault().postSticky( UpdatePlaylist(mPlayList!!))
                }
                ResultResponse.Status.ERROR -> {

                    if (mPlayList?.size == 0) {
                        playListAllActivityBinding?.loadingLinear?.visibility = View.GONE
                        playListAllActivityBinding?.noInternetLinear?.visibility = View.VISIBLE
                        playListAllActivityBinding?.playListRecyclerView?.visibility = View.GONE

                        glideRequestManager.load(R.drawable.server_error).apply(
                            RequestOptions().fitCenter())
                            .into(playListAllActivityBinding?.noInternetImageView!!)
                        playListAllActivityBinding?.noInternetTextView?.text =
                            getString(R.string.server_error)
                        playListAllActivityBinding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)
                    }
                }
                ResultResponse.Status.LOADING -> {
                    playListAllActivityBinding?.loadingLinear?.visibility = View.VISIBLE
                    playListAllActivityBinding?.noInternetLinear?.visibility = View.GONE
                    playListAllActivityBinding?.playListRecyclerView?.visibility = View.GONE
                }
                else -> {

                }
            }


        })
    }


    private fun createPlayListDialog() {
        var isPrivate = "0"
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)
        playListImageView = dialogView.findViewById(R.id.play_list_image_view)

        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)
        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()


        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }



        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                        && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        createPlayListApi(
                            "0",
                            playListNameEditText.text.toString(),
                            isPrivate,
                            mLastId
                        )
                        AppUtils.hideKeyboard(this, createPlayListTextView)
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }
                } else
                    AppUtils.showCommonToast(this, "Playlist name too short")
            } else
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
            //To close alert
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }

                    if (playListImageView != null) {
                        glideRequestManager.load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                            .into(playListImageView!!)
                    }
                }

            }
        }

    }

    private fun addInformationDialog() {

        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_information_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_information)

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }

    private fun createPlayListApi(
        playListId: String,
        playListName: String,
        private: String,
        icon_id: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody = CreatePlayListBody(playListId, playListName, icon_id, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(this)
            playListViewModel.createPlayList(mAuth, createPlayListBody)
            isApiCalled = true
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }


    fun onOptionItemClickOption(
        mMethod: String,
        mId: String,
        mPlayListName: String,
        private: String,
        icon: PlaylistDataIcon
    ) {
        when (mMethod) {

            "deletePlayList" -> deletePlayList(mId)
            "editPlayList" -> editPlayList(mId, mPlayListName, private, icon)
        }
    }

    private fun editPlayList(
        mId: String,
        mPlayListName: String,
        private: String,
        icon: PlaylistDataIcon
    ) {
        var iconFileUrl = ""
        if (icon.id != null) {
            iconFileUrl = icon.fileUrl!!
        }
        if (icon.id != null) {
            mLastId = icon.id
        }

        var isPrivate: String = private
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)
        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListNameEditText.setText(mPlayListName)
        createPlayListTextView.isAllCaps = true
        createPlayListTextView.text = getString(R.string.save)


        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)

        switchButtonPlayList.isChecked = private != "0"


        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()


        if (icon.fileUrl != null) {
            glideRequestManager.load(icon.fileUrl).into(playListImageView!!)
        }

        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            intent.putExtra("iconId", mLastId)
            intent.putExtra("iconFileUrl", iconFileUrl)
            startActivityForResult(intent, 1001)
        }

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        createPlayListApi(
                            mId,
                            playListNameEditText.text.toString(),
                            isPrivate,
                            mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }
                } else
                    AppUtils.showCommonToast(this, "Playlist name too short")
            } else
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
            //To close alert
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    private fun deletePlayList(mId: String) {
        val mAuth: String = RemoteConstant.getAuthDeletePlayList(this, mId)
        playListViewModel.deletePlayList(mAuth, mId)
    }

    override fun onItemClickOption(mId: String, mPlayListName: String, mImageUrl: String) {
        val intent = Intent(this, CommonPlayListActivity::class.java)
        intent.putExtra("mId", mId)
        intent.putExtra("title", mPlayListName)
        intent.putExtra("imageUrl", mImageUrl)
        intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        playListViewModel.playListModel.removeObservers(this)
        playListAllActivityBinding?.unbind()
    }

    fun optionDialog(
        playlistId: String?,
        playlistName: String?,
        private: String,
        icon: PlaylistDataIcon
    ) {
        val viewGroup: ViewGroup? = null
        mLastId = ""
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.play_list_options, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val editLinear: LinearLayout = sheetView.findViewById(R.id.edit_linear)
        val deleteLinear: LinearLayout = sheetView.findViewById(R.id.delete_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        if (private == "1") {
            shareLinear.visibility = View.GONE
        }
        editLinear.setOnClickListener {
            mBottomSheetDialog.dismiss()
            onOptionItemClickOption(
                "editPlayList",
                playlistId!!, playlistName!!, private, icon
            )
        }
        deleteLinear.setOnClickListener {
            val viewGroupNew: ViewGroup? = null
            val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
            val inflater: LayoutInflater = layoutInflater
            val dialogView: View = inflater.inflate(R.layout.common_dialog, viewGroupNew)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
            val okButton: Button = dialogView.findViewById(R.id.ok_button)

            val b: AlertDialog = dialogBuilder.create()
            b.show()
            okButton.setOnClickListener {

                mBottomSheetDialog.dismiss()
                onOptionItemClickOption(
                    "deletePlayList",
                    playlistId!!, "", private, PlaylistDataIcon()
                )
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
            cancelButton.setOnClickListener {
                mBottomSheetDialog.dismiss()
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }
        shareLinear.setOnClickListener {
            mBottomSheetDialog.dismiss()
            ShareAppUtils.sharePlayList(playlistId, this)

        }

    }


}
