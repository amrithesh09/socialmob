package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.view.utils.ItemTouchHelperViewHolder


class PlayListAllRecyclerListAdapter(
    private val mContext: Context,
    private val mItemClickListener: ItemClickListener,
    private var glideRequestManager: RequestManager
) : ListAdapter<PlaylistCommon, PlayListAllRecyclerListAdapter.PlayListViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PlaylistCommon>() {
                override fun areItemsTheSame(
                    oldItem: PlaylistCommon,
                    newItem: PlaylistCommon
                ): Boolean {
                    return oldItem.id == newItem.id

                }

                override fun areContentsTheSame(
                    oldItem: PlaylistCommon,
                    newItem: PlaylistCommon
                ): Boolean {
                    return oldItem.equals(newItem)
                }

            }
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayListViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.play_list_all_item_main, parent, false)
        return PlayListViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: PlayListViewHolder, position: Int) {
        val mPlayList: PlaylistCommon? = getItem(holder.adapterPosition)
        holder.textView.text = mPlayList?.playlistName


        if (mPlayList?.isPrivate.equals("1"))
            holder.privateImageView.visibility = View.VISIBLE
        else
            holder.privateImageView.visibility = View.GONE

        if (mPlayList?.playlistType == "system-generated" || mPlayList?.playlistType == "admin-generated") {
            glideRequestManager
                .load(mPlayList.media?.coverFile)
                .apply(RequestOptions.placeholderOf(0).error(R.drawable.default_list_icon))
                .into(holder.playListImageView)
        } else {
            glideRequestManager
                .load(mPlayList?.icon?.fileUrl)
                .apply(RequestOptions.placeholderOf(0).error(R.drawable.default_list_icon))
                .into(holder.playListImageView)
        }

        holder.itemView.setOnClickListener {

            var mImageUrl = ""

            if (mPlayList?.playlistType == "system-generated" || mPlayList?.playlistType == "admin-generated") {
                if (mPlayList.media?.coverFile != null &&
                    mPlayList.media.coverFile != ""
                ) {
                    mImageUrl = mPlayList.media.coverFile!!
                }
            } else {
                if (mPlayList?.icon?.fileUrl != null &&
                    mPlayList.icon.fileUrl != ""
                ) {
                    mImageUrl = mPlayList.icon.fileUrl
                }
            }

            mItemClickListener.onItemClickOption(
                mPlayList?.id!!, mPlayList.playlistName!!,
                mImageUrl
            )
        }


        if (mPlayList?.playlistType == "system-generated" || mPlayList?.playlistType == "admin-generated") {
            holder.playListOptionImageView.visibility = View.INVISIBLE
        }

        if (mPlayList?.playlistName == "Listen Later") {
            glideRequestManager
                .load(R.drawable.listen_later_new_one)
                .into(holder.playListImageView)
            holder.playListOptionImageView.visibility = View.GONE
            holder.privateImageView.visibility = View.GONE
        }


//        // TODO : option dialog
        holder.playListOptionImageView.setOnClickListener {
            if (mPlayList?.id != null &&
                mPlayList.playlistName != null &&
                mPlayList.isPrivate != null &&
                mPlayList.icon != null
            ) {
                (mContext as PlayListAllActivity).optionDialog(
                    mPlayList.id,
                    mPlayList.playlistName,
                    mPlayList.isPrivate,
                    mPlayList.icon
                )
            }
        }

    }

    /**
     * Simple example of a view holder that implements [ItemTouchHelperViewHolder] and has a
     * "handle" view that initiates a drag event when touched.
     */
    class PlayListViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        val textView: TextView = itemView.findViewById<View>(R.id.text) as TextView
        val playListOptionImageView: ImageView =
            itemView.findViewById<View>(R.id.play_list_option_image_view) as ImageView
        val playListImageView: ImageView =
            itemView.findViewById<View>(R.id.play_list_image_view) as ImageView
        val privateImageView: ImageView =
            itemView.findViewById<View>(R.id.private_image_view) as ImageView


    }

    interface ItemClickListener {
        fun onItemClickOption(mId: String, mPlayListName: String, mImageUrl: String)
    }
}
