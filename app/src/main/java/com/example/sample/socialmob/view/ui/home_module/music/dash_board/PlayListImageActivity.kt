package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PlayListImageActivityBinding
import com.example.sample.socialmob.model.music.IconDatum
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.viewmodel.music_menu.PlayListViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.doAsync
import javax.inject.Inject

@AndroidEntryPoint
class PlayListImageActivity : BaseCommonActivity() {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: PlayListImageActivityBinding? = null
    private val playListViewModel: PlayListViewModel by viewModels()
    private var mLastId = ""
    private var mFileUrl = ""
    private var iconId = ""
    private var iconFileUrl = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.play_list_image_activity)

        val i: Intent? = intent
        if (i?.hasExtra("iconId")!!) {
            iconId = i.getStringExtra("iconId")!!
        }
        if (i.hasExtra("iconFileUrl")) {
            iconFileUrl = i.getStringExtra("iconFileUrl")!!
        }



        binding?.tabLayout?.setupWithViewPager(binding?.viewpagerMain)
        callApiPlayListImages()

        binding?.refreshTextView?.setOnClickListener {
            callApiPlayListImages()
        }
        binding?.backImageView?.setOnClickListener {
            finish()
        }

        binding?.saveButton?.setOnClickListener {
            val mIntent = Intent()
            mIntent.putExtra("mFileUrl", mFileUrl)
            if (mLastId != "") {
                mIntent.putExtra("mLastId", mLastId)
            }
            setResult(1001, mIntent)
            onBackPressed()
        }
    }

    private fun callApiPlayListImages() {
        if (!InternetUtil.isInternetOn()) {
            playListViewModel.iconData?.postValue(ResultResponse.noInternet(ArrayList()))
        } else {
            playListViewModel.callApiPlayListImages()
        }

        playListViewModel.iconData?.nonNull()?.observe(this, Observer { iconData ->

            if (iconData != null && iconData.status == ResultResponse.Status.SUCCESS) {


                val mList: MutableList<IconDatum> = iconData.data as MutableList<IconDatum>
                val fragmentAdapter = ImageListAdapter(supportFragmentManager)
                for (i in 0 until mList.size) {

//                    val mIconData: List<IconDatumIcon> = mList[i].icons!!
                    val mName: String = mList[i].categoryName!!

                    fragmentAdapter.addFragment(
                        PlayListImageFragment().newInstance(mName, iconId, iconFileUrl), mName
                    )
                    if (iconId != "" && iconFileUrl != "") {
                        update(iconId, iconFileUrl)
                    }
                }
                binding?.viewpagerMain?.adapter = fragmentAdapter

                for (i in 0 until binding?.tabLayout?.tabCount!! - 1) {
                    val tab: View = (binding?.tabLayout?.getChildAt(0) as ViewGroup).getChildAt(i)
                    val p = tab.layoutParams as ViewGroup.MarginLayoutParams
                    p.setMargins(15, 0, 15, 0)
                    tab.requestLayout()
                }
                binding?.progressLinear?.visibility = View.GONE
                binding?.viewpagerMain?.visibility = View.VISIBLE
                binding?.noInternetLinear?.visibility = View.GONE
                binding?.saveButton?.visibility = View.VISIBLE


            } else if (iconData != null && iconData.status == ResultResponse.Status.ERROR) {
                binding?.saveButton?.visibility = View.GONE
                binding?.progressLinear?.visibility = View.GONE
                binding?.viewpagerMain?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.VISIBLE
                glideRequestManager.load(R.drawable.server_error).apply(
                    RequestOptions().fitCenter())
                    .into(binding?.noInternetImageView!!)
                binding?.noInternetTextView?.text = getString(R.string.server_error)
                binding?.noInternetSecTextView?.text = getString(R.string.server_error_content)

            } else if (iconData != null && iconData.status == ResultResponse.Status.LOADING) {
                binding?.progressLinear?.visibility = View.VISIBLE
                binding?.viewpagerMain?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
                binding?.saveButton?.visibility = View.GONE

            } else if (iconData != null && iconData.status == ResultResponse.Status.NO_INTERNET) {
                glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                    .into(binding?.noInternetImageView!!)
                binding?.saveButton?.visibility = View.GONE
                binding?.progressLinear?.visibility = View.GONE
                binding?.viewpagerMain?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.VISIBLE
            }


        })
    }

    fun update(id: String?, fileUrl: String?) {
        doAsync {
            if (mLastId == "") {
                playListViewModel.dbPlayListImagesDao?.setSelected(id!!, true)
            } else {
                playListViewModel.dbPlayListImagesDao?.setSelected(mLastId, false)
                playListViewModel.dbPlayListImagesDao?.setSelected(id!!, true)
            }

            mLastId = id!!
            mFileUrl = fileUrl!!
        }

    }

    fun selected() {
        binding?.saveButton?.setBackgroundColor(Color.parseColor("#1e90ff"))
        binding?.saveButton?.text = "This looks kickass!"
    }

    class ImageListAdapter(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {
        private val fragmentList: MutableList<androidx.fragment.app.Fragment> =
            java.util.ArrayList()
        private val fragmentTitleList: MutableList<String> = java.util.ArrayList()

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentTitleList[position]
        }

        fun addFragment(fragment: androidx.fragment.app.Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }
}
