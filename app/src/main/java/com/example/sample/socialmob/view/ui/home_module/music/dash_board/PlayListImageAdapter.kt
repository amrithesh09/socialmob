package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PlayListImageBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.music.IconDatumIcon

class PlayListImageAdapter(
    private var mContext: Context?,
    private var mIconData: List<IconDatumIcon>?
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<PlayListImageBinding>(
            LayoutInflater.from(parent.context),
            R.layout.play_list_image,
            parent,
            false
        )
        return RecommendedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mIconData?.get(holder.adapterPosition))

    }

    override fun getItemCount(): Int {
        return mIconData?.size!!
    }


    inner class RecommendedViewHolder(val binding: PlayListImageBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(iconDatumIcon: Any?) {

            binding.imageViewmodel = iconDatumIcon as IconDatumIcon
            binding.executePendingBindings()


            binding.playListImageView.setOnClickListener {
                (mContext as PlayListImageActivity).update(
                    iconDatumIcon._id.toString(),
                    iconDatumIcon.file_url
                )
            }
            if (iconDatumIcon.is_selected == true) {
                (mContext as PlayListImageActivity).selected()
                binding.selectPlayListImageView.visibility = View.VISIBLE
            } else {
                binding.selectPlayListImageView.visibility = View.GONE
            }

        }
    }


}
