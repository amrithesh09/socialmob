package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PlayListImageFragmentBinding
import com.example.sample.socialmob.model.music.IconDatumIcon
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.GridSpacingItemDecoration
import com.example.sample.socialmob.viewmodel.music_menu.PlayListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlayListImageFragment : Fragment() {

    private var binding: PlayListImageFragmentBinding? = null
    private val playListViewModel: PlayListViewModel by viewModels()
    private var iconId: String = ""
    private var iconFileUrl: String = ""
    fun newInstance(mName: String, iconId: String, iconFileUrl: String): PlayListImageFragment {
        val args = Bundle()
        args.putString("mName", mName)
        args.putString("iconId", iconId)
        args.putString("iconFileUrl", iconFileUrl)
        val fragment = PlayListImageFragment()
        fragment.arguments = args
        return fragment

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.play_list_image_fragment, container, false
        )
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.playListImageRecyclerView?.layoutManager = GridLayoutManager(activity, 3)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.penny_margin_start)
        binding?.playListImageRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(3, spacingInPixels, true)
        )

        val args: Bundle? = arguments
        val mName: String? = args?.getString("mName")!!
        if (args.containsKey("iconId")) {
            iconId = args.getString("iconId")!!
        }
        if (args.containsKey("iconFileUrl")) {
            iconFileUrl = args.getString("iconFileUrl")!!
        }


        val mPlayListImages: LiveData<List<IconDatumIcon>>? = playListViewModel.getIconData(mName!!)
        mPlayListImages?.nonNull()?.observe(viewLifecycleOwner, {
            if (it != null && it.isNotEmpty()) {
                binding?.playListImageRecyclerView?.adapter =
                    PlayListImageAdapter(activity, it)
            }
        })
    }
}
