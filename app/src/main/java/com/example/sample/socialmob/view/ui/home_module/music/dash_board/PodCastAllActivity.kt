package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PodcastAllActivityBinding
import com.example.sample.socialmob.model.music.Podcast
import com.example.sample.socialmob.repository.utils.*
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.options_menu_podcast_sort.view.*

@AndroidEntryPoint
class PodCastAllActivity : BaseCommonActivity(),
    RadioPodCastAdapter.QuickPlayListAdapterItemClickListener {

    private var activityBinding: PodcastAllActivityBinding? = null
    private val viewModel: MusicDashBoardViewModel by viewModels()
    private var mPodCastAllRecyclerListAdapter: PodCastAllRecyclerListAdapter? = null
    private var mPodcastList: MutableList<Podcast>? = ArrayList()
    private var isAddedLoader: Boolean = false
    private var mSortBy: String = "latest"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBinding = DataBindingUtil.setContentView(this, R.layout.podcast_all_activity)

        callApiCommon()
        activityBinding?.titleTextView?.text = getString(R.string.all_podcast)

        activityBinding?.allPlayListBackImageView?.setOnClickListener {
            onBackPressed()
        }
        activityBinding?.playListRecyclerView?.addOnScrollListener(CustomScrollListener())

        mPodCastAllRecyclerListAdapter = PodCastAllRecyclerListAdapter(this)
        activityBinding?.playListRecyclerView?.adapter = mPodCastAllRecyclerListAdapter

        observePodCast()


        // TODO : Sort Podcast
        activityBinding?.sortByTextView?.setOnClickListener {
            activityBinding?.sortByImageView?.performClick()
        }
        activityBinding?.sortByImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View =
                LayoutInflater.from(this).inflate(R.layout.options_menu_podcast_sort, viewGroup)
            val mQQPop = PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(activityBinding?.view)

            view.name_a_z_text_view.setOnClickListener {
                activityBinding?.playListRecyclerView?.smoothScrollToPosition(0)
                mSortBy = "name-a-z"
                mPodcastList?.clear()
                mPodCastAllRecyclerListAdapter?.swap(mPodcastList ?: ArrayList())
                callApiCommon()
                mQQPop.dismiss()
                activityBinding?.sortByTextView?.text = getString(R.string.name_a_z)
            }
            view.name_z_a_text_view.setOnClickListener {
                activityBinding?.playListRecyclerView?.smoothScrollToPosition(0)
                mSortBy = "name-z-a"
                mPodcastList?.clear()
                mPodCastAllRecyclerListAdapter?.swap(mPodcastList ?: ArrayList())
                callApiCommon()
                mQQPop.dismiss()
                activityBinding?.sortByTextView?.text = getString(R.string.name_z_a)
            }
            view.latest_text_view.setOnClickListener {
                activityBinding?.playListRecyclerView?.smoothScrollToPosition(0)
                mSortBy = "latest"
                mPodcastList?.clear()
                mPodCastAllRecyclerListAdapter?.swap(mPodcastList ?: ArrayList())
                callApiCommon()
                mQQPop.dismiss()
                activityBinding?.sortByTextView?.text = getString(R.string.latest)
            }
        }
    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            AppUtils.showCommonToast(this, "No Internet connection")
            activityBinding?.playListRecyclerView?.visibility = View.GONE
            activityBinding?.noInternetLinear?.visibility = View.VISIBLE
            activityBinding?.podcastProgress?.visibility = View.GONE

        } else {
            activityBinding?.playListRecyclerView?.visibility = View.GONE
            activityBinding?.noInternetLinear?.visibility = View.GONE
            activityBinding?.podcastProgress?.visibility = View.VISIBLE
            callApi(RemoteConstant.mOffset, RemoteConstant.mCount)
        }
    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager?.childCount
            val totalItemCount = recyclerView.layoutManager?.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            if (dy > 0) {
                if (totalItemCount != null && visibleItemCount != null) {
                    if (!viewModel.isLoadingPodcast && !viewModel.isLoadCompletedPodcast && !isAddedLoader) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                            callApi(totalItemCount.toString(), RemoteConstant.mCount)

                            val mutableList: MutableList<Podcast> = ArrayList()
                            val mList = Podcast(
                                0, "", "", "", "", ""
                            )
                            mutableList.add(mList)
                            mPodcastList?.addAll(mutableList)
                            mPodCastAllRecyclerListAdapter?.swap(mPodcastList ?: ArrayList())
                            isAddedLoader = true
                        }
                    }
                }
            }
        }
    }

    private fun observePodCast() {
        viewModel.podCastModelLiveData?.nonNull()?.observe(this) { resultResponse ->
            if (resultResponse != null) {
                val mPodCastListLocal = resultResponse.data as MutableList<Podcast>
                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        activityBinding?.playListRecyclerView?.visibility = View.VISIBLE
                        activityBinding?.noInternetLinear?.visibility = View.GONE
                        activityBinding?.podcastProgress?.visibility = View.GONE
                        mPodcastList?.addAll(mPodCastListLocal)
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            isAddedLoader = false
                            mPodcastList?.removeAt(mPodcastList?.size?.minus(1) ?: 0)
                        }
                        mPodcastList?.addAll(mPodCastListLocal)
                    }
                    ResultResponse.Status.NO_DATA -> {
                        activityBinding?.playListRecyclerView?.visibility = View.GONE
                        activityBinding?.noInternetLinear?.visibility = View.GONE
                        activityBinding?.podcastProgress?.visibility = View.GONE
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        if (isAddedLoader) {
                            isAddedLoader = false
                            mPodcastList?.removeAt(mPodcastList?.size?.minus(1) ?: 0)
                        }
                    }
                    else -> {

                    }
                }
                mPodCastAllRecyclerListAdapter?.swap(mPodcastList!!)
            }
        }
    }

    private fun callApi(mOffset: String, mCount: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathPodCast +
                    "offset=" + mOffset + "&count=" + mCount + "&sortBy=" + mSortBy,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        viewModel.getPodCastApi(mAuth, mOffset, mCount, mSortBy)
    }

    override fun onPlayListItemClick(item: String, mTitle: String, imageUrl: String, mDescription: String) {
        val intent = Intent(this, CommonPlayListActivity::class.java)
        intent.putExtra("mId", item)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", imageUrl)
        intent.putExtra("mDescription", mDescription)
        intent.putExtra("mIsFrom", "fromRadio")
        startActivity(intent)
    }
}
