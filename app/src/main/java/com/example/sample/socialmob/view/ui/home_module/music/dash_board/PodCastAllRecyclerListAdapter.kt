package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PodcastAllItemMainBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.music.Podcast
import com.example.sample.socialmob.view.utils.BaseViewHolder


class PodCastAllRecyclerListAdapter(
    private val mCallBack: RadioPodCastAdapter.QuickPlayListAdapterItemClickListener
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private val podCastList: MutableList<Podcast> = ArrayList()

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun getItemCount(): Int {
        return podCastList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: PodcastAllItemMainBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.podcast_all_item_main, parent, false
        )
        return PodCastInnerViewHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ProgressItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            podCastList[position]._id != "" -> ITEM_DATA
            podCastList[position]._id == "" -> ITEM_LOADING
            else -> ITEM_DATA
        }
    }


    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    override fun onBindViewHolder(viewHolder: BaseViewHolder<Any>, postion: Int) {
        viewHolder.bind(podCastList.get(postion))
    }

    inner class PodCastInnerViewHolder(val binding: PodcastAllItemMainBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            binding.podcastModel = podCastList.get(adapterPosition)
            binding.executePendingBindings()

            binding.root.setOnClickListener {
                var mDescription = ""
                if (podCastList[adapterPosition].description != null && podCastList[adapterPosition].description != "") {
                    mDescription = podCastList[adapterPosition].description ?: ""
                }
                mCallBack.onPlayListItemClick(
                    podCastList[adapterPosition]._id ?: "",
                    podCastList[adapterPosition].podcast_name ?: "",
                    podCastList[adapterPosition].cover_image ?: "",
                    mDescription
                )
            }
        }
    }

    fun swap(mutableList: MutableList<Podcast>) {
        val diffCallback =
            PodCastDiffAdapter(
                this.podCastList,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.podCastList.clear()
        this.podCastList.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    private class PodCastDiffAdapter(
        private val oldList: MutableList<Podcast>, private val newList: MutableList<Podcast>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition]._id == newList[newItemPosition]._id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].podcast_name == newList[newItemPosition].podcast_name
        }
    }
}
