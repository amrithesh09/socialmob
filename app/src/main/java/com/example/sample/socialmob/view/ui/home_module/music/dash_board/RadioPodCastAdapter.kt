package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.QuickPlayListItemBinding
import com.example.sample.socialmob.model.music.FeaturedPodcast
import com.example.sample.socialmob.view.utils.BaseViewHolder


class RadioPodCastAdapter(
     mContext: Activity?,
    private val mCallBack: QuickPlayListAdapterItemClickListener,
    private val mPodCastAllClick: PodCastAllClick
) : ListAdapter<FeaturedPodcast, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<FeaturedPodcast>() {
                override fun areItemsTheSame(
                    oldItem: FeaturedPodcast,
                    newItem: FeaturedPodcast
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: FeaturedPodcast,
                    newItem: FeaturedPodcast
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }


    private val ALL_VIEW_TYPE = 0
    private val PODCAST_VIEW_TYPE = 1
    private val mInflater: LayoutInflater = LayoutInflater.from(mContext)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        when (viewType) {
            ALL_VIEW_TYPE -> {
                return ViewAllHolder(mInflater.inflate(R.layout.podcast_item_view_all, parent, false))
            }
            PODCAST_VIEW_TYPE -> {
                val binding: QuickPlayListItemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.quick_play_list_item, parent, false
                )
                return QuickPlayListViewHolder(binding)
            }
        }
        val binding: QuickPlayListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.quick_play_list_item, parent, false
        )
        return QuickPlayListViewHolder(binding)

    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))

    }

    inner class ViewAllHolder(itemView: View) : BaseViewHolder<Any>(itemView) {

        private val mItemViewAll: LinearLayout = itemView.findViewById(R.id.view_all_layout)
        override fun bind(genre: Any) {

            mItemViewAll.setOnClickListener {
                mPodCastAllClick.onPodCastAllItemClick()
            }
        }

    }

    inner class QuickPlayListViewHolder(val binding: QuickPlayListItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.podcastModel = getItem(adapterPosition)
            binding.executePendingBindings()

            binding.root.setOnClickListener {
                var mDescription = ""
                if (getItem(adapterPosition)?.description != null && getItem(adapterPosition).description != "") {
                    mDescription = getItem(adapterPosition).description!!
                }
                mCallBack.onPlayListItemClick(
                    getItem(adapterPosition)?.id!!, getItem(adapterPosition).podcastName!!
                    , getItem(adapterPosition).coverImage!!, mDescription
                )
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.podcastName.equals("")) {
            ALL_VIEW_TYPE
        } else
            PODCAST_VIEW_TYPE

    }

    interface QuickPlayListAdapterItemClickListener {
        fun onPlayListItemClick(
            item: String, mTitle: String, imageUrl: String,
            mDescription: String
        )
    }

    interface PodCastAllClick {
        fun onPodCastAllItemClick()
    }
}