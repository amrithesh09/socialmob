package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AdmobItemNativeAdBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.RecentPlayTrackItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.google.android.ads.nativetemplates.NativeTemplateStyle
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.admob_item_native_ad.view.*
import java.util.*


class RecentPlayListAdapter(
    private val itemClickListener: RecentPlayListItemClickListener,
    private val mContext: Context?,
    private val mIsFrom: String,
    private var mListAdUitId: MutableList<String>?,
    private var requestManager: RequestManager
) :
    ListAdapter<TrackListCommon, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_AD = 3


        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<TrackListCommon>() {
                override fun areItemsTheSame(
                    oldItem: TrackListCommon,
                    newItem: TrackListCommon
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: TrackListCommon,
                    newItem: TrackListCommon
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_AD -> bindAd(parent)
            else -> bindData(parent)
        }
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {

//        val binding: AdmobItemBannerBinding = DataBindingUtil.inflate(
//            LayoutInflater.from(parent.context),
//            R.layout.admob_item_banner, parent, false
//        )
        val binding: AdmobItemNativeAdBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.admob_item_native_ad, parent, false
        )
        return AdViewHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: RecentPlayTrackItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recent_play_track_item, parent, false
        )
        return RecommendedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            getItem(position)?.id == "-1" -> ITEM_LOADING
            getItem(position)?.id == "ad" -> ITEM_AD
            getItem(position)?.id != "" -> ITEM_DATA
            else -> ITEM_DATA
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class AdViewHolder(val binding: AdmobItemNativeAdBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
//            val adRequest = AdRequest.Builder().addTestDevice("74CB4C4575947CE06704C6972734F731")
//                .build()
//            binding.root.adViewTrack.loadAd(adRequest)

            try {
                if (mContext != null && binding.root.my_template != null) {
                    val min = 0
                    val max = mListAdUitId?.size

                    val r = Random();
                    val i1 = r.nextInt(max!! - min + 1) + min

                    val adLoader = AdLoader.Builder(mContext, mListAdUitId?.get(i1))
                        .forUnifiedNativeAd { unifiedNativeAd ->
                            val styles =
                                NativeTemplateStyle.Builder().build()
                            val template = binding.root.my_template
                            template.setStyles(styles)
                            template.setNativeAd(unifiedNativeAd)
                        }
                        .build()
                    adLoader.loadAd(AdRequest.Builder().addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0").build())
                }
            } catch (e: Exception) {

            }

        }

    }

    inner class RecommendedViewHolder(val binding: RecentPlayTrackItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(mTrack: Any?) {

            if (mTrack != null) {
                binding?.trackViewModel = mTrack as TrackListCommon
                binding?.executePendingBindings()
            }


            if (getItem(adapterPosition)?.isPlaying!!) {
                binding?.queueListItemPlayImageView?.visibility = View.VISIBLE
                binding?.topTrackGifProgress?.visibility = View.VISIBLE
                binding?.topTrackGifProgress?.playAnimation()
            } else {
                binding?.queueListItemPlayImageView?.visibility = View.INVISIBLE
                binding?.topTrackGifProgress?.visibility = View.INVISIBLE
                binding?.topTrackGifProgress?.pauseAnimation()
            }


            binding?.trackOptions?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    (mContext as RecentlyPlayedActivity).musicOptionsDialog(
                        getItem(adapterPosition)
                    )
                }
            }
            binding?.topLinear?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (getItem(adapterPosition) != null &&
                        getItem(adapterPosition)?.media != null &&
                        getItem(adapterPosition)?.media?.coverFile != null
                    ) {
                        itemClickListener.onRecentItemClick(
                            adapterPosition,
                            getItem(adapterPosition)?.media?.coverFile ?: "", mIsFrom
                        )
                    }
                }
            }

            requestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        getItem(adapterPosition).media?.coverFile,
                        RemoteConstant.getWidth(mContext) / 2
                    )
                )
                .apply(
                    RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                            Priority.HIGH
                        )
                )
                .thumbnail(0.1f)
                .into(binding?.recommendedImageView!!)
        }
    }

    interface RecentPlayListItemClickListener {
        fun onRecentItemClick(itemId: Int, itemImage: String, isFrom: String)
    }


}