package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Intent
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.RecentlyPlayedActivtyBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.RecentlyPlayedViewModel
import com.google.android.gms.ads.RequestConfiguration
import com.google.firebase.analytics.FirebaseAnalytics
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class RecentlyPlayedActivity : BaseCommonActivity(),
    RecentPlayListAdapter.RecentPlayListItemClickListener,
    PlaylistListener<MediaItem> {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: RecentlyPlayedActivtyBinding? = null
    private val viewModel: RecentlyPlayedViewModel by viewModels()
    private var isAddedLoader: Boolean = false
    private var mAdapter: RecentPlayListAdapter? = null
    private var mStatus: ResultResponse.Status? = null
    private var playListImageView: ImageView? = null
    private var mLastId: String = ""
    private var mLastClickTime: Long = 0
    private val mediaItems = LinkedList<MediaItem>()
    private var playlistManager: PlaylistManager? = null

    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"
    private var mTrackIdGlobal: String = ""
    private var mAdCount: Int = 0
    private var mPlayPositionTrack = -1
    private val GROUP_ID = "listGroup".hashCode()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    companion object {
        var singlePlayListTrack: MutableList<TrackListCommon>? = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.recently_played_activty)

        val bundle: Bundle? = intent?.extras!!
        val isFrom = bundle?.getString("isFrom")!!
        RequestConfiguration.Builder().setTestDeviceIds(listOf("64A0685293CC7F3B6BC3ECA446376DA0"))
        binding?.recentlyPlayedBackImageView?.setOnClickListener {
            onBackPressed()
        }

        if (isFrom == "recently_played_songs") {
            // Call all operation  related to network or other ui blocking operations here.
            callApiRecentlyPlayed()
            binding?.titleTextView?.text = getString(R.string.recently_played_)
            observeTrack()

        } else {
            // Call all operation  related to network or other ui blocking operations here.
            callApiCommonLikedSong()
            binding?.titleTextView?.text = getString(R.string.liked_songs_)
            observeTrack()

            // TODO : Pagination
            binding?.photoGridRecyclerView?.addOnScrollListener(CustomScrollListener())
        }

        binding?.refreshTextView?.setOnClickListener {
            callApiRecentlyPlayed()
        }

        binding?.photoGridRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


        val mListAdUitId: MutableList<String> = ArrayList()
        mListAdUitId.add(getString(R.string.ad_unit_id))
        mListAdUitId.add(getString(R.string.ad_unit_id_2))

        mAdapter = if (isFrom == "recently_played_songs") {
            RecentPlayListAdapter(
                this, this,
                "dbRecentlyPlayedSongs", mListAdUitId, requestManager = glideRequestManager
            )
        } else {
            RecentPlayListAdapter(
                this, this, "dbLikedsongs",
                mListAdUitId, requestManager = glideRequestManager
            )
        }

        binding?.photoGridRecyclerView?.hasFixedSize()
        binding?.photoGridRecyclerView?.adapter = mAdapter

        // TODO : Download offlie track pending
        if (viewModel.downloadFalseTrackListModel != null) {
            viewModel.downloadFalseTrackListModel?.nonNull()
                ?.observe(this, { trackListCommonDbs ->
                    if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                        this.trackListCommonDbs = trackListCommonDbs
                    }
                })
        }
        val fetchConfiguration = FetchConfiguration.Builder(this)
            .setDownloadConcurrentLimit(4)
            .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
            .setNamespace(FETCH_NAMESPACE)
            .build()
        fetch = Fetch.getInstance(fetchConfiguration)

        binding?.gifProgress?.setOnClickListener { view: View? -> binding?.fab?.performClick() }
        binding?.fab?.setOnClickListener {
                if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    var mImageUrlPass = ""
                    if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                        Intent(this@RecentlyPlayedActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                } else if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null
                ) {
                    var mImageUrlPass = ""
                    if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                        Intent(this@RecentlyPlayedActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                }
        }
    }


    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int
        ) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()
                if (singlePlayListTrack?.size!! >= 15) {
                    if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                        mStatus != ResultResponse.Status.LOADING &&
                        visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                    ) {

                        val mNewListSize = singlePlayListTrack?.size?.minus(mAdCount)
                        callApiLikedSongs(
                            mNewListSize.toString(),
                            RemoteConstant.mCount
                        )

                        val mutableList: MutableList<TrackListCommon> = ArrayList()
                        val mList = TrackListCommon()
                        mutableList.add(mList)
                        mList.id = "-1"
                        singlePlayListTrack?.addAll(mutableList)
                        mAdapter?.notifyDataSetChanged()
                        isAddedLoader = true
                    }
                }
            }
        }
    }

    private fun callApiCommonLikedSong() {
        if (!InternetUtil.isInternetOn()) {
            binding?.progressLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.GONE
            binding?.photoGridRecyclerView?.visibility = View.GONE
            binding?.noDataLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.VISIBLE
            val shake: android.view.animation.Animation =
                AnimationUtils.loadAnimation(this, R.anim.shake)
            binding!!.noInternetLinear.startAnimation(shake) // starts animation
        } else {
            binding?.progressLinear?.visibility = View.VISIBLE
            binding?.noInternetLinear?.visibility = View.GONE
            binding?.photoGridRecyclerView?.visibility = View.GONE
            binding?.noDataLinear?.visibility = View.GONE
            callApiLikedSongs(
                RemoteConstant.mOffset,
                RemoteConstant.mCount
            )
        }
    }

    private fun callApiLikedSongs(mOffset: String, mCount: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.trackData +
                    RemoteConstant.pathFavoriteList + "?offset=" + mOffset + "&count=" + mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModel.getFavouriteTracksApi(
            mAuth, mOffset, mCount
        )
    }


    private fun callApiRecentlyPlayed() {
        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            binding?.progressLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.GONE
            binding?.photoGridRecyclerView?.visibility = View.GONE
            binding?.noDataLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.VISIBLE
            val shake: android.view.animation.Animation =
                AnimationUtils.loadAnimation(this, R.anim.shake)
            binding!!.noInternetLinear.startAnimation(shake) // starts animation
        } else {
            binding?.progressLinear?.visibility = View.VISIBLE
            binding?.noInternetLinear?.visibility = View.GONE
            binding?.photoGridRecyclerView?.visibility = View.GONE
            binding?.noDataLinear?.visibility = View.GONE
            callApi()
        }
    }

    private fun callApi() {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathRentlyPlayed +
                    RemoteConstant.mOffset + RemoteConstant.mSlash + RemoteConstant.mCount50,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModel.getRecentPlayTracks(mAuth, RemoteConstant.mOffset)

    }

    private fun removeLoaderFormList() {

        if (singlePlayListTrack != null && singlePlayListTrack?.size!! > 0 && singlePlayListTrack?.get(
                singlePlayListTrack?.size!! - 1
            ) != null && singlePlayListTrack?.get(singlePlayListTrack?.size!! - 1)?.id == "-1"
        ) {
            singlePlayListTrack?.removeAt(singlePlayListTrack?.size!! - 1)
            mAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }

    private fun observeTrack() {
        viewModel.recentPlayedTracks?.nonNull()?.observe(this, { trackList ->
            mStatus = trackList.status

            when (trackList.status) {
                ResultResponse.Status.SUCCESS -> {
                    val singlePlayListTrackLocal = trackList?.data!!.toMutableList()
                    val mTrackNewList: MutableList<TrackListCommon> = ArrayList()
                    for (i in singlePlayListTrackLocal.indices) {
                        if (i % 14 == 0) {
                            if (i != 0) {
                                val mList = TrackListCommon()
                                mList.id = "ad"
                                mTrackNewList.add(mList)
                                // TODO: Google Ad count
                                mAdCount = mAdCount.plus(1)

                            }
                            mTrackNewList.add(singlePlayListTrackLocal[i])
                        } else {
                            mTrackNewList.add(singlePlayListTrackLocal[i])
                        }
                    }

                    singlePlayListTrack?.addAll(mTrackNewList)
                    mAdapter?.submitList(singlePlayListTrack)

                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                    binding?.photoGridRecyclerView?.visibility = View.VISIBLE

                    updatePlayPause()

                    mStatus = ResultResponse.Status.LOADING_COMPLETED

                }
                ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                    if (isAddedLoader) {
                        removeLoaderFormList()
                    }
                }
                ResultResponse.Status.PAGINATED_LIST -> {

                    if (isAddedLoader) {
                        removeLoaderFormList()
                    }

                    val mPaginatedTackApiList: MutableList<TrackListCommon>? =
                        trackList?.data?.toMutableList()
                    val mTrackNewList: MutableList<TrackListCommon> = ArrayList()


                    for (i in mPaginatedTackApiList?.indices!!) {
                        if (i % 14 == 0) {
                            if (i != 0) {
                                val mList = TrackListCommon()
                                mList.id = "ad"
                                mTrackNewList.add(mList)
                                // TODO: Google Ad count
                                mAdCount = mAdCount.plus(1)

                            }
                            mTrackNewList.add(mPaginatedTackApiList[i])
                        } else {
                            mTrackNewList.add(mPaginatedTackApiList[i])
                        }
                    }

                    singlePlayListTrack?.addAll(mTrackNewList)
                    mAdapter?.notifyDataSetChanged()

                    updatePlayPause()
                    mStatus = ResultResponse.Status.LOADING_COMPLETED
                }
                ResultResponse.Status.ERROR -> {

                    if (singlePlayListTrack?.size == 0) {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.VISIBLE
                        binding?.photoGridRecyclerView?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        glideRequestManager.load(R.drawable.server_error).apply(
                            RequestOptions().fitCenter())
                            .into(binding?.noInternetImageView!!)
                        binding?.noInternetTextView?.text = getString(R.string.server_error)
                        binding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)
                    }
                }
                ResultResponse.Status.NO_DATA -> {
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.photoGridRecyclerView?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.VISIBLE
                }
                ResultResponse.Status.LOADING -> {
                    binding?.progressLinear?.visibility = View.VISIBLE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.photoGridRecyclerView?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE

                }
                else -> {

                }
            }
        })
    }


    override fun onRecentItemClick(itemId: Int, itemImage: String, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (singlePlayListTrack != null && singlePlayListTrack?.size!! > 0 && mPlayPositionTrack != -1) {
                singlePlayListTrack?.get(mPlayPositionTrack)?.isPlaying = false
                mAdapter?.notifyItemChanged(mPlayPositionTrack)
            }
            singlePlayListTrack?.map {
                if (it.numberId == singlePlayListTrack?.get(itemId)?.numberId) {
                    it.isPlaying = true
                    mPlayPositionTrack = singlePlayListTrack?.indexOf(it)!!
                } else {
                    it.isPlaying = false
                }
            }
            if (mPlayPositionTrack != -1) {
                mAdapter?.notifyItemChanged(mPlayPositionTrack)
            }

            if (singlePlayListTrack?.isNotEmpty()!!) {
                mediaItems.clear()
                for (sample in singlePlayListTrack!!) {
                    val mediaItem = MediaItem(sample, true)
                    mediaItems.add(mediaItem)
                }
            }
            launchMusic(itemId)

        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }


    private fun launchMusic(itemId: Int) {
        playlistManager?.setParameters(mediaItems, itemId)
        playlistManager?.id = 4.toLong()
        playlistManager?.currentPosition = itemId
        playlistManager?.play(0, false)
    }


    private fun updatePlayPause() {
        if (playlistManager != null
            && playlistManager?.playlistHandler != null
            && playlistManager?.playlistHandler?.currentMediaPlayer != null
            && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
            && playlistManager?.currentItem != null
            && playlistManager?.currentItem?.id != null
        ) {
            if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                val mTrackId =
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id?.toString()

                singlePlayListTrack?.map {
                    if (it.numberId == mTrackId) {
                        mPlayPositionTrack = singlePlayListTrack?.indexOf(it)!!
                    }
                }
                updatePLaying(mTrackId)
            } else {
                updatePause()
            }
        }
    }

    private fun updatePause() {
        singlePlayListTrack?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
            }
        }
        mAdapter?.notifyDataSetChanged()
    }

    private fun updatePLaying(mTrackId: String?) {
        mTrackIdGlobal = mTrackId!!
        singlePlayListTrack?.map {
            if (it.numberId == mTrackId) {
                it.isPlaying = true
                println("" + true)
            } else {
                it.isPlaying = false
                println("" + false)
            }
        }
        mAdapter?.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)
        if (playlistManager != null && playlistManager?.playlistHandler != null &&
            playlistManager?.playlistHandler?.currentMediaPlayer != null &&
            playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
        ) {
            binding?.fab?.visibility = View.GONE
            binding?.gifProgress?.visibility = View.VISIBLE
            binding?.gifProgress?.playAnimation()
            binding?.gifProgress?.bringToFront()
            updatePlayPause()
        } else {
            binding?.gifProgress?.visibility = View.GONE
            binding?.fab?.visibility = View.VISIBLE
            binding?.gifProgress?.pauseAnimation()
            updatePause()
        }
    }


    fun musicOptionsDialog(mTrackDetails: TrackListCommon) {
        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
            sheetView.findViewById(R.id.add_play_list_image_view)
        val equalizerImageView: ImageView = sheetView.findViewById(R.id.equalizer_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
            sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)
        val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE

        val allowOfflineDownload: String? = mTrackDetails.allowOfflineDownload
        if (allowOfflineDownload == "1") {
            downloadLinear.visibility = View.VISIBLE
        } else {
            downloadLinear.visibility = View.GONE
        }


        downloadLinear.setOnClickListener {

            firebaseAnalytics = FirebaseAnalytics.getInstance(this)


            val paramsNotification = Bundle()
            paramsNotification.putString(
                "user_id",
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
            )
            paramsNotification.putString("track_id", mTrackDetails.id)
            paramsNotification.putString("event_type", "track_download")
            firebaseAnalytics.logEvent("offline_track_download", paramsNotification)


            viewModel.addToDownload(mTrackDetails)
            mBottomSheetDialog.dismiss()
            Toast.makeText(this, "Track added to queue", Toast.LENGTH_SHORT).show()


            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                enqueueDownloads()
            }, 1000)

        }


        val mTrackId: String? = mTrackDetails.id
        var isLiked: String? = mTrackDetails.favorite
        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }


        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                viewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "false"
                    }

                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                viewModel.removeFavourites(mAuth, mTrackId)

            }

        }


        val mAuth =
            RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        viewModel.getPlayList(mAuth)

        viewModel.playListModel.nonNull().observe(this, {
            if (it.isNotEmpty()) {
                mPlayList = it as MutableList
                playListRecyclerView.adapter =
                    CommonPlayListAdapter(
                        mPlayList, this, false,
                        glideRequestManager
                    )

                // TODO : Update playlist ( Home page )
                EventBus.getDefault().postSticky( UpdatePlaylist(mPlayList!!))
            }
        })
        albumLinear.setOnClickListener {
            val intent = Intent(this, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackDetails.album?.id)
            intent.putExtra("title", mTrackDetails.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivity(intent)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackDetails.artist?.id)
            detailIntent.putExtra("mArtistName", mTrackDetails.artist?.artistName)
            startActivity(detailIntent)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                context = this,
                recyclerView = playListRecyclerView,
                mListener = object : RecyclerItemClickListener.ClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //TODO : Call Add to playlist API
                        if (mPlayList != null && mPlayList!![position].id != null) {
                            addTrackToPlayList(mTrackDetails.id!!, mPlayList!![position].id!!)
                        }
                        mBottomSheetDialog.dismiss()
                    }

                    override fun onLongItemClick(view: View?, position: Int) {

                    }


                })
        )

        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(
                mTrackDetails.id!!,
                this
            )
            mBottomSheetDialog.dismiss()
        }
    }

    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                    mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        viewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)

        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                        && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                            "0", playListNameEditText.text.toString(), isPrivate,
                            mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }

                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }

    }

    private fun createPlayListApi(
        playListId: String, playListName: String, private: String, mLastId: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody =
                CreatePlayListBody(playListId, playListName, mLastId, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            viewModel.createPlayListApi(mAuth, createPlayListBody)

        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }
                    if (playListImageView != null) {
                        glideRequestManager
                            .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                            .into(playListImageView!!)
                    }
                }
            }
        }
    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        if (mTrackIdGlobal != currentItem?.id.toString() || mTrackIdGlobal == currentItem?.id.toString()) {
            updatePLaying(currentItem?.id.toString())
            mTrackIdGlobal = currentItem?.id.toString()
        }
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.PLAYING -> {
                val itemChange = playlistManager?.currentItemChange
                if (itemChange != null) {
                    onPlaylistItemChanged(
                        itemChange.currentItem,
                        itemChange.hasNext,
                        itemChange.hasPrevious
                    )
                }
                binding?.fab?.visibility = View.GONE
                binding?.gifProgress?.visibility = View.VISIBLE
                binding?.gifProgress?.playAnimation()
                binding?.gifProgress?.bringToFront()
            }
            PlaybackState.ERROR -> {
            }
            PlaybackState.PAUSED -> {
                binding?.gifProgress?.visibility = View.GONE
                binding?.fab?.visibility = View.VISIBLE
                binding?.gifProgress?.pauseAnimation()
                updatePause()
            }
            PlaybackState.RETRIEVING -> {

            }
            PlaybackState.PREPARING -> {

            }
            PlaybackState.SEEKING -> {

            }
            PlaybackState.STOPPED -> {

            }
        }
        return true
    }


    override fun onDestroy() {
        super.onDestroy()
        singlePlayListTrack?.clear()
        viewModel.recentPlayedTracks?.postValue(null)
        viewModel.recentPlayedTracks?.removeObservers(this)
        binding?.photoGridRecyclerView?.adapter = null
    }

    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size!! > 0) {
            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests != null && requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }
    }
}
