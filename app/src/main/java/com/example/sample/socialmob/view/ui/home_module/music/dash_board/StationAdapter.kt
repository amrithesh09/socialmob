package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.StationListItemBinding
import com.example.sample.socialmob.model.music.MusicStation
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.AppUtils

class StationAdapter(
    private val mContext: Context?,
    private var mMusicStationClickListener: MusicStationClickListener,
    private var requestManager: RequestManager?
) :
    RecyclerView.Adapter<StationAdapter.ViewHolder>() {
    private var stationList: MutableList<MusicStation>? = ArrayList()
    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding: StationListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.station_list_item, parent, false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mStationItem = stationList?.get(holder.adapterPosition)
        holder.binding.musicViewModel = mStationItem
        holder.binding.executePendingBindings()

        requestManager?.load(
            RemoteConstant.getImageUrlFromWidth(
                mStationItem?.stationData?.coverImage, 100
            )
        )
            ?.thumbnail(0.1f)
            ?.into(holder.binding.playListImageView)

        holder.itemView.setOnClickListener {
            AppUtils.showCommonToast(mContext!!, "Long press to play the station")
        }
        holder.itemView.setOnLongClickListener {
            if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                mMusicStationClickListener.onMusicStationClickListener(
                    mStationItem?.id ?: "",
                    mStationItem?.genreName ?: "",
                    mStationItem?.stationData?.coverImage ?: "",
                    holder.adapterPosition,mStationItem?.genreName?:""
                )
            }
            return@setOnLongClickListener true
        }

        if (mStationItem?.isPlaying == true) {
            holder.binding.queueListItemPlayImageView.visibility = View.VISIBLE
            holder.binding.topTrackGifProgress.visibility = View.VISIBLE
            holder.binding.topTrackGifProgress.playAnimation()
        } else {
            holder.binding.queueListItemPlayImageView.visibility = View.INVISIBLE
            holder.binding.topTrackGifProgress.visibility = View.INVISIBLE
            if (holder.binding.topTrackGifProgress.isAnimating) {
                holder.binding.topTrackGifProgress.pauseAnimation()
            }
        }
    }

    override fun getItemCount(): Int {
        return stationList?.size!!
    }

    class ViewHolder(val binding: StationListItemBinding) : RecyclerView.ViewHolder(binding.root)


    interface MusicStationClickListener {
        fun onMusicStationClickListener(
            mMusicStationId: String,
            mTitle: String,
            imageUrl: String,
            adapterPosition: Int,
            mMusicStationName: String
        )
    }

    fun swap(mutableList: MutableList<MusicStation>) {
        val diffCallback =
            StationListDiffUtils(
                this.stationList!!,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.stationList?.clear()
        this.stationList?.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }
}

private class StationListDiffUtils(
    private val oldList: MutableList<MusicStation>, private val newList: MutableList<MusicStation>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].isPlaying == newList[newItemPosition].isPlaying
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return  oldList[oldItemPosition].isPlaying == newList[newItemPosition].isPlaying
    }
}