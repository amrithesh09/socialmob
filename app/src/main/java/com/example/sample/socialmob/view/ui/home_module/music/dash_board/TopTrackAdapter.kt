package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.TopTrackArtistItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.MyApplication


class TopTrackAdapter(
    private val topTrackItemClickListener: TopTrackItemClickListener,
    private val mContext: Activity?,
    private val isFrom: String?,
    private var requestManager: RequestManager
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var playlistManager = MyApplication.playlistManager
    private val mutableList = mutableListOf<TrackListCommon>()
    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        val binding: TopTrackArtistItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(mContext), R.layout.top_track_artist_item, parent, false
        )
        return TopTrackViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mutableList[holder.adapterPosition])
    }

    inner class TopTrackViewHolder(val binding: TopTrackArtistItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.trackViewModel = mutableList[adapterPosition]
            binding.executePendingBindings()

            requestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        mutableList[adapterPosition].media?.coverFile,
                        RemoteConstant.getWidth(mContext) / 2
                    )
                )
                .apply(
                    RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                            Priority.HIGH
                        )
                )
                .thumbnail(0.1f)
                .into(binding.topTrackImageView)

            // TODO : Playing Gif
            when {
                mutableList[adapterPosition].isPlaying == true -> {
                    binding.queueListItemPlayImageView.visibility = View.VISIBLE
                    binding.topTrackGifProgress.visibility = View.VISIBLE
                    binding.topTrackGifProgress.post {
                        binding.topTrackGifProgress.playAnimation()
                    }
                }
                mutableList[adapterPosition].numberId == playlistManager?.currentItemChange?.currentItem?.id?.toString() ?: 0 &&
                        mutableList[adapterPosition].isPlaying == false -> {

                    binding.queueListItemPlayImageView.visibility = View.VISIBLE
                    binding.topTrackGifProgress.visibility = View.VISIBLE
                    binding.topTrackGifProgress.post {
                        binding.topTrackGifProgress.pauseAnimation()
                    }
                }
                else -> {
                    binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                    binding.topTrackGifProgress.visibility = View.INVISIBLE
                    binding.topTrackGifProgress.post {
                        binding.topTrackGifProgress.pauseAnimation()
                    }
                }
            }

            binding.topTrackLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (isFrom == "TopTrack") {
                        topTrackItemClickListener.onTopTrackItemClick(
                            mutableList[adapterPosition].numberId?.toInt()!!,
                            mutableList[adapterPosition].media?.coverFile!!, "TopTrack"
                        )
                    } else {
                        topTrackItemClickListener.onTopTrackItemClick(
                            mutableList[adapterPosition].numberId?.toInt()!!,
                            mutableList[adapterPosition].media?.coverFile!!,
                            isFrom ?: "EditorsChoice"
                        )
                    }
                }
            }
            binding.topTrackOptionsImageView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    (mContext as HomeActivity).musicOptionsDialog(mutableList[adapterPosition])
                }
            }

        }
    }

    interface TopTrackItemClickListener {
        fun onTopTrackItemClick(itemId: Int, itemImage: String, isFrom: String)
    }

    override fun getItemCount(): Int {

        return if (isFrom == "EditorsChoice") {
            if (mutableList.size >= 5) {
                5
            } else {
                mutableList.size
            }
        } else {
            mutableList.size

        }
    }

    fun swap(list: List<TrackListCommon>) {
        val diffCallback = TrackDiffCallback(this.mutableList, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.mutableList.clear()
        this.mutableList.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    private class TrackDiffCallback(
        private val oldList: List<TrackListCommon>,
        private val newList: List<TrackListCommon>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
        }
    }


}
