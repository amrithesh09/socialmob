package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.TopTrackArtistItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.view.utils.BaseViewHolder

class TopTrackArtistAdapter(
    private val topTrackItemClickListener: TopTrackItemClickListener,
    private val mMusicOptionsDialog: MusicOptionsDialog?
) :
    ListAdapter<TrackListCommon, BaseViewHolder<Any>>(DIFF_CALLBACK) {


    companion object {
        const val ITEM_DATA = 1

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<TrackListCommon>() {
                override fun areItemsTheSame(
                    oldItem: TrackListCommon,
                    newItem: TrackListCommon
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: TrackListCommon,
                    newItem: TrackListCommon
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        val binding: TopTrackArtistItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.top_track_artist_item, parent, false
        )
        return TopTrackViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(position))
    }

    inner class TopTrackViewHolder(val binding: TopTrackArtistItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.trackViewModel = getItem(adapterPosition)
            binding.executePendingBindings()


            if (getItem(adapterPosition)?.isPlaying!!) {
                binding.queueListItemPlayImageView.visibility = View.VISIBLE
                binding.topTrackGifProgress.visibility = View.VISIBLE
                binding.topTrackGifProgress.playAnimation()
            } else {
                binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                binding.topTrackGifProgress.visibility = View.INVISIBLE
                if (binding.topTrackGifProgress.isAnimating) {
                    binding.topTrackGifProgress.pauseAnimation()
                }
            }

            binding.mainCardView.setOnClickListener {
                topTrackItemClickListener.onTopTrackItemClick(
                    getItem(adapterPosition).numberId!!
                    , getItem(adapterPosition).media?.coverFile!!, "TopTrack"
                )
            }
            binding.topTrackOptionsImageView.setOnClickListener {
                mMusicOptionsDialog?.musicOptionsDialog(getItem(adapterPosition))
            }
        }

    }

    interface TopTrackItemClickListener {
        fun onTopTrackItemClick(itemId: String, itemImage: String, isFrom: String)
    }

    interface AddToPlayListClickListener {
        fun onAddToPlayListItemClick(mTrackId: String)
    }

    interface MusicOptionsDialog {
        fun musicOptionsDialog(mTrackListCommon: TrackListCommon)
    }

}