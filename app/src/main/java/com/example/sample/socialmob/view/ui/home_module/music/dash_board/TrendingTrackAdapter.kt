package com.example.sample.socialmob.view.ui.home_module.music.dash_board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.TreadingTrackItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.MyApplication

class TrendingTrackAdapter(
    private val trackItemClickListener: TrendingItemClickListener,
    private val mContext: Context?,
    private val isNewTrack: Boolean?,
    private var requestManager: RequestManager
) : RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var playlistManager = MyApplication.playlistManager
    private val mutableList = mutableListOf<TrackListCommon>()
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        val binding: TreadingTrackItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.treading_track_item, parent, false
        )
        return TrendingTrackViewHolder(binding)

    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(mutableList[holder.adapterPosition])

    }

    inner class TrendingTrackViewHolder(val binding: TreadingTrackItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            binding.trackViewModel = mutableList[adapterPosition]
            binding.executePendingBindings()
            if (isNewTrack!!) {
                binding.newFlag.visibility = View.VISIBLE
            }

            when {
                mutableList[adapterPosition].isPlaying == true -> {
                    binding.queueListItemPlayImageView.visibility = View.VISIBLE
                    binding.trendingTrackGifProgress.visibility = View.VISIBLE
                    binding.trendingTrackGifProgress.post {
                        binding.trendingTrackGifProgress.playAnimation()
                    }
                }
                mutableList[adapterPosition].numberId == playlistManager?.currentItemChange?.currentItem?.id?.toString() ?: 0 &&
                        mutableList[adapterPosition].isPlaying == false -> {

                    binding.queueListItemPlayImageView.visibility = View.VISIBLE
                    binding.trendingTrackGifProgress.visibility = View.VISIBLE
                    binding.trendingTrackGifProgress.post {
                        binding.trendingTrackGifProgress.pauseAnimation()
                    }
                }
                else -> {
                    binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                    binding.trendingTrackGifProgress.visibility = View.INVISIBLE
                    binding.trendingTrackGifProgress.post {
                        binding.trendingTrackGifProgress.pauseAnimation()
                    }
                }
            }


            binding.trendingTrackLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (!isNewTrack) {
                        trackItemClickListener.onTrendingItemClick(
                            mutableList[adapterPosition].numberId?.toInt()!!,
                            mutableList[adapterPosition].media?.coverFile!!, "TrendingTrack"
                        )
                    } else {
                        trackItemClickListener.onTrendingItemClick(
                            mutableList[adapterPosition].numberId?.toInt()!!,
                            mutableList[adapterPosition].media?.coverFile!!, "NewTrack"
                        )
                    }
                }
            }
            binding.topTrackOptionsImageView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    (mContext as HomeActivity).musicOptionsDialog(mutableList[adapterPosition])
                }
            }

            requestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        mutableList[adapterPosition].media?.coverFile,
                        RemoteConstant.getWidth(mContext) / 2
                    )
                )
                .apply(
                    RequestOptions.placeholderOf(R.drawable.blur).error(R.drawable.blur)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                            Priority.HIGH
                        )
                )
                .thumbnail(0.1f)
                .into(binding.treandingTrackImageView)

        }
    }

    interface TrendingItemClickListener {
        fun onTrendingItemClick(itemId: Int, itemImage: String, isFrom: String)
    }

    override fun getItemCount(): Int = mutableList.size

    fun swap(list: List<TrackListCommon>) {
        val diffCallback = TrackDiffCallback(this.mutableList, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.mutableList.clear()
        this.mutableList.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    private class TrackDiffCallback(
        private val oldList: List<TrackListCommon>,
        private val newList: List<TrackListCommon>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
        }
    }
}


