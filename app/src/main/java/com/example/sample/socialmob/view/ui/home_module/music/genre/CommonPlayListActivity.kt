package com.example.sample.socialmob.view.ui.home_module.music.genre

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.text.Html
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.GenreListingActivityBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.BottomSheetPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.TrackCommentsActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.PlayListImageActivity
import com.example.sample.socialmob.view.ui.home_module.music.radio.GenrePlayListAdapter
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.music_menu.GenreListViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.android.material.appbar.AppBarLayout
import com.google.firebase.analytics.FirebaseAnalytics
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.options_menu_asc_desc.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class CommonPlayListActivity : BaseCommonActivity(),
    GenrePlayListAdapter.GenrePlayListItemClickListener,
    RadioPodCastEpisodeDetailsAdapter.RadioPodCastEpisodeItemClickListener,
    SinglePlayListAdapter.SinglePlayListItemClickListener,
    SinglePlayListAdapter.OptionItemClickListener,
    BottomSheetPlayListAdapter.PlayListItemClickListener,
    GenrePlayListAdapter.PlayListItemClickListener, SinglePlayListAdapter.PlayListItemClickListener,
    RadioPodCastEpisodeDetailsAdapter.OptionDialog, PlaylistListener<MediaItem> {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mChangeId = ""
    private var mPlayListId: String = ""
    private var isBindingExecuted: Boolean = false
    private var isPlayed: Boolean = false
    private var isLoaderAdded: Boolean = false
    private var isClickFromPlayAll: Boolean = false
    private var mLastClickTime: Long = 0
    private var screenWidth = 0

    private val genreListViewModel: GenreListViewModel by viewModels()
    private var binding: GenreListingActivityBinding? = null
    private var mGenreAdapter: GenrePlayListAdapter? = null
    private var mSinglePlayListAdapter: SinglePlayListAdapter? = null
    private var mPodCastAdapter: RadioPodCastEpisodeDetailsAdapter? = null


    private var radioPodcastList: MutableList<RadioPodCastEpisode>? = ArrayList()
    private var mStatus: ResultResponse.Status? = null


    private val mediaItems = LinkedList<MediaItem>()
    private var playlistManager: PlaylistManager? = null
    private var mPlaybackState: PlaybackState? = null

    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"

    private var mRewardedAd: RewardedAd? = null
    private var mIsLoading = false
    private var isRewardEarned = false
    private var mNightModeStatus: Boolean = false
    private val nativeAds: MutableList<UnifiedNativeAd> = ArrayList()

    companion object {
        var mGlobalGenreList: MutableList<TrackListCommon>? = ArrayList()
    }

    private var isLoaded = false
    private var mId = ""
    private var title = ""
    private var mIsFrom = ""
    private var mPlayListUser = ""
    private var mPlayListUserId = ""
    private var mAscDesc = "desc"

    private var playListImageView: ImageView? = null
    private var mLastId: String = ""
    private var mPlayPosition = -1
    private var PLAYLIST_ID = 4 //Arbitrary, for the example
    private var mAdCount: Int = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {

        delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_NO
        super.onCreate(savedInstanceState)
        // TODO : Get Bundle Value
        binding = DataBindingUtil.setContentView(this, R.layout.genre_listing_activity)
        mNightModeStatus =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)


        // TODO : Preload ads
        val mListAdUitId: MutableList<String> = ArrayList()
        mListAdUitId.add(getString(R.string.ad_unit_id))
        mListAdUitId.add(getString(R.string.ad_unit_id_2))

        for (i in 0 until mListAdUitId.size) {
            val adLoader = AdLoader.Builder(this, mListAdUitId[i])
                .forUnifiedNativeAd { unifiedNativeAd ->
                    nativeAds.add(unifiedNativeAd)
                }.withAdListener(AdListener())
                .build()

            adLoader.loadAd(
                AdRequest.Builder().addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0").build()
            )
        }
        binding?.toolbar?.setNavigationIcon(R.drawable.ic_back)
        binding?.toolbar?.setNavigationOnClickListener {
            binding?.genreListingBackImageView?.performClick()
        }

        // TODO: Slide changes
        binding?.appbar?.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (Math.abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                //  Collapsed
                if (mNightModeStatus) {
                    binding?.toolbar?.setNavigationIcon(R.drawable.ic_back)
                } else {
                    binding?.toolbar?.setNavigationIcon(R.drawable.ic_back_black)
                }

                binding?.swipeBackLayout?.isSwipeFromEdge = false
                when (mIsFrom) {
                    "fromRadio", "myPlayList", "forYouPlayList", "artistAlbum" -> {
                        binding?.collapsingToolbar?.title = title
                    }
                    "musicStation" -> {
                        binding?.collapsingToolbar?.title = title
                    }
                }

                binding?.playAllTextView?.visibility = View.GONE

            } else {
                binding?.toolbar?.setNavigationIcon(R.drawable.ic_back)
                //Expanded
                binding?.swipeBackLayout?.isSwipeFromEdge = true

                when (mIsFrom) {
                    "musicStation" -> {
                        binding?.collapsingToolbar?.title = ""
                    }
                    "fromRadio" -> {
                        binding?.collapsingToolbar?.title = ""
                        binding?.collapsingToolbar?.subtitle = ""
                    }
                    else -> {
                        binding?.collapsingToolbar?.title = title
                    }
                }
                binding?.playAllTextView?.visibility = View.VISIBLE
            }
        })

        //TODO : Ads
        MobileAds.initialize(this, getString(R.string.admob_app_id))
        loadRewardedAd()

        if (intent != null && intent.data != null) {

            // TODO : Share play list Track
            val data: Uri? = intent?.data
            if (data.toString().contains("playlist")) {
                if (data?.lastPathSegment != null) {
                    mId = data.lastPathSegment!!
                    mIsFrom = "fromMusicDashBoardPlayList"
                }
            } else {
                if (data?.lastPathSegment != null) {
                    mId = data.lastPathSegment!!
                    mIsFrom = "fromRadio"
                }
            }
        }

        if (intent?.extras != null) {
            val bundle: Bundle? = intent?.extras!!
            if (bundle?.containsKey("mId")!!) {
                mId = bundle.getString("mId")!!
            }
            if (bundle.containsKey("title")) {
                title = bundle.getString("title")!!
            }
            if (bundle.containsKey("mIsFrom")) {
                mIsFrom = bundle.getString("mIsFrom")!!
            }
            if (bundle.containsKey("mPlayListUser")) {
                mPlayListUser = bundle.getString("mPlayListUser")!!
                if (mPlayListUser != "") {
                    binding?.albumByTextView?.visibility = View.VISIBLE
                    binding?.albumByTextView?.text = "Playlist by "
                    binding?.podcastContent?.text = mPlayListUser
                    val size = resources.getDimensionPixelSize(R.dimen.five_dp)
                    binding?.podcastContent?.textSize = size.toFloat()
                    val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    params.setMargins(5, 0, 0, 0)
                    binding?.podcastContent?.layoutParams = params
                }

            }

            if (bundle.containsKey("mPlayListUserId")) {
                mPlayListUserId = bundle.getString("mPlayListUserId")!!
            }


            // TODO : Image for playlist item
            if (title != "") {
                when (title) {
                    "Listen Later" -> {
                        glideRequestManager
                            .load(R.drawable.listen_later_new_one)
                            .into(binding?.squareImageView!!)
                        glideRequestManager
                            .load(R.drawable.listen_later_new_one)
                            .into(binding?.genreImageView!!)
                    }
                }
            }
            if (bundle.containsKey("imageUrl")) {
                val imageUrl: String = bundle.getString("imageUrl")!!
                if (title != "Favourites" && title != "Listen Later") {
                    binding?.imageUrl = imageUrl
                }

            }
            if (bundle.containsKey("mDescription")) {
                val mDescription: String = bundle.getString("mDescription")!!
                val text =
                    Html.fromHtml("<font color=#a6b3c3>by </font> <font color=#1e90ff>" + mDescription + "</font>")
                binding?.description = text.toString()
            }
        }

        if (mIsFrom == "forYouPlayList") {
            binding?.genreName?.visibility = View.GONE
        } else {
            binding?.genreName?.text = title
            binding?.genreName?.bringToFront()
        }
        when (mIsFrom) {
            "Genre", "musicStation" -> {
                binding?.genreTitleTextView?.text = title.plus(getString(R.string.tracks))
                binding?.collapsingToolbar?.title = title.plus(getString(R.string.tracks))
                binding?.genreName?.visibility = View.GONE
                binding?.podcastContent?.visibility = View.GONE
                val params = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
                params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
                binding?.genreRelative?.layoutParams = params

                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }
            }
            "fromRadio" -> {
                binding?.collapsingToolbar?.title = ""
                binding?.collapsingToolbar?.subtitle = ""
                binding?.genreTitleTextView?.text = getString(R.string.all_episode)
                binding?.sortLinear?.visibility = View.VISIBLE
                val size = resources.getDimensionPixelSize(R.dimen.about_text_size)
                binding?.genreName?.textSize = size.toFloat()
                binding?.fabPodcastComments?.visibility = View.VISIBLE
                binding?.playAllTextView?.visibility = View.GONE
                binding?.artistNameLinear?.visibility = View.GONE

                binding?.fabPodcastComments?.setOnClickListener {


                    val detailIntent = Intent(this, TrackCommentsActivity::class.java)
                    detailIntent.putExtra(
                        RemoteConstant.mTrackId,
                        mId
                    )
                    detailIntent.putExtra(
                        RemoteConstant.mTrackName,
                        title
                    )
                    detailIntent.putExtra(RemoteConstant.mIsFrom, "fromPodcast")
                    if (playlistManager != null && playlistManager?.currentProgress != null
                        && playlistManager?.currentProgress?.position != null
                    ) {
                        detailIntent.putExtra(
                            RemoteConstant.mDuration,
                            playlistManager?.currentProgress?.position.toString()
                        )
                    }

                    startActivity(detailIntent)
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)

                }
                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                binding?.collapsingToolbar?.setExpandedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitle)
                binding?.collapsingToolbar?.setCollapsedSubtitleTextColor(R.style.CollapsedAppBarSubTitle)
                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }

            }
            "fromMusicDashBoardPlayList" -> {
                binding?.contentLinear?.visibility = View.GONE
                val size = resources.getDimensionPixelSize(R.dimen.about_text_size)
                binding?.genreName?.textSize = size.toFloat()

                binding?.genreName?.visibility = View.GONE
                binding?.contentLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.title = title
                binding?.sortLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                binding?.collapsingToolbar?.setExpandedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitle)
                binding?.collapsingToolbar?.setCollapsedSubtitleTextColor(R.style.CollapsedAppBarSubTitle)
                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }

            }
            "myPlayList" -> {
                binding?.contentLinear?.visibility = View.GONE
                val size = resources.getDimensionPixelSize(R.dimen.about_text_size)
                binding?.genreName?.textSize = size.toFloat()

                binding?.genreName?.visibility = View.GONE
                binding?.contentLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.title = title
                binding?.sortLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                binding?.collapsingToolbar?.setExpandedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitle)
                binding?.collapsingToolbar?.setCollapsedSubtitleTextColor(R.style.CollapsedAppBarSubTitle)
                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }
            }
            "forYouPlayList" -> {
                binding?.contentLinear?.visibility = View.GONE
                val size = resources.getDimensionPixelSize(R.dimen.about_text_size)
                binding?.genreName?.textSize = size.toFloat()

                binding?.genreName?.visibility = View.GONE
                binding?.contentLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.title = title
                binding?.sortLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                binding?.collapsingToolbar?.setExpandedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitle)
                binding?.collapsingToolbar?.setCollapsedSubtitleTextColor(R.style.CollapsedAppBarSubTitle)
                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }

            }
            "artistAlbum" -> {
                binding?.genreTitleTextView?.text = getString(R.string.all_tracks)
                binding?.genreTitleTextView?.visibility = View.GONE
                binding?.genreName?.visibility = View.GONE
                binding?.contentLinear?.visibility = View.GONE

                binding?.collapsingToolbar?.title = title
                binding?.sortLinear?.visibility = View.GONE
                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                binding?.collapsingToolbar?.setExpandedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitle)

                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                    binding?.collapsingToolbar?.setCollapsedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitleDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedSubtitleTextColor(R.style.CollapsedAppBarSubTitle)
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }
            }
            else -> {
                binding?.genreTitleTextView?.text = title
                binding?.collapsingToolbar?.title = title
                val size = resources.getDimensionPixelSize(R.dimen.about_text_size)
                binding?.genreName?.textSize = size.toFloat()

                binding?.description = title
                binding?.albumByTextView?.visibility = View.VISIBLE
                binding?.collapsingToolbar?.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
                binding?.collapsingToolbar?.setExpandedSubtitleTextAppearance(R.style.CollapsedAppBarSubTitle)
                binding?.collapsingToolbar?.setCollapsedSubtitleTextColor(R.style.CollapsedAppBarSubTitle)
                if (mNightModeStatus) {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarDark)
                } else {
                    binding?.collapsingToolbar?.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
                }
            }
        }

        mGlobalGenreList?.clear()
        radioPodcastList?.clear()

        //TODO : Observe data
        observeDataCommon()


        binding?.artistNameLinear?.setOnClickListener {
            if (genreListViewModel.mArtistId != "") {
                val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
                detailIntent.putExtra("mArtistId", genreListViewModel.mArtistId)
                detailIntent.putExtra("mArtistName", genreListViewModel.mArtistName)
                startActivity(detailIntent)
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
            }
        }
        binding?.podcastContent?.setOnClickListener {
            if (mPlayListUserId != "") {
                val intent = Intent(this, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mPlayListUserId)
                startActivity(intent)
            }
        }
        binding?.sharePlayList?.bringToFront()
        binding?.sharePlayList?.setOnClickListener {
            when (mIsFrom) {
                "fromRadio" -> {
                    ShareAppUtils.sharePodcast(mId, this)
                }
                else -> {
                    ShareAppUtils.sharePlayList(mId, this)
                }
            }
        }
        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }
        binding?.genreRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
            )


        when (mIsFrom) {
            "Genre" -> {
                binding?.genreRecyclerView?.isNestedScrollingEnabled = false
                binding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
                    val view: View =
                        binding?.nestedScrollView?.getChildAt(binding?.nestedScrollView?.childCount!! - 1)!!

                    val diff: Int =
                        view.bottom - (binding?.nestedScrollView?.height!! + binding?.nestedScrollView?.scrollY!!)

                    if (diff == 0) {
                        //your api call to fetch data
                        if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                            mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                            mGlobalGenreList?.size!! >= 15
                        ) {
                            val mNewListSize = mGlobalGenreList?.size?.minus(mAdCount)
                            loadMoreItemsGenre(mNewListSize!!)
                        }
                    }
                }
            }
            "fromRadio" -> {
                binding?.genreRecyclerView?.isNestedScrollingEnabled = false
                binding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
                    val view: View =
                        binding?.nestedScrollView?.getChildAt(binding?.nestedScrollView?.childCount!! - 1)!!

                    val diff: Int =
                        view.bottom - (binding?.nestedScrollView?.height!! + binding?.nestedScrollView?.scrollY!!)

                    if (diff == 0) {
                        //your api call to fetch data
                        if (!genreListViewModel.isLoadingradioPodcast && !genreListViewModel.isLoadCompletedradioPodcast &&
                            radioPodcastList?.size!! >= 15
                        ) {
                            loadMoreItemsRadioPodCast(radioPodcastList?.size!!)
                        } else {
                            if (genreListViewModel.isLoadCompletedradioPodcast) {
                                if (isLoaderAdded) {
                                    removeLoaderFormListPodcast()
                                }
                            }
                        }
                    }
                }
            }
            "fromMusicDashBoardPlayList" -> {

            }
        }

        binding?.genreListingBackImageView?.setOnClickListener {
            viewModelStore.clear()
            binding?.genreRecyclerView?.adapter = null
            onBackPressed()
        }
        binding?.playAllTextView?.setOnClickListener {
            binding?.layoutPlayAll?.performClick()
        }
        binding?.layoutPlayAll?.setOnClickListener {
            isClickFromPlayAll = true
            if (binding?.genreRecyclerView?.findViewHolderForAdapterPosition(0) != null) {
                binding?.genreRecyclerView?.findViewHolderForAdapterPosition(0)?.itemView?.performClick()
            }
        }
        binding?.layoutSuffleAll?.setOnClickListener {
            binding?.genreRecyclerView?.findViewHolderForAdapterPosition(
                Random().nextInt(
                    binding?.genreRecyclerView?.adapter!!.itemCount
                )
            )!!.itemView.performClick()

        }

        // TODO : Sort Podcast
        binding?.sortImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View =
                LayoutInflater.from(this).inflate(R.layout.options_menu_asc_desc, viewGroup)
            val mQQPop = PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(binding?.latestFirstDropDown)

            view.asc_text_view.setOnClickListener {
                mAscDesc = "desc"
                radioPodcastList!!.clear()
                callApiCommon()
                mQQPop.dismiss()
                binding?.sortImageView?.text = getString(R.string.latest_first)
            }
            view.desc_text_view.setOnClickListener {
                mAscDesc = "asc"
                radioPodcastList!!.clear()
                callApiCommon()
                mQQPop.dismiss()
                binding?.sortImageView?.text = getString(R.string.oldest_first)
            }
        }

        // TODO : Set album_relative_layout Height

        val params = binding?.albumRelativeLayout?.layoutParams
        // Changes the height and width to the specified *pixels*
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenWidth = displayMetrics.widthPixels
        params?.height = screenWidth
        binding?.albumRelativeLayout?.layoutParams = params


        // TODO : Download offlie track pending
        if (genreListViewModel.downloadFalseTrackListModel != null) {
            genreListViewModel.downloadFalseTrackListModel?.nonNull()
                ?.observe(this, { trackListCommonDbs ->
                    if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                        this.trackListCommonDbs = trackListCommonDbs
                    }
                })
        }

        if (InternetUtil.isInternetOn()) {
            val fetchConfiguration = FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(4)
                .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE)
//            .setNotificationManager(object : DefaultFetchNotificationManager(this) {
//                override fun getFetchInstanceForNamespace(namespace: String): Fetch {
//                    return fetch!!
//                }
//            })
                .build()
            fetch = Fetch.getInstance(fetchConfiguration)
        }
        binding?.gifProgress?.setOnClickListener { view: View? -> binding?.fab?.performClick() }
        binding?.fab?.setOnClickListener {
            if (playlistManager != null &&
                playlistManager?.playlistHandler != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
            ) {
                val detailIntent =
                    Intent(this@CommonPlayListActivity, NowPlayingActivity::class.java)
                detailIntent.putExtra(
                    RemoteConstant.extraIndexImage,
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl
                        ?: ""
                )
                startActivity(detailIntent)
            } else if (playlistManager != null &&
                playlistManager?.playlistHandler != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer != null
            ) {
                val detailIntent =
                    Intent(this@CommonPlayListActivity, NowPlayingActivity::class.java)
                detailIntent.putExtra(
                    RemoteConstant.extraIndexImage,
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl
                        ?: ""
                )
                startActivity(detailIntent)
            }
        }


    }


    private fun loadMoreItemsRadioPodCast(size: Int) {
        if (!genreListViewModel.isLoadingradioPodcast && !genreListViewModel.isLoadCompletedradioPodcast) {
            val mutableList: MutableList<RadioPodCastEpisode> = ArrayList()
            val mList = RadioPodCastEpisode(
                0, "", "", "", "", "",
                "", "", "", "", "",
                "", "", ""
            )

            mutableList.add(mList)
            radioPodcastList?.addAll(mutableList)
            mPodCastAdapter?.notifyItemInserted(radioPodcastList?.size!! - 1)
            isLoaderAdded = true
            podcastListingApi(mId, size.toString(), RemoteConstant.mCount, mAscDesc)
        }

    }


    private fun loadMoreItemsGenre(size: Int) {
        val mutableList: MutableList<TrackListCommon> = ArrayList()
        val mList = TrackListCommon()
        mList.id = "0"
        mutableList.add(mList)
        mGlobalGenreList?.addAll(mutableList)
        mGenreAdapter?.swap(mGlobalGenreList!!)
        mGenreAdapter?.notifyItemInserted(mGlobalGenreList?.size!! - 1)

        isLoaderAdded = true
        genreTrackListApi(mId, size.toString(), RemoteConstant.mCount)
    }

    private fun callApiCommon() {

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            checkInternet()
            binding?.isVisibleList = false
            binding?.isVisibleLoading = false
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = true
            val shake: android.view.animation.Animation =
                AnimationUtils.loadAnimation(this, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        } else {
            callApi(mIsFrom)
        }
    }

    private fun observeDataCommon() {
        if (binding?.genreRecyclerView?.adapter != null) {
            binding?.genreRecyclerView?.adapter = null
        }
        when (mIsFrom) {
            "Genre", "musicStation" -> {
                genreListViewModel.isVisibleItem = true
                observeGenreList()
            }
            "artistAlbum" -> {
                genreListViewModel.isVisibleItem = true
                observeGenreList()
            }
            "fromRadio" -> {

                genreListViewModel.isVisibleItem = false
                genreListViewModel.filterRadioPodCast(title)
                mPodCastAdapter = RadioPodCastEpisodeDetailsAdapter(
                    this, this, this@CommonPlayListActivity
                )
                binding?.genreRecyclerView?.adapter = mPodCastAdapter
                binding?.playAllTextView?.visibility = View.GONE
                observePodcastList()
            }
            "fromMusicDashBoardPlayList" -> {
                genreListViewModel.isVisibleItem = false

                observePlayList(false)
            }
            "myPlayList" -> {
                genreListViewModel.isVisibleItem = false
                observePlayList(true)
            }
            "forYouPlayList" -> {
                genreListViewModel.isVisibleItem = false
                observePlayList(false)
            }
            "fromPlayList" -> {
                genreListViewModel.isVisibleItem = false
                observePlayList(false)
            }
        }
    }

    private fun checkInternet() {
        InternetUtil.observe(this, { status ->
            if (status == true) {
                if (!isLoaded) {
                    isLoaded = true
                    callApi(mIsFrom)
                }
            } else {
                AppUtils.showCommonToast(this, "No Internet connection")
            }
        })
    }

    private fun callApi(mIsFrom: String) {
        when (mIsFrom) {
            "Genre" -> genreTrackListApi(mId, RemoteConstant.mOffset, RemoteConstant.mCount)
            "fromRadio" -> {
                podcastListingApi(mId, RemoteConstant.mOffset, RemoteConstant.mCount, mAscDesc)
            }
            "fromMusicDashBoardPlayList" -> musicPlayListApi(mId)
            "myPlayList" -> musicPlayListApi(mId)
            "forYouPlayList" -> musicPlayListApi(mId)
            "fromPlayList" -> playListTrackApi(mId)
            "artistAlbum" -> albumTrackApi(mId)
            "myDownloads" -> setLocalDeviceTrack()
            "musicStation" -> musicStation(mId)
        }
    }


    private fun setLocalDeviceTrack() {
        binding?.contentLinear?.visibility = View.GONE
        val size = resources.getDimensionPixelSize(R.dimen.about_text_size)
        binding?.genreName?.textSize = size.toFloat()
        binding?.genreName?.text = getString(R.string.my_downloads)

        glideRequestManager.load(R.drawable.my_downloads_bg)
            .into(binding?.squareImageView!!)
        glideRequestManager.load(R.drawable.my_downloads_bg)
            .into(binding?.genreImageView!!)
    }

    private fun albumTrackApi(id: String) {
        val mAuth: String = RemoteConstant.getAuthAlbumTrack(id, this)
        genreListViewModel.getAlbumTrack(mAuth, id)
    }

    private fun musicPlayListApi(id: String) {
        val mAuth: String = RemoteConstant.getAuthPlayListTrack(id, this)
        genreListViewModel.getPlayListTrack(mAuth, id)
    }

    private fun playListTrackApi(id: String) {
        val mAuth: String = RemoteConstant.getAuthPlayListTrack(id, this)
        genreListViewModel.getPlayListTrack(mAuth, id)
    }

    private fun genreTrackListApi(jukeBoxCategoryId: String, mOffset: String, mCount: String) {
        val mAuth: String =
            RemoteConstant.getAuthGenreListApi(this, jukeBoxCategoryId, mOffset, mCount)
        genreListViewModel.getGenreTrackList(mAuth, jukeBoxCategoryId, mOffset, mCount)

    }

    private fun musicStation(jukeBoxCategoryId: String) {
        val mAuth: String =
            RemoteConstant.getAuthStationTrack(jukeBoxCategoryId, this)
        genreListViewModel.getStationTrackList(mAuth, jukeBoxCategoryId)

    }

    private fun podcastListingApi(id: String, mOffset: String, mCount: String, mAscDesc: String) {
        val mAuth: String =
            RemoteConstant.getAuthPodcastEpisode(this, id, mOffset, mCount, mAscDesc)
        genreListViewModel.getPodcastEpisodeList(
            mAuth,
            id,
            mOffset,
            RemoteConstant.mCount,
            mAscDesc
        )
    }

    private fun observePodcastList() {

        genreListViewModel.isEmptyPodcast?.nonNull()?.observe(this, {
            if (it == true) {
                binding?.isVisibleList = false
                binding?.isVisibleLoading = false
                binding?.isVisibleNoData = true
                binding?.isVisibleNoInternet = false
                glideRequestManager.load(R.drawable.ic_empty_play_list)
                    .apply(RequestOptions().fitCenter())
                    .into(binding?.noDataImageView!!)
            }
        })
        genreListViewModel.radioPodCastListModel.nonNull().observe(this, {
            if (!genreListViewModel.isLoadingradioPodcast) {

                if (it?.isNotEmpty()!!) {

                    if (isLoaderAdded) {
                        removeLoaderFormListPodcast()
                    }
                    radioPodcastList = it as MutableList<RadioPodCastEpisode>


                    if (playlistManager != null &&
                        playlistManager?.currentPosition != null &&
                        playlistManager?.currentPosition != -1
                    ) {
                        val itemId = playlistManager?.currentItemChange?.currentItem?.id.toString()
                        radioPodcastList?.map { mList ->
                            if (mList.number_id == itemId) {
                                mList.isPlaying = "true"
                            } else {
                                mList.isPlaying = "false"
                            }
                        }
                    }

                    val radioPodcastListNew: MutableList<RadioPodCastEpisode> = ArrayList()
                    for (i in radioPodcastList?.indices!!) {
                        if (i % 15 == 0) {
                            if (i != 0) {
                                val mList = RadioPodCastEpisode(
                                    0, "", "", "",
                                    "-1", "", "",
                                    "", "", "",
                                    "", "", "", ""
                                )
                                radioPodcastListNew.add(mList)
                                // TODO: Google Ad count
                                mAdCount = mAdCount.plus(1)

                            }
                            radioPodcastListNew.add(radioPodcastList?.get(i)!!)
                        } else {
                            radioPodcastListNew.add(radioPodcastList?.get(i)!!)
                        }
                    }
                    mPodCastAdapter?.submitList(radioPodcastListNew)
                    mPodCastAdapter?.loadedAds(nativeAds)
                    binding?.isVisibleList = true
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = false
                    binding?.isVisibleNoInternet = false

                    // TODO : Share podcast
                    if (!isBindingExecuted) {
                        binding?.sharePlayList?.visibility = View.VISIBLE
                        if (genreListViewModel.mArtistAlbumImage != "") {
                            binding?.imageUrl = genreListViewModel.mArtistAlbumImage
                            val mTitle = genreListViewModel.description
                            binding?.description = mTitle.toString()
                            binding?.genreName?.text = genreListViewModel.podcastName
                            binding?.executePendingBindings()
                            isBindingExecuted = true
                        }
                    }

                } else {
                    binding?.isVisibleList = false
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = true
                    binding?.isVisibleNoInternet = false
                    glideRequestManager.load(R.drawable.ic_empty_play_list)
                        .apply(RequestOptions().fitCenter())
                        .into(binding?.noDataImageView!!)

                }
            }

        })
        if (genreListViewModel.isLoadCompletedradioPodcast) {
            if (isLoaderAdded) {
                removeLoaderFormListPodcast()
            }
        }


    }

    private fun observeGenreList() {

        val mListAdUitId: MutableList<String> = ArrayList()
        mListAdUitId.add(getString(R.string.ad_unit_id))
        mListAdUitId.add(getString(R.string.ad_unit_id_2))

        mGenreAdapter = GenrePlayListAdapter(
            this, this, this@CommonPlayListActivity,
        )
        binding?.genreRecyclerView?.hasFixedSize()
        binding?.genreRecyclerView?.adapter = mGenreAdapter

        genreListViewModel.mGenreName = title
        genreListViewModel.genreTrackListModel?.nonNull()
            ?.observe(this, { genreTrackList ->
                mStatus = genreTrackList.status
                if (genreTrackList.status == ResultResponse.Status.SUCCESS) {
                    val localList = genreTrackList.data as MutableList<TrackListCommon>
                    if (localList.isNotEmpty()) {
                        binding?.genreRecyclerView?.setBackgroundColor(Color.parseColor("#FFFFFF"))
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }

                        mGlobalGenreList?.addAll(localList)
                        mGenreAdapter?.loadedAds(nativeAds)
                        mGenreAdapter?.swap(mGlobalGenreList!!)

                        checkTrackPlayStatus()

                        binding?.isVisibleList = true
                        binding?.isVisibleNoData = false
                        binding?.isVisibleLoading = false
                        binding?.isVisibleNoInternet = false
                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                        // TODO : Add artist name
                        if (genreListViewModel.mArtistAlbumImage != "") {
                            binding?.imageUrl = genreListViewModel.mArtistAlbumImage
                            if (genreListViewModel.isVisibleShare) {
                                binding?.sharePlayList?.visibility = View.VISIBLE
                            }
                            binding?.executePendingBindings()
                        }
                        if (genreListViewModel.mArtistName != "") {

                            val mTitle =
                                Html.fromHtml("<font color=#a6b3c3>by </font> <font color=#1e90ff>" + genreListViewModel.mArtistName + "</font>")
                            binding?.description = mTitle.toString()
                            binding?.albumByTextView?.visibility = View.VISIBLE
                            val params = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                            )
                            params.setMargins(5, 0, 0, 0)
                            binding?.podcastContent?.layoutParams = params
                            val size = resources.getDimensionPixelSize(R.dimen.five_dp)
                            binding?.podcastContent?.textSize = size.toFloat()
                        } else {
                            val params = RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT
                            )
                            params.setMargins(0, screenWidth / 2, 0, 0)
                            binding?.genreRelative?.layoutParams = params

                        }

                    }
                } else if (genreTrackList.status == ResultResponse.Status.PAGINATED_LIST) {
                    removeLoaderFormList()
                    val mGlobalGenreListNew: MutableList<TrackListCommon> =
                        genreTrackList.data as MutableList<TrackListCommon>

                    if (mGlobalGenreListNew.isNotEmpty()) {
                        val mList = TrackListCommon()
                        mList.id = "ad"
                        mGlobalGenreList?.add(mList)
                        // TODO: Google Ad count
                        mAdCount = mAdCount.plus(1)


                        mGlobalGenreList?.addAll(mGlobalGenreListNew)
                        binding?.genreRecyclerView?.post {
                            mGenreAdapter?.swap(mGlobalGenreList!!)
                        }
                        checkTrackPlayStatus()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }

                    binding?.isVisibleList = true
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = false
                    binding?.isVisibleNoInternet = false

                } else if (genreTrackList.status == ResultResponse.Status.EMPTY_PAGINATED_LIST) {
                    removeLoaderFormList()

                } else if (genreTrackList.status == ResultResponse.Status.ERROR) {
                    if (mGlobalGenreList?.isEmpty()!!) {
                        binding?.isVisibleList = false
                        binding?.isVisibleLoading = false
                        binding?.isVisibleNoData = true
                        binding?.isVisibleNoInternet = false
                        glideRequestManager.load(R.drawable.server_error)
                            .apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.noInternetTextView?.text = getString(R.string.server_error)
                        binding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)
                    } else {
                        removeLoaderFormList()
                    }
                } else if (genreTrackList.status == ResultResponse.Status.NO_DATA) {
                    binding?.isVisibleList = false
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = true
                    binding?.isVisibleNoInternet = false
                    glideRequestManager.load(R.drawable.ic_empty_play_list)
                        .apply(RequestOptions().fitCenter())
                        .into(binding?.noDataImageView!!)

                    val mTitle =
                        Html.fromHtml("<font color=#a6b3c3>by </font> <font color=#1e90ff>" + genreListViewModel.mArtistName + "</font>")
                    binding?.description = mTitle.toString()
                } else if (genreTrackList.status == ResultResponse.Status.LOADING) {
                    if (genreTrackList.message == "0") {
                        binding?.isVisibleLoading = true
                        binding?.isVisibleList = false
                        binding?.isVisibleNoData = false
                        binding?.isVisibleNoInternet = false
                    }
                }
                genreListViewModel.genreTrackListModel?.postValue(null)
            })
    }

    private fun observePlayList(isDeleteEnabled: Boolean) {

        mSinglePlayListAdapter = SinglePlayListAdapter(
            itemClickListener = this,
            deleteEnabled = isDeleteEnabled,
            playListItemClickListener = this
        )
        binding?.genreRecyclerView?.adapter = mSinglePlayListAdapter

        genreListViewModel.genreTrackListModel?.nonNull()
            ?.observe(this, { genreTrackList ->
                mStatus = genreTrackList.status
                if (genreTrackList.status == ResultResponse.Status.SUCCESS) {
                    mGlobalGenreList = genreTrackList.data as MutableList<TrackListCommon>
                    if (mGlobalGenreList!!.isNotEmpty()) {
                        binding?.genreRecyclerView?.setBackgroundColor(Color.parseColor("#FFFFFF"))

                        mSinglePlayListAdapter?.submitList(mGlobalGenreList)
                        checkTrackPlayStatus()

                        binding?.isVisibleList = true
                        binding?.isVisibleNoData = false
                        binding?.isVisibleLoading = false
                        binding?.isVisibleNoInternet = false

                        if (genreListViewModel.imageUrl != "") {
                            binding?.imageUrl = genreListViewModel.imageUrl
                            binding?.executePendingBindings()
                        }
                        if (genreListViewModel.mGenreName != "") {
                            binding?.genreName?.text = genreListViewModel.mGenreName
                        }
                        if (genreListViewModel.isVisibleShare) {
                            binding?.sharePlayList?.visibility = View.VISIBLE
                        }

                        mPlayListUser = genreListViewModel.profileUsername
                        mPlayListUserId = genreListViewModel.profileId

                        if (mPlayListUser != "") {
                            binding?.albumByTextView?.visibility = View.VISIBLE
                            binding?.albumByTextView?.text = "Playlist by "
                            binding?.podcastContent?.text = mPlayListUser
                        } else {
                            binding?.albumByTextView?.visibility = View.GONE
                            binding?.podcastContent?.visibility = View.GONE
                        }

                        val size = resources.getDimensionPixelSize(R.dimen.five_dp)
                        binding?.podcastContent?.textSize = size.toFloat()
                        val params = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        params.setMargins(5, 0, 0, 0)
                        binding?.podcastContent?.layoutParams = params


                        mStatus = ResultResponse.Status.LOADING_COMPLETED


                    }
                } else if (genreTrackList.status == ResultResponse.Status.ERROR) {
                    if (mGlobalGenreList?.isEmpty()!!) {
                        binding?.isVisibleList = false
                        binding?.isVisibleLoading = false
                        binding?.isVisibleNoData = true
                        binding?.isVisibleNoInternet = false
                        glideRequestManager.load(R.drawable.ic_empty_play_list)
                            .apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                    } else {
                        removeLoaderFormList()
                    }
                } else if (genreTrackList.status == ResultResponse.Status.NO_DATA) {


                    if (genreListViewModel.imageUrl != "") {
                        binding?.imageUrl = genreListViewModel.imageUrl
                        binding?.executePendingBindings()
                    }
                    if (genreListViewModel.mGenreName != "") {
                        binding?.genreName?.text = genreListViewModel.mGenreName
                    }
                    if (genreListViewModel.isVisibleShare) {
                        binding?.sharePlayList?.visibility = View.VISIBLE
                    }

                    binding?.isVisibleList = false
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = true
                    binding?.isVisibleNoInternet = false
                    glideRequestManager.load(R.drawable.ic_empty_play_list)
                        .apply(RequestOptions().fitCenter())
                        .into(binding?.noDataImageView!!)

                } else if (genreTrackList.status == ResultResponse.Status.LOADING) {
                    if (genreTrackList.message == "0") {
                        binding?.isVisibleLoading = true
                        binding?.isVisibleList = false
                        binding?.isVisibleNoData = false
                        binding?.isVisibleNoInternet = false
                    }
                }

            })
    }


    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                    mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        genreListViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }


    override fun onRecommendedItemClick(itemId: Int, isFrom: String) {
        SharedPrefsUtils.setStringPreference(this, AppUtils.SELECTED_CATEGORY, "")
        populatePlayList(itemId, isFrom)
    }

    private fun populatePlayList(itemId: Int, isFrom: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            onSinglePlayListItemClick(itemId, "", isFrom, isFrom)
        }
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    private fun launchMusic(itemId: Int, mMillisecondPLay: String) {

        playlistManager?.setParameters(mediaItems, mPlayPosition)
        playlistManager?.id = PLAYLIST_ID.toLong()
        playlistManager?.currentPosition = itemId

        if (mMillisecondPLay != "" && mMillisecondPLay != "null") {
            playlistManager?.play(mMillisecondPLay.toLong(), false)
        } else {
            playlistManager?.play(0, false)
        }

        genreListViewModel.isCreatedCompletePlayList = false

    }

    override fun onRadioPodCastEpisodeItemClick(itemId: String, itemImage: String, isFrom: String) {
        var mMillisecondPLay = ""
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            // TODO : Do not comment since we need to add Analytics ( Radio Podcast )
//            if (playlistManager?.playlistHandler?.currentMediaPlayer != null &&
//                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null &&
//                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
//            ) {
//                playlistManager?.invokePausePlay()
//            }
            GlobalScope.launch(Dispatchers.Main) {
                delay(1000)

                SharedPrefsUtils.setStringPreference(
                    this@CommonPlayListActivity, AppUtils.SELECTED_CATEGORY, ""
                )

                radioPodcastList?.map {
                    if (it.number_id == itemId) {
                        mPlayPosition = radioPodcastList?.indexOf(it)!!
                        it.isPlaying = "true"
                        if (it.Millisecond != null) {
                            mMillisecondPLay = it.Millisecond
                        }
                    } else {
                        it.isPlaying = "false"
                    }
                }
                mPodCastAdapter?.submitList(radioPodcastList)
                mPodCastAdapter?.notifyDataSetChanged()

                genreListViewModel.createDBPodCastTrackPlayList(
                    radioPodcastList,
                    isFrom
                ) { mListCommon ->
                    mediaItems.clear()
                    for (sample in mListCommon) {
                        val mediaItem = MediaItem(sample, true)
                        mediaItems.add(mediaItem)
                    }
                    launchMusic(mPlayPosition, mMillisecondPLay)
                }
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }


    override fun onSinglePlayListItemClick(
        itemId: Int,
        itemImage: String,
        isFrom: String,
        playListName: String
    ) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (itemId == playlistManager?.currentItemChange?.currentItem?.id?.toInt() ?: 0) {
                if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    //TODO : If user clicked play-all always play first track
                    if (isClickFromPlayAll) {
                        isClickFromPlayAll = false
                        createLocalPlayListApi(itemId, genreListViewModel.mGenreName)
                    } else {
                        startActivityNowPlaying()
                    }
                } else {
                    createLocalPlayListApi(itemId, genreListViewModel.mGenreName)
                }
            } else {
                createLocalPlayListApi(itemId, genreListViewModel.mGenreName)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    private fun createLocalPlayListApi(itemId: Int, playListName: String) {
        genreListViewModel.createPlayList(mGlobalGenreList, playListName) { mTrackList ->
            mediaItems.clear()
            for (sample in mTrackList) {
                val mediaItem = MediaItem(sample, true)
                mediaItems.add(mediaItem)
            }
            launchMusic(mPlayPosition, "")
        }

        // TODO : Check if playing older position
        if (mGlobalGenreList != null && mGlobalGenreList?.size!! > 0 && mPlayPosition != -1) {
            if (mGlobalGenreList?.size!! >= mPlayPosition) {
                mGlobalGenreList?.get(mPlayPosition)?.isPlaying = false
                mSinglePlayListAdapter?.notifyItemChanged(mPlayPosition)
            }
        }

        mGlobalGenreList?.map {
            if (it.numberId == itemId.toString()) {
                it.isPlaying = true
                mPlayPosition = mGlobalGenreList?.indexOf(it)!!
            }
        }
        mSinglePlayListAdapter?.notifyItemChanged(mPlayPosition)

    }


    private fun startActivityNowPlaying() {

        var mImageUrlPass = ""
        if (
            playlistManager?.playlistHandler?.currentItemChange != null &&
            playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
        ) {
            mImageUrlPass =
                playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

        }
        val detailIntent =
            Intent(this, NowPlayingActivity::class.java)
        detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
        startActivity(detailIntent)
    }


    override fun onOptionItemClickOption(
        mMethod: String,
        mPlayListId: String,
        mTrackId: String
    ) {

        when (mMethod) {
            "deleteTrackFromPlayList" -> deleteTrackFromPlayList(mPlayListId, mTrackId)
//            "editPlayList" -> editPlayList(mId, mPlayListName)
        }
    }

    private fun deleteTrackFromPlayList(mPlayListId: String, mTrackId: String) {

        val mAuth = RemoteConstant.getAuthDeleteTrack(this, mPlayListId, mTrackId)
        genreListViewModel.deleteTrackToPlayList(mAuth, mTrackId, mPlayListId)

        genreListViewModel.isTrackDeleted.observe(this, {
            if (it?.success!!) {
                println("" + it.success)
            }
        })


    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)

        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListImageView?.setOnClickListener {
            val intent = Intent(this, PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                        && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        createPlayListApi(
                            "0", playListNameEditText.text.toString(), isPrivate,
                            mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(this, getString(R.string.have_play_list))
                    }

                } else {
                    AppUtils.showCommonToast(this, "Playlist name too short")
                }
            } else {
                AppUtils.showCommonToast(this, "Playlist name cannot be empty")
                //To close alert
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
        }

    }

    private fun createPlayListApi(
        playListId: String, playListName: String, private: String, mLastId: String
    ) {
        if (InternetUtil.isInternetOn()) {

            val createPlayListBody =
                CreatePlayListBody(playListId, playListName, mLastId, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(mContext = this)
            genreListViewModel.createPlayList(mAuth, createPlayListBody)


        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onPlayListItemClick(mId: String) {
        mPlayListId = mId
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 1051) {
                if (data != null) {
                    isPlayed = data.getBooleanExtra("isPlayed", false)
                }

            }
            if (resultCode == 1001) {
                if (data != null) {
                    if (data.getStringExtra("mFileUrl") != null) {
                        val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                        if (data.getStringExtra("mLastId") != null) {
                            mLastId = data.getStringExtra("mLastId")!!
                        }

                        if (playListImageView != null) {
                            glideRequestManager
                                .load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                                .into(playListImageView!!)
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        genreListViewModel.radioPodCastListModel.removeObservers(this)
        genreListViewModel.genreTrackListModel?.removeObservers(this)
        if (binding?.genreRecyclerView != null) {
            binding?.genreRecyclerView?.adapter = null
        }
//        genreListViewModel = null
    }

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun musicOptionsDialog(
        mTrackCommon: TrackListCommon, isDeleteEnabled: Boolean, mAdapterPosition: Int
    ) {
        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
            sheetView.findViewById(R.id.add_play_list_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
            sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)
        val deleteTrackLinear: LinearLayout = sheetView.findViewById(R.id.delete_track_linear)


        val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)

        val allowOfflineDownload: String? = mTrackCommon.allowOfflineDownload
        if (allowOfflineDownload == "1") {
            downloadLinear.visibility = View.VISIBLE
        } else {
            downloadLinear.visibility = View.GONE
        }

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE

        downloadLinear.setOnClickListener {
            mBottomSheetDialog.dismiss()
            showRewardedVideo(mTrackCommon)
        }


        if (isDeleteEnabled) {
            deleteTrackLinear.visibility = View.VISIBLE
        }

        // TODO : Getting my play list in search ( Visible remove track )
        val mProfileId =
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        if (mPlayListUserId == mProfileId) {
            deleteTrackLinear.visibility = View.VISIBLE
        }

        val mTrackId: String? = mTrackCommon.id
        var isLiked: String? = mTrackCommon.favorite


        deleteTrackLinear.setOnClickListener {
            onOptionItemClickOption(
                "deleteTrackFromPlayList",
                mTrackCommon.play_list_id!!, mTrackId!!
            )
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                if (mGlobalGenreList != null &&
                    mGlobalGenreList?.size!! > 0 &&
                    mGlobalGenreList?.get(mAdapterPosition) != null
                ) {
                    mGlobalGenreList?.removeAt(mAdapterPosition)
                }
                mSinglePlayListAdapter?.notifyDataSetChanged()
                mBottomSheetDialog.dismiss()
            }, 1000)

        }

        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }


        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                genreListViewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )

                val mPlayListSize = playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        playlistManager?.getItem(i)?.isFavorite = "false"
                    }

                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                genreListViewModel.removeFavourites(mAuth, mTrackId)

            }

        }


        val mAuth =
            RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.pathPlayList)
        genreListViewModel.getPlayList(mAuth)

        genreListViewModel.playListModel.nonNull()
            .observe(this, {

                if (it.isNotEmpty()) {
                    mPlayList = it as MutableList
                    playListRecyclerView.adapter = CommonPlayListAdapter(
                        mPlayList,
                        this, false, glideRequestManager
                    )

                    // TODO : Update playlist ( Home page )
                    EventBus.getDefault().postSticky(UpdatePlaylist(mPlayList!!))
                }

            })
        albumLinear.setOnClickListener {
            val intent = Intent(this, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackCommon.album?.id)
            intent.putExtra("title", mTrackCommon.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivityForResult(intent, 565)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(this, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackCommon.artist?.id)
            detailIntent.putExtra("mArtistName", mTrackCommon.artist?.artistName)
            startActivity(detailIntent)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                context = this,
                recyclerView = playListRecyclerView,
                mListener = object : RecyclerItemClickListener.ClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //TODO : Call Add to playlist API
                        if (mPlayList != null && mPlayList!![position].id != null) {
                            addTrackToPlayList(mTrackCommon.id!!, mPlayList!![position].id!!)
                        }
                        mBottomSheetDialog.dismiss()
                    }

                    override fun onLongItemClick(view: View?, position: Int) {

                    }


                })
        )

        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(
                mTrackCommon.id!!,
                this
            )
            mBottomSheetDialog.dismiss()
        }
    }

    override fun optionDialogRadioPodCast(_id: String, pod_cast_name: String) {

        val viewGroup: ViewGroup? = null
        val mBottomSheetDialog = RoundedBottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val addToPlaylistLinear: LinearLayout =
            sheetView.findViewById(R.id.add_to_playlist_linear)
        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
            sheetView.findViewById(R.id.add_play_list_image_view)
        val equalizerImageView: ImageView = sheetView.findViewById(R.id.equalizer_image_view)
        val playListRecyclerView: androidx.recyclerview.widget.RecyclerView =
            sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)
        val deleteTrackLinear: LinearLayout = sheetView.findViewById(R.id.delete_track_linear)
        val viewSeparator: View = sheetView.findViewById(R.id.view_separator)

        equalizerLinear.visibility = View.GONE
        shareLinear.visibility = View.VISIBLE
        albumLinear.visibility = View.GONE
        deleteTrackLinear.visibility = View.GONE
        addToPlaylistLinear.visibility = View.GONE
        likeMaterialButton.visibility = View.GONE
        likeLinear.visibility = View.GONE
        artistLinear.visibility = View.GONE
        addPlayListImageView.visibility = View.GONE
        equalizerImageView.visibility = View.GONE
        playListRecyclerView.visibility = View.GONE
        viewSeparator.visibility = View.GONE

        shareLinear.setOnClickListener {
            ShareAppUtils.sharePodcastEpisode(
                _id,
                this, pod_cast_name.replace("\\W+", " ").replace(" ", "-")
            )
            mBottomSheetDialog.dismiss()
        }


    }

    private fun removeLoaderFormList() {

        if (mGlobalGenreList != null &&
            mGlobalGenreList?.size!! > 0 &&
            mGlobalGenreList?.get(mGlobalGenreList?.size!! - 1) != null &&
            mGlobalGenreList?.get(mGlobalGenreList?.size!! - 1)?.id == "0"
        ) {
            val mRemovedPosition = mGlobalGenreList?.size!! - 1
            mGlobalGenreList?.removeAt(mGlobalGenreList?.size!! - 1)
            mGenreAdapter?.notifyItemRemoved(mRemovedPosition)
            isLoaderAdded = false
        }
    }

    private fun removeLoaderFormListPodcast() {

        if (radioPodcastList != null &&
            radioPodcastList?.size!! > 0 &&
            radioPodcastList?.get(radioPodcastList?.size!! - 1) != null &&
            radioPodcastList?.get(radioPodcastList?.size!! - 1)?.id == 0
        ) {
            val mRemovedPosition = radioPodcastList?.size!! - 1
            radioPodcastList?.removeAt(radioPodcastList?.size!! - 1)
            mPodCastAdapter?.notifyItemRemoved(mRemovedPosition)
            isLoaderAdded = false
        }
    }

    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)

        mChangeId =
            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id.toString()
        // TODO : Check is playing
        if (playlistManager != null && playlistManager?.playlistHandler != null &&
            playlistManager?.playlistHandler?.currentMediaPlayer != null &&
            playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
        ) {
            binding?.fab?.visibility = View.GONE
            binding?.gifProgress?.visibility = View.VISIBLE
            binding?.gifProgress?.playAnimation()
            binding?.gifProgress?.bringToFront()
            checkTrackPlayStatus()
        } else {
            binding?.gifProgress?.visibility = View.GONE
            binding?.fab?.visibility = View.VISIBLE
            binding?.gifProgress?.pauseAnimation()
            trackPause()
        }
        when (mIsFrom) {
            "fromRadio" -> {
                if (radioPodcastList?.size == 0) {
                    callApiCommon()
                }
            }
            else -> {
                if (mGlobalGenreList?.size == 0) {
                    callApiCommon()
                }
            }
        }


    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        if (mChangeId != currentItem?.id.toString() || mChangeId == currentItem?.id.toString()) {
            checkTrackPlayStatus()
            mChangeId = currentItem?.id.toString()
        }
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        mPlaybackState = playbackState
        when (playbackState) {
            PlaybackState.PLAYING -> {
                val itemChange = playlistManager?.currentItemChange
                if (itemChange != null) {
                    onPlaylistItemChanged(
                        itemChange.currentItem,
                        itemChange.hasNext,
                        itemChange.hasPrevious
                    )
                }

                binding?.fab?.visibility = View.GONE
                binding?.gifProgress?.visibility = View.VISIBLE
                binding?.gifProgress?.playAnimation()
                binding?.gifProgress?.bringToFront()
            }
            PlaybackState.PAUSED -> {
                binding?.gifProgress?.visibility = View.GONE
                binding?.fab?.visibility = View.VISIBLE
                binding?.gifProgress?.pauseAnimation()
                trackPause()
            }
            PlaybackState.RETRIEVING -> {
            }
            PlaybackState.PREPARING -> {
            }
            PlaybackState.SEEKING -> {
            }
            PlaybackState.STOPPED -> {
            }
            PlaybackState.ERROR -> {
            }
        }
        return true

    }

    private fun trackPause() {
        mGlobalGenreList?.map {
            if (it.numberId == playlistManager?.currentItemChange?.currentItem?.id.toString()
            ) {
                it.isPlaying = false
                mPlayPosition = mGlobalGenreList?.indexOf(it)!!
            }
        }
        if (mPlayPosition != -1) {
            when (mIsFrom) {
                "Genre", "musicStation", "artistAlbum" -> {
                    mGenreAdapter?.notifyItemChanged(mPlayPosition)
                }
                "fromMusicDashBoardPlayList", "myPlayList", "forYouPlayList", "fromPlayList" -> {
                    mSinglePlayListAdapter?.notifyItemChanged(mPlayPosition)
                }
            }
        }
    }

    private val GROUP_ID = "listGroup".hashCode()
    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size!! > 0) {

            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }

    }

    private fun checkTrackPlayStatus() {

        // TODO : If track is playing change status ( isPlaying )
        if (playlistManager != null &&
            playlistManager?.currentPosition != null &&
            playlistManager?.currentPosition != -1 &&
            playlistManager?.playlistHandler != null &&
            mGlobalGenreList != null && mGlobalGenreList?.isNotEmpty()!!
        ) {

            if (playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null &&
                playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying == true
            ) {
                // TODO : Check if playing older position
                if (mGlobalGenreList != null && mGlobalGenreList?.size ?: 0 > 0 && mPlayPosition != -1 &&
                    mGlobalGenreList?.size ?: 0 >= mPlayPosition && mGlobalGenreList?.get(
                        mPlayPosition
                    ) != null
                ) {
                    mGlobalGenreList?.get(mPlayPosition)?.isPlaying = false
                    when (mIsFrom) {
                        "Genre", "musicStation", "artistAlbum" -> {
                            if (mGenreAdapter != null) {
                                mGenreAdapter?.notifyItemChanged(mPlayPosition)
                            }
                        }
                        "fromMusicDashBoardPlayList", "myPlayList", "forYouPlayList", "fromPlayList" -> {
                            if (mSinglePlayListAdapter != null) {
                                mSinglePlayListAdapter?.notifyItemChanged(mPlayPosition)
                            }
                        }
                    }

                }

                // TODO : Checking if the current list track is playing
                mGlobalGenreList?.map {
                    if (it.numberId == playlistManager?.currentItemChange?.currentItem?.id.toString()
                    ) {
                        it.isPlaying = true
                        mPlayPosition = mGlobalGenreList?.indexOf(it)!!
                    }
                }

                //TODO : Notify play animation
                if (mPlayPosition != -1) {
                    when (mIsFrom) {
                        "Genre", "musicStation", "artistAlbum" -> {
                            mGenreAdapter?.notifyItemChanged(mPlayPosition)
                        }
                        "fromMusicDashBoardPlayList", "myPlayList", "forYouPlayList", "fromPlayList" -> {
                            mSinglePlayListAdapter?.notifyItemChanged(mPlayPosition)
                        }
                    }
                }
            } else {
                mGlobalGenreList?.map {
                    if (it.numberId == playlistManager?.currentItemChange?.currentItem?.id.toString()
                    ) {
                        it.isPlaying = false
                        mPlayPosition = mGlobalGenreList?.indexOf(it)!!
                    }
                }
                if (mPlayPosition != -1) {
                    when (mIsFrom) {
                        "Genre", "musicStation", "artistAlbum" -> {
                            mGenreAdapter?.notifyItemChanged(mPlayPosition)
                        }
                        "fromMusicDashBoardPlayList", "myPlayList", "forYouPlayList", "fromPlayList" -> {
                            mSinglePlayListAdapter?.notifyItemChanged(mPlayPosition)
                        }
                    }
                }
            }
        }

    }

    private fun loadRewardedAd() {
        try {
            if (mRewardedAd != null) {
                if (mRewardedAd?.isLoaded!!) {
                    mIsLoading = true
                    mRewardedAd = RewardedAd(this, getString(R.string.ad_unit_id_reward))
                    mRewardedAd?.loadAd(
                        AdRequest.Builder()
                            .build(),
                        object : RewardedAdLoadCallback() {
                            override fun onRewardedAdLoaded() {
                                mIsLoading = false
                            }

                            override fun onRewardedAdFailedToLoad(errorCode: Int) {
                                mIsLoading = false
                            }
                        }
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showRewardedVideo(mTrackDetails: TrackListCommon) {
        try {
            if (mRewardedAd != null && mRewardedAd?.isLoaded!!) {

                mRewardedAd?.show(
                    this,
                    object : RewardedAdCallback() {
                        override fun onUserEarnedReward(
                            rewardItem: RewardItem
                        ) {
                            isRewardEarned = true
                        }

                        override fun onRewardedAdClosed() {
                            isRewardEarned = if (!isRewardEarned) {

                                Toast.makeText(
                                    this@CommonPlayListActivity,
                                    "Finish the ad to download the track",
                                    Toast.LENGTH_LONG
                                ).show()

                                false
                            } else {
                                startDownload(mTrackDetails)
                                false
                            }
                            loadRewardedAd()
                        }

                        override fun onRewardedAdFailedToShow(errorCode: Int) {
                            Toast.makeText(
                                this@CommonPlayListActivity,
                                "something went wrong please try again",
                                Toast.LENGTH_LONG
                            ).show()
                        }

                        override fun onRewardedAdOpened() {
                        }
                    }
                )
            } else {
                startDownload(mTrackDetails)
            }
        } catch (e: Exception) {
            Toast.makeText(this, "" + e, Toast.LENGTH_SHORT).show()
        }
    }

    private fun startDownload(mTrackDetails: TrackListCommon) {

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)


        val paramsNotification = Bundle()
        paramsNotification.putString(
            "user_id",
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        )
        paramsNotification.putString("track_id", mTrackDetails.id)
        paramsNotification.putString("event_type", "track_download")
        firebaseAnalytics.logEvent("offline_track_download", paramsNotification)


        genreListViewModel.addToDownload(mTrackDetails)
        Toast.makeText(this, "Track added to queue", Toast.LENGTH_SHORT).show()


        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            enqueueDownloads()
        }, 1000)
    }
}