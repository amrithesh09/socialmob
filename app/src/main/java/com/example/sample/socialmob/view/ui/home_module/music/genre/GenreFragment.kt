package com.example.sample.socialmob.view.ui.home_module.music.genre

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.GenreFragmentBinding
import com.example.sample.socialmob.model.music.Genre
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GenreFragment : Fragment(), GenreListAdapter.GenreListAdapterItemClickListener {
    private val viewModel: MusicDashBoardViewModel by viewModels()
    private var mAdapter: GenreListAdapter? = null

    @Inject
    lateinit var glideRequestManager: RequestManager
    private var binding: GenreFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callApiCommon()
        mAdapter = GenreListAdapter(
            requireActivity(), this, glideRequestManager
        )
        binding?.genereRecyclerView?.adapter = mAdapter

        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }
    }

    private fun callApiCommon() {
        if (!InternetUtil.isInternetOn()) {
            binding?.loadingLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.VISIBLE
            binding?.genereRecyclerView?.visibility = View.GONE
            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(binding?.noInternetImageView!!)
            val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        } else {
            genreListApi()
        }
    }

    private fun genreListApi() {
        val mAuth: String =
            RemoteConstant.getAuthWithoutOffsetAndCount(requireActivity(), RemoteConstant.pathGenre)
        viewModel.getGenreApiCall(mAuth)
        viewModel.genresMutableLiveData?.observe(viewLifecycleOwner, { genreList ->
            if (genreList != null && genreList.status == ResultResponse.Status.SUCCESS) {
                val mGenreListData: MutableList<Genre> = genreList.data!!
                if (mGenreListData.isNotEmpty()) {
                    binding?.loadingLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.genereRecyclerView?.visibility = View.VISIBLE
                    mAdapter?.swap(mGenreListData)
                }
            } else if (genreList != null && genreList.status == ResultResponse.Status.ERROR) {
                binding?.loadingLinear?.visibility = View.GONE
                binding?.genereRecyclerView?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.VISIBLE
                glideRequestManager.load(R.drawable.server_error).apply(RequestOptions().fitCenter())
                    .into(binding?.noInternetImageView!!)
                binding?.noInternetTextView?.text = getString(R.string.server_error)
                binding?.noInternetSecTextView?.text = getString(R.string.server_error_content)

            } else if (genreList != null && genreList.status == ResultResponse.Status.LOADING) {
                binding?.loadingLinear?.visibility = View.VISIBLE
                binding?.genereRecyclerView?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.genre_fragment, container, false)
        return binding?.root!!
    }

    override fun onItemClick(jukeBoxCategoryId: String?, mTitle: String, imageUrl: String) {
        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", jukeBoxCategoryId)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", imageUrl)
        intent.putExtra("mIsFrom", "Genre")
        startActivity(intent)
    }
}