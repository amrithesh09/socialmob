package com.example.sample.socialmob.view.ui.home_module.music.genre

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.Genre
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.Constants


class GenreListAdapter internal constructor(
    private val context: Context,
    private val mIntentInterface: GenreListAdapterItemClickListener,
    private var glideRequestManager: RequestManager
) : RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private val mList: MutableList<Genre> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        when (viewType) {
            Constants.ViewType.LEFT ->
                return LeftHolder(mInflater.inflate(R.layout.genere_item_left, parent, false))
            Constants.ViewType.RIGHT ->
                return RightHolder(mInflater.inflate(R.layout.genere_item, parent, false))
        }
        return LeftHolder(mInflater.inflate(R.layout.genere_item_left, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mList[position])
    }


    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) {
            Constants.ViewType.RIGHT
        } else
            Constants.ViewType.LEFT

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class RightHolder(itemView: View) : BaseViewHolder<Any>(itemView) {
        private val mItem: TextView = itemView.findViewById(R.id.genere_title)
        private val mGenreImageView: ImageView = itemView.findViewById(R.id.genre_image_view)
        override fun bind(genre: Any) {
            mItem.text = mList[adapterPosition].genreName
            glideRequestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        mList[adapterPosition].media?.cover_image_filename,
                        RemoteConstant.getWidth(context) / 2
                    )
                )
                .thumbnail(0.1f)
                .into(mGenreImageView)
            mGenreImageView.setOnClickListener {
                var mV3Image = ""
                if (mList[adapterPosition].media?.squareImage != null && mList[adapterPosition].media?.squareImage != "") {
                    mV3Image = mList[adapterPosition].media?.squareImage!!
                }

                mIntentInterface.onItemClick(
                    mList[adapterPosition].id,
                    mList[adapterPosition].genreName,
                    mV3Image
                )
            }
        }

    }


    inner class LeftHolder(itemView: View) : BaseViewHolder<Any>(itemView) {
        private val mItem: TextView = itemView.findViewById(R.id.genere_title_left)
        private val mGenreImageView: ImageView = itemView.findViewById(R.id.genre_image_view)


        override fun bind(genre: Any) {
            mItem.text = mList[adapterPosition].genreName
            glideRequestManager
                .load(
                    RemoteConstant.getImageUrlFromWidth(
                        mList[adapterPosition].media?.cover_image_filename,
                        RemoteConstant.getWidth(context) / 2
                    )
                )
                .thumbnail(0.1f)
                .into(mGenreImageView)
            mGenreImageView.setOnClickListener {
                var mV3Image = ""
                if (mList[adapterPosition].media?.squareImage != null && mList[adapterPosition].media?.squareImage != "") {
                    mV3Image = mList[adapterPosition].media?.squareImage!!
                }

                mIntentInterface.onItemClick(
                    mList[adapterPosition].id,
                    mList[adapterPosition].genreName,
                    mV3Image
                )
            }
        }
    }

    internal interface GenreListAdapterItemClickListener {
        fun onItemClick(jukeBoxCategoryId: String?, mTitle: String, imageUrl: String)
    }

    fun swap(mutableList: MutableList<Genre>) {
        val diffCallback =
            GnereDiffUtils(
                this.mList,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.mList.clear()
        this.mList.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

}

private class GnereDiffUtils(
    private val oldList: MutableList<Genre>,
    private val newList: MutableList<Genre>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].genreName == newList[newItemPosition].genreName
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].genreName == newList[newItemPosition].genreName

    }
}

