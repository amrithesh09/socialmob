package com.example.sample.socialmob.view.ui.home_module.music.genre

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AdmobItemNativeAdBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.RadioPodCastEpisodeItemBinding
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.google.android.ads.nativetemplates.NativeTemplateStyle
import com.google.android.gms.ads.formats.UnifiedNativeAd
import kotlinx.android.synthetic.main.admob_item_native_ad.view.*
import java.util.*

class RadioPodCastEpisodeDetailsAdapter(
    private val mContext: Context,
    private val radioPodCastEpisodeItemClickListener: RadioPodCastEpisodeItemClickListener,
    private val optionDialog: OptionDialog?

) :
    ListAdapter<RadioPodCastEpisode, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    private var preloadedNativeAds: MutableList<UnifiedNativeAd> = ArrayList()

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_AD = 3
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<RadioPodCastEpisode>() {
                override fun areItemsTheSame(
                    oldItem: RadioPodCastEpisode,
                    newItem: RadioPodCastEpisode
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: RadioPodCastEpisode,
                    newItem: RadioPodCastEpisode
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_AD -> bindAd(parent)
            else -> bindData(parent)
        }
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: AdmobItemNativeAdBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.admob_item_native_ad, parent, false
        )
        return AdViewHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: RadioPodCastEpisodeItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.radio_pod_cast_episode_item,
            parent, false
        )
        return RadioPodCastDetailViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    override fun getItemViewType(position: Int): Int {

        return when {
            getItem(position)._id != "" -> ITEM_DATA
            getItem(position).episode_name == "-1" -> ITEM_AD
            else -> ITEM_LOADING
        }
    }

    fun loadedAds(nativeAds: MutableList<UnifiedNativeAd>) {
        this.preloadedNativeAds = nativeAds
    }

    inner class AdViewHolder(val binding: AdmobItemNativeAdBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            if (preloadedNativeAds.size > 0) {
                if (mContext != null && binding.root.my_template != null) {
                    val min = 0
                    val max = preloadedNativeAds.size
                    val r = Random()
                    val i1 = r.nextInt(max - min + 1) + min
                    val unifiedNativeAd: UnifiedNativeAd = preloadedNativeAds[i1]
                    val styles =
                        NativeTemplateStyle.Builder().build()
                    val template = binding.root.my_template
                    template.setStyles(styles)
                    template.setNativeAd(unifiedNativeAd)
                }
                binding.adLinear.visibility = View.VISIBLE
            } else {
//                binding.adLinear.visibility = View.GONE
            }
        }

    }


    inner class RadioPodCastDetailViewHolder(val binding: RadioPodCastEpisodeItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.trackViewModel = getItem(adapterPosition)!!
            binding.executePendingBindings()
            binding.trackOptions.bringToFront()

            if (getItem(adapterPosition)?.isPlaying == "true") {
                binding.queueListItemPlayImageView.visibility = View.VISIBLE
                binding.topTrackGifProgress.visibility = View.VISIBLE
                binding.topTrackGifProgress.playAnimation()
            } else {
                binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                binding.topTrackGifProgress.visibility = View.INVISIBLE
                if (binding.topTrackGifProgress.isAnimating) {
                    binding.topTrackGifProgress.pauseAnimation()
                }
            }

            binding.topLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (itemCount > 0 && getItem(adapterPosition).cover_image != null
                    ) {
                        radioPodCastEpisodeItemClickListener.onRadioPodCastEpisodeItemClick(
                            getItem(adapterPosition).number_id!!,
                            getItem(adapterPosition).cover_image!!,
                            getItem(adapterPosition).podcast_name ?: ""
                        )
                    }
                }
            }
            binding.trackOptions.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    optionDialog?.optionDialogRadioPodCast(
                        getItem(adapterPosition)._id!!,
                        getItem(adapterPosition).podcast_name!!
                    )
                }
            }
        }

    }

    interface RadioPodCastEpisodeItemClickListener {
        fun onRadioPodCastEpisodeItemClick(itemId: String, itemImage: String, isFrom: String)
    }

    interface OptionDialog {
        fun optionDialogRadioPodCast(_id: String, pod_cast_name: String)
    }
}