package com.example.sample.socialmob.view.ui.home_module.music.genre

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AdmobItemBannerBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.RecommendedTrackItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.view.utils.BaseViewHolder

class SinglePlayListAdapter(
    private val itemClickListener: SinglePlayListItemClickListener,
    private val deleteEnabled: Boolean,
    private var playListItemClickListener: PlayListItemClickListener
) :
    ListAdapter<TrackListCommon, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_AD = 3

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<TrackListCommon>() {
                override fun areItemsTheSame(
                    oldItem: TrackListCommon,
                    newItem: TrackListCommon
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: TrackListCommon,
                    newItem: TrackListCommon
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_AD -> bindAd(parent)
            else -> bindData(parent)
        }

    }

    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding:RecommendedTrackItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recommended_track_item, parent, false
        )
        return SinglePlayListViewHolder(binding)
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {

        val binding: AdmobItemBannerBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.admob_item_banner, parent, false
        )
        return AdViewHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(position))
    }

    inner class SinglePlayListViewHolder(val binding: RecommendedTrackItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.trackViewModel = getItem(adapterPosition)
            binding.executePendingBindings()


            if (getItem(adapterPosition)?.isPlaying!!) {
                binding.queueListItemPlayImageView.visibility = View.VISIBLE
                binding.topTrackGifProgress.visibility = View.VISIBLE
                binding.topTrackGifProgress.playAnimation()
            } else {
                binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                binding.topTrackGifProgress.visibility = View.INVISIBLE
                if (binding.topTrackGifProgress.isAnimating) {
                    binding.topTrackGifProgress.pauseAnimation()
                }
            }

            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    itemClickListener.onSinglePlayListItemClick(getItem(adapterPosition)?.numberId?.toInt()?:0, getItem(adapterPosition)?.media?.coverFile!!, "QuickPlayList","My playlist")
                }
            }
            binding.trackOptions.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    playListItemClickListener.musicOptionsDialog(
                       getItem(
                            adapterPosition
                        ), deleteEnabled, adapterPosition
                    )
                }
            }

        }

    }

    inner class AdViewHolder(val binding: AdmobItemBannerBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
//            val adRequest = AdRequest.Builder().addTestDevice("6A55E8D9F9E755271441D0891D626792")
//                .build()
//            binding.root.adViewTrack.loadAd(adRequest)
        }

    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    interface SinglePlayListItemClickListener {
        fun onSinglePlayListItemClick(itemId: Int, itemImage: String, isFrom: String, playListName: String)
    }

    interface OptionItemClickListener {
        fun onOptionItemClickOption(mMethod: String, mPlayListId: String, mTrackId: String)
    }

    interface PlayListItemClickListener {
        fun musicOptionsDialog(
            mTrackCommon: TrackListCommon,
            isDeleteEnabled: Boolean,
            mAdapterPosition: Int
        )
    }
}