package com.example.sample.socialmob.view.ui.home_module.music.musicvideo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MusicVideoFragmentBinding
import com.example.sample.socialmob.model.music.MusicVideoPayloadVideo
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.ui.home_module.music.ArtistVideoDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.music.MusicVideoListAdapter
import com.example.sample.socialmob.view.utils.RecyclerItemClickListener
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MusicVideoFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private var binding: MusicVideoFragmentBinding? = null
    private val viewModel: MusicDashBoardViewModel by viewModels()
    private var mVideoAdapter: MusicVideoListAdapter? = null
    private var musicVideo: MutableList<MusicVideoPayloadVideo>? = ArrayList()
    private var apiStatus: ResultResponse.Status? = ResultResponse.Status.LOADING
    private var isLoaderAdded: Boolean = false

    companion object {
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String?, param2: String?): MusicVideoFragment {
            val fragment = MusicVideoFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.music_video_fragment, container, false)
        return binding?.root!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mVideoAdapter = MusicVideoListAdapter(requireActivity(), ArrayList())
        binding?.genereRecyclerView?.layoutManager =
                WrapContentLinearLayoutManager(requireActivity())
        binding?.genereRecyclerView?.adapter = mVideoAdapter


        binding?.swipeToRefreshMusicVideo?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefreshMusicVideo?.isRefreshing = false
        binding?.swipeToRefreshMusicVideo?.setOnRefreshListener {
            musicVideo?.clear()
            callApiCommon()
        }

        binding?.featuredTitile?.visibility = View.VISIBLE
        binding?.noDataLinear?.visibility = View.GONE
        binding?.loadingLinear?.visibility = View.VISIBLE
        binding?.genereRecyclerView?.visibility = View.GONE
        binding?.noInternetLinear?.visibility = View.GONE
        callApiCommon()
        observeData()

        binding?.genereRecyclerView?.addOnItemTouchListener(
                RecyclerItemClickListener(
                        context = requireActivity(),
                        recyclerView = binding?.genereRecyclerView!!,
                        mListener = object : RecyclerItemClickListener.ClickListener {
                            override fun onItemClick(view: View, position: Int) {
                                if (musicVideo?.size!! > position) {
                                    val intent =
                                        Intent(activity, ArtistVideoDetailsActivity::class.java)
                                    intent.putExtra(
                                        "mUrl",
                                        musicVideo?.get(position)?.media?.masterPlaylist
                                    )
                                    intent.putExtra("mArtistId", musicVideo?.get(position)?.id)
                                    intent.putExtra("isFromViewAll", "false")
                                    startActivityForResult(intent, 10)
                                }
                            }

                            override fun onLongItemClick(view: View?, position: Int) {

                            }
                        })
        )

        binding?.genereRecyclerView?.addOnScrollListener(CustomScrollListener())
    }

    inner class CustomScrollListener :
            androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
                recyclerView: androidx.recyclerview.widget.RecyclerView,
                newState: Int
        ) {

        }

        override fun onScrolled(
                recyclerView: androidx.recyclerview.widget.RecyclerView,
                dx: Int,
                dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (!viewModel.isLoadingMusicVideo && apiStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    callApiCommon()
                    val loaderItem: MutableList<MusicVideoPayloadVideo> = ArrayList()
                    val mList = MusicVideoPayloadVideo()
                    mList.id = ""
                    loaderItem.add(mList)
                    musicVideo?.addAll(loaderItem)
                    binding?.genereRecyclerView?.post {
                        mVideoAdapter?.swapMusicVideo(musicVideo?:ArrayList())
                    }
                    isLoaderAdded = true

                }
            }
        }
    }
    /**
     * Remove loader from list - pagination
     */
    private fun removeLoaderFormList() {
        if (musicVideo != null && musicVideo?.size!! > 0 &&
                musicVideo?.get(musicVideo?.size!! - 1) != null &&
                musicVideo?.get(musicVideo?.size!! - 1)?.id == ""
        ) {
            musicVideo?.removeAt(musicVideo?.size!! - 1)
            mVideoAdapter?.swapMusicVideo(musicVideo?:ArrayList())
            isLoaderAdded = false
        }
    }

    private fun callApiCommon() {
        if (!InternetUtil.isInternetOn()) {
            if (musicVideo?.size == 0) {
                binding?.noDataLinear?.visibility = View.GONE
                binding?.loadingLinear?.visibility = View.GONE
                binding?.genereRecyclerView?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.VISIBLE
            }
        } else {
            viewModel.getMusicVideo((musicVideo?.size ?: 0).toString())
        }
    }

    private fun observeData() {

        viewModel.musicVideos.observe(viewLifecycleOwner, { musicVideos ->
            val mLocalList = musicVideos.data
            apiStatus = musicVideos.status
            when (musicVideos.status) {
                ResultResponse.Status.SUCCESS -> {
                    musicVideo?.addAll(mLocalList!!)
                    mVideoAdapter?.swapMusicVideo(musicVideo ?: ArrayList())
                    binding?.noDataLinear?.visibility = View.GONE
                    binding?.loadingLinear?.visibility = View.GONE
                    binding?.genereRecyclerView?.visibility = View.VISIBLE
                    binding?.noInternetLinear?.visibility = View.GONE
                    if (binding?.swipeToRefreshMusicVideo?.isRefreshing!!) {
                        binding?.swipeToRefreshMusicVideo?.isRefreshing = false
                    }
                }
                ResultResponse.Status.EMPTY_PAGINATED_LIST -> {

                    if (binding?.swipeToRefreshMusicVideo?.isRefreshing!!) {
                        binding?.swipeToRefreshMusicVideo?.isRefreshing = false
                    }
                    removeLoaderFormList()
                }
                ResultResponse.Status.PAGINATED_LIST -> {
                    removeLoaderFormList()
                    musicVideo?.addAll(mLocalList!!)
                    mVideoAdapter?.swapMusicVideo(musicVideo ?: ArrayList())
                    if (binding?.swipeToRefreshMusicVideo?.isRefreshing!!) {
                        binding?.swipeToRefreshMusicVideo?.isRefreshing = false
                    }
                }
                ResultResponse.Status.NO_INTERNET -> {
                    if (musicVideo?.size == 0) {
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.loadingLinear?.visibility = View.GONE
                        binding?.genereRecyclerView?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.VISIBLE
                    }
                    if (binding?.swipeToRefreshMusicVideo?.isRefreshing!!) {
                        binding?.swipeToRefreshMusicVideo?.isRefreshing = false
                    }
                }
                else -> {
                    removeLoaderFormList()
                    if (binding?.swipeToRefreshMusicVideo?.isRefreshing!!) {
                        binding?.swipeToRefreshMusicVideo?.isRefreshing = false
                    }
                }
            }
        })
    }
}