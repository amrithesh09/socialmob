package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.content.Context
import android.view.View
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.FeaturedPodcast
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.RadioPodCastAdapter
import com.example.sample.socialmob.view.utils.cardLayout.CardSliderAdapter
import kotlinx.android.synthetic.main.featured_podcast_adapter.view.*
import java.util.*

class FeaturedPodCastAdapter(
    items: ArrayList<FeaturedPodcast>, private val mContext: Context,
    private val mCallBack: RadioPodCastAdapter.QuickPlayListAdapterItemClickListener,
    private var glideRequestManager: RequestManager
) : CardSliderAdapter<FeaturedPodcast>(items) {

    override fun bindView(position: Int, itemContentView: View, item: FeaturedPodcast?) {

        item?.run {
            if (item.podcastName != null) {
                itemContentView.podcast_name_text_view.text = item.podcastName
            }
            if (item.isNew != null && Objects.requireNonNull(item.isNew) == "true") {
                itemContentView.new_flag.visibility = View.VISIBLE
            } else {
                itemContentView.new_flag.visibility = View.GONE
            }

            if (item.coverImage != null) {
                glideRequestManager
                    .load(item.coverImage)
                    .thumbnail(0.1f)
                    .into(itemContentView.podcast_image_view)
            }

            itemContentView.podcast_image_view.setOnClickListener {
                var mDescription: String? = ""
                if (item.description != null) {
                    mDescription = item.description
                }
                if (item.id != null) {
                    mCallBack.onPlayListItemClick(
                        item.id, item.podcastName!!, item.coverImage!!, mDescription!!
                    )
                }
            }


        }
    }

    override fun getItemContentLayout(position: Int) = R.layout.featured_podcast_adapter
}