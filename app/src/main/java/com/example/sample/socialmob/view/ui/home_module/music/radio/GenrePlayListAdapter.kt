package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AdmobItemNativeAdBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.RecommendedTrackItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.google.android.ads.nativetemplates.NativeTemplateStyle
import com.google.android.gms.ads.formats.UnifiedNativeAd
import kotlinx.android.synthetic.main.admob_item_native_ad.view.*
import java.util.*

class GenrePlayListAdapter(
    private val itemClickListener: GenrePlayListItemClickListener,
    private var playListItemClickListener: PlayListItemClickListener,
    private var mContext: Context
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var genreTrackList: MutableList<TrackListCommon>? = ArrayList()
    private var preloadedNativeAds: MutableList<UnifiedNativeAd> = ArrayList()

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_AD = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_AD -> bindAd(parent)
            else -> bindData(parent)
        }
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: AdmobItemNativeAdBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.admob_item_native_ad, parent, false
        )
        return AdViewHolder(binding)
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: RecommendedTrackItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recommended_track_item, parent, false
        )
        return GenrePlayListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(genreTrackList?.get(position))
    }

    inner class AdViewHolder(val binding: AdmobItemNativeAdBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            if (preloadedNativeAds.size > 0) {
                if (mContext != null && binding.root.my_template != null) {
                    val min = 0
                    val max = preloadedNativeAds.size
                    val r = Random()
                    val i1 = r.nextInt(max - min + 1) + min
                    val unifiedNativeAd: UnifiedNativeAd = preloadedNativeAds[i1]
                    val styles =
                        NativeTemplateStyle.Builder().build()
                    val template = binding.root.my_template
                    template.setStyles(styles)
                    template.setNativeAd(unifiedNativeAd)
                }
                binding.adLinear.visibility = View.VISIBLE
            } else {
//                binding.adLinear.visibility = View.GONE
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            genreTrackList?.get(position)?.id == "0" -> ITEM_LOADING
            genreTrackList?.get(position)?.id == "ad" -> ITEM_AD
            genreTrackList?.get(position)?.id != "" -> ITEM_DATA
            else -> ITEM_DATA
        }
    }

    inner class GenrePlayListViewHolder(val binding: RecommendedTrackItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(dataTrackCommon: Any?) {
            val mTrackCommon = dataTrackCommon as TrackListCommon
            binding.trackViewModel = mTrackCommon
            binding.executePendingBindings()

            try {
                if (mTrackCommon.isPlaying!!) {
                    binding.queueListItemPlayImageView.visibility = View.VISIBLE
                    binding.topTrackGifProgress.visibility = View.VISIBLE
                    binding.topTrackGifProgress.playAnimation()
                } else {
                    binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                    binding.topTrackGifProgress.visibility = View.INVISIBLE
                    if (binding.topTrackGifProgress.isAnimating) {
                        binding.topTrackGifProgress.pauseAnimation()
                    }
                }
                itemView.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (itemCount > 0 && mTrackCommon.numberId != null) {
                            itemClickListener.onRecommendedItemClick(
                                mTrackCommon.numberId?.toInt()!!, "Genre"
                            )
                        }
                    }
                }
                binding.trackOptions.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        playListItemClickListener.musicOptionsDialog(
                            mTrackCommon, false, adapterPosition
                        )
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("AmR_", "GenrePlayListViewHolder_" + e.printStackTrace())
            }

        }
    }

    interface GenrePlayListItemClickListener {
        fun onRecommendedItemClick(itemId: Int, isFrom: String)
    }

    interface PlayListItemClickListener {
        fun musicOptionsDialog(
            mTrackCommon: TrackListCommon,
            isDeleteEnabled: Boolean, mAdapterPosition: Int
        )
    }

    fun swap(mutableList: MutableList<TrackListCommon>) {
        val diffCallback =
            TrackListDiffUtils(
                this.genreTrackList!!,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback, false)

        this.genreTrackList?.clear()
        this.genreTrackList?.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)

        println(">>>>>List size" + mutableList.size)
    }

    override fun getItemCount(): Int {
        return genreTrackList?.size ?: 0
    }


    fun loadedAds(nativeAds: MutableList<UnifiedNativeAd>) {
        this.preloadedNativeAds = nativeAds
    }
}

private class TrackListDiffUtils(
    private val oldList: MutableList<TrackListCommon>,
    private val newList: MutableList<TrackListCommon>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].isPlaying?.equals(newList[newItemPosition].isPlaying)!!
    }
}