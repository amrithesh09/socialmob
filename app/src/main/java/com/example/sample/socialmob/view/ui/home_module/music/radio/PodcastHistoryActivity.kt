package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.RecentlyPlayedActivtyBinding
import com.example.sample.socialmob.model.music.PodcastHistoryTrack
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import java.util.*
import kotlin.collections.ArrayList

class PodcastHistoryActivity : BaseCommonActivity(),
    PodcastHistoryTrackAdapter.PodcastHistoryTrackPlayListItemClickListener,
    PlaylistListener<MediaItem> {


    private var playlistManager: PlaylistManager? = null
    private var isSubmittedList: Boolean = false
    private var binding: RecentlyPlayedActivtyBinding? = null
    private val viewModel: MusicDashBoardViewModel by viewModels()
    private var mPodCastAdapter: PodcastHistoryTrackAdapter? = null
    private var mPlaybackState: PlaybackState? = null

    private var mPlayPosition = -1
    private var PLAYLIST_ID = 4 //Arbitrary, for the example
    private var podCastHistoryTrackTrackModelList: MutableList<PodcastHistoryTrack>? = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.recently_played_activty)

        binding?.titleTextView?.text = getString(R.string.continue_listening)

        binding?.progressLinear?.visibility = View.GONE
        binding?.noInternetLinear?.visibility = View.GONE
        binding?.noDataLinear?.visibility = View.GONE
        binding?.photoGridRecyclerView?.visibility = View.VISIBLE


        binding?.recentlyPlayedBackImageView?.setOnClickListener {
            onBackPressed()
        }
        observePodHistory()

        binding?.photoGridRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        mPodCastAdapter = PodcastHistoryTrackAdapter(
            this,
            this, false
        )
        binding?.photoGridRecyclerView?.adapter = mPodCastAdapter

    }

    private fun observePodHistory() {
        viewModel.podcastHistoryModel.nonNull().observe(this, {
            if (it != null && it.isNotEmpty()) {
                if (!isSubmittedList) {
                    isSubmittedList = true
                    val mList = it as MutableList<PodcastHistoryTrack>
                    podCastHistoryTrackTrackModelList = mList.asReversed()
                    mPodCastAdapter?.submitList(podCastHistoryTrackTrackModelList)
                    mPodCastAdapter?.notifyDataSetChanged()
                    if (mPlaybackState != PlaybackState.PREPARING) {
                        setPlayPause()
                    }
                }
            }
        })

        binding?.gifProgress?.setOnClickListener { view: View? -> binding?.fab?.performClick() }
        binding?.fab?.setOnClickListener {
                if (playlistManager != null &&
                        playlistManager?.playlistHandler != null &&
                        playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                        playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    var mImageUrlPass = ""
                    if (
                            playlistManager?.playlistHandler?.currentItemChange != null &&
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                                playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                            Intent(this@PodcastHistoryActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                } else if (playlistManager != null &&
                        playlistManager?.playlistHandler != null &&
                        playlistManager?.playlistHandler?.currentMediaPlayer != null
                ) {
                    var mImageUrlPass = ""
                    if (
                            playlistManager?.playlistHandler?.currentItemChange != null &&
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                                playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                            Intent(this@PodcastHistoryActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                }
        }
    }

    private fun setPlayPause() {
        val mTrackId: String
        if (playlistManager != null
            && playlistManager?.playlistHandler != null
            && playlistManager?.playlistHandler?.currentMediaPlayer != null
            && playlistManager?.currentItemChange != null
            && playlistManager?.currentItemChange?.currentItem != null
            && playlistManager?.currentItem != null
            && playlistManager?.currentItem?.mExtraId != null
        ) {
            mTrackId =
                playlistManager?.currentItemChange?.currentItem?.mExtraId!!


            if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                podCastHistoryTrackTrackModelList?.map { podcastHistoryTrack ->
                    if (podcastHistoryTrack.mExtraId == mTrackId) {
                        podcastHistoryTrack.isPlaying = "true"
                    } else {
                        podcastHistoryTrack.isPlaying = "false"
                    }
                }
            } else {
                podCastHistoryTrackTrackModelList?.map { podcastHistoryTrack ->
                    podcastHistoryTrack.isPlaying = "false"
                }
            }
            mPodCastAdapter?.submitList(podCastHistoryTrackTrackModelList)
            mPodCastAdapter?.notifyDataSetChanged()
        }
    }

    // TODO : To change status in music page when Notification bar is pulled down/up
    // Since on resume not calling when Notification bar is pulled down/up
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        // handle when the user pull down the notification bar where
        // (hasFocus will ='false') & if the user pushed the
        // notification bar back to the top, then (hasFocus will ='true')
        if (!hasFocus) {
            Log.i("Tag", "Notification bar is pulled down")

        } else {
            setPlayPause()
            Log.i("Tag", "Notification bar is pushed up")
        }
        super.onWindowFocusChanged(hasFocus)

    }

    private fun updatePLaying(mTrackId: String?) {

        if (podCastHistoryTrackTrackModelList != null && podCastHistoryTrackTrackModelList?.size!! > 0 && mPlayPosition != -1) {
            podCastHistoryTrackTrackModelList?.get(mPlayPosition)?.isPlaying = "false"
            mPodCastAdapter?.notifyItemChanged(mPlayPosition)
        }
        podCastHistoryTrackTrackModelList?.map {
            if (it.mExtraId == mTrackId) {
                it.isPlaying = "true"
                mPlayPosition = podCastHistoryTrackTrackModelList?.indexOf(it)!!
            } else {
                it.isPlaying = "false"
            }
        }
        mPodCastAdapter?.notifyItemChanged(mPlayPosition)


    }

    override fun onPodcastHistoryTrackPlayListItemClick(
        itemId: Int, itemImage: String, isFrom: String, podcastId: String
    ) {
        // TODO : Do not comment since we need to add Analytics ( Radio Podcast )
        if (playlistManager != null
            && playlistManager?.playlistHandler != null
            && playlistManager?.playlistHandler?.currentMediaPlayer != null
            && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
        ) {
            playlistManager?.invokePausePlay()
        }


        updatePLaying(podcastId)

        viewModel.createDBPodCastHistoryTrackPlayList(
            podCastHistoryTrackTrackModelList!!, "Podcast History"
        ) { playList ->
            val list: MutableList<PlayListDataClass> = ArrayList()
            val mediaItems = LinkedList<MediaItem>()
            for (listItem in playList) {
                list.add(listItem)
                val mediaItem = MediaItem(listItem, true)
                mediaItems.add(mediaItem)
            }

            // TODO : If app launching first time no need to play music
            playlistManager?.setParameters(mediaItems, itemId)
            playlistManager?.currentPosition = itemId
            playlistManager?.id = PLAYLIST_ID.toLong()
            playlistManager?.play(0, false)

        }
    }

    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)

    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {

        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        mPlaybackState = playbackState
        when (playbackState) {
            PlaybackState.PLAYING -> {
                val itemChange = playlistManager?.currentItemChange
                if (itemChange != null) {
                    onPlaylistItemChanged(
                            itemChange.currentItem,
                            itemChange.hasNext,
                            itemChange.hasPrevious
                    )
                }
                binding?.fab?.visibility = View.GONE
                binding?.gifProgress?.visibility = View.VISIBLE
                binding?.gifProgress?.playAnimation()
                binding?.gifProgress?.bringToFront()
                setPlayPause()
            }
            PlaybackState.ERROR -> {
            }
            PlaybackState.PAUSED -> {
                binding?.gifProgress?.visibility = View.GONE
                binding?.fab?.visibility = View.VISIBLE
                binding?.gifProgress?.pauseAnimation()
                setPlayPause()
            }
            PlaybackState.RETRIEVING -> {

            }
            PlaybackState.PREPARING -> {

            }
            PlaybackState.SEEKING -> {

            }
            PlaybackState.STOPPED -> {

            }
        }
        return true
    }
}