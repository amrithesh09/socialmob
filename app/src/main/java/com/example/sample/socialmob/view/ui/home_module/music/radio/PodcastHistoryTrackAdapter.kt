package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PodcastHistoryTrackItemBinding
import com.example.sample.socialmob.databinding.PodcastHistoryTrackItemInnerBinding
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.music.PodcastHistoryTrack
import kotlinx.android.synthetic.main.options_menu_radio.view.*

class PodcastHistoryTrackAdapter(
    private val onPodcastHistoryTrackPlayListItemClick: PodcastHistoryTrackPlayListItemClickListener,
    private val mContext: androidx.fragment.app.FragmentActivity?,
    private val isFromRadioHome: Boolean
) :
    ListAdapter<PodcastHistoryTrack, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PodcastHistoryTrack>() {
                override fun areItemsTheSame(
                    oldItem: PodcastHistoryTrack,
                    newItem: PodcastHistoryTrack
                ): Boolean {
                    return oldItem.isPlaying == newItem.isPlaying
                }

                override fun areContentsTheSame(
                    oldItem: PodcastHistoryTrack,
                    newItem: PodcastHistoryTrack
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Any> {
        return if (isFromRadioHome) {
            val binding: PodcastHistoryTrackItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.podcast_history_track_item,
                parent,
                false
            )
            RecommendedViewHolder(binding)
        } else {

            val binding: PodcastHistoryTrackItemInnerBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.podcast_history_track_item_inner,
                parent,
                false
            )
            RecommendedViewHolderInner(binding)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))

    }


    inner class RecommendedViewHolder(val binding: PodcastHistoryTrackItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            binding.trackViewModel = getItem(adapterPosition)
            binding.executePendingBindings()


            if (getItem(adapterPosition)?.isPlaying == "true") {
                binding.itemPlayImageView.visibility = View.VISIBLE
                binding.trackGifProgress.visibility = View.VISIBLE
                binding.trackGifProgress.playAnimation()
            } else {
                binding.itemPlayImageView.visibility = View.INVISIBLE
                binding.trackGifProgress.visibility = View.INVISIBLE
                binding.trackGifProgress.pauseAnimation()
            }


            binding.topLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onPodcastHistoryTrackPlayListItemClick.onPodcastHistoryTrackPlayListItemClick(
                        adapterPosition,
                        getItem(adapterPosition)?.ThumbnailPath!!, "RadioRecommended",
                        getItem(adapterPosition)?.mExtraId!!
                    )
                }
            }

            binding.trackOptions.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    val viewGroup: ViewGroup? = null
                    val view: View =
                        LayoutInflater.from(mContext)
                            .inflate(R.layout.options_menu_radio, viewGroup)
                    val mQQPop = PopupWindow(
                        view,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    mQQPop.animationStyle = R.style.RightTopPopAnim
                    mQQPop.isFocusable = true
                    mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    mQQPop.isOutsideTouchable = true
                    mQQPop.showAsDropDown(binding.trackOptions, -220, -20)


                    view.share_episode.setOnClickListener {
                        if (adapterPosition != RecyclerView.NO_POSITION) {

                            ShareAppUtils.sharePodcastEpisode(
                                getItem(adapterPosition)?.mExtraId!!,
                                mContext,
                                getItem(adapterPosition)?.PodcastName?.replace("\\W+", " ")?.replace(" ", "-")!!
                            )
                            mQQPop.dismiss()
                        }
                    }
                }
            }
        }
    }

    inner class RecommendedViewHolderInner(val binding: PodcastHistoryTrackItemInnerBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            binding.trackViewModel = getItem(adapterPosition)
            binding.executePendingBindings()



            if (getItem(adapterPosition)?.isPlaying == "true") {
                binding.itemPlayImageView.visibility = View.VISIBLE
                binding.trackGifProgress.visibility = View.VISIBLE
                binding.trackGifProgress.playAnimation()
            } else {
                binding.itemPlayImageView.visibility = View.INVISIBLE
                binding.trackGifProgress.visibility = View.INVISIBLE
                binding.trackGifProgress.pauseAnimation()
            }


            binding.topLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onPodcastHistoryTrackPlayListItemClick.onPodcastHistoryTrackPlayListItemClick(
                        adapterPosition,
                        getItem(adapterPosition)?.ThumbnailPath!!, "RadioHistory",
                        getItem(adapterPosition)?.EpisodeId!!
                    )
                }
            }

            binding.trackOptions.visibility=View.GONE
//            binding.trackOptions.setOnClickListener {
//                if (adapterPosition != RecyclerView.NO_POSITION) {
//                    val viewGroup: ViewGroup? = null
//                    val view: View =
//                        LayoutInflater.from(mContext)
//                            .inflate(R.layout.options_menu_radio, viewGroup)
//                    val mQQPop = PopupWindow(
//                        view,
//                        ViewGroup.LayoutParams.WRAP_CONTENT,
//                        ViewGroup.LayoutParams.WRAP_CONTENT
//                    )
//                    mQQPop.animationStyle = R.style.RightTopPopAnim
//                    mQQPop.isFocusable = true
//                    mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                    mQQPop.isOutsideTouchable = true
//                    mQQPop.showAsDropDown(binding.trackOptions, -220, -20)
//
//
//                    view.share_episode.setOnClickListener {
//                        if (adapterPosition != RecyclerView.NO_POSITION) {
//                            ShareAppUtils.sharePodcastEpisode(
//                                getItem(adapterPosition)?.EpisodeId!!,
//                                mContext,
//                                getItem(adapterPosition)?.PodcastName?.replace("\\W+", " ")?.replace(" ", "-")!!
//                            )
//                            mQQPop.dismiss()
//                        }
//                    }
//                }
//            }

        }
    }

    interface PodcastHistoryTrackPlayListItemClickListener {
        fun onPodcastHistoryTrackPlayListItemClick(
            itemId: Int, itemImage: String, isFrom: String, podcastId: String
        )
    }
}
