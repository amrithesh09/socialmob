package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.RadioMainFragmentBinding
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.FancySwitch
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.events.EventDismissRadioDialog
import com.example.sample.socialmob.view.utils.events.EventMusicPlayError
import com.example.sample.socialmob.view.utils.events.PlayRadioEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaProgress
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.listener.ProgressListener
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

@AndroidEntryPoint
class RadioMainFragment : Fragment(), PlaylistListener<MediaItem>, ProgressListener {
    private var mIsVisibleToUser = false
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        mIsVisibleToUser = isVisibleToUser
    }

    private val viewModel: MusicDashBoardViewModel by viewModels()

    private var binding: RadioMainFragmentBinding? = null
    private var mediaItems = LinkedList<MediaItem>()
    private var playlistManager: PlaylistManager? = null
    private var PLAYLIST_ID = 401 //Arbitrary, for the example
    private var isFailure: Boolean = false
    private var isPlayingAudio: Boolean = true
    private var isFrom: Boolean = false
    private var customProgressDialog: CustomProgressDialog? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_no_animation)
        binding?.radioWaveAnimation?.pauseAnimation()
        customProgressDialog = CustomProgressDialog(requireActivity())

        binding?.fsSmile?.setSwitchStateChangedListener(object :
            FancySwitch.SwitchStateChangedListener {
            override fun onChanged(newState: FancySwitch.State) {
                when (newState) {
                    FancySwitch.State.ON -> {
                        if (!isFailure) {
                            isPlayingAudio = true
                            binding?.radioPlayerPlayPause?.post {
                                binding?.radioPlayerPlayPause?.performClick()
                            }
                            binding?.radioBg?.playAnimation()
                            binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_animation)
                            binding?.radioWaveAnimation?.playAnimation()
                            binding?.radioBg?.visibility = View.VISIBLE
                            binding?.radioWaveAnimation?.visibility = View.VISIBLE
                            isFrom = true
                        } else {
                            MyApplication.playlistManager?.playlistHandler?.pause(true)
                            AppUtils.showCommonToast(
                                activity!!,
                                "Oops! The song could not be played"
                            )
                        }
                    }
                    FancySwitch.State.OFF -> {
                        isFrom = true
                        isPlayingAudio = false
                        binding?.radioPlayerPlayPause?.post {
                            binding?.radioPlayerPlayPause?.performClick()
                        }
                        binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_no_animation)
                        binding?.radioBg?.visibility = View.GONE
                        binding?.radioWaveAnimation?.visibility = View.VISIBLE
                    }
                }
            }
        })

        binding?.nowPlayingBackImageView?.setOnClickListener {
            if (activity is CommonFragmentActivity) {
                (activity as CommonFragmentActivity).finish()
            }
        }
//        binding?.radioPlayerPlayPause?.setOnClickListener {
//            // TODO : Check Internet connection
//            if (InternetUtil.isInternetOn()) {
//                if (!isFailure) {
//                    customProgressDialog?.show()
//                    pauseMusic()
//                    radioPlay()
//                } else {
//                    AppUtils.showCommonToast(
//                        requireActivity(),
//                        "Oops! The song could not be played"
//                    )
//                }
//            }
//        }
        // TODO : Double TAP
        var isTaped = false
        val gd = GestureDetector(activity, object : GestureDetector.SimpleOnGestureListener() {
            override fun onDoubleTap(e: MotionEvent): Boolean {
                // TODO : Check Internet connection
                if (InternetUtil.isInternetOn()) {
                    if (!isFailure) {
                        if (isPlayingAudio) {
                            binding?.touchToTurnOnTextView?.text =
                                activity?.getString(R.string.touch_to_turn_on)
                            isFrom = true
                            isPlayingAudio = false
//                            binding?.radioPlayerPlayPause?.post {
//                                binding?.radioPlayerPlayPause?.performClick()
//                            }
                            binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_no_animation)
                            binding?.radioBg?.visibility = View.GONE
                            binding?.radioWaveAnimation?.visibility = View.VISIBLE
//                            if (activity is HomeActivity) {
//                                (activity as HomeActivity).stopMusicPlayFromRadio()
//                            }
                            playlistManager?.invokePausePlay()
                            isTaped = true
                            val handler = Handler(Looper.getMainLooper())
                            handler.postDelayed({
                                isTaped = false
                            }, 1000)

                        } else {
                            binding?.touchToTurnOnTextView?.text =
                                activity?.getString(R.string.touch_to_turn_off)

                            isPlayingAudio = true
//                            binding?.radioPlayerPlayPause?.post {
//                                binding?.radioPlayerPlayPause?.performClick()
//                            }
                            playlistManager?.invokePausePlay()
                            binding?.radioBg?.playAnimation()
                            binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_animation)
                            binding?.radioWaveAnimation?.playAnimation()
                            binding?.radioBg?.visibility = View.VISIBLE
                            binding?.radioWaveAnimation?.visibility = View.VISIBLE

                            isFrom = true
                        }
                    } else {
                        AppUtils.showCommonToast(activity!!, "Oops! The song could not be played")
                    }
                }
                return true
            }
        })
        binding?.touchToTurnOnTextView?.setOnTouchListener { v, event ->
            if (!isTaped) {
                gd.onTouchEvent(event)
            }
            true
        }

//         TODO : To check playlist already added
        setupPlaylistManager()

    }

    private fun radioPlay() {
        setupListeners()
        // TODO : To check playlist already added
        setupPlaylistManager()
        startPlayback(true)
        SharedPrefsUtils.setStringPreference(activity, AppUtils.SELECTED_CATEGORY, "Radio")
    }

    /**
     * Links the SeekBarChanged to the [.seekBar] and
     * onClickListeners to the media buttons that call the appropriate
     * invoke methods in the [.playlistManagerRadio]
     */
    private fun setupListeners() {
//        binding?.radioPlayerPlayPause?.setOnClickListener {
//            pauseMusic()
//            val currentPlaybackState = playlistManager?.currentPlaybackState
//            if (currentPlaybackState == PlaybackState.STOPPED) {
//                setupPlaylistManager()
//            } else {
//                playlistManager?.invokePausePlay()
//            }
//        }
    }

    private fun pauseMusic() {
//        if (activity != null && activity is HomeActivity &&
//            (activity as HomeActivity).playlistManager != null &&
//            (activity as HomeActivity).playlistManager?.playlistHandler != null &&
//            (activity as HomeActivity).playlistManager?.playlistHandler?.currentMediaPlayer != null &&
//            (activity as HomeActivity).playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
//        ) {
//            if ((activity as HomeActivity).playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
//                (activity as HomeActivity).playlistManager?.invokePausePlay()
//            }
//        }
    }


    /**
     * Retrieves the playlist instance and performs any generation
     * of content if it hasn't already been performed.
     *
     * @return True if the content was generated
     */
    private fun setupPlaylistManager() {
        viewModel.radioModel.nonNull().observe(viewLifecycleOwner, { mStationDetails ->
//            for (sample in mStationDetails!!) {
//                val mediaItem = MediaItem(sample, true)
//                mediaItems.add(mediaItem)
//            }
            if (mStationDetails.isNotEmpty()) {
                viewModel.radioStationName = mStationDetails[0].name

                binding?.stationNameView?.text = mStationDetails[0].name
                binding?.executePendingBindings()
            }
//            playlistManager?.setParameters(mediaItems, 0)
//            playlistManager?.id = PLAYLIST_ID.toLong()

//            startPlayback(true)
            viewModel.radioModel.removeObservers(this)
        })
    }

    /**
     * Starts the audio playback if necessary.
     *
     * @param forceStart True if the audio should be started from the beginning even if it is currently playing
     */
    private fun startPlayback(forceStart: Boolean) {
        // TODO : Stop if Other service is running
        //If we are changing audio files, or we haven't played before then start the playback
        if (forceStart || playlistManager?.currentPosition != 0) {
            playlistManager?.currentPosition = 0
            playlistManager?.play(0, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.radio_main_fragment, container, false)
        return binding?.root
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.STOPPED -> {
                println("///STOPPED")
                updatePlayPauseImage(false)
                if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                    val serviceClass1 = MediaServiceRadio::class.java
                    val intent11 = Intent(activity, serviceClass1)
                    requireActivity().stopService(intent11)
                }
            }
            PlaybackState.RETRIEVING, PlaybackState.PREPARING, PlaybackState.SEEKING -> {
                restartLoading()
            }
            PlaybackState.PLAYING -> {
                if (activity is HomeActivity) {
                    (activity as HomeActivity).stopMusicPlayFromRadio()
                }
                doneLoading(true)
                isPlayingAudio = true
            }
            PlaybackState.PAUSED -> {
                doneLoading(false)

            }
            else -> {
            }
        }

        return true
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager =
            requireActivity().getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return true
    }

    /**
     * Used to inform the controls to return to the loading stage.
     * This is the opposite of [.loadCompleted]
     */
    private fun restartLoading() {
        binding?.radioPlayerPlayPause?.visibility = View.INVISIBLE
    }

    /**
     * Called when we receive a notification that the current item is
     * done loading.  This will then update the view visibilities and
     * states accordingly.
     *
     * @param isPlaying True if the audio item is currently playing
     */
    private fun doneLoading(isPlaying: Boolean) {
        // TODO : Adding playing category to check already added
        loadCompleted()
        updatePlayPauseImage(isPlaying)
    }

    /**
     * Used to inform the controls to finalize their setup.  This
     * means replacing the loading animation with the PlayPause button
     */
    private fun loadCompleted() {
        binding?.radioPlayerPlayPause?.visibility = View.INVISIBLE
        if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
            customProgressDialog?.dismiss()
        }
    }

    /**
     * Updates the Play/Pause image to represent the correct playback state
     *
     * @param isPlaying True if the audio item is currently playing
     */
    private fun updatePlayPauseImage(isPlaying: Boolean) {
        isPlayingAudio = isPlaying
        SharedPrefsUtils.setStringPreference(activity, AppUtils.SELECTED_CATEGORY, "Radio")
        if (isPlaying) {
            binding?.radioBg?.playAnimation()
            binding?.touchToTurnOnTextView?.text = activity?.getString(R.string.touch_to_turn_off)
            binding?.touchToTurnOnTextView?.bringToFront()
            binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_animation)
            binding?.radioWaveAnimation?.playAnimation()
            binding?.radioBg?.visibility = View.VISIBLE
            binding?.radioWaveAnimation?.visibility = View.VISIBLE
            binding?.fsSmile?.post {
                binding?.fsSmile?.setState(FancySwitch.State.ON)
            }
            binding?.stationName?.setTextColor(Color.parseColor("#FFFFFF"))
        } else {
            binding?.fsSmile?.post {
                binding?.fsSmile?.setState(FancySwitch.State.OFF)

            }
            binding?.touchToTurnOnTextView?.text = activity?.getString(R.string.touch_to_turn_on)
            binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_no_animation)
            binding?.radioBg?.visibility = View.GONE
            binding?.radioWaveAnimation?.visibility = View.VISIBLE
            binding?.stationName?.setTextColor(Color.parseColor("#50FFFFFF"))

        }
    }

    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
        playlistManager?.unRegisterProgressListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)
        playlistManager?.registerProgressListener(this)

        //Makes sure to retrieve the current playback information
        updateCurrentPlaybackInformation()
    }


    /**
     * Makes sure to update the UI to the current playback item.
     */
    private fun updateCurrentPlaybackInformation() {
        val itemChange = playlistManager?.currentItemChange
        if (itemChange != null) {
            onPlaylistItemChanged(
                itemChange.currentItem,
                itemChange.hasNext,
                itemChange.hasPrevious
            )
        }

        val currentPlaybackState = playlistManager?.currentPlaybackState
        if (currentPlaybackState !== PlaybackState.STOPPED) {
            onPlaybackStateChanged(currentPlaybackState!!)
        }

        val mediaProgress = playlistManager?.currentProgress
        if (mediaProgress != null) {
            onProgressUpdated(mediaProgress)
        }
    }

    override fun onProgressUpdated(mediaProgress: MediaProgress): Boolean {

        return true
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventMusicPlayError) {
        if (activity != null) {
            AppUtils.showCommonToast(requireActivity(), "Oops! The song could not be played")
            isFrom = true
            isPlayingAudio = false
            binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_no_animation)
            binding?.radioBg?.visibility = View.GONE
            binding?.radioWaveAnimation?.visibility = View.VISIBLE
        }
        loadCompleted()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventDismissRadioDialog) {
        println("" + event)
        loadCompleted()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: PlayRadioEvent) {
        if (event.mIsNeedToPlay) {
            if (!isPlayingAudio) {
                if (binding != null) {

                    isPlayingAudio = true
                    binding?.radioPlayerPlayPause?.post {
                        binding?.radioPlayerPlayPause?.performClick()
                    }
                    binding?.radioBg?.playAnimation()
                    binding?.radioWaveAnimation?.setAnimation(R.raw.radio_wave_animation)
                    binding?.radioWaveAnimation?.playAnimation()
                    binding?.radioBg?.visibility = View.VISIBLE
                    binding?.radioWaveAnimation?.visibility = View.VISIBLE
                }

//                binding?.radioPlayerPlayPause?.setOnClickListener {
//                    // TODO : Check Internet connection
//                    if (InternetUtil.isInternetOn()) {
//                        if (!isFailure) {
//                            pauseMusic()
//                            radioPlay()
//                        } else {
//                            AppUtils.showCommonToast(
//                                requireActivity(),
//                                "Oops! The song could not be played"
//                            )
//                        }
//                    }
//                }
            }
        } else {
            updatePlayPauseImage(false)
        }
    }
}
