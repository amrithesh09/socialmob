package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.RadioFragmentBinding
import com.example.sample.socialmob.model.music.FeaturedPodcast
import com.example.sample.socialmob.model.music.PodcastHistoryTrack
import com.example.sample.socialmob.model.music.PodcastMeta
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.PodCastAllActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.RadioPodCastAdapter
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PodCastProgress
import com.example.sample.socialmob.viewmodel.music_menu.MusicDashBoardViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class RadioPodCastFragment : Fragment(), RadioPodCastAdapter.QuickPlayListAdapterItemClickListener,
    RadioPodCastAdapter.PodCastAllClick, RadioRecommendedAdapter.RadioRecommendedItemClickListener,
    PodcastHistoryTrackAdapter.PodcastHistoryTrackPlayListItemClickListener,
    RadioRecommendedAdapter.OptionDialog {
    @Inject
    lateinit var glideRequestManager: RequestManager

    companion object {
        var audioSessionId = -1
    }

    private var mPosHistoryTrack: Int = -1
    private var mPodCastId: String = ""
    private var mSelectedPlayListName: String = ""
    private var isCalledCreatePlayList = false
    private var PLAYLISTID = 4 //Arbitrary, for the example

    private val viewModel: MusicDashBoardViewModel by viewModels()
    private var binding: RadioFragmentBinding? = null
    private var mRadioPodCastAdapter: RadioPodCastAdapter? = null
    private var mPodCastHistoryAdapter: PodcastHistoryTrackAdapter? = null
    private var mRadioPodCastRecommendAdapter: RadioRecommendedAdapter? = null

    private var recommendedTrackModelList: MutableList<RadioPodCastEpisode>? = ArrayList()
    private var podCastHistoryTrackTrackModelList: MutableList<PodcastHistoryTrack>? = ArrayList()
    private var mTrackProgressList: List<PodCastProgress>? = ArrayList()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (recommendedTrackModelList?.size == 0) {
            apiCommon()
        }
        observePodHistory()

        MobileAds.initialize(activity)
        val adRequest =
            AdRequest.Builder()
                .addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0")
                .build()

        binding?.adViewRecommencedPodcast?.loadAd(adRequest)

        mPodCastHistoryAdapter = PodcastHistoryTrackAdapter(
            onPodcastHistoryTrackPlayListItemClick = this,
            mContext = activity,
            isFromRadioHome = true
        )
        mRadioPodCastRecommendAdapter = RadioRecommendedAdapter(
            this, this
        )
        mRadioPodCastAdapter = RadioPodCastAdapter(activity, this, this)

        binding?.goToOfflineMode?.setOnClickListener {
            if (activity != null) {
                (activity as HomeActivity).goToOffline()
            }
        }

        binding?.podcastHistoryViewAll?.setOnClickListener {
            val intent = Intent(activity, PodcastHistoryActivity::class.java)
            startActivity(intent)
        }
        binding?.allPodcastRelative?.setOnClickListener {
            onPodCastAllItemClick()
        }
        binding?.refreshTextView?.setOnClickListener {
            podCastMetaApi()
        }

        binding?.leftUpForRecyclerView?.adapter = mPodCastHistoryAdapter
        binding?.recommendedEpisodeRecyclerView?.adapter = mRadioPodCastRecommendAdapter
        binding?.podcastRecyclerView?.adapter = mRadioPodCastAdapter
    }

    private fun apiCommon() {

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            viewModel.podcastsMetaLiveData?.postValue(ResultResponse.noInternet(PodcastMeta()))
        } else {
            val mAuthPodcast =
                RemoteConstant.getAuthWithoutOffsetAndCount(
                    requireActivity(),
                    RemoteConstant.pathPodCastMeta
                )
            viewModel.podCastMeta(mAuthPodcast)
        }
        observePodcastMetaApi()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.radio_fragment, container, false)
        binding?.lifecycleOwner = this

        binding?.radioModel = viewModel
        return binding?.root
    }

    private fun podCastMetaApi() {
        if (!InternetUtil.isInternetOn()) {
            viewModel.podcastsMetaLiveData?.postValue(ResultResponse.noInternet(PodcastMeta()))
        } else {
            val mAuthPodCast =
                RemoteConstant.getAuthWithoutOffsetAndCount(
                    requireActivity(),
                    RemoteConstant.pathPodCastMeta
                )
            viewModel.podCastMeta(mAuthPodCast)
        }

    }

    private fun observePodcastMetaApi() {

        viewModel.podcastsMetaLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                if (resultResponse != null && resultResponse.status == ResultResponse.Status.SUCCESS) {


                    binding?.progressLinear?.visibility = View.GONE
                    binding?.dataLinear?.visibility = View.VISIBLE
                    binding?.noInternetLinear?.visibility = View.GONE

                    val mResultPayload = resultResponse.data?.payload

                    if (mResultPayload?.recommendedEpisodes?.isNotEmpty()!!) {
                        recommendedTrackModelList =
                            mResultPayload.recommendedEpisodes.toMutableList()
                        mRadioPodCastRecommendAdapter?.submitList(recommendedTrackModelList)
                    }

                    binding?.allPodcastTextView?.text =
                        resultResponse.data.payload.allPodcastImage?.text
                    glideRequestManager
                        .load(mResultPayload.allPodcastImage?.coverImage)
                        .apply(
                            RequestOptions.placeholderOf(0).error(0)
                                .error(0)
                                .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
                        )
                        .into(binding?.allPodcastImageView!!)


                    if (mResultPayload.featuredPodcasts != null && mResultPayload.featuredPodcasts.isNotEmpty()) {
                        // TODO : Featured Podcast
                        binding?.viewPager?.adapter = FeaturedPodCastAdapter(
                            mResultPayload.featuredPodcasts,
                            requireActivity(), this, glideRequestManager
                        )

                    }

                    if (mResultPayload.topPodcasts?.isNotEmpty()!!) {
                        // TODO : Add view all
                        val playListList: List<FeaturedPodcast> = mResultPayload.topPodcasts
                        val podcastSubItems: List<FeaturedPodcast> = playListList
                        mRadioPodCastAdapter?.submitList(podcastSubItems)
                        viewModel.podCastModelLiveData?.removeObservers(this)
                    }

                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.ERROR) {
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.dataLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.VISIBLE
                    glideRequestManager.load(R.drawable.server_error).apply(
                        RequestOptions().fitCenter())
                        .into(binding?.noInternetImageView!!)
                    binding?.noInternetTextView?.text =
                        getString(R.string.server_error)
                    binding?.noInternetSecTextView?.text =
                        getString(R.string.server_error_content)
                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.LOADING) {
                    binding?.progressLinear?.visibility = View.VISIBLE
                    binding?.dataLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.NO_INTERNET) {
                    glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                        .into(binding?.noInternetImageView!!)
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.dataLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.VISIBLE
                    val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                    binding?.noInternetLinear?.startAnimation(shake) // starts animation
                }
            })


    }

    private fun observePodHistory() {

        viewModel.mdbTrackProgressTrackList?.nonNull()?.observe(viewLifecycleOwner, {
            if (it != null) {
                mTrackProgressList = it
            }
        })


        viewModel.podcastHistoryModel.nonNull().observe(viewLifecycleOwner, {
            if (it.size > 3) {
                binding?.podcastHistoryViewAll?.visibility = View.VISIBLE
            } else {
                binding?.podcastHistoryViewAll?.visibility = View.GONE
            }

        })
        viewModel.podcastHistoryModelLastListen.nonNull().observe(viewLifecycleOwner, {

            if (it != null && it.isNotEmpty()) {

                val mLocalList = it as MutableList<PodcastHistoryTrack>
                podCastHistoryTrackTrackModelList = mLocalList

                mPodCastHistoryAdapter?.submitList(podCastHistoryTrackTrackModelList)
                mPodCastHistoryAdapter?.notifyDataSetChanged()
                binding?.historyPodcast?.visibility = View.VISIBLE


                if (MyApplication.playlistManager != null
                    && MyApplication.playlistManager?.playlistHandler != null
                    && MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null
                    && MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
                    && MyApplication.playlistManager?.currentItem != null
                    && MyApplication.playlistManager?.currentItem?.mExtraId != null
                ) {
                    if (MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                        val mTrackId =
                            MyApplication.playlistManager?.currentItemChange?.currentItem?.mExtraId.toString()
                        updatePLaying(mTrackId)
                    }
                }

            } else {
                binding?.historyPodcast?.visibility = View.GONE
            }

        })

    }

    override fun onPlayListItemClick(item: String, mTitle: String, imageUrl: String, mDescription: String) {

        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", item)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", imageUrl)
        intent.putExtra("mDescription", mDescription)
        intent.putExtra("mIsFrom", "fromRadio")
        startActivity(intent)
    }

    private var mLastClickTime: Long = 0


    override fun onRadioRecommendedItemClick(itemId: Int, itemImage: String, isFrom: String) {
        // TODO : Do not comment since we need to add Analytics ( Radio Podcast )
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            var mMillisecondPLay = ""
            mSelectedPlayListName = SharedPrefsUtils.getStringPreference(
                activity,
                RemoteConstant.SELECTEDPLAYLISTNAME, ""
            )!!

            mPodCastId = recommendedTrackModelList?.get(itemId)?._id!!

            mTrackProgressList?.map {
                if (it.episodeId == recommendedTrackModelList?.get(itemId)?._id) {
                    mMillisecondPLay = it.mMillisecond
                }
            }
            viewModel.createDBPlayList(
                recommendedTrackModelList, "Recommended episodes"
            ) { listItem ->
                val mediaItems = LinkedList<MediaItem>()
                for (i in listItem.indices) {
                    val mediaItem = MediaItem(listItem[i], true)
                    mediaItems.add(mediaItem)
                }
                launchMusic(
                    itemId, "Recommended Episodes", mediaItems, mMillisecondPLay
                )
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }


    private fun launchMusic(
        itemId: Int, mPlayList: String, dbPlayList: LinkedList<MediaItem>, mMillisecondPLay: String
    ) {

        MyApplication.playlistManager?.setParameters(dbPlayList, itemId)
        MyApplication.playlistManager?.id = PLAYLISTID.toLong()
        MyApplication.playlistManager?.currentPosition = itemId
        if (mMillisecondPLay != null && mMillisecondPLay != "") {
            MyApplication.playlistManager?.play(mMillisecondPLay.toLong(), false)
        } else {
            MyApplication.playlistManager?.play(0, false)
        }

        mSelectedPlayListName = mPlayList
        SharedPrefsUtils.setStringPreference(
            activity,
            RemoteConstant.SELECTEDPLAYLISTNAME, "RadioRecommended"
        )
    }


    private fun updatePLaying(mTrackId: String?) {
        recommendedTrackModelList?.map {
            if (it._id == mTrackId) {
                it.isPlaying = "true"
            } else {
                it.isPlaying = "false"
            }
        }

        if (recommendedTrackModelList?.isNotEmpty()!!) {
            mRadioPodCastRecommendAdapter?.submitList(recommendedTrackModelList)
            mRadioPodCastRecommendAdapter?.notifyDataSetChanged()
        }

        if (podCastHistoryTrackTrackModelList != null && podCastHistoryTrackTrackModelList?.size!! > 0) {
            podCastHistoryTrackTrackModelList?.map {
                if (it.mExtraId == mTrackId) {
                    it.isPlaying = "true"
                } else {
                    it.isPlaying = "false"
                }
            }
            mPodCastHistoryAdapter?.submitList(podCastHistoryTrackTrackModelList)
            mPodCastHistoryAdapter?.notifyDataSetChanged()
        }


    }

    override fun onPodCastAllItemClick() {
        val intent = Intent(activity, PodCastAllActivity::class.java)
        startActivity(intent)
    }

    override fun onPodcastHistoryTrackPlayListItemClick(
        itemId: Int, itemImage: String, isFrom: String, podcastId: String
    ) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            var mMillisecondPLay = ""
            mPodCastId = podcastId

            SharedPrefsUtils.setStringPreference(
                activity,
                RemoteConstant.SELECTEDPLAYLISTNAME, "PodcastHistoryTrack"
            )

            mSelectedPlayListName = SharedPrefsUtils.getStringPreference(
                activity,
                RemoteConstant.SELECTEDPLAYLISTNAME, ""
            )!!

            if (podCastHistoryTrackTrackModelList != null &&
                podCastHistoryTrackTrackModelList?.size!! > 0 &&
                mPosHistoryTrack != -1
            ) {
                podCastHistoryTrackTrackModelList?.get(
                    mPosHistoryTrack
                )
                    ?.isPlaying = "false"
                mPodCastHistoryAdapter?.notifyItemChanged(
                    mPosHistoryTrack
                )
            }
            podCastHistoryTrackTrackModelList?.map {
                if (it.mExtraId == podcastId) {
                    it.isPlaying = "true"
                    mMillisecondPLay = it.Millisecond!!
                } else {
                    it.isPlaying = "false"
                }
            }
            if (mPosHistoryTrack != -1) {
                mPodCastHistoryAdapter?.notifyItemChanged(
                    mPosHistoryTrack
                )
            }

            viewModel.createDBPodCastHistoryTrackPlayList(
                podCastHistoryTrackTrackModelList, "Podcast History"
            ) { listItem ->
                val mediaItems = LinkedList<MediaItem>()
                for (i in listItem.indices) {
                    val mediaItem = MediaItem(listItem[i], true)
                    mediaItems.add(mediaItem)
                }
                launchMusicHistory(
                    itemId, "Podcast History",
                    mediaItems, mMillisecondPLay
                )
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    private fun launchMusicHistory(
        itemId: Int,
        mPlayList: String, mediaItems: LinkedList<MediaItem>, mMillisecondPLay: String
    ) {

        MyApplication.playlistManager?.setParameters(mediaItems, itemId)
        MyApplication.playlistManager?.id = PLAYLISTID.toLong()
        MyApplication.playlistManager?.currentPosition = itemId
        if (mMillisecondPLay != null && mMillisecondPLay != "" && mMillisecondPLay != "null") {
            MyApplication.playlistManager?.play(mMillisecondPLay.toLong(), false)
        } else {
            MyApplication.playlistManager?.play(0, false)
        }

        mSelectedPlayListName = mPlayList

        SharedPrefsUtils.setStringPreference(
            activity,
            RemoteConstant.SELECTEDPLAYLISTNAME, "PodcastHistoryTrack"
        )

    }


    override fun optionDialog(_id: String, podcast_name: String) {
        val viewGroup: ViewGroup? = null
        val mBottomSheetDialog = RoundedBottomSheetDialog(requireActivity())
        val sheetView: View =
            layoutInflater.inflate(R.layout.play_list_options, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val editLinear: LinearLayout = sheetView.findViewById(R.id.edit_linear)
        val deleteLinear: LinearLayout = sheetView.findViewById(R.id.delete_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)

        editLinear.visibility = View.GONE
        deleteLinear.visibility = View.GONE

        shareLinear.setOnClickListener {
            mBottomSheetDialog.dismiss()
            try {
                ShareAppUtils.sharePodcastEpisode(
                    _id,
                    activity,
                    podcast_name.replace("\\W+", " ").replace(" ", "-")
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    fun updatePlayPause(mTrackId: String) {
        if (MyApplication.playlistManager != null
            && MyApplication.playlistManager?.playlistHandler != null
            && MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null
            && MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
        ) {
            if (MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {

                updatePLaying(mTrackId)
            } else {
                updatePause()
            }
        }
    }


    private fun updatePause() {

        recommendedTrackModelList?.map {
            if (it.isPlaying == "true") {
                it.isPlaying = "false"
            }
        }
        if (recommendedTrackModelList?.isNotEmpty()!!) {
            mRadioPodCastRecommendAdapter?.notifyDataSetChanged()
        }

        if (podCastHistoryTrackTrackModelList != null && podCastHistoryTrackTrackModelList?.size!! > 0) {
            podCastHistoryTrackTrackModelList?.map {
                if (it.isPlaying == "true") {
                    it.isPlaying = "false"
                }
            }

            mRadioPodCastRecommendAdapter?.notifyDataSetChanged()
        }

    }


}

