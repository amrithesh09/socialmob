package com.example.sample.socialmob.view.ui.home_module.music.radio

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.RadioRecommendedTrackItemBinding
import com.example.sample.socialmob.model.music.RadioPodCastEpisode

class RadioRecommendedAdapter(
    private val radioRecommendedItemClickListener: RadioRecommendedItemClickListener,
    private val optionDialog: OptionDialog?

) :
    ListAdapter<RadioPodCastEpisode, RadioRecommendedAdapter.RecommendedViewHolder>(DIFF_CALLBACK) {


    companion object {
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<RadioPodCastEpisode>() {
                override fun areItemsTheSame(
                    oldItem: RadioPodCastEpisode,
                    newItem: RadioPodCastEpisode
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: RadioPodCastEpisode,
                    newItem: RadioPodCastEpisode
                ): Boolean {
                    return oldItem.isPlaying?.equals(newItem.isPlaying) ?: false
                }
            }
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecommendedViewHolder {
        val binding: RadioRecommendedTrackItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.radio_recommended_track_item, parent,
            false
        )
        return RecommendedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecommendedViewHolder, position: Int) {
        holder.binding.trackViewModel = getItem(holder.adapterPosition)
        holder.binding.executePendingBindings()

        if (getItem(holder.adapterPosition).isPlaying == "true") {
            holder.binding.itemPlayImageView.visibility = View.VISIBLE
            holder.binding.trackGifProgress.visibility = View.VISIBLE
            holder.binding.trackGifProgress.playAnimation()
        } else {
            holder.binding.itemPlayImageView.visibility = View.INVISIBLE
            holder.binding.trackGifProgress.visibility = View.INVISIBLE
            holder.binding.trackGifProgress.pauseAnimation()
        }

        holder.binding.podcastCardView.setOnClickListener {
            if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                radioRecommendedItemClickListener.onRadioRecommendedItemClick(
                    holder.adapterPosition,
                    getItem(holder.adapterPosition).cover_image!!, "RadioRecommended"
                )
            }
        }
//        holder.binding.podcastNameTextView.setOnClickListener {
//            holder.binding.podcastCardView.performClick()
//        }
//        holder.binding.episodeNameTextView.setOnClickListener {
//            holder.binding.podcastCardView.performClick()
//        }

        holder.binding.trackOptions.setOnClickListener {
            if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                optionDialog?.optionDialog(
                    getItem(holder.adapterPosition)._id!!,
                    getItem(holder.adapterPosition).podcast_name!!
                )
            }
        }

    }

    class RecommendedViewHolder(val binding: RadioRecommendedTrackItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface RadioRecommendedItemClickListener {
        fun onRadioRecommendedItemClick(
            itemId: Int,
            itemImage: String,
            isFrom: String
        )
    }

    interface OptionDialog {
        fun optionDialog(
            _id: String, podcast_name: String
        )
    }
}