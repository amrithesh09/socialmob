package com.example.sample.socialmob.view.ui.home_module.music_video

import android.os.Bundle
import com.example.sample.socialmob.databinding.ReelsVideoBinding
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ReelsVideoActivity : BaseCommonActivity() {
    lateinit var binding: ReelsVideoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ReelsVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        val items: MutableList<VideoItem>? = ArrayList()
//        var adapter = MusicVideoViewPagerAdapter()
//        binding.musicViewPager.adapter = adapter
//        items?.add(VideoItem("https://hellotestvideo.s3-ap-southeast-1.amazonaws.com/aspect_video/1-1_video_aspect.mp4"))
//        adapter.submitList(items)
    }


}