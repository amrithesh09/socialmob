package com.example.sample.socialmob.view.ui.home_module.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.FutureTarget
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.backlog.articles.ArticleDetailsActivityNew
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedTextDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.NotificationHelper
import com.example.sample.socialmob.view.utils.events.NotificationEvent
import com.example.sample.socialmob.view.utils.events.UpdateConversationEvent
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * NOTE: There can only be one service in each app that receives FCM messages. If multiple
 * are declared in the Manifest then the first one will be chosen.
 *
 * In order to make this Kotlin sample functional, you must remove the following from the Java messaging
 * service in the AndroidManifest.xml:
 *
 * <intent-filter>
 *   <action android:name="com.google.firebase.MESSAGING_EVENT" />
 * </intent-filter>
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    // Obtain the shared Tracker instance.
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */


    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]


        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        if (remoteMessage?.from != null) {
            Log.d(TAG, "From: ${remoteMessage.from}")
        }
//       AppUtils.showCommonToast(this,"Message data payload: " + remoteMessage!!.data)
        // Check if message contains a data payload.
        remoteMessage?.data?.isNotEmpty()?.let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }

            // TODO : TO handle notification
            sendNotification(remoteMessage)
        }

        // Check if message contains a notification payload.
        remoteMessage?.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    // [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()

        // [END dispatch_job]

//        val dataCheckBuilder = PeriodicWorkRequest.Builder(MyWorker::class.java,1, TimeUnit.MINUTES)
//        val dataCheckWork = dataCheckBuilder.build()
//        WorkManager.getInstance().enqueue(dataCheckWork)
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(notificationModel: RemoteMessage?) {
        val mUniqueId = System.currentTimeMillis().toInt()
        var sourceId = ""
        var actionType = ""
        var read = ""
        var actionTitle = ""
        var sourceType = ""
        var actorsCount = ""
        var id = ""
        var createdDate = ""
        var toUserId = ""
        var profileId = ""
        var pimage = ""
        var email = ""
        var idActorsObject = ""
        var name = ""
        var relation = ""
        var sourcePath = ""
        var actionTitleNew = ""
        var mLength = ""
        var JukeBoxCategoryId = ""
        var TrackImage = ""
        var TrackFile = ""
        var AllowOffline = ""
        var JukeBoxCategoryName = ""
        var Author = ""
        var TrackName = ""
        var ContentId = ""
        var trackid = ""
        var numberId = ""
        var trackName = ""
        var coverFile = ""
        var coverImage = ""
        var trackFile = ""
        var duration = ""
        var play_count = ""
        var allowOfflineDownload = ""
        var albumName = ""
        var albumId = ""
        var artistName = ""
        var artistId = ""
        var genreName = ""
        var genreId = ""

        //TODO: For chat
        var convId = ""
        var mProfileId = ""
        var mUserName = ""
        var mUserImage = ""

        try {

            val params = notificationModel!!.data
            val jsonObject = JSONObject(params.toString())

            val payload = jsonObject.get("payload")
            val payloadObject = JSONObject(payload.toString())
//            val payloadObject = JSONObject("{payload={\"sourceId\":\"5b05512bbc48152ef0f27c4e\",\"actionType\":\"general_push_notification\",\"read\":false,\"actionTitle\":\"Hey checkout new feature penny. Update your app!\",\"sourceType\":\"general_notification\",\"_id\":\"5b05512bbc48152ef0f27c4e\",\"created_date\":\"2018-05-22T07:30:00.000Z\",\"media\":{\"_id\":\"5b05511fbc48152ef0f27c4d\",\"sourcePath\":\"https:\\/\\/dtxj78b7ybird.cloudfront.net\\/general-pushnotification-images\\/155a4397-3916-4ca7-87cc-fed9ee4956be.jpg\"}}}")

            if (payloadObject.has("sourceId")) {
                sourceId = payloadObject.get("sourceId").toString()
            }
            if (payloadObject.has("actionType")) {
                actionType = payloadObject.get("actionType").toString()
                if (actionType == "content_notification"||actionType == "user_contact_join_follow") {
                    sourceType = actionType
                }
            }
            if (payloadObject.has("read")) {
                read = payloadObject.get("read").toString()
            }
            if (payloadObject.has("actionTitle")) {
                actionTitle = payloadObject.get("actionTitle").toString()
                actionTitleNew = payloadObject.get("actionTitle").toString()
            }
            if (payloadObject.has("sourceType")) {
                sourceType = payloadObject.get("sourceType").toString()
            }
            if (payloadObject.has("actorsCount")) {
                actorsCount = payloadObject.get("actorsCount").toString()
            }
            if (payloadObject.has("_id")) {
                id = payloadObject.get("_id").toString()
            }
            if (payloadObject.has("created_date")) {
                createdDate = payloadObject.get("created_date").toString()
            }
            if (payloadObject.has("toUserId")) {
                toUserId = payloadObject.get("toUserId").toString()
            }

            // TODO : im-desktop-notification Notification
            if (sourceType == "im-desktop-notification") {
                val mMessageOject = payloadObject.get("msg_payload").toString()
                val mMessageJsonObject = JSONObject(mMessageOject)
                var mMessage: String = ""

                var msgType = ""
                if (mMessageJsonObject.has("msg_type")) {
                    msgType = mMessageJsonObject.get("msg_type").toString()
                }

                if (msgType == "ping") {
                    mMessage = "Sent a ping"
                } else if (mMessageJsonObject.has("text")) {
                    if (mMessageJsonObject.get("text").toString() != "") {
                        mMessage = mMessageJsonObject.get("text").toString()
                    }
                } else {
                    mMessage = "Sent an $msgType"
                }

                val mMessageSender = mMessageJsonObject.get("message_sender").toString()
                val mMessageData = JSONObject(mMessageSender)
                if (mMessageData.has("username")) {

                    mUserName = mMessageData.get("username").toString()
                }
                val mTitle = "$mUserName - $mMessage"
                val sb: Spannable = SpannableString(mTitle)
                sb.setSpan(
                    StyleSpan(Typeface.BOLD),
                    0,
                    mUserName.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                actionTitleNew = sb.toString()

                pimage = mMessageData.get("pimage").toString()

                convId = mMessageJsonObject.get("conv_id").toString()
                mProfileId = mMessageData.get("_id").toString()

                mUserImage = mMessageData.get("pimage").toString()


                // TODO : Update Conversation list
                EventBus.getDefault()
                    .post(UpdateConversationEvent(""))
            }
            // TODO : General Notification
            if (payloadObject.has("media")) {
                val mediaObject = payloadObject.get("media").toString()
                val mediaObjectNew = JSONObject(mediaObject)
                if (mediaObjectNew.has("sourcePath")) {
                    sourcePath = mediaObjectNew.get("sourcePath").toString()
                }
            }

            // TODO : Track Notification
            if (payloadObject.has("track")) {
                val mediaObject = payloadObject.get("track").toString()
                val mediaObjectNew = JSONObject(mediaObject)

                if (mediaObjectNew.has("_id")) {
                    trackid = mediaObjectNew.getString("_id")
                }

                if (mediaObjectNew.has("number_id")) {
                    numberId = mediaObjectNew.getString("number_id")
                }

                if (mediaObjectNew.has("track_name")) {
                    trackName = mediaObjectNew.getString("track_name")
                }


                val mediaObjectTrackMedia = mediaObjectNew.get("media").toString()
                val mediaObjectTrack = JSONObject(mediaObjectTrackMedia)

                if (mediaObjectTrack.has("track_file")) {
                    trackFile = mediaObjectTrack.getString("track_file")
                }
                if (mediaObjectTrack.has("cover_file")) {
                    coverImage = mediaObjectTrack.getString("cover_file")
                    sourcePath = coverImage
                }
                if (mediaObjectNew.has("duration")) {
                    duration = mediaObjectNew.getString("duration")
                }
                if (mediaObjectNew.has("play_count")) {
                    play_count = mediaObjectNew.getString("play_count")
                }

                if (mediaObjectNew.has("allow_offline_download")) {
                    allowOfflineDownload = mediaObjectNew.getString("allow_offline_download")
                }

                val mediaObjectTrackGenre = mediaObjectNew.get("genre").toString()
                val mediaObjectGenre = JSONObject(mediaObjectTrackGenre)

                if (mediaObjectGenre.has("genre_name")) {
                    albumName = mediaObjectGenre.getString("genre_name")
                }
                if (mediaObjectGenre.has("_id")) {
                    albumId = mediaObjectGenre.getString("_id")

                }
                val mediaObjectTrackArtist = mediaObjectNew.get("artist").toString()
                val mediaObjectArtist = JSONObject(mediaObjectTrackArtist)

                if (mediaObjectArtist.has("artist_name")) {
                    artistName = mediaObjectArtist.getString("artist_name")
                }
                if (mediaObjectArtist.has("_id")) {
                    artistId = mediaObjectArtist.getString("_id")
                }


            }
            //TODO : Podcast notification
            if (payloadObject.has("podcast")) {
                val podcastObject = payloadObject.get("podcast").toString()
                val mediaObjectNew = JSONObject(podcastObject)
                if (mediaObjectNew.has("CoverImage")) {
                    coverImage = mediaObjectNew.getString("CoverImage")
                    sourcePath = coverImage
                }
                if (payloadObject.has("PodcastId")) {
                    sourceId = mediaObjectNew.get("PodcastId").toString()
                }
            }


            //TODO : Playlist notification
            if (payloadObject.has("playlist")) {
                val podcastObject = payloadObject.get("playlist").toString()
                val mediaObjectNew = JSONObject(podcastObject)

                val mediaObjectTrackMedia = mediaObjectNew.get("Media").toString()
                val mediaObjectPodcast = JSONObject(mediaObjectTrackMedia)

                if (mediaObjectPodcast.has("CoverImage")) {
                    coverImage = mediaObjectPodcast.getString("CoverImage")
                    sourcePath = coverImage
                }
                if (mediaObjectNew.has("PlaylistID")) {
                    sourceId = mediaObjectNew.get("PlaylistID").toString()
                }
            }


            // TODO: Article Notification
            if (payloadObject.has("content")) {
                val contentObject = payloadObject.get("content").toString()
                val mediaObjectNew = JSONObject(contentObject)
                if (mediaObjectNew.has("ContentId")) {
                    ContentId = mediaObjectNew.get("ContentId").toString()
                }
                if (mediaObjectNew.has("SourcePath")) {
                    sourcePath = mediaObjectNew.get("SourcePath").toString()
                }
                if (mediaObjectNew.has("Title")) {
                    actionTitleNew = mediaObjectNew.get("Title").toString()
                }
            }
            val aa1 = JSONObject(payload.toString())
            val actorsObject: JSONObject
            if (aa1.has("actors") && aa1.get("actors") != null) {
                val actors = aa1.get("actors")
                actorsObject = JSONObject(actors.toString())

                if (actorsObject.has("ProfileId")) {
                    mProfileId = actorsObject.get("ProfileId").toString()
                }
                if (actorsObject.has("pimage")) {
                    pimage = actorsObject.get("pimage").toString()
                }
                if (actorsObject.has("Email")) {
                    email = actorsObject.get("Email").toString()
                }
                if (actorsObject.has("_id")) {
                    idActorsObject = actorsObject.get("_id").toString()
                }
                if (actorsObject.has("username")) {
                    name = actorsObject.get("username").toString()
                }
                if (actorsObject.has("relation")) {
                    relation = actorsObject.get("relation").toString()
                }
            }

//        val intent = Intent(this, HomeActivity::class.java)
            var intent: Intent? = Intent(this, HomeActivity::class.java)
            when (sourceType) {
                "user_contact_join_follow" -> {
                    intent = Intent(this, UserProfileActivity::class.java)
                    intent.putExtra("mProfileId", idActorsObject)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "podcast_push_notification" -> {
                    intent = Intent(this, CommonPlayListActivity::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("title", "")
                    intent.putExtra("imageUrl", coverImage)
                    intent.putExtra("mDescription", "")
                    intent.putExtra("mIsFrom", "fromRadio")
                    intent.putExtra("isNeedToCallApi", true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "playlist_push_notification" -> {
                    intent = Intent(this, CommonPlayListActivity::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("title", "")
                    intent.putExtra("mIsFrom", "fromPlayList")
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "content" -> {
                    intent = Intent(this, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("isNeedToCallApi", true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "content_re_comment" -> {
                    intent = Intent(this, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("isNeedToCallApi", true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "user_mention_content_comment" -> {
                    intent = Intent(this, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("isNeedToCallApi", true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "user_video_post" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_image_post" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_text_post" -> {
                    intent = Intent(this, FeedTextDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_post_like" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_post_re_comment" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_post_comment" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_post_mention" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_mention_post_comment" -> {
                    intent = Intent(this, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                }
                "user_follow" -> {
                    when (actionType) {
                        "user_follow_accepted" -> {
                            intent = Intent(this, UserProfileActivity::class.java)
                            intent.putExtra("mProfileId", idActorsObject)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        }
                        "user_follow_request" -> {
                            intent = Intent(this, HomeActivity::class.java)
                            intent.putExtra("mPos", "4")
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        }
                        "user_follow" -> {
                            intent = Intent(this, UserProfileActivity::class.java)
                            intent.putExtra("mProfileId", idActorsObject)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        }
                    }

                }
                "user_follow_accepted" -> {
                    intent = Intent(this, UserProfileActivity::class.java)
                    intent.putExtra("mProfileId", idActorsObject)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "user_follow_request" -> {
                    intent = Intent(this, HomeActivity::class.java)
                    intent.putExtra("mPos", "4")
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                "penny" -> {
                    intent = Intent(this, HomeActivity::class.java)
                    intent.putExtra("mPos", "3")
                    intent.putExtra("mPosInner", "1")
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
                "im-desktop-notification" -> {
                    intent = Intent(this, HomeActivity::class.java)
                    intent.putExtra("mPos", "3")
                    intent.putExtra("mPosInner", "1")
//                    intent = Intent(this, OneToOneChatActivity::class.java)
                    intent.putExtra("convId", convId)
                    intent.putExtra("mProfileId", mProfileId)
                    intent.putExtra("mUserName", mUserName)
                    intent.putExtra("mUserImage", mUserImage)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
                "general_notification" -> {
                    intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("mPos", "")
                    intent.putExtra("mPosInner", "")
                    intent.putExtra("general", true)
                    intent.putExtra("mediasourcePath", sourcePath)

                }
                "track_push_notification" -> {

                    intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("mPos", "")
                    intent.putExtra("mPosInner", "")
                    intent.putExtra("track", true)

                    intent.putExtra("trackid", trackid)
                    intent.putExtra("numberId", numberId)
                    intent.putExtra("trackName", trackName)
                    intent.putExtra("coverFile", coverFile)
                    intent.putExtra("coverImage", coverImage)
                    intent.putExtra("trackFile", trackFile)
                    intent.putExtra("play_count", play_count)
                    intent.putExtra("duration", duration)
                    intent.putExtra("allowOfflineDownload", allowOfflineDownload)
                    intent.putExtra("albumName", albumName)
                    intent.putExtra("albumId", albumId)
                    intent.putExtra("artistName", artistName)
                    intent.putExtra("artistId", artistId)
                    intent.putExtra("genreName", genreName)
                    intent.putExtra("genreId", genreId)


                }

                "radio_show_notification" -> {
                    intent = Intent(this, HomeActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    intent.putExtra("mPos", "")
                    intent.putExtra("mPosInner", "")
                    intent.putExtra("radio", true)
                }

                "content_notification" -> {
                    intent = Intent(this, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", ContentId)
                    intent.putExtra("isNeedToCallApi", true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
            }
            val pendingIntent = PendingIntent.getActivity(
                this, mUniqueId /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT
            )


            if (actorsCount != "") {
                actionTitleNew = if (Integer.parseInt(actorsCount) == 1) {
                    "$name $actionTitle"
//                    "" + actionTitle
                } else {

                    val mCountInt = actorsCount.toInt().minus(1)
                    "$name and $mCountInt other $actionTitle"
//                    "and $actorsCount other $actionTitle"
                }
            }
            var bitmap: Bitmap? = null
            if (pimage != "") {
                if (sourceType != "im-desktop-notification") {
                    try {
                        bitmap = Glide.with(applicationContext)
                            .asBitmap()
                            .load(getImageUrlFromWidth(pimage, 100))
                            .submit(100, 100)
                            .get()
                    } catch (e: Exception) {
                    }
                }
            }
            if (sourcePath == "") {
                val sound: Uri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE.toString() + "://" + packageName + "/" + R.raw.penny_sound) //Here is FILE_NAME is the name of file that you want to play
                var mNotificationId = if (sourceType == "im-desktop-notification") {
                    getString(R.string.default_notification_channel_id_chat)
                } else {
                    getString(R.string.default_notification_channel_id)
                }
                val notificationBuilder =
                    NotificationCompat.Builder(
                        this,
                        mNotificationId
                    )
                        .setSmallIcon(R.mipmap.ic_custom_launcher_new_color)
                        .setContentIntent(pendingIntent)
                        .setContentText(actionTitleNew)

                if (sourceType == "penny" || sourceType == "im-desktop-notification") {
                    notificationBuilder.setSound(sound)
                }
                val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (sourceType != "im-desktop-notification") {

                    var futureTarget: FutureTarget<Bitmap>? = null
                    if (bitmap != null) {
                        futureTarget = Glide.with(this)
                            .asBitmap()
                            .load(bitmap)
//                            .submit(640, 360)
                            .submit(100, 100)
                        notificationBuilder.setLargeIcon(bitmap)
                    }
                    Glide.with(this).clear(futureTarget)
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    val channel = NotificationChannel(
                        getString(R.string.default_notification_channel_id),
                        sourceType,
                        NotificationManager.IMPORTANCE_HIGH
                    )
                    notificationManager.createNotificationChannel(channel)
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && sourceType == "im-desktop-notification") {
                    val mNotificationHelper = NotificationHelper(this)
                    val notificationBuilder = mNotificationHelper?.getNotificationDM(
                        "",
                        actionTitleNew,
                        convId,
                        mProfileId,
                        mUserName,
                        pimage
                    )
                    if (notificationBuilder != null) {
                        mNotificationHelper!!.notify(
                            mUniqueId,
                            notificationBuilder
                        )
                    }
                } else {

                    notificationBuilder.setAutoCancel(true)
                    notificationManager.notify(
                        mUniqueId,
                        notificationBuilder.build()
                    )

                }
            } else {

                try {
                    bitmap = Glide.with(this)
                        .asBitmap()
                        .load(getImageUrlFromWidth(sourcePath, 500))
                        .submit(640, 360)
                        .get()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (bitmap != null) {
                    val notificationManager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val notification = NotificationCompat.Builder(
                        this, getString(R.string.default_notification_channel_id)
                    )
                        .setSmallIcon(R.mipmap.ic_custom_launcher_new_color)
                        .setContentTitle(actionTitleNew)
                        .setLargeIcon(bitmap)
                        .setContentIntent(pendingIntent)
                        .setStyle(
                            NotificationCompat.BigPictureStyle().bigPicture(bitmap).bigLargeIcon(
                                null
                            )
                        )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        val channel = NotificationChannel(
                            getString(R.string.default_notification_channel_id),
                            sourceType,
                            NotificationManager.IMPORTANCE_HIGH
                        )
                        notificationManager.createNotificationChannel(channel)
                    }
                    notification.setAutoCancel(true)
                    notificationManager.notify(
                        mUniqueId,
                        notification.build()
                    )
                } else {
                    val bm = BitmapFactory.decodeResource(
                        resources,
                        R.mipmap.ic_custom_launcher_new_color
                    )
                    val notificationManager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val notification = NotificationCompat.Builder(
                        this, getString(R.string.default_notification_channel_id)
                    )
                        .setSmallIcon(R.mipmap.ic_custom_launcher_new_color)
                        .setContentTitle(actionTitleNew)
                        .setLargeIcon(bm)
                        .setContentIntent(pendingIntent)
                        .setStyle(
                            NotificationCompat.BigPictureStyle().bigPicture(bitmap).bigLargeIcon(
                                null
                            )
                        )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        val channel = NotificationChannel(
                            getString(R.string.default_notification_channel_id),
                            "Notification",
                            NotificationManager.IMPORTANCE_HIGH
                        )
                        notificationManager.createNotificationChannel(channel)
                    }
                    notification.setAutoCancel(true)
                    notificationManager.notify(
                        mUniqueId,
                        notification.build()
                    )
                }
            }


            // Obtain the FirebaseAnalytics instance.
            firebaseAnalytics = FirebaseAnalytics.getInstance(this)


            val paramsNotification = Bundle()
            paramsNotification.putString("notification_id", sourceId)
            paramsNotification.putString("notification_type", sourceType)
            firebaseAnalytics.logEvent("notification", paramsNotification)

            EventBus.getDefault()
                .post(NotificationEvent(actionType))

            when (sourceType) {
                "im-desktop-notification" -> {
                    EventBus.getDefault()
                        .post(NotificationEvent("im-desktop-notification"))
                }
            }


        } catch (tx: Exception) {
            Log.e("Social mob", "" + tx)
        }

    }

    private fun getDate(createdDate: String): String {
        var formattedDate = ""
        var df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        df.timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null
        try {
            date = df.parse(createdDate)
            df.timeZone = TimeZone.getDefault()
            df.format(date)
            df = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
            formattedDate = df.format(date)

//            textView.setText(formattedDate)
        } catch (e: ParseException) {
            e.printStackTrace()

        }
        return formattedDate
    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }

    private fun getImageUrlFromWidth(imageUrl: String?, width: Int): String {
        var width = width
        if (width == 0) {
            width = 500
        }
        var url = ""
        if (imageUrl != null && imageUrl !== "") {
            url = "$imageUrl?imwidth=$width&impolicy=resize"
        }
        return url
    }
}
