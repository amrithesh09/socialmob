package com.example.sample.socialmob.view.ui.home_module.notification

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Color
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.*
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.model.profile.GetAllNotificationResponseModelPayloadNotification

class NotificationAdapter(
    private val notificationAdapterFollowItemClick: NotificationAdapterFollowItemClick?,
    private val notificationAdapterItemClick: NotificationAdapterItemClick?,
    private val adapterDetailsClick: AdapterDetailsClick?,
    private val adapterProfileAcceptRejectClick: AdapterProfileAcceptRejectClick?,
    private val mContext: Activity?,
    private val mMarkAllClick: MarkAllClick?,
    private val deletePosition: DeletePostion?

) :
    ListAdapter<GetAllNotificationResponseModelPayloadNotification, BaseViewHolder<Any>>(
        DIFF_CALLBACK
    ) {

    companion object {
        const val ITEM_FOLLOW = 1
        const val ITEM_COMMENT = 2
        const val ITEM_FOLLOW_REQUEST = 3
        const val ITEM_LOADING = 4
        const val ITEM_MARK_ALL_READ = 5


        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<GetAllNotificationResponseModelPayloadNotification>() {
                override fun areItemsTheSame(
                    oldItem: GetAllNotificationResponseModelPayloadNotification,
                    newItem: GetAllNotificationResponseModelPayloadNotification
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: GetAllNotificationResponseModelPayloadNotification,
                    newItem: GetAllNotificationResponseModelPayloadNotification
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_FOLLOW -> bindFollow(parent)
            ITEM_FOLLOW_REQUEST -> bindFollowRequest(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_MARK_ALL_READ -> bindMarkAll(parent)
            else -> bindOther(parent)
        }
    }

    private fun bindMarkAll(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: MarkAllItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.mark_all_item, parent, false
        )
        return MarkAllViewHolder(binding)

    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    private fun bindOther(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemCommentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_comment, parent, false
        )
        return ViewHolderItemOther(binding)
    }

    private fun bindFollow(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFollowNotificationBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_follow_notification, parent, false
        )
        return ViewHolderItemFollow(binding)
    }

    private fun bindFollowRequest(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFollowRequestNotificationBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_follow_request_notification, parent, false
        )
        return ViewHolderItemOFollowRequest(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.id != "") {
            when {
                getItem(position)?.actionType.equals("user_contact_join_follow") -> ITEM_FOLLOW
                getItem(position)?.actionType.equals("user_follow") -> ITEM_FOLLOW
                getItem(position)?.actionType.equals("user_follow_request") -> ITEM_FOLLOW_REQUEST
                getItem(position)?.actionType.equals("") -> ITEM_LOADING
                else -> ITEM_COMMENT
            }
        } else {
            ITEM_MARK_ALL_READ
        }
    }


    inner class ViewHolderItemFollow(val binding: LayoutItemFollowNotificationBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.notificationViewModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (getItem(adapterPosition)?.read == true) {
                val mMode =
                    SharedPrefsUtils.getBooleanPreference(
                        mContext,
                        RemoteConstant.NIGHT_MODE,
                        false
                    )
                if (mMode) {
                    binding.commentCardView.setBackgroundColor(Color.parseColor("#000000"))
                } else {
                    binding.commentCardView.setBackgroundColor(Color.parseColor("#FFFFFF"))
                }
            } else {
                val mMode =
                    SharedPrefsUtils.getBooleanPreference(
                        mContext,
                        RemoteConstant.NIGHT_MODE,
                        false
                    )
                if (mMode) {
                    binding.commentCardView.setBackgroundColor(Color.parseColor("#2f3542"))
                } else {
                    binding.commentCardView.setBackgroundColor(Color.parseColor("#FFCEE7F1"))
                }
            }



            binding.followRelative.setOnClickListener {
                if (getItem(adapterPosition)?.actors?.relation == "following") {
                    val dialogBuilder =
                        this.let { mContext?.let { it1 -> AlertDialog.Builder(it1) } }
                    val inflater = mContext?.layoutInflater
                    val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                    dialogBuilder?.setView(dialogView)
                    dialogBuilder?.setCancelable(false)

                    val cancelButton: Button = dialogView?.findViewById(R.id.cancel_button)!!
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b: AlertDialog = dialogBuilder?.create()!!
                    b.show()


                    okButton.setOnClickListener {
                        notificationAdapterFollowItemClick?.onNotificationFollowItemClick(
                            getItem(adapterPosition)?.actors?.relation!!,
                            getItem(adapterPosition)?.actors?.id!!,
                            adapterPosition.toString()
                        )

                        Handler().postDelayed({
                            notify(
                                adapterPosition,
                                getItem(adapterPosition)?.actors?.privateProfile
                            )
                        }, 100)
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }
                } else {
                    if (itemCount > 0
                        && getItem(adapterPosition)?.actors != null
                        && getItem(adapterPosition)?.actors?.relation != null
                        && getItem(adapterPosition)?.actors?.id != null
                        && getItem(adapterPosition)?.actors?.privateProfile != null
                    ) {
                        notificationAdapterFollowItemClick?.onNotificationFollowItemClick(
                            getItem(adapterPosition)?.actors?.relation!!,
                            getItem(adapterPosition)?.actors?.id!!,
                            adapterPosition.toString()
                        )

                        Handler().postDelayed({

                            notify(
                                adapterPosition,
                                getItem(adapterPosition)?.actors?.privateProfile
                            )

                        }, 100)
                    }

                }

            }
            itemView.setOnClickListener {
                notificationAdapterItemClick?.onNotificationAdapterItemClick(
                    getItem(adapterPosition)?.actors?.id!!,
                    "", getItem(adapterPosition)?.id!!,
                    getItem(adapterPosition)?.read!!
                )
                Handler().postDelayed({
                    getItem(adapterPosition)?.read?.let { it1 ->
                        notifyAsRead(
                            adapterPosition,
                            it1
                        )
                    }
                }, 100)
            }

        }

    }


    private fun notify(adapterPosition: Int, privateProfile: Boolean?) {
        if (privateProfile == false) {
            if (getItem(adapterPosition)?.actors?.relation == "following") {
                getItem(adapterPosition)?.actors?.relation = "none"
            } else {
                getItem(adapterPosition)?.actors?.relation = "following"
            }
        } else {
            when {
                getItem(adapterPosition)?.actors?.relation == "none" -> getItem(adapterPosition)?.actors?.relation =
                    "request pending"
                getItem(adapterPosition)?.actors?.relation == "following" -> getItem(adapterPosition)?.actors?.relation =
                    "none"
                getItem(adapterPosition)?.actors?.relation == "request pending" -> getItem(
                    adapterPosition
                )?.actors?.relation =
                    "none"
            }
        }
        notifyItemChanged(adapterPosition, itemCount)
    }

    inner class ViewHolderItemOFollowRequest(val binding: LayoutItemFollowRequestNotificationBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            binding.notificationViewModel = getItem(adapterPosition)
            binding.relation = "following"
            binding.nonRelation = "none"
            binding.executePendingBindings()

            try {
                if (getItem(adapterPosition)?.read == true) {
                    val mMode =
                        SharedPrefsUtils.getBooleanPreference(
                            mContext,
                            RemoteConstant.NIGHT_MODE,
                            false
                        )
                    if (mMode) {
                         binding.commentCardView.setBackgroundColor(Color.parseColor("#000000"))
                    } else {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#FFFFFF"))
                    }
                } else {
                    val mMode =
                        SharedPrefsUtils.getBooleanPreference(
                            mContext,
                            RemoteConstant.NIGHT_MODE,
                            false
                        )
                    if (mMode) {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#2f3542"))
                    } else {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#FFCEE7F1"))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }




            binding.acceptTextView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    adapterProfileAcceptRejectClick?.onProfileAcceptRejectClick(
                        getItem(adapterPosition)?.actors?.id!!, "accept"
                    )
                    Handler().postDelayed({
                        deletePosition?.onDeletePosition(adapterPosition)
                    }, 100)
                }
            }
            binding.rejectTextView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    adapterProfileAcceptRejectClick?.onProfileAcceptRejectClick(
                        getItem(adapterPosition)?.actors?.id!!, "reject"
                    )
                    Handler().postDelayed({
                        deletePosition?.onDeletePosition(adapterPosition)
                    }, 100)
                }
            }

            binding.profileImageView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    notificationAdapterItemClick?.onNotificationAdapterItemClick(
                        getItem(adapterPosition)?.actors?.id!!,
                        "",
                        getItem(adapterPosition)?.id!!,
                        getItem(adapterPosition)?.read!!
                    )
                }
            }
        }

    }


    inner class MarkAllViewHolder(val binding: MarkAllItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.markAsRead.setOnClickListener {
                Handler().postDelayed({
                    removeFirstItem()
                }, 100)
                mMarkAllClick?.onMarkAllClick()
            }
        }

    }

    inner class ViewHolderItemOther(val binding: LayoutItemCommentBinding) :
        BaseViewHolder<Any>(binding.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(`object`: Any?) {
            if (itemCount > 0) {
                binding.otherViewModel = getItem(adapterPosition)
                binding.executePendingBindings()

                if (getItem(adapterPosition)?.read == true) {
                    val mMode =
                        SharedPrefsUtils.getBooleanPreference(
                            mContext,
                            RemoteConstant.NIGHT_MODE,
                            false
                        )
                    if (mMode) {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#000000"))
                    } else {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#FFFFFF"))
                    }
                } else {
                    val mMode =
                        SharedPrefsUtils.getBooleanPreference(
                            mContext,
                            RemoteConstant.NIGHT_MODE,
                            false
                        )
                    if (mMode) {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#2f3542"))
                    } else {
                        binding.commentCardView.setBackgroundColor(Color.parseColor("#FFCEE7F1"))
                    }
                }


                //TODO : Notification Grouping
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (getItem(adapterPosition)?.actorsCount != null
                        && getItem(adapterPosition)?.actionTitle != null
                    ) {
                        if (getItem(adapterPosition)?.actorsCount == 1) {

                            binding.notificationDescriptionTextView.text =
                                getItem(adapterPosition)?.actionTitle
                        } else {
                            val mCountInt = getItem(adapterPosition)?.actorsCount?.minus(1)
                            binding.notificationDescriptionTextView.text =
                                " and " + mCountInt + " others " +
                                        getItem(adapterPosition)?.actionTitle
                        }
                    }
                }

                binding.commentCardView.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (getItem(adapterPosition)?.sourceId != null &&
                            getItem(adapterPosition)?.sourceType != null &&
                            getItem(adapterPosition)?.id != null &&
                            getItem(adapterPosition)?.read != null
                        ) {

                            adapterDetailsClick?.onAdapterDetailsClick(
                                getItem(adapterPosition)?.sourceId!!,
                                getItem(adapterPosition)?.sourceType!!,
                                getItem(adapterPosition)?.id!!,
                                getItem(adapterPosition)?.read!!
                            )

                            Handler().postDelayed({
                                notifyAsRead(
                                    adapterPosition,
                                    getItem(adapterPosition)?.read!!
                                )
                            }, 100)
                        }
                    }
                }
            }
        }

    }

    private fun notifyAsRead(adapterPosition: Int, read: Boolean) {
        if (!read) {
            getItem(adapterPosition)?.read = true
            notifyItemChanged(adapterPosition, itemCount)
        }
    }

    fun removeFirstItem() {
        if (itemCount > 0) {
//            listItemNotification?.removeAt(0)
            deletePosition?.onDeletePosition(0)
            notifyItemRemoved(0)
            notifyItemChanged(0, itemCount)
        }

    }

    interface DeletePostion {
        fun onDeletePosition(mPosition: Int)
    }

    interface NotificationAdapterFollowItemClick {
        fun onNotificationFollowItemClick(isFollowing: String, mId: String, mPosition: String)
    }

    interface MarkAllClick {
        fun onMarkAllClick()
    }

    interface NotificationAdapterItemClick {
        fun onNotificationAdapterItemClick(
            mProfileId: String,
            mIsOwn: String,
            mNotificationId: String,
            mIsRead: Boolean
        )
    }

    interface AdapterDetailsClick {
        fun onAdapterDetailsClick(
            sourceId: String,
            mType: String,
            mNotificationId: String,
            mIsRead: Boolean
        )
    }

    interface AdapterProfileAcceptRejectClick {
        fun onProfileAcceptRejectClick(mId: String, mType: String)
    }
}


