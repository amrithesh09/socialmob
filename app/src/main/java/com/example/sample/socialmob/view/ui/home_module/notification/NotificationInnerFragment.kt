package com.example.sample.socialmob.view.ui.home_module.notification

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.NotificationInnerFragmentBinding
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.backlog.articles.ArticleDetailsActivityNew
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedTextDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedVideoDetailsActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.model.profile.GetAllNotificationResponseModelPayloadNotification
import com.example.sample.socialmob.viewmodel.notification.NotificationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.notification_inner_fragment.*
import javax.inject.Inject

@AndroidEntryPoint
class NotificationInnerFragment : Fragment(),
    NotificationAdapter.NotificationAdapterFollowItemClick,
    NotificationAdapter.NotificationAdapterItemClick, NotificationAdapter.AdapterDetailsClick,
    NotificationAdapter.AdapterProfileAcceptRejectClick, NotificationAdapter.MarkAllClick,
    NotificationAdapter.DeletePostion {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private val ARG_BOOLEAN = "isFrom"
    private var binding: NotificationInnerFragmentBinding? = null
    private var notificationViewModel: NotificationViewModel? = null
    private var mListNotification: MutableList<GetAllNotificationResponseModelPayloadNotification>? =
        ArrayList()
    private var adapterNotificationRecyclerView: NotificationAdapter? = null
    private var mIsAddedMarkAllRead: Boolean = false
    private var mStatus: ResultResponse.Status? = null
    private var isAddedLoader: Boolean = false

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (binding != null) {
                binding?.swipeToRefresh?.isRefreshing = true
                mListNotification?.clear()
                callApiCommon()
            }

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: Bundle = requireArguments()
        val mIsFrom: String = args.getString(ARG_BOOLEAN, "")!!
        if (mIsFrom == getString(R.string.settings_)) {
            binding?.cardLayout?.visibility = View.GONE
        }

        binding?.swipeToRefresh?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefresh?.setOnRefreshListener {
            mListNotification?.clear()
            mListNotification = ArrayList()
            callApiCommon()
            if (!InternetUtil.isInternetOn()) {
                if (binding?.swipeToRefresh?.isRefreshing!!) {
                    binding?.swipeToRefresh?.isRefreshing = false
                }
            }
        }
        binding?.refreshTextView?.setOnClickListener {
            mListNotification?.clear()
            mListNotification = ArrayList()
            callApiCommon()
        }


        callApiCommon()
        observeDataNotification()

        // TODO : Set Adapter
        adapterNotificationRecyclerView = NotificationAdapter(
            notificationAdapterFollowItemClick = this,
            notificationAdapterItemClick = this,
            adapterDetailsClick = this,
            adapterProfileAcceptRejectClick = this,
            mContext = requireActivity(),
            mMarkAllClick = this,
            deletePosition = this
        )
        binding?.notificationRecyclerView?.adapter = adapterNotificationRecyclerView
        binding?.notificationRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
            )
        binding?.notificationRecyclerView?.addOnScrollListener(CustomScrollListener())
    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (dy > 0) {
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                ) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        if (!isAddedLoader) {
                            loadMoreItems(mListNotification?.size!!)
                        }
                    }
                }
            }
        }
    }

    private fun loadMoreItems(size: Int) {

        val mutableList: MutableList<GetAllNotificationResponseModelPayloadNotification> =
            ArrayList()
        val mList = GetAllNotificationResponseModelPayloadNotification()
        mList.actionType = ""
        mList.read = true
        mutableList.add(mList)
        mListNotification?.addAll(mutableList)
        adapterNotificationRecyclerView?.notifyDataSetChanged()
        isAddedLoader = true


        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mNotificationData +
                    RemoteConstant.mSlash + size +
                    RemoteConstant.mSlash + RemoteConstant.mCount, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(
                    activity,
                    RemoteConstant.mAppId,
                    ""
                )!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        notificationViewModel?.getAllNotification(
            mAuth,
            size.toString(),
            RemoteConstant.mCount
        )


    }

    private fun callApiCommon() {
        if (InternetUtil.isInternetOn()) {
            callApi()
        } else {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = false
            binding?.isVisibleNoDataPenny = false
            binding?.isVisibleNoDataNotification = false
            binding?.isVisibleNoInternet = true
            val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(binding?.noInternetImageView!!)
        }

    }

    private fun callApi() {

        binding?.isVisibleList = false
        binding?.isVisibleLoading = true
        binding?.isVisibleNoDataPenny = false
        binding?.isVisibleNoDataNotification = false
        binding?.isVisibleNoInternet = false
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mNotificationData +
                    RemoteConstant.mSlash + RemoteConstant.mOffset +
                    RemoteConstant.mSlash + RemoteConstant.mCount, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        notificationViewModel?.getAllNotification(
            mAuth,
            RemoteConstant.mOffset,
            RemoteConstant.mCount
        )
    }

    private fun observeDataNotification() {

        notificationViewModel?.notificationUsersPaginatedList?.nonNull()?.observe(
            viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status
                if (swipeToRefresh.isRefreshing) {
                    swipeToRefresh.isRefreshing = false
                }

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mListNotification = resultResponse?.data!!

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataNotificationLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        if (mListNotification?.size!! > 0) {
                            adapterNotificationRecyclerView?.submitList(mListNotification!!)
                        }

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                        mIsAddedMarkAllRead = false
                        checkHasUnreadMessage()

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mListNotification?.addAll(resultResponse?.data!!)
                        adapterNotificationRecyclerView?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.ERROR -> {

                        if (mListNotification?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataNotificationLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }


                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_no_notifications_new_image).apply(
                            RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataNotificationLinear?.visibility = View.VISIBLE

                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataNotificationLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }

            })
    }

    private fun removeLoaderFormList() {

        if (mListNotification != null && mListNotification?.size!! > 0 && mListNotification?.get(
                mListNotification?.size!! - 1
            ) != null && mListNotification?.get(mListNotification?.size!! - 1)?.actionType == ""
        ) {
            mListNotification?.removeAt(mListNotification?.size!! - 1)
            adapterNotificationRecyclerView?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }

    private fun checkHasUnreadMessage() {

        notificationViewModel?.isHasNotification?.nonNull()?.observe(viewLifecycleOwner, {
            if (it != null) {
                if (it == true) {
                    if (!mIsAddedMarkAllRead) {
                        mIsAddedMarkAllRead = true
                        val mNotification =
                            GetAllNotificationResponseModelPayloadNotification()
                        mNotification.id = ""
                        mListNotification?.add(0, mNotification)
                        adapterNotificationRecyclerView?.notifyDataSetChanged()
                    }
                }
            }
        })
    }


    fun newInstance(isFrom: String): NotificationInnerFragment {
        val args = Bundle()
        args.putSerializable(ARG_BOOLEAN, isFrom)
        val fragment = NotificationInnerFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ):
            View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.notification_inner_fragment, container, false
        )
        notificationViewModel =
            ViewModelProvider(this).get(NotificationViewModel::class.java)

        return binding?.root
    }

    private var mLastClickTime: Long = 0
    override fun onAdapterDetailsClick(
        sourceId: String, mType: String, mNotificationId: String, mIsRead: Boolean
    ) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            var intent: Intent? = Intent()
            when (mType) {
                "user_text_post" -> {
                    intent = Intent(activity, FeedTextDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "content" -> {
                    intent = Intent(activity, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("isNeedToCallApi", true)
                }
                "content_re_comment" -> {
                    intent = Intent(activity, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("isNeedToCallApi", true)
                }
                "user_mention_content_comment" -> {
                    intent = Intent(activity, ArticleDetailsActivityNew::class.java)
                    intent.putExtra("mId", sourceId)
                    intent.putExtra("isNeedToCallApi", true)
                }
                "user_video_post" -> {
                    intent = Intent(activity, FeedVideoDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_image_post" -> {
                    intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_post_like" -> {
                    intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_post_re_comment" -> {
                    intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_post_comment" -> {
                    intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_post_mention" -> {
                    intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_mention_post_comment" -> {
                    intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    intent.putExtra("mPostId", sourceId)
                }
                "user_follow" -> {
                    intent = Intent(activity, UserProfileActivity::class.java)
                    intent.putExtra("mProfileId", sourceId)
                }
                "user_follow_accepted" -> {
                    intent = Intent(activity, UserProfileActivity::class.java)
                    intent.putExtra("mProfileId", sourceId)
                }
            }
            startActivity(intent)
            if (!mIsRead) {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.notificationData + RemoteConstant.mSlash + mNotificationId +
                            RemoteConstant.mPathRead, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        activity, RemoteConstant.mAppId, ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                notificationViewModel?.readSingleNotification(mAuth, mNotificationId)

            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    override fun onProfileAcceptRejectClick(mId: String, mType: String) {
        if (mType == "accept") {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.profileData +
                        RemoteConstant.mSlash + mId + RemoteConstant.pathAcceptRequest,
                RemoteConstant.putMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    activity, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
            notificationViewModel?.acceptRequest(mAuth, mId)
        } else if (mType == "reject") {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.profileData +
                        RemoteConstant.mSlash + mId + RemoteConstant.pathAcceptRequest,
                RemoteConstant.deleteMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    activity, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
            notificationViewModel?.rejectRequest(mAuth, mId)
        }


    }

    override fun onNotificationAdapterItemClick(
        mProfileId: String, mIsOwn: String,
        mNotificationId: String, mIsRead: Boolean
    ) {
        if (!mIsRead) {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl +
                        RemoteConstant.notificationData +
                        RemoteConstant.mSlash + mNotificationId +
                        RemoteConstant.mPathRead, RemoteConstant.putMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    activity, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            notificationViewModel?.readSingleNotification(mAuth, mNotificationId)
        }
        val intent = Intent(activity, UserProfileActivity::class.java)
        intent.putExtra("mProfileId", mProfileId)
        intent.putExtra("mIsOwn", "false")
        startActivity(intent)
    }

    override fun onNotificationFollowItemClick(
        isFollowing: String, mId: String, mPosition: String
    ) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }
    }

    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        notificationViewModel?.unFollowProfile(mAuth, mId)
    }

    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        notificationViewModel?.followProfile(mAuth, mId)
    }

    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        notificationViewModel?.cancelRequest(mAuth, mId)
    }

    override fun onMarkAllClick() {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + "/api/v1/profile/notifications/read",
            RemoteConstant.putMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!! +
                ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        notificationViewModel?.markAllAsRead(mAuth)
        Handler().postDelayed({
            mIsAddedMarkAllRead = false
            mListNotification?.clear()
            callApiCommon()
        }, 1000)
    }

    override fun onDeletePosition(mPosition: Int) {
        if (mListNotification?.size ?: 0 > 0 && mListNotification?.get(mPosition)?.read != null) {
            mListNotification?.removeAt(mPosition)
            adapterNotificationRecyclerView?.notifyDataSetChanged()
        }
    }
}

