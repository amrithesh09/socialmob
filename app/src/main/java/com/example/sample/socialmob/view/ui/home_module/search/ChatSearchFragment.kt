package com.example.sample.socialmob.view.ui.home_module.search


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.model.chat.MsgPayload
import com.example.sample.socialmob.model.chat.SendMessage
import com.example.sample.socialmob.model.chat.WebSocketResponse
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.chat.ChatHistorySearchAdapter
import com.example.sample.socialmob.view.ui.home_module.chat.WebSocketChatActivity
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.view.utils.events.SearchEventChat
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

@AndroidEntryPoint
class ChatSearchFragment : Fragment(), SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick,
    SearchPeopleAdapter.SearchPeopleAdapterItemClick, SearchPeopleAdapter.SendMessage,
    ChatHistorySearchAdapter.MessageItemClick, ChatHistorySearchAdapter.DeleteConversation {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private val searchViewModel: SearchViewModel by viewModels()
    private var binding: SearchInnerFragmentBinding? = null
    private var mSearchWord = ""
    private var mName: String = ""

    private var mLastClickTime: Long = 0
    private var mStatus: ResultResponse.Status? = null
    private var isNewChat: Boolean = false
    private var mProfileIdNewChat = ""
    private var mNameNewChat = ""
    private var mImageNewChat = ""
    private var recentAdapter: ChatHistorySearchAdapter? = null


    private val viewModelChat: WebSocketChatViewModel? by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }
    companion object {
        private const val ARG_STRING = "mString"
        var mProfileId = ""
        fun newInstance(mName: String): ChatSearchFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = ChatSearchFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = arguments
        mName = args?.getString(ARG_STRING, "") ?: ""
        binding?.searchTitleTextView?.text = mName

        binding?.refreshTextView?.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(mSearchWord)
            }
        }

        observeChatHistory()
        val mProfileId =
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")!!
        recentAdapter =
            ChatHistorySearchAdapter(
                activity, this, mProfileId,
                this, true, glideRequestManager
            )
        binding?.searchInnerRecyclerView?.adapter = recentAdapter

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)
        return binding?.root
    }


    override fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
    }


    override fun onSearchPeopleAdapterItemClick(mProfileId: String, mIsOwn: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (mIsOwn == "false") {
                val intent = Intent(activity, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mProfileId)
                intent.putExtra("mIsOwn", mIsOwn)
                startActivityForResult(intent, 1501)
            } else {
                val intent = Intent(activity, MyProfileActivity::class.java)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
    }

    /**
     * Call API search users
     */
    fun callApiCommon(mSearchWord: String) {
        if (InternetUtil.isInternetOn()) {
            if (activity != null && binding != null) {
                if (mSearchWord != "") {
                    val nJwt =
                        "Bearer " + SharedPrefsUtils.getStringPreference(
                            activity,
                            RemoteConstant.mJwt,
                            ""
                        )!!
                    searchViewModel.searchUsers(
                        nJwt, mSearchWord
                    )
                    binding?.progressLinear?.visibility = View.VISIBLE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                    binding?.listLinear?.visibility = View.GONE
                }
            } else {
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            }
        }
    }
    /**
     * If we alraedy have conversation id navigate to chat page else send an  new penny
     */
    override fun onSendMessage(
        mProfileId: String, mNAme: String, mProfileImage: String, convId: String
    ) {
        // TODO : Send message
        if (convId != "") {
            val intent = Intent(activity, WebSocketChatActivity::class.java)
            intent.putExtra("convId", convId)
            intent.putExtra("mProfileId", mProfileId)
            intent.putExtra("mUserName", mNAme)
            intent.putExtra("mUserImage", mProfileImage)
            intent.putExtra("mStatus", mStatus)
            intent.putExtra("mLastMessageUser", "")
            startActivity(intent)
        } else {
            isNewChat = true
            // TODO : Send message
            mProfileIdNewChat = mProfileId
            mNameNewChat = mNAme
            mImageNewChat = mProfileImage
            pennySend(mProfileId, mNAme, mProfileImage)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun pennySend(
        mProfileId: String,
        mName: String,
        mImage: String
    ) {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_penny_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val messageTextView: TextView = dialogView.findViewById(R.id.message_text_view)
        val descriptionTextView: TextView = dialogView.findViewById(R.id.description_textView)
        val timeLinear: LinearLayout = dialogView.findViewById(R.id.time_linear)
        val pennyEditText: EditText = dialogView.findViewById(R.id.penny_edit_text)
        val closeButton: Button = dialogView.findViewById(R.id.close_button)
        val replayButton: Button = dialogView.findViewById(R.id.replay_button)
        val pennyProfileImageView: ImageView =
            dialogView.findViewById(R.id.penny_profile_image_view)
        val pennyOptionsImageView: ImageView =
            dialogView.findViewById(R.id.penny_options_image_view)
        replayButton.text = getString(R.string.send)
        //TODO : Custom For Send Penny

        glideRequestManager.load(mImage).apply(
            RequestOptions.placeholderOf(R.drawable.emptypicture).error(R.drawable.emptypicture)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
        ).into(pennyProfileImageView)
        messageTextView.visibility = View.GONE
        timeLinear.visibility = View.GONE
        pennyOptionsImageView.visibility = View.GONE
        descriptionTextView.text = "Send Penny to $mName"

        val alertDialog: AlertDialog = dialogBuilder.create()
        alertDialog.show()
        replayButton.setOnClickListener {
            if (pennyEditText.text.isNotEmpty()) {
                if (viewModelChat?.isWebSocketOpen!!) {
                    replayButton.isClickable = false
                    replayButton.isEnabled = false

                    val mSendMessage = SendMessage()
                    val mMsgPayload = MsgPayload()

                    mSendMessage.user = mProfileId
                    mSendMessage.type = "message"

                    mMsgPayload.user = mProfileId
                    mMsgPayload.msgType = "text"
                    mMsgPayload.text = pennyEditText.text.toString()
                    mMsgPayload.timestamp = System.currentTimeMillis()
                    mSendMessage.msgPayload = mMsgPayload

                    val mGson = Gson()
                    val mMessage =
                        mGson.toJson(mSendMessage)


                    viewModelChat?.sendMessageSm(mMessage)

                    Handler().postDelayed({
//                        AppUtils.showCommonToast(this, "Message send ")
                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        AppUtils.hideKeyboard(activity, binding?.noDataImageView!!)
                    }, 500)
                } else {
                    if (!viewModelChat?.isWebSocketOpen!! && !viewModelChat?.isWebSocketOpenCalled!!) {
                        viewModelChat?.connectToSocketSm()
                    }
                    Toast.makeText(activity, "Waiting for connection", Toast.LENGTH_SHORT).show()
                }
            } else {
                AppUtils.showCommonToast(
                    requireActivity(),
                    "You cannot send Penny with an empty note."
                )
            }
        }
        closeButton.setOnClickListener {
            isNewChat = false
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        viewModelChat?.newMessage?.observe(viewLifecycleOwner, { newMessage ->

            if (newMessage != null && newMessage != "") {
                handleMessage(newMessage, mProfileId, mName, mImage)
            }
        })

    }

    private fun handleMessage(text: String?, mProfileId: String, mName: String, mImage: String) {
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)
            if (webSocketResponse?.type != null) {
                if (webSocketResponse.type == "message") {
                    viewModelChat?.newMessage?.removeObservers(this)
                    val convId1 = webSocketResponse.msgPayload?.convId
                    val intent = Intent(activity, WebSocketChatActivity::class.java)
                    intent.putExtra("convId", convId1)
                    intent.putExtra("mProfileId", mProfileId)
                    intent.putExtra("mUserName", mName)
                    intent.putExtra("mUserImage", mImage)
                    intent.putExtra("mStatus", "")
                    intent.putExtra("mLastMessageUser", "")
                    startActivityForResult(intent, 112)

                    isNewChat = false
                    mProfileIdNewChat = ""
                    mNameNewChat = ""
                    mImageNewChat = ""

                }
            }

        } catch (e: Exception) {
            AppUtils.showCommonToast(requireActivity(), "" + e.printStackTrace())
            e.printStackTrace()
        }

    }

    private fun observeChatHistory() {
        binding?.searchInnerRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = true

        searchViewModel.getAllChatResponseModel?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {

                        val mChatRecentList = resultResponse?.data!!
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        recentAdapter?.submitList(mChatRecentList)
                        recentAdapter?.notifyDataSetChanged()

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.ERROR -> {
                        if (SearchTrackFragment.dbPlayList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_empty_penny).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })

    }

    override fun messageItemClick(
        convId: String?, mProfileId: String?, mUserName: String?, mUserImage: String?,
        mStatus: String?, mLastMessageUser: String?
    ) {
        if (convId != null && convId != "") {
            val intent = Intent(activity, WebSocketChatActivity::class.java)
            intent.putExtra("convId", convId)
            intent.putExtra("mProfileId", mProfileId)
            intent.putExtra("mUserName", mUserName)
            intent.putExtra("mUserImage", mUserImage)
            intent.putExtra("mStatus", mStatus)
            intent.putExtra("mLastMessageUser", mLastMessageUser)
            startActivity(intent)
        } else {
            isNewChat = true
            // TODO : Send message
            mProfileIdNewChat = mProfileId ?: ""
            mNameNewChat = mUserName ?: ""
            mImageNewChat = mUserImage ?: ""
            pennySend(
                mProfileId ?: "",
                mUserName ?: "",
                mUserImage ?: ""
            )
        }

    }

    override fun onDeleteConversation(position: Int, convId: String?) {

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SearchEventChat) {
        mSearchWord = event.mWord
        callApiCommon(mSearchWord)
    }
}

