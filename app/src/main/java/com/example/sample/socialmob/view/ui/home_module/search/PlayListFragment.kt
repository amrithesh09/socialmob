package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.PlaylistDataIcon
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.GridSpacingItemDecoration
import com.example.sample.socialmob.view.utils.NpaGridLayoutManager
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.events.SearchEventPeople
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlayListFragment : Fragment(), SearchPlayListAdapter.PlayListAdapterItemClickListener,
    SearchPlayListAdapter.PlayListAdapterOptionClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mName = ""
    private var mSearchWord = ""
    private var binding: SearchInnerFragmentBinding? = null
    private val searchViewModel: SearchViewModel by viewModels()
    private var mStatus: ResultResponse.Status? = null
    private var mAdapter: SearchPlayListAdapter? = null
    private var isAddedLoader: Boolean = false
    private var singlePlayList: MutableList<PlaylistCommon>? = ArrayList()

    companion object {
        private const val ARG_STRING = "mString"
        var mListSize = "0"

        fun newInstance(mName: String): PlayListFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = PlayListFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: Bundle? = arguments
        mName = args?.getString(ARG_STRING, "")!!
        binding?.searchTitleTextView?.text = mName

        binding?.searchInnerRecyclerView?.layoutManager = NpaGridLayoutManager(activity, 2)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.penny_margin_start)
        binding?.searchInnerRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(2, spacingInPixels, true)
        )
        mAdapter = SearchPlayListAdapter(this, this, glideRequestManager)
        binding?.searchInnerRecyclerView?.adapter = mAdapter

        /**
         * Check whether navigation from view-all click
         * Else Initially we have top items
         */
        if (mName == "PlayListForYouViewAll") {
            callApiAllPlayList(RemoteConstant.mOffset, RemoteConstant.mCount)

        } else {
            // TODO: For Top List
            val mPlayList: MutableList<PlaylistCommon>? =
                SearchFragment.mTopMusicData?.playlistsForYou?.toMutableList()
            mAdapter?.submitList(mPlayList)
        }



        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = true
        binding?.searchInnerRecyclerView?.addOnScrollListener(CustomScrollListener())

        observePlayList()

        binding?.refreshTextView?.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(
                    mSearchWord, RemoteConstant.mOffset,
                    RemoteConstant.mCount
                )
            }
        }
    }

    /**
     * Handle pagination API
     */
    inner class CustomScrollListener :
        RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: RecyclerView, newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: RecyclerView, dx: Int, dy: Int
        ) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as GridLayoutManager).findFirstVisibleItemPosition()
                mListSize = singlePlayList?.size.toString()
                if (singlePlayList?.size!! >= RemoteConstant.mCountTwleve.toInt()) {

                    if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                        mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                        visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                    ) {
                        if (mName == "PlayListForYouViewAll") {
                            if (!isAddedLoader) {
                                callApiAllPlayList(mListSize, RemoteConstant.mCount)
                                val mLoaderList: MutableList<PlaylistCommon> = ArrayList()
                                val mList = PlaylistCommon()
                                mList.id = "0"
                                mLoaderList.add(mList)
                                singlePlayList?.addAll(mLoaderList)
                                binding?.searchInnerRecyclerView?.post {
                                    mAdapter?.notifyItemInserted(singlePlayList?.size?.minus(1)!!)
                                }
                                isAddedLoader = true
                            }
                        } else {
                            if (mSearchWord != "" && !isAddedLoader) {
                                callApiCommon(mSearchWord, mListSize, RemoteConstant.mCount)
                                val mLoaderList: MutableList<PlaylistCommon> = ArrayList()
                                val mList = PlaylistCommon()
                                mList.id = "0"
                                mLoaderList.add(mList)
                                singlePlayList?.addAll(mLoaderList)
                                binding?.searchInnerRecyclerView?.post {
                                    mAdapter?.notifyItemInserted(singlePlayList?.size?.minus(1)!!)
                                }
                                isAddedLoader = true
                            }
                        }

                    }
                }
            }
        }
    }

    /**
     * If loader added on pagination need to remove common method
     */
    private fun removeLoaderFormList() {
        if (singlePlayList != null && singlePlayList?.size!! > 0 && singlePlayList?.get(
                singlePlayList?.size!! - 1
            ) != null && singlePlayList?.get(singlePlayList?.size!! - 1)?.id == "0"
        ) {
            singlePlayList?.removeAt(singlePlayList?.size!! - 1)
            mAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }

    private fun observePlayList() {

        searchViewModel.playListLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status
                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        singlePlayList = resultResponse?.data!!
                        mAdapter?.submitList(singlePlayList!!)

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        val size = singlePlayList?.size

                        singlePlayList?.addAll(resultResponse?.data!!)
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                        mAdapter?.notifyItemRangeInserted(size!!, singlePlayList?.size!!)
                    }
                    ResultResponse.Status.ERROR -> {
                        if (singlePlayList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_no_search_data).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)
        return binding?.root
    }

    /**
     * Navigate to common track list page
     */
    override fun onItemClick(
        item: String, mTitle: String, mImageUrl: String, mUserName: String, mUserId: String
    ) {
        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", item)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", mImageUrl)
        intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
        intent.putExtra("mPlayListUser", mUserName)
        intent.putExtra("mPlayListUserId", mUserId)
        activity?.startActivity(intent)
    }

    /**
     * Playlist options
     */
    override fun optionDialog(
        playlistId: String?, playlistName: String?, private: String, icon: PlaylistDataIcon,
        adapterPosition: Int
    ) {
        val viewGroup: ViewGroup? = null
        val mBottomSheetDialog = RoundedBottomSheetDialog(requireActivity())
        val sheetView: View = layoutInflater.inflate(R.layout.play_list_options, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val editLinear: LinearLayout = sheetView.findViewById(R.id.edit_linear)
        val deleteLinear: LinearLayout = sheetView.findViewById(R.id.delete_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)

        editLinear.visibility = View.GONE
        deleteLinear.visibility = View.GONE

        shareLinear.setOnClickListener {
            ShareAppUtils.sharePlayList(playlistId, requireActivity())
            mBottomSheetDialog.dismiss()

        }

    }

    /**
     * Search word receiving from activity an initiate search
     */
    fun onMessageEvent(event: SearchEventPeople) {
        mSearchWord = event.mWord
        if (mSearchWord != "") {
            singlePlayList?.clear()
            mAdapter?.notifyDataSetChanged()

            callApiCommon(
                mSearchWord, RemoteConstant.mOffset,
                RemoteConstant.mCount
            )
        }
    }
    /**
     * Search playlist API
     */
    private fun callApiCommon(mSearchWord: String, mOffset: String, mCount: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v1/search/playlist?playlist=" + mSearchWord + "&offset=" + mOffset + "&count=" + mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        searchViewModel.searchPlayList(mAuth, mOffset, mCount, mSearchWord)
    }
    /**
     * View all playlist API
     */
    private fun callApiAllPlayList(mOffset: String, mCount: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v1/music/playlist/all" + "?offset=" + mOffset + "&count=" + mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        searchViewModel.playListAll(mAuth, mOffset, mCount)


    }
}
