package com.example.sample.socialmob.view.ui.home_module.search

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.SearchArticleItemBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.ItemClick
import com.example.sample.socialmob.model.search.SearchArticleResponseModelPayloadContent

class SearchArticleAdapter(
    private val articleItems: MutableList<SearchArticleResponseModelPayloadContent>?,
    private val context: androidx.fragment.app.FragmentActivity?,
    private val searchArticleAdapterBookmarkClick: SearchArticleAdapterBookmarkClick,
    private val itemClick: ItemClick
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ProgressItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgerssViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<SearchArticleItemBinding>(
            LayoutInflater.from(parent.context), R.layout.search_article_item, parent, false
        )
        return ArticleViewHolder(binding)
    }


    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(articleItems!![position])
    }

    override fun getItemCount(): Int {
        return articleItems!!.size
    }

    inner class ProgerssViewHolder(val binding: ProgressItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    fun addAll(it: MutableList<SearchArticleResponseModelPayloadContent>?) {
        if (it?.size!! > 0) {
            val size = articleItems!!.size
            articleItems.clear() //here podcastList is an ArrayList populating the RecyclerView
            articleItems.addAll(it)// add new data
            notifyItemChanged(size, articleItems.size)// notify adapter of new data
        }
    }

    fun addLoader(it: MutableList<SearchArticleResponseModelPayloadContent>) {
        articleItems!!.addAll(it)
    }

    fun removeLoader() {
        if (articleItems!!.isNotEmpty()) {
            articleItems.removeAt(articleItems.size.minus(1))
        }
    }

    inner class ArticleViewHolder(val binding: SearchArticleItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.articleViewModel = articleItems!![adapterPosition]
            binding.executePendingBindings()
            itemView.setOnClickListener {
                itemClick.onItemClick(articleItems[adapterPosition].ContentId!!)
            }
            binding.bookmarkImageView.setOnClickListener {
                searchArticleAdapterBookmarkClick.onSearchArticleAdapterItemClick(
                    articleItems[adapterPosition].Bookmarked!!,
                    articleItems[adapterPosition].ContentId!!
                )
                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }

        }

    }
    private fun notify(adapterPosition: Int) {
        if (articleItems!![adapterPosition].Bookmarked == "true") {
            articleItems[adapterPosition].Bookmarked = "false"
        } else {
            articleItems[adapterPosition].Bookmarked = "true"
        }
        notifyItemChanged(adapterPosition, articleItems.size)
    }
    override fun getItemViewType(position: Int): Int {

        return if (articleItems!![position].ContentId != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }


    interface SearchArticleAdapterBookmarkClick {
        fun onSearchArticleAdapterItemClick(isBookMarked: String, mId: String)
    }
}
