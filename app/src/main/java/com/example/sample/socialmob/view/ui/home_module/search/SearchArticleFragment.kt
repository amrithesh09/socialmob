package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.backlog.articles.ArticleDetailsActivityNew
import com.example.sample.socialmob.view.ui.backlog.articles.BookmarkList
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.SearchEventArticle
import com.example.sample.socialmob.model.search.SearchArticleResponseModelPayloadContent
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class SearchArticleFragment : Fragment(), ItemClick,
    SearchArticleAdapter.SearchArticleAdapterBookmarkClick {
    companion object {
        private const val ARG_STRING = "mString"

        fun newInstance(mName: String): SearchArticleFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = SearchArticleFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var searchViewModel: SearchViewModel? = null
    private var binding: SearchInnerFragmentBinding? = null
    private var mGlobalArticle: MutableList<SearchArticleResponseModelPayloadContent>? = ArrayList()
    private var mArticleAdapter: SearchArticleAdapter? = null
    private var mSearchWord = ""
    private val visibleThreshold = 15

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 141) {
            if (data?.getSerializableExtra("mCommentList") != null) {
                val myList = data.getSerializableExtra("mCommentList") as ArrayList<BookmarkList>
                if (myList.size > 0) {
                    for (i in 0 until mGlobalArticle!!.size) {
                        for (i1 in 0 until myList.size) {
                            if (mGlobalArticle!![i].ContentId == myList[i1].mId) {
                                mGlobalArticle!![i].Bookmarked = myList[i1].mIsLike
                            }
                        }
                    }
                    mArticleAdapter!!.addAll(mGlobalArticle!!)
                    mArticleAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        val args = arguments
        val mName = args?.getString(ARG_STRING, "")
        binding!!.searchTitleTextView.text = mName.toString()


        // TODO : Pagination
        binding!!.searchInnerRecyclerView.layoutManager =
            WrapContentLinearLayoutManager(
                activity!!,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        mArticleAdapter = SearchArticleAdapter(
            mGlobalArticle, activity, this, this
        )
        binding!!.searchInnerRecyclerView.adapter = mArticleAdapter

        //TODO :
        observeList()
        // TODO : Swipe To Refresh
//        binding!!.nestedScrollView.viewTreeObserver?.addOnScrollChangedListener {
//            val view = binding!!.nestedScrollView.getChildAt(binding!!.nestedScrollView.childCount - 1)
//
//            val diff = view.bottom - (binding!!.nestedScrollView.height + binding!!.nestedScrollView.scrollY)
//
//            if (diff == 0) {
//                //your api call to fetch data
//                if (!searchViewModel!!.isLoadingArticle) {
//                    if (mSearchWord != "") {
//                        loadMoreItems(mGlobalArticle!!.size, mSearchWord)
//                    }
//
//                }
//            }
//        }

        binding!!.refreshTextView.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(mSearchWord)
            }
        }
    }


    private fun loadMoreItems(size: Int, mString: String?) {
        if (!searchViewModel!!.isLoadCompletedArticle) {
            if (activity != null) {
                val user: MutableList<SearchArticleResponseModelPayloadContent>? = ArrayList()
                val mList = SearchArticleResponseModelPayloadContent(
                    0, "", "", "", "", "",
                    "", "", ""
                )

                user!!.add(mList)
                mArticleAdapter!!.addLoader(user)
                mArticleAdapter!!.notifyDataSetChanged()
                val mAuth = RemoteConstant.getAuthSearchProfile(
                    activity!!,
                    RemoteConstant.pathContentSearch,
                    mString!!,
                    size.toString(),
                    RemoteConstant.mCount
                )
                searchViewModel!!.searchArticle(
                    mAuth,
                    size.toString(),
                    RemoteConstant.mCount,
                    mString
                )
            }

        }
    }


    private fun observeList() {
//        searchViewModel!!.searchArticleLiveData.nonNull().observe(this, Observer {
//            if (it!!.isNotEmpty()) {
//                binding!!.isVisibleList = true
//                binding!!.isVisibleLoading = false
//                binding!!.isVisibleNoData = false
//                binding!!.isVisibleNoInternet = false
//                mGlobalArticle = it as MutableList
//                mArticleAdapter!!.addAll(mGlobalArticle!!)
//            }
//        })

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        return binding!!.root
    }

    override fun onSearchArticleAdapterItemClick(isBookMarked: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isBookMarked) {
                "true" -> removeBookmarkApi(mId)
                "false" -> addToBookmarkApi(mId)
            }
        } else {
            AppUtils.showCommonToast(activity!!, getString(R.string.no_internet))
        }

    }

    private fun removeBookmarkApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathContent + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathBookmark, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel!!.removeBookmark(mAuth, mId)
    }

    private fun addToBookmarkApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathContent + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathBookmark, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel!!.addToBookmark(mAuth, mId)
    }

    override fun onItemClick(mString: String) {
        val intent = Intent(activity, ArticleDetailsActivityNew::class.java)
        intent.putExtra("mId", mString)
        intent.putExtra("isNeedToCallApi", true)
        startActivityForResult(intent, 141)

    }


    private fun observePaginatedList() {

        searchViewModel!!.paginatedArticleList.nonNull().observe(this, Observer {
            mGlobalArticle = it!!
            if (!searchViewModel!!.isLoadingArticle) {
                if (mGlobalArticle!!.isNotEmpty()) {
//                    binding!!.isVisibleList = true
//                    binding!!.isVisibleLoading = false
//                    binding!!.isVisibleNoData = false
//                    binding!!.isVisibleNoInternet = false
                    mArticleAdapter!!.removeLoader()
                    mArticleAdapter!!.addAll(it)
                    mArticleAdapter!!.notifyDataSetChanged()
                }
//                else {
//                    binding!!.isVisibleList = false
//                    binding!!.isVisibleLoading = false
//                    binding!!.isVisibleNoData = true
//                    binding!!.isVisibleNoInternet = false
//                }
                searchViewModel!!.isLoadingArticle = false
            }

        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SearchEventArticle) {
        mSearchWord = event.mWord
        callApiCommon(mSearchWord)
    }

    fun callApiCommon(mSearchWordNew: String) {
        mSearchWord = mSearchWordNew
        if (InternetUtil.isInternetOn()) {
            if (activity != null && binding != null) {
                val mAuth = RemoteConstant.getAuthSearchProfile(
                    activity!!,
                    RemoteConstant.pathContentSearch,
                    mSearchWord,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCount
                )
                searchViewModel!!.searchArticle(
                    mAuth,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCount,
                    mSearchWord
                )

//                binding!!.isVisibleList = false
//                binding!!.isVisibleLoading = true
//                binding!!.isVisibleNoData = false
//                binding!!.isVisibleNoInternet = false
                mGlobalArticle!!.clear()
                mArticleAdapter!!.addAll(mGlobalArticle!!)
                observePaginatedList()

            } else {
//                binding!!.isVisibleList = false
//                binding!!.isVisibleLoading = false
//                binding!!.isVisibleNoData = false
//                binding!!.isVisibleNoInternet = true
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding!!.noInternetLinear.startAnimation(shake) // starts animation
            }
        }
    }
}
