package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentArtistBinding
import com.example.sample.socialmob.model.music.Artist
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistViewPagerActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistsAdapter
import com.example.sample.socialmob.view.utils.GridSpacingItemDecoration
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.events.SearchEventHashTag
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchArtistFragment : Fragment(), ArtistsAdapter.ArtistClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: SearchInnerFragmentArtistBinding? = null
    private val searchViewModel: SearchViewModel by viewModels()
    private var mSearchWord = ""
    private var mAdapter: ArtistsAdapter? = null
    private var mStatus: ResultResponse.Status? = null
    private var isAddedLoader: Boolean = false
    private var mArtistList: MutableList<Artist>? = ArrayList()
    private var mName: String = ""

    companion object {
        private const val ARG_STRING = "mString"

        fun newInstance(mName: String): SearchArtistFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = SearchArtistFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: Bundle? = arguments
        mName = args?.getString(ARG_STRING, "")!!

        mAdapter = ArtistsAdapter(
            this, "searchArtist", requireActivity(), requestManager = glideRequestManager
        )

        binding?.searchInnerRecyclerView?.adapter = mAdapter


        if (mName == "AllArtist") {
            //TODO : Call API All Artist
            callApiAllArtist("0")

        } else {
            binding?.searchTitleTextView?.text = mName

            if (SearchFragment.mTopMusicData != null &&
                SearchFragment.mTopMusicData?.featuredArtists != null &&
                SearchFragment.mTopMusicData?.featuredArtists?.size!! > 0
            ) {
                mArtistList =
                    SearchFragment.mTopMusicData?.featuredArtists?.toMutableList()!!
                mAdapter?.submitList(mArtistList)
                mAdapter?.notifyDataSetChanged()
            }
        }

        binding?.searchInnerRecyclerView?.layoutManager = GridLayoutManager(activity, 3)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.five_dp)
        binding?.searchInnerRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(3, spacingInPixels, true)
        )


        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = true
//        binding?.searchInnerRecyclerView?.itemAnimator = null
        binding?.searchInnerRecyclerView?.addOnScrollListener(CustomScrollListener())

        observeArtist()
    }

    /**
     * Search srtist API
     */
    private fun callApiAllArtist(mLastId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mSlash + "api/v1/artist/list/" + mLastId + "/" + RemoteConstant.mCounttwenty,
            RemoteConstant.mGetMethod
        )
        val mAuth: String =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId, ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.getAllArtist(mAuth, mLastId, RemoteConstant.mCounttwenty)
    }

    private fun removeLoaderFormList() {
        if (mArtistList != null && mArtistList?.size!! > 0 && mArtistList?.get(
                mArtistList?.size!! - 1
            ) != null && mArtistList?.get(mArtistList?.size!! - 1)?.id == "0"
        ) {
            mArtistList?.removeAt(mArtistList?.size!! - 1)
            mAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }

    private fun observeArtist() {

        searchViewModel.artistLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status
                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mArtistList?.clear()
                        mArtistList = resultResponse?.data!!
                        mAdapter?.submitList(mArtistList!!)
                        mAdapter?.notifyDataSetChanged()

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mArtistList?.addAll(resultResponse?.data!!)
                        mAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {

                        if (mArtistList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_no_search_data).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }

            })

    }

    /**
     * Pagination scroll listener ( Artist API with offset )
     */
    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager?.childCount!!
                val totalItemCount = recyclerView.layoutManager?.itemCount!!
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as GridLayoutManager).findFirstVisibleItemPosition()

//                var lastItem = mAdapter?.itemCount!! - 1;
//                tryAnimation( (recyclerView.layoutManager as GridLayoutManager).findViewByPosition(lastItem))


                if (mArtistList?.size!! >= 15 && mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                    visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                ) {
                    if (mName == "AllArtist") {
                        if (!isAddedLoader) {
                            callApiAllArtist(mArtistList?.get(mArtistList?.size?.minus(1)!!)?.id!!)
                            val mLoaderList: MutableList<Artist> = ArrayList()
                            val mList = Artist()
                            mList.id = "0"
                            mLoaderList.add(mList)
                            mArtistList?.addAll(mLoaderList)
                            mAdapter?.notifyDataSetChanged()

                            isAddedLoader = true
                        }
                    } else {

                        if (mSearchWord != "" && !isAddedLoader) {
                            callApiCommon(
                                mSearchWord,
                                mArtistList?.size!!.toString(),
                                RemoteConstant.mCounttwenty
                            )
                            val mLoaderList: MutableList<Artist> = ArrayList()
                            val mList = Artist()
                            mList.id = "0"
                            mLoaderList.add(mList)
                            mArtistList?.addAll(mLoaderList)
                            mAdapter?.notifyDataSetChanged()

                            isAddedLoader = true
                        }
                    }

                }
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater, R.layout.search_inner_fragment_artist, container,
                false
            )
        return binding?.root
    }
    /**
     * If we have an list of artist passing list to Artist viewpager and populate view
     */
    override fun onArtistClickListener(
        mArtistId: String, mArtistPosition: String, mIsFrom: String
    ) {
        val detailIntent = Intent(activity, ArtistViewPagerActivity::class.java)
        detailIntent.putParcelableArrayListExtra(
            "mArtistList",
            mArtistList as java.util.ArrayList<out Parcelable>
        )
        detailIntent.putExtra("mArtistPosition", mArtistPosition)
        startActivity(detailIntent)
        activity?.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    fun onMessageEvent(event: SearchEventHashTag) {
        mSearchWord = event.mWord
        callApiCommon(mSearchWord)

    }

    fun callApiCommon(mSearchWordNew: String) {
        mSearchWord = mSearchWordNew
        mArtistList?.clear()
        mAdapter?.notifyDataSetChanged()

        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = true
        callApiCommon(
            mSearchWord, RemoteConstant.mOffset,
            RemoteConstant.mCounttwenty
        )
    }
    /**
     *  Search artist API
     */
    private fun callApiCommon(mSearchWord: String, mOffset: String, mCount: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v1/search/artist?artist=" + mSearchWord + "&offset=" + mOffset + "&count=" + mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        searchViewModel.searchArtist(mAuth, mOffset, mCount, mSearchWord)
    }
}
