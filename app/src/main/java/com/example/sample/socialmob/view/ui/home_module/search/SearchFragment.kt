package com.example.sample.socialmob.view.ui.home_module.search

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchFragmentBinding
import com.example.sample.socialmob.model.feed.TopItemsNetworkResponsePayload
import com.example.sample.socialmob.model.music.TopItemsMusicPayload
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.ViewPagerListener
import com.example.sample.socialmob.view.utils.events.SearchEventChat
import com.example.sample.socialmob.view.utils.events.SearchEventHashTag
import com.example.sample.socialmob.view.utils.events.SearchEventPeople
import com.example.sample.socialmob.view.utils.events.SearchEventTrack
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.common_activity.*
import kotlinx.android.synthetic.main.search_fragment.*
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit
import javax.inject.Inject
/**
 * Common page for ( Track,Artist,Hashtag,Playlist,Users)
 */
@AndroidEntryPoint
class SearchFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private val searchViewModel: SearchViewModel by viewModels()
    private var searchFragmentBinding: SearchFragmentBinding? = null
    private var isLoaded = false
    private var fragmentAdapter: SearchPageAdapter? = null
    private val ARG_BOOLEAN = "isFrom"
    private val ARG_BOOLEAN_LONG_PRESS = "isLongPress"
    private var mIsFrom = ""
    private var isLongPress = ""
    private var disposible: Disposable? = null

    companion object {
        var mTopMusicData: TopItemsMusicPayload? = null
        var mTopNetworkData: TopItemsNetworkResponsePayload? = null
    }

    fun newInstance(isFrom: String, longPress: String): SearchFragment {
        val args = Bundle()
        args.putSerializable(ARG_BOOLEAN, isFrom)
        args.putSerializable(ARG_BOOLEAN_LONG_PRESS, longPress)
        val fragment = SearchFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val args: Bundle = requireArguments()
        mIsFrom = args.getString(ARG_BOOLEAN, "")!!
        isLongPress = args.getString(ARG_BOOLEAN_LONG_PRESS, "")!!

        if (mIsFrom == "ChatSearch" || mIsFrom == "CreateNewChat") {
            searchFragmentBinding?.tabsMain?.visibility = View.GONE
            setAdapter()
        } else if (mIsFrom == "FeedSearch") {
            setAdapter()
        }

//        if (isLongPress != "") {
//            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
//
//            val handler = Handler()
//            handler.postDelayed({
//                searchFragmentBinding?.searchEditText?.requestFocus()
//                val mgr =
//                    activity?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
//                mgr.showSoftInput(searchFragmentBinding?.searchEditText, SHOW_IMPLICIT)
//            }, 100)
//        } else {
//            AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText)
//        }


        callApiCommon()
        searchFragmentBinding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }

//        searchFragmentBinding?.searchRelative?.background =
//            activity?.getDrawable(R.drawable.rounded_search_inactive)
        searchFragmentBinding?.searchEditText?.hint = activity?.getString(R.string.search_)

        val handler = Handler()
        handler.postDelayed({
            searchFragmentBinding?.searchEditText?.post {
                searchFragmentBinding?.searchEditText?.requestFocus()
                AppUtils.showKeyboard(requireActivity(), searchFragmentBinding?.searchEditText)
            }
        }, 1000)

        val mMode =
            SharedPrefsUtils.getBooleanPreference(activity, RemoteConstant.NIGHT_MODE, false)
        if (mMode) {
            DrawableCompat.setTint(
                searchFragmentBinding?.searchBackImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
            )
            DrawableCompat.setTint(
                searchFragmentBinding?.searchImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
            )
        } else {
            DrawableCompat.setTint(
                searchFragmentBinding?.searchBackImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.search_color_in_active)
            )
            DrawableCompat.setTint(
                searchFragmentBinding?.searchImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.search_color_in_active)
            )
        }

        try {
            searchFragmentBinding?.searchEditText?.setOnFocusChangeListener { view, b ->
                if (b) {
                    if (mMode) {
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchBackImageView?.drawable!!,
                            ContextCompat.getColor(view.context, R.color.line_color_new)
                        )
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchImageView?.drawable!!,
                            ContextCompat.getColor(view.context, R.color.line_color_new)
                        )
                    } else {
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchBackImageView?.drawable!!,
                            ContextCompat.getColor(
                                view.context,
                                R.color.search_color_in_active
                            )
                        )
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchImageView?.drawable!!,
                            ContextCompat.getColor(
                                view.context,
                                R.color.search_color_in_active
                            )
                        )
                    }
                } else {
//                searchFragmentBinding?.searchRelative?.background =
//                    activity?.getDrawable(R.drawable.rounded_search_inactive)
                    searchFragmentBinding?.searchEditText?.hint = activity?.getString(R.string.search_)
                    if (mMode) {
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchBackImageView?.drawable!!,
                            ContextCompat.getColor(view.context, R.color.line_color_new)
                        )
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchImageView?.drawable!!,
                            ContextCompat.getColor(view.context, R.color.line_color_new)
                        )
                    } else {
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchBackImageView?.drawable!!,
                            ContextCompat.getColor(
                                view.context,
                                R.color.search_color_in_active
                            )
                        )
                        DrawableCompat.setTint(
                            searchFragmentBinding?.searchImageView?.drawable!!,
                            ContextCompat.getColor(
                                view.context,
                                R.color.search_color_in_active
                            )
                        )
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "setOnFocusChangeListener_" + e.toString())
        }

        searchFragmentBinding?.searchBackImageView?.setOnClickListener {
            AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchBackImageView!!)
            (activity as CommonFragmentActivity).finish()
        }

        searchFragmentBinding?.searchImageView?.setOnClickListener {
            // TODO : Check Internet connection
            if (InternetUtil.isInternetOn()) {
                if (searchFragmentBinding?.searchEditText?.text.toString().isNotEmpty()) {
                    AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText!!)
                    onPageSelected(search_viewpager.currentItem)

                } else {
                    AppUtils.showCommonToast(requireActivity(), "Search word cannot be empty")
                }
            }
        }

        disposible = searchFragmentBinding?.searchEditText?.addRxTextWatcher()
            ?.debounce(400, TimeUnit.MILLISECONDS)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(AndroidSchedulers.mainThread())
            ?.subscribe {
                if (!TextUtils.isEmpty(it)) {
                    onPageSelected(search_viewpager.currentItem)
                }
            }
        searchFragmentBinding?.searchEditText?.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchFragmentBinding?.searchImageView?.performClick()

                return@OnEditorActionListener true
            }
            false
        })

    }


   /* override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: Bundle = requireArguments()
        mIsFrom = args.getString(ARG_BOOLEAN, "")!!
        isLongPress = args.getString(ARG_BOOLEAN_LONG_PRESS, "")!!

        if (mIsFrom == "ChatSearch" || mIsFrom == "CreateNewChat") {
            searchFragmentBinding?.tabsMain?.visibility = View.GONE
            setAdapter()
        } else if (mIsFrom == "FeedSearch") {
            setAdapter()
        }

//        if (isLongPress != "") {
//            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
//
//            val handler = Handler()
//            handler.postDelayed({
//                searchFragmentBinding?.searchEditText?.requestFocus()
//                val mgr =
//                    activity?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
//                mgr.showSoftInput(searchFragmentBinding?.searchEditText, SHOW_IMPLICIT)
//            }, 100)
//        } else {
//            AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText)
//        }


        callApiCommon()
        searchFragmentBinding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }

//        searchFragmentBinding?.searchRelative?.background =
//            activity?.getDrawable(R.drawable.rounded_search_inactive)
        searchFragmentBinding?.searchEditText?.hint = activity?.getString(R.string.search_)

        val handler = Handler()
        handler.postDelayed({
            searchFragmentBinding?.searchEditText?.post {
                searchFragmentBinding?.searchEditText?.requestFocus()
                AppUtils.showKeyboard(requireActivity(), searchFragmentBinding?.searchEditText)
            }
        }, 1000)

        val mMode =
            SharedPrefsUtils.getBooleanPreference(activity, RemoteConstant.NIGHT_MODE, false)
        if (mMode) {
            DrawableCompat.setTint(
                searchFragmentBinding?.searchBackImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
            )
            DrawableCompat.setTint(
                searchFragmentBinding?.searchImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
            )
        } else {

            DrawableCompat.setTint(
                searchFragmentBinding?.searchBackImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.search_color_in_active)
            )
            DrawableCompat.setTint(
                searchFragmentBinding?.searchImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.search_color_in_active)
            )
        }

        try {
            searchFragmentBinding?.searchEditText?.setOnFocusChangeListener { view, b ->
                if (b) {
                    if (mMode) {
                        if (activity != null) {
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchBackImageView?.drawable!!,
                                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
                            )
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchImageView?.drawable!!,
                                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
                            )
                        }
                    } else {
                        if (activity != null) {
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchBackImageView?.drawable!!,
                                ContextCompat.getColor(
                                    requireActivity(),
                                    R.color.search_color_in_active
                                )
                            )
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchImageView?.drawable!!,
                                ContextCompat.getColor(
                                    requireActivity(),
                                    R.color.search_color_in_active
                                )
                            )
                        }
                    }
                } else {
//                searchFragmentBinding?.searchRelative?.background =
//                    activity?.getDrawable(R.drawable.rounded_search_inactive)
                    searchFragmentBinding?.searchEditText?.hint = activity?.getString(R.string.search_)
                    if (mMode) {
                        if (activity != null) {
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchBackImageView?.drawable!!,
                                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
                            )
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchImageView?.drawable!!,
                                ContextCompat.getColor(requireActivity(), R.color.line_color_new)
                            )
                        }
                    } else {
                        if (activity != null) {
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchBackImageView?.drawable!!,
                                ContextCompat.getColor(
                                    requireActivity(),
                                    R.color.search_color_in_active
                                )
                            )
                            DrawableCompat.setTint(
                                searchFragmentBinding?.searchImageView?.drawable!!,
                                ContextCompat.getColor(
                                    requireActivity(),
                                    R.color.search_color_in_active
                                )
                            )
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "setOnFocusChangeListener_" + e.toString())
        }

        searchFragmentBinding?.searchBackImageView?.setOnClickListener {
            AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchBackImageView!!)
            (activity as CommonFragmentActivity).finish()
        }

        searchFragmentBinding?.searchImageView?.setOnClickListener {
            // TODO : Check Internet connection
            if (InternetUtil.isInternetOn()) {
                if (searchFragmentBinding?.searchEditText?.text.toString().isNotEmpty()) {
                    AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText!!)
                    onPageSelected(search_viewpager.currentItem)

                } else {
                    AppUtils.showCommonToast(requireActivity(), "Search word cannot be empty")
                }
            }
        }

        disposible = searchFragmentBinding?.searchEditText?.addRxTextWatcher()
            ?.debounce(400, TimeUnit.MILLISECONDS)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(AndroidSchedulers.mainThread())
            ?.subscribe {
                if (!TextUtils.isEmpty(it)) {
                    onPageSelected(search_viewpager.currentItem)
                }
            }
        searchFragmentBinding?.searchEditText?.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchFragmentBinding?.searchImageView?.performClick()

                return@OnEditorActionListener true
            }
            false
        })

    }*/

    override fun onDestroy() {
        super.onDestroy()
        if (disposible != null && !disposible?.isDisposed!!) {
            disposible?.dispose()
        }
    }

    private fun AutoCompleteTextView.addRxTextWatcher(): Observable<String?> {

        return Observable.create {
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    it.onNext(s?.toString()!!)
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }
            })
        }
    }


    private fun callApiCommon() {
        if (InternetUtil.isInternetOn()) {
            if (!isLoaded && mIsFrom == "CreateNewChat") {
                callApiTopNetwork()
                observeDataNetwork()
            }
            if (!isLoaded && mIsFrom == "SearchChat") {
                callApiTopNetwork()
                observeDataNetwork()
            }
            if (!isLoaded && mIsFrom == "FeedSearch") {
                callApiTopNetwork()
                observeDataNetwork()
            }

            if (!isLoaded && mIsFrom == "MusicSearch") {
                callTopListMusic()
                observeDataTopListMusic()

            }
        } else {
            if (!isLoaded && mIsFrom == "FeedSearch") {
                observeDataNetwork()
                searchViewModel.topItemsNetworkLiveData?.postValue(
                    ResultResponse(
                        ResultResponse.Status.NO_INTERNET,
                        TopItemsNetworkResponsePayload(),
                        ""
                    )
                )
            }
            if (!isLoaded && mIsFrom == "MusicSearch") {
                observeDataTopListMusic()
                searchViewModel.topItemsMusicLiveData?.postValue(
                    ResultResponse(
                        ResultResponse.Status.NO_INTERNET,
                        TopItemsMusicPayload(), ""
                    )
                )
            }
        }

    }


    private fun observeDataNetwork() {

        searchViewModel.topItemsNetworkLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                if (resultResponse != null && resultResponse.status == ResultResponse.Status.SUCCESS) {
                    mTopNetworkData = resultResponse.data
                    searchFragmentBinding?.progressLinear?.visibility = View.GONE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.GONE
                    searchFragmentBinding?.searchViewpager?.visibility = View.VISIBLE
                    if (fragmentAdapter?.getRegisteredFragment(0) != null) {
                        val fragment: Fragment =
                            fragmentAdapter?.getRegisteredFragment(0)!!
                        if (fragment is SearchPeopleFragment) {
                            fragment.updateProfileData()
                        }
                    }
                    if (fragmentAdapter?.getRegisteredFragment(1) != null) {
                        val fragment: Fragment =
                            fragmentAdapter?.getRegisteredFragment(1)!!
                        if (fragment is SearchHashTagsFragment) {
                            fragment.updateHashTagData()
                        }
                    }


                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.ERROR) {

                    searchFragmentBinding?.progressLinear?.visibility = View.GONE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
                    searchFragmentBinding?.searchViewpager?.visibility = View.GONE

                    glideRequestManager.load(R.drawable.server_error).apply(
                        RequestOptions().fitCenter())
                        .into(searchFragmentBinding?.noInternetImageView!!)
                    searchFragmentBinding?.noInternetTextView?.text =
                        getString(R.string.server_error)
                    searchFragmentBinding?.noInternetSecTextView?.text =
                        getString(R.string.server_error_content)

                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.LOADING) {
                    searchFragmentBinding?.progressLinear?.visibility = View.VISIBLE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.GONE
                    searchFragmentBinding?.searchViewpager?.visibility = View.GONE
                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.NO_INTERNET) {
                    glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                        .into(searchFragmentBinding?.noInternetImageView!!)
                    searchFragmentBinding?.progressLinear?.visibility = View.GONE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
                    searchFragmentBinding?.searchViewpager?.visibility = View.GONE
                    val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                    searchFragmentBinding?.noInternetLinear?.startAnimation(shake) // starts animation
                }

            })


    }

    private fun observeDataTopListMusic() {
        searchViewModel.topItemsMusicLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                if (resultResponse != null && resultResponse.status == ResultResponse.Status.SUCCESS) {
                    mTopMusicData = resultResponse.data
                    searchFragmentBinding?.progressLinear?.visibility = View.GONE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.GONE
                    searchFragmentBinding?.searchViewpager?.visibility = View.VISIBLE

                    setAdapter()

                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.ERROR) {

                    searchFragmentBinding?.progressLinear?.visibility = View.GONE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
                    searchFragmentBinding?.searchViewpager?.visibility = View.GONE

                    glideRequestManager.load(R.drawable.server_error).apply(
                        RequestOptions().fitCenter())
                        .into(searchFragmentBinding?.noInternetImageView!!)
                    searchFragmentBinding?.noInternetTextView?.text =
                        getString(R.string.server_error)
                    searchFragmentBinding?.noInternetSecTextView?.text =
                        getString(R.string.server_error_content)

                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.LOADING) {
                    searchFragmentBinding?.progressLinear?.visibility = View.VISIBLE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.GONE
                    searchFragmentBinding?.searchViewpager?.visibility = View.GONE
                } else if (resultResponse != null && resultResponse.status == ResultResponse.Status.NO_INTERNET) {
                    glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                        .into(searchFragmentBinding?.noInternetImageView!!)
                    searchFragmentBinding?.progressLinear?.visibility = View.GONE
                    searchFragmentBinding?.noInternetLinear?.visibility = View.VISIBLE
                    searchFragmentBinding?.searchViewpager?.visibility = View.GONE
                    val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                    searchFragmentBinding?.noInternetLinear?.startAnimation(shake) // starts animation
                }

            })

    }

    private fun setAdapter() {

        fragmentAdapter = SearchPageAdapter(childFragmentManager, mIsFrom)
        if (searchFragmentBinding?.searchViewpager?.adapter == null) {
            searchFragmentBinding?.searchViewpager?.adapter = fragmentAdapter
            searchFragmentBinding?.tabsMain?.setupWithViewPager(searchFragmentBinding?.searchViewpager)
        }
        searchFragmentBinding?.searchViewpager?.addOnPageChangeListener(ViewPagerListener(this::onPageSelected))
    }

    private fun callTopListMusic() {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.topItemsMusic, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mAppId, ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.searchTopListMusic(mAuth)
    }

    private fun callApiTopNetwork() {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.topItemsNetworking, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mAppId, ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.searchTopNetwork(mAuth)
        isLoaded = true

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        searchFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.search_fragment, container, false)

        searchFragmentBinding?.searchViewModel = searchViewModel

        return searchFragmentBinding?.root
    }

    private fun onPageSelected(position: Int) {


        if (searchFragmentBinding?.searchEditText?.text?.isNotEmpty()!!) {
            when (mIsFrom) {
                "ChatSearch" -> {
                    when (position) {
                        0 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                EventBus.getDefault()
                                    .post(
                                        SearchEventChat(
                                            searchFragmentBinding?.searchEditText?.text.toString()
                                        )
                                    )

                            }
                        }
                    }
                }
                "FeedSearch" -> {
                    when (position) {
                        0 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                if (fragmentAdapter?.getRegisteredFragment(0) != null) {
                                    val fragment: Fragment =
                                        fragmentAdapter?.getRegisteredFragment(0)!!
                                    if (fragment is SearchPeopleFragment) {
                                        fragment.onMessageEvent(
                                            SearchEventPeople(searchFragmentBinding?.searchEditText?.text.toString())
                                        )
                                    }
                                }
                            }
                        }
                        //                1 -> {
                        //                    if (searchFragmentBinding?.searchEditText?.text.toString().isNotEmpty()) {
                        //                        AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText)
                        //                        EventBus.getDefault()
                        //                            .post(SearchEventTrack(searchFragmentBinding?.searchEditText?.text.toString()))
                        //
                        //                    }
                        //                }
                        //                2 -> {
                        //                    if (searchFragmentBinding?.searchEditText.text.toString().isNotEmpty()) {
                        //                        AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText)
                        //                        EventBus.getDefault()
                        //                            .post(SearchEventArticle(searchFragmentBinding?.searchEditText.text.toString()))
                        //
                        //                    }
                        //                }
                        1 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                if (fragmentAdapter?.getRegisteredFragment(1) != null) {
                                    val fragment: Fragment =
                                        fragmentAdapter?.getRegisteredFragment(1)!!
                                    if (fragment is SearchHashTagsFragment) {
                                        fragment.onMessageEvent(
                                            SearchEventHashTag(searchFragmentBinding?.searchEditText?.text.toString())
                                        )
                                    }
                                }
                            }
                        }
                        //                3 -> {
                        //                    if (searchFragmentBinding?.searchEditText.text.toString().isNotEmpty()) {
                        //                        AppUtils.hideKeyboard(activity, searchFragmentBinding?.searchEditText)
                        //                        EventBus.getDefault()
                        //                            .post(SearchEvent(searchFragmentBinding?.searchEditText.text.toString()))
                        //
                        //                    }
                        //                }


                    }
                }
                "CreateNewChat" -> {
                    when (position) {
                        0 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                EventBus.getDefault().post(
                                    SearchEventPeople(
                                        searchFragmentBinding?.searchEditText?.text.toString()
                                    )
                                )
                            }
                        }
                    }
                }
                else -> {
                    when (position) {
                        0 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                if (fragmentAdapter?.getRegisteredFragment(0) != null) {
                                    val fragment: Fragment =
                                        fragmentAdapter?.getRegisteredFragment(0)!!
                                    if (fragment is SearchTrackFragment) {
                                        fragment.onMessageEvent(
                                            SearchEventTrack(searchFragmentBinding?.searchEditText?.text.toString())
                                        )
                                    }
                                }
                            }
                        }
                        1 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                if (fragmentAdapter?.getRegisteredFragment(1) != null) {
                                    val fragment: Fragment =
                                        fragmentAdapter?.getRegisteredFragment(1)!!
                                    if (fragment is SearchArtistFragment) {
                                        fragment.onMessageEvent(
                                            SearchEventHashTag(searchFragmentBinding?.searchEditText?.text.toString())
                                        )
                                    }
                                }
                            }
                        }
                        2 -> {
                            if (searchFragmentBinding?.searchEditText?.text.toString()
                                    .isNotEmpty()
                            ) {
                                if (fragmentAdapter?.getRegisteredFragment(2) != null) {
                                    val fragment: Fragment =
                                        fragmentAdapter?.getRegisteredFragment(2)!!
                                    if (fragment is PlayListFragment) {
                                        fragment.onMessageEvent(
                                            SearchEventPeople(searchFragmentBinding?.searchEditText?.text.toString())
                                        )
                                    }
                                }
                                EventBus.getDefault()
                                    .post(
                                        SearchEventPeople(
                                            searchFragmentBinding?.searchEditText?.text.toString()
                                        )
                                    )

                            }
                        }
                    }
                }
            }
        }

    }


}
