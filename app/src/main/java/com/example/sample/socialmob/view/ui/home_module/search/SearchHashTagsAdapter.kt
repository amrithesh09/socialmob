package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Intent
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.HashTagItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.ui.home_module.settings.BlockedUserAdapter
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.model.search.HashTag

class SearchHashTagsAdapter(
    private val hashTagItems: MutableList<HashTag>?,
    private val context: FragmentActivity?
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    private var mLastClickTime: Long = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            BlockedUserAdapter.ITEM_DATA -> bindData(parent)
            BlockedUserAdapter.ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: HashTagItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.hash_tag_item, parent, false
        )
        return HashTagViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(hashTagItems?.get(holder.adapterPosition))

    }

    override fun getItemCount(): Int {
        return hashTagItems?.size!!
    }

    override fun getItemViewType(position: Int): Int {

        return if (hashTagItems?.get(position)?._id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    inner class HashTagViewHolder(val binding: HashTagItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.hashTagViewModel = hashTagItems?.get(adapterPosition)
            binding.executePendingBindings()

            itemView.setOnClickListener {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return@setOnClickListener
                }else{
                    val detailIntent = Intent(context, PhotoGridDetailsActivity::class.java)
                    detailIntent.putExtra("mId", hashTagItems?.get(adapterPosition)?._id)
                    detailIntent.putExtra("mPosition", "")
                    detailIntent.putExtra("mPath", "hashTagFeed")
                    context?.startActivity(detailIntent)
                }
                mLastClickTime = SystemClock.elapsedRealtime()
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    fun swap(mutableList: MutableList<HashTag>?) {
        val diffCallback =
            SingleConversationListDiffUtils(
                this.hashTagItems!!,
                mutableList!!
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.hashTagItems.clear()
        this.hashTagItems.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    private class SingleConversationListDiffUtils(
        private val oldList: MutableList<HashTag>,
        private val newList: MutableList<HashTag>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition]._id == newList[newItemPosition]._id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition]._id == newList[newItemPosition]._id

        }

    }
}

