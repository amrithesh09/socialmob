package com.example.sample.socialmob.view.ui.home_module.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.view.utils.events.SearchEventHashTag
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchHashTagsFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private val searchViewModel: SearchViewModel by viewModels()
    private var binding: SearchInnerFragmentBinding? = null
    private var mGlobalHashTagList: MutableList<HashTag>? = ArrayList()
    private var mSearchWord = ""
    private var mHashTagAdapter: SearchHashTagsAdapter? = null
    private var mStatus: ResultResponse.Status? = null
    private var isAddedLoader: Boolean = false

    companion object {
        private const val ARG_STRING = "mString"

        fun newInstance(mName: String): SearchHashTagsFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = SearchHashTagsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: Bundle = requireArguments()
        val mName: String = args.getString(ARG_STRING, "")
        binding?.searchTitleTextView?.text = mName

        //TODO :
        binding?.searchInnerRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                requireActivity(), LinearLayoutManager.VERTICAL, false
            )

        if (SearchFragment.mTopNetworkData != null && SearchFragment.mTopNetworkData?.hashTags != null && SearchFragment.mTopNetworkData?.hashTags?.size ?: 0 > 0) {
            mGlobalHashTagList = SearchFragment.mTopNetworkData?.hashTags?.toMutableList()
        }
        mHashTagAdapter = SearchHashTagsAdapter(mGlobalHashTagList, activity)
        binding?.searchInnerRecyclerView?.adapter = mHashTagAdapter


        updateHashTagData()
        observeList()

        binding?.searchInnerRecyclerView?.addOnScrollListener(CustomScrollListener())
        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = false
        binding?.refreshTextView?.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(
                    mSearchWord,
                    mGlobalHashTagList?.size.toString(),
                    RemoteConstant.mCount
                )
            }
        }
    }
    /**
     * Pagination - Hash-tag API
     */
    inner class CustomScrollListener : RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                    visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                ) {
                    if (mSearchWord != "" && !isAddedLoader) {
                        callApiCommon(
                            mSearchWord, mGlobalHashTagList?.size.toString(), RemoteConstant.mCount
                        )
                        val mTrack: MutableList<HashTag> = ArrayList()
                        val mList = HashTag(0, "", "")
                        mTrack.add(mList)
                        mHashTagAdapter?.swap(mGlobalHashTagList)

                        isAddedLoader = true
                    }
                }
            }
        }
    }

    private fun observeList() {
        searchViewModel.searchHashTagLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mGlobalHashTagList?.clear()
                        mGlobalHashTagList = resultResponse?.data!!
                        mHashTagAdapter?.swap(mGlobalHashTagList)
                        mHashTagAdapter?.notifyDataSetChanged()

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            mGlobalHashTagList?.removeAt(mGlobalHashTagList?.size?.minus(1) ?: 0)
                            mHashTagAdapter?.swap(mGlobalHashTagList)
                            mHashTagAdapter?.notifyDataSetChanged()
                            isAddedLoader = false
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            mGlobalHashTagList?.removeAt(mGlobalHashTagList?.size?.minus(1) ?: 0)
                            mHashTagAdapter?.swap(mGlobalHashTagList)
                            mHashTagAdapter?.notifyDataSetChanged()
                            isAddedLoader = false
                        }
                        mGlobalHashTagList?.addAll(resultResponse?.data!!)
                        mHashTagAdapter?.swap(mGlobalHashTagList)
                        mHashTagAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {
                        if (mGlobalHashTagList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                mGlobalHashTagList?.removeAt(
                                    mGlobalHashTagList?.size?.minus(1) ?: 0
                                )
                                mHashTagAdapter?.swap(mGlobalHashTagList)
                                mHashTagAdapter?.notifyDataSetChanged()
                                isAddedLoader = false
                            }
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_no_search_data).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)

        return binding?.root
    }

    fun onMessageEvent(event: SearchEventHashTag) {
        mSearchWord = event.mWord
        mGlobalHashTagList?.clear()
        mHashTagAdapter?.swap(mGlobalHashTagList)
        callApiCommon(mSearchWord, "0", RemoteConstant.mCount)
    }

    fun callApiCommon(mSearchWordNew: String, mOffset: String, mCount: String) {
        mSearchWord = mSearchWordNew
        if (InternetUtil.isInternetOn()) {
            if (activity != null && binding != null) {
                val mAuth = RemoteConstant.getAuthSearchHashTag(
                    requireActivity(),
                    RemoteConstant.pathHashTagSearch,
                    mSearchWord,
                    mOffset,
                    mCount
                )
                searchViewModel.searchHashTags(mAuth, mOffset, mCount, mSearchWord)

            } else {
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            }
        }
    }

    fun updateHashTagData() {
        if (SearchFragment.mTopNetworkData != null && SearchFragment.mTopNetworkData?.hashTags != null && SearchFragment.mTopNetworkData?.hashTags?.size ?: 0 > 0) {
            mGlobalHashTagList = SearchFragment.mTopNetworkData?.hashTags?.toMutableList()
        }
        mHashTagAdapter?.swap(mGlobalHashTagList)
        mHashTagAdapter?.notifyDataSetChanged()

    }
}
