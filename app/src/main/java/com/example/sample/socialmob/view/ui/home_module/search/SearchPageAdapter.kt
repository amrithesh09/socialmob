package com.example.sample.socialmob.view.ui.home_module.search

import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sample.socialmob.view.ui.home_module.music.genre.GenreFragment
/**
 * Check and set-adapter corresponding search pages
 */
class SearchPageAdapter(fm: FragmentManager, private val mIsFrom: String) :
    FragmentStatePagerAdapter(fm) {

    private val registeredFragments: SparseArray<Fragment> = SparseArray()
    override fun getItem(position: Int): Fragment {
        when (mIsFrom) {
            "Discover" -> {
                return when (position) {
                    0 -> SearchArtistFragment.newInstance("AllArtist")
                    else -> GenreFragment()
                }
            }
            "FeedSearch" -> {
                return when (position) {
                    0 -> SearchPeopleFragment.newInstance("Suggested mobbers")
                    //            1 -> SearchTrackFragment.newInstance("SUGGESTED TRACKS")
                    //            2 -> SearchArticleFragment.newInstance("SUGGESTED ARTICLES")
                    else -> SearchHashTagsFragment.newInstance("Trending hashtags")
                    //            else -> return SearchTagsFragment.newInstance("SUGGESTED TAGS")


                }
            }
            "ChatSearch" -> {
//                return SearchPeopleChatFragment.newInstance("SearchChat")
                return ChatSearchFragment.newInstance("ChatSearch")
            }
            "CreateNewChat" -> {
                return SearchPeopleFragment.newInstance("CreateNewChat")
            }
            else -> {
                return when (position) {
                    0 -> SearchTrackFragment.newInstance("Suggested tracks")
                    1 -> SearchArtistFragment.newInstance("Artists you may like")
                    else -> PlayListFragment.newInstance("Playlists for you")
                }
            }
        }
    }

    override fun getCount(): Int {
        return if (mIsFrom == "FeedSearch" || mIsFrom == "Discover") {
            2
        } else if (mIsFrom == "ChatSearch") {
            1
        } else {
            3
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        when (mIsFrom) {
            "SearchChat" -> {
                return when (position) {
                    0 -> "People"
                    else -> "Hashtags"

                }
            }
            "Discover" -> {
                return when (position) {
                    0 -> "Artists"
                    else -> "Genre"

                }
            }
            "FeedSearch" -> {
                return when (position) {
                    0 -> "People"
                    // 1 -> "Tracks"
                    // 2 -> "Articles"
                    else -> "Hashtags"
                    // else -> "Tags"

                }
            }
            else -> {
                return when (position) {
                    0 -> "Tracks"
                    1 -> "Artists"
                    else -> "Playlists"

                }
            }
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        registeredFragments.put(position, fragment)
        return fragment
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return registeredFragments.get(position)
    }
}