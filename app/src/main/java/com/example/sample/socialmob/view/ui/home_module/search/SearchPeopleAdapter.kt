package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemFollowBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import org.jetbrains.anko.layoutInflater

class SearchPeopleAdapter(
    private var profileItems: MutableList<SearchProfileResponseModelPayloadProfile>?,
    private val searchPeopleAdapterFollowItemClick: SearchPeopleAdapterFollowItemClick,
    private val searchPeopleAdapterItemClick: SearchPeopleAdapterItemClick,
    private val mContext: Context,
    private val mIsFrom: String,
    private val mSendMessage: SendMessage,
    private var glideRequestManager: RequestManager
) :
    RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFollowBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_follow, parent, false
        )
        return ProfileViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(profileItems?.get(position))
    }

    override fun getItemCount(): Int {
        return profileItems?.size!!
    }

    inner class ProfileViewHolder(val binding: LayoutItemFollowBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.profileViewModel = profileItems?.get(adapterPosition)
            binding.executePendingBindings()
            if (profileItems?.get(adapterPosition)?.pimage != null) {
                glideRequestManager
                    .load(profileItems?.get(adapterPosition)?.pimage)
                    .thumbnail(0.1f)
                    .apply(
                        RequestOptions.placeholderOf(R.drawable.emptypicture)
                            .error(R.drawable.emptypicture)
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                    )
                    .into(binding.profileImageView)
            } else {
                glideRequestManager
                    .load(R.drawable.emptypicture).into(binding.profileImageView)
            }
            binding.followingTextView.setOnClickListener {
                if (profileItems?.size ?: 0 > 0 && profileItems?.get(adapterPosition)?.relation != null) {
                    if (profileItems?.get(adapterPosition)?.relation == "following") {
                        // If user is following we need to show confirmation dialog to un-follow
                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton =
                            dialogView.findViewById<View>(R.id.cancel_button) as Button
                        val okButton = dialogView.findViewById<View>(R.id.ok_button) as Button

                        val mAlertDialog: AlertDialog = dialogBuilder.create()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                profileItems?.get(adapterPosition)?.relation ?: "",
                                profileItems?.get(adapterPosition)?._id ?: ""
                            )
                            // Update locally
                            Handler().postDelayed({
                                notify(
                                    adapterPosition,
                                    profileItems?.get(adapterPosition)?.privateProfile
                                )
                            }, 100)
                            mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                        mAlertDialog.show()
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            profileItems?.get(adapterPosition)?.relation ?: "",
                            profileItems?.get(adapterPosition)?._id ?: ""
                        )
                        Handler().postDelayed({
                            if (profileItems != null && profileItems?.size ?: 0 > 0 &&
                                profileItems?.get(adapterPosition)?.privateProfile != null
                            ) {
                                notify(
                                    adapterPosition,
                                    profileItems?.get(adapterPosition)?.privateProfile
                                )
                            }
                        }, 100)
                    }
                }
            }
            // If navigated from chat search or new chat
            if (mIsFrom == "ChatSearch" || mIsFrom == "CreateNewChat") {
                binding.followingTextView.visibility = View.GONE
                binding.topLinear.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (profileItems?.get(adapterPosition)?._id != null) {

                            val convId: String =
                                if (profileItems?.get(adapterPosition)?.conv_id != null) {
                                    profileItems?.get(adapterPosition)?.conv_id ?: ""
                                } else {
                                    ""
                                }
                            mSendMessage.onSendMessage(
                                profileItems?.get(adapterPosition)?._id ?: "",
                                profileItems?.get(adapterPosition)?.username ?: "",
                                profileItems?.get(adapterPosition)?.pimage ?: "",
                                convId
                            )
                        }
                    }
                }
            } else {
                binding.followingTextView.visibility = View.VISIBLE
                binding.topLinear.setOnClickListener {
                    if (profileItems?.get(adapterPosition)?._id != null &&
                        profileItems?.get(adapterPosition)?.own != null
                    ) {
                        searchPeopleAdapterItemClick.onSearchPeopleAdapterItemClick(
                            profileItems?.get(adapterPosition)?._id ?: "",
                            profileItems?.get(adapterPosition)?.own ?: ""
                        )
                    }
                }
            }
            // Social-mob official id's user can't un-follow
            when (profileItems?.get(adapterPosition)?._id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding.followingTextView.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding.followingTextView.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding.followingTextView.visibility = View.GONE
                }
            }
        }
    }

    fun swap(mutableList: MutableList<SearchProfileResponseModelPayloadProfile>) {
        val diffCallback =
            SingleConversationListDiffUtils(
                this.profileItems,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.profileItems?.clear()
        this.profileItems?.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    private class SingleConversationListDiffUtils(
        private val oldList: MutableList<SearchProfileResponseModelPayloadProfile>?,
        private val newList: MutableList<SearchProfileResponseModelPayloadProfile>?
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList?.size ?: 0

        override fun getNewListSize() = newList?.size ?: 0

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList?.get(oldItemPosition)?._id == newList?.get(newItemPosition)?._id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList?.get(oldItemPosition)?.relation == newList?.get(newItemPosition)?.relation
        }

    }

    private fun notify(adapterPosition: Int, privateProfile: String?) {
        if (privateProfile == "false") {
            if (profileItems?.get(adapterPosition)?.relation == "following") {
                profileItems?.get(adapterPosition)?.relation = "none"
            } else {
                profileItems?.get(adapterPosition)?.relation = "following"
            }
        } else {
            when (profileItems?.get(adapterPosition)?.relation) {
                "none" -> profileItems?.get(adapterPosition)?.relation =
                    "request pending"
                "following" -> profileItems?.get(adapterPosition)?.relation =
                    "none"
                "request pending" -> profileItems?.get(adapterPosition)?.relation =
                    "none"
            }
        }
        notifyItemChanged(adapterPosition, profileItems?.size)
    }

    override fun getItemViewType(position: Int): Int {
        return if (profileItems?.get(position)?._id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    interface SearchPeopleAdapterFollowItemClick {
        fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String)
    }

    interface SearchPeopleAdapterItemClick {
        fun onSearchPeopleAdapterItemClick(mProfileId: String, mIsOwn: String)
    }

    interface SendMessage {
        fun onSendMessage(mProfileId: String, mName: String, mProfileImage: String, convId: String)
    }

}
