package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.model.chat.MsgPayload
import com.example.sample.socialmob.model.chat.SendMessage
import com.example.sample.socialmob.model.chat.WebSocketResponse
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.chat.WebSocketChatActivity
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.view.utils.events.SearchEventPeople
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchPeopleFragment : Fragment(), SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick,
    SearchPeopleAdapter.SearchPeopleAdapterItemClick, SearchPeopleAdapter.SendMessage {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private val searchViewModel: SearchViewModel by viewModels()
    private var binding: SearchInnerFragmentBinding? = null
    private var profilesList: MutableList<SearchProfileResponseModelPayloadProfile>? = ArrayList()
    private var mProfileAdapter: SearchPeopleAdapter? = null
    private var mSearchWord = ""
    private var mPos: Int = -1
    private var mFollowingCount: String = ""
    private var mName: String = ""

    private var mLastClickTime: Long = 0
    private var isAddedLoader: Boolean = false
    private var mStatus: ResultResponse.Status? = null
    private val viewModelChat: WebSocketChatViewModel? by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }
    private var isNewChat: Boolean = false
    private var mProfileIdNewChat = ""
    private var mNameNewChat = ""
    private var mImageNewChat = ""

    companion object {
        private const val ARG_STRING = "mString"
        var isFollow = ""
        var mProfileId = ""
        var isBlocked = ""
        fun newInstance(mName: String): SearchPeopleFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = SearchPeopleFragment()
            fragment.arguments = args
            return fragment
        }
    }
    /**
     * Check user clicked un-follow or blocked also update list
     */
    override fun onResume() {
        super.onResume()
        if (mProfileId != "" && isFollow != "" && isBlocked != "") {
            if (profilesList!!.size > 0) {
                for (i in 0 until profilesList!!.size) {
                    if (profilesList!![i]._id == mProfileId) {
                        profilesList!![i].relation = isFollow
                        if (isBlocked == "true") {
                            mPos = i
                        }
                    }
                }
                if (mPos != -1) {
                    // Need to remove when user un-follow an person
                    profilesList!!.removeAt(mPos)
                    mProfileAdapter?.swap(profilesList ?: ArrayList())
                    mPos = -1
                }
            }
            mProfileAdapter?.swap(profilesList ?: ArrayList())
            isFollow = ""
            mProfileId = ""
            isBlocked = ""
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments
        mName = args?.getString(ARG_STRING, "") ?: ""
        binding?.searchTitleTextView?.text = mName

        mProfileAdapter = SearchPeopleAdapter(
            profilesList!!,
            searchPeopleAdapterFollowItemClick = this,
            searchPeopleAdapterItemClick = this,
            mContext = requireActivity(),
            mIsFrom = mName,
            mSendMessage = this,glideRequestManager = glideRequestManager
        )
        binding?.searchInnerRecyclerView?.adapter = mProfileAdapter

        updateProfileData()



        binding?.refreshTextView?.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(mSearchWord, "0", RemoteConstant.mCount)
            }
        }

        binding?.searchInnerRecyclerView?.addOnScrollListener(CustomScrollListener())
        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = false

        observeUserList()
    }
    /**
     * Getting data from top list and populate
     */
    fun updateProfileData() {
        binding?.searchInnerRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                requireActivity(), LinearLayoutManager.VERTICAL, false
            )
        if (SearchFragment.mTopNetworkData != null &&
            SearchFragment.mTopNetworkData?.profiles != null &&
            SearchFragment.mTopNetworkData?.profiles?.size!! > 0
        ) {
            profilesList = SearchFragment.mTopNetworkData?.profiles?.toMutableList()
        }
        mProfileAdapter?.swap(profilesList ?: ArrayList())

    }
    /**
     * Pagination search people API
     */
    inner class CustomScrollListener :
        RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()
                if (profilesList?.size!! >= 15) {

                    if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                        mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                        mStatus != ResultResponse.Status.LOADING &&
                        mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                        visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                    ) {

                        if (mName != "CreateNewChat") {
                            if (mSearchWord != "" && !isAddedLoader) {

                                val user: MutableList<SearchProfileResponseModelPayloadProfile> =
                                    ArrayList()
                                val mList = SearchProfileResponseModelPayloadProfile(
                                    0, "", "", "", "", "",
                                    "", "", "", "", "", false, ""
                                )

                                user.add(mList)
                                profilesList?.addAll(user)
                                mProfileAdapter?.swap(profilesList ?: ArrayList())

                                isAddedLoader = true

                                callApiCommon(
                                    mSearchWord,
                                    profilesList?.size.toString(),
                                    RemoteConstant.mCount
                                )
                            }
                        }

                    }
                }
            }
        }
    }
    /**
     * Observe data and populate adapter
     */
    private fun observeUserList() {
        searchViewModel.searchProfileLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        profilesList = resultResponse?.data!!

                        mProfileAdapter?.swap(profilesList?.toMutableList()!!)
                        mProfileAdapter?.notifyDataSetChanged()

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            profilesList?.removeAt(profilesList?.size?.minus(1) ?: 0)
                            mProfileAdapter?.swap(profilesList ?: ArrayList())
                            isAddedLoader = false
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            profilesList?.removeAt(profilesList?.size?.minus(1) ?: 0)
                            mProfileAdapter?.swap(profilesList ?: ArrayList())
                            isAddedLoader = false
                        }
                        profilesList?.addAll(resultResponse?.data!!)
                        //                        mProfileAdapter?.addAll(resultResponse?.data!!)
                        mProfileAdapter?.swap(profilesList ?: ArrayList())
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {

                        if (profilesList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                profilesList?.removeAt(profilesList?.size?.minus(1) ?: 0)
                                mProfileAdapter?.swap(profilesList ?: ArrayList())
                                isAddedLoader = false
                            }
                        }

                    }
                    ResultResponse.Status.NO_DATA -> {
                        try {
                            glideRequestManager.load(R.drawable.ic_no_search_data).apply(RequestOptions().fitCenter())
                                .into(binding?.noDataImageView!!)
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.GONE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.VISIBLE

                            binding?.noInternetSecTextView?.text =
                                getString(R.string.no_data_playlist)
                            // binding?.noDataTextView?.text = getString(R.string.no_data_playlist)
                            binding?.noDataTextView?.visibility = View.GONE
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })

        searchViewModel.profile.nonNull().observe(viewLifecycleOwner, {profile->
            //TODO : To update following count
            mFollowingCount = profile!!.followingCount!!
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)
        return binding?.root
    }
    /**
     * update follow status
     */
    override fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }

    }
    /**
     * Cancel follow request API
     */
    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        searchViewModel.cancelRequest(mAuth, mId)
    }

    /**
     * Navigate to user profile
     * Check user clicked my profile
     */
    override fun onSearchPeopleAdapterItemClick(mProfileId: String, mIsOwn: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (mIsOwn == "false") {
                val intent = Intent(activity, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mProfileId)
                intent.putExtra("mIsOwn", mIsOwn)
                startActivityForResult(intent, 1501)
            } else {
                val intent = Intent(activity, MyProfileActivity::class.java)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
    }
    /**
     * Un-follow API
     */
    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.unFollowProfile(mAuth, mId, mFollowingCount)

    }
    /**
     * Follow user API
     */
    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.followProfile(mAuth, mId, mFollowingCount)

    }
    /**
     * Receiving search text from Search fragment - API
     */
    fun onMessageEvent(event: SearchEventPeople) {
        mSearchWord = event.mWord
        if (mSearchWord != "") {
            profilesList?.clear()
            mProfileAdapter?.swap(profilesList ?: ArrayList())
            callApiCommon(mSearchWord, "0", RemoteConstant.mCount)

        } else {
            if (mProfileAdapter != null) {
                mProfileAdapter?.swap(profilesList ?: ArrayList())
            }
        }
    }

    fun callApiCommon(mSearchWord: String, mOffset: String, mCount: String) {
        if (InternetUtil.isInternetOn()) {
            if (activity != null && binding != null) {
                if (mName == "CreateNewChat") {
                    if (mSearchWord == "") {
                        val nJwt =
                            "Bearer " + SharedPrefsUtils.getStringPreference(
                                activity, RemoteConstant.mJwt, ""
                            )!!
                        searchViewModel.getNewChatUsers(
                            nJwt
                        )
                    } else {
                        val mAuth: String = RemoteConstant.getAuthSearchProfile(
                            requireActivity(), RemoteConstant.pathProfileSearch,
                            mSearchWord, mOffset, mCount
                        )
                        searchViewModel.searchProfile(mAuth, mOffset, mCount, mSearchWord)
                    }

                } else {
                    val mAuth: String = RemoteConstant.getAuthSearchProfile(
                        requireActivity(), RemoteConstant.pathProfileSearch,
                        mSearchWord, mOffset, mCount
                    )
                    searchViewModel.searchProfile(mAuth, mOffset, mCount, mSearchWord)
                }

            } else {
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            }
        }
    }
    /**
     * If user have conversation id naigate to chat page or send an new penny
     */
    override fun onSendMessage(
        mProfileId: String, mName: String, mProfileImage: String, convId: String
    ) {
        // TODO : Send message
        if (convId != "") {
            val intent = Intent(activity, WebSocketChatActivity::class.java)
            intent.putExtra("convId", convId)
            intent.putExtra("mProfileId", mProfileId)
            intent.putExtra("mUserName", mName)
            intent.putExtra("mUserImage", mProfileImage)
            intent.putExtra("mStatus", "3")
            intent.putExtra("mLastMessageUser", "")
            startActivity(intent)
        } else {
            isNewChat = true
            // TODO : Send message
            mProfileIdNewChat = mProfileId
            mNameNewChat = mName
            mImageNewChat = mProfileImage
            pennySend(mProfileId, mName, mProfileImage)
        }
    }
    /**
     * Send penny custom UI and functionality
     */
    private fun pennySend(mProfileId: String, mName: String, mImage: String) {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_penny_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val messageTextView: TextView = dialogView.findViewById(R.id.message_text_view)
        val descriptionTextView: TextView = dialogView.findViewById(R.id.description_textView)
        val timeLinear: LinearLayout = dialogView.findViewById(R.id.time_linear)
        val pennyEditText: EditText = dialogView.findViewById(R.id.penny_edit_text)
        val closeButton: Button = dialogView.findViewById(R.id.close_button)
        val replayButton: Button = dialogView.findViewById(R.id.replay_button)
        val pennyProfileImageView: ImageView =
            dialogView.findViewById(R.id.penny_profile_image_view)
        val pennyOptionsImageView: ImageView =
            dialogView.findViewById(R.id.penny_options_image_view)
        replayButton.text = getString(R.string.send)
        //TODO : Custom For Send Penny
        glideRequestManager.load(mImage).apply(
            RequestOptions.placeholderOf(R.drawable.emptypicture).error(R.drawable.emptypicture)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
        ).into(pennyProfileImageView)
        messageTextView.visibility = View.GONE
        timeLinear.visibility = View.GONE
        pennyOptionsImageView.visibility = View.GONE
        descriptionTextView.text = "Send Penny to $mName"

        val mAlertDialog: AlertDialog = dialogBuilder.create()
        mAlertDialog.show()
        replayButton.setOnClickListener {
            if (pennyEditText.text.isNotEmpty()) {
                if (viewModelChat?.isWebSocketOpen!!) {
                    replayButton.isClickable = false
                    replayButton.isEnabled = false

                    val mSendMessage = SendMessage()
                    val mMsgPayload = MsgPayload()

                    mSendMessage.user = mProfileId
                    mSendMessage.type = "message"

                    mMsgPayload.user = mProfileId
                    mMsgPayload.msgType = "text"
                    mMsgPayload.text = pennyEditText.text.toString()
                    mMsgPayload.timestamp = System.currentTimeMillis()
                    mSendMessage.msgPayload = mMsgPayload

                    val mGson = Gson()
                    val mMessage =
                        mGson.toJson(mSendMessage)


                    viewModelChat?.sendMessageSm(mMessage)

                    Handler().postDelayed({
                        mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        AppUtils.hideKeyboard(activity, binding?.noDataImageView!!)
                    }, 500)
                } else {
                    if (!viewModelChat?.isWebSocketOpen!! && !viewModelChat?.isWebSocketOpenCalled!!) {
                        viewModelChat?.connectToSocketSm()
                    }
                    Toast.makeText(activity, "Waiting for connection", Toast.LENGTH_SHORT).show()
                }
            } else {
                AppUtils.showCommonToast(
                    requireActivity(),
                    "You cannot send Penny with an empty note."
                )
            }
        }
        closeButton.setOnClickListener {
            isNewChat = false
            mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        viewModelChat?.newMessage?.observe(viewLifecycleOwner, { newMessage ->
            if (isNewChat) {
                if (newMessage != null && newMessage != "") {
                    handleMessageNewUser(
                        newMessage
                    )
                }
            }
        })

    }
    /**
     * if penny send to new user receive conversation id from socket connection and navigate to chat page
     * Note : To launch chat page we need conversation id
     */
    private fun handleMessageNewUser(text: String?) {
        val sourceType: String
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)

            sourceType = webSocketResponse?.type!!
            when (sourceType) {
                "message" -> {

                    val convId1 = webSocketResponse.msgPayload?.convId
                    val intent = Intent(activity, WebSocketChatActivity::class.java)
                    intent.putExtra("convId", convId1)
                    intent.putExtra("mProfileId", mProfileIdNewChat)
                    intent.putExtra("mUserName", mNameNewChat)
                    intent.putExtra("mUserImage", mImageNewChat)
                    intent.putExtra("mStatus", "")
                    intent.putExtra("mLastMessageUser", "")
                    startActivityForResult(intent, 112)

                    isNewChat = false
                    mProfileIdNewChat = ""
                    mNameNewChat = ""
                    mImageNewChat = ""

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isNewChat = false
            mProfileIdNewChat = ""
            mNameNewChat = ""
            mImageNewChat = ""
        }
    }
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (mProfileAdapter != null) {
                mProfileAdapter?.swap(profilesList ?: ArrayList())
            }
        }
    }

}

