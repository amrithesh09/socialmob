package com.example.sample.socialmob.view.ui.home_module.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.UserPlayListItemBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.PlaylistDataIcon
import com.example.sample.socialmob.view.utils.BaseViewHolder

class SearchPlayListAdapter internal constructor(
    private val mCallBack: PlayListAdapterItemClickListener?,
    private val mOptionClick: PlayListAdapterOptionClickListener,
    private var glideRequestManager: RequestManager

) : ListAdapter<PlaylistCommon, BaseViewHolder<Any>>(DIFF_CALLBACK) {

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<PlaylistCommon>() {
                override fun areItemsTheSame(
                    oldItem: PlaylistCommon,
                    newItem: PlaylistCommon
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: PlaylistCommon,
                    newItem: PlaylistCommon
                ): Boolean {
                    return oldItem.playlistName?.equals(newItem.playlistName)!!
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: UserPlayListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.user_play_list_item, parent, false
        )
        return PlayListHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }


    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.id == "0") {
            ITEM_LOADING
        } else {
            ITEM_DATA
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    inner class PlayListHolder(val binding: UserPlayListItemBinding?) :
        BaseViewHolder<Any>(binding?.root) {
        override fun bind(mPlaytListData: Any) {
            val mPlayList: PlaylistCommon = mPlaytListData as PlaylistCommon
            binding?.quickPlayListTitleTextView?.text = mPlayList.playlistName

            if (mPlayList.playlistType == "system-generated" || mPlayList.playlistType == "admin-generated") {
                glideRequestManager
                    .load(mPlayList.media?.coverImage)
                    .apply(RequestOptions.placeholderOf(0).error(R.drawable.default_list_icon))
                    .into(binding?.playListImageView!!)

            } else {
                glideRequestManager
                    .load(mPlayList.icon?.fileUrl)
                    .apply(RequestOptions.placeholderOf(0).error(R.drawable.default_list_icon))
                    .into(binding?.playListImageView!!)
                binding.playListOptionImageView.visibility = View.VISIBLE
            }
            if (mPlayList.playlistType == "admin-generated") {
                binding.playListOptionImageView.visibility = View.VISIBLE
                binding.quickPlayListTitleTextView.visibility = View.GONE
                binding.gradientImageView.setImageResource(R.drawable.default_play_list_gradient_new)
            } else {
                binding.quickPlayListTitleTextView.visibility = View.VISIBLE
                binding.gradientImageView.setImageResource(R.drawable.default_play_list_gradient)
            }

            if (mPlayList.profile?.username != null) {
                binding.playListBy.text = mPlayList.profile.username
                binding.playListByLinear.visibility = View.VISIBLE
            } else {
                binding.playListByLinear.visibility = View.GONE
            }

            if (mPlayList.isPrivate.equals("1")) {
                binding.privateImageView.visibility = View.VISIBLE
            } else {
                binding.privateImageView.visibility = View.GONE
            }

            binding.playListImageView.setOnClickListener {
                var mUrl = ""
                var mUserName = ""
                var mUserId = ""
                if (mPlayList.playlistType == "system-generated" || mPlayList.playlistType == "admin-generated") {
                    if (mPlayList.media?.coverImage != null &&
                        mPlayList.media.coverImage != ""
                    ) {
                        mUrl = mPlayList.media.coverImage!!
                    }
                } else {
                    if (mPlayList.icon?.fileUrl != null &&
                        mPlayList.icon.fileUrl != ""
                    ) {
                        mUrl = mPlayList.icon.fileUrl
                    }
                }
                if (mPlayList.profile?.username != null) {
                    mUserName = mPlayList.profile.username!!
                }
                if (mPlayList.profile?._id != null) {
                    mUserId = mPlayList.profile._id
                }

                mCallBack?.onItemClick(
                    mPlayList.id!!,
                    mPlayList.playlistName!!,
                    mUrl,
                    mUserName, mUserId
                )
            }
            binding.playListOptionImageView.setOnClickListener {
                if (mPlayList.id != null &&
                    mPlayList.playlistName != null &&
                    mPlayList.isPrivate != null
                ) {
                    mOptionClick.optionDialog(
                        mPlayList.id!!,
                        mPlayList.playlistName!!,
                        mPlayList.isPrivate,
                        mPlayList.icon!!,
                        adapterPosition
                    )
                }
            }
            if (mPlayList.playlistName == "Listen Later") {
                glideRequestManager
                    .load(R.drawable.listen_later_new_one)
                    .into(binding.playListImageView)
                binding.playListOptionImageView.visibility = View.GONE
                binding.privateImageView.visibility = View.GONE
            }
        }
    }

    interface PlayListAdapterItemClickListener {
        fun onItemClick(
            item: String,
            mTitle: String,
            mImageUrl: String,
            mUserName: String,
            mUserId: String
        )
    }

    interface PlayListAdapterOptionClickListener {
        fun optionDialog(
            playlistId: String?,
            playlistName: String?,
            private: String,
            icon: PlaylistDataIcon,
            adapterPosition: Int
        )
    }
}