package com.example.sample.socialmob.view.ui.home_module.search

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.SearchTagItemBinding
import com.example.sample.socialmob.view.ui.backlog.articles.TagDetails
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.search.SearchTagResponseModelPayloadTag

class SearchTagsAdapter(
    private val tagItems: MutableList<SearchTagResponseModelPayloadTag>?,
    private val searchTagsAdapterItemClick: SearchTagsAdapterItemClick,
    private val mContext: Activity
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<ProgressItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgerssViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<SearchTagItemBinding>(
            LayoutInflater.from(parent.context), R.layout.search_tag_item, parent, false
        )
        return TagViewHolder(binding)
    }

    fun addAll(it: MutableList<SearchTagResponseModelPayloadTag>?) {
        if (it?.size!! > 0) {
            val size = tagItems!!.size
            tagItems.clear() //here podcastList is an ArrayList populating the RecyclerView
            tagItems.addAll(it)// add new data
            notifyItemChanged(size, tagItems.size)// notify adapter of new data
        }
    }

    fun addLoader(it: MutableList<SearchTagResponseModelPayloadTag>) {
        tagItems!!.addAll(it)
    }

    fun removeLoader() {
        if (tagItems!!.isNotEmpty()) {
            tagItems.removeAt(tagItems.size.minus(1))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(tagItems!![position])
    }

    override fun getItemCount(): Int {
        return tagItems!!.size
    }

    inner class ProgerssViewHolder(val binding: ProgressItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class TagViewHolder(val binding: SearchTagItemBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.tagViewModel = tagItems!![adapterPosition]
            binding.executePendingBindings()
            binding.followTagTextView.setOnClickListener {
                searchTagsAdapterItemClick.onSearchTagsAdapterItemClick(
                    tagItems[adapterPosition].Following!!,
                    tagItems[adapterPosition].TagId!!
                )
                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }
            itemView.setOnClickListener {
                val intent = Intent(mContext, TagDetails::class.java)
                intent.putExtra("tagName", tagItems[adapterPosition].TagName)
                intent.putExtra("tagId", tagItems[adapterPosition].TagId)
                intent.putExtra("tagImage", tagItems[adapterPosition].SquareImage)
                mContext.startActivityForResult(intent, 1901)
                mContext.overridePendingTransition(0, 0)
            }
        }
    }

    private fun notify(adapterPosition: Int) {
        if (tagItems!![adapterPosition].Following == "true") {
            tagItems[adapterPosition].Following = "false"
        } else {
            tagItems[adapterPosition].Following = "true"
        }
        notifyItemChanged(adapterPosition, tagItems.size)
    }

    override fun getItemViewType(position: Int): Int {

        return if (tagItems!![position].TagId != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    interface SearchTagsAdapterItemClick {
        fun onSearchTagsAdapterItemClick(isFollowing: String, mId: String)
    }
}

