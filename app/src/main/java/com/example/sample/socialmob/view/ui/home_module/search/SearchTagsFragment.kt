package com.example.sample.socialmob.view.ui.home_module.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchTagFragmentBinding
import com.example.sample.socialmob.model.search.SearchTagResponseModelPayloadTag
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.GridSpacingItemDecoration
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.events.SearchEvent
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@AndroidEntryPoint
class SearchTagsFragment : Fragment(), SearchTagsAdapter.SearchTagsAdapterItemClick {
    companion object {
        private const val ARG_STRING = "mString"
        var mTagId = ""
        var isFollow = ""
        fun newInstance(mName: String): SearchTagsFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = SearchTagsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val searchViewModel: SearchViewModel by viewModels()
    private var isLoaded: Boolean = false
    private var binding: SearchTagFragmentBinding? = null
    private var mGlobalTagList: MutableList<SearchTagResponseModelPayloadTag>? = ArrayList()
    private var mTagsAdapter: SearchTagsAdapter? = null
    private var mSearchWord = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments
        val mName = args?.getString(ARG_STRING, "")
        binding?.searchTitleTextView?.text = mName.toString()


        // TODO : Pagination
        binding?.searchTagRecyclerView?.layoutManager =
            (androidx.recyclerview.widget.GridLayoutManager(activity, 3))
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.spacing)
        binding?.searchTagRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(
                3,
                spacingInPixels,
                false
            )
        )

        mTagsAdapter = SearchTagsAdapter(mGlobalTagList, this, requireActivity())
        binding?.searchTagRecyclerView?.adapter = mTagsAdapter

        // TODO : Swipe To Refresh
        binding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
            val view =
                binding?.nestedScrollView?.getChildAt(binding?.nestedScrollView?.childCount!! - 1)

            val diff =
                view?.bottom?:0  - (binding?.nestedScrollView?.height!! + binding?.nestedScrollView?.scrollY!!)

            if (diff == 0) {
                //your api call to fetch data
                if (!searchViewModel.isLoadingTag) {

                    if (mSearchWord != "") {
                        loadMoreItems(mGlobalTagList!!.size, mSearchWord)
                    }

                }
            }
        }
        binding?.refreshTextView?.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(mSearchWord)
            }
        }
        //TODO : Observe List
        observeList()
    }

    private fun loadMoreItems(size: Int, mString: String?) {
        if (!searchViewModel.isLoadCompletedTag) {
            if (activity != null) {
                val user: MutableList<SearchTagResponseModelPayloadTag> = ArrayList()
                val mList = SearchTagResponseModelPayloadTag(
                    0, "", "", "", "", ""
                )

                user.add(mList)
                mTagsAdapter?.addLoader(user)
                mTagsAdapter?.notifyDataSetChanged()
                val mAuth = RemoteConstant.getAuthSearchProfile(
                    requireActivity(),
                    RemoteConstant.pathTagSearch,
                    mString!!,
                    size.toString(),
                    RemoteConstant.mCount
                )
                searchViewModel.searchTags(mAuth, size.toString(), RemoteConstant.mCount, mString)
            }

        }
    }

    private fun observeList() {

        searchViewModel.searchTagLiveData.nonNull().observe(viewLifecycleOwner, {
            if (!isLoaded) {
                isLoaded = true
                mTagsAdapter?.addAll(it as MutableList)
                binding?.isVisibleList = true
                binding?.isVisibleLoading = false
                binding?.isVisibleNoData = false
                binding?.isVisibleNoInternet = false
            }

        })

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.search_tag_fragment, container, false)
        return binding?.root!!
    }

    override fun onSearchTagsAdapterItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "true" -> unFollowTagApi(mId)
                "false" -> followTagApi(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }

    }

    private fun unFollowTagApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathTag + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.unFollowTag(mAuth, mId)
    }

    private fun followTagApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathTag + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        searchViewModel.followTag(mAuth, mId)
    }


    private fun observePaginatedList() {

        searchViewModel.paginatedTagList.nonNull().observe(viewLifecycleOwner, {
            mGlobalTagList = it!!
            if (!searchViewModel.isLoadingTag) {
                if (mGlobalTagList?.isNotEmpty()!!) {
                    binding?.isVisibleList = true
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = false
                    binding?.isVisibleNoInternet = false
                    mTagsAdapter?.removeLoader()
                    mTagsAdapter?.addAll(it)
                    mTagsAdapter?.notifyDataSetChanged()
                } else {
                    binding?.isVisibleList = false
                    binding?.isVisibleLoading = false
                    binding?.isVisibleNoData = true
                    binding?.isVisibleNoInternet = false
                }
                searchViewModel.isLoadingTag = false
            }

        })

    }
    fun callApiCommon(mSearchWordNew: String) {
        mSearchWord = mSearchWordNew
        if (InternetUtil.isInternetOn()) {
            if (activity != null && binding != null) {
                val mAuth = RemoteConstant.getAuthSearchProfile(
                    requireActivity(),
                    RemoteConstant.pathTagSearch,
                    mSearchWord,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCount
                )
                searchViewModel.searchTags(
                    mAuth,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCount,
                    mSearchWord
                )

                binding?.isVisibleList = false
                binding?.isVisibleLoading = true
                binding?.isVisibleNoData = false
                binding?.isVisibleNoInternet = false
                mGlobalTagList!!.clear()
                mTagsAdapter?.addAll(mGlobalTagList!!)
                observePaginatedList()
            } else {
                binding?.isVisibleList = false
                binding?.isVisibleLoading = false
                binding?.isVisibleNoData = false
                binding?.isVisibleNoInternet = true
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            }
        }

    }
    override fun onResume() {
        super.onResume()
        if (mTagId != "" && isFollow != "") {
            if (mGlobalTagList!!.size > 0) {
                for (i in 0 until mGlobalTagList!!.size) {
                    if (mGlobalTagList!![i].TagId == mTagId) {
                        mGlobalTagList!![i].Following = isFollow
                    }
                }
            }
            mTagsAdapter?.notifyDataSetChanged()
            mTagId = ""
            isFollow = ""
        }
    }
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SearchEvent) {
        mSearchWord = event.mWord
        callApiCommon(mSearchWord)
    }

}
