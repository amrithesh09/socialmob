package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AdmobItemNativeAdBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.databinding.RecommendedTrackItemBinding
import com.example.sample.socialmob.repository.utils.DbPlayClick
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.google.android.ads.nativetemplates.NativeTemplateStyle
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.admob_item_native_ad.view.*
import java.util.*

class SearchTrackAdapter(
    private var clickListener: PlayListOptionsClickListener,
    private var playPlayClick: DbPlayClick,
    private val mContext: Context?,
    private var mListAdUitId: MutableList<String>?
) : RecyclerView.Adapter<BaseViewHolder<Any>>() {

    private val mutableList = mutableListOf<TrackListCommon>()

    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        const val ITEM_AD = 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_AD -> bindAd(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: RecommendedTrackItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recommended_track_item, parent, false
        )
        return SearchTrackViewHolder(binding)
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {

//        val binding: AdmobItemBannerBinding = DataBindingUtil.inflate(
//            LayoutInflater.from(parent.context),
//            R.layout.admob_item_banner, parent, false
//        )
        val binding: AdmobItemNativeAdBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.admob_item_native_ad, parent, false
        )
        return AdViewHolder(binding)
    }

    override fun getItemCount() = mutableList.size
    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mutableList[holder.adapterPosition])

    }

    override fun getItemViewType(position: Int): Int {

        return when {
            mutableList[position].id == "ad" -> {
                ITEM_AD
            }
            mutableList[position].id != "" -> {
                ITEM_DATA
            }
            else -> {
                ITEM_LOADING
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class AdViewHolder(val binding: AdmobItemNativeAdBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
//            val adRequest = AdRequest.Builder().addTestDevice("74CB4C4575947CE06704C6972734F731")
//                .build()
//            binding.root.adViewTrack.loadAd(adRequest)

            try {
                if (mContext != null && binding.root.my_template != null) {
                    val min = 0
                    val max = mListAdUitId?.size

                    val r = Random();
                    val i1 = r.nextInt(max!! - min + 1) + min

                    val adLoader = AdLoader.Builder(mContext, mListAdUitId?.get(i1))
                        .forUnifiedNativeAd { unifiedNativeAd ->
                            val styles =
                                NativeTemplateStyle.Builder().build()
                            val template = binding.root.my_template
                            template.setStyles(styles)
                            template.setNativeAd(unifiedNativeAd)
                        }
                        .build()
                    adLoader.loadAd(AdRequest.Builder().addTestDevice("64A0685293CC7F3B6BC3ECA446376DA0").build())
                }
            } catch (e: Exception) {

            }

        }

    }

    inner class SearchTrackViewHolder(val binding: RecommendedTrackItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.trackViewModel = mutableList.get(adapterPosition)
            binding.executePendingBindings()


            if (mutableList[adapterPosition].isPlaying!!) {
                binding.queueListItemPlayImageView.visibility = View.VISIBLE
                binding.topTrackGifProgress.visibility = View.VISIBLE
                binding.topTrackGifProgress.playAnimation()
            } else {
                binding.queueListItemPlayImageView.visibility = View.INVISIBLE
                binding.topTrackGifProgress.visibility = View.INVISIBLE
                if (binding.topTrackGifProgress.isAnimating) {
                    binding.topTrackGifProgress.pauseAnimation()
                }
            }

            binding.topLinear.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    playPlayClick.onPlayClick(adapterPosition.toString())
                }
            }
            binding.trackOptions.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    clickListener.musicOptionsDialog(mutableList[adapterPosition])
                }
            }

        }
    }

    fun swap(list: List<TrackListCommon>) {
        val diffCallback = TrackDiffCallback(this.mutableList, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.mutableList.clear()
        this.mutableList.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    interface PlayListOptionsClickListener {
        fun musicOptionsDialog(mTrackDetails: TrackListCommon?)
    }
}

class TrackDiffCallback(
    private val oldList: List<TrackListCommon>,
    private val newList: List<TrackListCommon>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].numberId == newList[newItemPosition].numberId
    }


}