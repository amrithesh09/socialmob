package com.example.sample.socialmob.view.ui.home_module.search

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.DbPlayClick
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.CommonPlayListAdapter
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.PlayListImageActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.SearchEventTrack
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.events.UpdateTrackEvent
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.offline.Data
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState.*
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.search.SearchViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.firebase.analytics.FirebaseAnalytics
import com.suke.widget.SwitchButton
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class SearchTrackFragment : Fragment(),
    SearchTrackAdapter.PlayListOptionsClickListener, DbPlayClick, PlaylistListener<MediaItem> {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mSearchWord = ""
    private var binding: SearchInnerFragmentBinding? = null
    private var mLastId: String = ""
    private var mTrackAdapter: SearchTrackAdapter? = null
    private var isAddedLoader: Boolean = false
    private var mStatus: ResultResponse.Status? = null
    private var mLastClickTime: Long = 0
    private var playlistManager: PlaylistManager? = null
    private val mediaItems = LinkedList<MediaItem>()
    private var mPlayPosition = -1
    private var playListImageView: ImageView? = null
    private var mAdCount: Int = 0

    // Track Download
    private var trackListCommonDbs: List<TrackListCommonDb>? = ArrayList()
    private var fetch: Fetch? = null
    private val FETCH_NAMESPACE = "DownloadListActivity"


    private var isRewardEarned = false
    private lateinit var mRewardedAd: RewardedAd
    private var mIsLoading = false
    private val searchViewModel: SearchViewModel by viewModels()

    companion object {

        var dbPlayList: MutableList<TrackListCommon>? = ArrayList()

        private const val ARG_STRING = "mString"

        fun newInstance(mName: String): SearchTrackFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = SearchTrackFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: Bundle? = arguments
        val mName: String = args?.getString(ARG_STRING, "")!!
        binding?.searchTitleTextView?.text = mName


        MobileAds.initialize(activity, getString(R.string.admob_app_id))
        loadRewardedAd()

        binding?.refreshTextView?.setOnClickListener {
            if (mSearchWord != "") {
                callApiCommon(mSearchWord, "0", RemoteConstant.mCount)
            }
        }



        if (SearchFragment.mTopMusicData?.trendingTracks != null &&
            SearchFragment.mTopMusicData?.trendingTracks?.size!! > 0
        ) {
            dbPlayList = SearchFragment.mTopMusicData?.trendingTracks!!.toMutableList()
        }

        binding?.searchInnerRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding?.searchInnerRecyclerView?.addOnScrollListener(CustomScrollListener())

        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = true

        val mListAdUitId: MutableList<String> = ArrayList()
        mListAdUitId.add(getString(R.string.ad_unit_id))
        mListAdUitId.add(getString(R.string.ad_unit_id_2))
        mTrackAdapter = SearchTrackAdapter(
            this,
            this,
            activity,
            mListAdUitId
        )
        binding?.searchInnerRecyclerView?.adapter = mTrackAdapter
        mTrackAdapter?.swap(dbPlayList?.toMutableList()!!)
        mTrackAdapter?.notifyDataSetChanged()

        //TODO : Observe List
        observeList()

        // TODO : Track download

        val fetchConfiguration = FetchConfiguration.Builder(requireContext())
            .setDownloadConcurrentLimit(4)
            .setHttpDownloader(OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
            .setNamespace(FETCH_NAMESPACE)
//            .setNotificationManager(object : DefaultFetchNotificationManager(requireContext()) {
//                override fun getFetchInstanceForNamespace(namespace: String): Fetch {
//                    return fetch!!
//                }
//            })
            .build()
        fetch = Fetch.getInstance(fetchConfiguration)


        // TODO : Download offlie track pending
        if (searchViewModel.downloadFalseTrackListModel != null) {
            searchViewModel.downloadFalseTrackListModel?.nonNull()
                ?.observe(viewLifecycleOwner, { trackListCommonDbs ->
                    if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                        this.trackListCommonDbs = trackListCommonDbs

                    }
                })
        }

        binding?.fab?.visibility = View.VISIBLE
        binding?.gifProgress?.setOnClickListener {
            binding?.fab?.performClick()
        }
        binding?.fab?.setOnClickListener {
                if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                ) {
                    var mImageUrlPass = ""
                    if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                        Intent(activity, NowPlayingActivity::class.java)
//                    detailIntent.putExtra(
//                        RemoteConstant.mExtraIndex,
//                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id?.toString()
//                    )
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                } else if (playlistManager != null &&
                    playlistManager?.playlistHandler != null &&
                    playlistManager?.playlistHandler?.currentMediaPlayer != null
                ) {
                    var mImageUrlPass = ""
                    if (
                        playlistManager?.playlistHandler?.currentItemChange != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem != null &&
                        playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl != null
                    ) {
                        mImageUrlPass =
                            playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl!!

                    }
                    val detailIntent =
                        Intent(activity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                }
        }
    }

    inner class CustomScrollListener :
        RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                val visibleItemCount: Int = recyclerView.layoutManager?.childCount!!
                val totalItemCount: Int = recyclerView.layoutManager?.itemCount!!
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()
                if (dbPlayList?.size!! >= 15) {

                    if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                        mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                        mStatus != ResultResponse.Status.LOADING &&
                        mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                        visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                    ) {


                        if (mSearchWord != "" && !isAddedLoader) {
                            callApiCommon(
                                mSearchWord,
                                dbPlayList?.size.toString(),
                                RemoteConstant.mCount
                            )
                            val mTrack: MutableList<TrackListCommon> = ArrayList()
                            val mList = TrackListCommon()
                            mList.id = ""

                            mTrack.add(mList)
                            dbPlayList?.addAll(mTrack)
                            mTrackAdapter?.notifyDataSetChanged()

                            isAddedLoader = true
                        }

                    }
                }
            }
        }
    }


    private fun observeList() {

        searchViewModel.tracksListLiveData?.nonNull()
            ?.observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mPlayPosition = -1
                        val singlePlayListTrackLocal = resultResponse?.data!!.toMutableList()
                        val mTrackNewList: MutableList<TrackListCommon> = ArrayList()
                        for (i in singlePlayListTrackLocal.indices) {
                            if (i % 20 == 0) {
                                if (i != 0) {
                                    val mList = TrackListCommon()
                                    mList.id = "ad"
                                    mTrackNewList.add(mList)
                                    // TODO: Google Ad count
                                    mAdCount = mAdCount.plus(1)

                                }
                                mTrackNewList.add(singlePlayListTrackLocal[i])
                            } else {
                                mTrackNewList.add(singlePlayListTrackLocal[i])
                            }
                        }

                        dbPlayList?.addAll(mTrackNewList)

                        mTrackAdapter?.swap(dbPlayList?.toMutableList()!!)
                        mTrackAdapter?.notifyDataSetChanged()

                        updatePlayPause()

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE


                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }

                        val mGlobalGenreListNew: MutableList<TrackListCommon> =
                            resultResponse.data as MutableList<TrackListCommon>
                        val mTrackNewList: MutableList<TrackListCommon> = ArrayList()

                        for (i in mGlobalGenreListNew.indices) {
                            if (i % 20 == 0) {
                                if (i != 0) {
                                    val mList = TrackListCommon()
                                    mList.id = "ad"
                                    mGlobalGenreListNew.add(mList)
                                    // TODO: Google Ad count
                                    mAdCount = mAdCount.plus(1)

                                }
                                mTrackNewList.add(mGlobalGenreListNew[i])
                            } else {
                                mTrackNewList.add(mGlobalGenreListNew[i])
                            }
                        }

                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            dbPlayList?.addAll(mTrackNewList)
                            mTrackAdapter?.swap(dbPlayList?.toMutableList()!!)
                            mTrackAdapter?.notifyDataSetChanged()
                            updatePlayPause()
                            mStatus = ResultResponse.Status.LOADING_COMPLETED
                        }, 1000)
                    }
                    ResultResponse.Status.ERROR -> {

                        if (dbPlayList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        if (binding?.noDataImageView != null) {
                            glideRequestManager.load(R.drawable.ic_no_search_data).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noDataImageView!!)
                        }
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.LOADING -> {

                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE


                    }
                    else -> {

                    }
                }

            })


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)

        return binding?.root
    }

    private fun addTrackToPlayList(mTrackId: String, mPlayListId: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathDeletePlayList + RemoteConstant.mSlash +
                    mPlayListId + RemoteConstant.mSlash + mTrackId, RemoteConstant.putMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            activity, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        searchViewModel.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
    }

    private fun createPlayListDialogFragment() {
        val viewGroup: ViewGroup? = null
        var isPrivate = "0"
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(requireContext()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText = dialogView.findViewById(R.id.play_list_name_edit_text)
        playListImageView = dialogView.findViewById(R.id.play_list_image_view)

        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)
        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }


        val b: AlertDialog = dialogBuilder.create()
        b.show()


        playListImageView?.setOnClickListener {
            val intent = Intent(requireContext(), PlayListImageActivity::class.java)
            startActivityForResult(intent, 1001)
        }

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Favourites", ignoreCase = true)
                        && !playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        createPlayListApi(
                            "0",
                            playListNameEditText.text.toString(),
                            isPrivate,
                            mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(
                            requireContext(),
                            getString(R.string.have_play_list)
                        )
                    }

                } else
                    AppUtils.showCommonToast(requireContext(), "Playlist name too short")
            } else
                AppUtils.showCommonToast(requireContext(), "Playlist name cannot be empty")
            //To close alert
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    private fun createPlayListApi(
        playListId: String, playListName: String, private: String, icon_id: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody = CreatePlayListBody(playListId, playListName, icon_id, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(requireContext())
            searchViewModel.createPlayList(mAuth, createPlayListBody)

        } else {
            AppUtils.showCommonToast(requireContext(), getString(R.string.no_internet))
        }
    }


    override fun onPlayClick(mPosition: String) {
        try {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                if (dbPlayList?.size!! > mPosition.toInt()) {
                    val itemId = dbPlayList?.get(mPosition.toInt())?.numberId
                    if (itemId == MyApplication.playlistManager?.currentItemChange?.currentItem?.id?.toString() ?: 0) {
                        if (MyApplication.playlistManager != null &&
                            MyApplication.playlistManager?.playlistHandler != null &&
                            MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer != null &&
                            MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!
                        ) {
                            binding?.fab?.performClick()
                        } else {
                            createLocalPlayList(itemId, mPosition)
                        }
                    } else {
                        createLocalPlayList(itemId, mPosition)
                    }
                }
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "onPlayClick_" +e.toString())
        }

    }

    private fun createLocalPlayList(itemId: String?, mPosition: String) {

        updatePause()
        val mShuffleStatus =
            SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mShuffleStatus, "false"
            )

        if (mShuffleStatus == "true") {
            var selectedPosition = 0
            dbPlayList?.map {
                if (it.numberId == itemId.toString()) {
                    selectedPosition = dbPlayList?.indexOf(it)!!
                }
            }
            val mSelectedSong: TrackListCommon = dbPlayList?.get(selectedPosition)!!
            val mGlobalFirstList: MutableList<TrackListCommon> = ArrayList()
            val mGlobalListNew: MutableList<TrackListCommon> = ArrayList()
            mGlobalListNew.addAll(dbPlayList!!)

            //TODO : if track is playing adding playing track as first track
            mGlobalListNew.removeAt(selectedPosition)
            mGlobalListNew.shuffle()

            mGlobalFirstList.clear()
            mGlobalFirstList.add(0, mSelectedSong)
            mGlobalFirstList.addAll(mGlobalListNew)
            if (mGlobalFirstList.isNotEmpty()) {
                mediaItems.clear()
                for (sample in mGlobalFirstList) {
                    val mediaItem = MediaItem(sample, true)
                    mediaItems.add(mediaItem)
                }
            }
            var mPLayPosition = 0
            mGlobalFirstList.map {
                if (it.numberId == itemId) {
                    updatePLay(it.numberId)
                    mPLayPosition = mGlobalFirstList.indexOf(it)
                }
            }

            playlistManager?.setParameters(mediaItems, mPLayPosition)
            playlistManager?.id = 4.toLong()
            playlistManager?.currentPosition = mPLayPosition
            playlistManager?.play(0, false)

        } else {
            mediaItems.clear()
            for (sample in dbPlayList!!) {
                val mediaItem = MediaItem(sample, true)
                mediaItems.add(mediaItem)
            }

            playlistManager?.setParameters(mediaItems, mPosition.toInt())
            playlistManager?.id = 4.toLong()
            playlistManager?.currentPosition = mPosition.toInt()
            playlistManager?.play(0, false)

            updatePLay(itemId)
        }


        searchViewModel.createDBPlayList(dbPlayList,"Suggested tracks")
    }


    private fun updatePlayPause() {
        if (playlistManager != null
            && playlistManager?.playlistHandler != null
            && playlistManager?.playlistHandler?.currentMediaPlayer != null
            && playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying != null
            && playlistManager?.currentItem != null
            && playlistManager?.currentItem?.id != null
        ) {
            if (playlistManager?.playlistHandler?.currentMediaPlayer?.isPlaying!!) {
                val mTrackId =
                    playlistManager?.playlistHandler?.currentItemChange?.currentItem?.id?.toString()

                updatePLay(mTrackId)
            } else {
                updatePause()
            }
        }

    }

    private fun updatePause() {

        dbPlayList?.map {
            if (it.isPlaying == true) {
                it.isPlaying = false
            }
        }
        mTrackAdapter?.notifyDataSetChanged()

        if (binding?.fab?.visibility == View.GONE) {
            binding?.fab?.visibility = View.VISIBLE
        }
        if (binding?.gifProgress?.visibility == View.VISIBLE) {
            binding?.gifProgress?.visibility = View.GONE
        }
        if (binding?.gifProgress?.isAnimating == true) {
            binding?.gifProgress?.pauseAnimation()
        }

    }


    private fun updatePLay(itemId: String?) {
        // TODO : Check if playing older position
        try {
            if (dbPlayList != null && dbPlayList?.size!! > 0 && mPlayPosition != -1 &&
                dbPlayList?.size!! >= mPlayPosition &&
                dbPlayList?.get(mPlayPosition) != null &&
                dbPlayList?.get(mPlayPosition)?.isPlaying != null
            ) {
                dbPlayList?.get(mPlayPosition)?.isPlaying = false
                mTrackAdapter?.notifyItemChanged(mPlayPosition)
            }

            dbPlayList?.map {
                if (it.numberId == itemId.toString()) {
                    it.isPlaying = true
                    mPlayPosition = dbPlayList?.indexOf(it)!!
                }
            }
            mTrackAdapter?.notifyItemChanged(mPlayPosition)

            if (binding?.fab?.visibility == View.VISIBLE) {
                binding?.fab?.visibility = View.GONE
            }
            if (binding?.gifProgress?.visibility == View.GONE) {
                binding?.gifProgress?.visibility = View.VISIBLE
            }
            if (binding?.gifProgress?.isAnimating == false) {
                binding?.gifProgress?.playAnimation()
                binding?.gifProgress?.bringToFront()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onPause() {
        super.onPause()
        playlistManager?.unRegisterPlaylistListener(this)
    }

    override fun onResume() {
        super.onResume()
        playlistManager = MyApplication.playlistManager
        playlistManager?.registerPlaylistListener(this)

        //TODO : For initial list
        updatePlayPause()

    }

    fun onMessageEvent(event: SearchEventTrack) {
        mSearchWord = event.mWord
        dbPlayList?.clear()
        mTrackAdapter?.notifyDataSetChanged()
        callApiCommon(mSearchWord, "0", RemoteConstant.mCount)

    }

    private fun callApiCommon(
        mSearchWordNew: String, mOffset: String, mCount: String
    ) {
        mSearchWord = mSearchWordNew

        if (activity != null && binding != null) {
            if (InternetUtil.isInternetOn()) {

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v2/search/track?track=" + mSearchWord + "&offset=" + mOffset + "&count=" + mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    activity,
                    RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                searchViewModel.searchTrack(mAuth, mOffset, mCount, mSearchWord)

            } else {
                binding?.progressLinear?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.VISIBLE
                binding?.listLinear?.visibility = View.GONE
                binding?.noDataLinear?.visibility = View.GONE
                val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            }
        }
    }

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun musicOptionsDialog(mTrackDetails: TrackListCommon?) {
        val viewGroup: ViewGroup? = null
        var mPlayList: MutableList<PlaylistCommon>? = ArrayList()
        val mBottomSheetDialog = RoundedBottomSheetDialog(requireContext())
        val sheetView = layoutInflater.inflate(R.layout.bottom_menu_now_playing, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()

        val likeMaterialButton: ImageView = sheetView.findViewById(R.id.like_material_button)
        val likeLinear: LinearLayout = sheetView.findViewById(R.id.like_linear)
        val artistLinear: LinearLayout = sheetView.findViewById(R.id.artist_linear)
        val addPlayListImageView: ImageView =
            sheetView.findViewById(R.id.add_play_list_image_view)
        val playListRecyclerView: RecyclerView =
            sheetView.findViewById(R.id.play_list_recycler_view)
        val equalizerLinear: LinearLayout = sheetView.findViewById(R.id.equalizer_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        val albumLinear: LinearLayout = sheetView.findViewById(R.id.album_linear)
        val downloadLinear: LinearLayout = sheetView.findViewById(R.id.download_linear)


        val allowOfflineDownload: String? = mTrackDetails?.allowOfflineDownload
        if (allowOfflineDownload == "1") {
            downloadLinear.visibility = View.VISIBLE
        } else {
            downloadLinear.visibility = View.GONE
        }

        albumLinear.visibility = View.VISIBLE
        artistLinear.visibility = View.VISIBLE
        likeLinear.visibility = View.VISIBLE
        equalizerLinear.visibility = View.GONE

        val mTrackId: String? = mTrackDetails?.id
        var isLiked: String? = mTrackDetails?.favorite
        if (isLiked == "true") {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)
        } else {
            likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_grey)
        }


        downloadLinear.setOnClickListener {

            mBottomSheetDialog.dismiss()
            showRewardedVideo(mTrackDetails!!)
//            firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
//
//
//            val paramsNotification = Bundle()
//            paramsNotification.putString(
//                "user_id",
//                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")
//            )
//            paramsNotification.putString("track_id", mTrackDetails?.id)
//            paramsNotification.putString("event_type", "track_download")
//            firebaseAnalytics.logEvent("offline_track_download", paramsNotification)
//
//
//
//            searchViewModel?.addToDownload(mTrackDetails!!)
//            mBottomSheetDialog.dismiss()
//            Toast.makeText(activity, "Track added to queue", Toast.LENGTH_SHORT).show()
//
//
//            val handler = Handler(Looper.getMainLooper())
//            handler.postDelayed({
//                enqueueDownloads()
//            }, 1000)

        }
        likeMaterialButton.setOnClickListener {
            if (isLiked == "false") {
                isLiked = "true"

                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )
                //TODO : Update in Inner list
                updateLike(mTrackId, isLiked!!)
                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {
                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "true"
                    }

                }

                likeMaterialButton.setImageResource(R.drawable.ic_favorite_blue_24dp)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        activity,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        activity,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                searchViewModel.addToFavourites(mAuth, mTrackId)

            } else {
                isLiked = "false"
                // TODO : Update list ( Featured, Top , Trending )
                EventBus.getDefault().post(
                    UpdateTrackEvent(
                        mTrackId!!,
                        isLiked!!
                    )
                )
                //TODO : Update in Inner list
                updateLike(mTrackId, isLiked!!)

                val mPlayListSize = MyApplication.playlistManager?.itemCount
                for (i in 0 until mPlayListSize!!) {

                    val mCurrentTrackId = MyApplication.playlistManager?.getItem(i)?.mExtraId
                    if (mCurrentTrackId == mTrackId) {
                        MyApplication.playlistManager?.getItem(i)?.isFavorite = "false"
                    }

                }
                likeMaterialButton.setImageResource(R.drawable.ic_favorite_border_blue_24dp)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(
                        activity,
                        RemoteConstant.mApiKey,
                        ""
                    )!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.trackData +
                            RemoteConstant.mSlash + mTrackId +
                            RemoteConstant.pathFavorite, RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        activity,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                searchViewModel.removeFavourites(mAuth, mTrackId)

            }

        }


        val mAuth =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                requireContext(),
                RemoteConstant.pathPlayList
            )
        searchViewModel.getPlayList(mAuth)

        searchViewModel.playListModel?.nonNull()
            ?.observe(viewLifecycleOwner, {
                if (it.isNotEmpty()) {
                    mPlayList = it as MutableList
                    playListRecyclerView.adapter =
                        CommonPlayListAdapter(
                            mPlayList, requireContext(), false, glideRequestManager
                        )

                    // TODO: Update playlist ( Home page )
                    EventBus.getDefault().postSticky( UpdatePlaylist(mPlayList!!))
                }

            })
        albumLinear.setOnClickListener {
            val intent = Intent(activity, CommonPlayListActivity::class.java)
            intent.putExtra("mId", mTrackDetails?.album?.id)
            intent.putExtra("title", mTrackDetails?.album?.albumName)
            intent.putExtra("mIsFrom", "artistAlbum")
            startActivity(intent)
        }
        artistLinear.setOnClickListener {
            val detailIntent = Intent(activity, ArtistPlayListActivity::class.java)
            detailIntent.putExtra("mArtistId", mTrackDetails?.artist?.id)
            detailIntent.putExtra("mArtistName", mTrackDetails?.artist?.artistName)
            startActivity(detailIntent)
            activity?.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        }
        // TODO: Add to play list
        addPlayListImageView.setOnClickListener {
            mLastId = ""
            createPlayListDialogFragment()
        }
        playListRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                context = requireContext(),
                recyclerView = playListRecyclerView,
                mListener = object : RecyclerItemClickListener.ClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //TODO : Call Add to playlist API
                        if (mPlayList != null && mPlayList!![position].id != null) {
                            addTrackToPlayList(mTrackDetails?.id!!, mPlayList!![position].id!!)
                        }
                        mBottomSheetDialog.dismiss()
                    }

                    override fun onLongItemClick(view: View?, position: Int) {

                    }


                })
        )

        shareLinear.setOnClickListener {
            ShareAppUtils.shareTrack(
                mTrackDetails?.id!!,
                activity
            )
            mBottomSheetDialog.dismiss()
        }
    }

    private fun updateLike(mTrackId: String?, liked: String) {

        for (i in 0 until dbPlayList?.size!!) {
            if (dbPlayList?.get(i)?.id == mTrackId) {
                dbPlayList?.get(i)?.favorite = liked
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }
                    if (playListImageView != null) {
                        glideRequestManager.load(mFileUrl).into(playListImageView!!)
                    }
                }
            }
        }
    }

    private fun removeLoaderFormList() {
        if (dbPlayList != null &&
            dbPlayList?.size!! > 0 &&
            dbPlayList?.get(dbPlayList?.size!! - 1) != null &&
            dbPlayList?.get(dbPlayList?.size!! - 1)?.id == ""
        ) {
            dbPlayList?.removeAt(dbPlayList?.size!! - 1)
            mTrackAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }


    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PLAYING -> {
                updatePlayPause()
            }
            ERROR -> {
            }
            PAUSED -> {
                updatePlayPause()
            }
            RETRIEVING -> {

            }
            PREPARING -> {

            }
            SEEKING -> {

            }
            STOPPED -> {

            }
        }
        return true
    }


    // TODO : Offline track download


    private val GROUP_ID = "listGroup".hashCode()
    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs?.size!! > 0) {

            val requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs)
            if (requests != null && requests.size > 0) {
                fetch?.enqueue(requests) {

                }
            }
        }
    }

    private fun showRewardedVideo(mTrackDetails: TrackListCommon) {
        if (mRewardedAd.isLoaded) {

            mRewardedAd.show(
                activity,
                object : RewardedAdCallback() {
                    override fun onUserEarnedReward(
                        rewardItem: RewardItem
                    ) {
                        isRewardEarned = true
                    }

                    override fun onRewardedAdClosed() {
                        isRewardEarned = if (!isRewardEarned) {

                            Toast.makeText(
                                activity, "Finish the ad to download the track", Toast.LENGTH_LONG
                            ).show()
                            false
                        } else {
                            startDownload(mTrackDetails)
                            false
                        }
                        loadRewardedAd()
                    }

                    override fun onRewardedAdFailedToShow(errorCode: Int) {
                        Toast.makeText(
                            activity,
                            "something went wrong please try again",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                    override fun onRewardedAdOpened() {
                    }
                }
            )
        } else {
            startDownload(mTrackDetails)
        }
    }

    private fun startDownload(mTrackDetails: TrackListCommon) {

        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        val paramsNotification = Bundle()
        paramsNotification.putString(
            "user_id",
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")
        )
        paramsNotification.putString("track_id", mTrackDetails.id)
        paramsNotification.putString("event_type", "track_download")
        firebaseAnalytics.logEvent("offline_track_download", paramsNotification)
        searchViewModel.addToDownload(mTrackDetails)
        Toast.makeText(activity, "Track added to queue", Toast.LENGTH_SHORT).show()


        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            enqueueDownloads()
        }, 1000)
    }

    private fun loadRewardedAd() {
        if (!(::mRewardedAd.isInitialized) || !mRewardedAd.isLoaded) {
            mIsLoading = true
            mRewardedAd = RewardedAd(activity, getString(R.string.ad_unit_id_reward))
            mRewardedAd.loadAd(
                AdRequest.Builder()
                    .build(),
                object : RewardedAdLoadCallback() {
                    override fun onRewardedAdLoaded() {
                        mIsLoading = false
                    }

                    override fun onRewardedAdFailedToLoad(errorCode: Int) {
                        mIsLoading = false
                    }
                }
            )
        }
    }

}
