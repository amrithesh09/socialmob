package com.example.sample.socialmob.view.ui.home_module.settings

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import kotlinx.android.synthetic.main.about_socail_mob_activity.*


class AboutSocialMobActivity : BaseCommonActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_socail_mob_activity)

        about_socialmob_back_image_view.setOnClickListener {
            onBackPressed()
        }
        licenses_text_view.setOnClickListener {
            val intent = Intent(this, TermsConditionsPrivacyPolicyCommonActivity::class.java)
            intent.putExtra("mTitle", getString(R.string.privacy_policy_))
            intent.putExtra("mUrl", "https://socialmob.me/privacypolicy.html")
            startActivity(intent)
        }
        terms_of_service_text_view.setOnClickListener {
            val intent = Intent(this, TermsConditionsPrivacyPolicyCommonActivity::class.java)
            intent.putExtra("mTitle", getString(R.string.terms_conditions_))
            intent.putExtra("mUrl", "https://socialmob.me/termsofuse.html")
            startActivity(intent)
        }

        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            version_text_view.text = getString(R.string.version) + " " + version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@AboutSocialMobActivity, about_socialmob_back_image_view)
    }

}
