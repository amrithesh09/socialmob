package com.example.sample.socialmob.view.ui.home_module.settings

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.BlockedUsersItemBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import org.jetbrains.anko.layoutInflater


class BlockedUserAdapter(
    private val mContext: Context?,
    private val clickListener: ItemClickListener
) :
    ListAdapter<SearchProfileResponseModelPayloadProfile, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<SearchProfileResponseModelPayloadProfile>() {
                override fun areItemsTheSame(
                    oldItem: SearchProfileResponseModelPayloadProfile,
                    newItem: SearchProfileResponseModelPayloadProfile
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: SearchProfileResponseModelPayloadProfile,
                    newItem: SearchProfileResponseModelPayloadProfile
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgerssViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: BlockedUsersItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.blocked_users_item, parent, false
        )
        return BlockedUserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(getItem(holder.adapterPosition))
    }


    override fun getItemViewType(position: Int): Int {

        return if (getItem(position)._id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }


    inner class ProgerssViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class BlockedUserViewHolder(val binding: BlockedUsersItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.blockedUserViewModel = getItem(adapterPosition)
            binding.executePendingBindings()

            itemView.setOnClickListener {
                //
                val viewGroup: ViewGroup? = null
                val dialogBuilder = this.let { AlertDialog.Builder(mContext!!) }
                val inflater = mContext!!.layoutInflater
                val dialogView = inflater.inflate(R.layout.blocked_user_dialog, viewGroup)
                dialogBuilder.setView(dialogView)
                dialogBuilder.setCancelable(false)

                val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                val unblockButton: Button = dialogView.findViewById(R.id.unblock_button)

                val b: AlertDialog = dialogBuilder.create()
                b.show()
                unblockButton.setOnClickListener {
                    clickListener.onItemClick(
                        getItem(adapterPosition)._id!!,
                        adapterPosition
                    )
                    b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                }
                cancelButton.setOnClickListener {
                    b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                }
            }
        }

    }

    interface ItemClickListener {
        fun onItemClick(mProfileId: String, mPosition: Int)
    }
}