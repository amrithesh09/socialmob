package com.example.sample.socialmob.view.ui.home_module.settings

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.BlockedUsersListActivityBinding
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.blocked_users_list_activity.*
import javax.inject.Inject

@AndroidEntryPoint
class BlockedUsersActivity : BaseCommonActivity(), BlockedUserAdapter.ItemClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private val profileViewModel: ProfileViewModel by viewModels()
    private var mBlockedUserAdapter: BlockedUserAdapter? = null
    private var binding: BlockedUsersListActivityBinding? = null
    private var blockedUserList: MutableList<SearchProfileResponseModelPayloadProfile>? =
        ArrayList()

    private var isAddedLoader: Boolean = false
    private var mStatus: ResultResponse.Status? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.blocked_users_list_activity)

        // TODO : Check Internet connection
        checkInternet()
        observeData()
        binding?.blockedUsersBackImageView?.setOnClickListener {
            onBackPressed()
        }
        binding?.refreshTextView?.setOnClickListener {
            checkInternet()
        }

        binding?.blockedUsersRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding?.blockedUsersRecyclerView?.addOnScrollListener(CustomScrollListener())
    }

    inner class CustomScrollListener : RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: RecyclerView, newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: RecyclerView, dx: Int, dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager?.childCount!!
            val totalItemCount = recyclerView.layoutManager?.itemCount!!
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (blockedUserList?.size!! >= 15 && mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                mStatus != ResultResponse.Status.LOADING && !isAddedLoader
            ) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    loadMoreItems(blockedUserList?.size!!)
                }
            }
        }
    }

    private fun loadMoreItems(size: Int) {
        val blockedUser: MutableList<SearchProfileResponseModelPayloadProfile> = ArrayList()
        val mList = SearchProfileResponseModelPayloadProfile(
            0, "", "", "", "", "",
            "", "", "", "", "", false, "", ""
        )

        blockedUser.add(mList)
        blockedUserList?.addAll(blockedUser)
        mBlockedUserAdapter?.notifyDataSetChanged()
        isAddedLoader = true

        val mAuth = RemoteConstant.getAuthBlockedUsers(this, size)
        profileViewModel.getBlockedUsers(mAuth, size.toString(), RemoteConstant.mCount)
    }

    private fun observeData() {
        mBlockedUserAdapter = BlockedUserAdapter(this@BlockedUsersActivity, this)
        blocked_users_recycler_view.adapter = mBlockedUserAdapter
        profileViewModel.blockedUsersResponseModelPayloadUser?.nonNull()
            ?.observe(this, { trackList ->

                mStatus = trackList.status

                when (trackList.status) {
                    ResultResponse.Status.SUCCESS -> {
                        blockedUserList = trackList?.data!!.toMutableList()
                        mBlockedUserAdapter?.submitList(blockedUserList)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.blockedUsersRecyclerView?.visibility = View.VISIBLE
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.NO_INTERNET -> {
                        glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                            .into(binding?.noInternetImageView!!)
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        blockedUserList?.addAll(trackList?.data!!)
                        mBlockedUserAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {
                        if (blockedUserList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.blockedUsersRecyclerView?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.blockedUsersRecyclerView?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                        glideRequestManager.load(R.drawable.ic_user_data).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.blockedUsersRecyclerView?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }

            })
    }

    private fun checkInternet() {
        if (InternetUtil.isInternetOn()) {
            callApiBlockedUsers()
        } else {
            profileViewModel.blockedUsersResponseModelPayloadUser?.postValue(
                ResultResponse(ResultResponse.Status.NO_INTERNET, ArrayList(), "")
            )
            val shake: Animation =
                AnimationUtils.loadAnimation(this@BlockedUsersActivity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        }
    }

    private fun removeLoaderFormList() {
        if (blockedUserList?.size!! > 0 && blockedUserList?.get(blockedUserList?.size!! - 1) != null
            && blockedUserList?.get(blockedUserList?.size!! - 1)?.id == 0
        ) {
            blockedUserList?.removeAt(blockedUserList?.size!! - 1)
            mBlockedUserAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }


    private fun callApiBlockedUsers() {
        val mAuth = RemoteConstant.getAuthBlockedUsers(this, 0)
        profileViewModel.getBlockedUsers(mAuth, RemoteConstant.mOffset, RemoteConstant.mCount)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@BlockedUsersActivity, blocked_users_back_image_view)
    }

    override fun onItemClick(mProfileId: String, mPosition: Int) {
        val mAuth = RemoteConstant.getAuthUnBlockUser(this, mProfileId)
        if (InternetUtil.isInternetOn()) {
            profileViewModel.unbBlockUser(mAuth, mProfileId)
            blockedUserList?.removeAt(mPosition)
            mBlockedUserAdapter!!.notifyDataSetChanged()
            if (blockedUserList?.size!! == 0) {
                profileViewModel.blockedUsersResponseModelPayloadUser?.postValue(
                    ResultResponse(ResultResponse.Status.NO_DATA, ArrayList(), "")
                )
            }
        }
    }

}
