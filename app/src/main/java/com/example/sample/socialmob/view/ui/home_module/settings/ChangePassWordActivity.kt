package com.example.sample.socialmob.view.ui.home_module.settings

import android.app.ActivityManager
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.text.method.SingleLineTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import com.bumptech.glide.Glide
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.change_password_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response

@AndroidEntryPoint
class ChangePassWordActivity : BaseCommonActivity(), GoogleApiClient.OnConnectionFailedListener {
    private val profileViewModel: ProfileViewModel by viewModels()
    private var mGoogleApiClient: GoogleApiClient? = null
    private var isEnabledOldPassword = false
    private var isEnabledNewPassword = false
    private var isEnabledReEnterPassword = false

    private var customProgressDialog: CustomProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password_activity)

        customProgressDialog = CustomProgressDialog(this)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestScopes(Scope(Scopes.PLUS_ME), Scope(Scopes.PROFILE), Scope(Scopes.EMAIL))
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()

        change_password_back_press_image_view.setOnClickListener {
            onBackPressed()
        }
        cancel_text_view.setOnClickListener {
            onBackPressed()
        }
        change_password_text_view.setOnClickListener {
            callApiChangePassword()
        }

        //TODO : Password
        eye_old_password_image.setOnClickListener {
            if (!isEnabledOldPassword) {
                isEnabledOldPassword = true
                eye_old_password_image?.setImageResource(R.drawable.ic_eye_enable)
                old_password_edit_text?.transformationMethod = SingleLineTransformationMethod()
            } else {
                isEnabledOldPassword = false
                eye_old_password_image?.setImageResource(R.drawable.ic_eye_disable)
                old_password_edit_text?.transformationMethod = PasswordTransformationMethod()
            }

            old_password_edit_text?.setSelection(
                old_password_edit_text?.text?.length ?: 0
            )
        }

        eye_new_password_image_view.setOnClickListener {
            if (!isEnabledNewPassword) {
                isEnabledNewPassword = true
                eye_new_password_image_view?.setImageResource(R.drawable.ic_eye_enable)
                new_password_edit_text?.transformationMethod = SingleLineTransformationMethod()
            } else {
                isEnabledNewPassword = false
                eye_new_password_image_view?.setImageResource(R.drawable.ic_eye_disable)
                new_password_edit_text?.transformationMethod = PasswordTransformationMethod()
            }

            new_password_edit_text?.setSelection(
                new_password_edit_text?.text?.length ?: 0
            )
        }

        eye_re_enter_password_image.setOnClickListener {
            if (!isEnabledReEnterPassword) {
                isEnabledReEnterPassword = true
                eye_re_enter_password_image?.setImageResource(R.drawable.ic_eye_enable)
                re_enter_password_edit_text?.transformationMethod = SingleLineTransformationMethod()
            } else {
                isEnabledReEnterPassword = false
                eye_re_enter_password_image?.setImageResource(R.drawable.ic_eye_disable)
                re_enter_password_edit_text?.transformationMethod = PasswordTransformationMethod()
            }
            re_enter_password_edit_text?.setSelection(
                re_enter_password_edit_text?.text?.length ?: 0
            )
        }

        old_password_edit_text?.transformationMethod = PasswordTransformationMethod()
        new_password_edit_text?.transformationMethod = PasswordTransformationMethod()
        re_enter_password_edit_text?.transformationMethod = PasswordTransformationMethod()

        eye_re_enter_password_image?.visibility = View.GONE
        re_enter_password_edit_text.doOnTextChanged { it, _, _, _ ->
            // Respond to input text change
            if (it != null && it.isNotEmpty() || re_enter_password_edit_text?.hint != getString(R.string.re_enter_new_password)) {
                eye_re_enter_password_image?.visibility = View.VISIBLE
            } else {
                eye_re_enter_password_image?.visibility = View.GONE
            }
        }


        eye_new_password_image_view?.visibility = View.GONE
        new_password_edit_text.doOnTextChanged { it, _, _, _ ->
            // Respond to input text change
            if (it != null && it.isNotEmpty() || new_password_edit_text?.hint != getString(R.string.enter_new_password)) {
                eye_new_password_image_view?.visibility = View.VISIBLE
            } else {
                eye_new_password_image_view?.visibility = View.GONE
            }
        }

        eye_old_password_image?.visibility = View.GONE
        old_password_edit_text.doOnTextChanged { it, _, _, _ ->
            // Respond to input text change
            if (it != null && it.isNotEmpty() || old_password_edit_text?.hint != getString(R.string.enter_old_password)) {
                eye_old_password_image?.visibility = View.VISIBLE
            } else {
                eye_old_password_image?.visibility = View.GONE
            }
        }
    }

    private fun callApiChangePassword() {


        val mOldPassword: String = old_password_edit_text.text.toString()
        val mNewPassWord: String = new_password_edit_text.text.toString()
        val mReenterNewPassWord: String = re_enter_password_edit_text.text.toString()

        if (mOldPassword.isNotEmpty()) {
            if (mNewPassWord.isNotEmpty()) {
                if (mReenterNewPassWord.isNotEmpty()) {
                    if (mReenterNewPassWord.length > 5) {
                        if (mNewPassWord == mReenterNewPassWord) {
                            if (InternetUtil.isInternetOn()) {
                                customProgressDialog!!.show()
                                val fullData = RemoteConstant.getEncryptedString(
                                    SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mAppId,
                                        ""
                                    )!!,
                                    SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mApiKey,
                                        ""
                                    )!!,
                                    RemoteConstant.mBaseUrl + RemoteConstant.mChangePassword,
                                    RemoteConstant.postMethod
                                )
                                val mAuth =
                                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                        this, RemoteConstant.mAppId,
                                        ""
                                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


                                profileViewModel.changePassword(
                                    mAuth, mOldPassword, mNewPassWord, mReenterNewPassWord
                                )

                                profileViewModel.isSucessChangePasswordLiveData.nonNull()
                                    .observe(this, {
                                        if (!profileViewModel.isLoadingApiChangePassword) {
                                            if (profileViewModel.isSucessChangePassword) {

                                                logOutDialog()
                                            } else {
                                                customProgressDialog!!.dismiss()
                                            }
                                        }
                                    })


                            } else {
                                AppUtils.showCommonToast(this, getString(R.string.no_internet))
                            }

                        } else {
                            AppUtils.showCommonToast(this, "The passwords don't match.")
                        }
                    } else {
                        AppUtils.showCommonToast(
                            this,
                            "Password should be atleast 6 characters long."
                        )

                    }

                } else {
                    AppUtils.showCommonToast(this, "Please re-enter your new password.")
                }

            } else {
                AppUtils.showCommonToast(this, "Please provide your new password.")
            }

        } else {
            AppUtils.showCommonToast(this, "Please provide your current password.")
        }

    }

    private fun logOutDialog() {
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(it) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.logout_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val logoutCancelButton: Button = dialogView.findViewById(R.id.logout_cancel_button)
        val logOutButton: Button = dialogView.findViewById(R.id.log_out_button)
        val dialogDescription: TextView = dialogView.findViewById(R.id.dialog_description)
        dialogDescription.text = getString(R.string.log_out_from_all_devices)
        logoutCancelButton.text = getString(R.string.no_)
        logOutButton.text = getString(R.string.yes_)
        val b = dialogBuilder.create()
        b.show()
        logOutButton.setOnClickListener {
            signOut()
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        logoutCancelButton.setOnClickListener {
            signOut()
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    private fun signOut() {
        if (InternetUtil.isInternetOn()) {
            Glide.get(MyApplication.application!!).clearMemory()
            RemoteConstant.clearGlideCache()
            GlobalScope.launch {
                var response: Response<Any>? = null
                kotlin.runCatching {
                    response = WebServiceClient.client.create(BackEndApi::class.java)
                        .deleteUUID(
                            SharedPrefsUtils.getStringPreference(
                                this@ChangePassWordActivity,
                                RemoteConstant.mAppId,
                                ""
                            )!!
                        )
                }.onSuccess {
                    when (response!!.code()) {
                        200 -> clearAll()
                        else -> RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "Log out api"
                        )
                    }
                }.onFailure {
                    clearAll()
                }
            }

        } else {
            clearAll()
        }


    }

    private fun clearAll() {
        // Clear all notification
        GlobalScope.launch(Dispatchers.Main) {
            if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                val serviceClass1 = MediaServiceRadio::class.java
                val intent11 = Intent(this@ChangePassWordActivity, serviceClass1)
                stopService(intent11)
            }
            if (isMyServiceRunning(MediaService::class.java)) {
                val serviceClass1 = MediaService::class.java
                val intent11 = Intent(this@ChangePassWordActivity, serviceClass1)
                stopService(intent11)
            }
            profileViewModel.clearDataBase()
            if (getSystemService(Context.NOTIFICATION_SERVICE) != null) {
                val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                nMgr.cancelAll()
            }
            if (mGoogleApiClient!!.isConnected) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient)
                mGoogleApiClient!!.disconnect()
                mGoogleApiClient!!.connect()
            }
            SharedPrefsUtils.setStringPreference(
                this@ChangePassWordActivity,
                RemoteConstant.mAppId,
                ""
            )
            SharedPrefsUtils.setStringPreference(
                this@ChangePassWordActivity,
                RemoteConstant.mApiKey,
                ""
            )
            SharedPrefsUtils.setStringPreference(
                this@ChangePassWordActivity,
                RemoteConstant.mProfileId,
                ""
            )
            SharedPrefsUtils.setBooleanPreference(
                this@ChangePassWordActivity, RemoteConstant.mEnabledPushNotification, false
            )
            SharedPrefsUtils.setBooleanPreference(
                this@ChangePassWordActivity, RemoteConstant.mEnabledPrivateProfile, false
            )

            customProgressDialog!!.dismiss()
            val intent = Intent(this@ChangePassWordActivity, MainLandingActivity::class.java)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finishAffinity()
        }

    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager: ActivityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient?.isConnected!!) {
            mGoogleApiClient?.disconnect()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@ChangePassWordActivity, change_password_back_press_image_view)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customProgressDialog != null && customProgressDialog!!.isShowing)
            customProgressDialog!!.dismiss()
    }


}
