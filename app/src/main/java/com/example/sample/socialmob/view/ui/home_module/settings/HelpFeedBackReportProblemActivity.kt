package com.example.sample.socialmob.view.ui.home_module.settings

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.HelpFeedbackActivityBinding
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.help_feedback_activity.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HelpFeedBackReportProblemActivity : BaseCommonActivity() {

    private var mTitle = ""
    private var mPostId = ""
    private var customProgressDialog: CustomProgressDialog? = null
    private var binding: HelpFeedbackActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.help_feedback_activity)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val currentOrientation = resources.configuration.orientation
            requestedOrientation = if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        }

        customProgressDialog = CustomProgressDialog(this)
        // TODO : Get Bundle Value
        mTitle = intent.getStringExtra("mTitle")!!
        if (intent.hasExtra("mPostId")) {
            mPostId = intent.getStringExtra("mPostId")!!
        }
        binding?.helpFeedbackTitleTextView?.text = mTitle


        //String array.
        val myStrings =
            arrayOf(
                "Using Socialmob",
                "My account",
                "Fix a problem",
                "Policies & reporting",
                "Feature your music"
            )
        //Adapter for spinner

        binding?.feedbackSpinner?.adapter =
            ArrayAdapter(this, R.layout.simple_spinner_dropdown_item, myStrings)


        if (mPostId == "") {
            feedback_spinner_text_view.setOnClickListener {
                feedback_spinner.performClick()
            }
        } else {
            subject_edit_text.visibility = View.GONE
        }
        feedback_spinner.setSelection(0)


        //item selected listener for spinner
        feedback_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                feedback_spinner_text_view.text = myStrings[p2]
            }

        }
        help_feedback_back_image_view.setOnClickListener {
            onBackPressed()
        }

        cancel_text_view.setOnClickListener {
            onBackPressed()
        }


        if (mPostId == "") {
            feedback_submit.setOnClickListener {
                if (subject_edit_text.text.isNotEmpty()) {
                    if (message_edit_text.text.isNotEmpty()) {

                        feedbackSubmitDialog("Confirm submitting ?")
                    } else {
                        AppUtils.showCommonToast(this, "You haven't entered your message.")
                    }
                } else {
                    AppUtils.showCommonToast(this, "You haven't entered a subject.")
                }

            }
        } else {
            feedback_submit.setOnClickListener {
                if (message_edit_text.text.isNotEmpty()) {

                    feedbackPostSubmitDialog("Confirm submitting ?")
                } else {
                    AppUtils.showCommonToast(this, "You haven't entered your message.")
                }
            }
        }

    }

    private fun feedbackPostSubmitDialog(mTitle: String) {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(it) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.feedback_submit_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val dialogTitle: TextView = dialogView.findViewById(R.id.dialog_title)
        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val confirmButton: Button = dialogView.findViewById(R.id.confirm_button)

        dialogTitle.text = mTitle
        val b: AlertDialog = dialogBuilder.create()
        b.show()
        confirmButton.setOnClickListener {
            customProgressDialog?.show()
            callApiReport()

            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        cancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }


    private fun feedbackSubmitDialog(mTitle: String) {
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(it) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.feedback_submit_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val dialogTitle: TextView = dialogView.findViewById(R.id.dialog_title)
        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val confirmButton: Button = dialogView.findViewById(R.id.confirm_button)

        dialogTitle.text = mTitle
        val b: AlertDialog = dialogBuilder.create()
        b.show()
        confirmButton.setOnClickListener {
            customProgressDialog?.show()
            callApiFeedBack()

            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        cancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    private fun callApiReport() {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.postData +
                    RemoteConstant.mSlash + mPostId + RemoteConstant.mPathReport,
            RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        GlobalScope.launch {
            var response: Response<Any>? = null
            kotlin.runCatching {
                response = WebServiceClient.client.create(BackEndApi::class.java).reportPost(
                    mAuth, RemoteConstant.mPlatform, mPostId, message_edit_text.text.toString()
                )
            }.onSuccess {
                when (response?.code()) {
                    200 -> finish()
                    else -> {
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "Log out api"
                        )
                    }
                }
            }.onFailure {
                customProgressDialog?.dismiss()
            }
        }
    }


    private fun callApiFeedBack() {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.feedbackData, RemoteConstant.postMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mAppId, ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        if (InternetUtil.isInternetOn()) {
            var response: Response<Any>? = null
            GlobalScope.launch {
                kotlin.runCatching {
                    response = WebServiceClient.client.create(BackEndApi::class.java)
                        .postFeedBack(
                            mAuth,
                            RemoteConstant.mPlatform,
                            feedback_spinner.selectedItem.toString(),
                            subject_edit_text.text.toString(),
                            message_edit_text.text.toString()
                        )
                }.onSuccess {
                    when (response!!.code()) {
                        200 -> finish()
                        else -> {
                            RemoteConstant.apiErrorDetails(
                                application, response?.code()!!, "Log out api"
                            )
                        }
                    }
                }.onFailure {
                    customProgressDialog?.dismiss()
                }
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    private fun changeStatusBarColorBlack() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@HelpFeedBackReportProblemActivity, help_feedback_back_image_view)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customProgressDialog != null && customProgressDialog?.isShowing!!)
            customProgressDialog?.dismiss()
    }


}
