package com.example.sample.socialmob.view.ui.home_module.settings

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.InviteActivityBinding
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InviteActivity : BaseCommonActivity() {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var binding: InviteActivityBinding? = null
    private var mShareUrl: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        window.setFlags(
//            WindowManager.LayoutParams.FLAG_FULLSCREEN,
//            WindowManager.LayoutParams.FLAG_FULLSCREEN
//        )
        binding = DataBindingUtil.setContentView(this, R.layout.invite_activity)

        val i: Intent? = intent
        mShareUrl = i?.getStringExtra("mShareUrl")?:""

        initClickListner()
    }

    private fun initClickListner() {
        binding?.inviteImageView?.let {
            glideRequestManager.load(R.drawable.ic_invite_new).into(it)
        }

        binding?.closeButton?.setOnClickListener {
            finish()
        }
        binding?.inviteTextView?.setOnClickListener {
            // Obtain the FirebaseAnalytics instance.
            if (mShareUrl != "") {
                firebaseAnalytics = FirebaseAnalytics.getInstance(this)
                val paramsNotification = Bundle()
                paramsNotification.putString(
                    "user_id",
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
                )
                paramsNotification.putString("event_type", "user_invite")
                firebaseAnalytics.logEvent("invites", paramsNotification)
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, mShareUrl)
                startActivity(Intent.createChooser(shareIntent, getString(R.string.send_to)))
            }
        }
    }
}
