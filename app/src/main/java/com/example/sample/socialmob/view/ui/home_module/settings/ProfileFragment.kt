package com.example.sample.socialmob.view.ui.home_module.settings

import android.app.ActivityManager
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ProfileFragmentBinding
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.ui.profile.EditProfileActivity
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.view.utils.offline.DownloadListActivity
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.viewmodel.settings.SettingViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment(), GoogleApiClient.OnConnectionFailedListener {

    @Inject
    lateinit var glideRequestManager: RequestManager


    private var mShareUrl: String = ""
    private var mLastClickTime: Long = 0
    private val settingViewModel: SettingViewModel by viewModels()
    private var mGoogleApiClient: GoogleApiClient? = null
    private var customProgressDialog: CustomProgressDialog? = null
    private var binding: ProfileFragmentBinding? = null
    private val viewModelChat: WebSocketChatViewModel? by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }
    /**
     * Connect to google API client
     */
    override fun onStart() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(requireActivity())
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
        mGoogleApiClient?.connect()
        super.onStart()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        customProgressDialog = CustomProgressDialog(activity)

        binding?.profileImageCardView?.setOnClickListener {
            binding?.viewProfile?.performClick()
        }
        binding?.viewProfile?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(activity, MyProfileActivity::class.java)
                startActivityForResult(intent, 0)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.notificationLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                if (binding?.redDotNotificationImageView?.visibility == View.VISIBLE) {
                    binding?.redDotNotificationImageView?.visibility = View.GONE
                    val mMode =
                        SharedPrefsUtils.getBooleanPreference(
                            activity,
                            RemoteConstant.NIGHT_MODE,
                            false
                        )
                    if (mMode) {
                        binding?.notificationTextView?.setTextColor(Color.parseColor("#ced6e0"))
                        DrawableCompat.setTint(
                            binding?.notificationImageView?.drawable!!,
                            ContextCompat.getColor(requireActivity(), R.color.line_color_new)
                        )
                    } else {
                        binding?.notificationTextView?.setTextColor(Color.parseColor("#57606f"))
                        DrawableCompat.setTint(
                            binding?.notificationImageView?.drawable!!,
                            ContextCompat.getColor(
                                requireActivity(),
                                R.color.selected_notification_color
                            )
                        )
                    }
                }
                val intent = Intent(activity, CommonFragmentActivity::class.java)
                intent.putExtra("isFrom", "Notification")
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.inviteFriendsLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(activity, InviteActivity::class.java)
                intent.putExtra("mShareUrl", mShareUrl)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }

        binding?.settingsLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(activity, SettingsActivity::class.java)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.editProfileLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(activity, EditProfileActivity::class.java)
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
        binding?.logOutLinear?.setOnClickListener {
            logOutDialog()
        }
        binding?.shareProfileLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // TODO : Share my profile
            ShareAppUtils.shareProfile(
                requireActivity(),
                SharedPrefsUtils.getStringPreference(
                    requireActivity(), RemoteConstant.mProfileId, ""
                )
            )
        }
        binding?.myDownloadsLinear?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                println(">Double tap")
            } else {
                val intent = Intent(activity, DownloadListActivity::class.java)
                intent.putExtra("mIsFrom", "myDownloads")
                startActivity(intent)
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        }
    }
    /**
     * Observe profile details from local DB
     */
    private fun observeProfileData() {
        settingViewModel.profile.nonNull().observe(this, { profle ->
            if (profle != null && binding != null) {
                binding?.profileData = profle
                if (profle.pimage != "") {
                    glideRequestManager.load(profle.pimage).into(binding?.profileImageView!!)
                } else {
                    glideRequestManager.load(R.drawable.emptypicture).into(binding?.profileImageView!!)
                }
                binding?.executePendingBindings()
                if (profle.inviteLink != null) {
                    mShareUrl = profle.inviteLink
                }
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false)
        return binding?.root
    }
    /**
     * Logout confirmation dialog
     */
    private fun logOutDialog() {
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.logout_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val logoutCancelButton: Button = dialogView.findViewById(R.id.logout_cancel_button)
        val logOutButton: Button = dialogView.findViewById(R.id.log_out_button)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        logOutButton.setOnClickListener {
            signOut()
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        logoutCancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }
    /**
     * Sign out API
     */
    private fun signOut() {
        viewModelChat?.closeSocketSm()
        Glide.get(requireActivity()).clearMemory()
        RemoteConstant.clearGlideCache()
        (activity as HomeActivity).deleteDownloadedTracks()
        customProgressDialog?.show()
        if (InternetUtil.isInternetOn()) {
            GlobalScope.launch {
                var response: Response<Any>? = null
                kotlin.runCatching {
                    response = WebServiceClient.client.create(BackEndApi::class.java)
                        .deleteUUID(
                            SharedPrefsUtils.getStringPreference(
                                activity,
                                RemoteConstant.mAppId,
                                ""
                            )!!
                        )
                }.onSuccess {
                    when (response!!.code()) {
                        200 -> clearAll()
                        else -> {
                            RemoteConstant.apiErrorDetails(
                                requireActivity(), response?.code()!!, "Log out api"

                            )
                            clearAll()
                        }
                    }
                }.onFailure {
                    clearAll()
                }
            }
        } else {
            clearAll()
        }


    }
    /**
     *  Clear all data when logout
     *  Google Sign-out
     *  Stop running services
     *  Clear shared preference and local db
     */
    private fun clearAll() {
        // Clear all notification
        GlobalScope.launch(Dispatchers.Main) {

            if (mGoogleApiClient?.isConnected!!) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
                    println("" + it)
                }
            }

            if (isMyServiceRunning(MediaServiceRadio::class.java)) {
                val serviceClass1 = MediaServiceRadio::class.java
                val intent11 = Intent(activity, serviceClass1)
                activity?.stopService(intent11)
            }
            if (isMyServiceRunning(MediaService::class.java)) {
                val serviceClass1 = MediaService::class.java
                val intent11 = Intent(activity, serviceClass1)
                activity?.stopService(intent11)
            }
            settingViewModel.clearDataBase()
            if (activity?.getSystemService(Context.NOTIFICATION_SERVICE) != null) {
                val nMgr =
                    activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                nMgr.cancelAll()
            }

            SharedPrefsUtils.setStringPreference(
                activity,
                RemoteConstant.IS_LOCATION_UPDATED,
                "false"
            )
            SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mAppId, "")
            SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mApiKey, "")
            SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mJwt, "")
            SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mProfileId, "")
            SharedPrefsUtils.setStringPreference(activity, RemoteConstant.mUserName, "")
            SharedPrefsUtils.setBooleanPreference(
                activity,
                RemoteConstant.mEnabledPushNotification,
                false
            )
            SharedPrefsUtils.setBooleanPreference(
                activity,
                RemoteConstant.mEnabledPrivateProfile,
                false
            )

            customProgressDialog?.dismiss()

            val intent = Intent(activity, MainLandingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            activity?.overridePendingTransition(0, 0)
            activity?.finishAffinity()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customProgressDialog != null && customProgressDialog?.isShowing!!)
            customProgressDialog?.dismiss()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }
    /**
     * Get all running services
     */
    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        if (activity != null) {
            val manager: ActivityManager =
                activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)!!) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
        }
        return false
    }
    /**
     * Add notification dot
     */
    fun hasNotification() {
        if (binding != null &&
            binding?.redDotNotificationImageView != null
        ) {
            binding?.redDotNotificationImageView?.visibility = View.VISIBLE
            binding?.notificationTextView?.setTextColor(Color.parseColor("#1e90ff"))
            DrawableCompat.setTint(
                binding?.notificationImageView?.drawable!!,
                ContextCompat.getColor(requireActivity(), R.color.selected_tab_color)
            )
        }
    }
    /**
     * Remove notification dot and color change
     */
    fun removeNotification() {
        if (binding?.redDotNotificationImageView?.visibility == View.VISIBLE) {
            binding?.redDotNotificationImageView?.visibility = View.GONE
            val mMode =
                SharedPrefsUtils.getBooleanPreference(activity, RemoteConstant.NIGHT_MODE, false)
            if (mMode) {
                binding?.notificationTextView?.setTextColor(Color.parseColor("#ced6e0"))
                DrawableCompat.setTint(
                    binding?.notificationImageView?.drawable!!,
                    ContextCompat.getColor(requireActivity(), R.color.line_color_new)
                )
            } else {
                binding?.notificationTextView?.setTextColor(Color.parseColor("#57606f"))
                DrawableCompat.setTint(
                    binding?.notificationImageView?.drawable!!,
                    ContextCompat.getColor(requireActivity(), R.color.selected_notification_color)
                )
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            observeProfileData()
        }
    }

    override fun onResume() {
        super.onResume()
        observeProfileData()
    }


}
