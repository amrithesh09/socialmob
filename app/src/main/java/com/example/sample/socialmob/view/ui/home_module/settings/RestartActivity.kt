package com.example.sample.socialmob.view.ui.home_module.settings

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.restart_activity.*


class RestartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val mMode =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)
        if (mMode) {
            setTheme(R.style.AppTheme_NoActionBarTranslucentNight)
        } else {
            setTheme(R.style.AppTheme_NoActionBarTranslucent)
        }
        super.onCreate(savedInstanceState)
        if (mMode) {
            changeStatusBarColorBlack()
        } else {
            changeStatusBarColor()
        }
        setContentView(R.layout.restart_activity)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val currentOrientation = resources.configuration.orientation
            requestedOrientation = if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        }

        gif_progress?.play()
        gif_progress?.gifResource = R.drawable.loadergif

        SharedPrefsUtils.setBooleanPreference(
            this, RemoteConstant.NIGHT_MODE_CHANGED_POSTION, false
        )

        val mHandler = Handler(Looper.getMainLooper())
        mHandler.postDelayed({

            gif_progress.pause()

            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("mPos", "4")
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
//            TaskStackBuilder.create(this)
//                .addNextIntent(Intent(this, HomeActivity::class.java))
//                .addNextIntent(intent)
//                .startActivities()

        }, 2000)


    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    private fun changeStatusBarColorBlack() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }


}
