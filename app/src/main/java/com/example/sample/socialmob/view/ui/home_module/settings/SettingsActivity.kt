package com.example.sample.socialmob.view.ui.home_module.settings

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.ThemeUtils
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.example.sample.socialmob.viewmodel.settings.SettingViewModel
import kotlinx.android.synthetic.main.settings_activity.*


class SettingsActivity : BaseCommonActivity() {

    private val viewModel: SettingViewModel by viewModels()
    private var customProgressDialog: CustomProgressDialog? = null
    private var isNotChanged = false

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        customProgressDialog = CustomProgressDialog(this)

        settings_change_password_text_view.setOnClickListener {
            val intent = Intent(this, ChangePassWordActivity::class.java)
            startActivity(intent)
        }
        settings_blocked_users_text_view.setOnClickListener {
            val intent = Intent(this, BlockedUsersActivity::class.java)
            startActivity(intent)
        }
        settings_help_feedback_linear.setOnClickListener {
            val intent = Intent(this, HelpFeedBackReportProblemActivity::class.java)
            intent.putExtra("mTitle", getString(R.string.help_feedback_))
            startActivity(intent)
        }
        settings_report_problem_text_view.setOnClickListener {
            val intent = Intent(this, HelpFeedBackReportProblemActivity::class.java)
            intent.putExtra("mTitle", getString(R.string.report_a_problem_))
            startActivity(intent)
        }
        settings_terms_conditions_linear.setOnClickListener {
            val intent = Intent(this, TermsConditionsPrivacyPolicyCommonActivity::class.java)
            intent.putExtra("mTitle", getString(R.string.terms_conditions_))
            intent.putExtra("mUrl", "https://socialmob.me/termsofuse.html")
            startActivity(intent)
        }
        settings_privacy_policy_linear.setOnClickListener {
            val intent = Intent(this, TermsConditionsPrivacyPolicyCommonActivity::class.java)
            intent.putExtra("mTitle", getString(R.string.privacy_policy_))
            intent.putExtra("mUrl", "https://socialmob.me/privacypolicy.html")
            startActivity(intent)
        }
        settings_about_socialmob_linear.setOnClickListener {
            val intent = Intent(this, AboutSocialMobActivity::class.java)
            startActivity(intent)
        }
        settings_back_image_view.setOnClickListener {
            onBackPressed()
        }


        // TODO : To Check Push notification is enabled
        val isPushNotificationEnabled =
            SharedPrefsUtils.getBooleanPreference(
                this,
                RemoteConstant.mEnabledPushNotification, false
            )
        switch_button_push_notification.isChecked = isPushNotificationEnabled
        if (isPushNotificationEnabled) {
            push_notification_image_view.setImageResource(R.drawable.ic_settings_notifications_on)
        } else {
            push_notification_image_view.setImageResource(R.drawable.ic_settings_notifications_off)
        }


        val isPrivateProfile =
            SharedPrefsUtils.getBooleanPreference(
                this, RemoteConstant.mEnabledPrivateProfile,
                false
            )
        switch_button_private_profile.isChecked = isPrivateProfile

        if (isPrivateProfile) {
            account_privacy_image_view.setImageResource(
                R.drawable.ic_account_privacy_lock
            )
        } else {
            account_privacy_image_view.setImageResource(
                R.drawable.ic_account_privacy_unlock
            )
        }


        //TODO : Night Mode
        switch_button_night_mode.isChecked =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)

        switch_button_night_mode.setOnCheckedChangeListener { view, isChecked ->
            if (!isNotChanged) {
                if (!isChecked) {

                    val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
                    val inflater: LayoutInflater = this.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val dialogDescription: TextView =
                        dialogView.findViewById(R.id.dialog_description)
                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    dialogDescription.text = "All services will restart on theme switching."

                    val b: AlertDialog = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {

                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        stopMusic()
                        SharedPrefsUtils.setBooleanPreference(
                            this,
                            RemoteConstant.NIGHT_MODE_CHANGED,
                            true
                        )
                        SharedPrefsUtils.setBooleanPreference(
                            this,
                            RemoteConstant.NIGHT_MODE,
                            false
                        )
                        ThemeUtils.changeToTheme(this, ThemeUtils.NORMAL)
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        switch_button_night_mode.isChecked = true
                        isNotChanged = true
                        Handler().postDelayed({
                            isNotChanged = false
                        }, 1000)
                    }
                } else {
//                stopMusic()
//                SharedPrefsUtils.setBooleanPreference(this, RemoteConstant.NIGHT_MODE_CHANGED, true)
//                SharedPrefsUtils.setBooleanPreference(this, RemoteConstant.NIGHT_MODE, true)
//                ThemeUtils.changeToTheme(this, ThemeUtils.NIGHT)

                    val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
                    val inflater: LayoutInflater = this.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val dialogDescription: TextView =
                        dialogView.findViewById(R.id.dialog_description)
                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    dialogDescription.text = "All services will restart on theme switching."

                    val b: AlertDialog = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {

                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        stopMusic()
                        SharedPrefsUtils.setBooleanPreference(
                            this,
                            RemoteConstant.NIGHT_MODE_CHANGED,
                            true
                        )
                        SharedPrefsUtils.setBooleanPreference(this, RemoteConstant.NIGHT_MODE, true)
                        ThemeUtils.changeToTheme(this, ThemeUtils.NIGHT)
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        switch_button_night_mode.isChecked = false
                        isNotChanged = true
                        Handler().postDelayed({
                            isNotChanged = false
                        }, 1000)
                    }
                }
            }
        }

        switch_button_private_profile.setOnCheckedChangeListener { view, isChecked ->
            if (!isNotChanged) {
                if (isChecked) {
                    val dialogBuilder = this.let { AlertDialog.Builder(it) }
                    val inflater = this.layoutInflater
                    val dialogView = inflater.inflate(R.layout.private_account_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)
                    val dialogDescription: TextView =
                        dialogView.findViewById(R.id.dialog_description)
                    dialogDescription.text = getString(R.string.private_profile_dialog)
                    val b: AlertDialog = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        val fullData = RemoteConstant.getEncryptedString(
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                            SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                            )!!,
                            RemoteConstant.mBaseUrl + RemoteConstant.mPrivacyOnOffData,
                            RemoteConstant.putMethod
                        )
                        val mAuth =
                            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this, RemoteConstant.mAppId,
                                ""
                            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                        viewModel.changeToPrivateAccount(mAuth)
                        observePrivacy("on")
                        account_privacy_image_view.setImageResource(
                            R.drawable.ic_account_privacy_lock
                        )
                        customProgressDialog?.show()
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        switch_button_private_profile.isChecked = false
                        isNotChanged = true
                        Handler().postDelayed({
                            isNotChanged = false
                        }, 1000)
                    }

                } else {
                    val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(it) }
                    val inflater: LayoutInflater = this.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.private_account_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)
                    val dialogDescription: TextView =
                        dialogView.findViewById(R.id.dialog_description)
                    dialogDescription.text = getString(R.string.public_profile_dialog)
                    val b: AlertDialog = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        val fullData = RemoteConstant.getEncryptedString(
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                            SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                            )!!,
                            RemoteConstant.mBaseUrl + RemoteConstant.mPrivacyOnOffData,
                            RemoteConstant.deleteMethod
                        )
                        val mAuth =
                            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this, RemoteConstant.mAppId,
                                ""
                            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                        viewModel.changeToPublicAccount(mAuth)
                        observePrivacy("off")
                        account_privacy_image_view.setImageResource(
                            R.drawable.ic_account_privacy_unlock
                        )
                        customProgressDialog?.show()
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        switch_button_private_profile.isChecked = true
                        isNotChanged = true
                        Handler().postDelayed({
                            isNotChanged = false
                        }, 1000)
                    }

                }
            }
        }



        switch_button_push_notification.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mNotificationOnOffData,
                    RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName +
                            SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                            )!! +
                            ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                if (InternetUtil.isInternetOn()) {
                    viewModel.onPushNotification(mAuth)
                    observeData("on")
                    customProgressDialog?.show()
                    push_notification_image_view.setImageResource(
                        R.drawable.ic_settings_notifications_on
                    )

                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))
                }


            } else {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mNotificationOnOffData,
                    RemoteConstant.deleteMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName +
                            SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                            )!! +
                            ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                // TODO : Check Internet connection
                if (InternetUtil.isInternetOn()) {
                    viewModel.offPushNotification(mAuth)
                    observeData("off")
                    push_notification_image_view.setImageResource(
                        R.drawable.ic_settings_notifications_off
                    )
                    customProgressDialog?.show()
                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))
                }

            }
        }

    }

    private fun stopMusic() {

        if (isMyServiceRunning(MediaServiceRadio::class.java)) {
            val serviceClass1 = MediaServiceRadio::class.java
            val intent11 = Intent(this, serviceClass1)
            stopService(intent11)
        }
        if (isMyServiceRunning(MediaService::class.java)) {
            val serviceClass1 = MediaService::class.java
            val intent11 = Intent(this, serviceClass1)
            stopService(intent11)
        }
    }


    private fun observeData(mString: String) {

        viewModel.notificationResponseModel.nonNull().observe(this, {
            if (it != null && it.payload?.success == true) {
                if (mString == "on") {
                    AppUtils.showCommonToast(this, "Push notifications enabled.")
                    SharedPrefsUtils.setBooleanPreference(
                        this,
                        RemoteConstant.mEnabledPushNotification,
                        true
                    )
                } else {
                    AppUtils.showCommonToast(this, "Push notifications disabled.")
                    SharedPrefsUtils.setBooleanPreference(
                        this,
                        RemoteConstant.mEnabledPushNotification,
                        false
                    )
                }
            }
            customProgressDialog?.dismiss()
        })

    }

    private fun observePrivacy(mString: String) {
        viewModel.privacyResponseModel.nonNull().observe(this, {
            if (it != null && it.payload!!.success == true) {
                if (mString == "on") {
                    SharedPrefsUtils.setBooleanPreference(
                        this, RemoteConstant.mEnabledPrivateProfile, true
                    )
                } else {
                    SharedPrefsUtils.setBooleanPreference(
                        this, RemoteConstant.mEnabledPrivateProfile, false
                    )

                }
            }
            customProgressDialog?.dismiss()
        })

    }

    override fun onDestroy() {
        super.onDestroy()

        if (customProgressDialog != null && customProgressDialog?.isShowing!!)
            customProgressDialog?.dismiss()


    }

    override fun onBackPressed() {
        val isChangedTheme =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE_CHANGED, false)
        if (isChangedTheme) {
            try {
                SharedPrefsUtils.setBooleanPreference(
                    this,
                    RemoteConstant.NIGHT_MODE_CHANGED,
                    false
                )
                val intent = Intent(this, RestartActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finishAffinity()
                finish()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else
            super.onBackPressed()
        AppUtils.hideKeyboard(this@SettingsActivity, settings_back_image_view)
    }


    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager: ActivityManager =
            getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

}


