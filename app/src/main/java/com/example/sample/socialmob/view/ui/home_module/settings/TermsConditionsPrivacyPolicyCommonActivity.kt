package com.example.sample.socialmob.view.ui.home_module.settings

import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import androidx.core.content.ContextCompat
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import kotlinx.android.synthetic.main.terms_conditions_privacy_policy_common_activity.*


class TermsConditionsPrivacyPolicyCommonActivity : BaseCommonActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.terms_conditions_privacy_policy_common_activity)

        // TODO : Get Bundle Value
        terms_conditions_title_text_view.text = intent.getStringExtra("mTitle")

        terms_conditions_back_image_view.setOnClickListener {
            onBackPressed()
        }
        intent.getStringExtra("mUrl")?.let { web_view.loadUrl(it) }


        web_view.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                material_progress_bar.visibility = View.GONE
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                material_progress_bar.visibility = View.GONE
            }

            override fun onReceivedHttpError(
                view: WebView, request: WebResourceRequest, errorResponse: WebResourceResponse
            ) {
                material_progress_bar.visibility = View.GONE
            }

            override fun onReceivedSslError(
                view: WebView, handler: SslErrorHandler,
                error: SslError
            ) {
                material_progress_bar.visibility = View.GONE
            }
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        if (terms_conditions_back_image_view != null) {
            AppUtils.hideKeyboard(
                this@TermsConditionsPrivacyPolicyCommonActivity,
                terms_conditions_back_image_view
            )
        }
    }
}
