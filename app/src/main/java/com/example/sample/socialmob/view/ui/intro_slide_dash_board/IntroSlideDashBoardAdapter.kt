package com.example.sample.socialmob.view.ui.intro_slide_dash_board

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.backlog.articles.ArticleDetailsActivityNew
import com.example.sample.socialmob.model.dash_board.FullScreenDashboard


class IntroSlideDashBoardAdapter(
    internal var mContext: Activity,
    private val mResources: List<FullScreenDashboard>?,
    private val requestManager: RequestManager
) :
    PagerAdapter() {
    private var mLayoutInflater: LayoutInflater =
        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return mResources!!.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = mLayoutInflater.inflate(R.layout.dashboard_item, container, false)

        val imageView = itemView.findViewById<ImageView>(R.id.imageView)
        imageView.setImageResource(R.drawable.ic_nav_menu)
        val imageUrl = mResources!![position].SourcePath
        if (!mContext.isFinishing) {
            requestManager.load(RemoteConstant.getImageUrlFromWidth(imageUrl, RemoteConstant.getWidth(mContext)/2))
                .thumbnail(0.1f)
                .into(imageView)
        }


        imageView.setOnClickListener {
            var mLink = mResources[position].Link
            if (mLink.startsWith("http")) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(mLink))
                mContext.startActivity(browserIntent)
            } else {
                var mLastIndex = mLink.substring(mLink.lastIndexOf("/") + 1)
                val intent = Intent(mContext, ArticleDetailsActivityNew::class.java)
                intent.putExtra("mId", mLastIndex)
                intent.putExtra("isNeedToCallApi", true)
                mContext.startActivity(intent)
            }
        }



        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }
}