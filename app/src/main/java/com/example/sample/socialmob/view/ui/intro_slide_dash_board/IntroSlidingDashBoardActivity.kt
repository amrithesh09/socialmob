package com.example.sample.socialmob.view.ui.intro_slide_dash_board

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.DashboardActivityBinding
import com.example.sample.socialmob.model.dash_board.FullScreenDashboard
import com.example.sample.socialmob.model.dash_board.IntroSlidingDashBoardViewModel
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.AppUtils
import kotlinx.android.synthetic.main.dashboard_activity.*


class IntroSlidingDashBoardActivity : AppCompatActivity() {
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private var binding: DashboardActivityBinding? = null
    private var introSlidingDashBoardViewModel: IntroSlidingDashBoardViewModel? = null
    private var isLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        binding = DataBindingUtil.setContentView(this, R.layout.dashboard_activity)
        introSlidingDashBoardViewModel =
            ViewModelProvider(this).get(IntroSlidingDashBoardViewModel::class.java)
        binding?.viewmodel = introSlidingDashBoardViewModel

        inItObservables()

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            checkInternet()
        } else {
            callApi()
        }

        binding!!.skipTextView.setOnClickListener {
            val intent = Intent(this@IntroSlidingDashBoardActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            overridePendingTransition(0, 0)
            this@IntroSlidingDashBoardActivity.finish()
        }

    }

    private fun checkInternet() {
        InternetUtil.observe(this, { status ->
            if (status == true) {
                if (!isLoaded) {
                    isLoaded = true
                    // TODO : IntroDashBoard API
                    callApi()
                }

            } else {
                AppUtils.showCommonToast(this, "No Internet connection")
            }
        })
    }

    private fun callApi() {
        // TODO : IntroDashBoard API
        introDashBoardApi()
    }

    private fun introDashBoardApi() {
        val mAuth = RemoteConstant.getAuthWithoutOffsetAndCount(this, RemoteConstant.mAdvertmntData)
        introSlidingDashBoardViewModel?.getDashBoardImages(mAuth)
    }

    private fun inItObservables() {
        introSlidingDashBoardViewModel?.mListImages?.nonNull()?.observe(this, {
            if (it != null && it.isNotEmpty()) {
                setAdapter(it)
            }
            introSlidingDashBoardViewModel?.mListImages?.value = null
        })
    }

    private fun setAdapter(fullScreenDashboard: List<FullScreenDashboard>?) {
        val requestManager = Glide.with(this@IntroSlidingDashBoardActivity)
        val mDashBoardAdapter =
            IntroSlideDashBoardAdapter(this, fullScreenDashboard, requestManager)
        binding!!.pager.adapter = mDashBoardAdapter
        if (pager != null) {
            dots_indicator.setViewPager(pager)
        }
    }

    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }


    override fun onDestroy() {
        super.onDestroy()
        binding!!.pager.adapter = null
        binding!!.unbind()

    }


}
