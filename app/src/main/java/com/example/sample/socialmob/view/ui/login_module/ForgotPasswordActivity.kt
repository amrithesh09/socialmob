package com.example.sample.socialmob.view.ui.login_module

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Patterns
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ForgotPasswordActivityBinding
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.AudioServiceContext
import com.example.sample.socialmob.viewmodel.login.LoginViewModel
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ForgotPasswordActivity : AppCompatActivity() {

    private var forgotPasswordActivityBinding: ForgotPasswordActivityBinding? = null
    private var mISVisible: Boolean? = null
    private val viewModel: LoginViewModel by viewModels()
    private var customProgressDialog: CustomProgressDialog? = null

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(AudioServiceContext.getContext(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        forgotPasswordActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.forgot_password_activity)

        mISVisible = false
        customProgressDialog = CustomProgressDialog(this)


        val path = "android.resource://" + packageName + "/" + R.raw.mobin
        val uri = Uri.parse(path)
        forgotPasswordActivityBinding?.videoView?.setVideoURI(uri)
        forgotPasswordActivityBinding?.videoView?.requestFocus()
        forgotPasswordActivityBinding?.videoView?.start()
        forgotPasswordActivityBinding?.videoView?.setOnPreparedListener { mp -> mp.isLooping = true }

        // TODO : Layout Animation
        Sequent.origin(forgotPasswordActivityBinding?.transitionsContainerForgotPassword).anim(this, Animation.FADE_IN_UP)
            .delay(100).offset(100).start()
        Sequent.origin(forgotPasswordActivityBinding?.transitionsContainerTwoForgotPassword).anim(this, Animation.FADE_IN)
            .delay(100).offset(100).start()

        forgotPasswordActivityBinding?.continueTextView?.setOnClickListener {
            continueForgotPassWord()
        }
        forgotPasswordActivityBinding?.forgotPasswordTextView?.setOnClickListener {
            Handler().postDelayed({
                onBackPressed()
            }, 100)
        }
    }

    /**
     * Forgot password API
     * Link sending to mail
     */
    private fun continueForgotPassWord() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            if (validEmail(forgotPasswordActivityBinding?.forgotPasswordEmailTextView?.text.toString().trim())) {
                AppUtils.hideKeyboard(this, forgotPasswordActivityBinding?.forgotPasswordTextView)
                customProgressDialog!!.show()

                viewModel.forgotPassWord(forgotPasswordActivityBinding?.forgotPasswordEmailTextView?.text.toString().trim())
                viewModel.forgotPasswordResponseModel.nonNull()
                    .observe(this, { forgotPasswordResponseModel ->
                        if (forgotPasswordResponseModel?.payload?.success == true) {
                            customProgressDialog!!.dismiss()
                            val intent = Intent(this, SentLinkInMailActivity::class.java)
                            startActivity(intent)
                            overridePendingTransition(R.anim.fadein, R.anim.fadeout)
                            finish()
                        } else {
                            customProgressDialog?.dismiss()
                            if (forgotPasswordResponseModel?.payload?.message != null) {
                                AppUtils.showCommonToast(
                                    this,
                                    forgotPasswordResponseModel.payload?.message
                                )
                                viewModel.forgotPasswordResponseModel.value = null
                            }
                        }
                    })
            } else {
                AppUtils.showCommonToast(this, "Enter a valid Email id")
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        forgotPasswordActivityBinding?.videoView?.stopPlayback()
        if (customProgressDialog != null && customProgressDialog!!.isShowing)
            customProgressDialog!!.dismiss()

    }

    override fun onResume() {
        forgotPasswordActivityBinding?.videoView?.start()
        super.onResume()
    }

    override fun onPause() {
        forgotPasswordActivityBinding?.videoView?.pause()
        super.onPause()
    }


    private fun validEmail(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }

    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.tab_color_new)
    }

    override fun onBackPressed() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }
}