package com.example.sample.socialmob.view.ui.login_module

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.profile.ProfileSocialRegisterData
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.ui.sign_up.SignUpActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.login.LoginViewModel
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.transitionseverywhere.TransitionManager
import com.transitionseverywhere.extra.Scale
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.google_login_activity.*

@AndroidEntryPoint
class GoogleSignInActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private val viewModel: LoginViewModel by viewModels()
    private var mGoogleApiClient: GoogleApiClient? = null
    private var RC_SIGN_IN = 101
    private var isMobEnter = false

    override fun onStart() {
        super.onStart()
        mGoogleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient?.isConnected!!) {
            mGoogleApiClient?.disconnect()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.google_login_activity)

        val i = intent
        isMobEnter = i.getBooleanExtra("isMobEnter", false)


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestScopes(Scope(Scopes.PLUS_ME), Scope(Scopes.PROFILE), Scope(Scopes.EMAIL))
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()


        // TODO : Layout Animation
        Sequent.origin(transitions_container_google_sign_in).anim(this, Animation.BOUNCE_IN)
            .delay(1000).offset(100)
            .start()


        invite_code_edit_text.setOnClickListener {

            showImageView(close_image_view)
            fab_next.setImageResource(R.drawable.ic_arrow_forward)
            fab_next.setBackgroundResource(R.drawable.rounded_button_invite_code)

            Sequent.origin(title_linear).anim(this, Animation.FADE_IN_DOWN).delay(1000).offset(100)
                .start()
            hideLinear(title_linear)


        }


        fab_next.setOnClickListener {
            changeStatusBarColor()
            transitions_container_google_sign_in.setBackgroundResource(R.color.splash_color)
            content_linear.setBackgroundResource(R.color.splash_color)
            hideLinear(title_linear)
            if (invite_code_edit_text.text.toString().isNotEmpty()) {
                // TODO : Check Internet connection
                if (InternetUtil.isInternetOn()) {
                    viewModel.verifyCode(invite_code_edit_text.text.toString().trim())
                    viewModel.referralResponseModel.nonNull().observe(this, {
                        if (it?.payload?.valid == true) {
                            if (isMobEnter) {

                                SharedPrefsUtils.setStringPreference(
                                    this,
                                    "PROFILE_PIC_FILE_URL_NAME",
                                    ""
                                )
                                SharedPrefsUtils.setStringPreference(
                                    this,
                                    "PROFILE_PIC_FILE_NAME",
                                    ""
                                )
                                val intent = Intent(this, SignUpActivity::class.java)
                                intent.putExtra(
                                    "referral_id",
                                    invite_code_edit_text.text.toString().trim()
                                )
                                startActivity(intent)
                                finish()
                            } else {
                                codeVerifiedSuccess()
                            }
                        } else {
                            codeVerifiedFailed()
                        }
                    })

                    fab_next.setImageResource(0)
                    progress_bar.visibility = View.VISIBLE
                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))
                }

            } else {
                AppUtils.showCommonToast(this, "Invite code cannot be empty")
            }
        }



        verifyed_image_view.setOnClickListener {
            // TODO : Disabled in V_152
            // val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }

    private fun codeVerifiedFailed() {
        AppUtils.hideKeyboard(this, next_linear)
        changeStatusBarColorCodeUnVerified()
        // TODO : Layout Animation
        next_linear.visibility = View.VISIBLE
        linear_code_verified.visibility = View.VISIBLE

        progress_bar.visibility = View.GONE
        fab_next.visibility = View.VISIBLE
        fab_next.setImageResource(R.drawable.ic_arrow_forward)
        fab_next.setBackgroundResource(R.drawable.rounded_button_invite_code)

        verifyed_image_view.setBackgroundResource(R.color.invalid_page_bg)
        transitions_container_google_sign_in.setBackgroundResource(R.color.invalid_page_bg)
        content_linear.setBackgroundResource(R.color.invalid_page_bg)
        code_verified_text_view.text = getText(R.string.invalid_code)

        Sequent.origin(linear_code_verified).anim(this, Animation.BOUNCE_IN).delay(1000).offset(100)
            .start()

        Sequent.origin(next_linear).anim(this, Animation.BOUNCE_IN).delay(1000).offset(100)
            .start()
    }

    private fun codeVerifiedSuccess() {
        val handler = Handler()
        handler.postDelayed({

            changeStatusBarColorCodeVerified()
            // TODO : Layout Animation
            next_linear.visibility = View.VISIBLE
            linear_code_verified.visibility = View.VISIBLE

            progress_bar.visibility = View.GONE
            fab_next.visibility = View.GONE
            verifyed_image_view.visibility = View.VISIBLE
            enter_invite_code_linear.visibility = View.GONE


            transitions_container_google_sign_in.setBackgroundResource(R.color.verified_page_bg)
            content_linear.setBackgroundResource(R.color.verified_page_bg)

            Sequent.origin(linear_code_verified).anim(this, Animation.BOUNCE_IN).delay(1000)
                .offset(100)
                .start()

            Sequent.origin(next_linear).anim(this, Animation.BOUNCE_IN).delay(1000).offset(100)
                .start()

            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_SIGN_IN)

        }, 1000)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                handleSignInResult(account!!)
            } catch (e: ApiException) {
                e.printStackTrace()
            }
        }
    }

    private fun handleSignInResult(account: GoogleSignInAccount) {


        try {
            val idToken = account.idToken!!
            println("" + idToken)

            if (idToken != "") {
                val mSocialData = ProfileSocialRegisterData(
                    idToken,
                    "google",
                    invite_code_edit_text.text.toString()
                )
                // TODO : Check Internet connection
                if (InternetUtil.isInternetOn()) {


                    viewModel.socialLogin(mSocialData)

                    // TODO : HANDLE LOGIN DATA
                    viewModel.userLogin?.nonNull()?.observe(this, Observer {

                        if (it?.payload?.success == true) {

                            if (it.payload != null && it.payload?.profile != null) {
                                if (it.payload?.profile?.AppId != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mAppId,
                                        it.payload?.profile?.AppId ?: ""
                                    )
                                }
                                if (it.payload?.profile?.ApiKey != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mApiKey,
                                        it.payload?.profile?.ApiKey ?: ""
                                    )
                                }
                                if (it.payload?.profile?._id != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mProfileId,
                                        it.payload?.profile?._id ?: ""
                                    )
                                }
                                if (it.payload?.profile?.PushSubscription != null) {
                                    SharedPrefsUtils.setBooleanPreference(
                                        this,
                                        RemoteConstant.mEnabledPushNotification,
                                        it.payload?.profile?.PushSubscription ?: false
                                    )
                                }
                                if (it.payload?.profile?.privateProfile != null) {
                                    SharedPrefsUtils.setBooleanPreference(
                                        this,
                                        RemoteConstant.mEnabledPrivateProfile,
                                        it.payload?.profile?.privateProfile ?: false
                                    )
                                }
                            }

                            // TODO : Disabled in V_152
                            //val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
                            val intent = Intent(this, HomeActivity::class.java)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                            this.finish()

                            SharedPrefsUtils.setStringPreference(
                                this@GoogleSignInActivity,
                                RemoteConstant.mUserImage,
                                it.payload?.profile?.pimage ?: ""
                            )
                        } else {
                            AppUtils.showCommonToast(this, it?.payload?.message)
                        }
                    })
                }
            } else {
                AppUtils.showCommonToast(this, getString(R.string.no_internet))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun hideLinear(title_linear: LinearLayout) {
        TransitionManager.beginDelayedTransition(transitions_container_google_sign_in, Scale())
        title_linear.visibility = View.GONE
    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.statusBarColor = ContextCompat.getColor(this, R.color.splash_color)
    }

    private fun changeStatusBarColorCodeVerified() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.verified_page_bg)
    }

    private fun changeStatusBarColorCodeUnVerified() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.invalid_page_bg)
    }


    private fun hideTextView(visibleTextView: TextView) {

        TransitionManager.beginDelayedTransition(transitions_container_google_sign_in, Scale())
        visibleTextView.visibility = View.GONE
    }

    private fun showImageView(visibleTextView: ImageView) {

        TransitionManager.beginDelayedTransition(transitions_container_google_sign_in, Scale())
        visibleTextView.visibility = View.VISIBLE
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainLandingActivity::class.java)
        startActivity(intent)
        finishAffinity()
        AppUtils.hideKeyboard(this@GoogleSignInActivity, transitions_container_google_sign_in)
    }

}