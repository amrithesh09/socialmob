package com.example.sample.socialmob.view.ui.login_module

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.text.method.SingleLineTransformationMethod
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LoginActivityBinding
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.AudioServiceContext
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.login.LoginViewModel
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private var BACK = 100
    private var binding: LoginActivityBinding? = null
    private val loginViewModel: LoginViewModel by viewModels()
    private var customProgressDialog: CustomProgressDialog? = null
    private var isEnabled: Boolean = false

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(AudioServiceContext.getContext(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        binding = DataBindingUtil.setContentView(this, R.layout.login_activity)

        binding?.viewmodel = loginViewModel
        customProgressDialog = CustomProgressDialog(this)
        initObservables()

        // TODO : Play background video
        val path = "android.resource://" + packageName + "/" + R.raw.mobin
        val uri = Uri.parse(path)
        binding?.videoView?.setVideoURI(uri)
        binding?.videoView?.requestFocus()
        binding?.videoView?.start()
        binding?.videoView?.setOnPreparedListener { mp -> mp.isLooping = true }

        if (SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "") != "") {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(0, 0)
            this.finish()
            finishAffinity()
        }
        binding?.forgotPasswordTextView?.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivityForResult(intent, BACK)
            overridePendingTransition(0, 0)
            finish()
        }
        binding?.eyeImageView?.setOnClickListener {
            if (!isEnabled) {
                isEnabled = true
                binding?.eyeImageView?.setImageResource(R.drawable.ic_eye_enable)
                binding?.passwordEditText?.transformationMethod = SingleLineTransformationMethod()
            } else {
                isEnabled = false
                binding?.eyeImageView?.setImageResource(R.drawable.ic_eye_disable)
                binding?.passwordEditText?.transformationMethod = PasswordTransformationMethod()
            }

            binding?.passwordEditText?.setSelection(binding?.passwordEditText?.text?.length ?: 0)
        }
        binding?.eyeImageView?.visibility = View.GONE
        loginViewModel.passwordLiveData?.nonNull()?.observe(this, {passwordLiveData->
            if (passwordLiveData != null && passwordLiveData.isNotEmpty() || binding?.passwordEditText?.hint != getString(R.string.enter_password)) {
                binding?.eyeImageView?.visibility = View.VISIBLE
            } else {
                binding?.eyeImageView?.visibility = View.GONE
            }
        })
    }

    private fun initObservables() {
        loginViewModel.progressDialog?.observe(this, {progressDialog->
            if (progressDialog!!) customProgressDialog?.show() else customProgressDialog?.dismiss()
        })

        loginViewModel.userLogin?.nonNull()?.observe(this, {loginResponse->

            if (loginResponse?.payload?.success == true) {

                if (loginResponse.payload != null && loginResponse.payload?.profile != null) {
                    if (loginResponse.payload?.profile?.AppId != null) {
                        SharedPrefsUtils.setStringPreference(
                            this,
                            RemoteConstant.mAppId,
                            loginResponse.payload?.profile?.AppId ?: ""
                        )
                    }
                    if (loginResponse.payload?.profile?.ApiKey != null) {
                        SharedPrefsUtils.setStringPreference(
                            this,
                            RemoteConstant.mApiKey,
                            loginResponse.payload?.profile?.ApiKey ?: ""
                        )
                    }
                    if (loginResponse.payload?.jwt != null) {
                        SharedPrefsUtils.setStringPreference(
                            this,
                            RemoteConstant.mJwt,
                            loginResponse.payload?.jwt ?: ""
                        )
                    }
                    if (loginResponse.payload?.profile?._id != null) {
                        SharedPrefsUtils.setStringPreference(
                            this,
                            RemoteConstant.mProfileId,
                            loginResponse.payload?.profile?._id ?: ""
                        )
                    }
                    if (loginResponse.payload?.profile?.PushSubscription != null) {
                        SharedPrefsUtils.setBooleanPreference(
                            this,
                            RemoteConstant.mEnabledPushNotification,
                            loginResponse.payload?.profile?.PushSubscription ?: false
                        )
                    }
                    if (loginResponse.payload?.profile?.privateProfile != null) {
                        SharedPrefsUtils.setBooleanPreference(
                            this,
                            RemoteConstant.mEnabledPrivateProfile,
                            loginResponse.payload?.profile?.privateProfile ?: false
                        )
                    }
                    if (loginResponse.payload?.profile?.pimage != null) {
                        SharedPrefsUtils.setStringPreference(
                            this@LoginActivity,
                            RemoteConstant.mUserImage,
                            loginResponse.payload?.profile?.pimage ?: ""
                        )
                    }
                }

                // TODO : Disabled in V_152
                //val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                if (loginResponse.payload?.profile?.pimage != null) {
                    SharedPrefsUtils.setStringPreference(
                        this@LoginActivity,
                        RemoteConstant.mUserImage, loginResponse.payload?.profile?.pimage ?: ""
                    )
                }
            } else {
                AppUtils.showCommonToast(this, loginResponse.payload?.message)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == BACK) {
            // TODO : Layout Animation
            Sequent.origin(binding?.loginPasswordLinear).anim(this, Animation.BOUNCE_IN).delay(100)
                .offset(100).start()
            Sequent.origin(binding?.fadeDownLinear).anim(this, Animation.FADE_IN_DOWN).delay(100)
                .offset(100).start()

        }
    }


    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.tab_color_new)
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainLandingActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fadein, R.anim.fadeout)
        this.finish()
        overridePendingTransition(0, 0)
    }

    override fun onResume() {
        binding?.videoView?.start()
        super.onResume()
    }

    override fun onPause() {
        binding?.videoView?.pause()
        super.onPause()
    }


    override fun onDestroy() {
        super.onDestroy()
        binding?.videoView?.stopPlayback()
        if (customProgressDialog != null && customProgressDialog?.isShowing!!)
            customProgressDialog?.dismiss()

    }


}