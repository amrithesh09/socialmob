package com.example.sample.socialmob.view.ui.login_module

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AudioServiceContext
import kotlinx.android.synthetic.main.link_sent_activity.*

class SentLinkInMailActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(AudioServiceContext.getContext(newBase))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.link_sent_activity)

        done_text_view.setOnClickListener {
            onBackPressed()
        }
        val path = "android.resource://" + packageName + "/" + R.raw.mobin
        val uri = Uri.parse(path)
        videoView.setVideoURI(uri)
//        videoView.setMediaController(MediaController(this))
        videoView.requestFocus()
        videoView.start()
        videoView.setOnPreparedListener { mp -> mp.isLooping = true }

    }

    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.tab_color_new)
    }

    override fun onBackPressed() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }
    override fun onResume() {
        videoView.start()
        super.onResume()
    }

    override fun onPause() {
        videoView.pause()
        super.onPause()
    }

    override fun onDestroy() {
        videoView.stopPlayback()
        super.onDestroy()
    }

}