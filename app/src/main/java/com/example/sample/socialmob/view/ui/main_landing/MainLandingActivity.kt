package com.example.sample.socialmob.view.ui.main_landing

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MainLandingActivityBinding
import com.example.sample.socialmob.model.profile.ProfileSocialRegisterData
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.settings.TermsConditionsPrivacyPolicyCommonActivity
import com.example.sample.socialmob.view.ui.login_module.LoginActivity
import com.example.sample.socialmob.view.ui.sign_up.SetUpScreenActivity
import com.example.sample.socialmob.view.ui.sign_up.SignUpActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.AudioServiceContext
import com.example.sample.socialmob.view.utils.CustomTypefaceSpan
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.login.LoginViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainLandingActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {
    private val viewModel: LoginViewModel by viewModels()
    private var mGoogleApiClient: GoogleApiClient? = null
    private var RC_SIGN_IN = 101
    private var customProgressDialog: CustomProgressDialog? = null
    private var mainLandingActivityBinding: MainLandingActivityBinding? = null

    override fun onStart() {
        super.onStart()
        mGoogleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient?.isConnected!!) {
            mGoogleApiClient?.disconnect()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(AudioServiceContext.getContext(newBase))
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        hideStatusBar()
        mainLandingActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.main_landing_activity)
        customProgressDialog = CustomProgressDialog(this)

        val path = "android.resource://" + packageName + "/" + R.raw.mobin
        val uri = Uri.parse(path)
        mainLandingActivityBinding?.videoView?.setVideoURI(uri)
        mainLandingActivityBinding?.videoView?.requestFocus()
        mainLandingActivityBinding?.videoView?.start()
        mainLandingActivityBinding?.videoView?.setOnPreparedListener { mp -> mp.isLooping = true }


        //TODO :Click in Terms and condition
        val spannableString = SpannableString(
            "By registering you agree to the Terms of Use and Privacy Policy"
        )
        val termsAndCondition: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                val intent = Intent(
                    this@MainLandingActivity,
                    TermsConditionsPrivacyPolicyCommonActivity::class.java
                )
                intent.putExtra("mTitle", getString(R.string.terms_conditions_))
                intent.putExtra("mUrl", "https://socialmob.me/termsofuse.html")
                startActivity(intent)
            }
        }

        // Character starting from 32 - 45 is Terms and condition.
        // Character starting from 49 - 63 is privacy policy.
        val privacy: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                val intent = Intent(
                    this@MainLandingActivity,
                    TermsConditionsPrivacyPolicyCommonActivity::class.java
                )
                intent.putExtra("mTitle", getString(R.string.privacy_policy_))
                intent.putExtra("mUrl", "https://socialmob.me/privacypolicy.html")
                startActivity(intent)
            }
        }
        val font: Typeface =
            Typeface.createFromAsset(assets, "fonts/poppinsbold.otf")


        spannableString.setSpan(termsAndCondition, 32, 45, 0)
        spannableString.setSpan(privacy, 49, 63, 0)
        spannableString.setSpan(ForegroundColorSpan(Color.WHITE), 32, 45, 0)
        spannableString.setSpan(ForegroundColorSpan(Color.WHITE), 49, 63, 0)
        spannableString.setSpan(UnderlineSpan(), 32, 45, 0)
        spannableString.setSpan(UnderlineSpan(), 49, 63, 0)

        spannableString.setSpan(
            CustomTypefaceSpan("", font),
            32,
            45,
            Spanned.SPAN_EXCLUSIVE_INCLUSIVE
        )
        spannableString.setSpan(
            CustomTypefaceSpan("", font),
            49,
            63,
            Spanned.SPAN_EXCLUSIVE_INCLUSIVE
        )


        mainLandingActivityBinding?.termsTextView?.movementMethod = LinkMovementMethod.getInstance()
        mainLandingActivityBinding?.termsTextView?.setText(
            spannableString,
            TextView.BufferType.SPANNABLE
        )
        mainLandingActivityBinding?.termsTextView?.isSelected = true


        /**
         * This section is disable in version 152 ( IntroScreen dashboard )
         */
        if (SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "") != "") {
            if (SharedPrefsUtils.getBooleanPreference(this, "NEED_TO_SHOW_DASH_BOARD", false)) {
                // TODO : Disabled in V_152
                //val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                this.finish()
                finishAffinity()
            } else {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                this.finish()
                finishAffinity()
            }
        }


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.clientID))
            .requestScopes(Scope(Scopes.PLUS_ME), Scope(Scopes.PROFILE), Scope(Scopes.EMAIL))
            .requestEmail()
            .build()

        mGoogleApiClient =
            GoogleApiClient.Builder(this).enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        // TODO : If check signed in ( when logout - User need to show all the mail id's )
        signOut()

        mainLandingActivityBinding?.mobInLandingPageTextView?.setOnClickListener {
            // TODO : Need to check terms and condition is checked
            if (mainLandingActivityBinding?.termsCheck?.isChecked!!) {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fadein, R.anim.fadeout)
                this.finish()
            } else {
                AppUtils.showCommonToast(this, getString(R.string.agree_terms))
            }
        }

        mainLandingActivityBinding?.googleTextView?.setOnClickListener {
            // TODO : Need to check terms and condition is checked
            if (mainLandingActivityBinding?.termsCheck?.isChecked!!) {
                // TODO : Check Internet connection
                if (InternetUtil.isInternetOn()) {
                    customProgressDialog!!.show()
                    val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
                    startActivityForResult(signInIntent, RC_SIGN_IN)
                    mainLandingActivityBinding?.mainContentRelative?.isClickable = false
                } else {
                    AppUtils.showCommonToast(this, "No internet connection")
                }
            } else {
                AppUtils.showCommonToast(this, getString(R.string.agree_terms))
            }

        }
        mainLandingActivityBinding?.becomeAnMobberTextView?.setOnClickListener {
            // TODO : Need to check terms and condition is checked
            if (mainLandingActivityBinding?.termsCheck?.isChecked!!) {
                // TODO : Saving profile pic url saving in Shared preference ( Need to clear when user swipe closed )
                SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_URL_NAME", "")
                SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_NAME", "")
                val intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
                this.finish()
            } else {
                AppUtils.showCommonToast(this, getString(R.string.agree_terms))
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                handleSignInResult(account!!)
            } catch (e: ApiException) {
//                AppUtils.showCommonToast(this, "Error occurred please try again later")
                e.printStackTrace()
                mainLandingActivityBinding?.mainContentRelative?.isClickable = true
                customProgressDialog!!.dismiss()
            }
        }
    }

    /**
     * Handle google login
     */

    private fun handleSignInResult(account: GoogleSignInAccount) {
        try {
            val idToken = account.idToken!!

            if (idToken != "") {
                val mReferralId =
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.referrerUid, "")!!
                val mSocialData = ProfileSocialRegisterData(idToken, "google", mReferralId)
                // TODO : Check Internet connection
                if (InternetUtil.isInternetOn()) {
                    viewModel.socialLogin(mSocialData)

                    viewModel.isError.nonNull().observe(this, { isError ->
                        if (!isError!!) {
                            mainLandingActivityBinding?.mainContentRelative?.isClickable = true
                            customProgressDialog!!.dismiss()
                            AppUtils.showCommonToast(this, getString(R.string.empty_screen))
                            viewModel.isError.value = null
                        }
                    })

                    // TODO : HANDLE LOGIN DATA
                    viewModel.userLogin?.nonNull()?.observe(this, {
                        if (!viewModel.isLoadingSocial) {
                            SharedPrefsUtils.setStringPreference(
                                this,
                                RemoteConstant.referrerUid,
                                ""
                            )

                            if (it?.payload?.success == true) {

                                if (it.payload?.profile?.AppId != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mAppId,
                                        it.payload?.profile?.AppId!!
                                    )
                                }
                                if (it.payload?.profile?.ApiKey != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mApiKey,
                                        it.payload?.profile?.ApiKey!!
                                    )
                                }
                                if (it.payload?.profile?._id != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mProfileId,
                                        it.payload?.profile?._id!!
                                    )
                                }
                                if (it.payload?.profile?.PushSubscription != null) {
                                    SharedPrefsUtils.setBooleanPreference(
                                        this,
                                        RemoteConstant.mEnabledPushNotification,
                                        it.payload?.profile?.PushSubscription!!
                                    )
                                }
                                if (it.payload?.profile?.privateProfile != null) {
                                    SharedPrefsUtils.setBooleanPreference(
                                        this,
                                        RemoteConstant.mEnabledPrivateProfile,
                                        it.payload?.profile?.privateProfile!!
                                    )
                                }

                                if (it.payload?.profile?.pimage != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this@MainLandingActivity,
                                        RemoteConstant.mUserImage,
                                        it.payload?.profile?.pimage ?: ""
                                    )
                                }

                                if (it.payload?.jwt != null) {
                                    SharedPrefsUtils.setStringPreference(
                                        this,
                                        RemoteConstant.mJwt,
                                        it.payload?.jwt!!
                                    )
                                }
                                customProgressDialog?.dismiss()
                                mainLandingActivityBinding?.mainContentRelative?.isClickable = true

                                // TODO : If user is new we need to display setup screen with intro
                                if (it.payload?.newUser == true) {
                                    val intent = Intent(this, SetUpScreenActivity::class.java)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finishAffinity()
                                } else {
                                    // TODO : Disabled in V_152
                                    // val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
                                    val intent = Intent(this, HomeActivity::class.java)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                                }
                            } else {
                                mainLandingActivityBinding?.mainContentRelative?.isClickable = true
                                customProgressDialog!!.dismiss()
                                signOut()
                                AppUtils.showCommonToast(this, it?.payload?.message)
                            }
                        }
                    })
                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            customProgressDialog?.dismiss()
        }
    }

    private fun signOut() {
        if (mGoogleApiClient?.isConnected!!) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
            mGoogleApiClient?.disconnect()
            mGoogleApiClient?.connect()
        }
    }

    /**
     * Change status bar to black
     *
     */
    private fun hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

    override fun onDestroy() {
        super.onDestroy()
        mainLandingActivityBinding?.videoView?.stopPlayback()
        if (customProgressDialog != null && customProgressDialog!!.isShowing)
            customProgressDialog!!.dismiss()

    }

    override fun onResume() {
        mainLandingActivityBinding?.videoView?.start()
        super.onResume()
    }

    override fun onPause() {
        mainLandingActivityBinding?.videoView?.pause()
        super.onPause()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

}
