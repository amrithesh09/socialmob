package com.example.sample.socialmob.view.ui.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.AnthemListItemBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import kotlinx.android.synthetic.main.anthem_list_item.view.*


class AnthemAdapter(
    private var mItems: MutableList<TrackListCommon>?,
    private var anthemAdapterItemClick: AnthemAdapterItemClick
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AnthemAdapter.ViewHolder>() {
    private var mSelectedItem = -1

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.binding.trackModel = mItems!![position]
        viewHolder.binding.executePendingBindings()

        // TODO : Single selection radio button
        viewHolder.binding.anthemRadioButton.isChecked = position == mSelectedItem

        val clickListener = View.OnClickListener {
            anthemAdapterItemClick.onAnthemAdapterItemClick(
                mItems?.get(position)?.id!!,
                mItems?.get(position)?.trackName!!
            )
            mSelectedItem = viewHolder.adapterPosition
            notifyDataSetChanged()
        }
        viewHolder.itemView.setOnClickListener(clickListener)
        viewHolder.binding.root.anthem_radio_button.setOnClickListener(clickListener)
    }

    override fun getItemCount(): Int {
        return mItems!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): ViewHolder {
        val binding: AnthemListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.anthem_list_item, parent, false
        )
        return ViewHolder(binding)
    }

    fun addAll(singlePlayListTrack: MutableList<TrackListCommon>) {
        if (mItems != null) {
            val size = mItems?.size
            mItems?.addAll(singlePlayListTrack)// add new data
            notifyItemChanged(size!!, mItems?.size)
        }
    }

    class ViewHolder(val binding: AnthemListItemBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
//
    }

    interface AnthemAdapterItemClick {
        fun onAnthemAdapterItemClick(mId: String, mTrack: String)
    }
}
