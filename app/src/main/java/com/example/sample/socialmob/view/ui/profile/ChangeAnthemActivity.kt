package com.example.sample.socialmob.view.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ChangeAnthemActivityBinding
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.change_anthem_activity.*
import javax.inject.Inject

@AndroidEntryPoint
class ChangeAnthemActivity : BaseCommonActivity(), AnthemAdapter.AnthemAdapterItemClick {
    private val profileViewModel: ProfileViewModel by viewModels()
    private var mProfileAnthemTrackId: String = ""
    private var mTrack: String = ""
    private var binding: ChangeAnthemActivityBinding? = null
    private var singlePlayListTrack: MutableList<TrackListCommon>? = ArrayList()
    private var mStatus: ResultResponse.Status? = null
    private var mAdapter: AnthemAdapter? = null
    private var isAddedLoader: Boolean = false
    @Inject
    lateinit var glideRequestManager: RequestManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.change_anthem_activity)

        val i: Intent? = intent
        if (i?.getStringExtra("mProfileAnthemTrackId") != null) {
            mProfileAnthemTrackId = i.getStringExtra("mProfileAnthemTrackId")!!
        }


        save_changes_text_view.setOnClickListener {
            if (mProfileAnthemTrackId != "") {
                saveChanges()
            } else
                AppUtils.showCommonToast(this, "Select your anthem")
        }
        refresh_text_view.setOnClickListener {
            observeData()
        }
        binding?.informationImageview?.setOnClickListener {
            addInformationDialog()
        }

        callApi(RemoteConstant.mOffset, RemoteConstant.mCount)
        observeData()

        mAdapter = AnthemAdapter(singlePlayListTrack, this)
        change_anthem_recycler_view.adapter = mAdapter


    }

    private fun callApi(mOffset: String, mCount: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.trackData +
                    RemoteConstant.pathFavoriteList + "?offset=" + mOffset + "&count=" + mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        profileViewModel.getFavouriteTracks(
            mAuth, mOffset, mCount
        )

    }

    private fun addInformationDialog() {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.anthem_info_information_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton = dialogView.findViewById(R.id.close_button_information) as TextView

        val b: AlertDialog = dialogBuilder.create()
        b.show()

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    private fun saveChanges() {
        onBackPressed()
    }

    private fun observeData() {

        profileViewModel.recentPlayedTracks?.nonNull()?.observe(this, { trackList ->
            mStatus = trackList.status

            when (trackList.status) {
                ResultResponse.Status.SUCCESS -> {
                    singlePlayListTrack = trackList?.data!! as MutableList<TrackListCommon>

                    mAdapter?.addAll(singlePlayListTrack!!)

                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                    binding?.changeAnthemRecyclerView?.visibility = View.VISIBLE

                    mStatus = ResultResponse.Status.LOADING_COMPLETED

                    if (singlePlayListTrack?.size == 0) {
                        binding?.saveChangesTextView?.visibility = View.GONE
                    } else {
                        binding?.saveChangesTextView?.visibility = View.VISIBLE
                    }

                }
                ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                    if (isAddedLoader) {
                        //                        mAdapter?.removeLoader()
                        mAdapter?.notifyDataSetChanged()
                        isAddedLoader = false
                    }
                }
                ResultResponse.Status.PAGINATED_LIST -> {
                    if (isAddedLoader) {
                        mAdapter?.notifyDataSetChanged()
                        isAddedLoader = false
                    }
                    singlePlayListTrack?.addAll(trackList?.data!!)
                    mAdapter?.addAll(trackList?.data!! as MutableList<TrackListCommon>)
                    mStatus = ResultResponse.Status.LOADING_COMPLETED
                }
                ResultResponse.Status.ERROR -> {
                    if (singlePlayListTrack?.size == 0) {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.VISIBLE
                        binding?.changeAnthemRecyclerView?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        glideRequestManager.load(R.drawable.server_error).apply(
                            RequestOptions().fitCenter())
                            .into(binding?.noInternetImageView!!)
                        binding?.noInternetTextView?.text = getString(R.string.server_error)
                        binding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)
                    }
                }
                ResultResponse.Status.NO_DATA -> {
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.changeAnthemRecyclerView?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.VISIBLE
                }
                ResultResponse.Status.LOADING -> {
                    binding?.progressLinear?.visibility = View.VISIBLE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.changeAnthemRecyclerView?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                }
                else -> {

                }
            }
        })
    }


    override fun onAnthemAdapterItemClick(mId: String, mTrackName: String) {
        mProfileAnthemTrackId = mId
        mTrack = mTrackName
    }

    fun backClickChangeAnthem(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("mProfileAnthemTrackId", mProfileAnthemTrackId)
        intent.putExtra("mTrack", mTrack)
        setResult(1301, intent)
        super.onBackPressed()
        AppUtils.hideKeyboard(this@ChangeAnthemActivity, change_anthem_back_image_view)
    }

}