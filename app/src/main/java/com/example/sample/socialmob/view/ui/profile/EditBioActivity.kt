package com.example.sample.socialmob.view.ui.profile

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.EditBioActivtyBinding
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity

class EditBioActivity : BaseCommonActivity() {
    private var editBioActivityBinding: EditBioActivtyBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        editBioActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.edit_bio_activty)

        val i: Intent? = intent
        val bio: String = i?.getStringExtra("bio")!!
        editBioActivityBinding?.editBioEditText?.setText(bio)

        editBioActivityBinding?.editBioEditText?.post {
            editBioActivityBinding?.editBioEditText?.requestFocus()
            editBioActivityBinding?.editBioEditText?.setSelection(editBioActivityBinding?.editBioEditText?.text?.length!!)
        }
        //TODO : To display keyboard
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            AppUtils.showKeyboard(this, editBioActivityBinding?.editBioEditText)
        }, 1000)

        editBioActivityBinding?.saveButton?.setOnClickListener {
            if (editBioActivityBinding?.editBioEditText?.text.toString().isNotEmpty()) {
                AppUtils.hideKeyboard(this, editBioActivityBinding?.saveButton!!)
                val mIntent = Intent()
                mIntent.putExtra("bio", editBioActivityBinding?.editBioEditText?.text.toString())
                setResult(2001, mIntent)
                finish()
            } else {
                AppUtils.showCommonToast(this, "Bio cannot be empty.")
            }

        }
        editBioActivityBinding?.backImageView?.setOnClickListener {
            AppUtils.hideKeyboard(this, editBioActivityBinding?.editBioEditText!!)
            finish()
        }

    }
}
