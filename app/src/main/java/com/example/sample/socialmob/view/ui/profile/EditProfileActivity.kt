package com.example.sample.socialmob.view.ui.profile

import `in`.madapps.placesautocomplete.PlaceAPI
import `in`.madapps.placesautocomplete.adapter.PlacesAutoCompleteAdapter
import `in`.madapps.placesautocomplete.listener.OnPlacesDetailsListener
import `in`.madapps.placesautocomplete.model.Place
import `in`.madapps.placesautocomplete.model.PlaceDetails
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.*
import android.transition.Transition
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.EditProfileActivityBinding
import com.example.sample.socialmob.model.profile.EditProfileBody
import com.example.sample.socialmob.model.profile.LocationCoordinates
import com.example.sample.socialmob.model.profile.UserNameAvailableResponse
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.edit_profile_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.textColor
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class EditProfileActivity : BaseCommonActivity() {
    private var APIkey = "AIzaSyAm7nCFOD1tp_qSBwIPwi9WE4LKx12N71M"

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: EditProfileActivityBinding? = null
    private val profileViewModel: ProfileViewModel by viewModels()
    private var isChanged: Boolean = false
    private var mLocation: String = ""
    private var mName: String = ""
    private var mUserName: String = ""
    private var mUserBio: String = ""
    private var Location: String = ""
    private var jukeBoxCategoryId: String = ""
    private var mThemeCover: String = ""
    private var mProfileAnthemTrackId: String = ""
    private var mTrack: String = ""
    private var customProgressDialog: CustomProgressDialog? = null
    private val placesApi = PlaceAPI()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.edit_profile_activity)
        customProgressDialog = CustomProgressDialog(this)

        observeData()
        initTransition()
        // TODO : get profile details API
        getProfileDetails(SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, ""))


        Handler(Looper.getMainLooper()).post {
            edit_profile_camera_image_view.bringToFront()
            location_auto_complete.clearFocus()
            if (location_auto_complete.isPopupShowing) {
                location_auto_complete.dismissDropDown()
            }
            AppUtils.hideKeyboard(this, location_auto_complete)
        }
        placesApi.initialize(APIkey, this@EditProfileActivity)
        location_auto_complete.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                AppUtils.hideKeyboard(this, location_auto_complete)
            }
            true
        }

        location_auto_complete.setAdapter(PlacesAutoCompleteAdapter(this, placesApi))

        location_auto_complete.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                try {
                    val place = parent.getItemAtPosition(position) as Place

                    val mPlace = place.description
                    val mPlaceList = mPlace.split(",")
                    location_auto_complete.setText(mPlaceList[0])
                    getPlaceDetails(place.id)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        binding?.editBio?.setOnClickListener {
            val mIntent = Intent(this, EditBioActivity::class.java)
            mIntent.putExtra("bio", mUserBio)
            startActivityForResult(mIntent, 2001)
        }

        binding?.editUserNameImageView?.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(this) }
            val inflater: LayoutInflater = this.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.user_name_dialog, viewGroup)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
            val userNameEditText: EditText = dialogView.findViewById(R.id.user_name_edit_text)
            val userNameAvailableTextView: TextView =
                dialogView.findViewById(R.id.user_name_available_text_view)
            val checkTextView: TextView =
                dialogView.findViewById(R.id.check_text_view)

            userNameEditText.setText(mUserName.trim())
            userNameEditText.setSelection(userNameEditText.text.toString().trim().length)
            userNameEditText.post {
                userNameEditText.requestFocus()
                AppUtils.showKeyboard(this, userNameEditText)
            }
            val b: AlertDialog = dialogBuilder.create()
            b.show()


            closeButton.setOnClickListener {
                AppUtils.hideKeyboard(this, closeButton)
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }

            checkTextView.setOnClickListener {
                // TODO : Check user name is available API
                if (userNameEditText.text.isNotEmpty()) {
                    mUserName = userNameEditText.text.trim().toString()
                    customProgressDialog?.show()
                    val mUSerName = userNameEditText.text.toString()
                    val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(
                            application,
                            RemoteConstant.mAppId,
                            ""
                        )!!,
                        SharedPrefsUtils.getStringPreference(
                            application,
                            RemoteConstant.mApiKey,
                            ""
                        )!!,
                        RemoteConstant.mBaseUrl + "/api/v2/availability/username/" + mUSerName,
                        RemoteConstant.mGetMethod
                    )
                    val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        application,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                    GlobalScope.launch(Dispatchers.IO) {
                        var response: Response<UserNameAvailableResponse>? = null
                        kotlin.runCatching {
                            response = WebServiceClient.client.create(BackEndApi::class.java)
                                .checkUserNameAvailable(mAuth, mUSerName)
                        }.onSuccess {
                            when (response?.code()) {
                                200 -> {
                                    GlobalScope.launch(Dispatchers.Main) {
                                        if (response?.body()?.payload?.available!!) {
                                            binding?.userNameEditText?.text = mUSerName
                                            isChanged = true
                                            AppUtils.showCommonToast(
                                                this@EditProfileActivity,
                                                "User name is available"
                                            )
                                            val handler = Handler(Looper.getMainLooper())
                                            handler.postDelayed({
                                                AppUtils.hideKeyboard(
                                                    this@EditProfileActivity,
                                                    edit_profile_back_image_view
                                                )
                                            }, 1000)
                                            b.getButton(AlertDialog.BUTTON_NEGATIVE)
                                                ?.performClick()

                                        } else {
                                            userNameAvailableTextView.text =
                                                "This username is already taken."
                                            userNameAvailableTextView.textColor =
                                                Color.parseColor("#c44569")
//                                            )
                                        }
                                    }
                                }
                                else -> {
                                    RemoteConstant.apiErrorDetails(
                                        application, response?.code()!!, "Check Username"
                                    )
                                }
                            }
                            dismissCustomDialog()
                        }.onFailure {
                            AppUtils.showCommonToast(
                                this@EditProfileActivity,
                                "Error occur please try again later"
                            )
                            dismissCustomDialog()
                            runOnUiThread(
                                Runnable {
                                    b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                                })
                        }
                    }
                } else {
                    AppUtils.showCommonToast(this, getString(R.string.user_name_cannot_be_empty))
                }
            }
        }
        binding?.editProfileCameraImageView?.setOnClickListener {
            onBackPressed()
        }
        binding?.editProfileCameraImageView?.setOnClickListener {
            imagePickerAlert()
        }
        binding?.saveButton?.setOnClickListener {
            saveChanges()
        }
        binding?.editAnthemImageView?.setOnClickListener {
            editAnthem()
        }
        binding?.editLocation?.setOnClickListener {
            binding?.locationAutoComplete?.isEnabled = true
            binding?.locationAutoComplete?.isFocusable = true
            binding?.locationAutoComplete?.post {
                binding?.editLocation?.visibility = View.GONE
                binding?.locationAutoComplete?.requestFocus()
                binding?.locationAutoComplete?.setSelection(binding?.locationAutoComplete?.text?.length!!)
                AppUtils.showKeyboard(this, binding?.locationAutoComplete)
            }
        }

        // TODO : Observe data
        profileViewModel.myProfileData.nonNull().observe(this, {
            binding?.profileData = it?.profile
            binding?.profileTrack = it

            if (it.themeGenre?.jukeBoxCategoryId != null) {
                jukeBoxCategoryId = it.themeGenre.jukeBoxCategoryId.toString()
            }
            if (it.themeGenre?.themeCover != null) {
                mThemeCover = it.themeGenre.themeCover
            }
            binding?.viewModel = profileViewModel
            binding?.executePendingBindings()
        })

        // TODO : Initially dismiss drop down
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            location_auto_complete.dismissDropDown()
        }, 300)
    }

    private fun getProfileDetails(mProfileId: String?) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthUser(this@EditProfileActivity, mProfileId!!)
            profileViewModel.getMyProfile(mAuth, mProfileId)
        }


    }

//    fun changeThemeClick(view: View) {
//        val detailIntent = Intent(this, ChangeThemeActivity::class.java)
//        val buttonPair =
//            Pair.create<View, String>(change_theme_button, getString(R.string.theme_transition))
//        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, buttonPair)
//        detailIntent.putExtra("jukeBoxCategoryId", jukeBoxCategoryId)
//        detailIntent.putExtra("mThemeCover", mThemeCover)
//        startActivityForResult(detailIntent, 1401, options.toBundle())
//    }

    private fun initTransition() {
        //add transition listener to load full-size image on transition end
        window.sharedElementEnterTransition.addListener(object : Transition.TransitionListener {
            override fun onTransitionEnd(transition: Transition?) {
            }

            override fun onTransitionResume(transition: Transition?) {
            }

            override fun onTransitionPause(transition: Transition?) {
            }

            override fun onTransitionCancel(transition: Transition?) {
            }

            override fun onTransitionStart(transition: Transition?) {
            }
        })
    }

    private fun editAnthem() {
        val intent = Intent(this, ChangeAnthemActivity::class.java)
        intent.putExtra("mProfileAnthemTrackId", mProfileAnthemTrackId)
        startActivityForResult(intent, 1301)
    }

    private fun observeData() {
        profileViewModel.profile.nonNull().observe(this, {
            binding?.profileData = it
            if (it?.Location != null) {
                mLocation = it.Location
            }
            if (it.Name != null) {
                mName = it.Name!!
            }
            if (it?.username != null) {
                mUserName = it.username!!
            }
            if (it?.About != null) {
                mUserBio = it.About
            }
            if (it?.Location != null) {
                Location = it.Location
            }

            if (it.profileAnthemTrackId != null) {
                mProfileAnthemTrackId = it.profileAnthemTrackId
            }
            binding?.viewModel = profileViewModel
            binding?.executePendingBindings()
        })
    }


    private fun imagePickerAlert() {
        val dialogBuilder = this.let { AlertDialog.Builder(it) }
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_profile_image_picker_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val cancelTextView: TextView = dialogView.findViewById(R.id.cancel_button)
        val linearCamera: LinearLayout = dialogView.findViewById(R.id.linear_camera)
        val linearGallery: LinearLayout = dialogView.findViewById(R.id.linear_gallery)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        cancelTextView.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        linearGallery.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            val intent = Intent(this, PickerActivity::class.java)
            intent.putExtra("title", "UPLOAD PHOTO")
            intent.putExtra("IMAGES_LIMIT", 1)
            intent.putExtra("VIDEOS_LIMIT", 1)
            intent.putExtra("IS_FROM_CREATE_PROFILE", true)
            intent.putExtra("SELECT_POSITION", 1)
            SharedPrefsUtils.setIntegerPreference(this, "ITEM_SELECT", 1)
            startActivity(intent)
        }
        linearCamera.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            val intent = Intent(this, PickerActivity::class.java)
            intent.putExtra("title", "UPLOAD PHOTO")
            intent.putExtra("IMAGES_LIMIT", 1)
            intent.putExtra("VIDEOS_LIMIT", 1)
            intent.putExtra("IS_FROM_CREATE_PROFILE", true)
            intent.putExtra("SELECT_POSITION", 0)
            SharedPrefsUtils.setIntegerPreference(this, "ITEM_SELECT", 1)
            startActivity(intent)
        }
    }

    private fun delete() {
        val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
        if (dir.isDirectory) {
            val children = dir.list()
            if (children != null && children.isNotEmpty()) {
                for (i in children.indices) {
                    File(dir, children[i]).delete()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (Location != binding?.locationAutoComplete?.text.toString()) {
            isChanged = true
        }
        if (Location != binding?.locationAutoComplete?.text.toString()) {
            isChanged = true
        }

        if (isChanged) {
            val dialogBuilder = this.let { AlertDialog.Builder(it) }
            val inflater = this.layoutInflater
            val dialogView = inflater.inflate(R.layout.logout_dialog, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val dialogDescription =
                dialogView.findViewById<View>(R.id.dialog_description) as TextView
            val noButton = dialogView.findViewById<View>(R.id.logout_cancel_button) as Button
            val yesButton = dialogView.findViewById<View>(R.id.log_out_button) as Button

            dialogDescription.text = getString(R.string.do_you_want_to_save)
            noButton.text = getString(android.R.string.no)
            yesButton.text = getString(android.R.string.yes)

            val b = dialogBuilder.create()
            b.show()
            yesButton.setOnClickListener {
                saveChanges()
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            }
            noButton.setOnClickListener {
                delete()
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                super.onBackPressed()
                AppUtils.hideKeyboard(this@EditProfileActivity, edit_profile_back_image_view)
            }
        } else {
            // TODO: Delete if folder exists
            delete()
            super.onBackPressed()
            AppUtils.hideKeyboard(this@EditProfileActivity, edit_profile_back_image_view)
        }
    }

    /**
     *  Update profile API
     */
    private fun saveChanges() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            if (location_auto_complete.text.toString().isNotEmpty()) {
                if (user_name_edit_text.text.toString().isNotEmpty()) {
                    if (edi_profile_name_edit_text.text.toString().isNotEmpty()) {
                        if (user_name_edit_text.text.length > 2) {

                            if (edi_profile_name_edit_text.text.length > 3) {
                                customProgressDialog?.show()

                                if (isChanged) {
                                    edit_profile_image_view.visibility = View.INVISIBLE
                                }

                                val fullData = RemoteConstant.getEncryptedString(
                                    SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mAppId,
                                        ""
                                    )!!,
                                    SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mApiKey,
                                        ""
                                    )!!,
                                    RemoteConstant.mBaseUrl + RemoteConstant.updateProfileData,
                                    RemoteConstant.postMethod
                                )
                                val mAuth =
                                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                        this, RemoteConstant.mAppId,
                                        ""
                                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                                var mProfileImage: String? = null
                                if (SharedPrefsUtils.getStringPreference(
                                        this,
                                        "PROFILE_PIC_FILE_URL_NAME",
                                        ""
                                    ) != ""
                                ) {
                                    mProfileImage =
                                        SharedPrefsUtils.getStringPreference(
                                            this, "PROFILE_PIC_FILE_URL_NAME", ""
                                        )!!
                                }

                                val locationCoordinates =
                                    LocationCoordinates(
                                        SharedPrefsUtils.getStringPreference(
                                            this,
                                            RemoteConstant.LATITUDE,
                                            ""
                                        ),
                                        SharedPrefsUtils.getStringPreference(
                                            this,
                                            RemoteConstant.LONGITUDE,
                                            ""
                                        )
                                    )
                                val editProfileBody =
                                    EditProfileBody(
                                        edi_profile_name_edit_text.text.toString(),
                                        location_auto_complete.text.toString().trim(),
                                        mProfileImage,
                                        mProfileAnthemTrackId,
                                        jukeBoxCategoryId,
                                        mUserName,
                                        mUserBio,
                                        locationCoordinates
                                    )

                                // TODO : Update name in DB

                                profileViewModel.updateProfile(mAuth, editProfileBody)

                                val observerGetProfileThrowable =
                                    profileViewModel.getProfileThrowable()

                                observerGetProfileThrowable.nonNull().observe(this, {
                                    if (it != null) {
                                        AppUtils.showCommonToast(this, it.message)
                                        dismissCustomDialog()
                                    }
                                    if (profileViewModel != null && profileViewModel.getProfileThrowable()
                                            .hasObservers()
                                    ) {
                                        profileViewModel.getProfileThrowable()
                                            .removeObservers(this)
                                    }
                                })

                                val observeGetEditProfileResponse =
                                    profileViewModel.getEditProfileResponse()
                                observeGetEditProfileResponse.nonNull().observe(this, {
                                    if (it != null && it.payload?.success == true) {
                                        dismissCustomDialog()
                                        isChanged = false
                                        getProfileDetails(
                                            SharedPrefsUtils.getStringPreference(
                                                this,
                                                RemoteConstant.mProfileId,
                                                ""
                                            )
                                        )
                                        binding?.locationAutoComplete?.dismissDropDown()
                                        binding?.locationAutoComplete?.isFocusable = false
                                        binding?.locationAutoComplete?.isEnabled = false
                                        binding?.locationAutoComplete?.post {
                                            binding?.editLocation?.visibility = View.VISIBLE
                                            binding?.locationAutoComplete?.clearFocus()
                                        }
                                        // TODO: Delete if folder exists
                                        delete()
                                        finish()
                                    } else {
                                        dismissCustomDialog()
                                    }
                                    //                                    if (profileViewModel != null && profileViewModel?.getEditProfileResponse()?.hasObservers()!!) {
                                    //                                        profileViewModel?.getEditProfileResponse()
                                    //                                            ?.removeObservers(this)
                                    //                                    }
                                    edit_profile_image_view.visibility = View.VISIBLE
                                })

                            } else {
                                AppUtils.showCommonToast(this, "Name is too short")
                            }

                        } else {
                            AppUtils.showCommonToast(this, "User name is too short")
                        }

                    } else {
                        AppUtils.showCommonToast(this, "Please provide name")
                    }
                } else {
                    AppUtils.showCommonToast(this, "Please provide user name")
                }
            } else {
                AppUtils.showCommonToast(this, "Please provide your Location")
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    private fun dismissCustomDialog() {
        if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
            customProgressDialog?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        // TODO : Load Image ( TO Check If is updated)
        if (SharedPrefsUtils.getStringPreference(this, "PROFILE_PIC_FILE_NAME", "") != "") {
            val mProfileImage =
                SharedPrefsUtils.getStringPreference(this, "PROFILE_PIC_FILE_NAME", "")
            isChanged = true
            glideRequestManager.load(mProfileImage).into(edit_profile_image_view)
        }
        AppUtils.hideKeyboard(this@EditProfileActivity, edit_profile_image_view)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1401) {
            if (data != null) {
                jukeBoxCategoryId = data.getStringExtra("jukeBoxCategoryId")!!
                mThemeCover = data.getStringExtra("mThemeCover")!!
                glideRequestManager.load(mThemeCover).into(theme_image_view)
                isChanged = true
            }
        }
        if (resultCode == 1301) {
            if (data != null) {
                mProfileAnthemTrackId = data.getStringExtra("mProfileAnthemTrackId")!!
                mTrack = data.getStringExtra("mTrack")!!
                if (mTrack != "") {
                    profile_track_text_view.text = mTrack
                }
                isChanged = true
            }
        }
        if (resultCode == 2001) {
            if (data != null) {
                mUserBio = data.getStringExtra("bio")!!
                val mOldBio = binding?.bioTextView?.text
                binding?.bioTextView?.text = mUserBio
                if (mOldBio != mUserBio) {
                    isChanged = true
                }
            }
        }
    }

    private fun getPlaceDetails(placeId: String) {
        placesApi.fetchPlaceDetails(placeId, object :
            OnPlacesDetailsListener {
            override fun onError(errorMessage: String) {
                println("" + errorMessage)
            }

            override fun onPlaceDetailsFetched(placeDetails: PlaceDetails) {
                try {
                    setupUI(placeDetails)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun setupUI(placeDetails: PlaceDetails) {
        try {
            var street = ""
            var city = ""
            var state = ""
            var country = ""
            var zipCode = ""
            val address = placeDetails.address
            (0 until address.size).forEach { i ->
                when {
                    address[i].type.contains("street_number") -> street += address[i].shortName + " "
                    address[i].type.contains("route") -> street += address[i].shortName
                    address[i].type.contains("locality") -> city += address[i].shortName
                    address[i].type.contains("administrative_area_level_1") -> state += address[i].shortName
                    address[i].type.contains("country") -> country += address[i].shortName
                    address[i].type.contains("postal_code") -> zipCode += address[i].shortName
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_URL_NAME", "")
        SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_NAME", "")
    }
}
