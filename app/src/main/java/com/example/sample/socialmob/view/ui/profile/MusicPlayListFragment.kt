package com.example.sample.socialmob.view.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SearchInnerFragmentBinding
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.music.PlaylistDataIcon
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.PlayListImageActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPlayListAdapter
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.GridSpacingItemDecoration
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.events.UpdatePlaylist
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import com.suke.widget.SwitchButton
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

@AndroidEntryPoint
class MusicPlayListFragment : Fragment(), SearchPlayListAdapter.PlayListAdapterItemClickListener,
    SearchPlayListAdapter.PlayListAdapterOptionClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mId = ""
    private var mIsFromMyProfile = "false"
    private var mLastId: String = ""
    private var isApiCalled = false

    private var playListImageView: ImageView? = null
    private var mStatus: ResultResponse.Status? = null

    private val viewModel: ProfileViewModel by viewModels()
    private var binding: SearchInnerFragmentBinding? = null
    private var mAdapter: SearchPlayListAdapter? = null
    private var singlePlayList: MutableList<PlaylistCommon>? = ArrayList()

    companion object {
        private const val ARG_STRING = "mId"
        private const val ARG_BOOLEAN = "isFromMyProfile"
        fun newInstance(mId: String, isFromMyProfile: String): MusicPlayListFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mId)
            args.putSerializable(ARG_BOOLEAN, isFromMyProfile)
            val fragment = MusicPlayListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.search_inner_fragment, container, false)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: Bundle? = arguments
        mId = args?.getString(ARG_STRING, "")!!
        mIsFromMyProfile = args.getString(ARG_BOOLEAN, "")!!

        // TODO : Check Internet connection
        callApiCommon(mId)

        binding?.searchInnerRecyclerView?.layoutManager = GridLayoutManager(activity, 2)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.penny_margin_start)
        binding?.searchInnerRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(2, spacingInPixels, true)
        )

        mAdapter = SearchPlayListAdapter(this, this, glideRequestManager)
        binding?.searchInnerRecyclerView?.adapter = mAdapter

        binding?.searchInnerRecyclerView?.isNestedScrollingEnabled = true

        observeData()
    }

    private fun observeData() {

        viewModel.playListLiveData?.nonNull()?.observe(viewLifecycleOwner, { resultResponse ->
            mStatus = resultResponse.status
            when (resultResponse.status) {
                ResultResponse.Status.SUCCESS -> {
                    singlePlayList = resultResponse?.data!!
                    //                    mAdapter?.removeAll()
                    mAdapter?.submitList(singlePlayList!!)
                    if (mIsFromMyProfile == "true") {
                        // TODO : Update Music dashboard playlist
                        EventBus.getDefault()
                            .postSticky(UpdatePlaylist(singlePlayList ?: ArrayList()))
                    }
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                    binding?.listLinear?.visibility = View.VISIBLE

                    mStatus = ResultResponse.Status.LOADING_COMPLETED
                }
                ResultResponse.Status.ERROR -> {

                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.VISIBLE
                    binding?.listLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                    glideRequestManager.load(R.drawable.server_error).apply(RequestOptions().centerInside())
                        .into(binding?.noInternetImageView!!)
                    binding?.noInternetTextView?.text = getString(R.string.server_error)
                    binding?.noInternetSecTextView?.text =
                        getString(R.string.server_error_content)

                }
                ResultResponse.Status.NO_DATA -> {
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.listLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.VISIBLE

                    glideRequestManager.load(R.drawable.ic_no_search_data).apply(RequestOptions().fitCenter())
                        .into(binding?.noDataImageView!!)
                }
                ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                    mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                }
                ResultResponse.Status.LOADING -> {

                }
                else -> {
                }
            }
        })
    }

    private fun callApiCommon(mId: String) {
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthWithoutOffsetAndCount(
                requireActivity(), "api/v1/profile/$mId/playlists"
            )
            viewModel.getUserPlayList(mAuth, mId)
        } else {
            viewModel.playListLiveData?.postValue(ResultResponse.noInternet(ArrayList()))
            val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation

            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(binding?.noInternetImageView!!)
        }
    }

    override fun onItemClick(
        item: String, mTitle: String, mImageUrl: String, mUserName: String, mUserId: String
    ) {
        val intent = Intent(activity, CommonPlayListActivity::class.java)
        intent.putExtra("mId", item)
        intent.putExtra("title", mTitle)
        intent.putExtra("imageUrl", mImageUrl)
        intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
        intent.putExtra("mPlayListUser", mUserName)
        intent.putExtra("mPlayListUserId", mUserId)
        activity?.startActivity(intent)
    }

    override fun optionDialog(
        playlistId: String?,
        playlistName: String?,
        private: String,
        icon: PlaylistDataIcon,
        adapterPosition: Int
    ) {

        val viewGroup: ViewGroup? = null
        val mBottomSheetDialog = RoundedBottomSheetDialog(requireActivity())
        val sheetView: View = layoutInflater.inflate(R.layout.play_list_options, viewGroup)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val editLinear: LinearLayout = sheetView.findViewById(R.id.edit_linear)
        val deleteLinear: LinearLayout = sheetView.findViewById(R.id.delete_linear)
        val shareLinear: LinearLayout = sheetView.findViewById(R.id.share_linear)
        if (mIsFromMyProfile == "false") {
            editLinear.visibility = View.GONE
            deleteLinear.visibility = View.GONE
        }
        if (playlistName.equals("Listen Later", ignoreCase = true)) {
            editLinear.visibility = View.GONE
            deleteLinear.visibility = View.GONE
        }
        editLinear.setOnClickListener {
            onOptionItemClickOption(
                "editPlayList",
                playlistId!!, playlistName!!, private, icon
            )
            mBottomSheetDialog.dismiss()
        }
        deleteLinear.setOnClickListener {
            val viewGroupNew: ViewGroup? = null
            val dialogBuilder: AlertDialog.Builder =
                this.let { AlertDialog.Builder(requireActivity()) }
            val inflater: LayoutInflater = layoutInflater
            val dialogView: View = inflater.inflate(R.layout.common_dialog, viewGroupNew)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
            val okButton: Button = dialogView.findViewById(R.id.ok_button)

            val b = dialogBuilder.create()
            b.show()
            okButton.setOnClickListener {

                mBottomSheetDialog.dismiss()
                onOptionItemClickOption(
                    "deletePlayList",
                    playlistId!!, "", private, PlaylistDataIcon()
                )
                singlePlayList?.removeAt(adapterPosition)
                mAdapter?.notifyDataSetChanged()
                if (mIsFromMyProfile == "true") {
                    EventBus.getDefault().postSticky(UpdatePlaylist(singlePlayList ?: ArrayList()))
                }

                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

            }
            cancelButton.setOnClickListener {
                mBottomSheetDialog.dismiss()
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

            }
        }
        shareLinear.setOnClickListener {
            ShareAppUtils.sharePlayList(playlistId, requireActivity())
            mBottomSheetDialog.dismiss()

        }

    }

    fun onOptionItemClickOption(
        mMethod: String, mId: String, mPlayListName: String, private: String, icon: PlaylistDataIcon
    ) {
        when (mMethod) {
            "deletePlayList" -> deletePlayList(mId)
            "editPlayList" -> editPlayList(mId, mPlayListName, private, icon)
        }
    }

    private fun editPlayList(
        mId: String, mPlayListName: String, private: String, icon: PlaylistDataIcon
    ) {
        var iconFileUrl = ""
        if (icon.id != null) {
            iconFileUrl = icon.fileUrl!!
        }
        if (icon.id != null) {
            mLastId = icon.id
        }

        var isPrivate: String = private
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.create_play_list_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val closeButton: TextView = dialogView.findViewById(R.id.close_button_play_list)
        val createPlayListTextView: TextView =
            dialogView.findViewById(R.id.create_play_list_text_view)
        val playListNameEditText: EditText =
            dialogView.findViewById(R.id.play_list_name_edit_text)
        playListImageView = dialogView.findViewById(R.id.play_list_image_view)
        playListNameEditText.setText(mPlayListName)
        createPlayListTextView.isAllCaps = true
        createPlayListTextView.text = getString(R.string.save)


        val switchButtonPlayList: SwitchButton =
            dialogView.findViewById(R.id.switch_button_play_list)

        switchButtonPlayList.isChecked = private != "0"


        switchButtonPlayList.setOnCheckedChangeListener { view, isChecked ->
            isPrivate = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        val b: AlertDialog = dialogBuilder.create()
        b.show()


        if (icon.fileUrl != null) {
            glideRequestManager.load(icon.fileUrl).into(playListImageView!!)
        }

        playListImageView?.setOnClickListener {
            val intent = Intent(requireActivity(), PlayListImageActivity::class.java)
            intent.putExtra("iconId", mLastId)
            intent.putExtra("iconFileUrl", iconFileUrl)
            startActivityForResult(intent, 1001)
        }

        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        createPlayListTextView.setOnClickListener {
            if (playListNameEditText.text.isNotEmpty()) {
                if (playListNameEditText.text.length > 2) {
                    val playListName = playListNameEditText.text.toString()
                    if (!playListName.equals("Listen Later", ignoreCase = true)
                    ) {
                        createPlayListApi(
                            mId, playListNameEditText.text.toString(), isPrivate, mLastId
                        )
                    } else {
                        AppUtils.showCommonToast(
                            requireActivity(),
                            getString(R.string.have_play_list)
                        )
                    }
                } else
                    AppUtils.showCommonToast(requireActivity(), "Playlist name too short")
            } else
                AppUtils.showCommonToast(requireActivity(), "Playlist name cannot be empty")
            //To close alert
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }

    private fun deletePlayList(mId: String) {
        val mAuth: String = RemoteConstant.getAuthDeletePlayList(requireActivity(), mId)
        viewModel.deletePlayList(mAuth, mId)
    }

    private fun createPlayListApi(
        playListId: String, playListName: String, private: String, icon_id: String
    ) {
        if (InternetUtil.isInternetOn()) {
            val createPlayListBody = CreatePlayListBody(playListId, playListName, icon_id, private)
            val mAuth: String = RemoteConstant.getAuthPlayList(requireActivity())
            viewModel.createPlayList(mAuth, createPlayListBody, mId)
            isApiCalled = true
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1001) {
            if (data != null) {
                if (data.getStringExtra("mFileUrl") != null) {
                    val mFileUrl: String = data.getStringExtra("mFileUrl")!!
                    if (data.getStringExtra("mLastId") != null) {
                        mLastId = data.getStringExtra("mLastId")!!
                    }

                    if (playListImageView != null) {
                        glideRequestManager.load(RemoteConstant.getImageUrlFromWidth(mFileUrl, 200))
                            .into(playListImageView!!)
                    }
                }
            }
        }
    }
}
