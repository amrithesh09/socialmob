package com.example.sample.socialmob.view.ui.profile

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ProfileActivityNewBinding
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.profile.EditProfileBody
import com.example.sample.socialmob.model.profile.LocationCoordinates
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.settings.BlockedUsersActivity
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedAdapter
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.ui.profile.followers.FollowersActivity
import com.example.sample.socialmob.view.ui.profile.followers.FollowingActivity
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.ui.write_new_post.WritePostActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.FloatingActionsMenu
import com.example.sample.socialmob.view.utils.ShareAppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.no_internet.*
import kotlinx.android.synthetic.main.options_menu_my_profile_settings.view.*
import kotlinx.android.synthetic.main.profile_activity_new.*
import java.util.*

@AndroidEntryPoint
class MyProfileActivity : AppCompatActivity() {

    private var dbPlayList: MutableList<TrackListCommon>? = ArrayList()
    private var mGlobalProfile: Profile? = null
    private var mAlertDialog: AlertDialog? = null

    private val mExtraIndex = "EXTRA_INDEX"
    private val extraIndexIsFrom = "EXTRA_INDEX_IS_FROM"
    private var mProfileAnthemTrackId: String = ""
    private var jukeBoxCategoryId: String = ""
    private var mLastClickTime: Long = 0

    private var binding: ProfileActivityNewBinding? = null
    private val profileViewModel: ProfileViewModel by viewModels()
    private var viewPagerStateAdapter: MyViewPageStateAdapter? = null

    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    /**
     * Represents a geographical location.
     */
    private var mLastLocation: Location? = null

    companion object {
        var isUploading: Boolean = false
        private val TAG = "LocationProvider"
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val mMode =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)
        if (mMode) {
            setTheme(R.style.AppTheme_NoActionBarTranslucentNightHome)
        } else {
            setTheme(R.style.AppTheme_NoActionBarTranslucentHomeDayLight)
        }
        super.onCreate(savedInstanceState)
        if (mMode) {
            changeStatusBarColorBlack()
        } else {
            changeStatusBarColor()
        }
        binding = DataBindingUtil.setContentView(this, R.layout.profile_activity_new)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val currentOrientation = resources.configuration.orientation
            requestedOrientation = if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        isUploading = false

        setStatePageAdapter()

        // TODO : Retry click listener - API
        refresh_text_view.setOnClickListener {
            if (InternetUtil.isInternetOn()) {
                binding?.noInternetLinear?.visibility = View.GONE
                getProfileDetails(
                    SharedPrefsUtils.getStringPreference(
                        this,
                        RemoteConstant.mProfileId,
                        ""
                    )
                )
            } else {
                binding?.noInternetLinear?.visibility = View.VISIBLE
                val shake: Animation =
                    AnimationUtils.loadAnimation(this@MyProfileActivity, R.anim.shake)
                binding?.noInternetLinear?.startAnimation(shake) // starts animation
            }
        }
        observeData()

        binding?.backgroundDimmer?.setOnClickListener {
            collapse()
        }
        binding?.multipleActionsProfile?.setOnFloatingActionsMenuUpdateListener(object :
            FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
            override fun onMenuExpanded() {
                binding?.backgroundDimmer?.visibility = View.VISIBLE
                binding?.backgroundDimmer?.bringToFront()
                binding?.backgroundDimmer?.post {
                    binding?.multipleActionsProfile?.bringToFront()
                }
//                appbar.visibility = View.GONE
                if (mMode) {
                    setTheme(R.style.AppTheme_NoActionBarTranslucentNight)
                } else {
                    setTheme(R.style.AppTheme_NoActionBarTranslucent)
                }
                if (mMode) {
                    changeStatusBarColorBlack()
                } else {
                    changeStatusBarColor()
                }

                if (ProfileFeedAdapter.helper != null) {
                    ProfileFeedAdapter.helper!!.pause()
                }
            }

            override fun onMenuCollapsed() {
                binding?.backgroundDimmer?.visibility = View.GONE
                if (mMode) {
                    setTheme(R.style.AppTheme_NoActionBarTranslucentNight)
                } else {
                    setTheme(R.style.AppTheme_NoActionBarTranslucent)
                }
                if (mMode) {
                    changeStatusBarColorBlack()
                } else {
                    changeStatusBarColor()
                }
            }
        })

        binding?.profileSettingsImageView?.setOnClickListener {
            profileSettings()
        }
        binding?.profileTrackTextView?.setOnClickListener {
            playSingleTrack()
        }
        binding?.followingClickMyProfile?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            followingClickMyProfile()
        }
        binding?.followersClickMyProfile?.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            followersClickMyProfile()
        }

        // TODO : update location
        updateLocation()
    }

    private fun updateLocation() {


    }

    override fun onResume() {
        super.onResume()
        if (binding?.multipleActionsProfile != null && binding?.multipleActionsProfile?.isExpanded!!) {
            collapse()
        }
        // TODO : Check Internet connection - get profile API call
        if (InternetUtil.isInternetOn()) {
            binding?.noInternetLinear?.visibility = View.GONE
            getProfileDetails(
                SharedPrefsUtils.getStringPreference(
                    this,
                    RemoteConstant.mProfileId,
                    ""
                )
            )
        } else {
            binding?.noInternetLinear?.visibility = View.VISIBLE
            val shake: Animation =
                AnimationUtils.loadAnimation(this@MyProfileActivity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        }
    }

    /**
     * Play anthem track
     */
    private fun playSingleTrack() {
        if (dbPlayList?.size ?: 0 > 0) {
            if (dbPlayList?.get(0)?.id != null && dbPlayList?.get(0)?.id != "") {
                profileViewModel.createDBPlayList(dbPlayList,"My playlist")
                val detailIntent = Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(mExtraIndex, 0)
                detailIntent.putExtra(extraIndexIsFrom, "dblistUserProfile")
                startActivity(detailIntent)
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
            }
        }
    }

    private fun followingClickMyProfile() {
        val intent = Intent(this, FollowersActivity::class.java)
        intent.putExtra(
            "mProfileId",
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        )
        startActivity(intent)
    }

    private fun followersClickMyProfile() {
        val intent = Intent(this, FollowingActivity::class.java)
        intent.putExtra(
            "mProfileId",
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")
        )
        startActivity(intent)

    }

    private fun profileSettings() {
        val viewGroup: ViewGroup? = null
        val viewProfileSettings =
            LayoutInflater.from(this@MyProfileActivity)
                .inflate(R.layout.options_menu_my_profile_settings, viewGroup)
        val mQQPop =
            PopupWindow(
                viewProfileSettings,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        mQQPop.animationStyle = R.style.RightTopPopAnim
        mQQPop.isFocusable = true
        mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mQQPop.isOutsideTouchable = true
        mQQPop.showAsDropDown(binding?.profileSettingsImageView, -150, -15)


        viewProfileSettings.blocked_users_text_view.setOnClickListener {
            val intent = Intent(this@MyProfileActivity, BlockedUsersActivity::class.java)
            startActivity(intent)
            mQQPop.dismiss()
        }

        viewProfileSettings.share_profile_text_view.setOnClickListener {
            ShareAppUtils.shareProfile(
                mContext = this,
                mProfileId = SharedPrefsUtils.getStringPreference(
                    this,
                    RemoteConstant.mProfileId,
                    ""
                )
            )
            mQQPop.dismiss()
        }

    }

    private fun getProfileDetails(mProfileId: String?) {

        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            val mAuth: String = RemoteConstant.getAuthUser(this@MyProfileActivity, mProfileId!!)
            profileViewModel.getMyProfile(mAuth, mProfileId)
        } else {
            AppUtils.showCommonToast(this@MyProfileActivity, getString(R.string.no_internet))
        }

        profileViewModel.myProfileData.nonNull().observe(this, { profileDta ->
            if (profileDta != null) {
                if (profileDta.profile != null) {
                    mGlobalProfile = profileDta.profile!!
                    if (profileDta.profile?.celebrityVerified != null && profileDta.profile?.celebrityVerified == true) {
                        binding?.verifiedTextView?.visibility = View.VISIBLE
                    }
                    if (profileDta.themeGenre?.jukeBoxCategoryId != null) {
                        jukeBoxCategoryId = profileDta.themeGenre.jukeBoxCategoryId.toString()
                    }
                    binding?.profileData = profileDta.profile
                    binding?.profileTrack = profileDta
                    if (profileDta.profileAnthemTrack != null) {
                        dbPlayList?.clear()
                        dbPlayList?.add(profileDta.profileAnthemTrack)
                    }
                }
            }
        })

        binding?.editProfileTextView?.setOnClickListener {
            editProfile()
        }
    }

    // TODO : TODO : Upload Video click
    fun uploadVideo(view: View) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {

            binding?.backgroundDimmer?.performClick()
            SharedPrefsUtils.setStringPreference(this, "POST_FROM", "PROFILE")
            val intent = Intent(this, PickerActivity::class.java)
            intent.putExtra("title", "UPLOAD VIDEO")
            intent.putExtra("IMAGES_LIMIT", 5)
            intent.putExtra("VIDEOS_LIMIT", 1)
            startActivity(intent)
        }
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    // TODO : Upload Photo
    fun uploadPhoto(view: View) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {

            binding?.backgroundDimmer?.performClick()
            SharedPrefsUtils.setStringPreference(this, "POST_FROM", "PROFILE")
            val intent = Intent(this, PickerActivity::class.java)
            intent.putExtra("title", "UPLOAD PHOTO")
            intent.putExtra("IMAGES_LIMIT", 5)
            intent.putExtra("VIDEOS_LIMIT", 1)
            startActivity(intent)
        }
        mLastClickTime = SystemClock.elapsedRealtime()

    }

    //TODO : Write Post
    fun writePost(view: View) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {

            binding?.backgroundDimmer?.performClick()
            SharedPrefsUtils.setStringPreference(this, "POST_FROM", "PROFILE")
            val intent = Intent(this, WritePostActivity::class.java)
            startActivity(intent)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    private fun observeData() {

        profileViewModel.profile.nonNull().observe(this, { profile ->
            if (profile != null) {
                if (profile.About == null || profile.About == "") {
                    binding?.bioTextView?.visibility = View.GONE
                } else {
                    binding?.bioTextView?.visibility = View.VISIBLE
                }

                if (profile.profileAnthemTrackId != null) {
                    mProfileAnthemTrackId = profile.profileAnthemTrackId
                }
            }
            binding?.viewModel = profileViewModel
            binding?.profileData = profile
            binding?.executePendingBindings()
        })
    }


    private fun editProfile() {
        val detailIntent = Intent(this, EditProfileActivity::class.java)
        startActivityForResult(detailIntent, 1501)

    }

    fun profileBackPress(view: View) {
        onBackPressed()
    }

    private fun collapse() {

        Handler().postDelayed({
            binding?.multipleActionsProfile?.collapse()
        }, 1000)
    }

    private fun setStatePageAdapter() {
        profile_viewpager.adapter = null
        profile_viewpager.offscreenPageLimit = 3

        val mId = SharedPrefsUtils.getStringPreference(this, RemoteConstant.mProfileId, "")!!
        viewPagerStateAdapter = MyViewPageStateAdapter(supportFragmentManager)
        viewPagerStateAdapter?.addFragment(ProfileFeedFragment.newInstance(mId, "true"), "POSTS")
        viewPagerStateAdapter?.addFragment(ProfileMediaGridFragment.newInstance(mId, ""), "GALLERY")
//        myViewPageStateAdapter.addFragment( ProfileMusicFragment.newInstance("MY_PROFILE", it), "PLAYLISTS" )
        viewPagerStateAdapter?.addFragment(
            MusicPlayListFragment.newInstance(mId, "true"),
            "PLAYLISTS"
        )
        profile_viewpager.adapter = viewPagerStateAdapter
        profile_tabs.setupWithViewPager(profile_viewpager)

        profile_viewpager.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    binding?.multipleActionsProfile?.visibility = View.VISIBLE
                } else {
                    binding?.multipleActionsProfile?.visibility = View.GONE
                }
                if (ProfileFeedAdapter.helper != null) {
                    ProfileFeedAdapter.helper?.pause()
                }
            }

        })
//        view_refresh_header.postDelayed({
//            view_refresh_header.stopRefresh()
//        }, 1000)


    }

    class MyViewPageStateAdapter(fm: androidx.fragment.app.FragmentManager) :
        FragmentStatePagerAdapter(fm) {
        private val fragmentList: MutableList<androidx.fragment.app.Fragment> = ArrayList()
        private val fragmentTitleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return fragmentTitleList[position]
        }

        fun addFragment(fragment: androidx.fragment.app.Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }

    /**
     * If upload on progress need to show cancel dialog
     */
    @SuppressLint("SetTextI18n")
    override fun onBackPressed() {

//        view_refresh_header?.stopRefresh()
        if (!isUploading) {
            super.onBackPressed()
            AppUtils.hideKeyboard(this@MyProfileActivity, profile_back_image_view)
        } else {
            val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
            val inflater: LayoutInflater = layoutInflater
            val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
            val okButton: Button = dialogView.findViewById(R.id.ok_button)
            val dialogDescription: TextView = dialogView.findViewById(R.id.dialog_description)
            dialogDescription.text = getString(R.string.want_to_see_post)

            dialogDescription.text = "Upload on progress do you want to cancel"

            cancelButton.text = getString(R.string.no)
            okButton.text = getString(R.string.yes)

            mAlertDialog = dialogBuilder.create()
            mAlertDialog?.show()
            okButton.setOnClickListener {
                mAlertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                super.onBackPressed()
                AppUtils.hideKeyboard(this@MyProfileActivity, profile_back_image_view)
            }
            cancelButton.setOnClickListener {
                mAlertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        isVisibleBackDismissDialog()
        profile_viewpager.adapter = null
        binding?.unbind()
    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    private fun changeStatusBarColorBlack() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

    fun isVisibleBackDismissDialog() {
        if (mAlertDialog != null && mAlertDialog?.isShowing!!) {
            mAlertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

//            showSnackbar(R.string.permission_rationale, android.R.string.ok,
//                View.OnClickListener {
//                     Request permission
//                    startLocationPermissionRequest()
//                })

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation()
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
//                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
//                    View.OnClickListener {
//                        // Build intent that displays the App settings screen.
//                        val intent = Intent()
//                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                        val uri = Uri.fromParts("package",
//                            BuildConfig.APPLICATION_ID, null)
//                        intent.data = uri
//                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                        startActivity(intent)
//                    })
            }
        }
    }

    fun getView(): FloatingActionsMenu {
        return binding?.multipleActionsProfile!!
    }

    public override fun onStart() {
        super.onStart()

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     *
     *
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressLint("MissingPermission")
    /**
     * Update location API
     */
    private fun getLastLocation() {
        mFusedLocationClient!!.lastLocation
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result


                    SharedPrefsUtils.setStringPreference(
                        this,
                        RemoteConstant.LATITUDE,
                        mLastLocation?.latitude.toString()
                    )
                    SharedPrefsUtils.setStringPreference(
                        this,
                        RemoteConstant.LONGITUDE,
                        mLastLocation?.longitude.toString()
                    )

                    try {
                        if (SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.LONGITUDE,
                                ""
                            ) != ""
                        ) {

                            val mLocationUpdated = SharedPrefsUtils.getStringPreference(
                                this, RemoteConstant.IS_LOCATION_UPDATED, "false"
                            )
                            if (mLocationUpdated == "false") {
                                SharedPrefsUtils.setStringPreference(
                                    this,
                                    RemoteConstant.IS_LOCATION_UPDATED,
                                    "true"
                                )
                                val fullData = RemoteConstant.getEncryptedString(
                                    SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mAppId,
                                        ""
                                    )!!,
                                    SharedPrefsUtils.getStringPreference(
                                        this,
                                        RemoteConstant.mApiKey,
                                        ""
                                    )!!,
                                    RemoteConstant.mBaseUrl + RemoteConstant.updateProfileData,
                                    RemoteConstant.postMethod
                                )
                                val mAuth =
                                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                        this, RemoteConstant.mAppId,
                                        ""
                                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                                var mProfileImage: String? = null
                                if (SharedPrefsUtils.getStringPreference(
                                        this,
                                        "PROFILE_PIC_FILE_URL_NAME",
                                        ""
                                    ) != ""
                                ) {
                                    mProfileImage =
                                        SharedPrefsUtils.getStringPreference(
                                            this, "PROFILE_PIC_FILE_URL_NAME", ""
                                        )!!
                                }

                                val locationCoordinates: LocationCoordinates =
                                    LocationCoordinates(
                                        SharedPrefsUtils.getStringPreference(
                                            this,
                                            RemoteConstant.LATITUDE,
                                            ""
                                        ),
                                        SharedPrefsUtils.getStringPreference(
                                            this,
                                            RemoteConstant.LONGITUDE,
                                            ""
                                        )
                                    )
                                val editProfileBody =
                                    EditProfileBody(
                                        mGlobalProfile?.Name!!,
                                        mGlobalProfile?.Location!!,
                                        mProfileImage,
                                        mProfileAnthemTrackId,
                                        jukeBoxCategoryId,
                                        mGlobalProfile?.username!!,
                                        mGlobalProfile?.About!!,
                                        locationCoordinates
                                    )

                                // TODO : Update name in DB

                                profileViewModel.updateProfile(mAuth, editProfileBody)

                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                } else {
                    Log.w(TAG, "getLastLocation:exception", task.exception)
                }
            }
    }

}