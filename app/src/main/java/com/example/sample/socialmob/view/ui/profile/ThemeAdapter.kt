package com.example.sample.socialmob.view.ui.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ThemeListItemBinding
import com.example.sample.socialmob.model.music.Genre

// TODO : Disabled
class ThemeAdapter(
    private val themeItems: List<Genre>?,
    private val themeItemClickListner: ThemeItemClickListener?
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ThemeAdapter.QueueListViewHolder>() {
    private var rowIndex = -1
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): QueueListViewHolder {
        val binding = DataBindingUtil.inflate<ThemeListItemBinding>(
            LayoutInflater.from(parent.context), R.layout.theme_list_item, parent, false
        )
        return QueueListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: QueueListViewHolder, position: Int) {
        holder.binding.themeViewModel = themeItems!![position]
        holder.binding.executePendingBindings()

        holder.itemView.setOnClickListener {
            rowIndex = holder.adapterPosition
            notifyDataSetChanged()
        }
        if (rowIndex == position) {
            themeItemClickListner!!.onThemeItemClickListener(
                themeItems[holder.adapterPosition].id,
                themeItems[holder.adapterPosition].media?.squareImage!!
            )
            holder.binding.themeImageView.setImageResource(R.drawable.rounded_profile_bg)
        } else {
            holder.binding.themeImageView.setImageResource(0)
        }

    }

    override fun getItemCount(): Int {
        return themeItems!!.size
    }

    class QueueListViewHolder(val binding: ThemeListItemBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
//
    }

    interface ThemeItemClickListener {
        fun onThemeItemClickListener(jukeBoxCategoryId: String, mThemeCover: String)
    }
}
