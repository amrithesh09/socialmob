package com.example.sample.socialmob.view.ui.profile

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.write_new_post.CameraPhotoVideoBasicFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.ViewPagerAdapterWithTitle
import com.example.sample.socialmob.view.utils.galleryPicker.view.VideosFragment
import kotlinx.android.synthetic.main.upload_photo_activity.*

class UploadPhotoActivity : AppCompatActivity() {
    private lateinit var viewPagerAdapter: ViewPagerAdapterWithTitle
    private var mtitle = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.upload_photo_activity)

        // TODO : Get Bundle Value
        if (intent.hasExtra("title")) {
            mtitle = intent.getStringExtra("title") ?: ""
        }

        title_text_view.text = mtitle

        viewPagerAdapter = ViewPagerAdapterWithTitle(supportFragmentManager)
        addPagerFragments()
        view_pager_upload.adapter = viewPagerAdapter
        view_pager_upload.offscreenPageLimit = 2

        upload_tabs.setupWithViewPager(view_pager_upload)
    }

    fun backPressUploadPhoto(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@UploadPhotoActivity, upload_back_image_view)
    }

    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

    private fun addPagerFragments() {
        viewPagerAdapter.add(CameraPhotoVideoBasicFragment.newInstance(mtitle, ""), "CAMERA")
        viewPagerAdapter.add(VideosFragment(), "GALLERY")

    }

}