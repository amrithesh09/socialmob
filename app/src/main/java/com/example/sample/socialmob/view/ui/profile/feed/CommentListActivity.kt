package com.example.sample.socialmob.view.ui.profile.feed

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.CommentActivityBinding
import com.example.sample.socialmob.di.util.CacheMapperMention
import com.example.sample.socialmob.model.article.CommentPostData
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.view.utils.socialview.Mention
import com.example.sample.socialmob.view.utils.socialview.MentionArrayAdapter
import com.example.sample.socialmob.viewmodel.profile.PostCommentLikeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.comment_activity.*
import javax.inject.Inject

@AndroidEntryPoint
class CommentListActivity : BaseCommonActivity(), PostCommentAdapter.OptionPostItemClickListener {
    @Inject
    lateinit var glideRequestManager: RequestManager

    @Inject
    lateinit var cacheMapperMention: CacheMapperMention

    private var mPostId = ""
    private var mCommentCount = ""
    private var mIsMyPost = false
    private var isLoaderAdded: Boolean = false
    private var mPosition = 0
    private var mCountComments: Int = 0
    private var binding: CommentActivityBinding? = null
    private var mCommentAdapter: PostCommentAdapter? = null
    private val postCommentLikeViewModel: PostCommentLikeViewModel by viewModels()

    private var mGlobalCommentList: MutableList<CommentResponseModelPayloadComment>? = ArrayList()
    private var mStatus: ResultResponse.Status? = null

    // For mention
    private var mentionList: MutableList<Profile>? = ArrayList()
    private var defaultMentionAdapter: ArrayAdapter<Mention>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.comment_activity)


        val i: Intent = intent
        mPostId = i.getStringExtra("mPostId")!!
        mPosition = i.getIntExtra("mPosition", 0)
        if (i.hasExtra("mIsMyPost")) {
            mIsMyPost = i.getBooleanExtra("mIsMyPost", false)
        }
        if (i.hasExtra("needToFocus")) {
            val needToFocus = i.getBooleanExtra("needToFocus", false)
            if (needToFocus) {
                val handler = Handler(Looper.getMainLooper())
                handler.postDelayed({
                    AppUtils.showKeyboard(this, binding?.commentEditText)
                }, 1000)
            }
        }

        val profileImage = SharedPrefsUtils.getStringPreference(
            this@CommentListActivity, RemoteConstant.mUserImage, ""
        ) ?: ""
        if (profileImage != "") {
            glideRequestManager.load(RemoteConstant.getImageUrlFromWidth(profileImage, 100))
                .into(binding?.commentProfileImageView!!)
        } else {
            glideRequestManager.load(R.drawable.emptypicture)
                .into(binding?.commentProfileImageView!!)
        }

        // TODO : Init API call
        callApiCommon()
        observeCommentList()

        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
            observeCommentList()
        }


        binding?.postCommentsRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding?.postCommentsRecyclerView?.addOnScrollListener(CustomScrollListener())
        mCommentAdapter = PostCommentAdapter(
            this@CommentListActivity, this, mIsMyPost, glideRequestManager
        )
        binding?.postCommentsRecyclerView?.adapter = mCommentAdapter


        // TODO : Mention in comments
        binding?.commentEditText?.isMentionEnabled = true
        binding?.commentEditText?.setMentionTextChangedListener { _, text ->
            if (text.isNotEmpty()) {
                defaultMentionAdapter?.clear()
                postCommentLikeViewModel.getMentions(this, text.toString())
            }
        }
        observeSearchMention()

        defaultMentionAdapter = MentionArrayAdapter(this)
        binding?.commentEditText?.mentionAdapter = defaultMentionAdapter

        binding?.commentsBackImageView?.setOnClickListener {
            onBackPressed()
        }
        binding?.topLinear?.viewTreeObserver?.addOnGlobalLayoutListener {
            val r = Rect()
            binding?.topLinear?.getWindowVisibleDisplayFrame(r)
            val screenHeight = binding?.topLinear?.rootView?.height

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            val keypadHeight = screenHeight!! - r.bottom


            binding?.swipeBackLayout?.isSwipeFromEdge = keypadHeight > screenHeight * 0.15
        }

    }

    private fun observeSearchMention() {

        postCommentLikeViewModel.mentionProfileResponseModel.nonNull().observe(this, { profiles ->
            if (profiles != null) {
                val mMentionList: MutableList<Mention>
                if (profiles.isNotEmpty()) {
                    mentionList = profiles

                    val mMentionEditTextList: MutableList<String> =
                        binding?.commentEditText?.mentions ?: ArrayList()

                    val mPopulateList: MutableList<Profile> =
                        mentionList?.filter {
                            it.username !in mMentionEditTextList.map { item ->
                                item.replace("@", "")
                            }
                        } as MutableList<Profile>
                    mMentionList =
                        cacheMapperMention.mapFromEntityListMention(mPopulateList).toMutableList()
                    defaultMentionAdapter?.addAll(mMentionList)
                }

                if (defaultMentionAdapter == null) {
                    defaultMentionAdapter = MentionArrayAdapter(this)
                    binding?.commentEditText?.mentionAdapter = defaultMentionAdapter
                } else {
                    binding?.commentEditText?.mentionAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager?.childCount!!
            val totalItemCount = recyclerView.layoutManager?.itemCount!!
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                    !isLoaderAdded && mGlobalCommentList != null
                    && mGlobalCommentList?.isNotEmpty()!! && mGlobalCommentList?.size!! >= 15
                ) {
                    if (InternetUtil.isInternetOn()) {
                        loadMoreItems(mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1)?.commentId!!)
                    }
                }
            }
        }
    }

    private fun loadMoreItems(size: String) {
        val mCommentList: MutableList<CommentResponseModelPayloadComment> = ArrayList()
        val mList = CommentResponseModelPayloadComment()
        mList.commentId = ""
        mCommentList.add(mList)
        mGlobalCommentList?.addAll(mCommentList)
        mCommentAdapter?.notifyDataSetChanged()

        isLoaderAdded = true

        callApi(mPostId, size)

    }

    private fun callApiCommon() {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            callApi(mPostId, "0")
        } else {
            postCommentLikeViewModel.commentPaginatedResponseModel?.postValue(
                ResultResponse(
                    ResultResponse.Status.NO_INTERNET, ArrayList(), getString(R.string.no_internet)
                )
            )
            val shake: Animation =
                AnimationUtils.loadAnimation(this@CommentListActivity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        }

    }

    fun sendComment(view: View) {
        if (binding?.commentEditText?.text?.isNotEmpty()!!) {
            mCountComments += 1
            // TODO : Check Internet connection
            if (!InternetUtil.isInternetOn()) {
                AppUtils.showCommonToast(this@CommentListActivity, getString(R.string.no_internet))
            } else {
                val mAuth = RemoteConstant.getAuthPostComment(this@CommentListActivity, mPostId)
                val commentPostData =
                    CommentPostData(
                        binding?.commentEditText?.text.toString(),
                        null
                    )
                postCommentLikeViewModel.postComment(mAuth, mPostId, commentPostData)
                AppUtils.hideKeyboard(this@CommentListActivity, binding?.commentEditText!!)
            }
        } else
            AppUtils.showCommonToast(this@CommentListActivity, "Comment cannot be empty")
        binding?.commentEditText?.setText("")
    }

    private fun callApi(mPostId: String, size: String) {
        val mAuth =
            RemoteConstant.getAuthCommentList(
                this@CommentListActivity, mPostId, size, RemoteConstant.mCount.toInt()
            )
        postCommentLikeViewModel.getFeedComment(
            mAuth, mPostId, size, RemoteConstant.mCount
        )
    }

    private fun removeLoaderFormList() {

        if (mGlobalCommentList != null && mGlobalCommentList?.size!! > 0 && mGlobalCommentList?.get(
                mGlobalCommentList?.size!! - 1
            ) != null && mGlobalCommentList?.get(mGlobalCommentList?.size!! - 1)?.commentId == ""
        ) {
            mGlobalCommentList?.removeAt(mGlobalCommentList?.size!! - 1)
            mCommentAdapter?.notifyDataSetChanged()
            isLoaderAdded = false
        }
    }

    private fun observeCommentList() {
        postCommentLikeViewModel.commentPaginatedResponseModel?.nonNull()
            ?.observe(this, { resultResponse ->
                mStatus = resultResponse.status
                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        mGlobalCommentList = resultResponse?.data!!.toMutableList()
                        mCommentAdapter?.submitList(mGlobalCommentList)

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.VISIBLE

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mGlobalCommentList?.addAll(resultResponse?.data!!)
                        mCommentAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {
                        if (mGlobalCommentList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.dataLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE

                        glideRequestManager.load(R.drawable.ic_empty_comments).apply(RequestOptions().fitCenter())
                            .into(binding?.noCommentsImageView!!)
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }


            })
        postCommentLikeViewModel.commentPaginatedResponseModelCommentCount.observe(
            this, { commentCount -> mCommentCount = commentCount })
    }

    override fun onDestroy() {
        super.onDestroy()
        binding?.postCommentsRecyclerView?.adapter = null
    }

    override fun onOptionItemClickOption(
        mMethod: String, mId: String, mComment: String, mPosition: Int
    ) {
        when (mMethod) {
//            "editComment" -> editComment(mId, mComment)
            "deleteComment" -> deleteComment(mId, mPosition)
        }
    }

    private fun deleteComment(mId: String, mPosition: Int) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mPostId +
                    RemoteConstant.mPathComment +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        postCommentLikeViewModel.deleteComment(mAuth, mPostId, mId)
        postCommentLikeViewModel.commentDeleteResponseModel.nonNull().observe(
            this, { commentDeleteResponse ->
                mCommentCount = if (commentDeleteResponse?.payload?.commentsCount != null) {
                    commentDeleteResponse.payload.commentsCount
                } else {
                    "0"
                }
            })

        mGlobalCommentList?.removeAt(mPosition)
        mCommentAdapter?.notifyDataSetChanged()

    }

    override fun onBackPressed() {
        val intent = intent
        intent.putExtra("COMMENT_COUNT", mCommentCount)
        intent.putExtra("POST_ID", mPostId)
        intent.putExtra("POST_POSITION", mPosition)
        intent.putExtra("IS_FROM_COMMENT_LIST", true)
        setResult(1501, intent)
        super.onBackPressed()
        AppUtils.hideKeyboard(this@CommentListActivity, post_comments_recycler_view)
    }
}
