package com.example.sample.socialmob.view.ui.profile.feed

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemCommentsBinding
import com.example.sample.socialmob.model.article.SingleArticleApiModelPayloadComment
import kotlinx.android.synthetic.main.options_menu_article_edit.view.*

class FeedCommentAdapter(
    private val commentItems: List<SingleArticleApiModelPayloadComment>?,
    private val mContext: androidx.fragment.app.FragmentActivity?,
    private val optionArticleItemClickListener: OptionArticleItemClickListener
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<FeedCommentAdapter.CommentsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CommentsViewHolder {
        val binding = DataBindingUtil.inflate<LayoutItemCommentsBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_comments, parent, false
        )
        return CommentsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        holder.binding.commentViewModel = commentItems!![position]
        holder.binding.executePendingBindings()

        holder.binding.articleCommentOptionImageView.setOnClickListener {
            val viewGroup: ViewGroup? = null
            val view: View =
                LayoutInflater.from(mContext).inflate(R.layout.options_menu_article_edit, viewGroup)
            val mQQPop = PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            mQQPop.animationStyle = R.style.RightTopPopAnim
            mQQPop.isFocusable = true
            mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mQQPop.isOutsideTouchable = true
            mQQPop.showAsDropDown(holder.binding.articleCommentOptionImageView, -180, -25)

            view.edit_text_view.setOnClickListener {
                optionArticleItemClickListener.onOptionItemClickOption(
                    "editComment",
                    commentItems[position].id!!, commentItems[position].text!!
                )
                mQQPop.dismiss()
            }
            view.delete_text_view.setOnClickListener {
                optionArticleItemClickListener.onOptionItemClickOption(
                    "deleteComment",
                    commentItems[position].id!!, ""
                )
                mQQPop.dismiss()
            }
        }
    }

    override fun getItemCount(): Int {
        return commentItems!!.size
    }

    class CommentsViewHolder(val binding: LayoutItemCommentsBinding) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
//
    }

    interface OptionArticleItemClickListener {
        fun onOptionItemClickOption(mMethod: String, mId: String, mComment: String)
    }
}