package com.example.sample.socialmob.view.ui.profile.feed

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutPostItemCommentsBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.BaseViewHolder
import kotlinx.android.synthetic.main.options_menu_article_edit.view.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class PostCommentAdapter(
    private val mContext: FragmentActivity?,
    private val optionPostItemClickListener: OptionPostItemClickListener,
    private var mIsMyPost: Boolean,
    private var glideRequestManager: RequestManager
) :
    ListAdapter<CommentResponseModelPayloadComment, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<CommentResponseModelPayloadComment>() {
                override fun areItemsTheSame(
                    oldItem: CommentResponseModelPayloadComment,
                    newItem: CommentResponseModelPayloadComment
                ): Boolean {
                    return oldItem.commentId == newItem.commentId
                }

                override fun areContentsTheSame(
                    oldItem: CommentResponseModelPayloadComment,
                    newItem: CommentResponseModelPayloadComment
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutPostItemCommentsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_post_item_comments, parent, false
        )
        return CommentsViewHolder(binding)
    }


    override fun getItemViewType(position: Int): Int {

        return if (getItem(position)?.commentId != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }


    inner class CommentsViewHolder(val binding: LayoutPostItemCommentsBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.commentViewModel = getItem(adapterPosition)
            binding.executePendingBindings()

            if (getItem(adapterPosition)?.profile?.pimage != "") {
                glideRequestManager.load(
                    RemoteConstant.getImageUrlFromWidth(
                        getItem(adapterPosition)?.profile?.pimage, 200
                    )
                ).into(binding.commentProfileImageView)
            } else {
                glideRequestManager.load(R.drawable.emptypicture)
                    .into(binding.commentProfileImageView)
            }
            if (getItem(adapterPosition)?.own == true) {
                binding.commentOptionImageView.visibility = View.VISIBLE
            } else {
                binding.commentOptionImageView.visibility = View.GONE
            }
            if (mIsMyPost) {
                binding.commentOptionImageView.visibility = View.VISIBLE
            }

            binding.commentOptionImageView.setOnClickListener {
                val viewGroup: ViewGroup? = null
                val view: View = LayoutInflater.from(mContext)
                    .inflate(R.layout.options_menu_article_edit, viewGroup)
                val mQQPop =
                    PopupWindow(
                        view,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                mQQPop.animationStyle = R.style.RightTopPopAnim
                mQQPop.isFocusable = true
                mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mQQPop.isOutsideTouchable = true
                mQQPop.showAsDropDown(binding.commentOptionImageView, -180, -25)
                view.edit_text_view.visibility = View.GONE


                view.edit_text_view.setOnClickListener {
                    optionPostItemClickListener.onOptionItemClickOption(
                        "editComment",
                        getItem(adapterPosition)?.commentId!!,
                        getItem(adapterPosition)?.text!!,
                        adapterPosition
                    )
                    mQQPop.dismiss()
                }
                view.delete_text_view.setOnClickListener {

                    val dialogBuilder: AlertDialog.Builder =
                        this.let { AlertDialog.Builder(mContext) }
                    val inflater: LayoutInflater = mContext!!.layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b: AlertDialog = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        optionPostItemClickListener.onOptionItemClickOption(
                            "deleteComment",
                            getItem(adapterPosition)?.commentId!!, "",
                            adapterPosition
                        )
                        mQQPop.dismiss()
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        mQQPop.dismiss()
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                }
            }

            binding.commentUserNameTextView.setOnClickListener {
                binding.commentProfileImageView.performClick()
            }

            binding.commentProfileImageView.setOnClickListener {
                val intent = Intent(mContext, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", getItem(adapterPosition)?.profile!!._id)
                mContext!!.startActivityForResult(intent, 0)
            }


            binding.descriptionTextView.setOnMentionClickListener { view, text ->
                for (i in getItem(adapterPosition)?.mentions!!.indices) {
                    if (getItem(adapterPosition)?.mentions!![i].text!!.contains(text)) {
                        if (getItem(adapterPosition)?.mentions!![i].id != null) {
                            val intent = Intent(mContext, UserProfileActivity::class.java)
                            intent.putExtra(
                                "mProfileId",
                                getItem(adapterPosition)?.mentions!![i].id
                            )
                            mContext!!.startActivityForResult(intent, 0)
                        }
                    }
                }
            }
            binding.descriptionTextView.setOnClickListener {
                if (retrieveLinks(getItem(adapterPosition)?.text!!) != "") {
                    if (retrieveLinks(getItem(adapterPosition)?.text!!).contains("https://")) {
                        val url = retrieveLinks(getItem(adapterPosition)?.text!!)
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(url)
                        mContext!!.startActivity(intent)
                    } else {
                        try {
                            val url = retrieveLinks(getItem(adapterPosition)?.text!!)
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.data = Uri.parse("https://$url")
                            mContext!!.startActivity(intent)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }

    }

    fun retrieveLinks(text: String): String {
        try {
            val links: MutableList<String> = ArrayList()

            val regex =
                "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"
            val p: Pattern = Pattern.compile(regex)
            val m: Matcher = p.matcher(text)
            while (m.find()) {
                var urlStr: String = m.group()
                if (urlStr.startsWith("(") && urlStr.endsWith(")")) {

                    val stringArray = urlStr.toCharArray()

                    val newArray = CharArray(stringArray.size - 2)
                    System.arraycopy(stringArray, 1, newArray, 0, stringArray.size - 2)
                    urlStr = String(newArray)
                    println("Finally Url =$newArray")

                }
                println("...Url...$urlStr")
                links.add(urlStr)
            }
            return links[0]
        } catch (e: Exception) {
            try {
                val links: MutableList<String> = ArrayList()
                val m = Patterns.WEB_URL.matcher(text)
                while (m.find()) {
                    val url = m.group()
                    links.add(url)
                }
                return links[0]
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return ""
    }

    fun setIsOwn(mIsMyPostOwn: Boolean) {
        mIsMyPost = mIsMyPostOwn
        notifyDataSetChanged()
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }
    }

    interface OptionPostItemClickListener {
        fun onOptionItemClickOption(mMethod: String, mId: String, mComment: String, mPosition: Int)
    }
}