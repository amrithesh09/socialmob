package com.example.sample.socialmob.view.ui.profile.feed

import android.animation.Animator
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Handler
import android.os.SystemClock
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.*
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.*
import com.example.sample.socialmob.model.feed.PostFeedDataMention
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageListAdapterInner
import com.example.sample.socialmob.view.ui.home_module.feed.MainFeedAdapter
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleAdapter
import com.example.sample.socialmob.view.ui.home_module.settings.HelpFeedBackReportProblemActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.ui.write_new_post.WritePostActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.music.service.MediaService
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio
import com.google.android.gms.ads.AdRequest
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroUtil
import im.ene.toro.exoplayer.ExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.media.VolumeInfo
import im.ene.toro.widget.Container
import kotlinx.android.synthetic.main.admob_item.view.*
import kotlinx.android.synthetic.main.detailedimage_view_pager.*
import kotlinx.android.synthetic.main.options_menu_feed_option.view.*
import java.util.regex.Pattern


class ProfileFeedAdapter internal constructor(
    private val mContext: FragmentActivity?,
    private val feedItemClick: FeedItemClick,
    private val feedItemLikeClick: FeedItemLikeClick,
    private val feedCommentClick: FeedCommentClick,
    private val feedLikeListClick: FeedLikeListClick,
    private val optionItemClickListener: OptionItemClickListener,
    private val hashTagClickListener: HashTagClickListener,
    private val isEditEnabled: Boolean,
    private val mPlayTrack: HyperLink,
    private val mHyperLinkUrl: HyperLinkUrl,
    private val glideRequestManager: RequestManager,
    private val searchPeopleAdapterFollowItemClick: SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick
) : RecyclerView.Adapter<BaseViewHolder<Any>>() {
    private var list: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()

    private var isCalledHashTgaDetailsPage: Boolean = false
    private var progressBar: ProgressBar? = null
    private var uploadTextView: TextView? = null
    private var mLastClickTime: Long = 0


    fun submitList(mutableList: MutableList<PersonalFeedResponseModelPayloadFeed>) {
        val diffCallback =
            FeedListDiffUtils(
                this.list!!,
                mutableList
            )
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.list?.clear()
        this.list?.addAll(mutableList)
        diffResult.dispatchUpdatesTo(this)
    }

    companion object {
        var isMute = true
        const val ITEM_TEXT = 1
        const val ITEM_IMAGE = 2
        const val ITEM_VIDEO = 3
        const val ITEM_LOADING = 4
        const val ITEM_UPLOAD = 61
        const val ITEM_AD = 5
        const val ITEM_MUSIC = 6
        const val ITEM_ARTIST = 7
        const val ITEM_PLAYLIST = 8
        const val ITEM_PROFILE = 9
        const val ITEM_MUSIC_VIDEO = 10
        var helper: ExoPlayerViewHelper? = null
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return when (viewType) {
            ITEM_TEXT -> bindText(parent)
            ITEM_IMAGE -> bindImage(parent)
            ITEM_LOADING -> bindLoader(parent)
            ITEM_UPLOAD -> bindUploadLoader(parent)
            ITEM_AD -> bindAd(parent)
            ITEM_MUSIC -> bindMusic(parent)
            ITEM_ARTIST -> bindArtist(parent)
            ITEM_PLAYLIST -> bindPlaylist(parent)
            ITEM_PROFILE -> bindProfile(parent)
            ITEM_MUSIC_VIDEO -> bindMusicVideo(parent)
            else -> bindVideo(parent)
        }
    }

    private fun bindArtist(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemArtistBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_artist, parent, false
        )
        return ViewHolderItemArtist(binding)
    }

    private fun bindPlaylist(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemPlaylistBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_playlist, parent, false
        )
        return ViewHolderItemPlayList(binding)
    }

    private fun bindMusic(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemMusicBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_music, parent, false
        )
        return ViewHolderItemMusic(binding)
    }

    private fun bindProfile(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemProfileBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_profile, parent, false
        )
        return ViewHolderItemProfile(binding)
    }

    private fun bindMusicVideo(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemMusicVideoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_item_music_video, parent, false
        )
        return ViewHolderItemMusicVideo(binding)
    }

    private fun bindAd(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: AdmobItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.admob_item, parent, false
        )
        return AdViewHolder(binding, parent)
    }

    private fun bindUploadLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: UploadProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.upload_progress_item, parent, false
        )
        return UploadProgressViewHolder(binding)

    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }

    private fun bindVideo(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemVideoExoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_video_exo, parent, false
        )
        return VideoViewHolder(binding)
    }

    private fun bindImage(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<LayoutItemImageBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_image, parent, false
        )
        return ViewHolderItemImage(binding)
    }

    private fun bindText(parent: ViewGroup): BaseViewHolder<Any> {
        val binding = DataBindingUtil.inflate<LayoutItemTextBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_text, parent, false
        )
        return ViewHolderItemText(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(list?.get(holder.adapterPosition))
    }


    override fun getItemViewType(position: Int): Int {
        val mItem = list?.get(position)
        if (mItem?.action != null &&
            mItem.action?.post != null &&
            mItem.action?.post?.type != null
        ) {
            return when (mItem.action!!.post!!.type) {
                "user_text_post" -> {

                    when (mItem.action?.post?.ogTags?.meta?.socialmobMeta?.pageType) {
                        "music" -> {
                            ITEM_MUSIC
                        }
                        "artist" -> {
                            ITEM_ARTIST
                        }
                        "playlist" -> {
                            ITEM_PLAYLIST
                        }
                        "profile" -> {
                            ITEM_PROFILE
                        }
                        "track-video" -> {
                            ITEM_MUSIC_VIDEO
                        }
                        else -> {
                            ITEM_TEXT
                        }
                    }

                }
                "user_image_post" -> {
                    ITEM_IMAGE
                }
                "user_video_post" -> {
                    ITEM_VIDEO
                }
                else -> {
                    ITEM_TEXT
                }
            }
        } else {
            return when {
                mItem?.isAd != null && mItem.isAd == "ad" -> ITEM_AD
                mItem?.isLoader != null && mItem.isLoader == "upload" -> ITEM_UPLOAD
                else -> ITEM_LOADING
            }
        }

    }

    inner class ViewHolderItemPlayList(val binding: LayoutItemPlaylistBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            var mFeedItem = list?.get(adapterPosition)!!
            binding.textViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (mFeedItem.action?.post?.ogTags != null &&
                    mFeedItem.action?.post?.ogTags?.id != null &&
                    mFeedItem.action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = mFeedItem.action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(mFeedItem.action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }
                    binding.playMageView.setImageResource(R.drawable.ic_play)
                    binding.trackNameTextView.textSize = 20F
                    binding.authorTextView.textSize = 13F
                    binding.playListImageView.visibility = View.VISIBLE

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (mFeedItem.action?.post?.likedConnections != null) {
                    if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                            if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."

                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }

                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + ", "

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE
                                    likedCollection =
                                        likedCollection + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + "."

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                mFeedItem.action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (mFeedItem.action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        mFeedItem.action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (mFeedItem.actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding.followingTextView.setOnClickListener {

                    if (mFeedItem.actor?.relation == "following") {
                        val dialogBuilder = this.let {
                            androidx.appcompat.app.AlertDialog.Builder(
                                mContext!!
                            )
                        }
                        val inflater = mContext?.layoutInflater
                        val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                mFeedItem.actor?.relation ?: "",
                                mFeedItem.actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    mFeedItem.actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {

                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                        /*feedItemClick.onFeedItemClick(
                            mFeedItem.action?.post?.id!!,
                            "text"
                        )*/
                    }
                }

                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem?.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (mFeedItem.action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding.tvViewAllComments.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }
                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action?.post?.type,
                        mFeedItem.action?.post?.id!!, mContext!!
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (mFeedItem.action != null
                            && mFeedItem.action?.post != null
                            && mFeedItem.action?.post?.ogTags != null
                            && mFeedItem.action?.post?.ogTags?.url != null
                            && mFeedItem.action?.post?.ogTags?.url != ""
                        )/* {
                            feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "video")

                        }*/ {
                            if (mFeedItem.action?.post?.ogTags?.url!!.contains("http")) {

                                val url = mFeedItem.action?.post?.ogTags?.url
                                mHyperLinkUrl.onHyperLinkUrlClick(url!!)
                            }
                        } else {
                            if (retrieveLinks(mFeedItem.action?.post?.description!!) != "") {
                                try {
                                    goToLink(mFeedItem.action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            mFeedItem.action?.post?.hashTags!!, text,
                            mFeedItem.action?.post?.id!!,
                            mFeedItem.action?.post?.likesCount!!,
                            mFeedItem.action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(mFeedItem.action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(mFeedItem.actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition
                    )
                }
            }
        }
    }


    inner class ViewHolderItemArtist(val binding: LayoutItemArtistBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            var mFeedItem = list?.get(adapterPosition)!!
            binding.textViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (mFeedItem.action?.post?.ogTags != null &&
                    mFeedItem.action?.post?.ogTags?.id != null &&
                    mFeedItem.action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = mFeedItem.action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(mFeedItem.action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }
                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (mFeedItem.action?.post?.likedConnections != null) {
                    if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                            if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."

                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }

                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + ", "

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE
                                    likedCollection =
                                        likedCollection + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + "."

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                mFeedItem.action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (mFeedItem.action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        mFeedItem.action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (mFeedItem.actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding.followingTextView.setOnClickListener {

                    if (mFeedItem.actor?.relation == "following") {
                        val dialogBuilder = this.let {
                            androidx.appcompat.app.AlertDialog.Builder(
                                mContext!!
                            )
                        }
                        val inflater = mContext?.layoutInflater
                        val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                mFeedItem.actor?.relation ?: "",
                                mFeedItem.actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    mFeedItem.actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                        /*feedItemClick.onFeedItemClick(
                            mFeedItem.action?.post?.id!!,
                            "text"
                        )*/
                    }
                }

                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem?.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (mFeedItem.action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action?.post?.type,
                        mFeedItem.action?.post?.id!!, mContext!!
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (mFeedItem.action != null
                            && mFeedItem.action?.post != null
                            && mFeedItem.action?.post?.ogTags != null
                            && mFeedItem.action?.post?.ogTags?.url != null
                            && mFeedItem.action?.post?.ogTags?.url != ""
                        ) {
                            if (mFeedItem.action?.post?.ogTags?.url!!.contains("http")) {

                                val url = mFeedItem.action?.post?.ogTags?.url
                                mHyperLinkUrl.onHyperLinkUrlClick(url!!)
                            }
                        } else {
                            if (retrieveLinks(mFeedItem.action?.post?.description!!) != "") {
                                try {
                                    goToLink(mFeedItem.action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            mFeedItem.action?.post?.hashTags!!, text,
                            mFeedItem.action?.post?.id!!,
                            mFeedItem.action?.post?.likesCount!!,
                            mFeedItem.action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(mFeedItem.action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(mFeedItem.actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition
                    )
                }
            }
        }
    }

    inner class ViewHolderItemImage(val binding: LayoutItemImageBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            val mFeedItem = list?.get(adapterPosition)!!
            val feedImageListAdapterInner: FeedImageListAdapterInner?
            binding.imageViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (mFeedItem.action?.post?.likedConnections != null) {
                if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                    var likedCollection: String = ""
                    for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                        if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                            binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                View.VISIBLE

                            binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                View.GONE
                            binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                View.GONE
                            if (mFeedItem.action?.post?.likedConnections?.get(
                                    i
                                )?.pimage != ""
                            ) {
                                glideRequestManager.load(
                                    mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage
                                ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                            } else {
                                glideRequestManager.load(R.drawable.emptypicture)
                                    .into(binding.feedpostLikedby.ivFeedPostLiker1)
                            }

                            likedCollection =
                                "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                    i
                                )?.Name + "."

                            if (likedCollection.length >= 20) {
                                likedCollection.substring(
                                    0,
                                    20
                                ) + "..."
                            } else {
                                likedCollection
                            }

                            binding.feedpostLikedby.textViewLikedby.text =
                                likedCollection
                        } else {
                            if (i == 0) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name

                            } else {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                }

                                if (i > 2) {
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.VISIBLE
                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    }
                                }
                            }

                            if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                val mlikedCollection: String =
                                    if (likedCollection.length >= 20) {
                                        likedCollection.substring(
                                            0,
                                            20
                                        ) + "..."
                                    } else {
                                        likedCollection
                                    }
                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " others."
                                } else
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " other."
                            } else {
                                binding.feedpostLikedby.textViewLikedby.text = likedCollection
                            }
                        }
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }
            } else {
                binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
            }

            if (mFeedItem.action?.post?.lastComment != null) {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                binding.feedpostLastComment.textviewLastcommentUsername.text =
                    mFeedItem.action?.post?.lastComment?.profile?.name
                settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)

            } else {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
            }

            when (mFeedItem.actor?.id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
            }
            binding.followingTextView.setOnClickListener {

                if (mFeedItem.actor?.relation == "following") {
                    val dialogBuilder = this.let {
                        androidx.appcompat.app.AlertDialog.Builder(
                            mContext!!
                        )
                    }
                    val inflater = mContext?.layoutInflater
                    val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                        b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        mFeedItem.actor?.relation ?: "",
                        mFeedItem.actor?.id ?: ""
                    )
                    Handler().postDelayed({
                        notifyFollowing(
                            adapterPosition,
                            mFeedItem.actor?.privateProfile
                        )
                    }, 100)
                }


            }

            binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                if (
                    mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.id != null &&
                    mFeedItem.action?.post?.own != null
                ) {
                    feedItemClick.onFeedItemClick(
                        mFeedItem.action?.post?.id!!,
                        "image"
                    )
                }
            }

            binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    mFeedItem.action?.post?.id!!,
                    mFeedItem?.action?.post?.likesCount!!
                )
            }

            // TODO : Double TAP
            val gd = GestureDetector(mContext, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (itemCount > 0 && mFeedItem.action != null
                            && mFeedItem.action!!.post != null &&
                            mFeedItem.action?.post?.media != null &&
                            mFeedItem.action?.post?.media?.isNotEmpty()!!
                        ) {
                            feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "image")
                        }
                        /*{
                                val dialog: Dialog?
                                dialog = Dialog(mContext!!, R.style.You_Dialog)
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                dialog.setContentView(R.layout.detailedimage_view_pager)
    //                        dialog.setContentView(R.layout.detailedimage_new)
    //                        dialog.setCancelable(false)
                                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                val window = dialog.window
                                window?.setLayout(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT
                                )

                                val mMedia = mFeedItem.action?.post?.media

                                dialog.view_pager.adapter =
                                    ImageDetailsAdapter(mContext, mMedia!!, glideRequestManager)
                                dialog.view_pager.currentItem = binding.imageListpager.currentItem
                                val url = mFeedItem.actor?.pimage
                                try {
                                    if (url != null && url != "" && !url.contains("googleusercontent")) {
                                        glideRequestManager.load(
                                            RemoteConstant.getImageUrlFromWidth(
                                                url,
                                                500
                                            )
                                        )
                                            .thumbnail(0.1f)
                                            .apply(
                                                RequestOptions.placeholderOf(R.drawable.emptypicture)
                                                    .error(
                                                        R.drawable.emptypicture
                                                    ).centerCrop()
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(
                                                        Priority.HIGH
                                                    )
                                            )
                                            .into(dialog.profile_image_view)
                                    } else {
                                        if (url != null && url != "") {
                                            if (url.contains(".jpg")) {
                                                val str = url.replace("s96-c/photo.jpg", "")
                                                val str2 = str + "s700-c/photo.jpg"
                                                glideRequestManager.load(str2).thumbnail(0.1f)
                                                    .apply(
                                                        RequestOptions.placeholderOf(R.drawable.emptypicture)
                                                            .error(
                                                                R.drawable.emptypicture
                                                            ).centerCrop()
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .priority(
                                                                Priority.HIGH
                                                            )
                                                    )
                                                    .into(dialog.profile_image_view)
                                            } else {
                                                val str = url.replace("s96-c", "")
                                                val str2 = str + "s700-c"
                                                glideRequestManager.load(str2).thumbnail(0.1f)
                                                    .apply(
                                                        RequestOptions.placeholderOf(R.drawable.emptypicture)
                                                            .error(
                                                                R.drawable.emptypicture
                                                            ).centerCrop()
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .priority(
                                                                Priority.HIGH
                                                            )
                                                    )
                                                    .into(dialog.profile_image_view)
                                            }
                                        }
                                    }


                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }


                                if (mFeedItem.actor?.username != null && mFeedItem.actor?.username != ""
                                ) {
                                    val mTitle: String =
                                        if (mFeedItem.actor?.username?.length!! >= 10) {
                                            mFeedItem.actor?.username?.substring(
                                                0,
                                                10
                                            ) + "..."
                                        } else {
                                            mFeedItem.actor?.username!!
                                        }
                                    dialog.profile_feed_name_text_view.text = mTitle
                                }

                                dialog.close_image_view_alert_view_pager?.bringToFront()

                                dialog.close_image_view_alert_view_pager?.setOnClickListener {
                                    dialog.dismiss()
                                }
                                dialog.show()
                            }*/
                    }
                    return super.onSingleTapConfirmed(e)
                }

                override fun onDown(e: MotionEvent): Boolean {
                    return true
                }

                override fun onDoubleTap(e: MotionEvent): Boolean {


                    // TODO : Animation Heart
                    binding.heartAnim.bringToFront()
                    binding.heartAnim.visibility = View.VISIBLE
                    binding.heartAnim.playAnimation()
                    if (mFeedItem.action?.post?.liked == false) {
                        binding.feedpostReview.likeMaterialButton.performClick()
                    }
                    binding.heartAnim.addAnimatorListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {
                            Log.e("Animation:", "start")

                        }

                        override fun onAnimationEnd(animation: Animator) {
                            Log.e("Animation:", "end")
                            binding.heartAnim.visibility = View.GONE

                        }

                        override fun onAnimationCancel(animation: Animator) {
                            Log.e("Animation:", "cancel")
                        }

                        override fun onAnimationRepeat(animation: Animator) {
                            Log.e("Animation:", "repeat")
                            binding.heartAnim.pauseAnimation()
                            binding.heartAnim.visibility = View.GONE
                        }
                    })


                    if (mFeedItem.action?.post?.liked == false) {
                        binding.feedpostReview.likeMaterialButton.performClick()
                    }

                    return true
                }


                override fun onDoubleTapEvent(e: MotionEvent): Boolean {
                    return true
                }
            })

            binding.imageListpager.setOnTouchListener { v, event -> gd.onTouchEvent(event) }

            if (mFeedItem.action?.post?.media?.isNotEmpty()!!) {

                feedImageListAdapterInner = FeedImageListAdapterInner(
                    mFeedItem.action?.post?.media,
                    mContext!!, "", glideRequestManager
                )
                binding.imageListpager.adapter = feedImageListAdapterInner
                when (mFeedItem.action?.post?.media!![0].ratio) {
                    "1:1" -> {
                        binding.aspectRatioLayout.setAspectRatio(1F, 1F)
                    }
                    "3:4" -> {
                        binding.aspectRatioLayout.setAspectRatio(3F, 4F)
                    }
                    "4:3" -> {
                        binding.aspectRatioLayout.setAspectRatio(4F, 3F)
                    }
                    "16:9" -> {
                        binding.aspectRatioLayout.setAspectRatio(16F, 9F)
                    }
                    "9:16" -> {
                      //  binding.aspectRatioLayout.setAspectRatio(9F, 16F)
                        val valString = getScreenResolution(mContext.applicationContext!!)
                        if (valString!! < 1800) {
                            binding.aspectRatioLayout.setAspectRatio(4F, 4F)
                        } else {
                            binding.aspectRatioLayout.setAspectRatio(4F, 5F)
                        }
                    }
                    else -> {
                        binding.aspectRatioLayout.setAspectRatio(1F, 1F)
                    }
                }
                if (mFeedItem.action?.post?.media?.size!! > 1) {
                    binding.countTextView.visibility = View.VISIBLE

                    binding.imagePosition = "" + 1 + "/" +
                            mFeedItem.action?.post?.media?.size
                    binding.imageListpager.addOnPageChangeListener(object :
                        androidx.viewpager.widget.ViewPager.OnPageChangeListener {

                        override fun onPageScrollStateChanged(state: Int) {
                        }

                        override fun onPageScrolled(
                            position: Int,
                            positionOffset: Float,
                            positionOffsetPixels: Int
                        ) {

                        }

                        override fun onPageSelected(position: Int) {
                            if (mFeedItem.action != null &&
                                mFeedItem.action?.post != null &&
                                mFeedItem.action?.post?.media != null &&
                                mFeedItem.action?.post?.media?.isNotEmpty()!!
                            ) {
                                binding.imagePosition = "" + position.plus(1) + "/" +
                                        mFeedItem.action?.post?.media?.size
                            }
                        }

                    })
                } else {
                    binding.countTextView.visibility = View.GONE
                }
            }
            // TODO : FOLLOW?FOLLOWING IN REEL BOARD


            if (mFeedItem.actor != null && mFeedItem.actor!!.relation != null
                && mFeedItem.actor!!.relation != "" && mFeedItem.actor!!.own != null
                && mFeedItem.actor!!.own != "" && mFeedItem.actor!!.own != "true"
            ) {
//                binding.titleTextView.visibility = View.VISIBLE
                val reelBoardFollow = mFeedItem.actor!!.relation
                val isOwn = mFeedItem.actor!!.own
                var follow = ""
                var color = 0
                when (reelBoardFollow) {
                    "following" -> {
                        follow = "FOLLOWING"
                        color = Color.parseColor("#30ccb4")
                    }
                    "none" -> {
                        follow = "FOLLOW"
                        color = Color.parseColor("#1e90ff")
                    }
                    "request pending" -> {
                        follow = "REQUEST PENDING"
                        color = Color.parseColor("#FFFFFF")
                    }
                }
                if (isOwn != "true") {
                    binding.titleTextView.text = follow
                    binding.titleTextView.setTextColor(color)
                    if (follow == "FOLLOWING") {
                        binding.titleTextView.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_follow_check, 0, 0, 0
                        )
                    } else {
                        binding.titleTextView.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                    }
                }
            } else {
                binding.titleTextView.visibility = View.GONE
            }


            binding.feedpostReview.likeMaterialButton.setOnClickListener {
                // TODO : Viewpager scroll to 0
                // TODO:  Do not change it if notify it will reset adapter viewpager ( Image list - multiple item )
                var mCount = 0
                if (mFeedItem.action!!.post!!.liked == true) {
                    feedItemLikeClick.onFeedItemLikeClick(
                        mFeedItem.action!!.post!!.id!!,
                        "false"
                    )
                    binding.feedpostReview.likeMaterialButton.isFavorite = false
                    mCount =
                        Integer.parseInt(mFeedItem.action!!.post!!.likesCount!!)
                            .minus(1)
                    mFeedItem.action!!.post!!.likesCount = mCount.toString()
                    mFeedItem.action!!.post!!.liked = false
                    binding.feedpostReview.likeCountTextView.text = "$mCount Likes"
                } else {
                    feedItemLikeClick.onFeedItemLikeClick(
                        mFeedItem.action!!.post!!.id!!,
                        "true"
                    )

                    binding.feedpostReview.likeMaterialButton.isFavorite = true
                    mCount =
                        Integer.parseInt(mFeedItem.action!!.post!!.likesCount!!)
                            .plus(1)
                    mFeedItem.action!!.post!!.likesCount = mCount.toString()
                    mFeedItem.action!!.post!!.liked = true
                    binding.feedpostReview.likeCountTextView.text = "$mCount Likes"
                }

                if (mCount > 0) {
                    binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#1e90ff"))
                } else {
                    binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#747d8c"))
                }

            }
            binding.feedpostReview.commentLinear.setOnClickListener {
                /*feedCommentClick.onCommentClick(
                    mFeedItem.action!!.post!!.id!!, adapterPosition,
                    mFeedItem.action!!.post!!.own!!
                )*/
                feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "image")
            }

            binding?.tvViewAllComments?.setOnClickListener {
                /*feedCommentClick.onCommentClick(
                    mFeedItem.action!!.post!!.id!!, adapterPosition,
                    mFeedItem.action!!.post!!.own!!
                )*/
                feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "image")
            }

            binding.feedpostReview.likeLinear.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    mFeedItem.action?.post?.id!!,
                    mFeedItem.action?.post?.likesCount!!
                )
            }

            binding.feedpostReview.llshare.setOnClickListener {
                ShareAppUtils.sharePost(
                    mFeedItem.action?.post?.type,
                    mFeedItem.action?.post?.id!!, mContext!!
                )
            }

            binding.feedPostOptionImageView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    val viewGroup: ViewGroup? = null
                    val view: View =
                        LayoutInflater.from(mContext)
                            .inflate(R.layout.options_menu_feed_option, viewGroup)
                    val mQQPop = PopupWindow(
                        view,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    mQQPop.animationStyle = R.style.RightTopPopAnim
                    mQQPop.isFocusable = true
                    mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    mQQPop.isOutsideTouchable = true
                    mQQPop.showAsDropDown(binding.feedPostOptionImageView, -180, -20)

                    if (mFeedItem.action!!.post!!.own == true) {
                        view.edit_post.visibility = View.VISIBLE
                        view.delete_post.visibility = View.VISIBLE
                        //view.share_post.visibility = View.VISIBLE
                    } else {
                        view.edit_post.visibility = View.GONE
                        view.delete_post.visibility = View.GONE
                        //view.share_post.visibility = View.VISIBLE
                        view.report_post.visibility = View.VISIBLE
                    }
                    if (!isEditEnabled) {

                        if (mFeedItem.action != null && mFeedItem.action!!.post != null &&
                            mFeedItem.action!!.post!!.own != null
                        ) {
                            view.edit_post.visibility = View.GONE
                            view.delete_post.visibility = View.GONE
                        }
                    }
                    view.report_post.setOnClickListener {
                        mQQPop.dismiss()
                        val intent = Intent(mContext, HelpFeedBackReportProblemActivity::class.java)
                        intent.putExtra("mTitle", mContext!!.getString(R.string.report))
                        intent.putExtra("mPostId", mFeedItem.action!!.post!!.id)
                        mContext.startActivity(intent)
                    }
                    view.share_post.setOnClickListener {
                        ShareAppUtils.sharePost(
                            mFeedItem.action!!.post!!.type,
                            mFeedItem.action!!.post!!.id!!, mContext!!
                        )
                        mQQPop.dismiss()
                    }
                    view.edit_post.setOnClickListener {
                        ProfileFeedFragment.idPostPosition = adapterPosition
                        SharedPrefsUtils.setStringPreference(mContext, "POST_FROM", "PROFILE")
                        val intent = Intent(mContext, UploadPhotoActivityMentionHashTag::class.java)
                        intent.putExtra("mPostId", (mFeedItem.action?.post?.id!!))
                        intent.putExtra("mIsEdit", true)
                        mContext!!.startActivity(intent)
                        mQQPop.dismiss()
                    }
                    view.delete_post.setOnClickListener {


                        val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                        val inflater = mContext!!.layoutInflater
                        val dialogView = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b?.show()
                        okButton.setOnClickListener {
                            optionItemClickListener.onOptionItemClickOption(
                                "delete",
                                mFeedItem.action!!.post!!.id!!, adapterPosition
                            )

                            mQQPop.dismiss()
                            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            mQQPop.dismiss()
                            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }


                    }
                }
            }
            binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                for (i in mFeedItem.action!!.post!!.hashTags!!.indices) {
                    if (mFeedItem.action!!.post!!.hashTags!![i].name!!.equals(
                            text.toString(),
                            ignoreCase = true
                        )
                    ) {
                        if (mFeedItem.action!!.post!!.hashTags!![i]._id != null) {

                            if (!isCalledHashTgaDetailsPage) {
                                isCalledHashTgaDetailsPage = true
                                hashTagClickListener.onOptionItemClickOption(
                                    mFeedItem.action!!.post!!.hashTags!![i]._id!!,
                                    "",
                                    "hashTagFeed",
                                    mFeedItem.action!!.post!!.id!!,
                                    mFeedItem.action!!.post!!.likesCount!!,
                                    mFeedItem.action!!.post!!.commentsCount!!
                                )


                                Handler().postDelayed({
                                    isCalledHashTgaDetailsPage = false
                                }, 1000)
                            }


                        }
                    }
                }

            }
            binding.descriptionTextView.setOnMentionClickListener { view, text ->
                for (i in mFeedItem.action?.post?.mentions?.indices!!) {
                    if (mFeedItem.action?.post?.mentions?.get(i)?.text?.contains(
                            text
                        )!!
                    ) {
                        if (mFeedItem.action?.post?.mentions?.get(i)?._id != null) {

                            if (!isCalledHashTgaDetailsPage) {
                                isCalledHashTgaDetailsPage = true
                                val intent = Intent(mContext, UserProfileActivity::class.java)
                                intent.putExtra(
                                    "mProfileId",
                                    mFeedItem.action?.post?.mentions?.get(i)?._id
                                )
                                mContext?.startActivityForResult(intent, 0)
                                Handler().postDelayed({
                                    isCalledHashTgaDetailsPage = false
                                }, 1000)
                            }
                        }
                    }
                }
            }
            binding.profileFeedNameTextView.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding.profileImageView.setOnClickListener {
                val intent = Intent(mContext, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mFeedItem.actor!!.id)
                mContext!!.startActivity(intent)
            }
            binding?.feedImageLinear?.setOnClickListener {
                feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "image")
            }

        }
    }

    inner class ViewHolderItemMusic(val binding: LayoutItemMusicBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            var mFeedItem = list?.get(adapterPosition)!!
            binding.textViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (mFeedItem.action?.post?.ogTags != null &&
                    mFeedItem.action?.post?.ogTags?.id != null &&
                    mFeedItem.action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = mFeedItem.action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(mFeedItem.action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }

                    //TODO : Check description contains url
                    binding.typeTextTrackLinear.visibility = View.VISIBLE
                    binding.playMageView.visibility = View.VISIBLE
                    binding.trackImageView.visibility = View.VISIBLE

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (mFeedItem.action?.post?.likedConnections != null) {
                    if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                            if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."

                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }

                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + ", "

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE
                                    likedCollection =
                                        likedCollection + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + "."

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                mFeedItem.action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (mFeedItem.action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        mFeedItem.action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (mFeedItem.actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding.followingTextView.setOnClickListener {

                    if (mFeedItem.actor?.relation == "following") {
                        val dialogBuilder = this.let {
                            androidx.appcompat.app.AlertDialog.Builder(
                                mContext!!
                            )
                        }
                        val inflater = mContext?.layoutInflater
                        val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                mFeedItem.actor?.relation ?: "",
                                mFeedItem.actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    mFeedItem.actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem?.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (mFeedItem.action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }
                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action?.post?.type,
                        mFeedItem.action?.post?.id!!, mContext!!
                    )
                }
                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (mFeedItem.action != null
                            && mFeedItem.action?.post != null
                            && mFeedItem.action?.post?.ogTags != null
                            && mFeedItem.action?.post?.ogTags?.url != null
                            && mFeedItem.action?.post?.ogTags?.url != ""
                        ) {
                            if (mFeedItem.action?.post?.ogTags?.url!!.contains("http")) {

                                val url = mFeedItem.action?.post?.ogTags?.url
                                mHyperLinkUrl.onHyperLinkUrlClick(url!!)
                            }
                        } else {
                            if (retrieveLinks(mFeedItem.action?.post?.description!!) != "") {
                                try {
                                    goToLink(mFeedItem.action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            mFeedItem.action?.post?.hashTags!!, text,
                            mFeedItem.action?.post?.id!!,
                            mFeedItem.action?.post?.likesCount!!,
                            mFeedItem.action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(mFeedItem.action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(mFeedItem.actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition
                    )
                }
            }
        }
    }

    inner class ViewHolderItemMusicVideo(val binding: LayoutItemMusicVideoBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            var mFeedItem = list?.get(adapterPosition)!!
            binding.textViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (mFeedItem.action?.post?.ogTags != null &&
                    mFeedItem.action?.post?.ogTags?.id != null &&
                    mFeedItem.action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = mFeedItem.action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (mFeedItem.action?.post?.likedConnections != null) {
                    if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                            if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."

                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }

                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + ", "

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE
                                    likedCollection =
                                        likedCollection + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + "."

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                mFeedItem.action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (mFeedItem.action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        mFeedItem.action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (mFeedItem.actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding.followingTextView.setOnClickListener {

                    if (mFeedItem.actor?.relation == "following") {
                        val dialogBuilder = this.let {
                            androidx.appcompat.app.AlertDialog.Builder(
                                mContext!!
                            )
                        }
                        val inflater = mContext?.layoutInflater
                        val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                mFeedItem.actor?.relation ?: "",
                                mFeedItem.actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    mFeedItem.actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem?.action?.post?.likesCount!!
                    )
                }

                if (mFeedItem.action?.post?.ogTags?.meta?.socialmobMeta?.trackVideo?.artist != null) {
                    var mArtistName = ""
                    val artist =
                        mFeedItem.action?.post?.ogTags?.meta?.socialmobMeta?.trackVideo?.artist
                    for (i in 0 until artist?.size!!) {
                        mArtistName = if (artist.size == 1) {
                            artist[i].artistName!!
                        } else {
                            if (mArtistName != "") {
                                mArtistName + " , " + artist[i].artistName
                            } else {
                                artist[i].artistName!!
                            }
                        }
                    }
                    binding.artistDetailsTextView.text =
                        mArtistName

                }

                if (mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(mFeedItem.action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }

//                    //TODO : Check description contains url
//                    binding.typeTextTrackLinear.visibility = View.VISIBLE
//                    binding.playMageView.visibility = View.VISIBLE
//                    binding.trackImageView.visibility = View.VISIBLE

                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (mFeedItem.action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }
                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action?.post?.type,
                        mFeedItem.action?.post?.id!!, mContext!!
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (mFeedItem.action != null
                            && mFeedItem.action?.post != null
                            && mFeedItem.action?.post?.ogTags != null
                            && mFeedItem.action?.post?.ogTags?.url != null
                            && mFeedItem.action?.post?.ogTags?.url != ""
                        ) {
                            if (mFeedItem.action?.post?.ogTags?.url!!.contains("http")) {

                                val url = mFeedItem.action?.post?.ogTags?.url
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.data = Uri.parse(url)
                                mContext?.startActivity(intent)
                            }
                        } else {
                            if (retrieveLinks(mFeedItem.action?.post?.description!!) != "") {
                                try {
                                    goToLink(mFeedItem.action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            mFeedItem.action?.post?.hashTags!!, text,
                            mFeedItem.action?.post?.id!!,
                            mFeedItem.action?.post?.likesCount!!,
                            mFeedItem.action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(mFeedItem.action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(mFeedItem.actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition
                    )
                }
            }
        }
    }

    inner class ViewHolderItemProfile(val binding: LayoutItemProfileBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            var mFeedItem = list?.get(adapterPosition)!!
            binding.textViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (mFeedItem.action?.post?.ogTags != null &&
                    mFeedItem.action?.post?.ogTags?.id != null &&
                    mFeedItem.action?.post?.ogTags?.id != ""
                ) {
                    binding.textOgViewModel = mFeedItem.action?.post?.ogTags
                    binding.scrapLinear.visibility = View.VISIBLE
                } else {
                    binding.scrapLinear.visibility = View.GONE
                }

                if (mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.description != null
                ) {
                    val mDescription: String =
                        removeUrl(mFeedItem.action?.post?.description!!)
                    if (mDescription != "") {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                        binding.descriptionTextView.visibility = View.VISIBLE
                    } else {
                        binding.descriptionTextView.visibility = View.GONE
                    }
                } else {
                    binding.descriptionTextView.visibility = View.GONE
                    binding.descriptionTextView.text = ""
                }

                if (mFeedItem.action?.post?.likedConnections != null) {
                    if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                        var likedCollection: String = ""
                        for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                            if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE

                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE
                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name + "."

                                if (likedCollection.length >= 20) {
                                    likedCollection.substring(
                                        0,
                                        20
                                    ) + "..."
                                } else {
                                    likedCollection
                                }

                                binding.feedpostLikedby.textViewLikedby.text =
                                    likedCollection
                            } else {
                                if (i == 0) {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.GONE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                    }

                                    likedCollection =
                                        "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + ", "

                                } else {
                                    binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                        View.VISIBLE
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.GONE
                                    likedCollection =
                                        likedCollection + mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.Name + "."

                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                    }

                                    if (i > 2) {
                                        binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                            View.VISIBLE
                                        if (mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage != ""
                                        ) {
                                            glideRequestManager.load(
                                                mFeedItem.action?.post?.likedConnections?.get(
                                                    i
                                                )?.pimage
                                            ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        } else {
                                            glideRequestManager.load(R.drawable.emptypicture)
                                                .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                        }
                                    }
                                }

                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                    val mlikedCollection: String =
                                        if (likedCollection.length >= 20) {
                                            likedCollection.substring(
                                                0,
                                                20
                                            ) + "..."
                                        } else {
                                            likedCollection
                                        }
                                    if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " others."
                                    } else
                                        binding.feedpostLikedby.textViewLikedby.text =
                                            mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                                ?.minus(1)) + " other."
                                } else {
                                    binding.feedpostLikedby.textViewLikedby.text = likedCollection
                                }
                            }
                        }
                    } else {
                        binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }

                if (mFeedItem.action?.post?.lastComment != null) {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                    binding.feedpostLastComment.textviewLastcommentUsername.text =
                        mFeedItem.action?.post?.lastComment?.profile?.name
                    settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)
                } else {
                    binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
                }

                when (mFeedItem.actor?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followingTextView?.visibility = View.GONE
                    }
                }
                binding.followingTextView.setOnClickListener {

                    if (mFeedItem.actor?.relation == "following") {
                        val dialogBuilder = this.let {
                            androidx.appcompat.app.AlertDialog.Builder(
                                mContext!!
                            )
                        }
                        val inflater = mContext?.layoutInflater
                        val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                                mFeedItem.actor?.relation ?: "",
                                mFeedItem.actor?.id ?: ""
                            )
                            Handler().postDelayed({
                                notifyFollowing(
                                    adapterPosition,
                                    mFeedItem.actor?.privateProfile
                                )
                            }, 100)
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                                ?.performClick()

                        }
                    } else {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                    }


                }

                binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem?.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.likeMaterialButton.setOnClickListener {

                    if (mFeedItem.action?.post?.liked == true) {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "false"
                        )
                    } else {
                        feedItemLikeClick.onFeedItemLikeClick(
                            mFeedItem.action?.post?.id!!,
                            "true"
                        )
                    }
                    Handler().postDelayed({
                        notify(adapterPosition)
                    }, 100)
                }
                binding.feedpostReview.commentLinear.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }

                binding?.tvViewAllComments?.setOnClickListener {
                    if (
                        mFeedItem.action != null &&
                        mFeedItem.action?.post != null &&
                        mFeedItem.action?.post?.id != null &&
                        mFeedItem.action?.post?.own != null
                    ) {
                        feedCommentClick.onCommentClick(
                            mFeedItem.action?.post?.id!!, adapterPosition,
                            mFeedItem.action?.post?.own!!
                        )
                    }
                }
                binding.feedpostReview.likeLinear.setOnClickListener {
                    feedLikeListClick.onFeedLikeListClick(
                        mFeedItem.action?.post?.id!!,
                        mFeedItem.action?.post?.likesCount!!
                    )
                }

                binding.feedpostReview.llshare.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action?.post?.type,
                        mFeedItem.action?.post?.id!!, mContext!!
                    )
                }

                binding.scrapLinear.setOnClickListener {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
                        return@setOnClickListener
                    }
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (mFeedItem.action != null
                            && mFeedItem.action?.post != null
                            && mFeedItem.action?.post?.ogTags != null
                            && mFeedItem.action?.post?.ogTags?.url != null
                            && mFeedItem.action?.post?.ogTags?.url != ""
                        ) {
                            if (mFeedItem.action?.post?.ogTags?.url!!.contains("http")) {

                                val url = mFeedItem.action?.post?.ogTags?.url
                                mHyperLinkUrl.onHyperLinkUrlClick(url!!)
                            }
                        } else {
                            if (retrieveLinks(mFeedItem.action?.post?.description!!) != "") {
                                try {
                                    goToLink(mFeedItem.action?.post?.description!!)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                    mLastClickTime = SystemClock.elapsedRealtime()
                }

                binding.descriptionTextView.setOnClickListener {
                    binding.scrapLinear.performClick()
                }
                binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        hashTagClick(
                            mFeedItem.action?.post?.hashTags!!, text,
                            mFeedItem.action?.post?.id!!,
                            mFeedItem.action?.post?.likesCount!!,
                            mFeedItem.action?.post?.commentsCount!!, adapterPosition
                        )
                    }

                }
                binding.descriptionTextView.setOnMentionClickListener { view, text ->
                    mentionClick(mFeedItem.action?.post?.mentions, text)
                }
                binding.profileFeedNameTextView.setOnClickListener {
                    binding.profileImageView.performClick()
                }
                binding.profileImageView.setOnClickListener {
                    goToProfile(mFeedItem.actor?.id!!)
                }

                binding.feedPostOptionImageView.setOnClickListener {
                    feedOptions(
                        binding.feedPostOptionImageView, adapterPosition
                    )
                }
            }
        }
    }

    inner class VideoViewHolder(val binding: LayoutItemVideoExoBinding) :
        BaseViewHolder<Any>(binding.root),
        ToroPlayer {
        private val player = binding.player
        private var videoUri: Uri? = null
        private var helperExo: ExoPlayerViewHelper? = null
        var listener: ToroPlayer.EventListener? = null

        @SuppressLint("SetTextI18n")
        override fun bind(feedData: Any?) {
            val mFeedItem = list?.get(adapterPosition)!!
            val videoUrl = mFeedItem.action?.post?.media!![0].sourcePath

            if (videoUrl !== null) videoUri = Uri.parse(videoUrl)
            binding.videoViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (mFeedItem.action?.post?.media?.size!! > 0) {
                glideRequestManager
                    .load(mFeedItem.action?.post?.media!![0].thumbnail)
                    .thumbnail(0.1f)
                    .into(binding.mThumnail)
            }

            binding.heartAnim.visibility = View.GONE

            binding.imgVol.setOnClickListener {
                isMute = if (isMute) {
                    stopMusic()
                    helperExo?.volumeInfo = VolumeInfo(false, 1.0f)
                    binding.imgVol.setImageResource(R.drawable.ic_volume_up_grey_24dp)
                    false
                } else {
                    helperExo?.volumeInfo = VolumeInfo(false, 0.0f)
                    binding.imgVol.setImageResource(R.drawable.ic_volume_off_grey_24dp)
                    true
                }
            }

            if (mFeedItem.action?.post?.likedConnections != null) {
                if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                    var likedCollection: String = ""
                    for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                        if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                            binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                View.VISIBLE

                            binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                View.GONE
                            binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                View.GONE
                            if (mFeedItem.action?.post?.likedConnections?.get(
                                    i
                                )?.pimage != ""
                            ) {
                                glideRequestManager.load(
                                    mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage
                                ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                            } else {
                                glideRequestManager.load(R.drawable.emptypicture)
                                    .into(binding.feedpostLikedby.ivFeedPostLiker1)
                            }

                            likedCollection =
                                "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                    i
                                )?.Name + "."

                            if (likedCollection.length >= 20) {
                                likedCollection.substring(
                                    0,
                                    20
                                ) + "..."
                            } else {
                                likedCollection
                            }

                            binding.feedpostLikedby.textViewLikedby.text =
                                likedCollection
                        } else {
                            if (i == 0) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name

                            } else {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                }

                                if (i > 2) {
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.VISIBLE
                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    }
                                }
                            }

                            if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                val mlikedCollection: String =
                                    if (likedCollection.length >= 20) {
                                        likedCollection.substring(
                                            0,
                                            20
                                        ) + "..."
                                    } else {
                                        likedCollection
                                    }
                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " others."
                                } else
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " other."
                            } else {
                                binding.feedpostLikedby.textViewLikedby.text = likedCollection
                            }
                        }
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }
            } else {
                binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
            }

            if (mFeedItem.action?.post?.lastComment != null) {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                binding.feedpostLastComment.textviewLastcommentUsername.text =
                    mFeedItem.action?.post?.lastComment?.profile?.name

                settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)
            } else {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
            }

            when (mFeedItem.actor?.id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
            }
            binding.followingTextView.setOnClickListener {

                if (mFeedItem.actor?.relation == "following") {
                    val dialogBuilder = this.let {
                        androidx.appcompat.app.AlertDialog.Builder(
                            mContext!!
                        )
                    }
                    val inflater = mContext?.layoutInflater
                    val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                        b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        mFeedItem.actor?.relation ?: "",
                        mFeedItem.actor?.id ?: ""
                    )
                    Handler().postDelayed({
                        notifyFollowing(
                            adapterPosition,
                            mFeedItem.actor?.privateProfile
                        )
                    }, 100)
                }


            }

            binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                if (
                    mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.id != null &&
                    mFeedItem.action?.post?.own != null
                ) {
                    feedCommentClick.onCommentClick(
                        mFeedItem.action?.post?.id!!, adapterPosition,
                        mFeedItem.action?.post?.own!!
                    )
                }
            }

            binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    mFeedItem.action?.post?.id!!,
                    mFeedItem?.action?.post?.likesCount!!
                )
            }

            // TODO : Double TAP
            val gd = GestureDetector(mContext, object : GestureDetector.SimpleOnGestureListener() {
                override fun onDoubleTap(e: MotionEvent): Boolean {

//                    binding.backgroundDimmer.bringToFront()
                    binding.heartAnim.bringToFront()
                    binding.heartAnim.visibility = View.VISIBLE
//                    binding.backgroundDimmer.setBackgroundResource(R.color.shadowLight)
                    binding.heartAnim.playAnimation()
                    if (mFeedItem.action?.post?.liked == false) {
                        binding.feedpostReview.likeMaterialButton.performClick()
                    }
                    binding.heartAnim.addAnimatorListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {
                            Log.e("Animation:", "start")

                        }

                        override fun onAnimationEnd(animation: Animator) {
                            Log.e("Animation:", "end")
//                            binding.backgroundDimmer.setBackgroundResource(android.R.color.transparent)
                            binding.heartAnim.visibility = View.GONE

                        }

                        override fun onAnimationCancel(animation: Animator) {
                            Log.e("Animation:", "cancel")
                        }

                        override fun onAnimationRepeat(animation: Animator) {
                            Log.e("Animation:", "repeat")
                            binding.heartAnim.pauseAnimation()
                            binding.heartAnim.visibility = View.GONE
                        }
                    })

                    return true
                }
            })
            if (mFeedItem.action?.post?.media?.isNotEmpty()!!) {
                if (mFeedItem.action?.post?.media!![0].ratio != null) {
                    val mRatio = mFeedItem.action?.post?.media!![0].ratio
                    when (mRatio) {
                        "1:1" -> {
                            binding.mediaContainer.setAspectRatio(1F, 1F)
                        }
                        "4:3" -> {
                            binding.mediaContainer.setAspectRatio(4F, 3F)
                        }
                        "16:9" -> {
                            binding.mediaContainer.setAspectRatio(16F, 9F)
                        }
                        "9:16" -> {
                            //binding.mediaContainer.setAspectRatio(9F, 16F)
                            val valString = getScreenResolution(mContext?.applicationContext!!)
                            if (valString!! < 1800) {
                                binding.mediaContainer.setAspectRatio(4F, 4F)
                            } else {
                                binding.mediaContainer.setAspectRatio(4F, 5F)
                            }
                        }
                        else -> {
                            binding.mediaContainer.setAspectRatio(1F, 1F)
                        }
                    }
                }
            }

            itemView.setOnTouchListener { _, event ->
                gd.onTouchEvent(event)
            }

            binding.feedpostReview.likeMaterialButton.setOnClickListener {
                var mCount = 0
                if (mFeedItem.action?.post?.liked == true) {
                    feedItemLikeClick.onFeedItemLikeClick(
                        mFeedItem.action?.post?.id!!,
                        "false"
                    )
                    binding.feedpostReview.likeMaterialButton.isFavorite = false
                    mCount =
                        Integer.parseInt(mFeedItem.action?.post?.likesCount!!)
                            .minus(1)
                    mFeedItem.action?.post?.likesCount = mCount.toString()
                    mFeedItem.action?.post?.liked = false
                    binding.feedpostReview.likeCountTextView.text = "$mCount Likes"
                } else {
                    feedItemLikeClick.onFeedItemLikeClick(
                        mFeedItem.action?.post?.id!!,
                        "true"
                    )
                    binding.feedpostReview.likeMaterialButton.isFavorite = true
                    mCount =
                        Integer.parseInt(mFeedItem.action?.post?.likesCount!!)
                            .plus(1)
                    mFeedItem.action?.post?.likesCount = mCount.toString()
                    mFeedItem.action?.post?.liked = true
                    binding.feedpostReview.likeCountTextView.text = "$mCount Likes"
                }

                if (mCount > 0) {
                    binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#1e90ff"))
                } else {
                    binding.feedpostReview.likeCountTextView.setTextColor(Color.parseColor("#747d8c"))
                }
            }
            binding.feedpostReview.commentLinear.setOnClickListener {
                feedCommentClick.onCommentClick(
                    mFeedItem.action?.post?.id!!, adapterPosition,
                    mFeedItem.action?.post?.own!!
                )

                //feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "video")
            }

            binding?.tvViewAllComments?.setOnClickListener {
                feedCommentClick.onCommentClick(
                    mFeedItem.action?.post?.id!!, adapterPosition,
                    mFeedItem.action?.post?.own!!
                )
                //feedItemClick.onFeedItemClick(mFeedItem.action?.post?.id!!, "video")
            }
            binding.feedpostReview.likeLinear.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    mFeedItem.action?.post?.id!!,
                    mFeedItem.action?.post?.likesCount!!
                )
            }

            binding.feedpostReview.llshare.setOnClickListener {
                ShareAppUtils.sharePost(
                    mFeedItem.action?.post?.type,
                    mFeedItem.action?.post?.id!!, mContext!!
                )
            }

            binding.feedPostOptionImageView.setOnClickListener {
                val viewGroup: ViewGroup? = null
                val view: View =
                    LayoutInflater.from(mContext)
                        .inflate(R.layout.options_menu_feed_option, viewGroup)
                val mQQPop =
                    PopupWindow(
                        view,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                mQQPop.animationStyle = R.style.RightTopPopAnim
                mQQPop.isFocusable = true
                mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mQQPop.isOutsideTouchable = true
                mQQPop.showAsDropDown(binding.feedPostOptionImageView, -180, -20)


                if (mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post != null
                ) {
                    if (mFeedItem.action?.post?.own == true) {
                        view.edit_post.visibility = View.VISIBLE
                        view.delete_post.visibility = View.VISIBLE
                        //view.share_post.visibility = View.VISIBLE
                    } else {
                        view.edit_post.visibility = View.GONE
                        view.delete_post.visibility = View.GONE
                        //view.share_post.visibility = View.VISIBLE
                        view.report_post.visibility = View.VISIBLE
                    }
                }

                if (!isEditEnabled) {

                    if (mFeedItem.action != null && mFeedItem.action!!.post != null &&
                        mFeedItem.action!!.post!!.own != null
                    ) {
                        view.edit_post.visibility = View.GONE
                        view.delete_post.visibility = View.GONE
                    }
                }
                view.report_post.setOnClickListener {
                    mQQPop.dismiss()
                    val intent = Intent(mContext, HelpFeedBackReportProblemActivity::class.java)
                    intent.putExtra("mTitle", mContext?.getString(R.string.report))
                    intent.putExtra("mPostId", mFeedItem.action?.post?.id)
                    mContext?.startActivity(intent)
                }

                view.share_post.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action?.post?.type,
                        mFeedItem.action?.post?.id!!, mContext!!
                    )
                    mQQPop.dismiss()
                }


                view.edit_post.setOnClickListener {
                    ProfileFeedFragment.idPostPosition = adapterPosition
                    SharedPrefsUtils.setStringPreference(mContext, "POST_FROM", "PROFILE")
                    val intent = Intent(mContext, UploadPhotoActivityMentionHashTag::class.java)
                    intent.putExtra("mPostId", (mFeedItem.action?.post?.id!!))
                    intent.putExtra("mIsEdit", true)
                    mContext?.startActivity(intent)

                    mQQPop.dismiss()
                }
                view.delete_post.setOnClickListener {
                    val dialogBuilder =
                        this.let { androidx.appcompat.app.AlertDialog.Builder(mContext!!) }
                    val inflater = mContext?.layoutInflater
                    val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView?.findViewById(R.id.cancel_button)!!
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)!!

                    val b: androidx.appcompat.app.AlertDialog? = dialogBuilder.create()
                    b?.show()
                    okButton.setOnClickListener {
                        optionItemClickListener.onOptionItemClickOption(
                            "delete",
                            mFeedItem.action?.post?.id!!, adapterPosition
                        )
                        b?.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()
                        mQQPop.dismiss()
                    }
                    cancelButton.setOnClickListener {
                        mQQPop.dismiss()
                        b?.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()

                    }


                }
            }
            binding.descriptionTextView.setOnMentionClickListener { _, text ->
                //                AppUtils.showCommonToast(mContext, "" + text)
                for (i in mFeedItem.action?.post?.mentions?.indices!!) {
                    if (mFeedItem.action?.post?.mentions?.get(i)?.text?.contains(
                            text
                        )!!
                    ) {
                        if (mFeedItem.action?.post?.mentions?.get(i)?._id != null) {

                            if (!isCalledHashTgaDetailsPage) {
                                isCalledHashTgaDetailsPage = true
                                val intent = Intent(mContext, UserProfileActivity::class.java)
                                intent.putExtra(
                                    "mProfileId",
                                    mFeedItem.action?.post?.mentions?.get(i)?._id
                                )
                                mContext?.startActivityForResult(intent, 0)
                                Handler().postDelayed({
                                    isCalledHashTgaDetailsPage = false
                                }, 1000)
                            }
                        }
                    }
                }
            }
            binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                for (i in 0 until mFeedItem.action?.post?.hashTags!!.size) {
                    if (mFeedItem.action?.post?.hashTags!![i].name!!.equals(
                            text.toString(),
                            ignoreCase = true
                        )
                    ) {
                        if (mFeedItem.action?.post?.hashTags!![i]._id != null) {

                            if (!isCalledHashTgaDetailsPage) {
                                isCalledHashTgaDetailsPage = true
                                hashTagClickListener.onOptionItemClickOption(
                                    mFeedItem.action?.post?.hashTags!![i]._id!!,
                                    adapterPosition.toString(),
                                    "hashTagFeed",
                                    mFeedItem.action?.post?.id!!,
                                    mFeedItem.action?.post?.likesCount!!,
                                    mFeedItem.action?.post?.commentsCount!!
                                )
                                Handler().postDelayed({
                                    isCalledHashTgaDetailsPage = false
                                }, 1000)
                            }


                        }
                    }
                }

            }
            binding.profileFeedNameTextView.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding.profileImageView.setOnClickListener {
                val intent = Intent(mContext, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mFeedItem.actor?.id)
                mContext?.startActivity(intent)
            }

            if (mFeedItem.actor != null && mFeedItem.actor!!.relation != null
                && mFeedItem.actor!!.relation != "" && mFeedItem.actor!!.own != null
                && mFeedItem.actor!!.own != "" && mFeedItem.actor!!.own != "true"
            ) {
//                binding.titleTextView.visibility = View.VISIBLE
                val reelBoardFollow = mFeedItem.actor!!.relation
                val isOwn = mFeedItem.actor!!.own
                var follow = ""
                var color = 0
                when (reelBoardFollow) {
                    "following" -> {
                        follow = "FOLLOWING"
                        color = Color.parseColor("#30ccb4")
                    }
                    "none" -> {
                        follow = "FOLLOW"
                        color = Color.parseColor("#1e90ff")
                    }
                    "request pending" -> {
                        follow = "REQUEST PENDING"
                        color = Color.parseColor("#FFFFFF")
                    }
                }
                if (isOwn != "true") {
                    binding.titleTextView.text = follow
                    binding.titleTextView.setTextColor(color)

                    if (follow == "FOLLOWING") {
                        binding.titleTextView.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_follow_check,
                            0,
                            0,
                            0
                        )
                    } else {
                        binding.titleTextView.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                    }
                }
            } else {
                binding.titleTextView.visibility = View.GONE
            }


        }

        override fun getPlayerView() = player

        override fun getCurrentPlaybackInfo() = helperExo?.latestPlaybackInfo ?: PlaybackInfo()

        override fun initialize(container: Container, playbackInfo: PlaybackInfo) {
            if (helperExo == null) helperExo =
                ExoPlayerViewHelper(this, videoUri!!, null, MyApplication.config!!)
            if (listener == null) {
                listener = object : ToroPlayer.EventListener {
                    override fun onFirstFrameRendered() {
                        helper = helperExo
                        binding.player.post {
                            binding.player.visibility = View.VISIBLE
                            binding.player.bringToFront()
                            binding.imgVol.bringToFront()
                        }
                        binding.mThumnail.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE
//                        status.text = "First frame rendered"
                        if (!isMute) {
                            helperExo?.volumeInfo = VolumeInfo(false, 1.0f)
                            binding.imgVol.setImageResource(R.drawable.ic_volume_up_grey_24dp)
                        } else {
                            helperExo?.volumeInfo = VolumeInfo(false, 0.0f)
                            binding.imgVol.setImageResource(R.drawable.ic_volume_off_grey_24dp)
                        }

                    }

                    override fun onBuffering() {
//                        status.text = "Buffering"
                        binding.mThumnail.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.VISIBLE
                        binding.player.visibility = View.GONE
                    }

                    override fun onPlaying() {

                        binding.mThumnail.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        binding.player.visibility = View.VISIBLE
//                        status.text = "Playing"
                    }

                    override fun onPaused() {
//                        status.text = "Paused"
                        binding.mThumnail.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        binding.player.visibility = View.GONE
                    }

                    override fun onCompleted() {
                        binding.mThumnail.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE
                        binding.player.visibility = View.VISIBLE
//                        status.text = "Completed"
                    }

                }
                helperExo!!.addPlayerEventListener(listener!!)
            }
            helperExo!!.initialize(container, playbackInfo)
        }

        override fun play() {
            helperExo?.play()
        }

        override fun pause() {
            helperExo?.pause()
        }

        override fun isPlaying() = helperExo?.isPlaying ?: false

        override fun release() {
            if (listener != null) {
                helperExo?.removePlayerEventListener(listener)
                listener = null
            }
            helperExo?.release()
            helperExo = null
        }

        override fun wantsToPlay() = ToroUtil.visibleAreaOffset(this, itemView.parent) >= 0.65

        override fun getPlayerOrder() = adapterPosition
    }

    private fun stopMusic() {
        if (isMyServiceRunning(MediaServiceRadio::class.java)) {

            if (MyApplication.playlistManagerRadio != null &&
                MyApplication.playlistManagerRadio?.playlistHandler != null
            ) {
                MyApplication.playlistManagerRadio?.playlistHandler?.pause(true)
            }
        }
        if (isMyServiceRunning(MediaService::class.java)) {

            if (MyApplication.playlistManager != null && MyApplication.playlistManager?.playlistHandler != null) {
                MyApplication.playlistManager?.playlistHandler?.pause(true)
            }
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = mContext?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    private fun removeUrl(commentstr: String): String {
        var commentstr = commentstr
        val urlPattern =
            "((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)"
        val p = Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE)
        val m = p.matcher(commentstr)
        var i = 0
        while (m.find()) {
            commentstr = commentstr.replace(m.group(i).toRegex(), "").trim { it <= ' ' }
            i++
        }
        return commentstr
    }

    inner class ViewHolderItemText(val binding: LayoutItemTextBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(feedData: Any?) {
            val mFeedItem = list?.get(adapterPosition)!!
            binding.textViewProfileModel = mFeedItem
            binding.executePendingBindings()

            if (
                mFeedItem.action != null &&
                mFeedItem.action?.post != null &&
                mFeedItem.action?.post?.description != null
            ) {
                val mDescription: String =
                    removeUrl(mFeedItem.action?.post?.description!!)
                if (mDescription != "") {
                    if (mFeedItem.action != null
                        && mFeedItem.action?.post != null
                        && mFeedItem.action?.post?.ogTags != null
                        && mFeedItem.action?.post?.ogTags?.url != null
                        && mFeedItem.action?.post?.ogTags?.url != ""
                    ) {
                        binding.descriptionTextView.text = removeUrl(mDescription)
                    } else {
                        binding.descriptionTextView.text =
                            mFeedItem.action?.post?.description!!
                    }
                    binding.descriptionTextView.visibility = View.VISIBLE
                } else {
                    if (mFeedItem.action != null
                        && mFeedItem.action?.post != null
                        && mFeedItem.action?.post?.ogTags != null
                        && mFeedItem.action?.post?.ogTags?.url != null
                        && mFeedItem.action?.post?.ogTags?.url != ""
                    ) {
                        binding.descriptionTextView.visibility = View.GONE
                    } else {
                        binding.descriptionTextView.text =
                            mFeedItem.action?.post?.description!!
                        binding.descriptionTextView.visibility = View.VISIBLE
                    }
                }
            } else {
                binding.descriptionTextView.text = ""
            }

            if (mFeedItem.action?.post?.likedConnections != null) {
                if (mFeedItem.action?.post?.likedConnections?.size!! > 0) {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.VISIBLE
                    var likedCollection: String = ""
                    for (i in mFeedItem.action?.post?.likedConnections?.indices!!) {

                        if (mFeedItem.action?.post?.likedConnections?.size!! == 1) {
                            binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                View.VISIBLE

                            binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                View.GONE
                            binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                View.GONE
                            if (mFeedItem.action?.post?.likedConnections?.get(
                                    i
                                )?.pimage != ""
                            ) {
                                glideRequestManager.load(
                                    mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage
                                ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                            } else {
                                glideRequestManager.load(R.drawable.emptypicture)
                                    .into(binding.feedpostLikedby.ivFeedPostLiker1)
                            }

                            binding.feedpostLikedby.textViewLikedby.text =
                                "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                    i
                                )?.Name + "."
                        } else {
                            if (i == 0) {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.GONE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker1)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker1)
                                }

                                likedCollection =
                                    "Liked by " + mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.Name

                            } else {
                                binding.feedpostLikedby.cvFeedPostLiker1.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker2.visibility =
                                    View.VISIBLE
                                binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                    View.GONE

                                if (mFeedItem.action?.post?.likedConnections?.get(
                                        i
                                    )?.pimage != ""
                                ) {
                                    glideRequestManager.load(
                                        mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage
                                    ).into(binding.feedpostLikedby.ivFeedPostLiker2)
                                } else {
                                    glideRequestManager.load(R.drawable.emptypicture)
                                        .into(binding.feedpostLikedby.ivFeedPostLiker2)
                                }

                                if (i > 2) {
                                    binding.feedpostLikedby.cvFeedPostLiker3.visibility =
                                        View.VISIBLE
                                    if (mFeedItem.action?.post?.likedConnections?.get(
                                            i
                                        )?.pimage != ""
                                    ) {
                                        glideRequestManager.load(
                                            mFeedItem.action?.post?.likedConnections?.get(
                                                i
                                            )?.pimage
                                        ).into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    } else {
                                        glideRequestManager.load(R.drawable.emptypicture)
                                            .into(binding.feedpostLikedby.ivFeedPostLiker3)
                                    }
                                }
                            }

                            if (mFeedItem.action?.post?.likesCount?.toInt()!! > 1) {
                                val mlikedCollection: String =
                                    if (likedCollection.length >= 20) {
                                        likedCollection.substring(
                                            0,
                                            20
                                        ) + "..."
                                    } else {
                                        likedCollection
                                    }
                                if (mFeedItem.action?.post?.likesCount?.toInt()!! > 2) {
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " others."
                                } else
                                    binding.feedpostLikedby.textViewLikedby.text =
                                        mlikedCollection + " and " + (mFeedItem.action?.post?.likesCount?.toInt()
                                            ?.minus(1)) + " other."
                            } else {
                                binding.feedpostLikedby.textViewLikedby.text = likedCollection
                            }
                        }
                    }
                } else {
                    binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
                }
            } else {
                binding.feedpostLikedby.feedpostLikedby.visibility = View.GONE
            }

            if (mFeedItem.action?.post?.lastComment != null) {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.VISIBLE
                binding.feedpostLastComment.textviewLastcommentUsername.text =
                    mFeedItem.action?.post?.lastComment?.profile?.name
                settextMarquee(binding.feedpostLastComment.textviewLastcomment, mFeedItem.action?.post?.lastComment?.test)

            } else {
                binding.feedpostLastComment.clfeedPostComments.visibility = View.GONE
            }

            when (mFeedItem.actor?.id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followingTextView?.visibility = View.GONE
                }
            }
            binding.followingTextView.setOnClickListener {

                if (mFeedItem.actor?.relation == "following") {
                    val dialogBuilder = this.let {
                        androidx.appcompat.app.AlertDialog.Builder(
                            mContext!!
                        )
                    }
                    val inflater = mContext?.layoutInflater
                    val dialogView = inflater?.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView!!.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                            mFeedItem.actor?.relation ?: "",
                            mFeedItem.actor?.id ?: ""
                        )
                        Handler().postDelayed({
                            notifyFollowing(
                                adapterPosition,
                                mFeedItem.actor?.privateProfile
                            )
                        }, 100)
                        b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE)
                            ?.performClick()

                    }
                } else {
                    searchPeopleAdapterFollowItemClick.onSearchPeopleAdapterFollowItemClick(
                        mFeedItem.actor?.relation ?: "",
                        mFeedItem.actor?.id ?: ""
                    )
                    Handler().postDelayed({
                        notifyFollowing(
                            adapterPosition,
                            mFeedItem.actor?.privateProfile
                        )
                    }, 100)
                }


            }

            binding.feedpostLastComment.clfeedPostComments.setOnClickListener {
                if (
                    mFeedItem.action != null &&
                    mFeedItem.action?.post != null &&
                    mFeedItem.action?.post?.id != null &&
                    mFeedItem.action?.post?.own != null
                ) {

                    feedCommentClick.onCommentClick(
                        mFeedItem.action?.post?.id!!, adapterPosition,
                        mFeedItem.action?.post?.own!!
                    )
                    /*feedItemClick.onFeedItemClick(
                        mFeedItem.action?.post?.id!!,
                        "text"
                    )*/
                }
            }

            binding.feedpostLikedby.feedpostLikedby.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    mFeedItem.action?.post?.id!!,
                    mFeedItem?.action?.post?.likesCount!!
                )
            }

            if (mFeedItem.action?.post?.ogTags != null && mFeedItem.action?.post?.ogTags?.id != null
                && mFeedItem.action?.post?.ogTags?.id != ""
            ) {
                binding.textOgViewModel = mFeedItem.action?.post?.ogTags
                binding.scrapLinear.visibility = View.VISIBLE
            } else {
                binding.scrapLinear.visibility = View.GONE
            }


            // TODO : FOLLOW?FOLLOWING IN REEL BOARD


            if (mFeedItem.actor != null && mFeedItem.actor!!.relation != null
                && mFeedItem.actor!!.relation != "" && mFeedItem.actor!!.own != null
                && mFeedItem.actor!!.own != "" && mFeedItem.actor!!.own != "true"
            ) {
//                binding.titleTextView.visibility = View.VISIBLE
                val reelBoardFollow = mFeedItem.actor!!.relation
                val isOwn = mFeedItem.actor!!.own
                var follow = ""
                var color = 0
                when (reelBoardFollow) {
                    "following" -> {
                        follow = "FOLLOWING"
                        color = Color.parseColor("#30ccb4")
                    }
                    "none" -> {
                        follow = "FOLLOW"
                        color = Color.parseColor("#1e90ff")
                    }
                    "request pending" -> {
                        follow = "REQUEST PENDING"
                        color = Color.parseColor("#FFFFFF")
                    }
                }
                if (isOwn != "true") {
                    binding.titleTextView.text = follow
                    binding.titleTextView.setTextColor(color)

                    if (follow == "FOLLOWING") {
                        binding.titleTextView.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_follow_check,
                            0,
                            0,
                            0
                        )
                    } else {
                        binding.titleTextView.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0
                        )
                    }
                }
            } else {
                binding.titleTextView.visibility = View.GONE
            }

            binding.feedpostReview.likeMaterialButton.setOnClickListener {

                if (mFeedItem.action!!.post!!.liked == true) {
                    feedItemLikeClick.onFeedItemLikeClick(
                        mFeedItem.action!!.post!!.id!!,
                        "false"
                    )
                } else {
                    feedItemLikeClick.onFeedItemLikeClick(
                        mFeedItem.action!!.post!!.id!!,
                        "true"
                    )
                }
                Handler().postDelayed({
                    notify(adapterPosition)
                }, 100)
            }
            binding.feedpostReview.commentLinear.setOnClickListener {
                feedCommentClick.onCommentClick(
                    mFeedItem.action!!.post!!.id!!, adapterPosition,
                    mFeedItem.action!!.post!!.own!!
                )
            }

            binding?.tvViewAllComments?.setOnClickListener {
                feedCommentClick.onCommentClick(
                    mFeedItem.action!!.post!!.id!!, adapterPosition,
                    mFeedItem.action!!.post!!.own!!
                )
            }
            binding.feedpostReview.likeLinear.setOnClickListener {
                feedLikeListClick.onFeedLikeListClick(
                    mFeedItem.action?.post?.id!!,
                    mFeedItem.action?.post?.likesCount!!
                )
            }

            binding.feedpostReview.llshare.setOnClickListener {
                ShareAppUtils.sharePost(
                    mFeedItem.action?.post?.type,
                    mFeedItem.action?.post?.id!!, mContext!!
                )
            }

            binding.descriptionTextView.setOnClickListener {
                binding.scrapLinear.performClick()
            }
            binding.scrapLinear.setOnClickListener {
                if (mFeedItem.action != null && mFeedItem.action?.post != null
                    && mFeedItem.action?.post?.ogTags != null
                    && mFeedItem.action?.post?.ogTags?.url != null
                    && mFeedItem.action?.post?.ogTags?.url != ""
                ) {
                    if (mFeedItem.action?.post?.ogTags?.url!!.contains("http")) {
                        val url = mFeedItem.action?.post?.ogTags?.url
                        mPlayTrack.onHyperLinkClick(url!!)

                    } else {
                        AppUtils.showCommonToast(mContext!!, "Cant open url")
                    }
                } else {
                    if (retrieveLinks(mFeedItem.action!!.post!!.description!!) != "") {
                        if (mFeedItem.action != null && mFeedItem.action?.post != null &&
                            mFeedItem.action?.post?.ogTags != null &&
                            mFeedItem.action?.post?.ogTags?.url != null &&
                            mFeedItem.action?.post?.description != null
                        ) {
                            if (mFeedItem.action?.post?.ogTags?.url?.contains("http")!!) {
                                try {
                                    val url =
                                        retrieveLinks(mFeedItem.action?.post?.description!!)
                                    val intent = Intent(Intent.ACTION_VIEW)
                                    intent.data = Uri.parse(url)
                                    mContext!!.startActivity(intent)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                }
            }
            binding.feedPostOptionImageView.setOnClickListener {
                val viewGroup: ViewGroup? = null
                val view: View =
                    LayoutInflater.from(mContext)
                        .inflate(R.layout.options_menu_feed_option, viewGroup)
                val mQQPop = PopupWindow(
                    view,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                mQQPop.animationStyle = R.style.RightTopPopAnim
                mQQPop.isFocusable = true
                mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mQQPop.isOutsideTouchable = true
                mQQPop.showAsDropDown(binding.feedPostOptionImageView, -180, -20)

                if (mFeedItem.action!!.post!!.own == true) {
                    view.edit_post.visibility = View.VISIBLE
                    view.delete_post.visibility = View.VISIBLE
                    // view.share_post.visibility = View.VISIBLE
                } else {
                    view.edit_post.visibility = View.GONE
                    view.delete_post.visibility = View.GONE
                    //view.share_post.visibility = View.VISIBLE
                    view.report_post.visibility = View.VISIBLE
                }
                if (!isEditEnabled) {

                    if (mFeedItem.action != null && mFeedItem.action!!.post != null &&
                        mFeedItem.action!!.post!!.own != null
                    ) {
                        view.edit_post.visibility = View.GONE
                        view.delete_post.visibility = View.GONE
                    }
                }
                view.report_post.setOnClickListener {
                    mQQPop.dismiss()
                    val intent = Intent(mContext, HelpFeedBackReportProblemActivity::class.java)
                    intent.putExtra("mTitle", mContext!!.getString(R.string.report))
                    intent.putExtra("mPostId", mFeedItem.action!!.post!!.id)
                    mContext.startActivity(intent)
                }
                view.share_post.setOnClickListener {
                    ShareAppUtils.sharePost(
                        mFeedItem.action!!.post!!.type,
                        mFeedItem.action!!.post!!.id!!, mContext!!
                    )
                    mQQPop.dismiss()
                }

                view.edit_post.setOnClickListener {
                    ProfileFeedFragment.idPostPosition = adapterPosition
                    SharedPrefsUtils.setStringPreference(mContext, "POST_FROM", "PROFILE")
                    val intent = Intent(mContext, WritePostActivity::class.java)
                    intent.putExtra("mPostId", (mFeedItem.action!!.post!!.id!!))
                    intent.putExtra("mIsEdit", true)
                    mContext!!.startActivity(intent)

                    mQQPop.dismiss()
                }
                view.delete_post.setOnClickListener {

                    val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                    val inflater = mContext!!.layoutInflater
                    val dialogView = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b = dialogBuilder.create()
                    b?.show()
                    okButton.setOnClickListener {
                        if (adapterPosition != RecyclerView.NO_POSITION) {
                            optionItemClickListener.onOptionItemClickOption(
                                "delete",
                                mFeedItem.action!!.post!!.id!!, adapterPosition
                            )
                            mQQPop.dismiss()
                            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        }
                    }
                    cancelButton.setOnClickListener {
                        mQQPop.dismiss()
                        b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                    }


                }
            }
            binding.descriptionTextView.setOnHashtagClickListener { view, text ->
                for (i in mFeedItem.action!!.post!!.hashTags!!.indices) {
                    if (mFeedItem.action!!.post!!.hashTags!![i].name!!.equals(
                            text.toString(),
                            ignoreCase = true
                        )
                    ) {
                        if (mFeedItem.action!!.post!!.hashTags!![i]._id != null) {
                            if (!isCalledHashTgaDetailsPage) {
                                isCalledHashTgaDetailsPage = true
                                hashTagClickListener.onOptionItemClickOption(
                                    mFeedItem.action!!.post!!.hashTags!![i]._id!!,
                                    "",
                                    "hashTagFeed",
                                    mFeedItem.action!!.post!!.id!!,
                                    mFeedItem.action!!.post!!.likesCount!!,
                                    mFeedItem.action!!.post!!.commentsCount!!
                                )
                                Handler().postDelayed({
                                    isCalledHashTgaDetailsPage = false
                                }, 1000)
                            }


                        }
                    }
                }

            }
            binding.descriptionTextView.setOnMentionClickListener { view, text ->
                for (i in mFeedItem.action?.post?.mentions?.indices!!) {
                    if (mFeedItem.action?.post?.mentions?.get(i)?.text?.contains(
                            text
                        )!!
                    ) {
                        if (mFeedItem.action?.post?.mentions?.get(i)?._id != null) {

                            if (!isCalledHashTgaDetailsPage) {
                                isCalledHashTgaDetailsPage = true
                                val intent = Intent(mContext, UserProfileActivity::class.java)
                                intent.putExtra(
                                    "mProfileId",
                                    mFeedItem.action?.post?.mentions?.get(i)?._id
                                )
                                mContext?.startActivityForResult(intent, 0)
                                Handler().postDelayed({
                                    isCalledHashTgaDetailsPage = false
                                }, 1000)
                            }
                        }
                    }
                }
            }
            binding.profileFeedNameTextView.setOnClickListener {
                binding.profileImageView.performClick()
            }
            binding.profileImageView.setOnClickListener {
                val intent = Intent(mContext, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mFeedItem.actor!!.id)
                mContext!!.startActivity(intent)
            }
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    inner class AdViewHolder(val binding: AdmobItemBinding, val parent: ViewGroup) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            val adRequest = AdRequest.Builder()
//                .addTestDevice("6A55E8D9F9E755271441D0891D626792")
                .build()
            binding.root.adViewArticle.loadAd(adRequest)

        }
    }


    inner class UploadProgressViewHolder(val binding: UploadProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            progressBar = binding.progressBar
            uploadTextView = binding.uploadTextView
        }

    }

    fun getUploadTextView(): TextView? {
        return uploadTextView
    }

    fun getProgressBar(): ProgressBar? {
        return progressBar
    }

    private fun notify(adapterPosition: Int) {
        val mFeedItem = list?.get(adapterPosition)
        mFeedItem?.action!!.post!!.liked =
            mFeedItem.action!!.post!!.liked != true
        if (mFeedItem.action!!.post!!.liked == true) {
            val mCount =
                Integer.parseInt(mFeedItem.action!!.post!!.likesCount!!).plus(1)
            mFeedItem.action!!.post!!.likesCount = mCount.toString()
            mFeedItem.action!!.post!!.liked = true
        } else {
            val mCount =
                Integer.parseInt(mFeedItem.action!!.post!!.likesCount!!).minus(1)
            mFeedItem.action!!.post!!.likesCount = mCount.toString()
            mFeedItem.action!!.post!!.liked = false
        }
        notifyItemChanged(adapterPosition, itemCount)
    }

    interface FeedItemLikeClick {
        fun onFeedItemLikeClick(mPostId: String, isLike: String)
    }

    interface FeedCommentClick {
        fun onCommentClick(mPostId: String, mPosition: Int, mIsMyPost: Boolean)
    }

    interface FeedLikeListClick {
        fun onFeedLikeListClick(mPostId: String, mLikeCount: String)
    }

    interface OptionItemClickListener {
        fun onOptionItemClickOption(mMethod: String, mId: String, mPos: Int)
    }

    interface FeedItemClick {
        fun onFeedItemClick(mPostId: String, mType: String)
    }

    interface HashTagClickListener {
        fun onOptionItemClickOption(
            mId: String,
            mPosition: String,
            mPath: String,
            mPostId: String,
            mLikeCount: String,
            mCommentCount: String
        )
    }

    interface AdapterFollowItemClick {
        fun onAdapterFollowItemClick(isFollowing: String, mId: String)
    }

    interface HyperLink {
        fun onHyperLinkClick(mUrl: String)
    }

    interface HyperLinkUrl {
        fun onHyperLinkUrlClick(mUrl: String)
    }

    fun retrieveLinks(text: String): String {
        try {
            val links: MutableList<String> = ArrayList()

            val regex =
                "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"
            val p = Pattern.compile(regex)
            val m = p.matcher(text)
            while (m.find()) {
                var urlStr = m.group()
                urlStr.toCharArray()

                if (urlStr.startsWith("(") && urlStr.endsWith(")")) {

                    val stringArray = urlStr.toCharArray()

                    val newArray = CharArray(stringArray.size - 2)
                    System.arraycopy(stringArray, 1, newArray, 0, stringArray.size - 2)
                    urlStr = String(newArray)
                    println("Finally Url =$newArray")

                }
                println("...Url...$urlStr")
                links.add(urlStr)
            }
            return links[0]
        } catch (e: Exception) {
            try {
                val links: MutableList<String> = ArrayList()
                val m = Patterns.WEB_URL.matcher(text)
                while (m.find()) {
                    val url = m.group()
                    links.add(url)
                }
                return links[0]
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return ""
    }

    private fun goToLink(description: String) {
        val url =
            retrieveLinks(description)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        mContext?.startActivity(intent)

    }

    private fun hashTagClick(
        hashTags: List<HashTag>, text: CharSequence, id: String,
        likesCount: String, commentsCount: String, adapterPosition: Int
    ) {
        for (i in hashTags.indices) {
            if (hashTags[i].name!!.equals(text.toString(), ignoreCase = true)) {
                if (hashTags[i]._id != null) {
                    if (!isCalledHashTgaDetailsPage) {
                        isCalledHashTgaDetailsPage = true
                        hashTagClickListener.onOptionItemClickOption(
                            hashTags[i]._id!!, adapterPosition.toString(), "hashTagFeed",
                            id, likesCount, commentsCount
                        )
                    }
                    Handler().postDelayed({
                        isCalledHashTgaDetailsPage = false
                    }, 1000)
                }
            }

        }

    }

    private fun goToProfile(id: String) {
        val intent = Intent(mContext, UserProfileActivity::class.java)
        intent.putExtra("mProfileId", id)
        mContext?.startActivity(intent)
    }

    private fun mentionClick(
        mentions: List<PostFeedDataMention>?,
        text: CharSequence
    ) {
        for (i in mentions?.indices!!) {
            if (mentions[i].text!!.contains(text)) {
                if (mentions[i]._id != null) {
                    val intent = Intent(mContext, UserProfileActivity::class.java)
                    intent.putExtra(
                        "mProfileId",
                        mentions[i]._id
                    )
                    mContext?.startActivity(intent)
                }
            }
        }
    }

    private fun feedOptions(
        feedPostOptionImageView: ImageView, adapterPosition: Int
    ) {

        val viewGroup: ViewGroup? = null
        val view: View =
            LayoutInflater.from(mContext)
                .inflate(R.layout.options_menu_feed_option, viewGroup)
        val mQQPop = PopupWindow(
            view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        mQQPop.animationStyle = R.style.RightTopPopAnim
        mQQPop.isFocusable = true
        mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mQQPop.isOutsideTouchable = true
        mQQPop.showAsDropDown(feedPostOptionImageView, -180, -20)
        var mFeedItem = list?.get(adapterPosition) as PersonalFeedResponseModelPayloadFeed
        if (mFeedItem.action!!.post!!.own == true) {
            view.edit_post.visibility = View.VISIBLE
            view.delete_post.visibility = View.VISIBLE
            //view.share_post.visibility = View.VISIBLE
        } else {
            view.edit_post.visibility = View.GONE
            view.delete_post.visibility = View.GONE
            //view.share_post.visibility = View.VISIBLE
            view.report_post.visibility = View.VISIBLE
        }
        if (!isEditEnabled) {

            if (mFeedItem.action != null && mFeedItem.action!!.post != null &&
                mFeedItem.action!!.post!!.own != null
            ) {
                view.edit_post.visibility = View.GONE
                view.delete_post.visibility = View.GONE
            }
        }
        view.report_post.setOnClickListener {
            mQQPop.dismiss()
            val intent = Intent(mContext, HelpFeedBackReportProblemActivity::class.java)
            intent.putExtra("mTitle", mContext!!.getString(R.string.report))
            intent.putExtra("mPostId", mFeedItem.action!!.post!!.id)
            mContext.startActivity(intent)
        }
        view.share_post.setOnClickListener {
            ShareAppUtils.sharePost(
                mFeedItem.action!!.post!!.type,
                mFeedItem.action!!.post!!.id!!, mContext!!
            )
            mQQPop.dismiss()
        }

        view.edit_post.setOnClickListener {
            ProfileFeedFragment.idPostPosition = adapterPosition
            SharedPrefsUtils.setStringPreference(mContext, "POST_FROM", "PROFILE")
            val intent = Intent(mContext, WritePostActivity::class.java)
            intent.putExtra("mPostId", (mFeedItem.action!!.post!!.id!!))
            intent.putExtra("mIsEdit", true)
            mContext!!.startActivity(intent)

            mQQPop.dismiss()
        }
        view.delete_post.setOnClickListener {

            val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
            val inflater = mContext!!.layoutInflater
            val dialogView = inflater.inflate(R.layout.common_dialog, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)

            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
            val okButton: Button = dialogView.findViewById(R.id.ok_button)

            val b = dialogBuilder.create()
            b?.show()
            okButton.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    optionItemClickListener.onOptionItemClickOption(
                        "delete",
                        mFeedItem.action!!.post!!.id!!, adapterPosition
                    )
                    mQQPop.dismiss()
                    b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                }
            }
            cancelButton.setOnClickListener {
                mQQPop.dismiss()
                b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

            }


        }
    }

    fun updateCommentCount(mPostPosition: Int, mCommentCount: String) {
        val mFeedItem = list?.get(mPostPosition) as PersonalFeedResponseModelPayloadFeed
        if (itemCount > 0 && mFeedItem.action != null &&
            mFeedItem.action?.post != null && mFeedItem.action?.post?.commentsCount != null
        ) {

            mFeedItem.action?.post?.commentsCount = mCommentCount
            notifyItemChanged(mPostPosition)
        }
    }


    private class FeedListDiffUtils(
        private val oldList: MutableList<PersonalFeedResponseModelPayloadFeed>,
        private val newList: MutableList<PersonalFeedResponseModelPayloadFeed>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].action?.actionId == newList[newItemPosition].action?.actionId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].action?.actionId == newList[newItemPosition].action?.actionId

        }
    }

    private fun notifyFollowing(adapterPosition: Int, privateProfile: Boolean?) {
        val mFeedItem = list?.get(adapterPosition) as PersonalFeedResponseModelPayloadFeed
        if (!privateProfile!!) {
            if (mFeedItem.actor?.relation == "following") {
                mFeedItem.actor?.relation = "none"
            } else {
                mFeedItem.actor?.relation = "following"
            }
        } else {
            when (mFeedItem.actor?.relation) {
                "none" -> mFeedItem.actor?.relation =
                    "request pending"
                "following" -> mFeedItem.actor?.relation =
                    "none"
                "request pending" -> mFeedItem.actor?.relation =
                    "none"
            }
        }

        notifyItemChanged(adapterPosition, itemCount)
    }

    private fun settextMarquee(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length >= 30) {
                value.substring(0, 30) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    /**
     * Screen Ratio
     */
    private fun getScreenResolution(context: Context): Int? {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val width: Int = metrics.widthPixels
        val height: Int = metrics.heightPixels
        return height
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

}