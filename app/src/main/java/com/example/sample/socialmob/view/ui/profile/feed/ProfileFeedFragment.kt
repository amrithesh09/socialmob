package com.example.sample.socialmob.view.ui.profile.feed

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ProfileFeedFragmentBinding
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPost
import com.example.sample.socialmob.model.profile.PostFeedData
import com.example.sample.socialmob.repository.utils.*
import com.example.sample.socialmob.view.ui.home_module.feed.*
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleAdapter
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.gallery.PhotoGridDetailsActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.view.utils.events.UploadStatus
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.profile_feed_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.runOnUiThread
import org.jsoup.Jsoup
import java.io.File
import java.net.URL
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFeedFragment : Fragment(), ProfileFeedAdapter.FeedItemLikeClick,
    ProfileFeedAdapter.FeedCommentClick, ProfileFeedAdapter.FeedLikeListClick,
    ProfileFeedAdapter.OptionItemClickListener, ProfileFeedAdapter.FeedItemClick,
    ProfileFeedAdapter.HashTagClickListener, ProfileFeedAdapter.AdapterFollowItemClick,
    ProfileFeedAdapter.HyperLink,
    ProgressRequestBody.UploadCallbacks, ProfileFeedAdapter.HyperLinkUrl, SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var customProgressDialog: CustomProgressDialog? = null
    private val viewModel: ProfileViewModel by viewModels()
    private var binding: ProfileFeedFragmentBinding? = null
    private var mId = ""
    private var mIsFromMyProfile = "false"
    private var feedList: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()
    private var profileFeedAdapter: ProfileFeedAdapter? = null
    private var isRefreshCalled = false
    private var isLoaderAdded = false
    private var mStatus: ResultResponse.Status? = null
    private var mLastClickTime: Long = 0
    private var RESPONSE_CODE = 1501

    companion object {

        private const val ARG_STRING = "mId"
        private const val ARG_BOOLEAN = "isFromMyProfile"

        var isUploadStarted: Boolean = false
        var mAddFeeData: PersonalFeedResponseModelPayloadFeedActionPost? = null
        var isCompletedVideoEdit: Boolean = false
        var EXPORT_STARTED: Boolean = false
        var titlePost: String = ""
        var mAuthPost: String = ""
        var idPostPosition: Int? = null
        var mAuthPostType: String = ""
        var mImageListPost: String = ""
        var mImageListPostList: ArrayList<GalleryData> = ArrayList()
        var mThumbListPost: ArrayList<File>? = ArrayList()
        var postDataPost = PostFeedData()
        var ratio = ""
        var isAddedFeed: Boolean = false
        var isUpdatedFeed: Boolean = false
        var idPost: String = ""

        fun newInstance(mId: String, isFromMyProfile: String): ProfileFeedFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mId)
            args.putSerializable(ARG_BOOLEAN, isFromMyProfile)
            val fragment = ProfileFeedFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        //add this code to pause videos (when app is minimised or paused)
        EventBus.getDefault().unregister(this)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UploadStatus) {
        if (isCompletedVideoEdit && !isUploadStarted) {
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (EXPORT_STARTED) {
            MyProfileActivity.isUploading = true
            uploadTopLoader()
            if (activity != null && activity is MyProfileActivity) {
                try {
                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        (activity as MyProfileActivity).getView().visibility = View.GONE
                    }, 1000)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            EXPORT_STARTED = false
        }

        if (isAddedFeed && titlePost != "" && isCompletedVideoEdit) {
//            binding?.swipeToRefreshFeed?.isEnabled = false
            if (InternetUtil.isInternetOn()) {
                if (activity != null && activity is MyProfileActivity) {
                    try {
                        (activity as MyProfileActivity).getView().visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isRefreshCalled = false
                uploadPost()
            } else {
                AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
            }
            isAddedFeed = false
            mAddFeeData = null
            titlePost = ""
            isUploadStarted = false
        } else if (mAddFeeData != null && !isUpdatedFeed) {
            isRefreshCalled = false
            isAddedFeed = false
            mAddFeeData = null
            titlePost = ""

            if (idPost != "") {
                if (feedList?.size ?: 0 > 0) {
                    feedList?.removeAt(0)
                    binding?.profileFeedRecyclerView?.post {
                        profileFeedAdapter?.submitList(feedList ?: ArrayList())
                    }
                }
                showDialogDoYouWantSeePost(idPost, true, mAuthPostType)
            }
            isUploadStarted = false
        } else if (isUpdatedFeed) {

            if (mAddFeeData != null && mAddFeeData?.id != null && feedList != null &&
                idPostPosition != null &&
                feedList?.get(idPostPosition!!) != null &&
                feedList?.get(idPostPosition!!)?.action != null &&
                feedList?.get(idPostPosition!!)?.action?.post != null
            ) {
                feedList?.get(idPostPosition!!)?.action?.post = mAddFeeData
                profileFeedAdapter?.notifyItemChanged(idPostPosition!!)
                isAddedFeed = false
                mAddFeeData = null
                titlePost = ""
                isUpdatedFeed = false
                idPostPosition = null
                isUploadStarted = false
            }
        }
    }

    private fun uploadTopLoader() {

        val mListSize = feedList?.size
        val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = java.util.ArrayList()
        val mList = PersonalFeedResponseModelPayloadFeed()
        mList.isLoader = "upload"
        loaderItem.add(mList)
        feedList?.addAll(0, loaderItem)
        if (mListSize == 0) {
            binding?.profileFeedRecyclerView?.post {
                profileFeedAdapter?.submitList(feedList ?: ArrayList())
            }

            binding?.progressLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.GONE
            binding?.noDataLinear?.visibility = View.GONE
            binding?.profileFeedRecyclerView?.visibility = View.VISIBLE
            binding?.dataLinear?.visibility = View.VISIBLE

        } else {
            binding?.profileFeedRecyclerView?.post {
                profileFeedAdapter?.submitList(feedList ?: ArrayList())
            }
        }

        if (profileFeedAdapter?.getUploadTextView() != null) {
            val uploadTextView = profileFeedAdapter?.getUploadTextView()
            uploadTextView?.text = "Uploading"
        }
        if (profileFeedAdapter?.getProgressBar() != null) {
            val mProgressBar = profileFeedAdapter?.getProgressBar()
            if (mProgressBar?.isIndeterminate == false) {
                mProgressBar.isIndeterminate = true
                profileFeedAdapter?.notifyItemChanged(0)
            }
        }
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            binding?.profileFeedRecyclerView?.smoothScrollToPosition(0)
        }, 1000)
    }

    @SuppressLint("SetTextI18n")
    private fun uploadPost() {

        val mListSize = feedList?.size


        MyProfileActivity.isUploading = true
        if (feedList != null && feedList?.isNotEmpty()!! && feedList?.get(0)?.isLoader != "upload") {
            val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()
            val mList = PersonalFeedResponseModelPayloadFeed()
            mList.isLoader = "upload"
            loaderItem!!.add(mList)
            feedList?.addAll(0, loaderItem)

            if (mListSize == 0) {
                binding?.profileFeedRecyclerView?.post {
                    profileFeedAdapter?.submitList(feedList ?: ArrayList())
                }

                binding?.progressLinear?.visibility = View.GONE
                binding?.noInternetLinear?.visibility = View.GONE
                binding?.noDataLinear?.visibility = View.GONE
                binding?.profileFeedRecyclerView?.visibility = View.VISIBLE
                binding?.dataLinear?.visibility = View.VISIBLE

            } else {
                binding?.profileFeedRecyclerView?.post {
                    profileFeedAdapter?.submitList(feedList ?: ArrayList())
                }
            }
        }
        if (mAuthPostType == "video") {

            isUploadStarted = true
            try {


                if (profileFeedAdapter?.getUploadTextView() != null) {
                    val uploadTextView = profileFeedAdapter?.getUploadTextView()
                    if (uploadTextView?.text != "Compressing") {
                        uploadTextView?.text = "Compressing"
                    }
                }
                viewModel.getPreSignedUrl(
                    mAuthPost,
                    mAuthPostType,
                    mImageListPost,
                    mImageListPostList,
                    mThumbListPost,
                    postDataPost,
                    ratio,
                    this@ProfileFeedFragment
                )

            } catch (e: Exception) {
                feedList?.removeAt(0)
                binding?.profileFeedRecyclerView?.post {
                    profileFeedAdapter?.submitList(feedList ?: ArrayList())
                }
                AppUtils.showCommonToast(
                    requireActivity(), "Error occur please try again later"
                )
                e.printStackTrace()

            }

        } else {
            if (profileFeedAdapter?.getUploadTextView() != null) {
                val uploadTextView = profileFeedAdapter?.getUploadTextView()
                uploadTextView?.text = "Uploading"
            }
            if (profileFeedAdapter?.getProgressBar() != null) {
                val mProgressBar = profileFeedAdapter?.getProgressBar()
                if (mProgressBar?.isIndeterminate == false) {
                    mProgressBar.isIndeterminate = true
                    profileFeedAdapter?.notifyItemChanged(0)
                }
            }
            viewModel.getPreSignedUrl(
                mAuthPost,
                mAuthPostType,
                mImageListPost,
                mImageListPostList,
                mThumbListPost,
                postDataPost, ratio, this
            )
        }

        viewModel.isUploaded.nonNull().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (!viewModel.isUploading) {
                if (it == true) {
                    MyProfileActivity.isUploading = false
                    try {
                        (activity as MyProfileActivity).isVisibleBackDismissDialog()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (idPost != "") {
                        showDialogDoYouWantSeePost(idPost, false, mAuthPostType)
                    }
                    titlePost = ""
                    mAuthPost = ""
                    mAuthPostType = ""
                    mImageListPost = ""
                    mImageListPostList = ArrayList()
                    mThumbListPost = ArrayList()
                    postDataPost = PostFeedData()
                } else {
                    feedList?.removeAt(0)
                    binding?.profileFeedRecyclerView?.post {
                        profileFeedAdapter?.submitList(feedList ?: ArrayList())
                    }
                    AppUtils.showCommonToast(
                        requireActivity(),
                        "Error occur please try again later"
                    )
                }
            }
            if (activity != null && activity is MyProfileActivity) {
                try {
                    (activity as MyProfileActivity).getView().visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            viewModel.isUploaded.value = null
        })
    }

    private fun showDialogDoYouWantSeePost(
        idPost: String, isFromWritePost: Boolean, mAuthPostType: String
    ) {
        if (!isFromWritePost) {
            feedList?.removeAt(0)
            binding?.profileFeedRecyclerView?.post {
                binding?.profileFeedRecyclerView?.smoothScrollToPosition(0)
                profileFeedAdapter?.submitList(feedList ?: ArrayList())
            }
        }
        if (feedList?.size == 0) {
            refresh()
        }

        val dialogBuilder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.common_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val okButton: Button = dialogView.findViewById(R.id.ok_button)
        val dialogDescription: TextView = dialogView.findViewById(R.id.dialog_description)
        dialogDescription.text = getString(R.string.want_to_see_post)

        cancelButton.text = getString(R.string.no)
        okButton.text = getString(R.string.yes)

        val b = dialogBuilder.create()
        b?.show()
        okButton.setOnClickListener {
            if (activity != null && activity is MyProfileActivity) {
                try {
                    (activity as MyProfileActivity).getView().visibility = View.VISIBLE
                } catch (e: Exception) {
                }
            }
            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            when {
                isFromWritePost -> {
                    val intent = Intent(activity, FeedTextDetailsActivity::class.java)
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)

                }
                mAuthPostType == "video" -> {
                    val intent = Intent(activity, FeedVideoDetailsActivity::class.java)
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)
                }
                else -> {
                    val intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    //intent.putExtra("mPostViewType", "")
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)
                }
            }

        }
        cancelButton.setOnClickListener {
            if (activity != null && activity is MyProfileActivity) {
                try {
                    (activity as MyProfileActivity).getView().visibility = View.VISIBLE
                } catch (e: Exception) {
                }
            }
            b?.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

        }

    }

    private fun refresh() {
        if (profileFeedAdapter != null && !isRefreshCalled) {
            isRefreshCalled = true
            binding?.profileFeedRecyclerView?.smoothScrollToPosition(0)
            val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
            val mList = PersonalFeedResponseModelPayloadFeed()
            loaderItem.add(mList)
            feedList?.addAll(0, loaderItem)
            binding?.profileFeedRecyclerView?.post {
                profileFeedAdapter?.submitList(feedList ?: ArrayList())
            }

            feedList?.clear()
            callApi(mId, mIsFromMyProfile)
            if (activity != null && activity is MyProfileActivity) {
                try {
                    (activity as MyProfileActivity).getView().visibility = View.VISIBLE
                } catch (e: Exception) {
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == RESPONSE_CODE) {
            if (data != null) {
                var mCommentCount: String? = ""
                if (data.hasExtra("COMMENT_COUNT")) {
                    mCommentCount = data.getStringExtra("COMMENT_COUNT")!!
                }
                val mPostPosition: Int
                if (data.hasExtra("POST_POSITION")) {
                    mPostPosition = data.getIntExtra("POST_POSITION", 0)

                    if (mCommentCount != null && mCommentCount != "") {
                        profileFeedAdapter?.updateCommentCount(mPostPosition, mCommentCount)
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.profile_feed_fragment, container, false)
        binding?.viewModel = viewModel

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments
        mId = args?.getString(ARG_STRING, "")!!
        mIsFromMyProfile = args.getString(ARG_BOOLEAN, "")!!

        // TODO : Check Internet connection
        callApiCommon(mId, mIsFromMyProfile)

        internet_refresh_text_view.setOnClickListener {
            feedList?.clear()
            callApiCommon(mId, mIsFromMyProfile)
        }

        // TODO : Added for pagination issue fix
        binding?.swipeToRefreshFeed?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefreshFeed?.isRefreshing = false
        binding?.swipeToRefreshFeed?.setOnRefreshListener {
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                feedList?.clear()
                callApiCommon(mId, mIsFromMyProfile)
                binding?.swipeToRefreshFeed?.isRefreshing = false
            }, 1000)
        }
        // TODO : Pagination
        binding?.profileFeedRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                requireActivity(),
                LinearLayoutManager.VERTICAL,
                false
            )
        binding?.profileFeedRecyclerView?.hasFixedSize()
        binding?.profileFeedRecyclerView?.addOnScrollListener(CustomScrollListener())

        profileFeedAdapter =
            ProfileFeedAdapter(
                activity, this, this, this,
                this, this, this,
                true, this, this, glideRequestManager, this
            )
        binding?.profileFeedRecyclerView?.adapter = profileFeedAdapter

        //TODO : Check if Track Hyperlink clicked ( From profile feed )
        viewModel.trackDetailsModel.nonNull()
            .observe(viewLifecycleOwner, Observer { trackDetailsModel ->
                if (trackDetailsModel?.payload != null && trackDetailsModel.payload?.track != null) {
                    val dbPlayList: MutableList<TrackListCommon> = ArrayList()
                    dbPlayList.add(trackDetailsModel.payload?.track!!)
                    viewModel.createDbPlayListModelNotification(
                        dbPlayList,"Suggested tracks"
                    )
                    val detailIntent = Intent(activity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                    detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                    startActivityForResult(detailIntent, 0)
                    activity?.overridePendingTransition(0, 0)
                    viewModel.trackDetailsModel.value = null
                } else {
                    AppUtils.showCommonToast(requireActivity(), "No track found")
                }
            })

        viewModel.podcastResponseModel?.observe(viewLifecycleOwner, { podCastResponseModel ->
            if (podCastResponseModel?.payload != null && podCastResponseModel.payload?.episode != null
            ) {
                val dbPlayList: MutableList<RadioPodCastEpisode> = ArrayList()
                dbPlayList.add(podCastResponseModel.payload?.episode!!)
                viewModel.createDbPlayListModelNotificationPodcast(
                    dbPlayList,"Suggested tracks"
                )
                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
                val detailIntent = Intent(activity, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                startActivity(detailIntent)
                activity?.overridePendingTransition(0, 0)
                viewModel.podcastResponseModel?.value = null
            } else {
                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }
        })
    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (feedList?.size!! >= 15 && mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                mStatus != ResultResponse.Status.LOADING &&
                mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
            ) {
                if (!isLoaderAdded) {
                    if (InternetUtil.isInternetOn()) {
                        if (mIsFromMyProfile == "true") {
                            loadMoreMyProfile(feedList?.get(feedList?.size!!.minus(1))?.action?.actionId!!)
                        } else {
                            loadMoreItemsUserProfile(feedList?.get(feedList?.size!!.minus(1))?.action?.actionId!!)
                        }
                    }
                }
            }
        }
    }

    private fun loadMoreMyProfile(size: String) {
        val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
        val mList = PersonalFeedResponseModelPayloadFeed()
        mList.isLoader = "loader"
        loaderItem.add(mList)
        feedList?.addAll(loaderItem)
        profileFeedAdapter?.submitList(feedList ?: ArrayList())

        val mAuth = RemoteConstant.getAuthPersonalFeed(
            activity, mId, size, RemoteConstant.mCount
        )
        viewModel.getPersonalFeed(
            mAuth, SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mProfileId, "")!!,
            size, RemoteConstant.mCount
        )
        isLoaderAdded = true
    }

    private fun loadMoreItemsUserProfile(size: String) {
        val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
        val mList = PersonalFeedResponseModelPayloadFeed()
        mList.isLoader = "loader"
        loaderItem.add(mList)
        feedList?.addAll(loaderItem)
        profileFeedAdapter?.submitList(feedList ?: ArrayList())
        val mAuth = RemoteConstant.getAuthPersonalFeed(
            activity,
            mId,
            size,
            RemoteConstant.mCount
        )
        viewModel.getPersonalFeed(mAuth, mId, size, RemoteConstant.mCount)
        isLoaderAdded = true
    }

    private fun callApiCommon(mId: String, mIsFromMyProfile: String) {
        if (!InternetUtil.isInternetOn()) {
            viewModel.mProfileFeedList?.postValue(ResultResponse.noInternet(ArrayList()))
            val shake: Animation = AnimationUtils.loadAnimation(activity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation

            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(binding?.noInternetImageView!!)
        } else {
            callApi(mId, mIsFromMyProfile)
        }
        observeData()
    }


    private fun observeData() {
        viewModel.mProfileFeedList?.nonNull()
            ?.observe(viewLifecycleOwner, Observer { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        val mLocalData = resultResponse?.data!!
                        feedList?.addAll(mLocalData)

                        binding?.profileFeedRecyclerView?.post {
                            profileFeedAdapter?.submitList(feedList ?: ArrayList())
                        }
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.profileFeedRecyclerView?.visibility = View.VISIBLE
                        binding?.dataLinear?.visibility = View.VISIBLE

                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                        try {
                            if (activity != null && activity is MyProfileActivity) {
                                (activity as MyProfileActivity).getView().visibility =
                                    View.VISIBLE
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {
                        val mLocalData = resultResponse?.data!!
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }
                        binding?.profileFeedRecyclerView?.post {
                            feedList?.addAll(mLocalData)
                            profileFeedAdapter?.submitList(feedList ?: ArrayList())
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED
                    }
                    ResultResponse.Status.ERROR -> {
                        if (feedList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.profileFeedRecyclerView?.visibility = View.GONE
                            binding?.dataLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        }
                    }
                    ResultResponse.Status.NO_DATA -> {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.profileFeedRecyclerView?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                        glideRequestManager.load(R.drawable.ic_no_feed).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.profileFeedRecyclerView?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })
    }

    private fun removeLoaderFormList() {
        if (feedList != null && feedList?.size!! > 0 && feedList?.get(
                feedList?.size!! - 1
            ) != null && feedList?.get(feedList?.size!! - 1)?.isLoader == "loader"
        ) {
            feedList?.removeAt(feedList?.size!! - 1)
            binding?.profileFeedRecyclerView?.post {
                profileFeedAdapter?.submitList(feedList ?: ArrayList())
            }
            isLoaderAdded = false
        }
    }

    private fun likePost(postId: String) {
        val mAuth = RemoteConstant.getAuthLikePost(activity, postId)
        viewModel.likePost(mAuth, postId)
    }

    private fun disLikePost(postId: String) {
        val mAuth = RemoteConstant.getAuthDislikePost(activity, postId)
        viewModel.disLikePost(mAuth, postId)
    }

    private fun callApi(mId: String, mIsFromMyProfile: String) {
        if (mIsFromMyProfile == "true") {
            val mAuth =
                RemoteConstant.getAuthPersonalFeed(
                    activity, mId, "0", RemoteConstant.mCount
                )
            viewModel.getPersonalFeed(
                mAuth, SharedPrefsUtils.getStringPreference(
                    activity, RemoteConstant.mProfileId, ""
                )!!, "0", RemoteConstant.mCount
            )
        } else {
            val mAuth =
                RemoteConstant.getAuthPersonalFeed(
                    activity, mId, "0", RemoteConstant.mCount
                )
            viewModel.getPersonalFeed(
                mAuth, mId, "0", RemoteConstant.mCount
            )
        }
    }

    override fun onFeedItemLikeClick(mPostId: String, isLike: String) {
        if (isLike == "true") {
            likePost(mPostId)
        } else {
            disLikePost(mPostId)

        }
    }

    override fun onCommentClick(mPostId: String, mPosition: Int, mIsMyPost: Boolean) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            val intent = Intent(activity, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mIsMyPost", mIsMyPost)
            intent.putExtra("mPosition", mPosition)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onFeedLikeListClick(mPostId: String, mLikeCount: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(activity, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onOptionItemClickOption(mMethod: String, mId: String, mPos: Int) {
        if (mMethod == "delete") {
            Handler().postDelayed({
                if (feedList != null && feedList?.get(mPos)?.action != null) {
                    feedList?.removeAt(mPos)
                    profileFeedAdapter?.submitList(feedList ?: ArrayList())
                    profileFeedAdapter?.notifyItemRemoved(mPos)
                    profileFeedAdapter?.notifyItemRangeChanged(mPos, feedList?.size?.minus(1)!!)

                    if (feedList?.size == 0) {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.profileFeedRecyclerView?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                        glideRequestManager.load(R.drawable.ic_no_feed).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                    }
                }
            }, 100)
            viewModel.deletePost(mId, requireActivity())
        }
    }

    override fun onFeedItemClick(mPostId: String, mType: String) {
        if (mType == "image") {
            val intent = Intent(activity, FeedImageDetailsActivity::class.java)
            //intent.putExtra("mPostViewType", "")
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        if (mType == "video") {
            val intent = Intent(activity, FeedVideoDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
        if (mType == "text") {
            val intent = Intent(activity, FeedTextDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, RESPONSE_CODE)
        }
    }

    override fun onAdapterFollowItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
            }
        } else {
            AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
        }
    }

    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        viewModel.unFollowProfile(mAuth, mId, "")
    }

    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        viewModel.followProfile(mAuth, mId)
    }

    override fun onOptionItemClickOption(
        mId: String, mPosition: String, mPath: String, mPostId: String, mLikeCount: String,
        mCommentCount: String
    ) {
        val detailIntent = Intent(activity, PhotoGridDetailsActivity::class.java)
        detailIntent.putExtra("mId", mId)
        detailIntent.putExtra("mPosition", mPosition)
        detailIntent.putExtra("mPath", mPath)
        detailIntent.putExtra("mPostId", mPostId)
        detailIntent.putExtra("mLikeCount", mLikeCount)
        detailIntent.putExtra("mCommentCount", mCommentCount)
        startActivityForResult(detailIntent, RESPONSE_CODE)
    }

    override fun onHyperLinkClick(mUrl: String) {

        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(activity)
        }
        runOnUiThread {
            customProgressDialog?.show()
        }
        var mType = ""
        var mId = ""
        var mTarget = ""
        var mPostType = ""

        try {
            doAsync {
                val htmlSrc = URL(mUrl).readText()
                val doc = Jsoup.parse(htmlSrc)
                val eMETA = doc.select("meta")

                for (i in 0 until eMETA.size) {
                    val name = eMETA[i].attr("name")
                    val content = eMETA[i].attr("content")

                    if (name == "pagetype") {
                        mType = content
                    }
                    if (name == "source") {
                        mId = content
                    }
                    if (name == "target") {
                        mTarget = content
                    }
                    if (name == "post-type") {
                        mPostType = content
                    }
                }

                if (mTarget == "socialmob") {
                    when (mType) {
                        "music" -> {
                            val mAuth =
                                RemoteConstant.getAuthTrackDetails(requireActivity(), mId)
                            viewModel.getTrackDetails(mAuth, mId)
                        }
                        "playlist" -> {
                            val intent = Intent(activity, CommonPlayListActivity::class.java)
                            intent.putExtra("mId", mId)
                            intent.putExtra("title", "")
                            intent.putExtra("imageUrl", "")
                            intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
                            startActivity(intent)
                        }
                        "podcast-episode" -> {
                            val fullData = RemoteConstant.getEncryptedString(
                                SharedPrefsUtils.getStringPreference(
                                    activity,
                                    RemoteConstant.mAppId,
                                    ""
                                )!!,
                                SharedPrefsUtils.getStringPreference(
                                    activity,
                                    RemoteConstant.mApiKey,
                                    ""
                                )!!,
                                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathPodCastEpisodeDetails
                                        + RemoteConstant.mSlash + mId, RemoteConstant.mGetMethod
                            )
                            val mAuth =
                                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                    activity, RemoteConstant.mAppId,
                                    ""
                                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                            viewModel.getPodCastDetails(mAuth, mId)
                        }
                        "podcast" -> {
                            val intent = Intent(activity, CommonPlayListActivity::class.java)
                            intent.putExtra("mId", mId)
                            intent.putExtra("title", "")
                            intent.putExtra("imageUrl", "")
                            intent.putExtra("mDescription", "")
                            intent.putExtra("mIsFrom", "fromRadio")
                            startActivity(intent)
                        }
                        "profile" -> {
                            val intent = Intent(activity, UserProfileActivity::class.java)
                            intent.putExtra("mProfileId", mId)
                            startActivity(intent)

                        }
                        "artist" -> {
                            val detailIntent = Intent(activity, ArtistPlayListActivity::class.java)
                            detailIntent.putExtra("mArtistId", mId)
                            startActivity(detailIntent)
                            activity?.overridePendingTransition(
                                R.anim.slide_in_up,
                                R.anim.slide_out_up
                            )
                        }
                        "post" -> {
                            when (mPostType) {
                                "user_text_post" -> {
                                    val intent =
                                        Intent(activity, FeedTextDetailsActivity::class.java)
                                    intent.putExtra("mPostId", mId)
                                    startActivity(intent)

                                }
                                "user_video_post" -> {
                                    val intent =
                                        Intent(activity, FeedVideoDetailsActivity::class.java)
                                    intent.putExtra("mPostId", mId)
                                    startActivity(intent)
                                }
                                "user_image_post" -> {
                                    val intent =
                                        Intent(activity, FeedImageDetailsActivity::class.java)
                                    //intent.putExtra("mPostViewType", "")
                                    intent.putExtra("mPostId", mId)
                                    startActivity(intent)
                                }
                            }
                        }
                    }

                } else {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(mUrl)
                    activity?.startActivity(intent)
                    activity?.overridePendingTransition(0, 0)
                }
                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                customProgressDialog?.dismiss()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onProgressUpdate(percentage: Int, timestamp: String?) {
        try {
            val uploadTextView = profileFeedAdapter?.getUploadTextView()
            if (uploadTextView?.text != "Uploading") {
                uploadTextView?.text = "Uploading"
            }
            val mProgressBar = profileFeedAdapter?.getProgressBar()
            mProgressBar?.progress = percentage
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onError() {

    }

    override fun onFinish() {

    }

    override fun onHyperLinkUrlClick(url: String) {
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(requireActivity())
        }
        runOnUiThread {
            customProgressDialog?.show()
        }
        try {
            var mType = ""
            var mId = ""
            var mTarget = ""
            var mPostType = ""
            doAsync {
                val htmlSrc = URL(url).readText()
                val doc = Jsoup.parse(htmlSrc)
                val eMETA = doc.select("meta")

                for (i in 0 until eMETA.size) {
                    val name = eMETA[i].attr("name")
                    val content = eMETA[i].attr("content")

                    if (name == "pagetype") {
                        mType = content
                    }
                    if (name == "source") {
                        mId = content
                    }
                    if (name == "target") {
                        mTarget = content
                    }
                    if (name == "post-type") {
                        mPostType = content
                    }
                }
                if (mTarget == "socialmob") {
                    when (mType) {
                        "music" -> playTrack(mId)
                        "playlist" -> {
                            gotoPlayList(mId)
                        }
                        "podcast-episode" -> {
                            gotoPodcastEpisode(mId)
                        }
                        "podcast" -> {
                            gotoPodCast(mId)
                        }
                        "profile" -> {
                            gotoProfile(mId)
                        }
                        "artist" -> {
                            gotoArtistProfile(mId)
                        }
                        "post" -> {
                            goToPost(mId, mPostType)
                        }
                        else -> {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.data = Uri.parse(url)
                            startActivity(intent)
                            requireActivity().overridePendingTransition(0, 0)
                        }
                    }
                } else {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                    requireActivity().overridePendingTransition(0, 0)
                }

                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                customProgressDialog?.dismiss()
            }
        }
    }

    private fun playTrack(mId: String) {
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(requireActivity())
        }
        runOnUiThread {
            customProgressDialog?.show()
        }
        val mAuth =
            RemoteConstant.getAuthTrackDetails(requireActivity(), mId)
        viewModel.getTrackDetails(mAuth, mId)
    }

    private fun gotoPlayList(mId: String) {
        val intent = Intent(requireActivity(), CommonPlayListActivity::class.java)
        intent.putExtra("mId", mId)
        intent.putExtra("title", "")
        intent.putExtra("imageUrl", "")
        intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
        startActivity(intent)
    }

    private fun gotoPodcastEpisode(mId: String) {
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(requireActivity())
        }
        runOnUiThread {
            customProgressDialog?.show()
        }
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(requireActivity(), RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(requireActivity(), RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathPodCastEpisodeDetails
                    + RemoteConstant.mSlash + mId, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            requireActivity(), RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        viewModel.getPodCastDetails(mAuth, mId)
    }

    private fun gotoProfile(mId: String) {
        val intent = Intent(requireActivity(), UserProfileActivity::class.java)
        intent.putExtra("mProfileId", mId)
        startActivity(intent)
    }

    private fun gotoArtistProfile(mId: String) {
        val detailIntent = Intent(requireActivity(), ArtistPlayListActivity::class.java)
        detailIntent.putExtra("mArtistId", mId)
        startActivity(detailIntent)
        requireActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    private fun goToPost(mId: String, mPostType: String) {
        when (mPostType) {
            "user_text_post" -> {
                val intent = Intent(requireActivity(), FeedTextDetailsActivity::class.java)
                intent.putExtra("mPostId", mId)
                startActivity(intent)
            }
            "user_video_post" -> {
                val intent = Intent(requireActivity(), FeedVideoDetailsActivity::class.java)
                intent.putExtra("mPostId", mId)
                startActivity(intent)
            }
            "user_image_post" -> {
                val intent = Intent(requireActivity(), FeedImageDetailsActivity::class.java)
                //intent.putExtra("mPostViewType", "")
                intent.putExtra("mPostId", mId)
                startActivity(intent)
            }
        }
    }

    private fun gotoPodCast(mId: String) {
        val intent = Intent(requireActivity(), CommonPlayListActivity::class.java)
        intent.putExtra("mId", mId)
        intent.putExtra("title", "")
        intent.putExtra("imageUrl", "")
        intent.putExtra("mDescription", "")
        intent.putExtra("mIsFrom", "fromRadio")
        startActivity(intent)
    }

    override fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String) {
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(requireContext(), getString(R.string.no_internet))
        }
    }

    /**
     * Cancel follow request - API
     */
    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                activity, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        viewModel.cancelRequest(mAuth, mId)

    }
}