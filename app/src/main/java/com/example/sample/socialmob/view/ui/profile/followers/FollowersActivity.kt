package com.example.sample.socialmob.view.ui.profile.followers

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.FollowersActivityBinding
import com.example.sample.socialmob.model.profile.Following
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.WrapContentLinearLayoutManager
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FollowersActivity : BaseCommonActivity(), FollowersAdapter.FollowersAdapterFollowItemClick,
    FollowersAdapter.FollowersAdapterItemClick {
    @Inject
    lateinit var glideRequestManager: RequestManager

    private var binding: FollowersActivityBinding? = null
    private val profileViewModel: ProfileViewModel by viewModels()
    private var mProfileId = ""
    private var adapter: FollowersAdapter? = null
    private var followersList: MutableList<Following>? = ArrayList()
    private var mCount = ""
    private var mStatus: ResultResponse.Status? = null
    private var isAddedLoader: Boolean = false

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.followers_activity)
        val extras: Bundle = intent.extras!!
        mProfileId = extras.getString("mProfileId", "")
        binding?.titleTextView?.text = getString(R.string.followers)
        binding?.refreshTextView?.setOnClickListener {
            callApiCommon()
        }
        // TODO : Check Internet connection
        callApiCommon()

        binding?.followersRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false
            )
        binding?.followersRecyclerView?.addOnScrollListener(CustomScrollListener())

        profileViewModel.profile.nonNull().observe(this, {
            if (it?.followingCount != null) {
                mCount = it.followingCount
            }
        })
        binding?.backImageView?.setOnClickListener {
            finish()
        }
    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {

            val visibleItemCount = recyclerView.layoutManager!!.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()

            if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
                mStatus != ResultResponse.Status.LOADING &&
                mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
            ) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {

                    if (followersList?.size!! >= 15) {
                        loadMoreItems(followersList?.size!!)
                    }
                }
            }

        }
    }

    private fun loadMoreItems(size: Int) {


        if (followersList?.get(size.minus(1))?.recordId != null) {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl +
                        RemoteConstant.profileDataV2 +
                        RemoteConstant.mSlash + mProfileId +
                        RemoteConstant.mFollowerProfile +
                        RemoteConstant.mSlash +
                        followersList?.get(size.minus(1))?.recordId!! +
                        RemoteConstant.mSlash +
                        RemoteConstant.mCount, RemoteConstant.mGetMethod
            )
            val mAuth: String =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    this, RemoteConstant.mAppId, ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            profileViewModel.getFollowersList(
                mAuth,
                mProfileId,
                size.toString(),
                RemoteConstant.mCount,
                followersList?.get(size.minus(1))?.recordId!!
            )


            val followings: MutableList<Following> = ArrayList()
            val mFollow = Following()
            mFollow.id = ""
            followings.add(mFollow)
            followersList?.addAll(followings)
            adapter?.notifyDataSetChanged()
            isAddedLoader = true
        }


    }

    private fun callApiCommon() {
        if (InternetUtil.isInternetOn()) {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = true
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = false
            callApi()
            observeFollowersList()
        } else {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = false
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = true
            val shake: Animation =
                AnimationUtils.loadAnimation(this@FollowersActivity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        }

    }

    private fun observeFollowersList() {
        adapter = FollowersAdapter(
            this, this,
            this@FollowersActivity,glideRequestManager
        )
        binding?.followersRecyclerView?.adapter = adapter

        profileViewModel.followerPaginatedList?.nonNull()
            ?.observe(this, { resultResponse ->

                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        followersList = resultResponse?.data!!

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE
                        adapter?.submitList(followersList!!)
                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        followersList?.addAll(resultResponse?.data!!)
                        adapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.ERROR -> {

                        if (followersList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter())
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }
                    }
                    ResultResponse.Status.NO_DATA_WITH_MESSAGE -> {
                        glideRequestManager.load(R.drawable.private_account).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE
                        binding?.noDataTextView?.text = getString(R.string.private_profile)
                        binding?.noDataSecTextView?.text =
                            getString(R.string.private_profile_sec)

                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_no_search_data).apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE

                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY

                    }
                    ResultResponse.Status.LOADING -> {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                    else -> {

                    }
                }
            })
    }

    private fun removeLoaderFormList() {
        if (followersList != null && followersList?.size!! > 0 && followersList?.get(
                followersList?.size!! - 1
            ) != null && followersList?.get(followersList?.size!! - 1)?.id == ""
        ) {
            followersList?.removeAt(followersList?.size!! - 1)
            adapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }


    private fun callApi() {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileDataV2 +
                    RemoteConstant.mSlash + mProfileId +
                    RemoteConstant.mFollowerProfile +
                    RemoteConstant.mSlash + RemoteConstant.mOffset +
                    RemoteConstant.mSlash + RemoteConstant.mCount, RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            this, RemoteConstant.mAppId, ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        profileViewModel.getFollowersList(
            mAuth, mProfileId, RemoteConstant.mOffset, RemoteConstant.mCount, "0"
        )
    }

    override fun onFollowersAdapterFollowItemClick(
        isFollowing: String, mId: String, mPosition: Int
    ) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    override fun onFollowersAdapterItemClick(mProfileId: String, mIsOwn: String) {
        if (mIsOwn == "false") {
            try {
                val intent = Intent(this@FollowersActivity, UserProfileActivity::class.java)
                intent.putExtra("mProfileId", mProfileId)
                intent.putExtra("mIsOwn", mIsOwn)
                startActivityForResult(intent, 0)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            val intent = Intent(this, MyProfileActivity::class.java)
            startActivity(intent)
        }
    }

    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        profileViewModel.cancelRequest(mAuth, mId)

    }

    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        profileViewModel.unFollowProfile(mAuth, mId, mCount)
    }

    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId, ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        profileViewModel.followProfile(mAuth, mId)
    }

}