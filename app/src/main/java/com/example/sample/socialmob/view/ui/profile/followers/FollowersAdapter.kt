package com.example.sample.socialmob.view.ui.profile.followers

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemFollowingBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.profile.Following
import com.example.sample.socialmob.view.utils.BaseViewHolder
import org.jetbrains.anko.layoutInflater

class FollowersAdapter(
    private val searchPeopleAdapterFollowItemClick: FollowersAdapterFollowItemClick,
    private val searchPeopleAdapterItemClick: FollowersAdapterItemClick,
    private val mContext: Context,
    private var glideRequestManager: RequestManager
) :
    ListAdapter<Following, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2


        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<Following>() {
                override fun areItemsTheSame(
                    oldItem: Following,
                    newItem: Following
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Following,
                    newItem: Following
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFollowingBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_following, parent, false
        )
        return ProfileViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {

        holder.bind(getItem(holder.adapterPosition))
    }

    inner class ProfileViewHolder(val binding: LayoutItemFollowingBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.profileViewModel = getItem(adapterPosition)
            binding.executePendingBindings()

            glideRequestManager.load(getItem(adapterPosition).pimage).thumbnail(0.1f)
                .into(binding.profileImageView)

            binding.followingTextView.setOnClickListener {

                if (getItem(adapterPosition).relation != null) {
                    if (getItem(adapterPosition).relation == "following") {
                        val dialogBuilder: AlertDialog.Builder =
                            this.let { AlertDialog.Builder(mContext) }

                        val inflater: LayoutInflater = mContext.layoutInflater
                        val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                        val okButton: Button = dialogView.findViewById(R.id.ok_button)

                        val b: AlertDialog = dialogBuilder.create()
                        b.show()
                        okButton.setOnClickListener {
                            if (itemCount > 0 && getItem(adapterPosition).relation != null &&
                                getItem(adapterPosition).id != null
                            ) {
                                searchPeopleAdapterFollowItemClick.onFollowersAdapterFollowItemClick(
                                    getItem(adapterPosition).relation!!,
                                    getItem(adapterPosition).id!!,
                                    adapterPosition
                                )
                                Handler().postDelayed({
                                    if (itemCount > 0 && getItem(adapterPosition).privateProfile != null) {
                                        notifyFollow(
                                            adapterPosition,
                                            getItem(adapterPosition).privateProfile?.toString()
                                        )
                                    }
                                }, 100)
                                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                            }
                        }
                        cancelButton.setOnClickListener {
                            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                        }
                    } else {
                        if (getItem(adapterPosition).relation != null &&
                            getItem(adapterPosition).id != null
                        ) {
                            searchPeopleAdapterFollowItemClick.onFollowersAdapterFollowItemClick(
                                getItem(adapterPosition).relation!!,
                                getItem(adapterPosition).id!!,
                                adapterPosition
                            )
                            Handler().postDelayed({
                                if (adapterPosition != RecyclerView.NO_POSITION) {
                                    if (itemCount > 0 && getItem(adapterPosition).privateProfile != null) {

                                        notifyFollow(
                                            adapterPosition,
                                            getItem(adapterPosition).privateProfile?.toString()
                                        )
                                    }
                                }
                            }, 100)
                        }
                    }
                }


            }
            itemView.setOnClickListener {
                searchPeopleAdapterItemClick.onFollowersAdapterItemClick(
                    getItem(adapterPosition).id!!,
                    getItem(adapterPosition).own!!
                )
            }

            // Social-mob official id's user can't un-follow
            when (getItem(adapterPosition).id) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding.followingTextView.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding.followingTextView.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding.followingTextView.visibility = View.GONE
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (getItem(position).id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    private fun notifyFollow(adapterPosition: Int, privateProfile: String?) {
        if (privateProfile == "false") {
            if (getItem(adapterPosition).relation == "following") {
                getItem(adapterPosition).relation = "none"
            } else {
                getItem(adapterPosition).relation = "following"
            }
        } else {
            when {
                getItem(adapterPosition).relation == "none" -> getItem(adapterPosition).relation =
                    "request pending"
                getItem(adapterPosition).relation == "following" -> getItem(adapterPosition).relation =
                    "none"
                getItem(adapterPosition).relation == "request pending" -> getItem(adapterPosition).relation =
                    "none"
            }
        }
        notifyDataSetChanged()
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    interface FollowersAdapterFollowItemClick {
        fun onFollowersAdapterFollowItemClick(isFollowing: String, mId: String, mPosition: Int)
    }

    interface FollowersAdapterItemClick {
        fun onFollowersAdapterItemClick(mProfileId: String, mIsOwn: String)
    }

}
