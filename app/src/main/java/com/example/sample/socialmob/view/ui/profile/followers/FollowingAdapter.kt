package com.example.sample.socialmob.view.ui.profile.followers

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemFollowingBinding
import com.example.sample.socialmob.databinding.ProgressItemBinding
import com.example.sample.socialmob.model.profile.Following
import com.example.sample.socialmob.view.utils.BaseViewHolder
import org.jetbrains.anko.layoutInflater

class FollowingAdapter(
    private val searchPeopleAdapterFollowItemClick: FollowingAdapterFollowItemClick,
    private val searchPeopleAdapterItemClick: FollowingAdapterItemClick,
    private val mContext: Context,
    private var glideRequestManager: RequestManager
) :
    ListAdapter<Following, BaseViewHolder<Any>>(DIFF_CALLBACK) {
    companion object {
        const val ITEM_DATA = 1
        const val ITEM_LOADING = 2

        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<Following>() {
                override fun areItemsTheSame(
                    oldItem: Following,
                    newItem: Following
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Following,
                    newItem: Following
                ): Boolean {
                    return oldItem.equals(newItem)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_DATA -> bindData(parent)
            ITEM_LOADING -> bindLoader(parent)
            else -> bindData(parent)
        }
    }

    private fun bindLoader(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: ProgressItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.progress_item, parent, false
        )
        return ProgressViewHolder(binding)
    }


    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {
        val binding: LayoutItemFollowingBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_following, parent, false
        )
        return ProfileViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(getItem(position))

    }

    override fun getItemViewType(position: Int): Int {

        return if (getItem(position)?.id != "") {
            ITEM_DATA
        } else {
            ITEM_LOADING
        }
    }

    inner class ProfileViewHolder(val binding: LayoutItemFollowingBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

            getItem(adapterPosition)?.let {
                binding.profileViewModel = getItem(adapterPosition)
                binding.executePendingBindings()

                glideRequestManager.load(getItem(adapterPosition).pimage).thumbnail(0.1f)
                    .into(binding.profileImageView)
                // Social-mob official id's user can't un-follow
                when (getItem(adapterPosition)?.id) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding.followingTextView.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding.followingTextView.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding.followingTextView.visibility = View.GONE
                    }
                }

                binding.followingTextView.setOnClickListener {
                    getItem(adapterPosition)?.relation?.let {

                        if (getItem(adapterPosition)?.relation == "following") {
                            val dialogBuilder = this.let { AlertDialog.Builder(mContext) }
                            val inflater = mContext.layoutInflater
                            val dialogView = inflater.inflate(R.layout.common_dialog, null)
                            dialogBuilder.setView(dialogView)
                            dialogBuilder.setCancelable(false)

                            val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                            val okButton: Button = dialogView.findViewById(R.id.ok_button)

                            val b: AlertDialog = dialogBuilder.create()
                            b.show()
                            okButton.setOnClickListener {
                                if (itemCount > 0 &&
                                    getItem(adapterPosition)?.relation != null &&
                                    getItem(adapterPosition)?.id != null
                                ) {
                                    searchPeopleAdapterFollowItemClick.onFollowersAdapterFollowItemClick(
                                        getItem(adapterPosition)?.relation!!,
                                        getItem(adapterPosition)?.id!!,
                                        adapterPosition
                                    )
                                    getItem(adapterPosition)?.privateProfile.let {

                                        Handler().postDelayed({
                                            notifyDelete(
                                                adapterPosition,
                                                getItem(adapterPosition)?.privateProfile
                                            )
                                        }, 100)
                                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                                    }
                                }
                            }
                            cancelButton.setOnClickListener {
                                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

                            }
                        } else {
                            searchPeopleAdapterFollowItemClick.onFollowersAdapterFollowItemClick(
                                getItem(adapterPosition)?.relation!!,
                                getItem(adapterPosition)?.id!!,
                                adapterPosition
                            )
                            getItem(adapterPosition)?.privateProfile.let {
                                Handler().postDelayed({
                                    notifyDelete(
                                        adapterPosition,
                                        getItem(adapterPosition)?.privateProfile
                                    )
                                }, 100)
                            }
                        }

                    }
                }
                itemView.setOnClickListener {
                    if (itemCount > 0) {
                        searchPeopleAdapterItemClick.onFollowersAdapterItemClick(
                            getItem(adapterPosition)?.id!!,
                            getItem(adapterPosition)?.own!!
                        )
                    }
                }
            }
        }
//
    }

    private fun notifyDelete(adapterPosition: Int, privateProfile: Boolean?) {
        if (itemCount > 0) {

            if (privateProfile == false) {
                if (getItem(adapterPosition)?.relation == "following") {
                    getItem(adapterPosition)?.relation = "none"
                } else {
                    getItem(adapterPosition)?.relation = "following"
                }
            } else {
                when {
                    getItem(adapterPosition)?.relation == "none"
                    -> getItem(adapterPosition)?.relation =
                        "request pending"
                    getItem(adapterPosition)?.relation == "following"
                    -> getItem(adapterPosition)?.relation =
                        "none"
                    getItem(adapterPosition)?.relation == "request pending"
                    -> getItem(adapterPosition)?.relation =
                        "none"
                }
            }
            notifyItemChanged(adapterPosition, itemCount)
        }
    }

    inner class ProgressViewHolder(val binding: ProgressItemBinding) :
        BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {

        }

    }

    interface FollowingAdapterFollowItemClick {
        fun onFollowersAdapterFollowItemClick(isFollowing: String, mId: String, mPosition: Int)
    }

    interface FollowingAdapterItemClick {
        fun onFollowersAdapterItemClick(mProfileId: String, mIsOwn: String)
    }

}
