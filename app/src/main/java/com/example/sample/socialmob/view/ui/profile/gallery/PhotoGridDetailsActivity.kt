package com.example.sample.socialmob.view.ui.profile.gallery

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.PhotoGridActivtyBinding
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.repository.utils.*
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedTextDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedVideoDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.LikeListActivity
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.ArtistPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.music.genre.CommonPlayListActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleAdapter
import com.example.sample.socialmob.view.ui.profile.feed.CommentListActivity
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedAdapter
import com.example.sample.socialmob.view.ui.profile.userProfile.UserProfileActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlayListManagerRadio
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.viewmodel.profile.ProfileDetailsViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import im.ene.toro.media.VolumeInfo
import kotlinx.android.synthetic.main.photo_grid_activty.*
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import java.net.URL
import javax.inject.Inject

@AndroidEntryPoint
class PhotoGridDetailsActivity : BaseCommonActivity(), ProfileFeedAdapter.FeedItemLikeClick,
    ProfileFeedAdapter.FeedCommentClick,
    ProfileFeedAdapter.FeedLikeListClick, ProfileFeedAdapter.OptionItemClickListener,
    ProfileFeedAdapter.FeedItemClick, ProfileFeedAdapter.HashTagClickListener,
    ProfileFeedAdapter.AdapterFollowItemClick, PlaylistListener<MediaItem>,
    ProfileFeedAdapter.HyperLink, ProfileFeedAdapter.HyperLinkUrl, SearchPeopleAdapter.SearchPeopleAdapterFollowItemClick {

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var customProgressDialog: CustomProgressDialog? = null
    private var playlistManager: PlaylistManager? = null
    private var playlistManagerRadio: PlayListManagerRadio? = null
    private var mStatus: ResultResponse.Status? = null
    private var mLastClickTime: Long = 0
    private val musicGridViewModel: ProfileDetailsViewModel by viewModels()
    private var binding: PhotoGridActivtyBinding? = null
    private var feedList: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()

    private var mPosition: Int = 0
    private var mId: String = ""
    private var mPath: String = ""
    private var type: String = ""
    private var mTitle: String = ""
    private var CODE = 1501
    private var profileFeedAdapter: ProfileFeedAdapter? = null
    private var mCommentCount = ""
    private var mLikeCount = ""
    private var mPostId = ""
    private var mRecordId = "0"
    private var mPostIdHash = ""
    private var mIsLiked: Boolean = false
    private var isLoaderAdded: Boolean = false

    // Check if whether first item added
    private var isAddedFirstItem: Boolean = false

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE) {
            if (data != null) {
                if (data.getStringExtra("COMMENT_COUNT") != null) {
                    mCommentCount = data.getStringExtra("COMMENT_COUNT")!!
                }
                if (data.getStringExtra("LIKE_COUNT") != null) {
                    mLikeCount = data.getStringExtra("LIKE_COUNT")!!
                }
                if (data.getStringExtra("POST_ID") != null) {
                    mPostId = data.getStringExtra("POST_ID")!!
                }
                val mPostPosition = data.getIntExtra("POST_POSITION", 0)
                if (mCommentCount != "") {
                    profileFeedAdapter?.updateCommentCount(mPostPosition, mCommentCount)
                }
            }
        }
    }

    private var mList: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.photo_grid_activty)
        binding?.photoGridViewModel = musicGridViewModel
        binding?.executePendingBindings()

        mPosition = intent.getIntExtra("mPosition", 0)
        mId = intent.getStringExtra("mId")!!
        mPath = intent.getStringExtra("mPath")!!
        if (intent.getStringExtra("mTitle") != null) {
            mTitle = intent.getStringExtra("mTitle")!!
        }
        if (intent.getStringExtra("type") != null) {
            type = intent.getStringExtra("type")!!
        }
        if (intent.getStringExtra("mRecordId") != null) {
            mRecordId = intent.getStringExtra("mRecordId")!!
        }

        if (mPath == "hashTagFeed") {
            if (intent.getStringExtra("mPostId") != null) {
                mPostIdHash = intent.getStringExtra("mPostId")!!
            }
            if (intent.getStringExtra("mLikeCount") != null) {
                mLikeCount = intent.getStringExtra("mLikeCount")!!
            }
            if (intent.getStringExtra("mCommentCount") != null) {
                mCommentCount = intent.getStringExtra("mCommentCount")!!
            }
        }
        if (intent.hasExtra("mList")) {
            mList = intent.getStringExtra("mList")!!
        }

        profileFeedAdapter = ProfileFeedAdapter(this,
            this, this,
            this, this, this,
            this, false, this, this,
            glideRequestManager, this
        )

        binding?.photoGridRecyclerView?.layoutManager =
            WrapContentLinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
            )
        binding?.photoGridRecyclerView?.hasFixedSize()
        binding?.photoGridRecyclerView?.addOnScrollListener(CustomScrollListener())
        binding?.photoGridRecyclerView?.adapter = profileFeedAdapter


        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            observeData()
            if (mList.isNotEmpty() && mList != "") {
                val gson = Gson()
                val feedListNew: PersonalFeedResponseModelPayloadFeed =
                    gson.fromJson(mList, PersonalFeedResponseModelPayloadFeed::class.java)
                val mDataList: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
                mDataList.addAll(listOf(feedListNew))
                feedList?.addAll(mDataList)
                profileFeedAdapter?.submitList(feedList ?: ArrayList())
                isAddedFirstItem = true
                Handler().postDelayed({
                    if (feedList?.size!! > 0) {
                        if (feedList?.get(0)?.recordId != null) {
                            mRecordId = feedList?.get(0)?.recordId!!
                        } else {
                            mRecordId = feedList?.get(0)?.action?.actionId!!
                        }
                    }
                    callApiLoadMore()
                }, 100)

            } else {
                binding?.isVisibleList = false
                binding?.isVisibleLoading = true
                binding?.isVisibleNoData = false
                binding?.isVisibleNoInternet = false
                callApi()
            }

        } else {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = false
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = true
        }
        binding?.uploadBackImageView?.setOnClickListener {
            onBackPressed()
        }

        //TODO : Check if Track Hyperlink clicked ( From profile feed )
        musicGridViewModel.trackDetailsModel?.nonNull()?.observe(this, Observer {
            if (it?.payload != null && it.payload?.track != null) {
                val dbPlayList: MutableList<TrackListCommon> = ArrayList()
                dbPlayList.add(it.payload?.track!!)
                musicGridViewModel.createDbPlayListModelNotification(
                    dbPlayList, "Suggested tracks"
                )
                val detailIntent = Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                startActivityForResult(detailIntent, 0)
                overridePendingTransition(0, 0)
                musicGridViewModel.trackDetailsModel?.value = null
            } else {
                AppUtils.showCommonToast(this, "No track found")
            }
        })


        musicGridViewModel.podCastResponseModel?.observe(this, {
            if (it?.payload != null && it.payload?.episode != null
            ) {
                val dbPlayList: MutableList<RadioPodCastEpisode> = ArrayList()
                val mPlayList = RadioPodCastEpisode(
                    System.currentTimeMillis().toInt(),
                    it.payload?.episode?._id!!,
                    it.payload?.episode?.number_id!!,
                    it.payload?.episode?.podcast_name,
                    it.payload?.episode?.episode_name,
                    it.payload?.episode?.source_path,
                    it.payload?.episode?.cover_image,
                    "",
                    "",
                    RemoteConstant.getImageUrlFromWidth(
                        it.payload?.episode?.cover_image,
                        400
                    ),
                    "", it.payload?.episode?.podcast_id!!, "false",
                    it.payload?.episode?.play_count
                )
                dbPlayList.add(mPlayList)
                musicGridViewModel.createDbPlayListModelNotificationPodcast(
                    dbPlayList, "Suggested tracks"
                )

                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
                val detailIntent = Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
                detailIntent.putExtra(RemoteConstant.extraIndexIsFrom, "dblistMyFavTrack")
                startActivity(detailIntent)
                overridePendingTransition(0, 0)
                musicGridViewModel.podCastResponseModel?.value = null
            } else {
                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }


        })

    }


    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView,
            dx: Int,
            dy: Int
        ) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as WrapContentLinearLayoutManager).findFirstVisibleItemPosition()
                when (mPath) {
                    "hashTagFeed" -> {
                        if (feedList!!.size >= 15 &&
                            mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                            mStatus != ResultResponse.Status.LOADING &&
                            mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            && !isLoaderAdded
                        ) {
                            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                                mPosition = feedList?.size!!
                                callApiLoadMore()
                            }
                        }
                    }
                    "campaign" -> {
                        if (
                            feedList!!.size >= 15 && mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                            mStatus != ResultResponse.Status.LOADING &&
                            mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            && !isLoaderAdded
                        ) {
                            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                                if (feedList?.get(feedList?.size!! - 1)?.action?.actionId != null) {
                                    mRecordId =
                                        feedList?.get(feedList?.size!! - 1)?.action?.actionId!!
                                    callApiLoadMore()
                                }


                            }
                        }
                    }
                    "" -> {
                        if (
                            feedList!!.size >= 15 && mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                            mStatus != ResultResponse.Status.LOADING &&
                            mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            && !isLoaderAdded
                        ) {
                            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                                if (feedList?.get(feedList?.size!! - 1)?.recordId != null) {
                                    mRecordId = feedList?.get(feedList?.size!! - 1)?.recordId!!
                                    callApiLoadMore()
                                } else if (feedList?.get(feedList?.size!! - 1)?.action?.actionId != null) {
                                    mRecordId = feedList?.get(feedList?.size!! - 1)?.recordId!!
                                }


                            }
                        }
                    }
                    else -> {
                        if (feedList!!.size >= 15 &&
                            mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                            mStatus != ResultResponse.Status.LOADING &&
                            mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                            && !isLoaderAdded
                        ) {
                            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                                if (feedList?.get(feedList?.size!! - 1)?.recordId != null) {
                                    mRecordId = feedList?.get(feedList?.size!! - 1)?.recordId!!
                                    callApiLoadMore()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun callApiLoadMore() {

        val loaderItem: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
        val mList = PersonalFeedResponseModelPayloadFeed()

        if (!isLoaderAdded) {
            mList.isLoader = "loader"
            loaderItem.add(mList)
            feedList?.addAll(loaderItem)
            binding?.photoGridRecyclerView?.post {
                profileFeedAdapter?.notifyDataSetChanged()
            }
        }
        isLoaderAdded = true
        when (mPath) {
            "hashTagFeed" -> {
                title_text_view.text = ""
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + "/api/v4/hashtag/feed/" + mId + "/all/" + feedList?.get(
                        mPosition.minus(1)
                    )?.action?.actionId + RemoteConstant.mSlash
                            + RemoteConstant.mCount, RemoteConstant.mGetMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getHAshTagFeed(
                    mAuth,
                    mId,
                    feedList?.get(
                        mPosition.minus(1)
                    )?.action?.actionId!!,
                    RemoteConstant.mCount, this
                )

            }
            "campaign" -> {
                title_text_view.text = mTitle
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v4/hashtag/feed/" + mId + "/media/" + mRecordId + "/" + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getMediaGridCampaign(
                    mAuth,
                    mId, "media",
                    mRecordId,
                    RemoteConstant.mCount
                )
            }
            "" -> {
                title_text_view.text = getString(R.string.reel_board)

                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mFeedmediagrid + RemoteConstant.mSlash +
                            mRecordId + RemoteConstant.mSlash + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                musicGridViewModel.getPhotoGridMediagrid(
                    mAuth,
                    mId,
                    mRecordId,
                    RemoteConstant.mCount
                )

            }
            else -> {
                title_text_view.text = getString(R.string.gallery)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mMediaGrid + RemoteConstant.mSlash +
                            mId + RemoteConstant.mSlash +
                            mRecordId + RemoteConstant.mSlash + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getMediaGrid(
                    mAuth,
                    mId,
                    mRecordId,
                    RemoteConstant.mCount
                )
            }
        }
    }


    override fun onBackPressed() {
        val intent = intent
        intent.putExtra("COMMENT_COUNT", mCommentCount)
        intent.putExtra("LIKE_COUNT", mLikeCount)
        intent.putExtra("POST_ID", mPostIdHash)
        intent.putExtra("IS_LIKED", mIsLiked)
        intent.putExtra("IS_FROM_COMMENT_LIST", false)
        setResult(1501, intent)
        super.onBackPressed()
        AppUtils.hideKeyboard(this@PhotoGridDetailsActivity, binding?.noInternetImageView!!)
    }

    private fun observeData() {


        musicGridViewModel.mPhotoVideoList.nonNull().observe(this, Observer { resultResponse ->
            mStatus = resultResponse.status

            when (resultResponse.status) {
                ResultResponse.Status.SUCCESS -> {
                    if (!isAddedFirstItem) {
                        feedList = resultResponse?.data!!.toMutableList()
                        profileFeedAdapter?.submitList(feedList ?: ArrayList())
                    } else {
                        if (isLoaderAdded) {
                            removeLoaderFormList()
                        }

                        val mFeedNewData: MutableList<PersonalFeedResponseModelPayloadFeed> =
                            resultResponse.data!!
                        feedList?.addAll(mFeedNewData)
                        profileFeedAdapter?.notifyDataSetChanged()
                    }

                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.GONE
                    binding?.photoGridRecyclerView?.visibility = View.VISIBLE
                    binding?.dataLinear?.visibility = View.VISIBLE

                    mStatus = ResultResponse.Status.LOADING_COMPLETED

                }
                ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                    if (isLoaderAdded) {
                        removeLoaderFormList()
                    }
                    mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                }
                ResultResponse.Status.PAGINATED_LIST -> {

                    if (isLoaderAdded) {
                        removeLoaderFormList()
                    }
                    val mListSize = feedList?.size
                    feedList?.addAll(resultResponse?.data!!)
                    profileFeedAdapter?.submitList(feedList?:ArrayList())
                    if (feedList?.size == 0) {
                        profileFeedAdapter?.notifyDataSetChanged()
                    } else {
                        profileFeedAdapter?.notifyItemRangeInserted(
                            mListSize ?: 0,
                            feedList?.size ?: 0
                        )
                    }
                    mStatus = ResultResponse.Status.LOADING_COMPLETED
                }
                ResultResponse.Status.ERROR -> {
                    if (feedList?.size == 0) {
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.VISIBLE
                        binding?.photoGridRecyclerView?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        glideRequestManager.load(R.drawable.server_error).apply(
                            RequestOptions().fitCenter())
                            .into(binding?.noInternetImageView!!)
                        binding?.noInternetTextView?.text = getString(R.string.server_error)
                        binding?.noInternetSecTextView?.text =
                            getString(R.string.server_error_content)
                    }
                }
                ResultResponse.Status.NO_DATA -> {
                    binding?.progressLinear?.visibility = View.GONE
                    binding?.noInternetLinear?.visibility = View.GONE
                    binding?.dataLinear?.visibility = View.GONE
                    binding?.photoGridRecyclerView?.visibility = View.GONE
                    binding?.noDataLinear?.visibility = View.VISIBLE
                }
                ResultResponse.Status.LOADING -> {
                    if (!isAddedFirstItem) {
                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.dataLinear?.visibility = View.GONE
                        binding?.photoGridRecyclerView?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                    }
                }
                else -> {

                }
            }
        })
    }


    private fun removeLoaderFormList() {

        if (feedList != null && feedList?.size!! > 0 && feedList?.get(
                feedList?.size!! - 1
            ) != null && feedList?.get(feedList?.size!! - 1)?.isLoader == "loader"
        ) {
            feedList?.removeAt(feedList?.size!! - 1)
            profileFeedAdapter?.notifyItemRemoved(feedList?.size ?: 0 - 1)
            isLoaderAdded = false
        }
    }


    private fun callApi() {

        when (mPath) {
            // TODO : Hash Tag feeds
            "hashTagFeed" -> {
                title_text_view.text = ""
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + "/api/v4/hashtag/feed/" + mId + "/all/" + "0" + RemoteConstant.mSlash
                            + RemoteConstant.mCount, RemoteConstant.mGetMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getHAshTagFeed(
                    mAuth,
                    mId,
                    "0",
                    RemoteConstant.mCount,
                    this
                )

            }
            // TODO :
            "" -> {
                title_text_view.text = getString(R.string.reel_board)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mFeedmediagrid + RemoteConstant.mSlash +
                            mRecordId + RemoteConstant.mSlash + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                musicGridViewModel.getPhotoGridMediagrid(
                    mAuth,
                    mId,
                    mRecordId,
                    RemoteConstant.mCount
                )
            }
            // TODO : Profile media grid feeds
            else -> {
                title_text_view.text = getString(R.string.gallery)
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mMediaGrid + RemoteConstant.mSlash +
                            mId + RemoteConstant.mSlash +
                            mRecordId + RemoteConstant.mSlash + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getMediaGrid(mAuth, mId, mRecordId, RemoteConstant.mCount)
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        binding?.photoGridRecyclerView?.adapter = null
        if (musicGridViewModel.mPhotoVideoList.hasActiveObservers()) {
            musicGridViewModel.mPhotoVideoList.removeObservers(this)
        }
        binding?.unbind()
    }

    override fun onFeedItemClick(mPostId: String, mType: String) {


        if (mType == "image") {
            val intent = Intent(this, FeedImageDetailsActivity::class.java)
            //intent.putExtra("mPostViewType", "")
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, CODE)
        }
        if (mType == "video") {
            val intent = Intent(this, FeedVideoDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, CODE)
        }
        if (mType == "text") {
            val intent = Intent(this, FeedTextDetailsActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            startActivityForResult(intent, CODE)
        }
    }

    override fun onOptionItemClickOption(mMethod: String, mId: String, mPos: Int) {
        if (mMethod == "delete") {
            musicGridViewModel.deletePost(mId, this)
        }

    }

    override fun onFeedLikeListClick(mPostId: String, mLikeCount: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            if (mLikeCount != "" && mLikeCount.toInt() > 0) {
                val intent = Intent(this, LikeListActivity::class.java)
                intent.putExtra("mPostId", mPostId)
                startActivity(intent)
            }
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onFeedItemLikeClick(mPostId: String, isLike: String) {
        if (isLike == "true") {
            if (mPostIdHash == mPostId) {
                mLikeCount = Integer.parseInt(mLikeCount).plus(1).toString()
                mIsLiked = true
            }
            likePost(mPostId)
        } else {
            if (mPostIdHash == mPostId) {
                mLikeCount = Integer.parseInt(mLikeCount).minus(1).toString()

                mIsLiked = false
            }
            disLikePost(mPostId)
        }
    }

    override fun onOptionItemClickOption(
        mId: String, mPosition: String, mPath: String, mPostId: String, mLikeCount: String,
        mCommentCount: String
    ) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {

            val detailIntent = Intent(this, PhotoGridDetailsActivity::class.java)
            detailIntent.putExtra("mId", mId)
            detailIntent.putExtra("mPosition", mPosition)
            detailIntent.putExtra("mPath", mPath)
            detailIntent.putExtra("mPostId", mPostId)
            detailIntent.putExtra("mLikeCount", mLikeCount)
            detailIntent.putExtra("mCommentCount", mCommentCount)
            startActivityForResult(detailIntent, CODE)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    override fun onCommentClick(mPostId: String, mPosition: Int, mIsMyPost: Boolean) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            println(">Double tap")
        } else {
            val intent = Intent(this, CommentListActivity::class.java)
            intent.putExtra("mPostId", mPostId)
            intent.putExtra("mPosition", mPosition)
            intent.putExtra("mIsMyPost", mIsMyPost)
            startActivityForResult(intent, CODE)
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    private fun likePost(postId: String) {
        val mAuth = RemoteConstant.getAuthLikePost(this, postId)
        musicGridViewModel.likePost(mAuth, postId)
    }

    private fun disLikePost(postId: String) {
        val mAuth = RemoteConstant.getAuthDislikePost(this, postId)
        musicGridViewModel.disLikePost(mAuth, postId)
    }

    override fun onAdapterFollowItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        musicGridViewModel.unFollowProfile(mAuth, mId, "")
    }

    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        musicGridViewModel.followProfile(mAuth, mId)
    }

    override fun onPause() {
        super.onPause()
        if (SharedPrefsUtils.getStringPreference(this, AppUtils.SELECTED_CATEGORY, "") == "Radio") {
            playlistManagerRadio?.unRegisterPlaylistListener(this)
        } else {
            playlistManager?.unRegisterPlaylistListener(this)

        }

        if (ProfileFeedAdapter.helper != null) {
            ProfileFeedAdapter.helper!!.pause()
        }
    }

    override fun onHyperLinkClick(mUrl: String) {
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog(this)
        }
        runOnUiThread {
            customProgressDialog?.show()
        }
        var mType = ""
        var mId = ""
        var mTarget = ""
        var mPostType = ""

        try {

            doAsync {


                val htmlSrc = URL(mUrl).readText()
                val doc = Jsoup.parse(htmlSrc)
                val eMETA = doc.select("meta")

                for (i in 0 until eMETA.size) {
                    val name = eMETA[i].attr("name")
                    val content = eMETA[i].attr("content")

                    if (name == "pagetype") {
                        mType = content
                    }
                    if (name == "source") {
                        mId = content
                    }
                    if (name == "target") {
                        mTarget = content
                    }
                    if (name == "post-type") {
                        mPostType = content
                    }

                }

                if (mTarget == "socialmob") {
                    when (mType) {
                        "music" -> {
                            val mAuth =
                                RemoteConstant.getAuthTrackDetails(
                                    this@PhotoGridDetailsActivity,
                                    mId
                                )
                            musicGridViewModel.getTrackDetails(mAuth, mId)
                        }
                        "playlist" -> {
                            val intent = Intent(
                                this@PhotoGridDetailsActivity,
                                CommonPlayListActivity::class.java
                            )
                            intent.putExtra("mId", mId)
                            intent.putExtra("title", "")
                            intent.putExtra("imageUrl", "")
                            intent.putExtra("mIsFrom", "fromMusicDashBoardPlayList")
                            startActivity(intent)
                        }
                        "podcast-episode" -> {
                            val fullData = RemoteConstant.getEncryptedString(
                                SharedPrefsUtils.getStringPreference(
                                    this@PhotoGridDetailsActivity,
                                    RemoteConstant.mAppId,
                                    ""
                                )!!,
                                SharedPrefsUtils.getStringPreference(
                                    this@PhotoGridDetailsActivity,
                                    RemoteConstant.mApiKey,
                                    ""
                                )!!,
                                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathPodCastEpisodeDetails
                                        + RemoteConstant.mSlash + mId, RemoteConstant.mGetMethod
                            )
                            val mAuth =
                                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                    this@PhotoGridDetailsActivity, RemoteConstant.mAppId,
                                    ""
                                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                            musicGridViewModel.getPodCastDetails(mAuth, mId)
                        }
                        "podcast" -> {
                            val intent = Intent(
                                this@PhotoGridDetailsActivity,
                                CommonPlayListActivity::class.java
                            )
                            intent.putExtra("mId", mId)
                            intent.putExtra("title", "")
                            intent.putExtra("imageUrl", "")
                            intent.putExtra("mDescription", "")
                            intent.putExtra("mIsFrom", "fromRadio")
                            startActivity(intent)
                        }
                        "profile" -> {
                            val intent = Intent(
                                this@PhotoGridDetailsActivity,
                                UserProfileActivity::class.java
                            )
                            intent.putExtra("mProfileId", mId)
                            startActivity(intent)

                        }
                        "artist" -> {
                            val detailIntent = Intent(
                                this@PhotoGridDetailsActivity,
                                ArtistPlayListActivity::class.java
                            )
                            detailIntent.putExtra("mArtistId", mId)
                            startActivity(detailIntent)
                            overridePendingTransition(
                                R.anim.slide_in_up,
                                R.anim.slide_out_up
                            )
                        }
                        "post" -> {
                            when (mPostType) {
                                "user_text_post" -> {
                                    val intent =
                                        Intent(
                                            this@PhotoGridDetailsActivity,
                                            FeedTextDetailsActivity::class.java
                                        )
                                    intent.putExtra("mPostId", mId)
                                    startActivity(intent)

                                }
                                "user_video_post" -> {
                                    val intent =
                                        Intent(
                                            this@PhotoGridDetailsActivity,
                                            FeedVideoDetailsActivity::class.java
                                        )
                                    intent.putExtra("mPostId", mId)
                                    startActivity(intent)
                                }
                                "user_image_post" -> {
                                    val intent =
                                        Intent(
                                            this@PhotoGridDetailsActivity,
                                            FeedImageDetailsActivity::class.java
                                        )
                                    //intent.putExtra("mPostViewType", "")
                                    intent.putExtra("mPostId", mId)
                                    startActivity(intent)
                                }
                            }
                        }
                    }

                } else {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(mUrl)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
                if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                    customProgressDialog?.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                customProgressDialog?.dismiss()
            }
        }
    }

    override fun onHyperLinkUrlClick(mUrl: String) {

    }

    override fun onPlaylistItemChanged(
        currentItem: MediaItem?, hasNext: Boolean, hasPrevious: Boolean
    ): Boolean {
        return true
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.STOPPED -> println("---->Stopped")

            PlaybackState.RETRIEVING, PlaybackState.PREPARING, PlaybackState.SEEKING -> println("")

            PlaybackState.PLAYING -> {
                ProfileFeedAdapter.isMute = true
                try {
                    ProfileFeedAdapter.helper?.pause()
                    if (ProfileFeedAdapter.helper != null && ProfileFeedAdapter.helper != null && ProfileFeedAdapter.helper?.volumeInfo != null) {
                        ProfileFeedAdapter.helper?.volumeInfo = VolumeInfo(false, 0.0f)
                    }
                } catch (e: Exception) {
                    println("Error -> ProfileFeedAdapter.helper")
                }
            }
            PlaybackState.PAUSED -> {
                println()
            }

            else -> {
            }
        }
        return true
    }


    override fun onResume() {
        super.onResume()
        if (SharedPrefsUtils.getStringPreference(this, AppUtils.SELECTED_CATEGORY, "") == "Radio") {
            playlistManagerRadio = MyApplication.playlistManagerRadio
            playlistManagerRadio?.registerPlaylistListener(this)
        } else {
            playlistManager = MyApplication.playlistManager
            playlistManager?.registerPlaylistListener(this)
        }
        //Makes sure to retrieve the current playback information
        updateCurrentPlaybackInformation()
    }

    /**
     * Makes sure to update the UI to the current playback item.
     */
    private fun updateCurrentPlaybackInformation() {
        if (SharedPrefsUtils.getStringPreference(this, AppUtils.SELECTED_CATEGORY, "") == "Radio") {
            val currentPlaybackState = playlistManagerRadio?.currentPlaybackState
            if (currentPlaybackState !== PlaybackState.STOPPED) {
                onPlaybackStateChanged(currentPlaybackState!!)
            }
        } else {
            val currentPlaybackState = playlistManager?.currentPlaybackState
            if (currentPlaybackState !== PlaybackState.STOPPED) {
                onPlaybackStateChanged(currentPlaybackState!!)
            }
        }
    }

    override fun onSearchPeopleAdapterFollowItemClick(isFollowing: String, mId: String) {
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }
    }

    /**
     * Cancel follow request - API
     */
    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        musicGridViewModel.cancelRequest(mAuth, mId)

    }

}


