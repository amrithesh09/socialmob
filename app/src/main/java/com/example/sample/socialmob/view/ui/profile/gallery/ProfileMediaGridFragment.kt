package com.example.sample.socialmob.view.ui.profile.gallery

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.GalleryFragmentBinding
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPost
import com.example.sample.socialmob.model.profile.PostFeedData
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedImageGalleryGridAdapter
import com.example.sample.socialmob.view.ui.home_module.feed.FeedTextDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.feed.FeedVideoDetailsActivity
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.ui.write_new_post.WritePostActivity
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.events.UploadStatus
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.viewmodel.feed.FeedViewModel
import com.example.sample.socialmob.viewmodel.feed.MusicAndPhotoGridViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.gallery_fragment.*
import net.vrgsoft.videcrop.ffmpeg.ExecuteBinaryResponseHandler
import net.vrgsoft.videcrop.ffmpeg.FFmpeg
import net.vrgsoft.videcrop.ffmpeg.FFtask
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class ProfileMediaGridFragment : Fragment(),
    FeedImageGalleryGridAdapter.GridImageItemClickListener,
    ProgressRequestBody.UploadCallbacks {
    @Inject
    lateinit var glideRequestManager: RequestManager

    @SuppressLint("SetTextI18n")
    override fun onProgressUpdate(percentage: Int, timestamp: String?) {
        if (binding?.uploadTextView != null) {
            val uploadTextView = binding?.uploadTextView
            if (uploadTextView?.text != "Uploading") {
                uploadTextView?.text = "Uploading"
            }
        }
        if (binding?.progressBarUpload != null) {
            val mProgressBar = binding?.progressBarUpload
            mProgressBar?.progress = percentage

            if (mProgressBar?.isIndeterminate == true) {
                mProgressBar.isIndeterminate = false
            }
        }
    }

    override fun onError() {
        binding?.progressCardView?.visibility = View.GONE
        AppUtils.showCommonToast(
            requireActivity(),
            "Error occur please try again later"
        )
    }

    override fun onFinish() {

    }


    private val musicGridViewModel: MusicAndPhotoGridViewModel by viewModels()
    private var binding: GalleryFragmentBinding? = null
    private var feedList: MutableList<PersonalFeedResponseModelPayloadFeed>? = ArrayList()
    private var mId = ""
    private var mType = ""
    private var feedImageGalleryAdapter: FeedImageGalleryGridAdapter? = null
    private var mStatus: ResultResponse.Status? = null
    private var isAddedLoader: Boolean = false
    private var mFFMpeg: FFmpeg? = null
    private var mFFTask: FFtask? = null

    companion object {

        var mAddFeeData: PersonalFeedResponseModelPayloadFeedActionPost? = null
        var EXPORT_STARTED: Boolean = false
        var isCompletedVideoEdit: Boolean = false
        var isUploadStarted: Boolean = false
        var isAddedFeed: Boolean = false
        var isUpdatedFeed: Boolean = false

        var hashTagDataname: String = ""
        var idPost: String = ""
        var idPostPosition: Int? = null
        var titlePost: String = ""
        var mAuthPost: String = ""
        var mAuthPostType: String = ""
        var mImageListPost: String = ""
        var mImageListPostList: ArrayList<GalleryData> = java.util.ArrayList()
        var mThumbListPost: ArrayList<File>? = java.util.ArrayList()
        var postDataPost = PostFeedData()
        var ratio = ""
        private const val ARG_STRING = "mId"
        private const val ARG_TYPE = "mType"

        fun newInstance(mId: String, mType: String): ProfileMediaGridFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mId)
            args.putSerializable(ARG_TYPE, mType)
            val fragment = ProfileMediaGridFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: Bundle? = arguments
        mId = args?.getString(ARG_STRING, "")!!
        mType = args.getString(ARG_TYPE, "")!!


        // Call all operation  related to network or other ui blocking operations here.
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            if (mType == "") {
                callApi(mId, mType)
            } else {
                binding?.campaignImageView?.visibility = View.VISIBLE
                binding?.campaignTextView?.visibility = View.VISIBLE
                binding?.multipleActions?.visibility = View.VISIBLE
                binding?.progressCardView?.visibility = View.GONE
                callApi(mId, mType)
            }
        } else {
            binding?.isVisibleList = false
            binding?.isVisibleLoading = false
            binding?.isVisibleNoData = false
            binding?.isVisibleNoInternet = true
            glideRequestManager.load(R.drawable.ic_app_offline).apply(RequestOptions().fitCenter())
                .into(binding?.noDataImageView!!)
        }

        // perform all ui related operation here
        observeData()


        binding?.galleryRecyclerView?.layoutManager =
            androidx.recyclerview.widget.GridLayoutManager(activity, 3)
        binding?.galleryRecyclerView?.addItemDecoration(
            GridSpacingItemDecoration(3, 0, true)
        )

        feedImageGalleryAdapter = FeedImageGalleryGridAdapter(this, glideRequestManager)
        gallery_recycler_view.layoutManager =
            (androidx.recyclerview.widget.GridLayoutManager(activity, 3))
        gallery_recycler_view.addItemDecoration(
            GridSpacingItemDecoration(3, 5, false)
        )
        binding?.galleryRecyclerView?.adapter = feedImageGalleryAdapter

//        binding?.galleryRecyclerView?.isNestedScrollingEnabled = false
        binding?.galleryRecyclerView?.addOnScrollListener(CustomScrollListener())

        binding?.swipeToRefreshFeed?.setColorSchemeResources(R.color.purple_500)
        binding?.swipeToRefreshFeed?.isRefreshing = false
        binding?.swipeToRefreshFeed?.setOnRefreshListener {
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                binding?.swipeToRefreshFeed?.isRefreshing = false
            }, 1000)
        }
//        binding?.nestedScrollView?.viewTreeObserver?.addOnScrollChangedListener {
//            val viewNestedView =
//                binding?.nestedScrollView?.getChildAt(binding?.nestedScrollView?.childCount!! - 1)
//
//            val diff =
//                viewNestedView?.bottom?:0 - (binding?.nestedScrollView?.height!! + binding?.nestedScrollView!!.scrollY)
//
//            if (diff == 0) {
//                //your api call to fetch data
//                if (mStatus == ResultResponse.Status.LOADING_COMPLETED &&
//                    mStatus != ResultResponse.Status.LOADING_PAGINATED_LIST &&
//                    mStatus != ResultResponse.Status.LOADING &&
//                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
//                ) {
//                    if (feedList?.get(feedList?.size!! - 1)?.recordId != null) {
//                        loadMoreItems(feedList?.get(feedList?.size!! - 1)?.recordId!!)
//                    } else if (feedList?.get(feedList?.size!! - 1)?.action?.actionId != null) {
//                        loadMoreItems(feedList?.get(feedList?.size!! - 1)?.action?.actionId!!)
//                    }
//                }
//            }
//        }


        binding?.backgroundDimmer?.setOnClickListener {
            collapse()
        }
        binding?.multipleActions?.setOnFloatingActionsMenuUpdateListener(object :
            FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
            override fun onMenuExpanded() {

                binding?.backgroundDimmer?.bringToFront()
                binding?.multipleActions?.bringToFront()
                binding?.backgroundDimmer?.visibility = View.VISIBLE
            }

            override fun onMenuCollapsed() {
                binding?.backgroundDimmer?.visibility = View.GONE
            }
        })

        binding?.actionWriteAPost?.setOnClickListener {
            writePost()
        }
        binding?.actionUploadAVideo?.setOnClickListener {
            uploadVideo()
        }
        binding?.actionUploadAPhoto?.setOnClickListener {
            uploadPhoto()
        }
    }

    inner class CustomScrollListener :
        androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(
            recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int
        ) {

        }

        override fun onScrolled(
            recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int
        ) {
            if (dy > 0) {
                val visibleItemCount = recyclerView.layoutManager!!.childCount
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                val firstVisibleItemPosition =
                    (recyclerView.layoutManager as GridLayoutManager).findFirstVisibleItemPosition()

                if (feedList?.size!! >= 15 && mStatus == ResultResponse.Status.LOADING_COMPLETED &&
                    mStatus != ResultResponse.Status.LOADING &&
                    mStatus != ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY &&
                    visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0
                ) {
                    if (feedList?.get(feedList?.size!! - 1)?.recordId != null) {
                        loadMoreItems(feedList?.get(feedList?.size!! - 1)?.recordId!!)
                    } else if (feedList?.get(feedList?.size!! - 1)?.action?.actionId != null) {
                        loadMoreItems(feedList?.get(feedList?.size!! - 1)?.action?.actionId!!)
                    }
                }
            }
        }
    }

    // TODO : TODO : Upload Video
    private fun uploadVideo() {
        SharedPrefsUtils.setStringPreference(requireActivity(), "POST_FROM", "CAMPAIGN_FEED")
        SharedPrefsUtils.setStringPreference(requireActivity(), "CAMPAIGN_NAME", hashTagDataname)
        val intent = Intent(activity, PickerActivity::class.java)
        intent.putExtra("title", "UPLOAD VIDEO")
        intent.putExtra("IMAGES_LIMIT", 5)
        intent.putExtra("VIDEOS_LIMIT", 1)
        startActivity(intent)
        collapse()
    }

    // TODO : Upload Photo
    private fun uploadPhoto() {
        SharedPrefsUtils.setStringPreference(requireActivity(), "POST_FROM", "CAMPAIGN_FEED")
        SharedPrefsUtils.setStringPreference(requireActivity(), "CAMPAIGN_NAME", hashTagDataname)
        val intent = Intent(activity, PickerActivity::class.java)
        intent.putExtra("title", "UPLOAD PHOTO")
        intent.putExtra("IMAGES_LIMIT", 5)
        intent.putExtra("VIDEOS_LIMIT", 1)
        startActivity(intent)
        collapse()
    }

    //TODO : Write Post
    private fun writePost() {
        SharedPrefsUtils.setStringPreference(requireActivity(), "POST_FROM", "CAMPAIGN_FEED")
        if (hashTagDataname != "") {
            SharedPrefsUtils.setStringPreference(
                requireActivity(),
                "CAMPAIGN_NAME",
                hashTagDataname
            )
        }
        val intent = Intent(activity, WritePostActivity::class.java)
        startActivity(intent)
        collapse()
    }

    private fun collapse() {

        Handler().postDelayed({
            //run this method only when run is true
            binding?.multipleActions?.collapse()
        }, 100)
    }

    override fun onDestroy() {
        super.onDestroy()
        SharedPrefsUtils.setStringPreference(requireActivity(), "CAMPAIGN_NAME", "")
    }

    private fun loadMoreItems(size: String) {
        if (!musicGridViewModel.isLoadCompletedFeedPhotoGrid) {
            val mListSizeNew = feedList?.size!!
            val mutableList: MutableList<PersonalFeedResponseModelPayloadFeed> = ArrayList()
            val mList = PersonalFeedResponseModelPayloadFeed()
            mList.action?.actionId = ""
            mutableList.add(mList)
            feedList?.addAll(mutableList)
            isAddedLoader = true
            feedImageGalleryAdapter?.notifyItemChanged(mListSizeNew, feedList?.size)
            if (mType == "") {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mMediaGrid + RemoteConstant.mSlash +
                            mId + RemoteConstant.mSlash +
                            size + RemoteConstant.mSlash + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(
                            activity,
                            RemoteConstant.mAppId,
                            ""
                        )!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getMediaGrid(mAuth, mId, size, RemoteConstant.mCount)
            } else {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v4/hashtag/feed/" + mId + "/media/" + size + "/" + RemoteConstant.mCount,
                    RemoteConstant.mGetMethod
                )
                val mAuth = RemoteConstant.frameWorkName +
                        SharedPrefsUtils.getStringPreference(
                            activity,
                            RemoteConstant.mAppId,
                            ""
                        )!! +
                        ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

                musicGridViewModel.getMediaGridCampaign(
                    mAuth,
                    mId, "media",
                    size,
                    RemoteConstant.mCount
                )
            }
        }

    }

    private var feedViewModel: FeedViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        feedViewModel = ViewModelProvider(this).get(FeedViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.gallery_fragment, container, false)
        return binding?.root
    }

    private fun callApi(mId: String, mType: String) {
        binding?.isVisibleList = false
        binding?.isVisibleLoading = true
        binding?.isVisibleNoData = false
        binding?.isVisibleNoInternet = false

        if (mType == "") {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mMediaGrid + RemoteConstant.mSlash +
                        mId + RemoteConstant.mSlash +
                        RemoteConstant.mOffset + RemoteConstant.mSlash + RemoteConstant.mCounttwentyFive,
                RemoteConstant.mGetMethod
            )
            val mAuth = RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            musicGridViewModel.getMediaGrid(
                mAuth,
                mId,
                RemoteConstant.mOffset,
                RemoteConstant.mCounttwentyFive
            )
        } else {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + "api/v4/hashtag/feed/" + mId + "/media/0/" + RemoteConstant.mCount,
                RemoteConstant.mGetMethod
            )
            val mAuth = RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            musicGridViewModel.getMediaGridCampaign(
                mAuth,
                mId, "media",
                RemoteConstant.mOffset,
                RemoteConstant.mCount
            )
        }
    }

    private fun observeData() {


        musicGridViewModel.mPhotoVideoList.nonNull()
            .observe(viewLifecycleOwner, { resultResponse ->
                mStatus = resultResponse.status

                when (resultResponse.status) {
                    ResultResponse.Status.SUCCESS -> {
                        feedList = resultResponse?.data!!

                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.VISIBLE

                        feedImageGalleryAdapter?.submitList(feedList!!)

                        mStatus = ResultResponse.Status.LOADING_COMPLETED


                        if (activity != null && musicGridViewModel.hashTagData != null) {
                            try {
                                if (musicGridViewModel.hashTagData?.coverImage != null) {
                                    glideRequestManager
                                        .load(musicGridViewModel.hashTagData?.coverImage)
                                        .apply(RequestOptions().centerCrop().placeholder(0))
                                        .thumbnail(0.1f)
                                        .into(binding?.campaignImageView!!)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            if (musicGridViewModel.hashTagData?.description != null) {
                                binding?.campaignTextView?.text =
                                    musicGridViewModel.hashTagData?.description
                            }
                            if (musicGridViewModel.hashTagData?.name != null) {
                                try {
                                    (activity as CommonFragmentActivity).setTitleToolbar(
                                        musicGridViewModel.hashTagData?.name!!
                                    )

                                    hashTagDataname = musicGridViewModel.hashTagData?.name!!
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            }
                        }


                    }
                    ResultResponse.Status.EMPTY_PAGINATED_LIST -> {
                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY
                    }
                    ResultResponse.Status.PAGINATED_LIST -> {

                        if (isAddedLoader) {
                            removeLoaderFormList()
                        }
                        feedList?.addAll(resultResponse?.data!!)
                        feedImageGalleryAdapter?.notifyDataSetChanged()
                        mStatus = ResultResponse.Status.LOADING_COMPLETED

                    }
                    ResultResponse.Status.ERROR -> {

                        if (feedList?.size == 0) {
                            binding?.progressLinear?.visibility = View.GONE
                            binding?.noInternetLinear?.visibility = View.VISIBLE
                            binding?.listLinear?.visibility = View.GONE
                            binding?.noDataLinear?.visibility = View.GONE
                            glideRequestManager.load(R.drawable.server_error).apply(
                                RequestOptions().fitCenter()
                            )
                                .into(binding?.noInternetImageView!!)
                            binding?.noInternetTextView?.text = getString(R.string.server_error)
                            binding?.noInternetSecTextView?.text =
                                getString(R.string.server_error_content)
                        } else {
                            if (isAddedLoader) {
                                removeLoaderFormList()
                            }
                        }


                    }
                    ResultResponse.Status.NO_DATA -> {
                        glideRequestManager.load(R.drawable.ic_no_search_data)
                            .apply(RequestOptions().fitCenter())
                            .into(binding?.noDataImageView!!)
                        binding?.progressLinear?.visibility = View.GONE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.VISIBLE

                    }
                    ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY -> {
                        mStatus = ResultResponse.Status.LOADING_COMPLETED_WITH_EMPTY

                    }
                    ResultResponse.Status.LOADING -> {

                        binding?.progressLinear?.visibility = View.VISIBLE
                        binding?.noInternetLinear?.visibility = View.GONE
                        binding?.listLinear?.visibility = View.GONE
                        binding?.noDataLinear?.visibility = View.GONE


                    }
                    else -> {

                    }
                }

            })

    }

    private fun removeLoaderFormList() {

        if (feedList != null && feedList?.size!! > 0 && feedList?.get(
                feedList?.size!! - 1
            ) != null
        ) {
            feedList?.removeAt(feedList?.size!! - 1)
            feedImageGalleryAdapter?.notifyDataSetChanged()
            isAddedLoader = false
        }
    }


    override fun onItemClick(
        item: Int, mPostId: String, mRecordId: String,
        mList: String,
        type: String
    ) {

        val detailIntent = Intent(activity, PhotoGridDetailsActivity::class.java)
        detailIntent.putExtra("mId", mId)
        detailIntent.putExtra("mPosition", item)
        detailIntent.putExtra("mRecordId", mRecordId)
        if (mType == "") {
            detailIntent.putExtra("mPath", RemoteConstant.mMediaGrid)
        } else {
            detailIntent.putExtra("mPath", "campaign")
        }
        detailIntent.putExtra("mList", mList)
        detailIntent.putExtra("type", type)
        if (musicGridViewModel.hashTagData?.name != null) {
            detailIntent.putExtra("mTitle", musicGridViewModel.hashTagData?.name)
        }
        startActivity(detailIntent)


    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        //add this code to pause videos (when app is minimised or paused)
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UploadStatus) {
        if (!event.isAdded) {
            binding?.progressCardView?.visibility = View.VISIBLE
        }
        if (isCompletedVideoEdit && !isUploadStarted) {
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()

        if (EXPORT_STARTED && !isCompletedVideoEdit) {
            binding?.progressCardView?.visibility = View.VISIBLE
            EXPORT_STARTED = false
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                binding?.nestedScrollView?.smoothScrollTo(0, 0)
            }, 1000)
        }
        if (isAddedFeed && titlePost != "" && isCompletedVideoEdit) {
//            binding?.swipeToRefresh?.isEnabled = false
            if (InternetUtil.isInternetOn()) {
                binding?.multipleActions?.visibility = View.GONE
                uploadPost()
            } else {
                AppUtils.showCommonToast(requireActivity(), getString(R.string.no_internet))
            }
            isCompletedVideoEdit = false
            isAddedFeed = false
            mAddFeeData = null
            titlePost = ""
            isUploadStarted = false
        } else if (mAddFeeData != null && !isUpdatedFeed) {
            mAddFeeData = null
            isCompletedVideoEdit = false
            isAddedFeed = false
            mAddFeeData = null
            titlePost = ""

            if (idPost != "") {
                showDialogDoYouWantSeePost(
                    idPost, true,
                    mAuthPostType
                )
            }
            isUploadStarted = false
        } else if (isUpdatedFeed) {

            /* TODO : Update Data in List */
            if (idPostPosition != null && mAddFeeData != null) {
                feedList?.get(idPostPosition!!)?.action?.post =
                    mAddFeeData
                feedImageGalleryAdapter?.notifyItemChanged(
                    idPostPosition!!,
                    mAddFeeData
                )
            }
            mAddFeeData = null
            isAddedFeed = false
            isCompletedVideoEdit = false
            mAddFeeData = null
            titlePost = ""
            isUpdatedFeed = false
            idPostPosition = null
            isUploadStarted = false
        }
    }

    private fun clearFfmpeg() {

        if (mFFTask != null && !mFFTask?.isProcessCompleted!!) {
            mFFTask?.sendQuitSignal()
        }
        if (mFFMpeg != null) {
            mFFMpeg?.deleteFFmpegBin()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun uploadPost() {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            binding?.nestedScrollView?.smoothScrollTo(0, 0)
        }, 1000)
        CommonFragmentActivity.isUploading = true
        val mListSize = feedList?.size
        binding?.progressCardView?.visibility = View.VISIBLE
        if (mListSize == 0) {
            feedImageGalleryAdapter?.submitList(feedList)
            feedImageGalleryAdapter?.notifyDataSetChanged()

            binding?.galleryRecyclerView?.visibility = View.VISIBLE
            binding?.listLinear?.visibility = View.VISIBLE
            binding?.progressLinear?.visibility = View.GONE
            binding?.noInternetLinear?.visibility = View.GONE
            binding?.noDataLinear?.visibility = View.GONE
            binding?.backgroundDimmer?.visibility = View.GONE

        } else {
            feedImageGalleryAdapter?.notifyDataSetChanged()
        }



        if (mAuthPostType == "video") {
            isUploadStarted = true
            try {
                val file = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
                if (!file.exists()) {
                    file.mkdir()
                }

                val mFileNew =
                    File(file.toString() + "/" + System.currentTimeMillis() + "socialmob1.mp4")


                var cmdNew = arrayOf(
                    "-y",
                    "-i",
                    mImageListPostList[0].photoUri,
                    "-strict",
                    "experimental",
                    "-vcodec",
                    "libx264",
                    "-preset",
                    "slow",
                    "-profile",
                    "baseline",
                    "-b:v", "1M", "-bufsize", "2M", "-b:a", "64K",
                    mFileNew.absolutePath
                )

                mImageListPostList.get(0).photoUri = mFileNew.absolutePath

                // TODO : VIDEO CROP
                mFFMpeg = FFmpeg.getInstance(activity)
                if (mFFMpeg!!.isSupported) {
                    mFFTask = mFFMpeg!!.execute(
                        cmdNew,
                        object : ExecuteBinaryResponseHandler() {
                            override fun onSuccess(message: String) {
                                cmdNew = arrayOf("")

                                feedViewModel?.getPreSignedUrl(
                                    mAuthPost,
                                    mAuthPostType,
                                    mImageListPost,
                                    mImageListPostList,
                                    mThumbListPost,
                                    postDataPost,
                                    ratio,
                                    this@ProfileMediaGridFragment
                                )

                                clearFfmpeg()
                            }

                            override fun onProgress(message: String) {
//                                Log.e("onProgress", message)
//                                println("message>>>>>>"+message)
                            }

                            override fun onFailure(message: String) {
                                binding?.progressCardView?.visibility = View.GONE
                                AppUtils.showCommonToast(
                                    activity!!,
                                    "Error occur please try again later"
                                )
                                Log.e("onFailure", message)
                                clearFfmpeg()
                            }


                            override fun onProgressPercent(percent: Float) {

                                println("percent>>>>>>" + percent / 10000)
                                if (binding?.progressBarUpload != null) {
                                    val mProgressBar = binding?.progressBarUpload
                                    val mpercent = percent / 10000

                                    if (mpercent.toInt() < 100) {
                                        mProgressBar?.progress = mpercent.toInt()
                                        if (mProgressBar?.isIndeterminate == true) {
                                            mProgressBar.isIndeterminate = false
                                        }
                                    } else {
                                        if (mProgressBar?.isIndeterminate == false) {
                                            mProgressBar.isIndeterminate = true
                                        }
                                    }
                                }
                                if (binding?.uploadTextView != null) {
                                    val uploadTextView =
                                        binding?.uploadTextView
                                    if (uploadTextView?.text != "Compressing") {
                                        uploadTextView?.text = "Compressing"
                                    }
                                }

                            }

                            override fun onStart() {
                                if (binding?.uploadTextView != null) {
                                    val uploadTextView =
                                        binding?.uploadTextView
                                    uploadTextView?.text = "Compressing"
                                }
                                if (binding?.progressBarUpload != null) {
                                    val mProgressBar = binding?.progressBarUpload
                                    mProgressBar?.progress = 10
                                }
                            }

                            override fun onFinish() {

                                if (binding?.progressBarUpload != null) {
                                    val mProgressBar = binding?.progressBarUpload
                                    mProgressBar?.progress = 0
                                }
                            }
                        },
                        1 * 1.0f / 1000
                    )
                }

            } catch (e: Exception) {
                binding?.progressCardView?.visibility = View.GONE
                AppUtils.showCommonToast(
                    requireActivity(),
                    "Error occur please try again later"
                )
                clearFfmpeg()
                e.printStackTrace()

            }

        } else {
            if (binding?.uploadTextView != null) {
                val uploadTextView = binding?.uploadTextView
                uploadTextView?.text = "Uploading"
            }
            if (binding?.progressBarUpload != null) {
                val mProgressBar = binding?.progressBarUpload
                if (mProgressBar?.isIndeterminate == false) {
                    mProgressBar.isIndeterminate = true
                }
            }
            feedViewModel?.getPreSignedUrl(
                mAuthPost,
                mAuthPostType,
                mImageListPost,
                mImageListPostList,
                mThumbListPost,
                postDataPost,
                ratio,
                this
            )

        }

        // TODO : Scroll To top

        feedViewModel?.isUploaded?.nonNull()?.observe(viewLifecycleOwner, {

            if (!feedViewModel?.isUploading!!) {
                if (it == true) {
                    CommonFragmentActivity.isUploading = false
                    if (idPost != "") {
                        showDialogDoYouWantSeePost(
                            idPost, false,
                            mAuthPostType
                        )
                    }
                    titlePost = ""
                    mAuthPost = ""
                    mAuthPostType = ""
                    mImageListPost = ""
                    mImageListPostList = java.util.ArrayList()
                    mThumbListPost = java.util.ArrayList()
                    postDataPost = PostFeedData()
                    ratio = ""

                } else {
                    binding?.progressCardView?.visibility = View.GONE
                    AppUtils.showCommonToast(
                        requireActivity(),
                        "Error occur please try again later"
                    )
                }
                binding?.multipleActions?.visibility = View.VISIBLE
            }

            feedViewModel?.isUploaded?.value = null
        })

    }

    private fun showDialogDoYouWantSeePost(
        idPost: String, isFromWritePost: Boolean, mAuthPostType: String
    ) {
        if (!isFromWritePost) {
            binding?.progressCardView?.visibility = View.GONE
            if (feedList != null && feedList?.size!! > 0) {
                val handler = Handler(Looper.getMainLooper())
                handler.postDelayed({
                    binding?.nestedScrollView?.smoothScrollTo(0, 0)
                }, 1000)
            }
        }


        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(requireActivity()) }
        val inflater: LayoutInflater = layoutInflater
        val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val okButton: Button = dialogView.findViewById(R.id.ok_button)
        val dialogDescription: TextView = dialogView.findViewById(R.id.dialog_description)
        dialogDescription.text = getString(R.string.want_to_see_post)

        cancelButton.text = getString(R.string.no)
        okButton.text = getString(R.string.yes)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        okButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            when {
                isFromWritePost -> {
                    val intent = Intent(activity, FeedTextDetailsActivity::class.java)
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)

                }
                mAuthPostType == "video" -> {
                    val intent = Intent(activity, FeedVideoDetailsActivity::class.java)
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)
                }
                else -> {
                    val intent = Intent(activity, FeedImageDetailsActivity::class.java)
                    //intent.putExtra("mPostViewType", "")
                    intent.putExtra("mPostId", idPost)
                    startActivity(intent)
                }
            }
        }
        cancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
    }

}
