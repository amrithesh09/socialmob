package com.example.sample.socialmob.view.ui.profile.userProfile

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.UserProfileActivityNewBinding
import com.example.sample.socialmob.model.chat.MsgPayload
import com.example.sample.socialmob.model.chat.SendMessage
import com.example.sample.socialmob.model.chat.WebSocketResponse
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.profile.UserProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.chat.WebSocketChatActivity
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.ui.home_module.search.SearchPeopleFragment
import com.example.sample.socialmob.view.ui.profile.MusicPlayListFragment
import com.example.sample.socialmob.view.ui.profile.MyProfileActivity
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedAdapter
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.ui.profile.followers.FollowersActivity
import com.example.sample.socialmob.view.ui.profile.followers.FollowingActivity
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.TapTarget.OnSpotlightStateChangedListener
import com.example.sample.socialmob.view.utils.TapTarget.Spotlight
import com.example.sample.socialmob.view.utils.TapTarget.shape.Circle
import com.example.sample.socialmob.view.utils.TapTarget.target.SimpleTarget
import com.example.sample.socialmob.view.utils.events.NotificationEvent
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.viewmodel.profile.ProfileViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.options_menu_user_profile_settings.view.*
import kotlinx.android.synthetic.main.user_profile_activity_new.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

@AndroidEntryPoint
class UserProfileActivity : BaseCommonActivity() {
    private var binding: UserProfileActivityNewBinding? = null
    private val profileViewModel: ProfileViewModel by viewModels()
    private val viewModelChat: WebSocketChatViewModel? by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }

    @Inject
    lateinit var glideRequestManager: RequestManager

    private var mProfileId = ""
    private var mIsOwn = ""
    private var isNewChat = false
    private var isApiCalled = false
    private var isBlocked = "false"
    private var mName: String = ""
    private var mImage: String = ""
    private var convId: String = ""
    private var isFollowing: String = ""
    private var mProfileDta: UserProfileResponseModelPayloadProfile? = null
    private var dbPlayList: MutableList<TrackListCommon>? = ArrayList()
    private val mExtraIndex = "EXTRA_INDEX"
    private val extraIndexIsFrom = "EXTRA_INDEX_IS_FROM"
    private var mCount = ""
    private var mLastClickTime: Long = 0
    private var viewPagerStateAdapter: MyViewPageStateAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.user_profile_activity_new)
        binding?.viewModel = profileViewModel
        binding?.executePendingBindings()
        if (intent != null && intent.extras != null) {
            val extras: Bundle? = intent.extras
            if (extras?.containsKey("mProfileId")!!) {
                mProfileId = extras.getString("mProfileId")!!
            }
            if (extras.containsKey("mIsOwn")) {
                mIsOwn = extras.getString("mIsOwn")!!
            }
            // Social-mob official id's user can't un-follow
            when (mProfileId) {
                "5a59a1bf9c57150978e9e26a" -> {
                    binding?.followProfileTextView?.visibility = View.GONE
                }
                "59bf615b5aff121370572ace" -> {
                    binding?.followProfileTextView?.visibility = View.GONE
                }
                "5e6cefd07b9b571850cf43ed" -> {
                    binding?.followProfileTextView?.visibility = View.GONE
                }
            }

        }

        if (intent?.action != null && intent?.data != null) {
            val data: Uri? = intent?.data
            if (data?.lastPathSegment != null) {
                mProfileId = data.lastPathSegment!!
                // Social-mob official id's user can't un-follow
                when (mProfileId) {
                    "5a59a1bf9c57150978e9e26a" -> {
                        binding?.followProfileTextView?.visibility = View.GONE
                    }
                    "59bf615b5aff121370572ace" -> {
                        binding?.followProfileTextView?.visibility = View.GONE
                    }
                    "5e6cefd07b9b571850cf43ed" -> {
                        binding?.followProfileTextView?.visibility = View.GONE
                    }
                }
            }
        }

        /**
         *  To Check passed id is user id if true navigate to my profile
         *  Condition on mention click from feed page @User
         */
        if (mProfileId == SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mProfileId,
                ""
            )
        ) {
            val intent = Intent(this, MyProfileActivity::class.java)
            intent.putExtra("mProfileId", mProfileId)
            startActivity(intent)
            this.finish()
        } else {

            if (mProfileId != "") {
                checkInternetUserProfile(binding?.profileSettingsImageView!!)
            }

            binding?.profileSettingsImageView?.setOnClickListener {
                profileSettings(mProfileId)
            }
            binding?.followProfileTextView?.setOnClickListener {
                if (isFollowing == "following") {
                    val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
                    val inflater: LayoutInflater = layoutInflater
                    val dialogView: View = inflater.inflate(R.layout.common_dialog, null)
                    dialogBuilder.setView(dialogView)
                    dialogBuilder.setCancelable(false)

                    val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
                    val okButton: Button = dialogView.findViewById(R.id.ok_button)

                    val b: AlertDialog = dialogBuilder.create()
                    b.show()
                    okButton.setOnClickListener {
                        onFollowItemClick(isFollowing, mProfileId)
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                    }
                    cancelButton.setOnClickListener {
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        binding?.followProfileTextView?.isClickable = true

                    }


                } else {
                    onFollowItemClick(isFollowing, mProfileId)
                }
                binding?.followProfileTextView?.isClickable = false
            }
            binding?.profileTrackTextView?.setOnClickListener {
                playSingleTrack()
            }
            followersClick.setOnClickListener {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return@setOnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                followersClick()
            }
            followingClick.setOnClickListener {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return@setOnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                followingClick()
            }
        }
        profileViewModel.profile.observe(this, Observer {

            if (it?.followingCount != null) {
                mCount = it.followingCount
            }
        })

        binding?.pennyImageView?.setOnClickListener {

            if (convId == "") {
                pennySend()
            } else {
                val intent = Intent(this, WebSocketChatActivity::class.java)
                intent.putExtra("convId", convId)
                intent.putExtra("mProfileId", mProfileId)
                intent.putExtra("mUserName", mProfileDta?.username)
                intent.putExtra("mUserImage", mProfileDta?.pimage)
                intent.putExtra("mStatus", "")
                intent.putExtra("mLastMessageUser", "")
                startActivityForResult(intent, 112)
            }
        }
        viewModelChat?.newMessage?.observe(this, { newMessage ->

            if (newMessage != null && newMessage != "") {
                if (isNewChat) {
                    handleMessage(newMessage)
                }
            }
        })
    }

    private fun playSingleTrack() {

        if (dbPlayList?.size ?: 0 > 0) {
            if (dbPlayList?.get(0)?.id != null && dbPlayList?.get(0)?.id != "") {
                profileViewModel.createDBPlayList(dbPlayList,"My playlist")

                val detailIntent = Intent(this, NowPlayingActivity::class.java)
                detailIntent.putExtra(mExtraIndex, 0)
                detailIntent.putExtra(extraIndexIsFrom, "dblistUserProfile")
                startActivity(detailIntent)
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
            }
        }

    }

    private fun onFollowItemClick(isFollowing: String, mId: String) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            when (isFollowing) {
                "following" -> unFollowUserApi(mId)
                "none" -> followUserApi(mId)
                "request pending" -> requestCancel(mId)
            }
            observeFollowRequest()
        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    private fun observeFollowRequest() {
        profileViewModel.followResponseModel.nonNull().observe(this, {
            if (it?.payload?.message != null) {
                mProfileDta?.relation = it.payload?.message
                isFollowing = it.payload?.message ?: ""
            } else {
                mProfileDta?.relation = "none"
                isFollowing = "none"
            }
            binding?.profileDataUser = mProfileDta

            binding?.followProfileTextView?.isClickable = true

            if (it.payload != null && mProfileDta?.privateProfile == true) {
                if (mProfileDta?.relation == "following") {
                    penny_image_view.visibility = View.VISIBLE
                } else {
                    penny_image_view.visibility = View.INVISIBLE
                }
            } else {
                penny_image_view.visibility = View.VISIBLE
            }


        })
    }

    fun checkInternetUserProfile(view: View) {
        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            val mAuth = RemoteConstant.getAuthUser(this@UserProfileActivity, mProfileId)

            profileViewModel.getUserProfile(mAuth, mProfileId)
            binding?.isInternetAvailable = true
            binding?.isLoadingUserProfile = true
            observeProfileData()

        } else {
            binding?.isInternetAvailable = false
            binding?.isLoadingUserProfile = false

            val shake: Animation =
                AnimationUtils.loadAnimation(this@UserProfileActivity, R.anim.shake)
            binding?.noInternetLinear?.startAnimation(shake) // starts animation
        }
    }

    private fun observeProfileData() {

        profileViewModel.userProfileResponseModel.observe(this, {
            if (it?.payload?.relation != null && it.payload?.relation == "blocked") {
                AppUtils.showCommonToast(this, "Cannot show content due to privacy concerns")
                this.finish()
            } else {
                if (it.payload?.profile?.celebrityVerified != null && it.payload?.profile?.celebrityVerified == "true") {
                    binding?.verifiedTextView?.visibility = View.VISIBLE
                }
                if (it.payload?.profile?.relation != null) {
                    isFollowing = it.payload?.profile?.relation ?: ""
                    if (it.payload?.profile?.relation!! == "blocked") {
                        AppUtils.showCommonToast(
                            this,
                            "Cannot show content due to privacy concerns"
                        )
                        this.finish()
                    }
                }
                binding?.isLoadingUserProfile = false
                mProfileDta = it.payload?.profile!!
                if (
                    it?.payload != null &&
                    it?.payload?.conv_id != null
                ) {
                    convId = it.payload?.conv_id ?: ""
                }
                binding?.profileDataUser = mProfileDta

                if (it.payload?.profile?.privateProfile == true) {
                    if (it.payload?.profile?.relation == "following") {

                        private_profile_linear.visibility = View.GONE
                        user_profile_viewpager.visibility = View.VISIBLE
                    } else {
                        private_profile_linear.visibility = View.VISIBLE
                        glideRequestManager.load(R.drawable.private_account).apply(RequestOptions().fitCenter())
                            .into(binding?.privateImageView!!)
                        user_profile_viewpager.visibility = View.GONE
                    }
                } else {
                    private_profile_linear.visibility = View.GONE
                    user_profile_viewpager.visibility = View.VISIBLE
                }


                if (it.payload != null && it.payload?.profile != null && it.payload?.profile?.name != null) {
                    mName = it.payload?.profile?.name ?: ""
                }
                if (it.payload != null && it.payload?.profile != null && it.payload?.profile?.pimage != null) {
                    mImage = it.payload?.profile?.pimage ?: ""
                }
                binding?.profileTrack = it.payload
                if (it.payload?.profileAnthemTrack != null) {
                    dbPlayList?.clear()
                    dbPlayList?.add(it.payload?.profileAnthemTrack!!)
                }

                if (it.payload != null) {
                    if (it.payload?.profile?.about == null || it.payload?.profile?.about == "") {
                        binding?.bioTextView?.visibility = View.GONE
                    } else {
                        binding?.bioTextView?.visibility = View.VISIBLE
                    }
                }
                if (it.payload != null && it.payload?.profile?.privateProfile == true) {
                    if (it.payload?.profile?.relation == "following") {
                        penny_image_view.visibility = View.VISIBLE
                    } else {
                        penny_image_view.visibility = View.INVISIBLE
                    }
                } else {
                    penny_image_view.visibility = View.VISIBLE
                    tapTargetPenny()
                }
            }


        })




        setStatePageAdapter()
    }

    private fun profileSettings(mProfileId: String) {
        val viewGroup: ViewGroup? = null
        val viewProfileSettings =
            LayoutInflater.from(this@UserProfileActivity)
                .inflate(R.layout.options_menu_user_profile_settings, viewGroup)
        val mQQPop =
            PopupWindow(
                viewProfileSettings,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        mQQPop.animationStyle = R.style.RightTopPopAnim
        mQQPop.isFocusable = true
        mQQPop.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mQQPop.isOutsideTouchable = true
        mQQPop.showAsDropDown(binding?.profileSettingsImageView, -250, -15)


        viewProfileSettings.report_text_view.setOnClickListener {
            reportProfile(mProfileId)
            mQQPop.dismiss()
        }
        viewProfileSettings.block_text_view.setOnClickListener {

            confirmBlockUserAlert(mProfileId)
            mQQPop.dismiss()
        }
        viewProfileSettings.share_profile_text_view.setOnClickListener {
            ShareAppUtils.shareProfile(this, mProfileId)
            mQQPop.dismiss()
        }

    }

    override fun onResume() {
        super.onResume()
        isNewChat = false
    }

    private fun reportProfile(mProfileId: String) {

        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(this) }
        val inflater: LayoutInflater = layoutInflater
        val dialogView: View = inflater.inflate(R.layout.report_dialog, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val okButton: Button = dialogView.findViewById(R.id.ok_button)
        val dialogDescription: EditText = dialogView.findViewById(R.id.dialog_description)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        okButton.setOnClickListener {
            if (dialogDescription.text.isNotEmpty()) {
                val fullData = RemoteConstant.getEncryptedString(
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                    SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                    RemoteConstant.mBaseUrl +
                            RemoteConstant.profileData +
                            RemoteConstant.mSlash + mProfileId + RemoteConstant.mPathReport,
                    RemoteConstant.putMethod
                )
                val mAuth =
                    RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mAppId,
                        ""
                    ) + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                profileViewModel.reportProfile(
                    mAuth,
                    mProfileId,
                    dialogDescription.text.toString()
                )

                profileViewModel.reportProfileResponse.nonNull().observe(this, {
                    if (it?.payload != null && it.payload?.success != null) {
                        if (it.payload?.success == true) {
                            onBackPressed()
                        }
                    }
                })
                b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
            } else {
                AppUtils.showCommonToast(this, "The message box is blank.")
            }
        }
        cancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()

        }


    }

    private fun confirmBlockUserAlert(mProfileId: String) {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(it) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.blocked_user_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val alertText: TextView = dialogView.findViewById(R.id.alert_text)
        val cancelButton: Button = dialogView.findViewById(R.id.cancel_button)
        val unblockButton: Button = dialogView.findViewById(R.id.unblock_button)

        unblockButton.text = getString(R.string.block)
        alertText.text = getString(R.string.are_you_sure_you_d_like_to_block_this_user)

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        unblockButton.setOnClickListener {
            val mAuth = RemoteConstant.getAuthBlockUser(this@UserProfileActivity, mProfileId)

            profileViewModel.blockUser(mAuth, mProfileId)
            isApiCalled = true
            isBlocked = "true"

            profileBackPress(binding?.profileBackImageView!!)
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }
        cancelButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun pennySend() {
        val viewGroup: ViewGroup? = null
        val dialogBuilder: AlertDialog.Builder = this.let { AlertDialog.Builder(it) }
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_penny_dialog, viewGroup)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val messageTextView: TextView = dialogView.findViewById(R.id.message_text_view)
        val descriptionTextView: TextView = dialogView.findViewById(R.id.description_textView)
        val timeLinear: LinearLayout = dialogView.findViewById(R.id.time_linear)
        val pennyEditText: EditText = dialogView.findViewById(R.id.penny_edit_text)
        val closeButton: Button = dialogView.findViewById(R.id.close_button)
        val replayButton: Button = dialogView.findViewById(R.id.replay_button)
        val pennyProfileImageView: ImageView =
            dialogView.findViewById(R.id.penny_profile_image_view)
        val pennyOptionsImageView: ImageView =
            dialogView.findViewById(R.id.penny_options_image_view)
        replayButton.text = getString(R.string.send)
        //TODO : Custom For Send Penny

        if (mImage != "") {
            glideRequestManager.load(RemoteConstant.getImageUrlFromWidth(mImage, 100)).apply(
                RequestOptions.placeholderOf(R.drawable.emptypicture).error(R.drawable.emptypicture)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH)
            ).into(pennyProfileImageView)
        } else {
            glideRequestManager.load(R.drawable.emptypicture).into(pennyProfileImageView)
        }
        messageTextView.visibility = View.GONE
        timeLinear.visibility = View.GONE
        pennyOptionsImageView.visibility = View.GONE
        descriptionTextView.text = "Send Penny to $mName"

        val b: AlertDialog = dialogBuilder.create()
        b.show()
        replayButton.setOnClickListener {
            if (pennyEditText.text.isNotEmpty()) {
                if (viewModelChat?.isWebSocketOpen!!) {
                    replayButton.isClickable = false
                    replayButton.isEnabled = false

                    val mSendMessage = SendMessage()
                    val mMsgPayload = MsgPayload()

                    mSendMessage.user = mProfileId
                    mSendMessage.type = "message"

                    mMsgPayload.user = mProfileId
                    mMsgPayload.msgType = "text"
                    mMsgPayload.text = pennyEditText.text.toString()
                    mMsgPayload.timestamp = System.currentTimeMillis()
                    mSendMessage.msgPayload = mMsgPayload

                    val mGson = Gson()
                    val mMessage =
                        mGson.toJson(mSendMessage)


                    viewModelChat?.sendMessageSm(mMessage)
                    isNewChat = true
                    Handler().postDelayed({
//                        AppUtils.showCommonToast(this, "Message send ")
                        b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
                        AppUtils.hideKeyboard(this@UserProfileActivity, binding?.pennyImageView!!)
                    }, 500)
                } else {
                    if (!viewModelChat?.isWebSocketOpen!! && !viewModelChat?.isWebSocketOpenCalled!!) {
                        viewModelChat?.connectToSocketSm()
                    }
                    Toast.makeText(this, "Waiting for connection", Toast.LENGTH_SHORT).show()
                }
            } else {
                AppUtils.showCommonToast(this, "You cannot send Penny with an empty note.")
            }
        }
        closeButton.setOnClickListener {
            b.getButton(AlertDialog.BUTTON_NEGATIVE)?.performClick()
        }


    }

    private fun handleMessage(text: String?) {
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)
            if (webSocketResponse?.type != null) {
                if (webSocketResponse.type == "message") {
                    viewModelChat?.newMessage?.removeObservers(this)
                    val convId1 = webSocketResponse.msgPayload?.convId
                    val intent = Intent(this, WebSocketChatActivity::class.java)
                    intent.putExtra("convId", convId1)
                    intent.putExtra("mProfileId", mProfileId)
                    intent.putExtra("mUserName", mProfileDta?.username)
                    intent.putExtra("mUserImage", mProfileDta?.pimage)
                    intent.putExtra("mStatus", "")
                    intent.putExtra("mLastMessageUser", "")
                    startActivityForResult(intent, 112)
                } else if (webSocketResponse.type == "desktop-notification") {

                    val mFromProfileId = webSocketResponse.from!!
                    val pimage = webSocketResponse.msgPayload?.message_sender?.pimage ?: ""
                    val mUserName = webSocketResponse.msgPayload?.message_sender?.username ?: ""
                    val mMesssage = webSocketResponse.msgPayload?.text
                    val mTitle = "$mUserName - $mMesssage"
                    val sb: Spannable = SpannableString(mTitle)
                    sb.setSpan(
                        StyleSpan(Typeface.BOLD),
                        0,
                        mUserName.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )

                    val actionTitleNew = sb.toString()


                    val mMyProfileId = SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mProfileId, ""
                    )
                    val mConnectedUser = SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mConnectedUser, ""
                    )
                    if (mFromProfileId != mMyProfileId) {
                        if (mFromProfileId != mConnectedUser || !MyApplication.isForeground) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                val mNotificationHelper = NotificationHelper(this)
                                val notificationBuilder: Notification.Builder =
                                    mNotificationHelper.getNotificationDM(
                                        "",
                                        actionTitleNew,
                                        convId,
                                        mFromProfileId,
                                        mUserName,
                                        pimage
                                    )
                                if (notificationBuilder != null) {
                                    mNotificationHelper.notify(
                                        System.currentTimeMillis().toInt(),
                                        notificationBuilder
                                    )
                                }
                            } else {
                                val intent = Intent(this, WebSocketChatActivity::class.java)

                                intent.putExtra("convId", convId)
                                intent.putExtra("mProfileId", mFromProfileId)
                                intent.putExtra("mUserName", mUserName)
                                intent.putExtra("mUserImage", pimage)


                                val pendingIntent = PendingIntent.getActivity(
                                    this, 0 /* Request code */, intent,
                                    PendingIntent.FLAG_ONE_SHOT
                                )
                                val sound: Uri =
                                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.packageName + "/" + R.raw.penny_sound) //Here is FILE_NAME is the name of file that you want to play


                                val notificationBuilder =
                                    NotificationCompat.Builder(
                                        this,
                                        getString(R.string.default_notification_channel_id)
                                    )
                                        .setSmallIcon(R.mipmap.ic_custom_launcher_new_color)
                                        .setContentIntent(pendingIntent)
                                        .setContentText(actionTitleNew)
                                notificationBuilder.setSound(sound)
//                            notificationBuilder.setDefaults(DEFAULT_SOUND) //Important for heads-up notification
//                            notificationBuilder.priority = Notification.PRIORITY_MAX
                                val notificationManager =
                                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                                    val channel = NotificationChannel(
                                        getString(R.string.default_notification_channel_id_chat),
                                        "chat",
                                        NotificationManager.IMPORTANCE_HIGH
                                    )
                                    notificationManager.createNotificationChannel(channel)
                                }
                                notificationBuilder.setAutoCancel(true)
                                notificationManager.notify(
                                    System.currentTimeMillis().toInt(),
                                    notificationBuilder.build()
                                )
                            }
                            EventBus.getDefault()
                                .post(NotificationEvent("im-desktop-notification"))
                        }
                    }

                }
            }

        } catch (e: Exception) {
            AppUtils.showCommonToast(this, "" + e.printStackTrace())
            e.printStackTrace()
        }

    }

    private fun unFollowUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            ) + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        profileViewModel.unFollowProfile(mAuth, mId, mCount)
    }

    private fun followUserApi(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.profileData + RemoteConstant.mSlash + mId +
                    RemoteConstant.pathFollow, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            ) + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        profileViewModel.followProfile(mAuth, mId)
    }

    private fun requestCancel(mId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.profileData +
                    RemoteConstant.mSlash + mId + RemoteConstant.mPathFollowCancel,
            RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.mAppId,
                ""
            ) + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        profileViewModel.cancelRequest(mAuth, mId)

    }

    fun profileBackPress(view: View) {
        onBackPressed()
    }

    private fun followingClick() {
        val intent = Intent(this, FollowingActivity::class.java)
        intent.putExtra("mProfileId", mProfileId)
        startActivity(intent)

    }

    private fun followersClick() {
        val intent = Intent(this, FollowersActivity::class.java)
        intent.putExtra("mProfileId", mProfileId)
        startActivity(intent)

    }

    private fun setStatePageAdapter() {
        user_profile_viewpager.offscreenPageLimit = 3
        viewPagerStateAdapter = MyViewPageStateAdapter(supportFragmentManager)
        viewPagerStateAdapter?.addFragment(
            ProfileFeedFragment.newInstance(mProfileId, "false"),
            "POSTS"
        )
//        viewPagerStateAdapter.addFragment(
//            ProfileMusicFragment.newInstance(mProfileId, null),
//            "Music"
//        )
        viewPagerStateAdapter?.addFragment(
            ProfileMediaGridFragment.newInstance(mProfileId, ""),
            "GALLERY"
        )
        viewPagerStateAdapter?.addFragment(
            MusicPlayListFragment.newInstance(mProfileId, "false"),
            "PLAYLISTS"
        )
        user_profile_viewpager.adapter = viewPagerStateAdapter
        profile_tabs.setupWithViewPager(user_profile_viewpager)

        user_profile_viewpager.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {

                if (ProfileFeedAdapter.helper != null) {
                    ProfileFeedAdapter.helper?.pause()
                }
            }

        })
//        binding?.viewRefreshHeader?.postDelayed({
//            binding?.viewRefreshHeader?.stopRefresh()
//        }, 1000)


    }

    private fun tapTargetPenny() {

        if (!SharedPrefsUtils.getBooleanPreference(
                this,
                RemoteConstant.isTapTargetPlayedPenny,
                false
            )
        ) {
            SharedPrefsUtils.setBooleanPreference(
                this,
                RemoteConstant.isTapTargetPlayedPenny,
                true
            )
            Handler().postDelayed({

                val pennyRadius = 100F
                val pennyTarget = SimpleTarget.Builder(this@UserProfileActivity)
                    .setPoint(findViewById<View>(R.id.penny_image_view))
                    .setShape(Circle(pennyRadius))
                    .setTitle("Penny")
                    .setDescription("Your own personal message-bearer")
                    .setOverlayPoint(50f, 200f)
                    .build()


                // create spotlight
                Spotlight.with(this)
                    .setOverlayColor(R.color.background_shade)
                    .setDuration(1000L)
                    .setAnimation(DecelerateInterpolator(50f))
                    .setTargets(pennyTarget)
                    .setClosedOnTouchedOutside(true)
                    .setOnSpotlightStateListener(object : OnSpotlightStateChangedListener {
                        override fun onStarted() {
                        }

                        override fun onEnded() {

                        }
                    })
                    .start()
            }, 1000)
        }


    }

    class MyViewPageStateAdapter(fm: androidx.fragment.app.FragmentManager) :
        FragmentStatePagerAdapter(fm) {
        private val fragmentList: MutableList<androidx.fragment.app.Fragment> = ArrayList()
        private val fragmentTitleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentTitleList[position]
        }

        fun addFragment(fragment: androidx.fragment.app.Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }

    override fun onBackPressed() {
        SearchPeopleFragment.isFollow = isFollowing
        SearchPeopleFragment.mProfileId = mProfileId
        SearchPeopleFragment.isBlocked = isBlocked
        super.onBackPressed()
        AppUtils.hideKeyboard(this@UserProfileActivity, profile_back_image_view)
    }


}