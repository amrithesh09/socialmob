package com.example.sample.socialmob.view.ui.sign_up

import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AppUtils
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import com.transitionseverywhere.TransitionManager
import com.transitionseverywhere.extra.Scale
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.create_profile_birth_date.*
import java.util.*

@AndroidEntryPoint
class BirthDateFragment : Fragment() {
    private var isExpanded = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        birth_date_linear.setOnClickListener {
            expandBirthDate()
        }
        birth_date_calender_linear.setOnClickListener {
            calenderDialog()
        }
    }

    private fun expandBirthDate() {
        if (!isExpanded) {
            isExpanded = true
            hideLinearLayout(birth_date_linear)
            showLinearLayout(birth_date_split_linear)
            Handler().postDelayed({
                // TODO : To visible button
                date_linear.visibility = View.VISIBLE
                year_linear.visibility = View.VISIBLE
                birth_date_calender_linear.visibility = View.VISIBLE

                Sequent.origin(date_linear).anim(activity, Animation.FADE_IN_RIGHT).delay(0)
                    .offset(0)
                    .start()
                Sequent.origin(year_linear).anim(activity, Animation.FADE_IN_LEFT).delay(0)
                    .offset(0)
                    .start()
                Sequent.origin(birth_date_calender_linear).anim(activity, Animation.BOUNCE_IN)
                    .delay(0).offset(0)
                    .start()
            }, 1000)
        }
    }

    private fun calenderDialog() {
        val c = Calendar.getInstance()
        var day = c.get(Calendar.DAY_OF_MONTH)
        var month = c.get(Calendar.MONTH)
        var year = c.get(Calendar.YEAR)


        val dpd = DatePickerDialog(
            requireActivity(),
            { _, selyear, monthOfYear, dayOfMonth ->
                day = dayOfMonth
                month = monthOfYear + 1
                year = selyear
                val userAge = GregorianCalendar(year, month, day)
                val minAdultAge = GregorianCalendar()
                minAdultAge.add(Calendar.YEAR, -18)
                if (minAdultAge.before(userAge)) {
                    AppUtils.showCommonToast(
                        requireActivity(),
                        "Sorry!!! You are not allowed to use this app"
                    )
                } else {
                    (activity as SignUpActivity).mBirthDate =
                        year.toString() + "-" + month + "-" + dayOfMonth
                    date_text_view.text = day.toString()
                    month_text_view.text = month.toString()
                    year_text_view.text = year.toString()
                }

                var zodiacSign = ""
                when (month) {
                    1 -> zodiacSign = if (day < 20) {
                        "Capricon"
                    } else {
                        "Aquarius"
                    }
                    2 -> zodiacSign = if (day < 19) {
                        "Aquarius"
                    } else {
                        "Pisces"
                    }
                    3 -> zodiacSign = if (day < 21) {
                        "Pisces"
                    } else {
                        "Aries"
                    }
                    4 -> zodiacSign = if (day < 20) {
                        "Aries"
                    } else {
                        "Taurus"
                    }
                    5 -> zodiacSign = if (day < 21) {
                        "Taurus"
                    } else {
                        "Gemini"
                    }
                    6 -> zodiacSign = if (day < 21) {
                        "Gemini"
                    } else {
                        "Cancer"
                    }
                    7 -> zodiacSign = if (day < 23) {
                        "Cancer"
                    } else {
                        "Leo"
                    }
                    8 -> zodiacSign = if (day < 23) {
                        "Leo"
                    } else {
                        "Virgo"
                    }
                    9 -> zodiacSign = if (day < 23) {
                        "Virgo"
                    } else {
                        "Libra"
                    }
                    10 -> zodiacSign = if (day < 23) {
                        "Libra"
                    } else {
                        "Scorpio"
                    }
                    11 -> zodiacSign = if (day < 22) {
                        "Scorpio"
                    } else {
                        "Sagittarius"
                    }
                    12 -> zodiacSign = if (day < 22) {
                        "Sagittarius"
                    } else {
                        "Capricon"
                    }
                }

                (activity as SignUpActivity).mZodiacSign = zodiacSign


            }, year, month, day
        )
        if (date_text_view.text != null && date_text_view.text != "") {
            val mDate = date_text_view.text.toString()
            val mMonth = month_text_view.text.toString()
            val mYear = year_text_view.text.toString()
            if (mYear != "" && mMonth != "" && mDate != "") {
                dpd.updateDate(mYear.toInt(), mMonth.toInt().minus(1), mDate.toInt())
            }
        }
        dpd.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.create_profile_birth_date, container, false)
    }

    private fun hideLinearLayout(visibleLayout: LinearLayout) {
        TransitionManager.beginDelayedTransition(transitions_container_birth_date, Scale())
        visibleLayout.visibility = View.GONE
    }

    private fun showLinearLayout(visibleLayout: LinearLayout) {
        TransitionManager.beginDelayedTransition(transitions_container_birth_date, Scale())
        visibleLayout.visibility = View.VISIBLE
    }
}
