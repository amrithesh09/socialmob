package com.example.sample.socialmob.view.ui.sign_up

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.card_four_fragment.*
import javax.inject.Inject

@AndroidEntryPoint
class CardFourFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.card_four_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        glideRequestManager.load(R.drawable.card_four).into(card_four_image_view)
    }
}
