package com.example.sample.socialmob.view.ui.sign_up

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.card_one_fragment.*
import javax.inject.Inject

@AndroidEntryPoint
class CardOneFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.card_one_fragment, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mContent = "Hello " + SharedPrefsUtils.getStringPreference(
            activity,
            RemoteConstant.mUserName,
            ""
        ) + ",\n You’ve entered \nSocialmob."

        card_one_title.text = mContent

        glideRequestManager.load(R.drawable.card_one).into(card_one_image_view)
    }
}
