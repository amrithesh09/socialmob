package com.example.sample.socialmob.view.ui.sign_up

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.card_two_fragment.*
import javax.inject.Inject

@AndroidEntryPoint
class CardTwoFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.card_two_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        glideRequestManager.load(R.drawable.card_two).into(card_two_image_view)
    }
}
