package com.example.sample.socialmob.view.ui.sign_up

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.sample.socialmob.R

class CreateProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        hideStatusBar()
        setContentView(R.layout.create_profile_email)

//        verifyed_image_view.setOnClickListener {
//
//            val intent = Intent(this, MainActivity::class.java)
//            startActivity(intent)
//            finish()
//
//        }


    }

    private fun hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.window
            .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

}