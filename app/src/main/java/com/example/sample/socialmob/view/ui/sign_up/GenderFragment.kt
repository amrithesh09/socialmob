package com.example.sample.socialmob.view.ui.sign_up

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.create_profile_gender.*

@AndroidEntryPoint
class GenderFragment : Fragment() {
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            hideKeyboard()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO : Default Color
        changeBg(male_text_view, female_text_view, other_text_view, "male")
        (activity as SignUpActivity).mGender = "male"

        male_text_view.setOnClickListener {
            changeBg(male_text_view, female_text_view, other_text_view, "male")
        }

        female_text_view.setOnClickListener {
            changeBg(female_text_view, male_text_view, other_text_view, "female")
        }

        other_text_view.setOnClickListener {
            changeBg(other_text_view, female_text_view, male_text_view, "other")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        return layoutInflater.inflate(R.layout.create_profile_gender, container, false)
    }
    /**
     * Change selection
     */
    private fun changeBg(
        textView1: TextView, textView2: TextView, textView3: TextView, mGender: String
    ) {

        textView1.setTextColor(Color.parseColor("#FFFFFF"))
        textView2.setTextColor(Color.parseColor("#000000"))
        textView3.setTextColor(Color.parseColor("#000000"))
        when (mGender) {
            "male" -> {
                (activity as SignUpActivity).mGender = "male"
                gender_male_image_view.visibility = View.VISIBLE
                gender_female_image_view.visibility = View.GONE
                gender_other_image_view.visibility = View.GONE
                select_gender_title_text_view.setTextColor(Color.parseColor("#1b98ff"))
                select_gender_title_view.setBackgroundColor(Color.parseColor("#1b98ff"))
                textView1.setBackgroundResource(R.drawable.rounded_button_gender_male)
                textView2.setBackgroundResource(R.drawable.rounded_button_gender_white)
                textView3.setBackgroundResource(R.drawable.rounded_button_gender_white)
            }
            "female" -> {
                (activity as SignUpActivity).mGender = "female"
                gender_male_image_view.visibility = View.GONE
                gender_female_image_view.visibility = View.VISIBLE
                gender_other_image_view.visibility = View.GONE
                select_gender_title_text_view.setTextColor(Color.parseColor("#C84F9D"))
                select_gender_title_view.setBackgroundColor(Color.parseColor("#C84F9D"))
                textView1.setBackgroundResource(R.drawable.rounded_button_gender_female)
                textView2.setBackgroundResource(R.drawable.rounded_button_gender_white)
                textView3.setBackgroundResource(R.drawable.rounded_button_gender_white)
            }
            "other" -> {
                (activity as SignUpActivity).mGender = "other"
                gender_male_image_view.visibility = View.GONE
                gender_female_image_view.visibility = View.GONE
                gender_other_image_view.visibility = View.VISIBLE
                select_gender_title_text_view.setTextColor(Color.parseColor("#36489e"))
                select_gender_title_view.setBackgroundColor(Color.parseColor("#36489e"))
                textView1.setBackgroundResource(R.drawable.rounded_button_gender_other)
                textView2.setBackgroundResource(R.drawable.rounded_button_gender_white)
                textView3.setBackgroundResource(R.drawable.rounded_button_gender_white)
            }
        }

    }

    fun hideKeyboard() {
        val inputManager: InputMethodManager =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(view?.windowToken, InputMethodManager.SHOW_FORCED)
    }
}
