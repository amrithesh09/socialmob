package com.example.sample.socialmob.view.ui.sign_up

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.CreateProfileEmailBinding
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.create_profile_email.*

@AndroidEntryPoint
class ProfileEmailFragment : Fragment() {
    private var mIsVisibleToUser: Boolean? = null
    private var binding: CreateProfileEmailBinding? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        mIsVisibleToUser = isVisibleToUser
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mIsVisibleToUser == true) {
            // TODO : Layout Animation
            Sequent.origin(linear_profile).anim(activity, Animation.BOUNCE_IN).delay(100)
                .offset(100)
                .start()
        }
        sign_up_email_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                (activity as SignUpActivity).mEmailAdress = p0.toString()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.create_profile_email, container, false)
        return binding?.root
    }
}