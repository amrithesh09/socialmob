package com.example.sample.socialmob.view.ui.sign_up

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.create_profile_image.*
import javax.inject.Inject

@AndroidEntryPoint
class ProfileImageFragment : Fragment() {
    @Inject
    lateinit var glideRequestManager: RequestManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        first_preview_image_view.visibility = View.VISIBLE
        create_profile_image_view.visibility = View.GONE

        skip_text_view.setOnClickListener {
            if (activity is SignUpActivity) {
                (activity as SignUpActivity).nextPage()
            }
        }
        profile_camera_image_view.setOnClickListener {
            val intent = Intent(activity, PickerActivity::class.java)
            intent.putExtra("title", "UPLOAD PHOTO")
            intent.putExtra("IMAGES_LIMIT", 1)
            intent.putExtra("VIDEOS_LIMIT", 1)
            intent.putExtra("IS_FROM_CREATE_PROFILE", true)
            intent.putExtra("SELECT_POSITION", 0)
            startActivity(intent)

        }
        profile_gallery_image_view.setOnClickListener {
            val intent = Intent(activity, PickerActivity::class.java)
            intent.putExtra("title", "UPLOAD PHOTO")
            intent.putExtra("IMAGES_LIMIT", 1)
            intent.putExtra("VIDEOS_LIMIT", 1)
            intent.putExtra("IS_FROM_CREATE_PROFILE", true)
            intent.putExtra("SELECT_POSITION", 1)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        // TODO : Load Image
        if (SharedPrefsUtils.getStringPreference(activity, "PROFILE_PIC_FILE_NAME", "") != "") {
            first_preview_image_view.visibility = View.GONE
            create_profile_image_view.visibility = View.VISIBLE
            glideRequestManager
                .load(SharedPrefsUtils.getStringPreference(activity, "PROFILE_PIC_FILE_NAME", ""))
                .into(create_profile_image_view)
        } else {
            first_preview_image_view.visibility = View.VISIBLE
            create_profile_image_view.visibility = View.GONE
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.create_profile_image, container, false)
    }

    fun hideKeyboard() {
        val inputManager: InputMethodManager =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(view?.windowToken, InputMethodManager.SHOW_FORCED)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            hideKeyboard()
        }
    }
}