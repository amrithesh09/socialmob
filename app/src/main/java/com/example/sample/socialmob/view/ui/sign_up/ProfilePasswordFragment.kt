package com.example.sample.socialmob.view.ui.sign_up

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.profile_create_password.*

@AndroidEntryPoint
class ProfilePasswordFragment : Fragment() {
    private var mIsShown: Boolean? = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        enter_password_linear.setOnTouchListener { v, _ ->
            if (mIsShown == false) {
                re_enter_password_linear.visibility = View.VISIBLE
                Sequent.origin(re_enter_password_linear).anim(activity, Animation.FADE_IN_DOWN)
                    .delay(100).offset(100)
                    .start()
                mIsShown = true
            }
            false
        }

        enter_password_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                (activity as SignUpActivity).mPassword = p0.toString()
                if (mIsShown == false) {
                    re_enter_password_linear.visibility = View.VISIBLE
                    Sequent.origin(re_enter_password_linear).anim(activity, Animation.FADE_IN_DOWN)
                        .delay(100)
                        .offset(100)
                        .start()
                    mIsShown = true
                }
            }
        })
        re_enter_password_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                (activity as SignUpActivity).mPasswordRenter = p0.toString()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.profile_create_password, container, false)
    }
}