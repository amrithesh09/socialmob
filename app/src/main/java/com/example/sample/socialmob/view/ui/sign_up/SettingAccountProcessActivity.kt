package com.example.sample.socialmob.view.ui.sign_up

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.profile.ProfileRegisterData
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.viewmodel.login.SignUpViewModel
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.setting_account_process_activity.*
import javax.inject.Inject

@AndroidEntryPoint
class SettingAccountProcessActivity : AppCompatActivity() {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private val signUpViewModel: SignUpViewModel by viewModels()

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.setting_account_process_activity)

        gif.visibility = View.VISIBLE
        gif.play()
        gif.gifResource = R.drawable.loadergif
        val profileData: ProfileRegisterData?
        if (intent != null && intent.extras != null && intent?.extras?.getSerializable("profileData") != null) {
            profileData = intent?.extras?.getSerializable("profileData") as? ProfileRegisterData

            // TODO : Register API
            signUpViewModel.mobEnter(profileData!!)
            signUpViewModel.loginResponse.observe(this, {

                if (it?.payload?.success == true) {

                    SharedPrefsUtils.setStringPreference(this, RemoteConstant.referrerUid, "")
                    if (it.payload != null && it.payload?.profile != null) {
                        if (it.payload?.profile?.AppId != null) {
                            SharedPrefsUtils.setStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                it.payload?.profile?.AppId ?: ""
                            )
                        }
                        if (it.payload?.profile?.ApiKey != null) {
                            SharedPrefsUtils.setStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                it.payload?.profile?.ApiKey ?: ""
                            )
                        }
                        if (it.payload?.profile?._id != null) {
                            SharedPrefsUtils.setStringPreference(
                                this,
                                RemoteConstant.mProfileId,
                                it.payload?.profile?._id ?: ""
                            )
                        }
                        if (it.payload?.profile?.PushSubscription != null) {
                            SharedPrefsUtils.setBooleanPreference(
                                this,
                                RemoteConstant.mEnabledPushNotification,
                                it.payload?.profile?.PushSubscription ?: false
                            )
                        }
                        if (it.payload?.profile?.privateProfile != null) {
                            SharedPrefsUtils.setBooleanPreference(
                                this,
                                RemoteConstant.mEnabledPrivateProfile,
                                it.payload?.profile?.privateProfile ?: false
                            )
                        }
                        if (it.payload?.profile?.username != null) {
                            SharedPrefsUtils.setStringPreference(
                                this,
                                RemoteConstant.mUserName,
                                it.payload?.profile?.username ?: ""
                            )
                        }
                        if (it.payload?.jwt != null) {
                            SharedPrefsUtils.setStringPreference(
                                this,
                                RemoteConstant.mJwt,
                                it.payload?.jwt ?: ""
                            )
                        }
                    }

                    gif.visibility = View.GONE
                    profile_image_view.visibility = View.VISIBLE
                    glideRequestManager
                        .load(
                            SharedPrefsUtils.getStringPreference(
                                this,
                                "PROFILE_PIC_FILE_NAME",
                                ""
                            )
                        )
                        .into(profile_image_view)

                    setting_text_view.text =
                        "Welcome to Socialmob\n" + "" + it.payload!!.profile!!.username
                    // TODO : Layout Animation
                    Sequent.origin(setting_account_linear).anim(this, Animation.FADE_IN_UP)
                        .delay(100).offset(100)
                        .start()
                    SharedPrefsUtils.setStringPreference(
                        this@SettingAccountProcessActivity, RemoteConstant.mUserImage,
                        it.payload?.profile?.pimage ?: ""
                    )
                    SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_URL_NAME", "")
                    SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_NAME", "")
                    startIntroScreen()
                } else {
                    finish()
                    AppUtils.showCommonToast(this, it?.payload?.message)
                }
            })


        }
        setting_text_view.setOnClickListener {
            // TODO : Disabled in V_152 ( IntroSlidingDashBoardActivity )
            // val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }

    private fun startIntroScreen() {
        val handler = Handler()
        handler.postDelayed({
            val intent = Intent(this, SetUpScreenActivity::class.java)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finishAffinity()

        }, 3000)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun changeStatusBarColor() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

}