package com.example.sample.socialmob.view.ui.sign_up

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Patterns
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.example.sample.socialmob.BuildConfig
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.profile.ProfileRegisterData
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.music.HomeViewPagerAdapter
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.ViewPagerListener
import com.example.sample.socialmob.viewmodel.login.SignUpViewModel
import com.fujiyuu75.sequent.Animation
import com.fujiyuu75.sequent.Sequent
import com.transitionseverywhere.ChangeBounds
import com.transitionseverywhere.TransitionManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.sign_up_activity.*
import java.util.regex.Pattern

@AndroidEntryPoint
class SignUpActivity : AppCompatActivity() {


    private val signUpViewModel: SignUpViewModel by viewModels()
    var mEmailAdress = ""
    var mName = ""
    var mProfileImageUrl = ""
    var mGender = ""
    var mBirthDate = ""
    var mPassword = ""
    var mPasswordRenter = ""
    var mZodiacSign = ""
    var mRefferalId = ""

    private var customProgressDialog: CustomProgressDialog? = null

    private lateinit var pagerAdapterView: HomeViewPagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            changeStatusBarColor()
        }
        setContentView(R.layout.sign_up_activity)
        val i = intent
        if (i?.getStringExtra("referral_id") != null) {
            mRefferalId = i.getStringExtra("referral_id") ?: ""
        }
        mRefferalId = SharedPrefsUtils.getStringPreference(this, RemoteConstant.referrerUid, "")!!
        customProgressDialog = CustomProgressDialog(this)

        pagerAdapterView = HomeViewPagerAdapter(supportFragmentManager)
        addPagerFragments()
        viewPager.adapter = pagerAdapterView
        viewPager.offscreenPageLimit = 6

        viewPager.addOnPageChangeListener(ViewPagerListener(this::onPageSelected))
        dots_indicator.setViewPager(viewPager)


        previous_image_view.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem - 1
        }
        next_image_view.setOnClickListener {

            nextPage()
        }

        // TODO : Layout Animation
        Sequent.origin(signup_linear).anim(this, Animation.BOUNCE_IN).delay(100).offset(100)
            .start()


    }

     fun nextPage() {
        if (viewPager.currentItem == 0) {

            if (validEmail(mEmailAdress)) {
                if (InternetUtil.isInternetOn()) {
                    customProgressDialog!!.show()
                    AppUtils.hideKeyboard(this, viewPager)
                    signUpViewModel.checkEmailAvailable(mEmailAdress)
                    signUpViewModel.emailAvailableResponseModelBoolean.nonNull()
                        .observe(this, { emailAvailableResponseModelBoolean ->
                            if (emailAvailableResponseModelBoolean!!) {
                                viewPager.currentItem = 1
                            } else {
                                AppUtils.showCommonToast(
                                    this,
                                    "The email address you have entered is already registered"
                                )
                            }
                            signUpViewModel.emailAvailableResponseModelBoolean.value = null
                            customProgressDialog!!.dismiss()
                        })
                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))

                }
            } else {
                AppUtils.showCommonToast(this, "Enter a valid Email id")
            }
        } else if (viewPager.currentItem == 1) {

            if (mName.isNotEmpty() && mName != "") {
                if (mName.length > 4) {
                    viewPager.currentItem = 2
                } else
                    AppUtils.showCommonToast(this, "Name is too short")
            } else {
                AppUtils.showCommonToast(this, "Please provide your name")
            }
        } else if (viewPager.currentItem == 2) {

//        PROFILE_PIC_FILE_URL_NAME
//        PROFILE_PIC_FILE_NAME

//            mProfileImageUrl =
//                SharedPrefsUtils.getStringPreference(this, "PROFILE_PIC_FILE_URL_NAME", "")!!
//            if (mProfileImageUrl.isNotEmpty() && mProfileImageUrl == "") {
//                viewPager.currentItem = 3
//            } else {
//                AppUtils.showCommonToast(this, "C'mon show us that face")
//            }
            viewPager.currentItem = 3
        } else if (viewPager.currentItem == 3) {
            if (mBirthDate.isNotEmpty() && mBirthDate != "") {
                viewPager.currentItem = 4
            } else {
                AppUtils.showCommonToast(this, "Check the birthday again")
            }
        } else if (viewPager.currentItem == 4) {
            if (mGender.isNotEmpty() && mGender != "") {
                viewPager.currentItem = 5
            } else {
                AppUtils.showCommonToast(this, "Select your gender")
            }
        }
        finish_text_view.setOnClickListener {
            finishClick()
        }
    }
    /**
     * Validation
     */
    private fun finishClick() {

        // TODO : Check Internet connection
        if (InternetUtil.isInternetOn()) {
            if (viewPager.currentItem == 5) {
                AppUtils.hideKeyboard(this, viewPager)
                if (mPassword.isNotEmpty() && mPassword != "") {
                    if (mPasswordRenter.isNotEmpty() && mPasswordRenter != "") {
                        if (mPassword.length >= 5 && mPasswordRenter.length >= 5) {
                            if (isPasswordMatching(mPassword, mPasswordRenter)) {
                                val profileData = ProfileRegisterData(
                                    mEmailAdress,
                                    mPassword,
                                    mGender,
                                    "",
                                    mBirthDate,
                                    mName,
                                    "",
                                    null,
                                    null,
                                    RemoteConstant.mPlatformId,
                                    BuildConfig.VERSION_NAME,
                                    mRefferalId,
                                    mProfileImageUrl,
                                    "",
                                    "",
                                    ""
                                )
                                // TODO : Check Internet connection
                                if (InternetUtil.isInternetOn()) {
                                    val intent =
                                        Intent(this, SettingAccountProcessActivity::class.java)
                                    intent.putExtra("profileData", profileData)
                                    startActivity(intent)
                                } else {
                                    AppUtils.showCommonToast(this, "No internet connection")
                                }

                            } else {
                                AppUtils.showCommonToast(this, "Passwords are not matching")
                            }
                        } else {
                            AppUtils.showCommonToast(
                                this,
                                "Please choose password of atleast 5 character long"
                            )
                        }
                    } else {
                        AppUtils.showCommonToast(this, "Please Re-enter password")
                    }

                } else {
                    AppUtils.showCommonToast(this, "Please enter password")
                }
            }

        } else {
            AppUtils.showCommonToast(this, getString(R.string.no_internet))
        }

    }

    private fun onPageSelected(position: Int) {
        when (position) {
            0 -> {
                val transition = ChangeBounds()
                transition.duration = (700).toLong()
                transition.interpolator =
                    FastOutSlowInInterpolator()
                transition.startDelay = (100).toLong()
                TransitionManager.beginDelayedTransition(transitions_container_sign_up, transition)

                val params = next_image_view.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.CENTER
                next_image_view.layoutParams = params

                val transition1 = ChangeBounds()
                transition1.duration = (700).toLong()
                transition1.interpolator =
                    FastOutSlowInInterpolator()
                transition1.startDelay = (100).toLong()
                TransitionManager.beginDelayedTransition(transitions_container_sign_up, transition1)

                val params1 = previous_image_view.layoutParams as FrameLayout.LayoutParams
                params1.gravity = Gravity.CENTER
                previous_image_view.layoutParams = params1
            }
            1 -> {
                val transition = ChangeBounds()
                transition.duration = (700).toLong()
                transition.interpolator =
                    FastOutSlowInInterpolator()
                transition.startDelay = (500).toLong()
                TransitionManager.beginDelayedTransition(transitions_container_sign_up, transition)

                val params = previous_image_view.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.START
                previous_image_view.layoutParams = params


                val transition1 = ChangeBounds()
                transition1.duration = (700).toLong()
                transition1.interpolator =
                    FastOutSlowInInterpolator()
                transition1.startDelay = (500).toLong()
                TransitionManager.beginDelayedTransition(transitions_container_sign_up, transition)

                val params1 = next_image_view.layoutParams as FrameLayout.LayoutParams
                params1.gravity = Gravity.END
                next_image_view.layoutParams = params1
            }
            4 -> {
                finish_text_view.visibility = View.GONE
                next_image_view.visibility = View.VISIBLE
            }
            5 -> {
                next_image_view.visibility = View.GONE
                finish_text_view.visibility = View.VISIBLE
            }
        }
    }

    private fun addPagerFragments() {
        pagerAdapterView.addFragments(ProfileEmailFragment())
        pagerAdapterView.addFragments(ProfileNameFragment())
        pagerAdapterView.addFragments(ProfileImageFragment())
        pagerAdapterView.addFragments(BirthDateFragment())
        pagerAdapterView.addFragments(GenderFragment())
        pagerAdapterView.addFragments(ProfilePasswordFragment())
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun changeStatusBarColor() {
        // finally change the color
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    private fun validEmail(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }
    /**
     * Saving uploaded image path in prefrence need to clear
     */
    override fun onDestroy() {
        super.onDestroy()
        SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_URL_NAME", "")
        SharedPrefsUtils.setStringPreference(this, "PROFILE_PIC_FILE_NAME", "")
        if (customProgressDialog != null && customProgressDialog!!.isShowing)
            customProgressDialog!!.dismiss()
    }
    /**
     * Check both password are same
     */
    private fun isPasswordMatching(password: String, confirmPassword: String): Boolean {
        return try {
            val pattern = Pattern.compile(password, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(confirmPassword)

            matcher.matches()
        } catch (e: Exception) {
            false
        }
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        val intent = Intent(this, MainLandingActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fadein, R.anim.fadeout)
        this.finish()
        overridePendingTransition(0, 0)
    }

}
