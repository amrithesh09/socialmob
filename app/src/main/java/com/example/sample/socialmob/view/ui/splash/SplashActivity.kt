package com.example.sample.socialmob.view.ui.splash

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.main_landing.MainLandingActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.AudioServiceContext
import com.example.sample.socialmob.view.utils.DeviceInfo.device.DeviceDataInfo
import com.example.sample.socialmob.view.utils.DeviceInfo.device.DeviceDifferentData
import com.example.sample.socialmob.view.utils.DeviceInfo.device.model.Device
import com.example.sample.socialmob.view.utils.DeviceInfo.device.model.Memory
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import kotlinx.android.synthetic.main.layout_item_video_exo.view.*
import kotlinx.android.synthetic.main.splash_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class SplashActivity : AppCompatActivity() {
    private var isPreparing: Boolean = false

    /**
     * Need device details for crashlytics
     */
    private var device: Device? = null
    private var memory: Memory? = null

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(AudioServiceContext.getContext(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        /**
         * To check Dark mode enabled
         */
        val mMode =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)
        if (mMode) {
            setTheme(R.style.AppTheme_NoActionBarTranslucentNightHome)
        } else {
            setTheme(R.style.AppTheme_NoActionBarTranslucentHomeDayLight)
        }
        super.onCreate(savedInstanceState)
        // TODO : Change Hide status bar
        setContentView(R.layout.splash_activity)
        /**
         * Orientation and status bar translucent issue fix for one plus devices
         */
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val currentOrientation = resources.configuration.orientation
            requestedOrientation = if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        }
        // TODO : Enable Dark mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        //TODO : Do not change since chat notification posting
        SharedPrefsUtils.setStringPreference(this, RemoteConstant.mConnectedUser, "")
        //TODO : ( Do not change need to set the value after app launch )
        SharedPrefsUtils.setBooleanPreference(this, RemoteConstant.isTapTargetPlayedHome, true)
        SharedPrefsUtils.setStringPreference(this, AppUtils.IS_PLAYING_RADIO, "false")


        // TODO : Initially set dark mode on
        if (SharedPrefsUtils.getStringPreference(
                this, RemoteConstant.isFirstTime, "false"
            ) == "false"
        ) {
            SharedPrefsUtils.setStringPreference(
                this, RemoteConstant.isFirstTime, "true"
            )
            SharedPrefsUtils.setBooleanPreference(this, RemoteConstant.NIGHT_MODE, true)
        }


        // TODO : Post Device Data
        if (SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "") != "") {
            if (InternetUtil.isInternetOn()) {
                postDeviceData()
            }
        } else {

//            postDeviceDataV1()
        }


        // TODO : Fire base Instance to check user is invited or not , We need to pass referral id while registering
        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(this) { pendingDynamicLinkData ->
                // Get deep link from result (may be null if no link is found)
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData.link
                }
                if (deepLink != null && deepLink.getBooleanQueryParameter("invitedby", false)) {
                    val referrerUid: String = deepLink.getQueryParameter("invitedby")!!
                    SharedPrefsUtils.setStringPreference(
                        this,
                        RemoteConstant.referrerUid,
                        referrerUid
                    )
                }

            }
            .addOnFailureListener(this) {
                println("Error")
            }

        // TODO : To test firebase crashlytics
//        throw  RuntimeException("This is a crash")
//        val mBtn: Button? = Button(this)
//        mBtn?.text = "CRASH"
//        throw  RuntimeException("Crash!")
    }

    /**
     * Posting device details API
     */
    private fun postDeviceDataV1() {
        try {
            device = Device(this@SplashActivity)
            memory = Memory(this@SplashActivity)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val deviceDifferentData = DeviceDifferentData()
        deviceDifferentData.deviceData = device
        deviceDifferentData.memoryData = memory

        val deviceDataInfo = DeviceDataInfo()
        deviceDataInfo.info = deviceDifferentData

        GlobalScope.launch {
            WebServiceClient.client.create(BackEndApi::class.java)
                .postDeviceInfoV1(
                    "sm_1", RemoteConstant.mPlatform, deviceDataInfo
                )
        }

    }

    /**
     * Posting device details API
     */
    private fun postDeviceData() {
        try {
            device = Device(this@SplashActivity)
            memory = Memory(this@SplashActivity)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val deviceDifferentData = DeviceDifferentData()
        deviceDifferentData.deviceData = device
        deviceDifferentData.memoryData = memory

        val deviceDataInfo = DeviceDataInfo()
        deviceDataInfo.info = deviceDifferentData


        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.deviceInfoData, RemoteConstant.postMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                this,
                RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        GlobalScope.launch(Dispatchers.IO) {
            WebServiceClient.client.create(BackEndApi::class.java)
                .postDeviceInfo(
                    mAuth, RemoteConstant.mPlatform, deviceDataInfo
                )
        }
    }

    private fun startActivityIntent() {
        // TODO : Check whether appId is available
        if (SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "") != "") {
            if (SharedPrefsUtils.getBooleanPreference(this, "NEED_TO_SHOW_DASH_BOARD", false)) {
                SharedPrefsUtils.setBooleanPreference(this, "NEED_TO_SHOW_DASH_BOARD", false)
                // TODO : IntroSlidingDashBoardActivity Disabled in V_152
                //val intent = Intent(this, IntroSlidingDashBoardActivity::class.java)
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                this.finish()
            } else {
                val intent = Intent(this, HomeActivity::class.java)
//                val intent = Intent(this, SetUpScreenActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
            }
        } else {
            val intent = Intent(this, MainLandingActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }

    /**
     * We need to play long video on first open , short video in other timw
     */
    override fun onResume() {
        super.onResume()
        try {
            if (!isPreparing) {
                isPreparing = true
                if (!SharedPrefsUtils.getBooleanPreference(this, "IS_SHOW_LONG_VIDEO", false)) {
                    SharedPrefsUtils.setBooleanPreference(this, "IS_SHOW_LONG_VIDEO", true)
                    video_view.setRawData(R.raw.sp_long)
                } else {
                    video_view.setRawData(R.raw.sp_short_new)
                }
                video_view.isLooping = false
                video_view.prepareAsync {
                    isPreparing = false
                    video_view.start()
                }
                video_view?.setOnCompletionListener {
                    startActivityIntent()
                }
            }
        } catch (ioe: Exception) {
            //ignore
            startActivityIntent()
        }
    }

    /**
     * Pause video when destroy
     */
    override fun onPause() {
        video_view.pause()
        super.onPause()
    }

    /**
     * Stop video when destroy
     */
    override fun onDestroy() {
        video_view.stop()
        super.onDestroy()
    }
}
