package com.example.sample.socialmob.view.ui.write_new_post;

public class Config {


    public class KeyName
    {
        public static final String FILEPATH="filepath";
        public static final String TITLE="title";
        public static final String IMAGE_LIST="image_list";
    }
    public class MessageType{
        public static final String TEXT = "text";
        public static final String IMAGE = "image";
        public static final String GIF = "gif";
        public static final String VIDEO = "video";
        public static final String AUDIO = "audio";
        public static final String RECORDING = "recording";
        public static final String DOCUMENTS = "doc";
        public static final String DETAIL_INFO="info";

    }
}
