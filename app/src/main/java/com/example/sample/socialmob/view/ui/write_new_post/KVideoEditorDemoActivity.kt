package com.example.sample.socialmob.view.ui.write_new_post

import android.app.Activity
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.*
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.VesdkEventTracker
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import ly.img.android.pesdk.VideoEditorSettingsList
import ly.img.android.pesdk.assets.filter.basic.FilterPackBasic
import ly.img.android.pesdk.assets.font.basic.FontPackBasic
import ly.img.android.pesdk.backend.decoder.ImageSource
import ly.img.android.pesdk.backend.model.EditorSDKResult
import ly.img.android.pesdk.backend.model.config.CropAspectAsset
import ly.img.android.pesdk.backend.model.constant.OutputMode
import ly.img.android.pesdk.backend.model.state.AssetConfig
import ly.img.android.pesdk.backend.model.state.LoadSettings
import ly.img.android.pesdk.backend.model.state.TransformSettings
import ly.img.android.pesdk.backend.model.state.VideoEditorSaveSettings
import ly.img.android.pesdk.backend.model.state.manager.SettingsList
import ly.img.android.pesdk.ui.activity.ImgLyIntent
import ly.img.android.pesdk.ui.activity.VideoEditorBuilder
import ly.img.android.pesdk.ui.model.state.UiConfigAspect
import ly.img.android.pesdk.ui.model.state.UiConfigFilter
import ly.img.android.pesdk.ui.model.state.UiConfigMainMenu
import ly.img.android.pesdk.ui.model.state.UiConfigText
import ly.img.android.pesdk.ui.panels.item.CropAspectItem
import ly.img.android.pesdk.ui.panels.item.ToolItem
import ly.img.android.pesdk.ui.utils.PermissionRequest
import ly.img.android.serializer._3.IMGLYFileWriter
import ly.img.android.serializer._3._0._0.PESDKFileWriter
import java.io.File
import java.io.IOException
import java.util.*

class KVideoEditorDemoActivity : Activity(), PermissionRequest.Response {
    private var VESDK_RESULT_BROADCAST_RECEIVER = "com.SocialMob.BroadcastReceiverAction"


    private var mImageList: ArrayList<GalleryData> = ArrayList()
    private var mFile: File? = null

    private var path: String? = null

    companion object {
        const val VESDK_RESULT = 1
        const val GALLERY_RESULT = 2
    }

    // Important permission request for Android 6.0 and above, don't forget to add this!
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        PermissionRequest.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun permissionGranted() {}


    override fun permissionDenied() {
        /* TODO: The Permission was rejected by the user. The Editor was not opened,
         * Show a hint to the user and try again. */
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_editor)

        mImageList = intent.getParcelableArrayListExtra(Config.KeyName.IMAGE_LIST)!!

        if (mImageList.size > 0) {
            path = mImageList[0].photoUri

            val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "socialmob")
            if (!file.exists()) {
                file.mkdir()
            }
            mFile = File(file.toString() + "/" + System.currentTimeMillis() + "socialmob.mp4")

            try {
                if (getImageContentUri(this, File(path)) != null) {
                    openEditor(getImageContentUri(this, File(path)))
                } else {
                    AppUtils.showCommonToast(this, "getting null")
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getImageContentUri(
        context: Context,
        imageFile: File
    ): Uri? {
        val filePath = imageFile.absolutePath
        val cursor = context.contentResolver.query(
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
            arrayOf(MediaStore.Video.Media._ID),
            MediaStore.Video.Media.DATA + "=? ",
            arrayOf(filePath),
            null
        )
        return if (cursor != null && cursor.moveToFirst()) {
            val id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID))
            cursor.close()
            Uri.withAppendedPath(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                "" + id
            )
        } else {
            if (imageFile.exists()) {
                val values = ContentValues()
                values.put(MediaStore.Video.Media.DATA, filePath)
                context.contentResolver.insert(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values
                )
            } else {
                null
            }
        }
    }

    private fun openEditor(inputImage: Uri?) {

        val mFilePath = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "socialmob")
        if (!mFilePath.exists()) {
            mFilePath.mkdir()
        }
        val settingsList = VideoEditorSettingsList()
            .configure<UiConfigFilter> {
                it.setFilterList(FilterPackBasic.getFilterPack())
            }
            .configure<UiConfigText> {
                it.setFontList(FontPackBasic.getFontPack())
            }
            .configure<VideoEditorSaveSettings> {
                // Set custom editor video export settings
//                it.setOutputToGallery(mFileOutput.absolutePath)
                it.setOutputToGallery(Environment.DIRECTORY_DCIM+"/socialmob")
                it.outputMode = OutputMode.EXPORT_ALWAYS
            }
        settingsList.setEventTracker(
            VesdkEventTracker(
                "100"
            )
        )

        settingsList.getSettingsModel(AssetConfig::class.java).apply {
            getAssetMap(CropAspectAsset::class.java)
                .clear().add(
                    CropAspectAsset("aspect_1_1", 1, 1, false),
                    CropAspectAsset("aspect_16_9", 16, 9, false),
                    CropAspectAsset("aspect_4_3", 4, 3, false)
                )
        }

        // Add your own Asset to UI config and select the Force crop Mode.
        settingsList.getSettingsModel(UiConfigAspect::class.java).apply {
            setAspectList(
                CropAspectItem("aspect_1_1"),
                CropAspectItem("aspect_16_9"),
                CropAspectItem("aspect_4_3")
            )
            forceCropMode = UiConfigAspect.ForceCrop.SHOW_TOOL_NEVER
        }

        settingsList.getSettingsModel(UiConfigMainMenu::class.java).toolList.clear()
        settingsList.getSettingsModel(UiConfigMainMenu::class.java).apply {
            // Set the tools you want keep sure you licence is cover the feature and do not forget to include the correct modules in your build.gradle
            setToolList(
//                ToolItem(
//                    "imgly_tool_trim",
//                    R.string.vesdk_video_trim_title_name,
//                    ImageSource.create(R.drawable.imgly_icon_tool_trim)
//                ),
                ToolItem(
                    "imgly_tool_transform",
                    R.string.pesdk_transform_title_name,
                    ImageSource.create(R.drawable.imgly_icon_tool_transform)
                )
            )
        }

        settingsList.configure<LoadSettings> {
            it.source = inputImage
        }
        settingsList[LoadSettings::class].source = inputImage

        try {
            VideoEditorBuilder(this)
                .setSettingsList(settingsList)
                .startActivityForResult(this, VESDK_RESULT)
//                .startActivityForBroadcast(
//                    this@KVideoEditorDemoActivity,
//                    VESDK_RESULT_BROADCAST_RECEIVER
//                )
//            finish()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == GALLERY_RESULT) {
            // Open Editor with some uri in this case with an video selected from the system gallery.
            openEditor(data?.data)

        } else if (resultCode == RESULT_OK && requestCode == VESDK_RESULT) {
            // Editor has saved an Video.
//            val resultURI = data?.getParcelableExtra<Uri?>(ImgLyIntent.RESULT_IMAGE_URI)?.also {
//                // Scan result uri to show it up in the Gallery
//                sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).setData(it))
//            }
//
//            val sourceURI = data?.getParcelableExtra<Uri?>(ImgLyIntent.SOURCE_IMAGE_URI)?.also {
//                // Scan source uri to show it up in the Gallery
//                sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).setData(it))
//            }
//
//            Log.i("VESDK", "Source video is located here $sourceURI")
//            Log.i("VESDK", "Result video is located here $resultURI")
//
//            // TODO: Do something with the result image
//
//            // OPTIONAL: read the latest state to save it as a serialisation
//            val lastState = data?.getParcelableExtra<SettingsList>(ImgLyIntent.SETTINGS_LIST)
////            var mRatio=lastState?.
//            try {
//                PESDKFileWriter(lastState!!).writeJson(
//                    File(
//                        Environment.getExternalStorageDirectory(),
//                        "serialisationReadyToReadWithPESDKFileReader.json"
//                    )
//                )
//            } catch (e: IOException) {
//                e.printStackTrace()
//                Toast.makeText(this, "" + e.printStackTrace(), Toast.LENGTH_SHORT).show()
//            }

//            val file = File(resultURI.toString())
            val dataUri =  EditorSDKResult(data!!);
            val dataSdk = EditorSDKResult(dataUri.intent);
            Log.i("VESDK", "Source video is located here ${dataSdk.sourceUri}")
            Log.i("VESDK", "Result video is located here ${dataSdk.resultUri}")

            // TODO: Do something with the result video

            // OPTIONAL: read the latest state to save it as a serialisation
            val lastState = dataSdk.settingsList
            try {
                IMGLYFileWriter(lastState).writeJson(
                    File(
                        Environment.getExternalStorageDirectory(),
                        "serialisationReadyToReadWithPESDKFileReader.json"
                    )
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }
            mImageList.clear()
            val mGalleryData = GalleryData()
            val mPath = getFilePath(this, Uri.parse(dataSdk.resultUri.toString()))
            mGalleryData.photoUri = mPath!!
            mImageList.add(mGalleryData)



            val settingsList = dataSdk.settingsList
            val ts = settingsList.getSettingsModel(
                TransformSettings::class.java
            )
            val mRatio = ts.getAspectRation() // Returns the aspect ratio

            var mVideoRatio = "1:1"
            if (mRatio.toString() == "1.333333") {
                mVideoRatio = "4:3"
            } else if (mRatio.toString() == "1.777778") {
                mVideoRatio = "16:9"
            } else if (mRatio.toString() == "1") {
                mVideoRatio = "1:1"
            }

            val mHandler = Handler(Looper.getMainLooper())
            mHandler.postDelayed({
                val intent = Intent(this, UploadPhotoActivityMentionHashTag::class.java)
                intent.putExtra("title", getString(R.string.upload_video))
                intent.putExtra("file", dataSdk.resultUri.toString())
                intent.putExtra("RATIO", mVideoRatio)
                intent.putExtra("type", "video")
                intent.putExtra("mImageList", mImageList)
                startActivity(intent)
                finish()
            }, 100)


        } else if (resultCode == RESULT_CANCELED && requestCode == VESDK_RESULT) {
            // Editor was canceled
            val sourceURI = data?.getParcelableExtra<Uri?>(ImgLyIntent.SOURCE_IMAGE_URI)
            // TODO: Do something with the source...

            finish()
        }
    }

    //    @Throws(URISyntaxException::class)
    private fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                context.applicationContext,
                uri
            )
        ) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {


            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor? = null
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }


}
