package com.example.sample.socialmob.view.ui.write_new_post

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.MainHashTagListItemBinding
import com.example.sample.socialmob.viewmodel.feed.MainTagsResponseModelPayloadMainTag

class MainHashTagAdapter(
    private var mItems: MutableList<MainTagsResponseModelPayloadMainTag>?,
    private var mainHashTagAdapterItemClick: MainHashTagAdapterItemClick,
    private var isMainTag: Boolean
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<MainHashTagAdapter.ViewHolder>() {
    private var rowIndex = -1

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.binding.hashTagViewModel = mItems?.get(viewHolder.adapterPosition)
        viewHolder.binding.executePendingBindings()

        viewHolder.itemView.setOnClickListener {

            mainHashTagAdapterItemClick.onMainHashTagAdapterItemClick(
                mItems?.get(viewHolder.adapterPosition)?.id!!,
                mItems?.get(viewHolder.adapterPosition)?.name!!
            )

            if (!isMainTag) {
                Handler().postDelayed({
                    notify(viewHolder.adapterPosition)
                }, 100)
            } else {
                rowIndex = viewHolder.adapterPosition
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return mItems?.size!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<MainHashTagListItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.main_hash_tag_list_item, parent, false
        )
        return ViewHolder(binding)
    }

    class ViewHolder(val binding: MainHashTagListItemBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
//
    }

    interface MainHashTagAdapterItemClick {
        fun onMainHashTagAdapterItemClick(mId: String, mName: String)
    }

    private fun notify(adapterPosition: Int) {
        if (mItems?.size!! > 0) {
            mItems?.removeAt(adapterPosition)
            notifyItemRemoved(adapterPosition)
            notifyItemChanged(adapterPosition, mItems?.size)
        }
    }
}
