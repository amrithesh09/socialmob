package com.example.sample.socialmob.view.ui.write_new_post

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Rect
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.view.View
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.ActivityUploadPhotoMentionHashTagBinding
import com.example.sample.socialmob.di.util.CacheMapperMention
import com.example.sample.socialmob.model.feed.SingleFeedResponseModelPayload
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.profile.PostFeedData
import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.feed.MainFeedFragment
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.socialview.Hashtag
import com.example.sample.socialmob.view.utils.socialview.HashtagArrayAdapter
import com.example.sample.socialmob.view.utils.socialview.Mention
import com.example.sample.socialmob.view.utils.socialview.MentionArrayAdapter
import com.example.sample.socialmob.viewmodel.profile.ProfileUploadPostViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject


@AndroidEntryPoint
class UploadPhotoActivityMentionHashTag : BaseCommonActivity(),
    MainHashTagAdapter.MainHashTagAdapterItemClick,
    VideoImageListAdapter.AdapterItemClick {
    @Inject
    lateinit var cacheMapperMention: CacheMapperMention

    private var defaultHashTagAdapter: ArrayAdapter<Hashtag>? = null
    private var defaultMentionAdapter: ArrayAdapter<Mention>? = null
    private var isHashTag: Boolean = false
    private var isMention: Boolean = false
    private val profileUclaPostViewModel: ProfileUploadPostViewModel by viewModels()
    private var fileThumbnail = ""
    private var type = ""
    private var title = ""
    private var IS_FROM_CREATE_PROFILE = false
    private var mImageList: ArrayList<GalleryData> = ArrayList()
    private val mImageListNew: ArrayList<GalleryData> = ArrayList()
    private var customProgressDialog: CustomProgressDialog? = null
    private var mThumbList: ArrayList<File>? = ArrayList()
    private var isApiCalled: Boolean = false
    private val tagFeed: MutableList<HashTag> = ArrayList()
    private val tagFeedString: MutableList<String> = ArrayList()
    private var binding: ActivityUploadPhotoMentionHashTagBinding? = null

    private var mPostId: String = ""
    private var mIsEdit: Boolean = false

    private var mEditDescription: String = ""
    private var mEditPayload: SingleFeedResponseModelPayload? = null

    private var mRatio: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_upload_photo_mention_hash_tag)
        customProgressDialog = CustomProgressDialog(this)

        val i = intent
        if (i?.getStringExtra("mPostId") != null) {
            mPostId = i.getStringExtra("mPostId")!!
            mIsEdit = i.getBooleanExtra("mIsEdit", false)

            if (InternetUtil.isInternetOn()) {
                callApi(mPostId)
                binding?.progressBarLoading?.visibility = View.VISIBLE
                binding?.linearLayout?.visibility = View.GONE
                title = getString(R.string.update)
            }

        }


        if (!mIsEdit) {
            if (intent?.getStringExtra("title") != null) {
                title = intent?.getStringExtra("title")!!
            }
            if (intent?.getStringExtra("RATIO") != null) {
                mRatio = intent?.getStringExtra("RATIO")!!
            }
            if (intent.hasExtra("file")) {
                fileThumbnail = intent?.getStringExtra("file")!!
            }
            type = intent?.getStringExtra("type")!!
            try {
                mImageList = intent?.getParcelableArrayListExtra("mImageList")!!
            } catch (e: Exception) {
            }

            IS_FROM_CREATE_PROFILE = intent?.getBooleanExtra("IS_FROM_CREATE_PROFILE", false)!!
        }

        binding?.uploadconfirmTitleTextView?.text = title
        binding?.uploadconfirmTextView?.text = title

        if (fileThumbnail != "") {
            val mGalleryData = GalleryData()
            mGalleryData.photoUri = fileThumbnail
            mImageListNew.add(mGalleryData)
            binding?.videoImageListRecyclerView?.visibility = View.VISIBLE
            binding?.videoImageListRecyclerView?.adapter = VideoImageListAdapter(
                this@UploadPhotoActivityMentionHashTag, mImageListNew,
                this, false
            )
        } else {
            binding?.videoImageListRecyclerView?.visibility = View.VISIBLE
            binding?.videoImageListRecyclerView?.adapter = VideoImageListAdapter(
                this@UploadPhotoActivityMentionHashTag, mImageList,
                this, false
            )
        }

        // TODO : TO check Keyboard is visible or not
        binding?.mainRelative?.viewTreeObserver?.addOnGlobalLayoutListener {
            val r = Rect()
            binding?.mainRelative?.getWindowVisibleDisplayFrame(r)
            val screenHeight = binding?.mainRelative?.rootView?.height

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            val keypadHeight = screenHeight!! - r.bottom


            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                binding?.uploadconfirmTextView?.visibility = View.GONE
            } else {
                // keyboard is closed
                binding?.uploadconfirmTextView?.visibility = View.VISIBLE
            }
        }

        binding?.writePostEditText?.isHashtagEnabled = true
        binding?.writePostEditText?.isMentionEnabled = true

        defaultMentionAdapter = MentionArrayAdapter(this)
        binding?.writePostEditText?.mentionAdapter = defaultMentionAdapter

        defaultHashTagAdapter = HashtagArrayAdapter(this)
        binding?.writePostEditText?.hashtagAdapter = defaultHashTagAdapter


        binding?.writePostEditText?.setHashtagTextChangedListener { view, text ->
            isHashTag = true
            isMention = false
            profileUclaPostViewModel.getHashTags(this, text.toString())
        }
        observeHasTag()

        binding?.writePostEditText?.setMentionTextChangedListener { view, text ->
            isHashTag = false
            isMention = true
            if (text.isNotEmpty()) {
                defaultMentionAdapter?.clear()
                profileUclaPostViewModel.getMentions(this, text.toString())
            }
        }
        observeSearchMention()

        // TODO : For campaign hash tag
        if (SharedPrefsUtils.getStringPreference(this, "POST_FROM", "") == "CAMPAIGN_FEED") {
            if (SharedPrefsUtils.getStringPreference(this, "CAMPAIGN_NAME", "") != "") {
                binding?.writePostEditText?.setText(
                    SharedPrefsUtils.getStringPreference(
                        this, "CAMPAIGN_NAME", ""
                    )
                )
            }
        }
        binding?.uploadBackImageView?.setOnClickListener {
            onBackPressed()
        }
    }
    /**
     * For Edit get post details API
     */
    private fun callApi(mPostId: String) {

        profileUclaPostViewModel.getFeedDetails(mPostId)
        profileUclaPostViewModel.responseModel.nonNull().observe(this, {responseModel->
            if (responseModel?.payload != null) {
                mEditPayload = responseModel.payload!!
                if (responseModel.payload != null && responseModel.payload?.post != null && responseModel.payload?.post?.action != null &&
                    responseModel.payload?.post?.action?.post != null && responseModel.payload?.post?.action?.post?.description != null
                ) {
                    binding?.writePostEditText?.setText(responseModel.payload?.post?.action?.post?.description)
                    mEditDescription = responseModel.payload?.post?.action?.post?.description!!
                }
                binding?.progressBarLoading?.visibility = View.GONE
                binding?.linearLayout?.visibility = View.VISIBLE


                if (responseModel.payload != null && responseModel.payload?.post != null && responseModel.payload?.post?.action != null && responseModel.payload?.post?.action?.post != null &&
                    responseModel.payload?.post?.action?.post?.media != null && responseModel.payload?.post?.action?.post?.media?.isNotEmpty()!!
                ) {
                    //                private var mImageList: ArrayList<GalleryData> = ArrayList()
                        // Add gallery data to list when update
                    for (i in 0 until responseModel.payload?.post?.action?.post?.media?.size!!) {
                        val item = GalleryData(
                            0, "",
                            RemoteConstant.getImageUrlFromWidth(
                                responseModel.payload?.post?.action?.post?.media?.get(i)?.sourcePath!!,
                                100
                            ), 0, true, true, 0, 0,
                            "", ""
                        )
                        mImageList.add(item)
                    }

                    binding?.videoImageListRecyclerView?.visibility = View.VISIBLE
                    binding?.videoImageListRecyclerView?.adapter = VideoImageListAdapter(
                        this@UploadPhotoActivityMentionHashTag, mImageList,
                        this, false
                    )
                }
            }
        })
    }
    /**
     * Upload to server
     * Edit case handled
     */

    fun uploadImageToServer(view: View) {

        // TODO : Check Internet connection
        if (!InternetUtil.isInternetOn()) {
            AppUtils.showCommonToast(
                this@UploadPhotoActivityMentionHashTag,
                getString(R.string.no_internet)
            )
        } else {

            customProgressDialog?.show()
            if (title == "UPLOAD VIDEO") {

                for (i in 0 until mImageListNew.size) {
                    runOnUiThread {
                        val videoThumbImageFile: File

                        val bitmap =
                            ThumbnailUtils.createVideoThumbnail(
                                getFilePath(this, Uri.parse(mImageListNew[i].photoUri))!!,
                                MediaStore.Video.Thumbnails.MINI_KIND
                            )

                        val file =
                            File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
                        if (!file.exists()) {
                            file.mkdir()
                        }
                        videoThumbImageFile =
                            File(file, "socialmob" + System.currentTimeMillis() / 1000 + ".jpg")
                        try {
                            val os = FileOutputStream(videoThumbImageFile)
                            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, os)
                            os.flush()
                            os.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        mThumbList?.add(videoThumbImageFile)
                    }
                }
            }
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mPresignedurl +
                        RemoteConstant.mSlash + type + RemoteConstant.mSlash + mImageList.size.toString(),
                RemoteConstant.mGetMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    this,
                    RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


            // <------------------------------------------------------------------------------------>
            // <------------------------------------------------------------------------------------>
            val postData = PostFeedData()

            postData.description = binding?.writePostEditText?.text.toString()
            postData.type = "text"
            postData.media = null

            // <------------------------------------------------------------------------------------>
            // <------------------------------------------------------------------------------------>


            if (mIsEdit) {
                // TODO : Set type for edit item
                if (mEditPayload != null && mEditPayload?.post != null && mEditPayload?.post?.action != null &&
                    mEditPayload?.post?.action?.source != null
                ) {
                    if (mEditPayload?.post?.action?.source == "user_video_post") {
                        postData.type = "video"
                    } else {
                        postData.type = "image"
                    }
                }

                // TODO : Update Post
                profileUclaPostViewModel.updatePost(postData, mPostId)
                profileUclaPostViewModel.textFeedResponseMode.nonNull().observe(this, {
                    customProgressDialog?.dismiss()
                    if (it?.payload?.success == true) {
                        if (SharedPrefsUtils.getStringPreference(
                                this,
                                "POST_FROM",
                                ""
                            ) == "MAIN_FEED"
                        ) {
                            MainFeedFragment.isUpdatedFeed = true
                            it.payload.post?.own = true
                            MainFeedFragment.mAddFeeData = it.payload.post
                        } else {
                            ProfileFeedFragment.isUpdatedFeed = true
                            ProfileFeedFragment.mAddFeeData = it.payload.post
                        }
                        onBackPressed()
                    }
                })
                profileUclaPostViewModel.textFeedResponseMThrowable.nonNull()
                    .observe(this, {
                        customProgressDialog?.dismiss()
                        AppUtils.showCommonToast(this, "Error occur please try again later")
                        finish()
                    })

            } else {

                when {
                    SharedPrefsUtils.getStringPreference(
                        this,
                        "POST_FROM",
                        ""
                    ) == "CAMPAIGN_FEED" -> {

                        ProfileMediaGridFragment.EXPORT_STARTED = true
                        ProfileMediaGridFragment.titlePost = title
                        ProfileMediaGridFragment.mAuthPostType = type
                        ProfileMediaGridFragment.postDataPost = postData
                        if (mIsEdit || title != "UPLOAD VIDEO") {
                            ProfileMediaGridFragment.isAddedFeed = true
                            ProfileMediaGridFragment.mThumbListPost = mThumbList
                            ProfileMediaGridFragment.mAuthPost = mAuth
                            ProfileMediaGridFragment.mImageListPost = mImageList.size.toString()
                            ProfileMediaGridFragment.mImageListPostList = mImageList
                            ProfileMediaGridFragment.ratio = mRatio
                            ProfileMediaGridFragment.isCompletedVideoEdit = true
                        }
                        customProgressDialog?.dismiss()
                        finish()


                    }
                    SharedPrefsUtils.getStringPreference(this, "POST_FROM", "") == "MAIN_FEED" -> {

                        MainFeedFragment.EXPORT_STARTED = true
                        MainFeedFragment.titlePost = title
                        MainFeedFragment.mAuthPostType = type
                        MainFeedFragment.postDataPost = postData
//                        if (mIsEdit || title != "UPLOAD VIDEO") {
                            MainFeedFragment.isAddedFeed = true
                            MainFeedFragment.mThumbListPost = mThumbList
                            MainFeedFragment.mAuthPost = mAuth
                            MainFeedFragment.mImageListPost = mImageList.size.toString()
                            MainFeedFragment.mImageListPostList = mImageList
                            MainFeedFragment.ratio = mRatio
                            MainFeedFragment.isCompletedVideoEdit = true
//                        }
                        customProgressDialog?.dismiss()
                        finish()


                    }
                    else -> {
                        ProfileFeedFragment.EXPORT_STARTED = true
                        ProfileFeedFragment.titlePost = title
                        ProfileFeedFragment.mAuthPostType = type
                        ProfileFeedFragment.postDataPost = postData
//                        if (mIsEdit || title != "UPLOAD VIDEO") {
                            ProfileFeedFragment.isAddedFeed = true
                            ProfileFeedFragment.mAuthPost = mAuth
                            ProfileFeedFragment.mImageListPost = mImageList.size.toString()
                            ProfileFeedFragment.mImageListPostList = mImageList
                            ProfileFeedFragment.mThumbListPost = mThumbList
                            ProfileFeedFragment.ratio = mRatio
                            ProfileFeedFragment.isCompletedVideoEdit = true
//                        }
                        customProgressDialog?.dismiss()
                        finish()

                    }
                }
            }

        }

    }

    override fun onMainHashTagAdapterItemClick(mId: String, mName: String) {
//
//        hashTags.
        val item = HashTag(0, mName, mId)
        tagFeed.add(item)
        tagFeedString.add("#$mName")

        if (!isApiCalled) {
            isApiCalled = true
            binding?.selectedMainHashTagTextView?.visibility = View.VISIBLE
            binding?.selectedMainHashTagView?.visibility = View.VISIBLE
            binding?.progressBar?.visibility = View.VISIBLE


            ///api/v1/subtags/:mainTagId/list/:offset/:count
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(this, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.pathSubHashTag + mId +
                        RemoteConstant.mSlash + "list" + RemoteConstant.mSlash + RemoteConstant.mOffset +
                        RemoteConstant.mSlash + RemoteConstant.mCount,
                RemoteConstant.mGetMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    this, RemoteConstant.mAppId, ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            profileUclaPostViewModel.getSubHAshTag(mAuth, mId)

            profileUclaPostViewModel.subTagResponseModel.nonNull().observe(this, {
                if (it?.payload?.hashTags != null && it.payload.hashTags.isNotEmpty()) {
                    binding?.mainHashTahRecyclerView?.adapter = MainHashTagAdapter(
                        it.payload.hashTags as MutableList,
                        this, false

                    )
                    binding?.progressBar?.visibility = View.GONE

                }
            })
        }
        var mString = ""
        for (i in 0 until binding?.selectedMainHashTagTextView?.hashtags?.size!!) {
            val one = binding?.selectedMainHashTagTextView?.hashtags!![i]
            mString = "$mString #$one"
        }
        mString = "$mString #$mName"
        binding?.selectedMainHashTagTextView?.setText(mString)
    }

    override fun onBackPressed() {
        delete()
        super.onBackPressed()
        AppUtils.hideKeyboard(this@UploadPhotoActivityMentionHashTag, binding?.uploadBackImageView)
    }
    /**
     * Delete when upload is completed
     */
    private fun delete() {
        val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
        if (dir.isDirectory) {
            val children = dir.list()
            if (children != null && children.isNotEmpty()) {
                for (i in children.indices) {
                    File(dir, children[i]).delete()
                }
            }
        }
    }
    /**
     * Observe mention and populate to adapter
     */
    private fun observeSearchMention() {

        profileUclaPostViewModel.mentionProfileResponseModel.nonNull().observe(this, { profiles ->
            if (profiles != null) {
                val mMentionList: MutableList<Mention>
                if (profiles.isNotEmpty()) {

                    val mMentionEditTextList: MutableList<String> =
                        binding?.writePostEditText?.mentions ?: ArrayList()

                    val mPopulateList: MutableList<Profile> =
                        profiles.filter {
                            it.username !in mMentionEditTextList.map { item ->
                                item.replace("@", "")
                            }
                        } as MutableList<Profile>
                    mMentionList =
                        cacheMapperMention.mapFromEntityListMention(mPopulateList).toMutableList()
                    defaultMentionAdapter?.addAll(mMentionList)
                }
                if (defaultMentionAdapter == null) {
                    defaultMentionAdapter = MentionArrayAdapter(this)
                    binding?.writePostEditText?.mentionAdapter = defaultMentionAdapter
                } else {
                    binding?.writePostEditText?.mentionAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

    /**
     * Observe hashtag and populate to adapter
     */
    private fun observeHasTag() {
        profileUclaPostViewModel.hashTagsResponseModel.nonNull().observe(this, { hashTagList ->
            binding?.writePostEditText?.hashtagAdapter?.clear()
            if (hashTagList?.isNotEmpty()!!) {
                for (i in hashTagList.indices) {
                    defaultHashTagAdapter?.add(
                        Hashtag(hashTagList[i].name!!)
                    )
                }
            }
            Handler().postDelayed(
                { binding?.writePostEditText?.hashtagAdapter?.notifyDataSetChanged() },
                100
            )
        })

    }

    /**
     * Get actual file path
     */
    //    @Throws(URISyntaxException::class)
    private fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (DocumentsContract.isDocumentUri(context.applicationContext, uri)
        ) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {


            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor?
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    override fun onAdapterItemClick(mPostion: String) {

    }

}
