package com.example.sample.socialmob.view.ui.write_new_post

import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.LayoutItemImageVideoListBinding
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import java.util.*


class VideoImageListAdapter(
    private val mContext: Context,
    private var mImageList: ArrayList<GalleryData>?
    , private val adapterItemClick: AdapterItemClick, private var isNeedToVisibleCountAndSelection: Boolean
) :
    RecyclerView.Adapter<VideoImageListAdapter.ProfileViewHolder>() {

    private var rowIndex = 0

    companion object {
        var itemPos: Int = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ProfileViewHolder {
        val binding = DataBindingUtil.inflate<LayoutItemImageVideoListBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_item_image_video_list, parent, false
        )
        binding.root.layoutParams.width = (getScreenWidth() / 5)

        return ProfileViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        itemPos = holder.adapterPosition

        holder.binding.listViewModel = mImageList!![position]
        holder.binding.executePendingBindings()

        if (isNeedToVisibleCountAndSelection) {
            holder.binding.position = holder.adapterPosition.plus(1).toString()

            holder.itemView.setOnClickListener {
                rowIndex = holder.adapterPosition
                notifyDataSetChanged()
                adapterItemClick.onAdapterItemClick(position.toString())


            }
            if (rowIndex == holder.adapterPosition) {
                holder.binding.itemLinear.setBackgroundResource(R.drawable.border_image)
                holder.binding.itemTextView.setBackgroundResource(R.drawable.border_text_view_white)
                holder.binding.itemTextView.setTextColor(Color.parseColor("#000000"))


            } else {
                holder.binding.itemLinear.setBackgroundResource(0)
                holder.binding.itemTextView.setBackgroundResource(R.drawable.border_text_view_grey)
                holder.binding.itemTextView.setTextColor(Color.parseColor("#EEEEEE"))


            }
        } else {
            holder.binding.itemLinear.setBackgroundResource(0)
            holder.binding.itemTextView.visibility = View.GONE
        }

    }

    override fun getItemCount(): Int {
        return mImageList!!.size
    }

    class ProfileViewHolder(val binding: LayoutItemImageVideoListBinding) : RecyclerView.ViewHolder(binding.root) {
//
    }

    private fun getScreenWidth(): Int {

        val wm = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)

        return size.x
    }

    interface AdapterItemClick {
        fun onAdapterItemClick(mPostion: String)
    }


}