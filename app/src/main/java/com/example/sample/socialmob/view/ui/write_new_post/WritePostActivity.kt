package com.example.sample.socialmob.view.ui.write_new_post

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.WritePostActivityBinding
import com.example.sample.socialmob.di.util.CacheMapperMention
import com.example.sample.socialmob.model.feed.ScrapperUrl
import com.example.sample.socialmob.model.feed.SingleFeedResponseModelPayload
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.profile.Meta
import com.example.sample.socialmob.model.profile.OgTags
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPost
import com.example.sample.socialmob.model.profile.PostFeedData
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.home_module.feed.MainFeedFragment
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.socialview.Hashtag
import com.example.sample.socialmob.view.utils.socialview.HashtagArrayAdapter
import com.example.sample.socialmob.view.utils.socialview.Mention
import com.example.sample.socialmob.view.utils.socialview.MentionArrayAdapter
import com.example.sample.socialmob.viewmodel.profile.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.write_post_activity.*
import java.util.regex.Pattern
import javax.inject.Inject

@AndroidEntryPoint
class WritePostActivity : BaseCommonActivity() {
    @Inject
    lateinit var glideRequestManager: RequestManager

    @Inject
    lateinit var cacheMapperMention: CacheMapperMention

    private var defaultHashtagAdapter: ArrayAdapter<Hashtag>? = null
    private var defaultMentionAdapter: ArrayAdapter<Mention>? = null

    private val profileUploadViewModel: ProfileUploadPostViewModel by viewModels()
    private var isHaveWebUrl = false
    private var isHyperlinksCalled = false
    private var binding: WritePostActivityBinding? = null
    private var customProgressDialog: CustomProgressDialog? = null

    private var isHashTag: Boolean = false
    private var isMention: Boolean = false
    private var mPostId: String = ""
    private var mEditDescription: String = ""
    private var mIsEdit: Boolean = false
    private var mEditPayload: SingleFeedResponseModelPayload? = null
    private var ogTags: OgTags? = OgTags()
    private var mData: PersonalFeedResponseModelPayloadFeedActionPost? = null
    private var isAdded: Boolean = false
    private var isUpdatedFeed: Boolean = false
    private var idPost: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.write_post_activity)

        val i = intent
        if (i?.getStringExtra("mPostId") != null) {
            mPostId = i.getStringExtra("mPostId") ?: ""
            mIsEdit = i.getBooleanExtra("mIsEdit", false)
            if (mIsEdit) {
                binding?.titleTextView?.text = getString(R.string.update)
            }
            if (InternetUtil.isInternetOn()) {
                callApi(mPostId)
                progress_bar.visibility = View.VISIBLE
                post_card_view.visibility = View.GONE
                post_text_view.text = getString(R.string.update)
            }
        }


        observeData()

        binding?.writePostBackImageView?.setOnClickListener {
            onBackPressed()
        }

        binding?.postTextView?.setOnClickListener {
            postTextFeed()
        }

        customProgressDialog = CustomProgressDialog(this)
        binding?.writePostEditText?.isHashtagEnabled = true
        binding?.writePostEditText?.isMentionEnabled = true
        binding?.writePostEditText?.isHyperlinkEnabled = true

        defaultMentionAdapter = MentionArrayAdapter(this)
        binding?.writePostEditText?.mentionAdapter = defaultMentionAdapter
        defaultHashtagAdapter = HashtagArrayAdapter(this)
        binding?.writePostEditText?.hashtagAdapter = defaultHashtagAdapter


        binding?.writePostEditText?.setHashtagTextChangedListener { view, text ->
            isHashTag = true
            isMention = false
            if (text.isNotEmpty()) {
                profileUploadViewModel.getHashTags(this, text.toString())
            }
        }
        observeHasTag()

        binding?.writePostEditText?.setMentionTextChangedListener { view, text ->
            isHashTag = false
            isMention = true
            if (text.isNotEmpty()) {
                defaultMentionAdapter?.clear()
                profileUploadViewModel.getMentions(this, text.toString())
            }
        }
        observeMentionData()

        binding?.writePostEditText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isHaveWebUrl = containsLink(s.toString())
                if (isHaveWebUrl) {
                    if (binding?.writePostEditText?.hyperlinks?.size ?: 0 > 0) {
                        isHyperlinksCalled = true
                        val mUrl = ScrapperUrl(binding?.writePostEditText?.hyperlinks?.get(0) ?: "")
                        if (ogTags?.url == null || ogTags?.url == "" || ogTags?.url != mUrl.url) {
                            profileUploadViewModel.getScrapData(mUrl)
                            binding?.scrapLinear?.visibility = View.GONE
                            binding?.progressBar?.visibility = View.VISIBLE
                        } else {
                            binding?.scrapLinear?.visibility = View.VISIBLE
                            binding?.progressBar?.visibility = View.GONE
                        }
                    } else {
                        isHyperlinksCalled = false
                    }
                }
            }
        })

        binding?.closeImageView?.setOnClickListener {
            binding?.scrapLinear?.visibility = View.GONE
            val ogTagsNew = OgTags()
            ogTagsNew.id = ""
            ogTagsNew.meta = Meta()
            ogTagsNew.url = ""
            ogTags = ogTagsNew
            profileUploadViewModel.scrapUrlResponseModel.postValue(null)
        }

        // TODO : To Handle Share Text
        if (intent?.action != null && intent?.clipData != null) {
            val clip = intent?.clipData!!
            if (clip.itemCount > 0) {
                var mData = clip.getItemAt(0).coerceToText(this).toString()
                Handler().postDelayed({
                    binding?.closeImageView?.performClick()
                    when {

                        //TODO : Need to change the content when we are sharing to socialmob app
                        //Track
                        mData.contains("This track on SocialMob is mind-blowing! Give it a listen, I'm sure you'll think the same!.") -> {
                            mData = mData.replace(
                                "This track on SocialMob is mind-blowing! Give it a listen, I'm sure you'll think the same!.",
                                "This track is amazing! Give it a listen by clicking on the link below."
                            )
                        }
                        //Podcast
                        mData.contains("This podcast on SocialMob is really good. I recommend you download the app now and tune in!") -> {
                            mData = mData.replace(
                                "This podcast on SocialMob is really good. I recommend you download the app now and tune in!",
                                "I think you'll find this podcast interesting! Tune in and find out if you do."
                            )
                        }
                        //Podcast episode
                        mData.contains("Listen to this mind-blowing episode from this podcast on SocialMob. Tune in by downloading the app now! You will love it!") -> {
                            mData = mData.replace(
                                "Listen to this mind-blowing episode from this podcast on SocialMob. Tune in by downloading the app now! You will love it!",
                                "This episode of the podcast is mind-blowing! Tune in to find out why."
                            )
                        }
                        //Post
                        mData.contains("See what your friends are upto. Check Socialmob to stay connected with your friends and never miss a thing.") -> {
                            mData = mData.replace(
                                "See what your friends are upto. Check Socialmob to stay connected with your friends and never miss a thing.",
                                "Hey! You need to check out this post. I think you will find it interesting!"
                            )
                        }
                        //Profile
                        mData.contains("Hey! This person is interesting! Check their profile out! .") -> {
                            mData = mData.replace(
                                "Hey! This person is interesting! Check their profile out! .",
                                "Hey! Check out this profile! I think you'll be interested to connect with this person!"
                            )
                        }
                        //Playlist
                        mData.contains("This is such a good playlist, you'll love it! Click on the link to have a listen.") -> {
                            mData = mData.replace(
                                "This is such a good playlist, you'll love it! Click on the link to have a listen.",
                                "This is such a kick-ass playlist! Here's the link so that you can also play it on repeat!"
                            )
                        }
                        //Artist
                        mData.contains("Hey, I'm sure you'll find this artist amazing! Check it out.") -> {
                            mData = mData.replace(
                                "Hey, I'm sure you'll find this artist amazing! Check it out.",
                                "I think this artist is super awesome! Here's the link to their tracks; I have a feeling you'll love them as much as I do!"
                            )
                        }
                    }
                    binding?.writePostEditText?.setText(mData)
                }, 1000)

            }
        }
    }

    private fun observeMentionData() {
        profileUploadViewModel.mentionProfileResponseModel.nonNull().observe(this, { profiles ->
            if (profiles != null) {
                val mMentionList: MutableList<Mention>
                if (profiles.isNotEmpty()) {
                    val mMentionEditTextList: MutableList<String> =
                        binding?.writePostEditText?.mentions ?: ArrayList()

                    val mPopulateList: MutableList<Profile> =
                        profiles.filter {
                            it.username !in mMentionEditTextList.map { item ->
                                item.replace("@", "")
                            }
                        } as MutableList<Profile>
                    mMentionList =
                        cacheMapperMention.mapFromEntityListMention(mPopulateList).toMutableList()
                    defaultMentionAdapter?.addAll(mMentionList)
                }

                if (defaultMentionAdapter == null) {
                    defaultMentionAdapter = MentionArrayAdapter(this)
                    binding?.writePostEditText?.mentionAdapter = defaultMentionAdapter
                } else {
                    binding?.writePostEditText?.mentionAdapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun callApi(mPostId: String) {

        profileUploadViewModel.getFeedDetails(mPostId)
        profileUploadViewModel.responseModel.nonNull().observe(this, { responseModel ->

            if (responseModel?.payload != null && responseModel.payload?.post != null && responseModel.payload?.post?.action != null &&
                responseModel.payload?.post?.action?.post != null && responseModel.payload?.post?.action?.post?.description != null
            ) {
                binding?.writePostEditText?.setText(responseModel.payload?.post?.action?.post?.description)
                mEditDescription = responseModel.payload?.post?.action?.post?.description!!
                mEditPayload = responseModel.payload!!

            }
            if (responseModel?.payload != null && responseModel.payload?.post != null && responseModel.payload?.post?.action != null
                && responseModel.payload?.post?.action?.post != null && responseModel.payload?.post?.action?.post?.ogTags != null
            ) {
                ogTags = responseModel.payload?.post?.action?.post?.ogTags
            }
            binding?.progressBar?.visibility = View.GONE
            binding?.postCardView?.visibility = View.VISIBLE
            binding?.closeImageView?.bringToFront()


            if (ogTags != null && ogTags?.meta != null && ogTags?.id != null && ogTags?.id != "") {
                binding?.scrapLinear?.visibility = View.VISIBLE
                if (ogTags?.meta?.ogDescription != null) {
                    binding?.ogDescription?.text = ogTags?.meta?.ogDescription
                }
                if (ogTags?.meta?.ogTitle != null) {
                    binding?.ogTitle?.text = ogTags?.meta?.ogTitle
                }

                if (ogTags?.meta?.ogImage != null && ogTags?.meta?.ogImage?.url != null) {
                    glideRequestManager.load(ogTags?.meta?.ogImage?.url)
                        .into(binding?.ogImageView!!)
                }
            }

        })
    }

    private fun postTextFeed() {
        if (binding?.writePostEditText?.text.toString().trim().isNotEmpty() ||
            (ogTags != null && ogTags?.url != null)
        ) {

            customProgressDialog?.show()
            val postData = PostFeedData()

            postData.description = binding?.writePostEditText?.text.toString()
            postData.type = "text"
            postData.media = null
            postData.ogTags = ogTags
            if (!InternetUtil.isInternetOn()) {
                AppUtils.showCommonToast(this@WritePostActivity, getString(R.string.no_internet))
            } else {
                if (mIsEdit) {
                    profileUploadViewModel.updatePost(postData, mPostId)
                } else {
                    profileUploadViewModel.uploadTextPost(postData)
                }
            }

            profileUploadViewModel.textFeedResponseMode.nonNull()
                .observe(this, { feedResponseModel ->
                    customProgressDialog?.dismiss()
                    if (feedResponseModel?.payload?.success == true) {
                        mData = feedResponseModel.payload.post
                        idPost = feedResponseModel.payload.post?.id!!
                        if (!mIsEdit) {
                            isAdded = true
                        } else {
                            isUpdatedFeed = true
                        }
                        onBackPressed()
                    }
                })
            profileUploadViewModel.textFeedResponseMThrowable.nonNull().observe(this, { exception ->
                if (exception != null) {
                    AppUtils.showCommonToast(this, "Something went wrong")
                    if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
                        customProgressDialog?.dismiss()
                    }
                    finish()
                }
            })
        } else {
            Toast.makeText(
                this@WritePostActivity, "You haven't written anything.", Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun observeData() {
        profileUploadViewModel.scrapUrlResponseModel.nonNull()
            .observe(this, { scrapUrlResponseModel ->
                if (scrapUrlResponseModel?.payload?.success == true) {
                    ogTags = scrapUrlResponseModel.payload?.ogTags
                    binding?.viewmodel = scrapUrlResponseModel
                    binding?.executePendingBindings()
                    scrap_linear.visibility = View.VISIBLE
                    progress_bar.visibility = View.GONE
                    close_image_view.bringToFront()
                } else {
                    progress_bar.visibility = View.GONE
                }
                profileUploadViewModel.scrapUrlResponseModel.value = null
            })
    }

    /**
     *  Check string contains URL
     */
    private fun containsLink(input: String): Boolean {
        val regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"
        val p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
        val m = p.matcher(input)
        return m.find()
    }

    private fun observeHasTag() {
        profileUploadViewModel.hashTagsResponseModel.nonNull().observe(this, { hashTagList ->
            binding?.writePostEditText?.hashtagAdapter?.clear()
            if (hashTagList.isNotEmpty()) {
                for (i in hashTagList.indices) {
                    defaultHashtagAdapter?.add(
                        Hashtag(hashTagList[i].name!!)
                    )
                }
            }
            Handler().postDelayed(
                {
                    binding?.writePostEditText?.hashtagAdapter?.notifyDataSetChanged()
                }, 100
            )
        })
    }

    override fun onBackPressed() {
        if (SharedPrefsUtils.getStringPreference(this, "POST_FROM", "") == "MAIN_FEED") {
            MainFeedFragment.mAddFeeData = mData
            if (!mIsEdit) {
                MainFeedFragment.isAddedFeed = isAdded
            } else {
                MainFeedFragment.isUpdatedFeed = isUpdatedFeed
            }
            MainFeedFragment.idPost = idPost
        } else {
            ProfileFeedFragment.mAddFeeData = mData
            if (!mIsEdit) {
                ProfileFeedFragment.isAddedFeed = isAdded
            } else {
                ProfileFeedFragment.isUpdatedFeed = isUpdatedFeed
            }
            ProfileFeedFragment.idPost = idPost
        }
        super.onBackPressed()
        if (binding != null) {
            AppUtils.hideKeyboard(this@WritePostActivity, binding?.writePostBackImageView!!)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customProgressDialog != null && customProgressDialog?.isShowing!!)
            customProgressDialog?.dismiss()
    }
}