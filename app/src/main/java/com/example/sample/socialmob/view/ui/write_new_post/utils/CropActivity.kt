package com.example.sample.socialmob.view.ui.write_new_post.utils

import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.CustomProgressDialog
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.nonNull
import com.example.sample.socialmob.view.ui.write_new_post.Config.KeyName.IMAGE_LIST
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.galleryPicker.view.PickerActivity
import com.example.sample.socialmob.viewmodel.profile.ProfileUploadPostViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_crop.*
import java.io.File
import java.util.*

@AndroidEntryPoint
class CropActivity : AppCompatActivity(), CropImageListAdapter.AdapterItemClick {
    private var mImageList: ArrayList<GalleryData> = ArrayList()
    private var mOldList: ArrayList<GalleryData> = ArrayList()
    private var mImageListNew: ArrayList<GalleryData> = ArrayList()
    private var isFromProfilePic = false
    private var isNeedToAd = false
    private var customProgressDialog: CustomProgressDialog? = null
    private var title: String? = null
    private var mRatio: String? = "1:1"
    private val profileUploadViewModel: ProfileUploadPostViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.activity_crop)

        customProgressDialog = CustomProgressDialog(this)
        isFromProfilePic = intent.getBooleanExtra("IS_FROM_CREATE_PROFILE", false)
        // TODO : We have no permission add new photos when navigate from camera capture
        isNeedToAd = intent.getBooleanExtra("IS_NEED_TO_ADD", true)
        try {
            mOldList = intent.getParcelableArrayListExtra(IMAGE_LIST)!!
            mImageList = intent.getParcelableArrayListExtra(IMAGE_LIST)!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        title = intent.getStringExtra("title")



        try {
            // Only
            if (mImageList.size < 5) {
                if (isNeedToAd) {
                    item_add_linear.visibility = View.VISIBLE
                } else {
                    item_add_linear.visibility = View.GONE
                }
            } else {
                item_add_linear.visibility = View.GONE

            }
            item_add_linear.setOnClickListener {
                onBackPressed()
            }
            val pagerAdapter = CropPagerAdapter(
                supportFragmentManager, mImageList, 1, 1,
                isFromProfilePic
            )
            viewPager.adapter = pagerAdapter
            viewPager.offscreenPageLimit = 5


            videoImageListRecyclerView.adapter = CropImageListAdapter(
                this@CropActivity,
                mImageList, this
            )
            uploadconfirm_back_image_view.setOnClickListener {
                onBackPressed()
            }
            if (isFromProfilePic) {
                videoImageListRecyclerView.visibility = View.INVISIBLE
            }

            save_button.setOnClickListener {
                if (mImageList.size == 1) {
                    CropFirstFragmentRatio.cropImageAsync(this)

                }
                if (mImageList.size == 2) {
                    CropFirstFragmentRatio.cropImageAsync(this)
                    SecondCropFragmentRatio.cropImageAsync(this)

                }
                if (mImageList.size == 3) {
                    CropFirstFragmentRatio.cropImageAsync(this)
                    SecondCropFragmentRatio.cropImageAsync(this)
                    ThirdCropFragmentRatio.cropImageAsync(this)

                }
                if (mImageList.size == 4) {
                    CropFirstFragmentRatio.cropImageAsync(this)
                    SecondCropFragmentRatio.cropImageAsync(this)
                    ThirdCropFragmentRatio.cropImageAsync(this)
                    FouthCropFragmentRatio.cropImageAsync(this)

                }
                if (mImageList.size == 5) {
                    CropFirstFragmentRatio.cropImageAsync(this)
                    SecondCropFragmentRatio.cropImageAsync(this)
                    ThirdCropFragmentRatio.cropImageAsync(this)
                    FouthCropFragmentRatio.cropImageAsync(this)
                    FifthCropFragmentRatio.cropImageAsync(this)
                }

            }
        } catch (e: Exception) {
            finish()
        }

    }


    fun addPhotoUri(mUri: String) {

        runOnUiThread {
            // Stuff that updates the UI
            customProgressDialog?.show()
        }
        val data = GalleryData()
        data.photoUri = mUri
        mImageListNew.add(data)

        if (mImageListNew.size == mImageList.size) {
            customProgressDialog?.dismiss()
//            val intent = Intent(this, FilterActivity::class.java)
//            intent.putParcelableArrayListExtra("MEDIA", mImageListNew!!)
//            intent.putExtra("title", title)
//            intent.putParcelableArrayListExtra("MEDIA", mImageListNew)
//            intent.putExtra("IS_FROM_CREATE_PROFILE", isFromProfilePic)
//            intent.putExtra("RATIO", mRatio)
//            startActivity(intent)
//            overridePendingTransition(0, 0)
//            this.finish()
            if (isFromProfilePic) {
                if (InternetUtil.isInternetOn()) {
                    if (mImageList.size > 0) {
                        customProgressDialog?.show()

                        // TODO : Upload Profile Image to Server
                        val fullData = RemoteConstant.getEncryptedString(
                            SharedPrefsUtils.getStringPreference(this, RemoteConstant.mAppId, "")!!,
                            SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mApiKey,
                                ""
                            )!!,
                            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mPresignedurl +
                                    RemoteConstant.mSlash + "image" + RemoteConstant.mSlash + "1",
                            RemoteConstant.mGetMethod
                        )
                        val mAuth =
                            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                this,
                                RemoteConstant.mAppId,
                                ""
                            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
                        profileUploadViewModel.getPreSignedUrlProfile(
                            mAuth,
                            "image",
                            "1",
                            mImageListNew[0].photoUri,
                            ""
                        )

                        profileUploadViewModel.isUploaded.nonNull()
                            .observe(this, {
                                if (it == true) {
                                    finish()
                                } else {
                                    AppUtils.showCommonToast(
                                        this,
                                        "Error occur please try again later"
                                    )
                                }
                                customProgressDialog?.dismiss()
                            })
                    }

                } else {
                    AppUtils.showCommonToast(this, getString(R.string.no_internet))
                    customProgressDialog?.dismiss()
                }
            } else {
                customProgressDialog?.dismiss()
                if (mImageListNew.size == mImageList.size) {

                    val intent = Intent(this, UploadPhotoActivityMentionHashTag::class.java)
                    intent.putExtra("title", title)
                    if (mImageListNew[0].photoUri.contains(".mp4")) {
                        intent.putExtra("type", "video")
                        intent.putExtra("mImageList", mImageListNew)
                    } else {
                        intent.putExtra("type", "image")
                        intent.putExtra("mImageList", mImageListNew)
                    }
                    intent.putExtra("IS_FROM_CREATE_PROFILE", isFromProfilePic)
                    intent.putExtra("RATIO", mRatio)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    this.finish()
                    if (PickerActivity.mContext != null) {
                        PickerActivity.mContext?.finish()
                    }
                }


            }

        }

    }

    override fun onBackPressed() {
        delete()
        super.onBackPressed()
        AppUtils.hideKeyboard(this@CropActivity, uploadconfirm_back_image_view)
    }


    override fun onDestroy() {
        super.onDestroy()
//        for (i in 0 until mOldList.size) {
//            val myFile = File(mOldList[i].photoUri)
//            if (myFile.exists())
//                myFile.delete()
//        }
        if (customProgressDialog != null && customProgressDialog?.isShowing!!) {
            customProgressDialog?.dismiss()
        }
    }

    fun setRatio(mWidth: Int, mHeight: Int, mPos: Int) {
        viewPager.adapter = null
        mRatio = "$mWidth:$mHeight"

        val pagerAdapter =
            CropPagerAdapter(supportFragmentManager, mImageList, mWidth, mHeight, isFromProfilePic)
        viewPager.adapter = pagerAdapter
        pagerAdapter.notifyDataSetChanged()
        viewPager.currentItem = mPos
    }

    private fun changeStatusBarColor() {
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

    private fun delete() {
        val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
        if (dir.isDirectory) {
            val children = dir.list()
            if (children != null && children.isNotEmpty()) {
                for (i in children.indices) {
                    File(dir, children[i]).delete()
                }
            }
        }
    }

    override fun onAdapterItemClick(mPosition: String) {
        // TODO Handle item click
        viewPager.currentItem = mPosition.toInt()
    }

}
