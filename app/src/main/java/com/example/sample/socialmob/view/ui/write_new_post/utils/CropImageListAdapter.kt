package com.example.sample.socialmob.view.ui.write_new_post.utils

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.Point
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.databinding.SmallImageCropBinding
import com.example.sample.socialmob.view.utils.BaseViewHolder
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import java.util.*


class CropImageListAdapter(
    private val mContext: Context,
    private var mImageList: ArrayList<GalleryData>?, private val adapterItemClick: AdapterItemClick

) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Any>>() {
    companion object {
        const val ITEM_ADD = 1
        const val ITEM_IMAGE = 2
    }

    override fun getItemViewType(position: Int): Int {

        return if (mImageList!![position].photoUri == "") {
            ITEM_ADD
        } else {
            ITEM_IMAGE
        }
//        return super.getItemViewType(position)
    }

    private var rowIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {

        return when (viewType) {
            ITEM_ADD -> bindAdd(parent)
            ITEM_IMAGE -> bindData(parent)
            else -> bindData(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        holder.bind(mImageList!![position])

    }

    private fun bindAdd(parent: ViewGroup): BaseViewHolder<Any> {


        val binding: SmallImageCropBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.small_image_crop, parent, false
        )
        return ViewHolderAdd(binding)

    }

    private fun bindData(parent: ViewGroup): BaseViewHolder<Any> {

        val binding: SmallImageCropBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.small_image_crop, parent, false
        )
        return ViewHolderImage(binding)
    }

    inner class ViewHolderImage(val binding: SmallImageCropBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.listViewModel = mImageList!![adapterPosition]
            binding.executePendingBindings()

            binding.position = adapterPosition.plus(1).toString()

            itemView.setOnClickListener {
                rowIndex = adapterPosition
                adapterItemClick.onAdapterItemClick(adapterPosition.toString())
                notifyDataSetChanged()
            }
            if (rowIndex == adapterPosition) {
                binding.itemLinear.setBackgroundResource(R.drawable.border_image)
                binding.itemTextView.setBackgroundResource(R.drawable.border_text_view_white)
                binding.itemTextView.setTextColor(Color.parseColor("#000000"))
            } else {
                binding.itemLinear.setBackgroundResource(0)
                binding.itemTextView.setBackgroundResource(R.drawable.border_text_view_grey)
                binding.itemTextView.setTextColor(Color.parseColor("#EEEEEE"))
            }
        }
    }

    override fun getItemCount(): Int {
        return mImageList!!.size
    }

    inner class ViewHolderAdd(val binding: SmallImageCropBinding) : BaseViewHolder<Any>(binding.root) {
        override fun bind(`object`: Any?) {
            binding.itemImageView.setImageResource(R.drawable.ic_add_stories)
            binding.itemImageView.setBackgroundResource(R.color.white)
            itemView.setOnClickListener {
                (mContext as CropActivity).onBackPressed()
            }
        }
    }

    interface AdapterItemClick {
        fun onAdapterItemClick(mPosition: String)
    }

}