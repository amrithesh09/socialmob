package com.example.sample.socialmob.view.ui.write_new_post.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import java.util.*

class CropPagerAdapter internal constructor(
    fm: FragmentManager,
    private var mImageList: ArrayList<GalleryData>,
    private val mWidth: Int, private val mHeight: Int, private val fromProfilePic: Boolean
) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = CropFirstFragmentRatio.newInstance(
                mImageList[0].photoUri, mImageList.size, mWidth,
                mHeight,fromProfilePic
            )
            1 -> fragment = SecondCropFragmentRatio.newInstance(
                mImageList[1].photoUri, mImageList.size, mWidth,
                mHeight
            )
            2 -> fragment = ThirdCropFragmentRatio.newInstance(
                mImageList[2].photoUri, mImageList.size, mWidth,
                mHeight
            )
            3 -> fragment = FouthCropFragmentRatio.newInstance(
                mImageList[3].photoUri, mImageList.size, mWidth,
                mHeight
            )
            4 -> fragment = FifthCropFragmentRatio.newInstance(
                mImageList[4].photoUri, mImageList.size, mWidth,
                mHeight
            )
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return mImageList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Tab " + (position + 1)
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }
}