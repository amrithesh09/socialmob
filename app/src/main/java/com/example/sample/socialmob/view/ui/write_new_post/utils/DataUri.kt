package com.example.sample.socialmob.view.ui.write_new_post.utils

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataUri(var photoUri: String = ""): Parcelable