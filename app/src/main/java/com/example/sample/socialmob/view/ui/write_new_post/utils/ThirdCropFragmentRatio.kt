package com.example.sample.socialmob.view.ui.write_new_post.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.write_new_post.utils.cropper.BitmapUtils
import com.example.sample.socialmob.view.ui.write_new_post.utils.imagecrop.view.ImageCropView
import java.io.File
import java.io.FileInputStream

class ThirdCropFragmentRatio : Fragment() {
    private var ratio11btn: Button? = null
    private var ratio34btn: Button? = null
    private var ratio43btn: Button? = null
    private var ratio169btn: Button? = null
    private var rotatedBitmap: Bitmap? = null

    companion object {
        private const val ARG_STRING = "mString"
        private const val ARG_INT = "mInt"

        private const val ARG_WIDTH = "mWidth"
        private const val ARG_HEIGHT = "mHeight"


        private var imageCropView: ImageCropView? = null
        fun newInstance(
            mName: String,
            size: Int,
            mWidth: Int,
            mHeight: Int
        ): ThirdCropFragmentRatio {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            args.putSerializable(ARG_INT, size)
            args.putSerializable(ARG_WIDTH, mWidth)
            args.putSerializable(ARG_HEIGHT, mHeight)
            val fragment = ThirdCropFragmentRatio()
            fragment.arguments = args
            return fragment
        }

        // Save image to local storage
        fun cropImageAsync(cropActivity: CropActivity) {
            if (imageCropView != null && imageCropView?.croppedImage != null) {
                val bitmap = imageCropView?.croppedImage
                if (bitmap != null) {

                    val fileDir =
                        File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
                    if (!fileDir.exists()) {
                        fileDir.mkdir()
                    }
                    val file = File(fileDir, "/" + System.currentTimeMillis() + "socialmob.png")

                    BitmapUtils.writeBitmapToFile(bitmap, file, 90)
                    cropActivity.addPhotoUri(file.toString())
                    bitmap.recycle()
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val args = arguments
        val mFilePath = args?.getString(ARG_STRING, "")!!
        val mSize = args.getInt(ARG_INT)

        val mWidth = args.getInt(ARG_WIDTH)
        val mHeight = args.getInt(ARG_HEIGHT)
        // TODO  : update ratio
        when ("$mWidth:$mHeight") {
            "1:1" -> {
                ratio11btn!!.setBackgroundResource(R.drawable.rounded_status_crop)
                ratio34btn!!.setBackgroundResource(0)
                ratio43btn!!.setBackgroundResource(0)
                ratio169btn!!.setBackgroundResource(0)
            }
            "3:4" -> {
                ratio11btn!!.setBackgroundResource(0)
                ratio34btn!!.setBackgroundResource(R.drawable.rounded_status_crop)
                ratio43btn!!.setBackgroundResource(0)
                ratio169btn!!.setBackgroundResource(0)
            }
            "4:3" -> {
                ratio11btn!!.setBackgroundResource(0)
                ratio34btn!!.setBackgroundResource(0)
                ratio43btn!!.setBackgroundResource(R.drawable.rounded_status_crop)
                ratio169btn!!.setBackgroundResource(0)
            }
            "16:9" -> {
                ratio11btn!!.setBackgroundResource(0)
                ratio34btn!!.setBackgroundResource(0)
                ratio43btn!!.setBackgroundResource(0)
                ratio169btn!!.setBackgroundResource(R.drawable.rounded_status_crop)
            }
        }
        try {
            if (mBitmap != null) {
                mBitmap!!.recycle()
                mBitmap = null
            }
            if (rotatedBitmap != null) {
                rotatedBitmap!!.recycle()
                rotatedBitmap = null
            }
            rotationCount = 0
            val options = BitmapFactory.Options()
            options.inSampleSize = 3
            options.inJustDecodeBounds = false
//            mBitmap = decodeImageFile(File(mFilePath), 1280, 720)
            mBitmap = decodeImageFile(File(mFilePath), 640, 360)
            val ei = ExifInterface(mFilePath)
            val orientation = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            // Check orientation ( For samsung devices )
            // https://stackoverflow.com/questions/20478765/how-to-get-the-correct-orientation-of-the-image-selected-from-the-default-image
            rotatedBitmap = when (orientation) {

                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(mBitmap!!, 90)

                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(mBitmap!!, 180)

                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(mBitmap!!, 270)

                ExifInterface.ORIENTATION_NORMAL -> mBitmap!!
                else -> mBitmap!!
            }

            imageCropView!!.setImageBitmap(rotatedBitmap)
            imageCropView!!.setAspectRatio(mWidth, mHeight)

            ratio11btn!!.setOnClickListener { v ->
                if (isPossibleCrop(1, 1)) {
                    (activity as CropActivity).setRatio(1, 1, 2)
                } else {
                    Toast.makeText(activity, R.string.can_not_crop, Toast.LENGTH_SHORT).show()
                }
            }

            ratio34btn!!.setOnClickListener { v ->
                if (isPossibleCrop(3, 4)) {
                    (activity as CropActivity).setRatio(3, 4, 2)

                } else {
                    Toast.makeText(activity, R.string.can_not_crop, Toast.LENGTH_SHORT).show()
                }
            }

            ratio43btn!!.setOnClickListener { v ->
                if (isPossibleCrop(4, 3)) {
                    (activity as CropActivity).setRatio(4, 3, 2)
                } else {
                    Toast.makeText(activity, R.string.can_not_crop, Toast.LENGTH_SHORT).show()
                }
            }

            ratio169btn!!.setOnClickListener { v ->
                if (isPossibleCrop(16, 9)) {
                    (activity as CropActivity).setRatio(16, 9, 2)
                } else {
                    Toast.makeText(activity, R.string.can_not_crop, Toast.LENGTH_SHORT).show()
                }
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun decodeImageFile(f: File, WIDTH: Int, HIGHT: Int): Bitmap? {
        try {
            //Decode image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(FileInputStream(f), null, o)

            //The new size we want to scale to
            //Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= WIDTH && o.outHeight / scale / 2 >= HIGHT)
                scale *= 2

            //Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(FileInputStream(f), null, o2)
        } catch (e: Exception) {
        }

        return null
    }
    /**
     * Check
     */
    private fun isPossibleCrop(widthRatio: Int, heightRatio: Int): Boolean {
        val bitmap = imageCropView!!.viewBitmap ?: return false
        val bitmapWidth = bitmap.width
        val bitmapHeight = bitmap.height
        return !(bitmapWidth < widthRatio && bitmapHeight < heightRatio)
    }

    private var mBitmap: Bitmap? = null

    private var rotationCount = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.slidingimages_layout_ratio, container, false)

        imageCropView = view.findViewById(R.id.imageCropView)
        ratio11btn = view.findViewById(R.id.ratio11btn)
        ratio34btn = view.findViewById(R.id.ratio34btn)
        ratio43btn = view.findViewById(R.id.ratio43btn)
        ratio169btn = view.findViewById(R.id.ratio169btn)



        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        imageCropView = null
        if (mBitmap != null) {
            mBitmap!!.recycle()
            mBitmap = null
        }
        if (rotatedBitmap != null) {
            rotatedBitmap!!.recycle()
            rotatedBitmap = null
        }
    }

}
