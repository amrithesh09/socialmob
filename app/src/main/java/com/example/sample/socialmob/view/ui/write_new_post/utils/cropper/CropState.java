package com.example.sample.socialmob.view.ui.write_new_post.utils.cropper;

public enum CropState {
    STARTED,
    SUCCESS,
    ERROR,
    FAILURE_GESTURE_IN_PROCESS
}
