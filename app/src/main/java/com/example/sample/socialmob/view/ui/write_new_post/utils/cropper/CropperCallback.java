package com.example.sample.socialmob.view.ui.write_new_post.utils.cropper;

import android.graphics.Bitmap;

public abstract class CropperCallback {

    public void onStarted() {

    }

    public abstract void onCropped(Bitmap bitmap);

    public void onOutOfMemoryError() {

    }

    public void onError() {

    }
}
