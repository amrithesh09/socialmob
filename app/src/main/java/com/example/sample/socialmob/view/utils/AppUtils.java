package com.example.sample.socialmob.view.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class AppUtils {

    @NotNull
    public static final String SELECTED_CATEGORY = "SELECTED_CATEGORY";
    @NotNull
    public static final String SELECTED_FILTER = "SELECTED_FILTER";
    @NotNull
    public static final String SELECTED_IS_FROM_PODCAST_OR_GENRE = "SELECTED_IS_FROM_PODCAST_OR_GENRE";

    @NotNull
    public static final String IS_PLAYING_RADIO = "false";

    public static File file;

    public static void hideKeyboard(Context context, View view) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        }

    }
    public static void hideKeyboards(Context context) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        }

    }

    public static void showKeyboard(Context context, View view) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }

    }


    public static void showCommonToast(@NotNull Context mContext, @Nullable String mMessage) {
        // Set this up in the UI thread.

        new CustomTask(mContext, mMessage).execute();
//        Toast.makeText(mContext, mMessage, Toast.LENGTH_SHORT).show();
    }


    private static String getDurationString(int millis) {

//        int hours = seconds / 3600;
//        int minutes = (seconds % 3600) / 60;
//        seconds = seconds % 60;
//
//        return twoDigitString(minutes) + " : " + twoDigitString(seconds);
        String hms = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        System.out.println(hms);
        return hms;
    }

    private static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    @SuppressLint("StaticFieldLeak")
    private static class CustomTask extends AsyncTask<Void, Void, Void> {

        private Context mContext;
        private String mMessage;

        CustomTask(Context mContext, String mMessage) {

            this.mContext = mContext;
            this.mMessage = mMessage;
        }

        protected Void doInBackground(Void... param) {
            //Do some work
            return null;
        }

        protected void onPostExecute(Void param) {
            //Print Toast or open dialog
            Toast.makeText(mContext, mMessage, Toast.LENGTH_SHORT).show();
//            Snackbar.make(mContext, "Got the ResultResponse", Snackbar.LENGTH_LONG)
//                    .setAction("Submit", mOnClickListener)
//                    .setActionTextColor(Color.RED)
//                    .show();
        }
    }
}

