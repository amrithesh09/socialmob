package com.example.sample.socialmob.view.utils

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import com.androidisland.vita.VitaOwner
import com.androidisland.vita.vita
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.chat.WebSocketResponse
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.chat.model.PongData
import com.example.sample.socialmob.viewmodel.chat.WebSocketChatViewModel
import com.example.sample.socialmob.viewmodel.feed.PingData
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers

@AndroidEntryPoint
open class BaseCommonActivity : AppCompatActivity() {
    private var rxTask: Disposable? = null
    private val viewModelChat: WebSocketChatViewModel by lazy {
        vita.with(VitaOwner.Multiple(this)).getViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // TODO : Dark mode
        val mMode =
            SharedPrefsUtils.getBooleanPreference(this, RemoteConstant.NIGHT_MODE, false)
        if (mMode) {
            setTheme(R.style.AppTheme_NoActionBarTranslucentNight)
        } else {
            setTheme(R.style.AppTheme_NoActionBarTranslucent)
        }
        super.onCreate(savedInstanceState)
        if (mMode) {
            changeStatusBarColorBlack()
        } else {
            changeStatusBarColor()
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            val currentOrientation = resources.configuration.orientation
            requestedOrientation = if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        }


        rxTask = viewModelChat.rxWebSocket?.onTextMessage()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnNext { socketMessageEvent ->
                val isActivityInForeground =
                    lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)
                if (isActivityInForeground) {
                    handleMessage(socketMessageEvent?.text)
                }
            }
            ?.subscribe(
                Functions.emptyConsumer(),
                { throwable: Throwable -> println(throwable) })
    }

    private fun handleMessage(text: String?) {
        val sourceType: String
        try {
            val gsonWebSocketResponse = GsonBuilder()
                .setLenient()
                .create()
            val webSocketResponse: WebSocketResponse? =
                gsonWebSocketResponse.fromJson(text, WebSocketResponse::class.java)
            sourceType = webSocketResponse?.type!!
            when (sourceType) {
                "ping" -> {
                    val mPongData =
                        PongData()
                    val mPingData = PingData()

                    mPingData.pingId = webSocketResponse.pingData?.pingId

                    mPongData.type = "pong"
                    mPongData.from = SharedPrefsUtils.getStringPreference(
                        this, RemoteConstant.mProfileId, ""
                    )
                    mPongData.pingData = mPingData
                    val mGson = Gson()
                    val mMessage = mGson.toJson(mPongData)
//                    println(">>>>>>>>>>Send ping from base activity")
                    viewModelChat.sendMessageSm(mMessage)
                }
            }
        } catch (e: Exception) {
            AppUtils.showCommonToast(this, "" + e.printStackTrace())
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (rxTask != null) {
            rxTask?.dispose()
        }
    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    private fun changeStatusBarColorBlack() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    }

}
