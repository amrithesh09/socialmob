package com.example.sample.socialmob.view.utils

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Typeface
import android.text.format.DateUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.utils.MyApplication.Companion.application
import com.example.sample.socialmob.view.utils.SharedPrefsUtils.Companion.getBooleanPreference
import com.example.sample.socialmob.view.utils.materialLikeButton.MaterialFavoriteButton
import com.example.sample.socialmob.view.utils.socialview.SocialTextView
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import javax.inject.Inject

//object BindingAdapterApp @Inject constructor(var req:RequestOptions){
object BindingAdapterApp{
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView?, url: String?) {
        try {
            if (view != null) {
                if (url != null && url != "" && !url.contains("googleusercontent")) {
                    Glide.with(view).load(getImageUrlFromWidth(url, 500))
                        .thumbnail(0.1f)
                        .apply(
                            RequestOptions.placeholderOf(R.drawable.emptypicture)
                                .error(R.drawable.emptypicture)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                        )
                        .into(view)
                } else {
                    if (url == null || url == "") {
                        Glide.with(view).load(R.drawable.emptypicture)
                            .into(view)
                    } else if (url != "") {
                        if (url!!.contains(".jpg")) {
                            val str = url.replace("s96-c/photo.jpg", "")
                            val str2 = str + "s700-c/photo.jpg"
                            Glide.with(view).load(str2).thumbnail(0.1f)
                                .apply(
                                    RequestOptions.placeholderOf(R.drawable.emptypicture)
                                        .error(R.drawable.emptypicture)
                                        .centerCrop()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .priority(Priority.HIGH)
                                )
                                .into(view)
                        } else {
                            val str = url.replace("s96-c", "")
                            val str2 = str + "s700-c"
                            Glide.with(view).load(str2).thumbnail(0.1f)
                                .apply(
                                    RequestOptions.placeholderOf(R.drawable.emptypicture)
                                        .error(R.drawable.emptypicture)
                                        .centerCrop()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .priority(Priority.HIGH)
                                )
                                .into(view)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrlFitXy")
    fun loadImageFitXy(view: ImageView?, url: String?) {
        try {
            if (view != null) {
                if (url != null && url != "") {
//                    RequestBuilder<Drawable> thumbnail = Glide.with(view)
//                            .load(getImageUrlFromWidth(url, 100));
//                    Glide.with(view).load(url).thumbnail(thumbnail)
                    Glide.with(view).load(url).thumbnail(0.1f)
                        .apply(
                            RequestOptions.placeholderOf(R.drawable.emptypicture)
                                .error(R.drawable.emptypicture)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                        )
                        .into(view)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("srcImage")
    fun loadsrcImage(view: ImageView?, url: String?) {
        try {
            if (view != null) {
                if (url != null && url != "") {
                    Glide.with(view).load(url)
                        .apply(
                            RequestOptions.placeholderOf(R.drawable.emptypicture)
                                .error(R.drawable.emptypicture)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                        )
                        .into(view)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    @JvmStatic
    @BindingAdapter("setprogress")
    fun setprogress(view: ProgressBar, mProgress: String?) {
        try {
            if (mProgress != null) {
                view.progress = mProgress.toInt()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrlPalaceHolder")
    fun loadImageUrlPalaceHolder(
        view: ImageView?,
        url: String?
    ) {
        try {
            if (view != null && url != null && url != "") {
                Glide.with(view).load(getImageUrlFromWidth(url, 500))
                    .thumbnail(0.1f)
                    .apply(
                        RequestOptions.placeholderOf(R.drawable.emptypicture)
                            .error(R.drawable.emptypicture)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                    )
                    .into(view)
            }else{
                if (view!=null) {
                    Glide.with(view).load(R.drawable.emptypicture)
                        .into(view)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrlFeed")
    fun loadImageUrlFeed(view: ImageView?, url: String?) {
        try {
            if (view != null) {
                if (url != null && url != "") {
                    Glide.with(view).load(url)
                        .apply(
                            RequestOptions.placeholderOf(R.drawable.emptypicture)
                                .error(R.drawable.emptypicture)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                        )
                        .into(view)
                } else {
                    Glide.with(view).load(R.drawable.emptypicture)
                        .into(view)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrlResizePalaceHolder")
    fun loadimageUrlResizePalaceHolder(
        view: ImageView?,
        url: String?
    ) {
        try {
            if (view != null) {
//                RequestBuilder<Drawable> thumbnail = MyApplication.mGlideInstances?
//                        .load(getImageUrlFromWidth(url, 10));

//                MyApplication.mGlideInstances?.load(getImageUrlFromWidth(url, 200)).thumbnail(thumbnail)
                Glide.with(view).load(getImageUrlFromWidth(url, 200))
                    .thumbnail(0.1f)
                    .apply(
                        RequestOptions.placeholderOf(0).error(0).centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                    )
                    .into(view)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrlLocal")
    fun loadLocalImage(view: ImageView?, url: String?) {
        try {
            if (view != null) {
                if (url != null && url != "") {
                    Glide.with(view).load(url).into(view)
                } else {
                    Glide.with(view).load(R.drawable.emptypicture).into(view)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("loadFavourite")
    fun loadFavourite(
        materialFavoriteButton: MaterialFavoriteButton,
        isTrue: Boolean?
    ) {
        if (isTrue != null) {
            materialFavoriteButton.isFavorite = isTrue
        }
    }

    @JvmStatic
    @BindingAdapter("setProgress")
    fun setProgress(circularSeekBar: CircularSeekBar, mProgress: String?) {
        if (mProgress != null && mProgress != "") {
            circularSeekBar.progress = mProgress.toInt().toFloat()
        }
    }

    @JvmStatic
    @BindingAdapter("isVisiblePlayIcon")
    fun loadisVisiblePlayIcon(
        imageView: ImageView,
        isTrue: Boolean?
    ) {
        if (isTrue != null) {
            if (isTrue) {
                imageView.visibility = View.VISIBLE
            } else {
                imageView.visibility = View.GONE
            }
        }
    }

    @JvmStatic
    @BindingAdapter("isLiked")
    fun setLiked(
        imageView: ImageView,
        isTrue: Boolean?
    ) {
        if (isTrue != null) {
            if (isTrue) {
                imageView.setImageResource(R.drawable.ic_favorite_black_24dp)
            } else {
                imageView.setImageResource(R.drawable.ic_favorite_border_grey)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("isVisibleText")
    fun loadisVisibleText(textView: TextView?, isTrue: Boolean?) {
        if (textView != null && isTrue != null) {
            if (isTrue) {
                textView.visibility = View.VISIBLE
            } else {
                textView.visibility = View.GONE
            }
        } else {
            if (textView != null) {
                textView.visibility = View.GONE
            }
        }
    }

    @JvmStatic
    @BindingAdapter("isInternetAvailable")
    fun loadIsInternetAvailable(
        linearLayout: LinearLayout,
        isTrue: Boolean?
    ) {
        if (isTrue != null) {
            if (isTrue) {
                linearLayout.visibility = View.GONE
            } else {
                linearLayout.visibility = View.VISIBLE
            }
        }
    }

    @JvmStatic
    @BindingAdapter("linearVisibility")
    fun loadlinearVisiblity(
        linearLayout: LinearLayout,
        isTrue: Boolean?
    ) {
        if (isTrue != null) {
            if (isTrue) {
                linearLayout.visibility = View.VISIBLE
            } else {
                linearLayout.visibility = View.GONE
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:visibility")
    fun setVisibility(view: View, value: Boolean?) {
        if (value != null) {
            view.visibility = if (value) View.VISIBLE else View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("android:textVisibility")
    fun setTextVisibility(view: TextView, value: String?) {
        if (value != null && value !== "") {
            if (value == "true") {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }
    }

    @JvmStatic
    @BindingAdapter("bindProfileAnthemtext")
    fun setBindProfileAnthemText(
        view: TextView,
        value: String?
    ) {
        if (value != null && value != "") {
            view.text = value
        } else {
            view.setText(R.string.no_tracks_added)
        }
    }

    @JvmStatic
    @BindingAdapter("duration")
    fun setDuration(
        view: TextView,
        value: String?
    ) {
        if (value != null && value != "") {
            var mDuration: String
            val seconds: Int = value.toInt() % 60
            var minutes: Int = value.toInt() / 60
            if (minutes >= 60) {
                val hours = minutes / 60
                minutes %= 60
                if (hours >= 24) {
                    val days = hours / 24
                    mDuration = String.format(
                        "%d days %02d:%02d:%02d",
                        days,
                        hours % 24,
                        minutes,
                        seconds
                    )
                } else {
                    mDuration = String.format("%02d:%02d:%02d", hours, minutes, seconds)
                }
            } else {
                mDuration = String.format("00:%02d:%02d", minutes, seconds)
            }

            view.text = mDuration
        }
    }

    @JvmStatic
    @BindingAdapter("textSplit")
    fun setTextSplit(
        view: AutoCompleteTextView,
        value: String?
    ) {
        if (value != null && value != "") {
            try {
                val locationArray =
                    value.split(",".toRegex()).toTypedArray()
                view.setText(locationArray[0])
                view.dismissDropDown()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("bindLikeTextView")
    fun setLikeTextView(view: TextView, value: String?) {
        if (value != null && value != "") {
            view.text = "$value Likes"

            if (value.toInt() > 0) {
                view.setTextColor(Color.parseColor("#1e90ff"))
            } else {
                view.setTextColor(Color.parseColor("#747d8c"))
            }
        }
    }

    @JvmStatic
    @BindingAdapter("bindCommentTextView")
    fun setCommentTextView(view: TextView, value: String?) {
        if (value != null && value != "") {
            view.text = "$value Comments"
        }
    }

    private fun removeUrl(commentstr: String): String {
        var commentstr = commentstr
        val urlPattern =
            "((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)"
        val p =
            Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE)
        val m = p.matcher(commentstr)
        var i = 0
        while (m.find()) {
            commentstr = commentstr.replace(m.group(i).toRegex(), "").trim { it <= ' ' }
            i++
        }
        return commentstr
    }

    @BindingAdapter("android:linktext")
    fun setlinkText(view: SocialTextView, value: String?) {
        if (value != null && value != "") {
            val displayText = removeUrl(value)
            if (displayText.length > 0) {
                view.text = displayText
            } else {
                view.visibility = View.GONE
            }
        }
    }

    private fun extractLinks(text: String): Array<String> {
        val links: MutableList<String> =
            ArrayList()
        val m = Patterns.WEB_URL.matcher(text)
        while (m.find()) {
            val url = m.group()
            Log.d("", "URL extracted: $url")
            links.add(url)
        }
        return links.toTypedArray()
    }

    @JvmStatic
    @BindingAdapter("textMarquee")
    fun settextMarquee(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length >= 25) {
                value.substring(0, 25) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    @JvmStatic
    @BindingAdapter("textDescription")
    fun settextDescription(view: TextView, value: String?) {
        if (value != null && value != "") {
            view.text = value
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("textMarquee15")
    fun settextMarquee15(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length >= 15) {
                value.substring(0, 15) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    @JvmStatic
    @BindingAdapter("textViewAllComments")
    fun setTextViewAllComments(view: TextView, value: String?) {
        if (value != null && value.toInt() != 0) {
            view.visibility = View.VISIBLE
            val mTitle: String
            mTitle = if (value.toInt() >= 10) {
                "View All " + value + " Comments"
            } else {
                "View All Comments"
            }
            view.text = mTitle
        } else {
            view.visibility = View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("textMarquee10")
    fun settextMarquee10(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length > 10) {
                value.substring(0, 10) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    @JvmStatic
    @BindingAdapter("textMarquee150")
    fun settextMarquee150(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length >= 150) {
                value.substring(0, 150) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    @JvmStatic
    @BindingAdapter("textMarquee20")
    fun settextMarquee20(view: TextView, value: String?) {
        if (value != null && value != "") {
            val mTitle: String
            mTitle = if (value.length >= 20) {
                value.substring(0, 20) + "..."
            } else {
                value
            }
            view.text = mTitle
        }
    }

    @JvmStatic
    @BindingAdapter("android:follow")
    fun loadFollow(textInputEditText: TextView, s1: String?) {
        if (s1 != null && s1 != "") {
            var s = ""
            when (s1) {
                "following" -> s = "Following"
                "none" -> s = "Follow"
                "request pending" -> s = "Requested"
            }
            textInputEditText.text = s
        }
    }

    @BindingAdapter("android:trackText")
    fun loadTrackText(textInputEditText: TextView, s1: String?) {
        var s: String? = "NO TRACKS ADDED"
        if (s1 != null && s1 != "") {
            s = s1
        }
        textInputEditText.text = s
    }

    @JvmStatic
    @BindingAdapter("android:hashTagName")
    fun loadhashTagName(textView: TextView, mTagName: String?) {
        if (mTagName != null && mTagName != "") {
            textView.text = "#$mTagName"
        }
    }

    @JvmStatic
    @BindingAdapter("setfontFamily")
    fun loadSetfontFamily(textView: TextView, isLIke: Boolean?) {
        if (isLIke != null) {
            if (isLIke) {
                if (textView.context != null && textView.context.assets != null) {
                    val face = Typeface.createFromAsset(
                        textView.context.assets,
                        "fonts/poppinsmedium.otf"
                    )
                    textView.typeface = face
                    textView.setTextColor(Color.parseColor("#1e90ff"))
                }
            } else {
                if (textView.context != null && textView.context.assets != null) {
                    val face = Typeface.createFromAsset(
                        textView.context.assets,
                        "fonts/poppinssemibold.otf"
                    )
                    textView.typeface = face
                    textView.setTextColor(Color.parseColor("#747d8c"))
                }
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:followDrawable3px")
    fun loadFollowDrawable3px(textView: TextView, s: String?) {
        try {
            val mMode = getBooleanPreference(
                application,
                "night_mode",
                false
            )
            if (s != null && s != "") {
                var follow = 0
                var color = 0
                when (s) {
                    "following" -> if (!mMode) {
                        follow = R.drawable.rounded_button_following
                        color = Color.parseColor("#FFFFFF")
                    } else {
                        follow = R.drawable.rounded_button_following
                        color = Color.parseColor("#ced6e0")
                    }
                    "none" -> if (!mMode) {
                        follow = R.drawable.rounded_button_follow
                        color = Color.parseColor("#2f3542")
                    } else {
                        follow = R.drawable.rounded_button_follow_dark
                        color = Color.parseColor("#ced6e0")
                    }
                    "request pending" -> if (!mMode) {
                        follow = R.drawable.rounded_button_requested
                        color = Color.parseColor("#FFFFFF")
                    } else {
                        follow =
                            R.drawable.rounded_button_requested_dark
                        color = Color.parseColor("#ced6e0")
                    }
                }
                textView.setBackgroundResource(follow)
                textView.isAllCaps = true
                textView.setTextColor(color)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("android:followDrawable")
    fun loadFollowDrawable(textView: TextView, s: String?) {
        try {
            val mMode = getBooleanPreference(
                application,
                "night_mode",
                false
            )
            if (s != null && s != "") {
                var follow = 0
                var color = 0
                when (s) {
                    "following" -> if (!mMode) {
                        follow = R.drawable.rounded_button_following
                        color = Color.parseColor("#FFFFFF")
                    } else {
                        follow = R.drawable.rounded_button_following
                        color = Color.parseColor("#ced6e0")
                    }
                    "none" -> if (!mMode) {
                        follow = R.drawable.rounded_button_follow
                        color = Color.parseColor("#2f3542")
                    } else {
                        follow = R.drawable.rounded_button_follow_dark
                        color = Color.parseColor("#ced6e0")
                    }
                    "request pending" -> if (!mMode) {
                        follow = R.drawable.rounded_button_requested
                        color = Color.parseColor("#FFFFFF")
                    } else {
                        follow =
                            R.drawable.rounded_button_requested_dark
                        color = Color.parseColor("#ced6e0")
                    }
                }
                textView.setBackgroundResource(follow)
                textView.isAllCaps = true
                textView.setTextColor(color)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("android:followArtistDrawable")
    fun loadFollowArtistDrawable(imageView: ImageView, s: String?) {
        try {
            if (s != null && s != "") {
                var follow = 0
                when (s) {
                    "following" -> follow = R.drawable.ic_artist_follow_active
                    "none" -> follow = R.drawable.ic_artist_follow_inactive
                }
                imageView.setImageResource(follow)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("android:blockDrawable")
    fun loadBlockDrawable(textView: TextView, s: String?) {
        try {
            val mMode = getBooleanPreference(
                application,
                "night_mode",
                false
            )
            if (s != null && s != "") {
                var follow = 0
                var color = 0
                if (!mMode) {
                    follow = R.drawable.rounded_button_blocked_user
                    color = Color.parseColor("#FFFFFF")
                } else {
                    follow = R.drawable.rounded_button_following_dark
                    color = Color.parseColor("#ced6e0")
                }
                textView.setBackgroundResource(follow)
                textView.isAllCaps = true
                textView.setTextColor(color)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("android:submitDrawable")
    fun loadSubmitDrawable(textView: TextView, s: String?) {
        try {
            val mMode = getBooleanPreference(
                application,
                "night_mode",
                false
            )
            var follow = 0
            var color = 0
            if (!mMode) {
                follow = R.drawable.rounded_button_following
                color = Color.parseColor("#FFFFFF")
            } else {
                follow = R.drawable.rounded_button_following_dark
                color = Color.parseColor("#ced6e0")
            }
            textView.setBackgroundResource(follow)
            textView.isAllCaps = true
            textView.setTextColor(color)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @BindingAdapter(value = ["reelBoardFollow", "isOwn"])
    fun reelBoardFollow(
        textView: TextView,
        reelBoardFollow: String?,
        isOwn: String
    ) {
        if (reelBoardFollow != null && reelBoardFollow != "") {
            var follow = ""
            var color = 0
            when (reelBoardFollow) {
                "following" -> {
                    follow = "FOLLOWING"
                    color = Color.parseColor("#30ccb4")
                }
                "none" -> {
                    follow = "FOLLOW"
                    color = Color.parseColor("#1e90ff")
                }
                "request pending" -> {
                    follow = "REQUEST PENDING"
                    color = Color.parseColor("#FFFFFF")
                }
            }
            if (isOwn != "true") {
                textView.text = follow
                textView.setTextColor(color)
            }
        }
    }

    @BindingAdapter("android:profileFollowDrawable")
    fun loadProfileFollowDrawable(
        textView: TextView,
        s: String?
    ) {
        if (s != null && s != "") {
            var follow = 0
            var color = 0
            when (s) {
                "following" -> {
                    follow = R.drawable.rounded_button_following
                    color = Color.parseColor("#FFFFFF")
                }
                "none" -> {
                    follow = R.drawable.rounded_button_following
                    color = Color.parseColor("#FFFFFF")
                }
                "request pending" -> {
                    follow = R.drawable.rounded_button_requested
                    color = Color.parseColor("#FFFFFF")
                }
            }
            textView.setBackgroundResource(follow)
            textView.setTextColor(color)
        }
    }

    @BindingAdapter("android:followTextColor")
    fun loadfollowTextColor(textView: TextView, s: String?) {
        if (s != null && s != "") {
            var color = 0
            when (s) {
                "following" -> color = Color.parseColor("#FFFFFF")
                "none" -> color = Color.parseColor("#1e90ff")
                "request pending" -> color = Color.parseColor("#FFFFFF")
            }
            textView.setTextColor(color)
        }
    }

    @JvmStatic
    @BindingAdapter("android:followTagDrawable")
    fun loadFollowTagDrawable(textView: TextView, s: String?) {
        if (s != null && s != "") {
            var follow = 0
            var color = 0
            var text = ""
            if (s == "true") {
                follow = R.drawable.rounded_button_following
                color = Color.parseColor("#FFFFFF")
                text = "Following"
            } else if (s == "false") {
                follow = R.drawable.rounded_button_follow
                color = Color.parseColor("#1e90ff")
                text = "Follow"
            }
            textView.setBackgroundResource(follow)
            textView.setTextColor(color)
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("android:dateConvert")
    fun loadDateConvert(textView: TextView, s: String?) {
        if (s != null && s != "") {
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
//            df.setTimeZone(TimeZone.getTimeZone("UTC"));
//            Date date = null;
//            try {
//                date = df.parse(s);
//                df.setTimeZone(TimeZone.getDefault());
//                df.format(date);
//                df = new SimpleDateFormat("dd MMM yyyy · hh:mm aaa", Locale.ENGLISH);
//                String formattedDate = df.format(date);
//
//                textView.setText(formattedDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//
//            }
            @SuppressLint("SimpleDateFormat") val sdf =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            sdf.timeZone = TimeZone.getTimeZone("GMT")
            val time: Long
            try {
                time = sdf.parse(s).time
                val now = System.currentTimeMillis()
                val ago =
                    DateUtils.getRelativeTimeSpanString(
                        time,
                        now,
                        DateUtils.MINUTE_IN_MILLIS
                    )
                if (ago.toString() == "0 minutes ago") {
                    textView.text = "Just now"
                } else {
                    textView.text = ago.toString()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:dateConvertTimeStamp")
    fun loadDateConvertTimeStamp(
        textView: TextView,
        s: String?
    ) {
        if (s != null && s != "") {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//            val formatter = SimpleDateFormat("hh:mm aa")
            val dateString = formatter.format(Date(s.toLong()))
            Log.d("AmR_", "ValDate_" + dateString.toString())
            try {
                textView.text = dateString
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    @JvmStatic
    @BindingAdapter("android:dateConvertTime")
    fun loadDateConvertTime(textView: TextView, s: String?) {
        try {
            if (s != null && s != "") {
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val output = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val dateString = sdf.format(Date(s.toLong()))
                val d = sdf.parse(dateString)
                Log.d("AmR_", "ValDate_" + s.toString())
                val formattedTime = output.format(d)
                textView.text = formattedTime
            }
        } catch (e: Exception) {
            Log.d("AmR_", "ValDate_" + e.toString())
            e.printStackTrace()
        }
    }

    @JvmStatic
    @BindingAdapter("android:dateConvertTimeStampConversation")
    fun loadDateConvertTimeStampConversation(
        textView: TextView,
        s: String?
    ) {
        if (s != null && s != "") {
            val formatter = SimpleDateFormat("hh:mm aa")
            val dateString = formatter.format(Date(s.toLong()))
            try {
                textView.text = " · " + dateString
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:dateConvertDateOnly")
    fun loadDateConvertDateOnly(textView: TextView, s: String?) {
        if (s != null && s != "") {
            var df =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            df.timeZone = TimeZone.getTimeZone("UTC")
            var date: Date? = null
            try {
                date = df.parse(s)
                df.timeZone = TimeZone.getDefault()
                df.format(date)
                df = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
                val formattedDate = df.format(date)
                textView.text = formattedDate
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    @SuppressLint("SetTextI18n")
    @BindingAdapter("android:textTime")
    fun loadTextTime(textView: TextView, totalSecs: String?) {
        if (totalSecs != null && totalSecs != "") {
            val pTime = totalSecs.toInt()
            @SuppressLint("DefaultLocale") val timeString =
                String.format("%02d:%02d", pTime / 60, pTime % 60)
            textView.text = "$timeString min"
        }
    }

    @JvmStatic
    @BindingAdapter("android:bookmarked")
    fun loadBookmarked(imageView: ImageView, s: String?) {
        var image = 0
        if (s != null && s != "") {
            if (s == "true") {
                image = R.drawable.ic_bookmarked_new
            } else if (s == "false") {
                image = R.drawable.ic_bookmark_new
            }
            imageView.setImageResource(image)
        }
    }

    private fun getImageUrlFromWidth(imageUrl: String?, width: Int): String {
        var width = width
        if (width == 0) {
            width = 500
        }
        var url = ""
        if (imageUrl != null && imageUrl !== "") {
            url = "$imageUrl?imwidth=$width&impolicy=resize"
        }
        return url
    }
}