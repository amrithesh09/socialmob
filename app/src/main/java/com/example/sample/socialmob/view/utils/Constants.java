package com.example.sample.socialmob.view.utils;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.example.sample.socialmob.view.utils.Constants.ViewType.LEFT;
import static com.example.sample.socialmob.view.utils.Constants.ViewType.RIGHT;


public class Constants {

    @IntDef({RIGHT, LEFT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewType {
        int RIGHT = 200;
        int LEFT = 300;
    }
}
