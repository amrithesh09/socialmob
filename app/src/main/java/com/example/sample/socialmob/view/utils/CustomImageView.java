package com.example.sample.socialmob.view.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;

public class CustomImageView extends AppCompatImageView {
    private Context context;
    private GestureListener mGestureListener;
    private GestureDetector mGestureDetector;

    public CustomImageView(Context context) {
        super(context);
        sharedConstructing(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedConstructing(context);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        sharedConstructing(context);
    }

    private void sharedConstructing(Context context) {
        super.setClickable(true);
        this.context = context;
        mGestureListener = new GestureListener();
        Log.e("Adding", "Listener:::");
        mGestureDetector = new GestureDetector(context, mGestureListener, null, true);
        setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mGestureDetector.onTouchEvent(event);
                //..my other code logic
                invalidate();
                return true; // indicate event was handled
            }

        });
    }

    public class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            // TODO DoubleTap Comparison
            Log.e("onDoubleTap", "onDoubleTap");
            return true;
        }
    }
}