package com.example.sample.socialmob.view.utils.DeviceInfo.device;

import com.example.sample.socialmob.view.utils.DeviceInfo.device.model.Device;
import com.example.sample.socialmob.view.utils.DeviceInfo.device.model.Memory;

public class DeviceDifferentData {
    public Device deviceData;
    public Memory memoryData;
}