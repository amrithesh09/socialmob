/*
 * Created by Bincy Baby on 23/7/18 2:11 PM
 * Copyright (c) 2018 Padath Infotainment
 */

package com.example.sample.socialmob.view.utils.DeviceInfo.device;


import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import androidx.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Locale;

public class DeviceInfo {


    private Context context;

    public DeviceInfo(Context context) {
        this.context = context;
    }

    public final String getReleaseBuildVersion() {
        return Build.VERSION.RELEASE;
    }

    public final String getBuildVersionCodeName() {
        return Build.VERSION.CODENAME;
    }

    public final String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public final String getModel() {
        return Build.MODEL;
    }


    public String getProduct() {
        return Build.PRODUCT;
    }

    public final String getFingerprint() {
        return Build.FINGERPRINT;
    }

    public final String getHardware() {
        return Build.HARDWARE;
    }


    @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public final String getRadioVer() {
        return Build.getRadioVersion();
    }


    public final String getDevice() {
        return Build.DEVICE;
    }

    public final String getBoard() {
        return Build.BOARD;
    }

    public final String getDisplayVersion() {
        return Build.DISPLAY;
    }

    public final String getBuildBrand() {
        return Build.BRAND;
    }

    public final String getBuildHost() {
        return Build.HOST;
    }

    public final long getBuildTime() {
        return Build.TIME;
    }

    public final String getBuildUser() {
        return Build.USER;
    }

    public final String getSerial() {
        return Build.SERIAL;
    }

    public final String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public final String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public final int getSdkVersion() {
        return Build.VERSION.SDK_INT;
    }

    public String getScreenDensity() {
        int density = context.getResources().getDisplayMetrics().densityDpi;
        String scrType;
        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                scrType = "ldpi";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                scrType = "mdpi";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                scrType = "hdpi";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                scrType = "xhdpi";
                break;
            default:
                scrType = "other";
                break;
        }
        return scrType;
    }

    public int getScreenHeight() {
        int height;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = null;
        if (wm != null) {
            display = wm.getDefaultDisplay();
        }
        Point size = new Point();
        if (display != null) {
            display.getSize(size);
        }
        height = size.y;
        return height;
    }

    public int getScreenWidth() {
        int width;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = null;
        if (wm != null) {
            display = wm.getDefaultDisplay();
        }
        Point size = new Point();
        if (display != null) {
            display.getSize(size);
        }
        width = size.x;
        return width;
    }


    public boolean hasExternalSDCard() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }


    public final long getTotalRAM() {
        long totalMemory;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
            if (activityManager != null) {
                activityManager.getMemoryInfo(mi);
            }
            return mi.totalMem;
        }
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/meminfo", "r");
            String load = reader.readLine().replaceAll("\\D+", "");
            totalMemory = (long) Integer.parseInt(load);
            reader.close();
            return totalMemory;
        } catch (IOException e) {
            return 0L;
        }
    }

    public final long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize, availableBlocks;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
        } else {
            blockSize = stat.getBlockSize();
            availableBlocks = stat.getAvailableBlocks();
        }
        return availableBlocks * blockSize;
    }

    public final long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize;
        long totalBlocks;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            blockSize = stat.getBlockSizeLong();
            totalBlocks = stat.getBlockCountLong();
        } else {
            blockSize = stat.getBlockSize();
            totalBlocks = stat.getBlockCount();
        }
        return totalBlocks * blockSize;
    }


    public final long getAvailableExternalMemorySize() {
        if (hasExternalSDCard()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize;
            long availableBlocks;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                blockSize = stat.getBlockSizeLong();
                availableBlocks = stat.getAvailableBlocksLong();
            } else {
                blockSize = stat.getBlockSize();
                availableBlocks = stat.getAvailableBlocks();
            }
            return availableBlocks * blockSize;
        }
        return 0;
    }


    public final long getTotalExternalMemorySize() {
        if (hasExternalSDCard()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize;
            long totalBlocks;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                blockSize = stat.getBlockSizeLong();
                totalBlocks = stat.getBlockCountLong();
            } else {
                blockSize = stat.getBlockSize();
                totalBlocks = stat.getBlockCount();
            }
            return totalBlocks * blockSize;
        }
        return 0;
    }
}
