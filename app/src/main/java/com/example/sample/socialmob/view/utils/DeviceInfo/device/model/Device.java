/*
 * Created by Bincy Baby on 23/7/18 2:11 PM
 * Copyright (c) 2018 Padath Infotainment
 */

package com.example.sample.socialmob.view.utils.DeviceInfo.device.model;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.sample.socialmob.view.utils.DeviceInfo.device.DeviceInfo;

public class Device {

    private String releaseBuildVersion;
    private String buildVersionCodeName;
    private String manufacturer;
    private String model;
    private String product;
    private String fingerprint;
    private String hardware;
    private String radioVersion;
    private String device;
    private String board;
    private String displayVersion;
    private String buildBrand;
    private String buildHost;
    private long buildTime;
    private String buildUser;
    private String serial;
    private String osVersion;
    private String language;
    private int sdkVersion;
    private String screenDensity;
    private int screenHeight;
    private int screenWidth;
    private Memory memory;

    @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public Device(Context context) {
        DeviceInfo deviceInfo = new DeviceInfo(context);
        this.releaseBuildVersion = deviceInfo.getReleaseBuildVersion();
        this.buildVersionCodeName = deviceInfo.getBuildVersionCodeName();
        this.manufacturer = deviceInfo.getManufacturer();
        this.model = deviceInfo.getModel();
        this.product = deviceInfo.getProduct();
        this.fingerprint = deviceInfo.getFingerprint();
        this.hardware = deviceInfo.getHardware();
        this.radioVersion = deviceInfo.getRadioVer();
        this.device = deviceInfo.getDevice();
        this.board = deviceInfo.getBoard();
        this.displayVersion = deviceInfo.getDisplayVersion();
        this.buildBrand = deviceInfo.getBuildBrand();
        this.buildHost = deviceInfo.getBuildHost();
        this.buildTime = deviceInfo.getBuildTime();
        this.buildUser = deviceInfo.getBuildUser();
        this.serial = deviceInfo.getSerial();
        this.osVersion = deviceInfo.getOSVersion();
        this.language = deviceInfo.getLanguage();
        this.sdkVersion = deviceInfo.getSdkVersion();
        this.screenDensity = deviceInfo.getScreenDensity();
        this.screenHeight = deviceInfo.getScreenHeight();
        this.screenWidth = deviceInfo.getScreenWidth();

    }
}
