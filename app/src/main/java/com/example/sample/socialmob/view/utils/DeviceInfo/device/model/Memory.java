/*
 * Created by Bincy Baby on 23/7/18 2:11 PM
 * Copyright (c) 2018 Padath Infotainment
 */

package com.example.sample.socialmob.view.utils.DeviceInfo.device.model;


import android.content.Context;
import com.example.sample.socialmob.view.utils.DeviceInfo.device.DeviceInfo;

/**
 * The type Memory.
 */
public class Memory {

    private boolean hasExternalSDCard;
    private long totalRAM;
    private long availableInternalMemorySize;
    private long totalInternalMemorySize;
    private long availableExternalMemorySize;
    private long totalExternalMemorySize;

    /**
     * Instantiates a new Memory.
     *
     * @param context the context
     */
    public Memory(Context context) {
        DeviceInfo deviceInfo = new DeviceInfo(context);
        this.hasExternalSDCard = deviceInfo.hasExternalSDCard();
        this.totalRAM = deviceInfo.getTotalRAM();
        this.availableInternalMemorySize = deviceInfo.getAvailableInternalMemorySize();
        this.totalInternalMemorySize = deviceInfo.getTotalInternalMemorySize();
        this.availableExternalMemorySize = deviceInfo.getAvailableExternalMemorySize();
        this.totalExternalMemorySize = deviceInfo.getTotalExternalMemorySize();
    }
}
