package com.example.sample.socialmob.view.utils;

public interface ISize {

    int width();

    int height();

}
