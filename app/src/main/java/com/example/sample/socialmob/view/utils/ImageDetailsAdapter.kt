package com.example.sample.socialmob.view.utils

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeedActionPostMedium
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.github.chrisbanes.photoview.PhotoView

class ImageDetailsAdapter(
    private var mContext: Context,
    private val mMedia: List<PersonalFeedResponseModelPayloadFeedActionPostMedium>,
    private val requestManager: RequestManager
) : PagerAdapter() {

    override fun getCount(): Int {
        return mMedia.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): View {
        val photoView = PhotoView(container.context)
        requestManager
            .load(
                RemoteConstant.getImageUrlFromWidth(
                    mMedia[position].sourcePath,
                    RemoteConstant.getWidth(mContext) / 2
                )
            )
            .thumbnail(0.1f)
            .apply(RequestOptions().fitCenter())
            .into(photoView)


        container.addView(
            photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        return photoView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

}
