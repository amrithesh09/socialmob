package com.example.sample.socialmob.view.utils

interface ItemClick {
    fun onItemClick(mString: String)
}