package com.example.sample.socialmob.view.utils

import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import cat.ereza.customactivityoncrash.config.CaocConfig
import com.androidisland.vita.startVita
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.utils.music.manager.PlayListManagerRadio
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import dagger.hilt.android.HiltAndroidApp
import im.ene.toro.exoplayer.Config
import im.ene.toro.exoplayer.ExoCreator
import im.ene.toro.exoplayer.MediaSourceBuilder
import im.ene.toro.exoplayer.ToroExo
import java.io.File

@HiltAndroidApp
class MyApplication : MultiDexApplication(), LifecycleObserver {


    override fun onCreate() {
        super.onCreate()
        InternetUtil.init(this)

        application = this
        playlistManager = PlaylistManager(this)
        playlistManagerRadio = PlayListManagerRadio(this)


        when (RemoteConstant.environment) {
            RemoteConstant.Environment.DEVELOPMENT -> {
                RemoteConstant.mBaseUrl = "http://smqastaging.web.socialmob.me"
                RemoteConstant.mBaseUrlChat =  "http://159.89.167.142:3004" // dev chat domain
                RemoteConstant.mBaseUrlWebSocket = "ws://159.89.167.142:8080/ws?token=" // dev socket domain
            }
            else -> {
                RemoteConstant.mBaseUrl = "https://web.socialmob.me"
                RemoteConstant.mBaseUrlChat = "https://imapi.socialmob.me" // production chat domain
                RemoteConstant.mBaseUrlWebSocket = "ws://imws.socialmob.me/ws?token=" // production socket domain
            }
        }



        startVita()
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return
//        }
//
//        LeakCanary.install(this)
        CaocConfig.Builder.create()
            .restartActivity(HomeActivity::class.java) //default: null (your app's launch activity)
            .errorActivity(YourCustomErrorActivity::class.java) //default: null (default error activity)
            .apply()

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.deleteNotificationChannel("fcm_default_channel")
        }

        val cache = SimpleCache(
            File(filesDir.path + "/toro_cache"),
            LeastRecentlyUsedCacheEvictor(cacheFile)
        )
        config = Config.Builder()
            .setMediaSourceBuilder(MediaSourceBuilder.LOOPING)
            .setCache(cache)
            .build()
        exoCreator = ToroExo.with(this).getCreator(config)

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

    }

    override fun onTerminate() {
        super.onTerminate()

        application = null
        playlistManager = null
        playlistManagerRadio = null


    }

    companion object {

        val mFileUploadedList: MutableList<String>? = ArrayList()
        var application: MyApplication? = null
            private set
        var playlistManager: PlaylistManager? = null
            private set
        var playlistManagerRadio: PlayListManagerRadio? = null
            private set

        var cacheFile = 2 * 1024 * 1024.toLong() // size of each cache file.
        var config: Config? = null
        var exoCreator: ExoCreator? = null
        var isForeground = false
        var simpleCache: SimpleCache? = null

    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        if (level >= TRIM_MEMORY_BACKGROUND) ToroExo.with(this).cleanUp()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        isForeground = false
        Log.d("Awww", "App in background")
//        EventBus.getDefault().post(SocketDisconnectionEvent())
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        isForeground = true
        Log.d("Yeeey", "App in foreground")

    }

}