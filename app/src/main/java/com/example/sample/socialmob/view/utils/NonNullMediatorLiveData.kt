package com.example.sample.socialmob.view.utils

import androidx.lifecycle.MediatorLiveData

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()
