/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.sample.socialmob.view.utils

import android.annotation.TargetApi
import android.app.*
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Color
import android.os.Build
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.HomeActivity

/**
 * Helper class to manage notification channels, and create notifications.
 */
internal class NotificationHelper @TargetApi(Build.VERSION_CODES.O) constructor(context: Context?) :
    ContextWrapper(context) {
    private var mNotificationManager: NotificationManager? = null

    /**
     * Get a follow/un-follow notification
     *
     *
     *
     * Provide the builder rather than the notification it's self as useful for making
     * notification changes.
     *
     * @param title the title of the notification
     * @param body  the body text for the notification
     * @return A Notification.Builder configured with the selected channel and details
     */
    @TargetApi(Build.VERSION_CODES.O)
    fun getNotificationFollower(
        title: String?,
        body: String?
    ): Notification.Builder {
        return Notification.Builder(
            applicationContext,
            FOLLOWERS_CHANNEL
        )
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(smallIcon)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
    }

    /**
     * Get a direct message notification
     *
     *
     *
     * Provide the builder rather than the notification it's self as useful for making
     * notification changes.
     *
     * @param title Title for notification.
     * @param body  Message for notification.
     * @return A Notification.Builder configured with the selected channel and details
     */

    var convId: String = ""
    var mFromProfileId: String = ""
    var mUserName: String = ""
    var pimage: String = ""

    @TargetApi(Build.VERSION_CODES.O)
    fun getNotificationDM(
        title: String?, body: String?, convId: String, mFromProfileId: String,
        mUserName: String, pimage: String
    ): Notification.Builder {
//        this.convId = ""
//        this.mFromProfileId = ""
//        this.mUserName = ""
//        this.pimage = ""
        this.convId = convId
        this.mFromProfileId = mFromProfileId
        this.mUserName = mUserName
        this.pimage = pimage
        return Notification.Builder(
            applicationContext,
            DIRECT_MESSAGES
        )
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(smallIcon)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
    }// The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    // Adds the back stack for the Intent (but not the Intent itself)
    // Adds the Intent that starts the Activity to the top of the stack

    /**
     * Create a PendingIntent for opening up the MainActivity when the notification is pressed
     *
     * @return A PendingIntent that opens the MainActivity
     */
    private val pendingIntent: PendingIntent
        private get() {
//            val openMainIntent = Intent(this, WebSocketChatActivity::class.java)
            val openMainIntent = Intent(this, HomeActivity::class.java)
            openMainIntent.putExtra("mPos", "3")
            openMainIntent.putExtra("mPosInner", "1")
            openMainIntent.putExtra("convId", convId)
            openMainIntent.putExtra("mProfileId", mFromProfileId)
            openMainIntent.putExtra("mUserName", mUserName)
            openMainIntent.putExtra("mUserImage", pimage)
            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            // This ensures that navigating backward from the Activity leads out of
            // your application to the Home screen.
            val stackBuilder = TaskStackBuilder.create(this)
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(HomeActivity::class.java)
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(openMainIntent)
            return stackBuilder.getPendingIntent(System.currentTimeMillis().toInt(), PendingIntent.FLAG_ONE_SHOT)
        }

    /**
     * Send a notification.
     *
     * @param id           The ID of the notification
     * @param notification The notification object
     */
    fun notify(id: Int, notification: Notification.Builder) {
        notificationManager!!.notify(id, notification.build())
    }

    /**
     * Get the small icon for this app
     *
     * @return The small icon resource id
     */
    private val smallIcon: Int
        private get() = R.mipmap.ic_launcher_foreground

    /**
     * Get the notification mNotificationManager.
     *
     *
     *
     * Utility method as this helper works with it a lot.
     *
     * @return The system service NotificationManager
     */
    private val notificationManager: NotificationManager?
        private get() {
            if (mNotificationManager == null) {
                mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            }
            return mNotificationManager
        }

    companion object {
        const val FOLLOWERS_CHANNEL = "follower"
        const val DIRECT_MESSAGES = "direct_messages"
    }

    /**
     * Registers notification channels, which can be used later by individual notifications.
     *
     * @param context The application context
     */
    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the channel object with the unique ID FOLLOWERS_CHANNEL
            val followersChannel = NotificationChannel(
                FOLLOWERS_CHANNEL,
                getString(R.string.default_notification_channel_id_chat),
                NotificationManager.IMPORTANCE_DEFAULT
            )

            // Configure the channel's initial settings
            followersChannel.lightColor = Color.GREEN
            followersChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                500,
                200,
                500
            )
            followersChannel.setShowBadge(false)

            // Submit the notification channel object to the notification manager
            notificationManager!!.createNotificationChannel(followersChannel)

            // Create the channel object with the unique ID DIRECT_MESSAGES
            val directMessagesChannel = NotificationChannel(
                DIRECT_MESSAGES,
                getString(R.string.default_notification_channel_id_chat),
                NotificationManager.IMPORTANCE_HIGH
            )

            // Configure the channel's initial settings
            directMessagesChannel.lightColor = Color.BLUE
            directMessagesChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                500,
                200,
                500
            )
            directMessagesChannel.setShowBadge(true)

            // Submit the notification channel object to the notification manager
            notificationManager!!.createNotificationChannel(directMessagesChannel)
        }
    }
}