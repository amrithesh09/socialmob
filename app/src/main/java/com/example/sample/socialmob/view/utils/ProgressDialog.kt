package com.example.sample.socialmob.view.utils

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R

class ProgressDialog(context: Context?) : Dialog(context!!) {
    var gifProgress: GifView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.progress_dialog)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        gifProgress = findViewById(R.id.gif_image_view)
        gifProgress?.play()
        gifProgress?.gifResource = R.drawable.play_list_gif

    }

}