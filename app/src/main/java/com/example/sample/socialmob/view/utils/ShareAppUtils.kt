package com.example.sample.socialmob.view.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.Hashids
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks

class ShareAppUtils {

    companion object {


        fun shareVideo(videoId: String, mContext: Context?) {

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

            val sAux = "https://share.socialmob.me/share/track-video/$videoId"
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse(
                        "https://socialmob.page.link/?link=" +
                                sAux
                    )
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "This music video is totally awesome! You should hit play now! \n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext!!.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                    it.printStackTrace()
                }


        }

        fun shareTrack(JukeBoxTrackId: String, mContext: Context?) {

//            /share/track/5db96416d4572fdfe311662b?source=sm_2

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

//            val sAux = "https://socialmob.me/share/track/$JukeBoxTrackId?source=sm_2"
            val sAux = "https://share.socialmob.me/share/track/$JukeBoxTrackId?source=sm_2"
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse(
                        "https://socialmob.page.link/?link=" +
                                sAux
                    )
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "This track on SocialMob is mind-blowing! Give it a listen, I'm sure you'll think the same!.\n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext!!.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                    it.printStackTrace()
                }


        }

        fun shareArticle(mId: Long, mContext: Context?) {

            val hashids = Hashids("hgdhfdf44hfg8h8fg8h8fg8hf8h8f8f8hfh8f")
            val encriptedString = hashids.encode(mId)
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
//            val sAux = "https://socialmob.me/share/article/$encriptedString"
            val sAux = "https://share.socialmob.me/share/article/$encriptedString"

            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse("https://socialmob.page.link/?link=$sAux")
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "Download Socialmob to expose yourself to the world via interesting articles and magazine topics. Chance upon a different perspective..\n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext!!.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                }


        }

        fun sharePodcastEpisode(EpisodeId: String, mContext: Context?, mPodcastName: String) {


            if (EpisodeId != null && EpisodeId != "") {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

//                val sAux = "https://socialmob.me/share/podcast/v2/$mPodcastName/$EpisodeId"
                val uri =
                    Uri.encode("https://share.socialmob.me/share/podcast/v2/$mPodcastName/$EpisodeId")

                FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLongLink(
                        Uri.parse("https://socialmob.page.link/?link=$uri")
                    )
                    .buildShortDynamicLink()
                    .addOnSuccessListener { result ->
                        // Short link created
                        val shortLink = result.shortLink
                        val flowchartLink = result.previewLink
                        val sAuxNew =
                            "Listen to this mind-blowing episode from this podcast on SocialMob. Tune in by downloading the app now! You will love it! \n"
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                        sendIntent.type = "text/plain"
                        mContext!!.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                    }
                    .addOnFailureListener {
                        // Error
                        // ...
                    }
            } else {
                AppUtils.showCommonToast(mContext!!, "Can't share episode")
            }
        }

        fun sharePodcast(podcastId: String, mContext: Context?) {


            if (podcastId != "") {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

//                val sAux = "https://socialmob.me/share/show-podcast/v2/$podcastId"
                val sAux = "https://share.socialmob.me/share/show-podcast/v2/$podcastId"

                FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLongLink(
                        Uri.parse("https://socialmob.page.link/?link=$sAux")
                    )
                    .buildShortDynamicLink()
                    .addOnSuccessListener { result ->
                        // Short link created
                        val shortLink = result.shortLink
                        val flowchartLink = result.previewLink
                        val sAuxNew =
                            "This podcast on SocialMob is really good. I recommend you download the app now and tune in!\n"
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                        sendIntent.type = "text/plain"
                        mContext!!.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                    }
                    .addOnFailureListener {
                        // Error
                        // ...
                    }
            } else {
                AppUtils.showCommonToast(mContext!!, "Can't share episode")
            }
        }

        fun sharePost(type: String?, id: String, mContext: Context) {

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")
            var sAux = ""
            var mType = ""
            when (type) {
                "user_image_post" -> mType = "image"
                "user_text_post" -> mType = "text"
                "user_video_post" -> mType = "video"
            }

//            sAux = "https://socialmob.me/share/post/$id?$mType"
            sAux = "https://share.socialmob.me/share/post/$id?$mType"
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse("https://socialmob.page.link/?link=$sAux")
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "See what your friends are upto. Check Socialmob to stay connected with your friends and never miss a thing.\n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                    it.printStackTrace()
                }

        }

        fun shareProfile(mContext: Context, mProfileId: String?) {

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

//            val sAux = "https://socialmob.me/share/profile/$mProfileId"
            val sAux = "https://share.socialmob.me/share/profile/$mProfileId"

            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse("https://socialmob.page.link/?link=$sAux")
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "Hey! This person is interesting! Check their profile out! .\n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                }

        }

        fun sharePlayList(playlistId: String?, mContext: Context) {

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

//            val sAux = "https://socialmob.me/share/playlist/$playlistId?source=sm_2"
            val sAux = "https://share.socialmob.me/share/playlist/$playlistId?source=sm_2"

            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse("https://socialmob.page.link/?link=$sAux")
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "This is such a good playlist, you'll love it! Click on the link to have a listen.\n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                }


        }

        fun shareArtist(mContext: Context, mArtistId: String) {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SocialMob")

//            val sAux = "https://socialmob.me/share/artist/$mArtistId?source=sm_2"
            val sAux = "https://share.socialmob.me/share/artist/$mArtistId?source=sm_2"

            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(
                    Uri.parse("https://socialmob.page.link/?link=$sAux")
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { result ->
                    // Short link created
                    val shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    val sAuxNew =
                        "Hey, I'm sure you'll find this artist amazing! Check it out. \n"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "" + sAuxNew + "\n" + shortLink)
                    sendIntent.type = "text/plain"
                    mContext.startActivity(Intent.createChooser(sendIntent, "Choose one"))
                }
                .addOnFailureListener {
                    // Error
                    // ...
                    it.printStackTrace()
                }

        }

    }

}
