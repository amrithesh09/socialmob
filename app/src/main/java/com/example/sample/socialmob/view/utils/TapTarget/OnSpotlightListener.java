package com.example.sample.socialmob.view.utils.TapTarget;

interface OnSpotlightListener {

  /**
   * called when SpotlightView is clicked
   */
  void onSpotlightViewClicked();
}
