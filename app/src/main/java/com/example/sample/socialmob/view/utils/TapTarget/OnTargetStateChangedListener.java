package com.example.sample.socialmob.view.utils.TapTarget;

import com.example.sample.socialmob.view.utils.TapTarget.target.Target;

/**
 * On Target State Changed Listener
 **/
public interface OnTargetStateChangedListener<T extends Target> {
  /**
   * Called when Target is started
   */
  void onStarted(T target);

  /**
   * Called when Target is started
   */
  void onEnded(T target);
}
