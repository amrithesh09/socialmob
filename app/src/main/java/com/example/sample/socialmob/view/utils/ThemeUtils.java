package com.example.sample.socialmob.view.utils;

import android.app.Activity;
import android.content.Intent;

import com.bumptech.glide.Glide;
import com.example.sample.socialmob.R;

public class ThemeUtils {
    private static int cTheme;


    public final static int NIGHT = 0;

    public final static int NORMAL = 1;

    public static void changeToTheme(Activity activity, int theme) {

        cTheme = theme;
        if (MyApplication.Companion.getApplication() != null) {
            Glide.with(MyApplication.Companion.getApplication()).pauseRequests();
        }
        activity.finish();
        activity.overridePendingTransition(0, 0);
        onActivityCreateSetTheme(activity);

        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.overridePendingTransition(0, 0);

    }

    private static void onActivityCreateSetTheme(Activity activity) {

        switch (cTheme) {

            default:

            case NIGHT:

                activity.setTheme(R.style.AppTheme_NoActionBarTranslucentNight);

                break;

            case NORMAL:

                activity.setTheme(R.style.AppTheme_NoActionBarTranslucent);

                break;

        }

    }
}