package com.example.sample.socialmob.view.utils

import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Parcel
import android.os.Parcelable
import android.provider.DocumentsContract
import android.provider.MediaStore
import com.example.sample.socialmob.IMGLYEvents.ProgressState_EXPORT_PROGRESS
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.feed.MainFeedFragment
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.ui.write_new_post.UploadPhotoActivityMentionHashTag
import com.example.sample.socialmob.view.utils.MyApplication.Companion.application
import com.example.sample.socialmob.view.utils.events.UploadStatus
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import ly.img.android.pesdk.annotations.OnEvent
import ly.img.android.pesdk.backend.model.state.EditorSaveState
import ly.img.android.pesdk.backend.model.state.EditorSaveState.Event.EXPORT_DONE
import ly.img.android.pesdk.backend.model.state.EditorSaveState.Event.EXPORT_START
import ly.img.android.pesdk.backend.model.state.EditorSaveState.Event.EXPORT_START_IN_BACKGROUND
import ly.img.android.pesdk.backend.model.state.TransformSettings
import ly.img.android.pesdk.backend.model.state.TransformSettings.Event.ASPECT
import ly.img.android.pesdk.backend.model.state.manager.EventTracker
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.FileOutputStream
import java.util.*

class VesdkEventTracker : EventTracker, Parcelable {
    private var trackerId: String? = null
    private var mVideoRatio = "1:1"
    private var mAuth = ""
    private var mThumbList: ArrayList<File>? = ArrayList()

    constructor(trackerId: String?) {
        init(trackerId)
    }

    constructor(ini: Parcel) : super(ini) {
        init(ini.readString())
    }


    @OnEvent(EXPORT_START)
    fun exportStarted() {
        println(">>>>>>>>>>>>EditorSaveState.Event.EXPORT_START")

//                    MainFeedFragment.EXPORT_STARTED = true
//        MainFeedFragment.isCompletedVideoEdit = false
//        //                    ProfileFeedFragment.EXPORT_STARTED = true
//        ProfileFeedFragment.isCompletedVideoEdit = false
//        //                    ProfileMediaGridFragment.EXPORT_STARTED = true
//        ProfileMediaGridFragment.isCompletedVideoEdit = false
//        val intentVideo = Intent(application, UploadPhotoActivityMentionHashTag::class.java)
//        intentVideo.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        intentVideo.putExtra("title", "UPLOAD VIDEO")
//        intentVideo.putExtra("file", "")
//        intentVideo.putExtra("RATIO", "")
//        intentVideo.putExtra("type", "video")
//        intentVideo.putExtra("mImageList", "")
//        Objects.requireNonNull(application)?.startActivity(intentVideo)
    }

    @OnEvent(value = [ASPECT], triggerDelay = 100)
    fun onTRANSFORMATION(settingsList: TransformSettings) {
        println(">>>>>>transformSettings$settingsList")
        val ts = settingsList.getSettingsModel(
            TransformSettings::class.java
        )
        val mRatio = ts.getAspectRation() // Returns the aspect ratio
        if (mRatio == 1.333333) {
            mVideoRatio = "4:3"
        } else if (mRatio == 1.777778) {
            mVideoRatio = "16:9"
        } else if (mRatio == 1.0) {
            mVideoRatio = "1:1"
        }
        println(">>>>>>>>>>>>Ratio>>$mVideoRatio")
    }

    @OnEvent(EXPORT_DONE)
    fun exportDone(onResultSaved: EditorSaveState) {
        println(">>>>>>>>>>>>EditorSaveState.Event.EXPORT_DONE")

//        val mImageList: ArrayList<GalleryData> = ArrayList()
//        val mGalleryData = GalleryData()
//        val mPath = getFilePath(application!!, onResultSaved.outputUri!!)
//        mGalleryData.photoUri = mPath!!
//        mImageList.add(mGalleryData)
//
//
//        val fullData = RemoteConstant.getEncryptedString(
//            SharedPrefsUtils.getStringPreference(
//                application!!,
//                RemoteConstant.mAppId,
//                ""
//            )!!,
//            SharedPrefsUtils.getStringPreference(
//                application!!,
//                RemoteConstant.mApiKey,
//                ""
//            )!!,
//            RemoteConstant.mBaseUrl + RemoteConstant.mSlash + RemoteConstant.mPresignedurl +
//                    RemoteConstant.mSlash + "video" + RemoteConstant.mSlash + mImageList.size.toString(),
//            RemoteConstant.mGetMethod
//        )
//        mAuth =
//            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
//                application!!,
//                RemoteConstant.mAppId,
//                ""
//            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
//
//
//
//
//        for (i in 0 until mImageList.size) {
//
//            val videoThumbImageFile: File
//
//            val bitmap =
//                ThumbnailUtils.createVideoThumbnail(
//                    mImageList[i].photoUri,
//                    //                                mImageListNew[i].photoUri,
//                    MediaStore.Video.Thumbnails.MINI_KIND
//                )
//
//            val file =
//                File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
//            if (!file.exists()) {
//                file.mkdir()
//            }
//            //                        val filesDir = filesDir
//            videoThumbImageFile =
//                File(file, "socialmob" + System.currentTimeMillis() / 1000 + ".jpg")
//
//            try {
//                val os = FileOutputStream(videoThumbImageFile)
//                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, os)
//                os.flush()
//                os.close()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//            mThumbList?.add(videoThumbImageFile)

//        }

//        when {
//            SharedPrefsUtils.getStringPreference(
//                application,
//                "POST_FROM",
//                ""
//            ) == "CAMPAIGN_FEED" -> {
//                ProfileMediaGridFragment.isAddedFeed = true
//                ProfileMediaGridFragment.mThumbListPost = mThumbList
//                ProfileMediaGridFragment.mAuthPost = mAuth
//                ProfileMediaGridFragment.mImageListPost = mImageList.size.toString()
//                ProfileMediaGridFragment.mImageListPostList = mImageList
//                ProfileMediaGridFragment.ratio = mVideoRatio
//                ProfileMediaGridFragment.isCompletedVideoEdit = true
//            }
//            SharedPrefsUtils.getStringPreference(application, "POST_FROM", "") == "MAIN_FEED" -> {
//                MainFeedFragment.isAddedFeed = true
//                MainFeedFragment.mThumbListPost = mThumbList
//                MainFeedFragment.mAuthPost = mAuth
//                MainFeedFragment.mImageListPost = mImageList.size.toString()
//                MainFeedFragment.mImageListPostList = mImageList
//                MainFeedFragment.ratio = mVideoRatio
//                MainFeedFragment.isCompletedVideoEdit = true
//                println("" + MainFeedFragment.isCompletedVideoEdit)
//            }
//            else -> {
//                ProfileFeedFragment.isAddedFeed = true
//                ProfileFeedFragment.mAuthPost = mAuth
//                ProfileFeedFragment.mImageListPost = mImageList.size.toString()
//                ProfileFeedFragment.mImageListPostList = mImageList
//                ProfileFeedFragment.mThumbListPost = mThumbList
//                ProfileFeedFragment.ratio = mVideoRatio
//                ProfileFeedFragment.isCompletedVideoEdit = true
//
//
//            }
//        }
//        EventBus.getDefault().post(UploadStatus(true, onResultSaved.outputUri!!, mVideoRatio, true))
    }

    @OnEvent(EXPORT_START_IN_BACKGROUND)
    fun exportBg() {
        println(">>>>>>>>>>>>EditorSaveState.Event.EXPORT_START_IN_BACKGROUND")
    }

    @OnEvent(ProgressState_EXPORT_PROGRESS)
    fun exportProgressBg() {
        println(">>>>>>>>>>>>EditorSaveState.Event.EXPORT_START_IN_BACKGROUND")
    }


    private fun init(trackerId: String?) {
        this.trackerId = trackerId
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(trackerId)
    }

    companion object CREATOR : Parcelable.Creator<VesdkEventTracker> {
        override fun createFromParcel(parcel: Parcel): VesdkEventTracker {
            return VesdkEventTracker(parcel)
        }

        override fun newArray(size: Int): Array<VesdkEventTracker?> {
            return arrayOfNulls(size)
        }
    }

    private fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                context.applicationContext,
                uri
            )
        ) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {


            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor? = null
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }
}