package com.example.sample.socialmob.view.utils

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.viewmodel.settings.SettingViewModel
import kotlinx.android.synthetic.main.error_activity.*

class YourCustomErrorActivity : BaseCommonActivity() {
    private val viewModel: SettingViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO : Change status bar color
        changeStatusBarColor()
        setContentView(R.layout.error_activity)

        go_to_home.setOnClickListener {
            viewModel.clearDataBaseWithoutProfile()
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(0, 0)
            this.finish()
            finishAffinity()
        }
    }

    private fun changeStatusBarColor() {
        // finally change the color
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window
            .setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
    }

}