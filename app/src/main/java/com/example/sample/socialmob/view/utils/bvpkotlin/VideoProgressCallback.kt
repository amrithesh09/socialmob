package com.example.sample.socialmob.view.utils.bvpkotlin

interface VideoProgressCallback {

    fun onProgressUpdate(position: Int, duration: Int)
}