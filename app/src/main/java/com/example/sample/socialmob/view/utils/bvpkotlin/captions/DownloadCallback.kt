package com.example.sample.socialmob.view.utils.bvpkotlin.captions

import java.io.File

interface DownloadCallback {
    fun onDownload(file: File)
    fun onFail(e: Exception)
}