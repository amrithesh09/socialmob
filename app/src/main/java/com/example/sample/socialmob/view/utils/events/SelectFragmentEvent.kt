package com.example.sample.socialmob.view.utils.events

class SelectFragmentEvent(private var mSelectFragment: String) {

    fun gemSelectFragment(): String {
        return mSelectFragment
    }

    fun setmSelectFragment(mSelectFragment: String) {
        this.mSelectFragment = mSelectFragment
    }

}
