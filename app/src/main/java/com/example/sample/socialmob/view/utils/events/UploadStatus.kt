package com.example.sample.socialmob.view.utils.events

import android.net.Uri

class UploadStatus(
    var isAdded: Boolean,
    var mFilePath: Uri,
    var mRatio: String,
    var isCompleted: Boolean = false
)