package com.example.sample.socialmob.view.utils.galleryPicker.model

object GalleryDataType {
    val IMAGE = 1
    val VIDEO = 2
}