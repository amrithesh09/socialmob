package com.example.sample.socialmob.view.utils.galleryPicker.model.interactor

interface PhotosInteractor {
    fun getPhoneAlbums()
}