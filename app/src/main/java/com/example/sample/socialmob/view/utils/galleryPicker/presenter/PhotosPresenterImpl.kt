package com.example.sample.socialmob.view.utils.galleryPicker.presenter

import com.example.sample.socialmob.view.utils.galleryPicker.model.interactor.PhotosInteractorImpl
import com.example.sample.socialmob.view.utils.galleryPicker.view.PhotosFragment

class PhotosPresenterImpl(var photosFragment: PhotosFragment): PhotosPresenter {
    val interactor: PhotosInteractorImpl = PhotosInteractorImpl(this)
    override fun getPhoneAlbums() {
        interactor.getPhoneAlbums()
    }
}