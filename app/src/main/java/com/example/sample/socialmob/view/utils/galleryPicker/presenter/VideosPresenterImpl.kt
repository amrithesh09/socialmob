package com.example.sample.socialmob.view.utils.galleryPicker.presenter

import com.example.sample.socialmob.view.utils.galleryPicker.model.interactor.VideosInteractorImpl
import com.example.sample.socialmob.view.utils.galleryPicker.view.VideosFragment

class VideosPresenterImpl(var videosFragment: VideosFragment): VideosPresenter {
    var interactor = VideosInteractorImpl(this)
    override fun getPhoneAlbums() {
        interactor.getPhoneAlbums()
    }
}