package com.example.sample.socialmob.view.utils.galleryPicker.utils.font

object FontsConstants {
    val MULI_REGULAR = "poppinsmedium.otf"
    val MULI_SEMIBOLD = "poppinssemibold.otf"
}