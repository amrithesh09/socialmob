package com.example.sample.socialmob.view.utils.galleryPicker.utils.scroll

/**
 * Created by deepan-5901 on 25/01/18.
 */
interface OnFastScrollStateChangeListener {
    fun onFastScrollStart()
    fun onFastScrollStop()
}
