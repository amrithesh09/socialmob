package com.example.sample.socialmob.view.utils.galleryPicker.view

import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryAlbums
import kotlin.collections.ArrayList

interface OnPhoneImagesObtained {
    fun onComplete(albums: ArrayList<GalleryAlbums>)
    fun onError()
}
