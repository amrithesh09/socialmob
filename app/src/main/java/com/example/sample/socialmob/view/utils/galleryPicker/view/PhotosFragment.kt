package com.example.sample.socialmob.view.utils.galleryPicker.view

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.feed.ImageEditorActivity
import com.example.sample.socialmob.view.ui.write_new_post.Config
import com.example.sample.socialmob.view.ui.write_new_post.Config.KeyName.IMAGE_LIST
import com.example.sample.socialmob.view.ui.write_new_post.utils.CropActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryAlbums
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.galleryPicker.presenter.PhotosPresenterImpl
import com.example.sample.socialmob.view.utils.galleryPicker.utils.MLog
import com.example.sample.socialmob.view.utils.galleryPicker.utils.RunOnUiThread
import com.example.sample.socialmob.view.utils.galleryPicker.utils.font.FontsConstants
import com.example.sample.socialmob.view.utils.galleryPicker.utils.font.FontsManager
import com.example.sample.socialmob.view.utils.galleryPicker.utils.keypad.HideKeypad
import com.example.sample.socialmob.view.utils.galleryPicker.view.adapters.AlbumAdapter
import com.example.sample.socialmob.view.utils.galleryPicker.view.adapters.ImageGridAdapter
import kotlinx.android.synthetic.main.fragment_media.*
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.*

class PhotosFragment : Fragment(), ImagePickerContract {

    var photoList: ArrayList<GalleryData> = ArrayList()
    var albumList: ArrayList<GalleryAlbums> = ArrayList()
    lateinit var glm: androidx.recyclerview.widget.GridLayoutManager
    var photoids: ArrayList<Int> = ArrayList()
    val imagePickerPresenter: PhotosPresenterImpl = PhotosPresenterImpl(this)
    lateinit var listener: OnPhoneImagesObtained
    private val PERMISSIONS_READ_WRITE = 123

    lateinit var ctx: Context
    private var mName = ""
    private var isColorChanged = false
    private var isClicked = false

    companion object {
        private const val ARG_STRING = "mString"

        fun newInstance(mName: String): PhotosFragment {
            val args = Bundle()
            args.putSerializable(ARG_STRING, mName)
            val fragment = PhotosFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        val args = arguments
        mName = args?.getString(ARG_STRING, "")!!

    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        ctx = inflater.context
        return inflater.inflate(R.layout.fragment_media, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        allowAccessButton.outlineProvider = ViewOutlineProvider.BACKGROUND

        initViews()

        allowAccessButton.setOnClickListener {
            if (isReadWritePermitted()) initGalleryViews() else checkReadWritePermission()
        }

        if (activity != null) HideKeypad().hideKeyboard(requireActivity())
        backFrame.setOnClickListener { activity?.onBackPressed() }

//        imageGrid.setPopUpTypeface(FontsManager(ctx).getTypeface(FontsConstants.MULI_SEMIBOLD))
        galleryIllusTitle.typeface = FontsManager(ctx).getTypeface(FontsConstants.MULI_SEMIBOLD)
        galleryIllusContent.typeface = FontsManager(ctx).getTypeface(FontsConstants.MULI_REGULAR)
        allowAccessButton.typeface = FontsManager(ctx).getTypeface(FontsConstants.MULI_SEMIBOLD)
    }

    fun initViews() {
        photoList.clear()
        albumList.clear()
        photoids.clear()
        if (isReadWritePermitted()) initGalleryViews() else allowAccessFrame.visibility =
                View.VISIBLE


        // TODO : TO Change selected item count
        create_set_card_view.visibility = View.GONE
        create_set_card_view.setOnClickListener {
            if (!isColorChanged) {
                isColorChanged = true
                create_set_card_view.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
                create_set_text_view.setTextColor(Color.parseColor("#000000"))
                (ctx as PickerActivity).IMAGES_THRESHOLD = 5
                galleryOperation()

            } else {
                isColorChanged = false
                create_set_card_view.setCardBackgroundColor(Color.parseColor("#666666"))
                create_set_text_view.setTextColor(Color.parseColor("#bbbbbb"))
                (ctx as PickerActivity).IMAGES_THRESHOLD = 1
                galleryOperation()
            }

        }

    }

    fun initGalleryViews() {
        allowAccessFrame.visibility = View.GONE
        glm = androidx.recyclerview.widget.GridLayoutManager(ctx, 4)
        imageGrid.itemAnimator = null
        val bundle = this.arguments
        if (bundle != null) photoids =
                if (bundle.containsKey("photoids")) bundle.getIntegerArrayList("photoids")!! else ArrayList()
        galleryOperation()
    }

    override fun galleryOperation() {
        doAsync {
            albumList = ArrayList()
            listener = object : OnPhoneImagesObtained {
                override fun onComplete(albums: ArrayList<GalleryAlbums>) {
                    albums.sortWith(compareBy { it.name })
                    for (album in albums) {
                        albumList.add(album)
                    }
                    albumList.add(0, GalleryAlbums(0, "All Photos", albumPhotos = photoList))
                    photoList.sortWith(compareByDescending { File(it.photoUri).lastModified() })

                    for (id in photoids) {
                        for (image in photoList) {
                            if (id == image.id) image.isSelected = true
                        }
                    }

                    RunOnUiThread(ctx).safely {
                        imageGrid.layoutManager = glm
                        initRecyclerViews()
                        done.setOnClickListener {

                            val newList: ArrayList<GalleryData> = ArrayList()
                            photoList.filterTo(newList) { it.isSelected && it.isEnabled }

                            if (newList.isNotEmpty()) {
                                if (!isClicked) {
                                    isClicked = true

                                    Handler().postDelayed({
                                        isClicked = false
                                    }, 1000)

                                    val i: Intent?
                                    if (newList.size == 1) {
                                        i = Intent(context, ImageEditorActivity::class.java)
                                    } else {
                                        i = Intent(context, CropActivity::class.java)
                                    }
                                    i.putExtra(Config.KeyName.FILEPATH, newList[0].photoUri)
                                    i.putExtra(IMAGE_LIST, newList)
                                    i.putExtra(Config.KeyName.TITLE, "UPLOAD PHOTO")
                                    when (mName) {
                                        "Profile" -> {
                                            i.putExtra("IS_NEED_TO_ADD", false)
                                            i.putExtra("IS_FROM_CREATE_PROFILE", true)
                                        }
                                        "WritePost" -> i.putExtra("IS_FROM_WRITE_POST", true)
                                        else -> i.putExtra("IS_FROM_CREATE_PROFILE", false)
                                    }
                                    startActivity(i)
                                    activity!!.overridePendingTransition(0, 0)



                                    when (mName) {
                                        "Profile" -> {
                                            activity!!.finish()
                                        }
                                    }
//
                                }
                            } else {
                                AppUtils.showCommonToast(activity!!, "Please choose an image")
                            }

                        }
                        albumselection.setOnClickListener {
                            toggleDropdown()
                        }
                        dropdownframe.setOnClickListener {
                            toggleDropdown()
                        }
                    }
                }

                override fun onError() {
                    MLog.e("CURSOR", "FAILED")
                }
            }

            doAsync {
                getPhoneAlbums(ctx, listener)
            }
        }
    }

    override fun initRecyclerViews() {
        albumsrecyclerview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(ctx)
        albumsrecyclerview.adapter = AlbumAdapter(ArrayList(), this)
        imageGrid.adapter =
                ImageGridAdapter(
                        imageList = photoList,
                        threshold = (ctx as PickerActivity).IMAGES_THRESHOLD
                )
        if (mName == "Profile") {
            ImageGridAdapter.THRESHOLD = 1
        }
        progress_bar.visibility = View.GONE
    }

    override fun toggleDropdown() {
        dropdown.animate().rotationBy(0f).setDuration(300).setInterpolator(LinearInterpolator())
                .start()
        if ((albumsrecyclerview.adapter as AlbumAdapter).malbumList.size == 0) {
            albumsrecyclerview.adapter = AlbumAdapter(albumList, this)
            dropdown.setImageResource(R.drawable.ic_dropdown_rotate)
            try {
                done.isEnabled = false
                val animation = AnimationUtils.loadAnimation(ctx, R.anim.scale_down)
                done.startAnimation(animation)
            } catch (e: Exception) {
            }
            done.visibility = View.GONE
        } else {
            albumsrecyclerview.adapter = AlbumAdapter(ArrayList(), this)
            dropdown.setImageResource(R.drawable.ic_dropdown)
            done.isEnabled = true
            done.visibility = View.VISIBLE
        }
        if (mName == "Profile") {
            ImageGridAdapter.THRESHOLD = 1
        }
    }

    override fun getPhoneAlbums(context: Context, listener: OnPhoneImagesObtained) {
        imagePickerPresenter.getPhoneAlbums()
    }

    override fun updateTitle(galleryAlbums: GalleryAlbums) {
        albumselection.text = galleryAlbums.name
    }

    override fun updateSelectedPhotos(selectedlist: ArrayList<GalleryData>) {
        for (selected in selectedlist) {
            for (photo in photoList) {
                photo.isSelected = selected.id == photo.id
                photo.isEnabled = selected.id == photo.id
            }
        }
    }

    @TargetApi(android.os.Build.VERSION_CODES.JELLY_BEAN)
    fun checkReadWritePermission(): Boolean {
        requestPermissions(
                arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ), PERMISSIONS_READ_WRITE
        )
        return true
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_READ_WRITE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) initGalleryViews()
            else allowAccessFrame.visibility = View.VISIBLE
        }
    }

    private fun isReadWritePermitted(): Boolean =
            (context?.checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && context?.checkCallingOrSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED)
}