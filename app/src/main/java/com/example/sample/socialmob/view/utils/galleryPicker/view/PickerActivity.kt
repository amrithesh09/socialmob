package com.example.sample.socialmob.view.utils.galleryPicker.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentUris
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sample.socialmob.R
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.write_new_post.CameraPhotoVideoBasicFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_picker.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class PickerActivity : BaseCommonActivity() {

    val PERMISSIONS_CAMERA = 124
    var IMAGES_THRESHOLD = 0
    var VIDEOS_THRESHOLD = 0
    var REQUEST_RESULT_CODE = 101
    var title = ""
    var IS_FROM_CREATE_PROFILE = false
    var IS_FROM_WRITE_POST = false
    var SELECT_POSITION = 0

    companion object {
        @SuppressLint("StaticFieldLeak")
        var mContext: Activity? = null
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        // TODO :
        try {
            val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
            if (dir.isDirectory) {
                val children = dir.list()
                for (i in children.indices) {
                    File(dir, children[i]).delete()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }



        mContext = this

        val i = intent
        title = i.getStringExtra("title")!!
        IMAGES_THRESHOLD = i.getIntExtra("IMAGES_LIMIT", 0)
        VIDEOS_THRESHOLD = i.getIntExtra("VIDEOS_LIMIT", 0)
        REQUEST_RESULT_CODE = i.getIntExtra("REQUEST_RESULT_CODE", 0)
        IS_FROM_CREATE_PROFILE = i.getBooleanExtra("IS_FROM_CREATE_PROFILE", false)
        IS_FROM_WRITE_POST = i.getBooleanExtra("IS_FROM_WRITE_POST", false)
        SELECT_POSITION = i.getIntExtra("SELECT_POSITION", 0)

        title_text_view.text = title

        setUpViewPager(viewpager)
        upload_tabs.setupWithViewPager(viewpager)
//        setupTabIcons()
//        tabs.getTabAt(0)?.setIcon(selectedTabIcons[0])
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.setIcon(tabIconList[tab.position])
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.setIcon(selectedTabIcons[tab.position])
            }

        })

        camera.setOnClickListener {
            if (isCameraPermitted()) dispatchTakePictureIntent() else checkCameraPermission()
        }

        // TODO : Select Viewpager Position
        viewpager.currentItem = SELECT_POSITION
    }

    private fun isCameraPermitted(): Boolean {
        val permission = Manifest.permission.CAMERA
        val cameraPermission = checkCallingOrSelfPermission(permission)
        return (cameraPermission == PackageManager.PERMISSION_GRANTED)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun checkCameraPermission(): Boolean {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSIONS_CAMERA)
        return true
    }

    val REQUEST_TAKE_PHOTO = 1
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
            }
            if (photoFile != null) {
                val photoURI =
                    FileProvider.getUriForFile(
                        this,
                        "com.example.abhikumar.socialmob.fileprovider",
                        photoFile
                    )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFileName, ".jpg", storageDir)
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) galleryAddPic()
    }

    private var mCurrentPhotoPath: String = ""

    private fun galleryAddPic() {
        val f = File(mCurrentPhotoPath)
        val contentUri = Uri.fromFile(f)
        val path =
            "${Environment.getExternalStorageDirectory()}${File.separator}Zoho Social${File.separator}media${File.separator}Zoho Social Images"
        val folder = File(path)
        if (!folder.exists()) folder.mkdirs()
        val file = File(
            path,
            "${SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())}_picture.jpg"
        )
        val out = FileOutputStream(file)
        val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, contentUri)
        val ei = ExifInterface(mCurrentPhotoPath)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        val rotatedBitmap: Bitmap? = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> null
        }
        rotatedBitmap?.compress(Bitmap.CompressFormat.JPEG, 70, out)
        out.close()
        ContentUris.parseId(
            Uri.parse(
                MediaStore.Images.Media.insertImage(
                    contentResolver,
                    file.absolutePath,
                    file.name,
                    file.name
                )
            )
        )
        try {
            viewpager.currentItem = 0
            ((viewpager.adapter as ViewPagerAdapter).mFragmentList[0] as? PhotosFragment)?.initViews()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }


    val tabIconList: ArrayList<Int> = ArrayList()
    private val tabIcons = intArrayOf(
        R.drawable.ic_picker_photos_unselected,
        R.drawable.ic_video_unselected
    )

    private val selectedTabIcons = intArrayOf(
        R.drawable.ic_picker_photos_selected,
        R.drawable.ic_video_selected
    )

    private fun setupTabIcons() {
        tabIconList.add(tabIcons[0])
        tabIconList.add(tabIcons[1])
        tabs.getTabAt(0)?.setIcon(tabIconList[0])
        tabs.getTabAt(1)?.setIcon(tabIconList[1])

        for (i in 0 until tabs.tabCount) {
            val tab = tabs.getTabAt(i)
            tab?.setCustomView(R.layout.tab_icon)
        }
    }

    private fun setUpViewPager(viewPager: androidx.viewpager.widget.ViewPager) {
        val adapter = ViewPagerAdapter(this@PickerActivity.supportFragmentManager)
        when {
            IS_FROM_CREATE_PROFILE -> adapter.addFragment(
                CameraPhotoVideoBasicFragment.newInstance(title, "Profile"),
                "CAMERA"
            )
            IS_FROM_WRITE_POST -> adapter.addFragment(
                CameraPhotoVideoBasicFragment.newInstance(title, "WritePost"),
                "CAMERA"
            )
            else -> adapter.addFragment(
                CameraPhotoVideoBasicFragment.newInstance(title, ""),
                "CAMERA"
            )
        }


        if (title == "UPLOAD VIDEO") {
            val videosFragment = VideosFragment()
            adapter.addFragment(videosFragment, "VIDEOS")
        } else {
            when {
                IS_FROM_CREATE_PROFILE -> {
                    val photosFragment = PhotosFragment.newInstance("Profile")
                    adapter.addFragment(photosFragment, "PHOTOS")
                }
                IS_FROM_WRITE_POST -> {
                    val photosFragment = PhotosFragment.newInstance("WritePost")
                    adapter.addFragment(photosFragment, "PHOTOS")
                }
                else -> {
                    val photosFragment = PhotosFragment.newInstance("")
                    adapter.addFragment(photosFragment, "PHOTOS")
                }
            }
        }




        viewPager.adapter = adapter
        (viewPager.adapter as ViewPagerAdapter).notifyDataSetChanged()
        viewPager.currentItem = 0
    }

    internal inner class ViewPagerAdapter(manager: androidx.fragment.app.FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        val mFragmentList: ArrayList<androidx.fragment.app.Fragment> = ArrayList()
        private val mFragmentTitleList: ArrayList<String> = ArrayList()

        override fun getItem(position: Int): androidx.fragment.app.Fragment =
            mFragmentList[position]

        override fun getCount(): Int = mFragmentList.size

        fun addFragment(fragment: androidx.fragment.app.Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getItemPosition(`object`: Any): Int = POSITION_NONE

        //        override fun getPageTitle(position: Int): CharSequence? = null
        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    fun backPressUploadPhoto(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppUtils.hideKeyboard(this@PickerActivity, viewpager)
    }

}
