package com.example.sample.socialmob.view.utils.galleryPicker.view

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.fragment.app.Fragment
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.write_new_post.Config
import com.example.sample.socialmob.view.ui.write_new_post.KVideoEditorDemoActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryAlbums
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.galleryPicker.presenter.VideosPresenterImpl
import com.example.sample.socialmob.view.utils.galleryPicker.utils.MLog
import com.example.sample.socialmob.view.utils.galleryPicker.utils.RunOnUiThread
import com.example.sample.socialmob.view.utils.galleryPicker.utils.font.FontsConstants
import com.example.sample.socialmob.view.utils.galleryPicker.utils.font.FontsManager
import com.example.sample.socialmob.view.utils.galleryPicker.utils.keypad.HideKeypad
import com.example.sample.socialmob.view.utils.galleryPicker.view.adapters.AlbumAdapter
import com.example.sample.socialmob.view.utils.galleryPicker.view.adapters.VideoGridAdapter
import kotlinx.android.synthetic.main.fragment_media.*
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.*

class VideosFragment : Fragment(), ImagePickerContract {

    var photoList: ArrayList<GalleryData> = ArrayList()
    var albumList: ArrayList<GalleryAlbums> = ArrayList()
    lateinit var glm: androidx.recyclerview.widget.GridLayoutManager
    var photoids: ArrayList<Int> = ArrayList()

    val imagePickerPresenter: VideosPresenterImpl = VideosPresenterImpl(this)

    lateinit var listener: OnPhoneImagesObtained

    private val PERMISSIONS_READ_WRITE = 123
    private var mLastClickTime: Long = 0
    lateinit var ctx: Context

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        ctx = inflater.context
        return inflater.inflate(R.layout.fragment_media, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        allowAccessButton.outlineProvider = ViewOutlineProvider.BACKGROUND

//        imageGrid.setPopUpTypeface(FontsManager(ctx).getTypeface(FontsConstants.MULI_SEMIBOLD))
        done.text = "SELECT"
        albumselection.text = "All Videos"
        progress_bar.visibility = View.GONE
        if (isReadWritePermitted()) initGalleryViews() else allowAccessFrame.visibility =
            View.VISIBLE

        allowAccessButton.setOnClickListener {
            if (isReadWritePermitted()) initGalleryViews() else checkReadWritePermission()
        }

        if (activity != null) HideKeypad().hideKeyboard(requireActivity())
        backFrame.setOnClickListener { activity?.onBackPressed() }

        galleryIllusTitle.typeface = FontsManager(ctx).getTypeface(FontsConstants.MULI_SEMIBOLD)
        galleryIllusContent.typeface = FontsManager(ctx).getTypeface(FontsConstants.MULI_REGULAR)
        allowAccessButton.typeface = FontsManager(ctx).getTypeface(FontsConstants.MULI_SEMIBOLD)
    }

    fun initGalleryViews() {
        allowAccessFrame.visibility = View.GONE
        glm = androidx.recyclerview.widget.GridLayoutManager(ctx, 3)
        imageGrid.itemAnimator = null
        val bundle = this.arguments
        if (bundle != null) photoids =
            if (bundle.containsKey("photoids")) bundle.getIntegerArrayList("photoids")!! else ArrayList()
        galleryOperation()
    }

    override fun galleryOperation() {

        doAsync {
            albumList = ArrayList()
            listener = object : OnPhoneImagesObtained {
                override fun onComplete(albums: ArrayList<GalleryAlbums>) {
                    albums.sortWith(compareBy { it.name })
                    for (album in albums) {
                        albumList.add(album)
                    }
                    albumList.add(0, GalleryAlbums(0, "All Videos", albumPhotos = photoList))
                    photoList.sortWith(compareByDescending { File(it.photoUri).lastModified() })

                    for (id in photoids) {
                        for (image in photoList) {
                            if (id == image.id) image.isSelected = true
                        }
                    }

                    RunOnUiThread(ctx).safely {
                        imageGrid.layoutManager = glm
                        initRecyclerViews()
                        done.setOnClickListener {


                            val newList: ArrayList<GalleryData> = ArrayList()
                            photoList.filterTo(newList) { it.isSelected && it.isEnabled }
//                            val i = Intent()
//                            i.putParcelableArrayListExtra("MEDIA", newList)
//                            (ctx as PickerActivity).setResult((ctx as PickerActivity).REQUEST_RESULT_CODE, i)
//                            (ctx as PickerActivity).onBackPressed()

                            if (newList.isNotEmpty()) {
//                                val i = Intent(context, DetailedVideo::class.java)


                                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                                    println(">Double tap")
                                } else {
                                    val i =
//                                        Intent(context, VideoEditorSdkActivity::class.java)
                                        Intent(context, KVideoEditorDemoActivity::class.java)
//                                        Intent(context, KVideoEditorDemoActivityNew::class.java)
                                    i.putExtra(Config.KeyName.FILEPATH, newList[0].photoUri)
                                    i.putExtra(Config.KeyName.TITLE, "UPLOAD VIDEO")
                                    i.putExtra(Config.KeyName.IMAGE_LIST, newList)
                                    startActivity(i)
                                    activity?.overridePendingTransition(0, 0)
                                    activity?.finish()
                                }
                                mLastClickTime = SystemClock.elapsedRealtime()

                            } else {
                                AppUtils.showCommonToast(activity!!, "Please choose an video")
                            }

                        }
                        albumselection.setOnClickListener {
                            toggleDropdown()
                        }
                        dropdownframe.setOnClickListener {
                            toggleDropdown()
                        }
                    }
                }

                override fun onError() {
                    MLog.e("CURSOR", "FAILED")
                }
            }

            doAsync {
                getPhoneAlbums(ctx, listener)
            }
        }
    }


    override fun initRecyclerViews() {
        albumsrecyclerview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(ctx)
        albumsrecyclerview.adapter = AlbumAdapter(ArrayList(), this)
        imageGrid.adapter =
            VideoGridAdapter(
                imageList = photoList,
                threshold = (ctx as PickerActivity).VIDEOS_THRESHOLD
            )
    }

    override fun toggleDropdown() {
        dropdown.animate().rotationBy(0f).setDuration(300).setInterpolator(LinearInterpolator())
            .start()
        if ((albumsrecyclerview.adapter as AlbumAdapter).malbumList.size == 0) {
            albumsrecyclerview.adapter = AlbumAdapter(albumList, this)
            dropdown.setImageResource(R.drawable.ic_dropdown_rotate)
            try {
                done.isEnabled = false
                val animation = AnimationUtils.loadAnimation(ctx, R.anim.scale_down)
                done.startAnimation(animation)
            } catch (e: Exception) {
            }
            done.visibility = View.GONE
        } else {
            albumsrecyclerview.adapter = AlbumAdapter(ArrayList(), this)
            dropdown.setImageResource(R.drawable.ic_dropdown)
            done.isEnabled = true
            done.visibility = View.VISIBLE
        }
    }

    override fun getPhoneAlbums(context: Context, listener: OnPhoneImagesObtained) {
        imagePickerPresenter.getPhoneAlbums()
    }

    override fun updateTitle(galleryAlbums: GalleryAlbums) {
        albumselection.text = galleryAlbums.name
    }

    override fun updateSelectedPhotos(selectedlist: ArrayList<GalleryData>) {
        for (selected in selectedlist) {
            for (photo in photoList) {
                photo.isSelected = selected.id == photo.id
                photo.isEnabled = selected.id == photo.id
            }
        }
    }

    @TargetApi(android.os.Build.VERSION_CODES.JELLY_BEAN)
    fun checkReadWritePermission(): Boolean {
        requestPermissions(
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ), PERMISSIONS_READ_WRITE
        )
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_READ_WRITE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) initGalleryViews()
            else allowAccessFrame.visibility = View.VISIBLE
        }
    }

    private fun isReadWritePermitted(): Boolean =
        (context?.checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && context?.checkCallingOrSelfPermission(
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
}