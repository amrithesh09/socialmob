package com.example.sample.socialmob.view.utils.music.data

import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.view.utils.playlistcore.annotation.SupportedMediaType
import com.example.sample.socialmob.view.utils.playlistcore.api.PlaylistItem
import com.example.sample.socialmob.model.music.Episode
import com.example.sample.socialmob.model.music.RadioPodCastEpisode
import com.example.sample.socialmob.model.music.Station

/**
 * A custom [PlaylistItem]
 * to hold the information pertaining to the audio and video profileItems
 */
class MediaItem : PlaylistItem {

    private var sampleRadio: RadioPodCastEpisode? = null
    private var sampleRecommendedEpisode: Episode? = null
    private var sampleStation: Station? = null
    private var playListDataClass: PlayListDataClass? = null
    private var trackListCommon: TrackListCommon? = null
    private var isAudio: Boolean = false


    override val mExtraId: String?
        get() {
            var mId: String? = ""
            when {
                playListDataClass != null -> mId = playListDataClass?.mExtraId
                trackListCommon != null -> mId = trackListCommon?.id
                else -> ""
            }
            return mId
        }

    override val mExtraIdPodCast: String?
        get() {
            var mId: String? = ""
            when {
                playListDataClass != null -> mId = playListDataClass?.mExtraIdPodCast
                trackListCommon != null -> mId = trackListCommon?.id
                else -> ""
            }
            return mId
        }


    override val id: Long
        get() {
            val mId: String?
            when {
                sampleRadio != null -> mId = ""
                sampleStation != null -> mId = ""
                playListDataClass != null -> mId = playListDataClass?.JukeBoxTrackId
                trackListCommon != null -> mId = trackListCommon?.numberId
                else -> mId = sampleRecommendedEpisode?.EpisodeId
            }
            return if (mId != null) {
                if (mId == "") 0 else java.lang.Long.parseLong(mId)
            } else
                0


        }

    override val downloaded: Boolean?
        get() {
            return false
        }
    override val downloadedOffline: String?
        get() {
            var allowOffline: String? = "0"
            when {
                playListDataClass != null -> {
                    if (playListDataClass?.AllowOffline != "") {
                        allowOffline = playListDataClass?.AllowOffline!!
                    } else {
                        allowOffline = "0"
                    }

                }
                trackListCommon != null -> {
                    if (trackListCommon?.allowOfflineDownload != "") {
                        allowOffline = trackListCommon?.allowOfflineDownload!!
                    } else {
                        allowOffline = "0"
                    }

                }
            }
            return allowOffline!!
        }
    override val playListName: String?
        get() {
            return when {
                playListDataClass != null -> playListDataClass?.playListName
                trackListCommon != null -> trackListCommon?.playListName
                else -> ""
            }
        }

    override val mediaType: Int
        @SupportedMediaType
        get() = if (isAudio) {
            1
        } else {
            0
        }

    override//        return sample.getTrackFile();
    val mediaUrl: String?
        get() {
            return when {
                sampleRadio != null -> sampleRadio?.source_path
                sampleStation != null -> sampleStation?.station_url
                playListDataClass != null -> playListDataClass?.TrackFile
                trackListCommon != null -> trackListCommon?.media?.trackFile
                else -> sampleRecommendedEpisode?.EpisodeSourcePath
            }
        }

    override val downloadedMediaUri: String?
        get() = null

    override//        return sample.getTrackImage();
    val thumbnailUrl: String?
        get() {
            val mTrackImage: String?
            when {
                sampleRadio != null -> mTrackImage = sampleRadio?.cover_image
                sampleStation != null -> mTrackImage = sampleStation?.albumArt
                playListDataClass != null -> mTrackImage = playListDataClass?.TrackImage
                trackListCommon != null -> mTrackImage = trackListCommon?.media?.coverFile
                else -> mTrackImage = sampleRecommendedEpisode?.ThumbnailPath
            }
            return mTrackImage
        }

    override val artworkUrl: String?
        get() = ""

    override//        return sample.getTrackName();
    val title: String?
        get() {
            val mTitle: String?
            when {
                sampleRadio != null -> mTitle = sampleRadio?.episode_name
                sampleStation != null -> mTitle = sampleStation?.name
                playListDataClass != null -> mTitle = playListDataClass?.TrackName
                trackListCommon != null -> mTitle = trackListCommon?.trackName
                else -> mTitle = sampleRecommendedEpisode?.EpisodeName
            }

            return mTitle
        }

    override//        return sample.getJukeBoxCategoryName();
    val album: String?
        get() {

            val mAlbum: String?
            when {
                sampleRadio != null -> mAlbum = sampleRadio?.podcast_name
                playListDataClass != null -> mAlbum = playListDataClass?.JukeBoxCategoryName
                trackListCommon != null -> mAlbum = trackListCommon?.genre?.genreName
                sampleStation != null -> mAlbum = ""
                else -> mAlbum = sampleRecommendedEpisode?.PodcastName
            }

            return mAlbum
        }

    override//        return sample.getAuthor();
    //        return sample.getAuthor();
    val artist: String?
        get() {
            val mArtist: String?
            when {
                playListDataClass != null -> mArtist = playListDataClass?.Author
                sampleRadio != null -> mArtist = sampleRadio?.podcast_name
                trackListCommon != null -> mArtist = trackListCommon?.artist?.artistName
                sampleStation != null -> mArtist = ""
                else -> mArtist = ""
            }

            return mArtist
        }

    override val artistId: String?
        get() {
            var artistId: String? = ""
            when {
                playListDataClass != null -> artistId = playListDataClass?.artistId
                trackListCommon != null -> artistId = trackListCommon?.artist?.id
            }
            return artistId
        }
    override val albumId: String?
        get() {
            var albumId: String? = ""
            when {
                playListDataClass != null -> albumId = playListDataClass?.albumId
                trackListCommon != null -> albumId = trackListCommon?.album?.id
            }
            return albumId
        }
    override val albumName: String?
        get() {
            var albumName: String? = ""
            when {
                playListDataClass != null -> albumName = playListDataClass?.albumName
                trackListCommon != null -> albumName = trackListCommon?.album?.albumName
            }
            return albumName
        }
    override val playTime: String?
        get() {
            var mMillisec: String? = ""
            if (sampleRadio != null) {
                mMillisec = sampleRadio?.Millisecond
            }
            return mMillisec
        }

    override var isFavorite: String
        get() {

            var isFavorite: String? = "false"
            when {
                playListDataClass != null -> isFavorite = playListDataClass?.Favorite
                trackListCommon != null -> isFavorite = trackListCommon?.favorite
            }

            return isFavorite!!
        }
        set(value) {
            when {
                playListDataClass != null -> playListDataClass?.Favorite = value
                trackListCommon != null -> trackListCommon?.favorite = value
            }
        }
    override var playCount: String
        get() {

            var playCount: String? = "0"
            when {
                playListDataClass != null -> playCount = playListDataClass?.playCount
                trackListCommon != null -> playCount = trackListCommon?.playCount
            }

            return playCount!!
        }
        set(value) {
            when {
                playListDataClass != null -> playListDataClass?.playCount = value
                trackListCommon != null -> trackListCommon?.playCount = value
            }
        }

    constructor(sampleRadio: RadioPodCastEpisode, isAudio: Boolean) {
        this.sampleRadio = sampleRadio
        this.isAudio = isAudio
    }

    constructor(sampleRecommendedEpisode: Episode, isAudio: Boolean) {
        this.sampleRecommendedEpisode = sampleRecommendedEpisode
        this.isAudio = isAudio
    }

    constructor(sampleStation: Station, isAudio: Boolean) {
        this.sampleStation = sampleStation
        this.isAudio = isAudio
    }

    constructor(playListDataClass: PlayListDataClass, isAudio: Boolean) {
        this.playListDataClass = playListDataClass
        this.isAudio = isAudio
    }

    constructor(trackListCommon: TrackListCommon, isAudio: Boolean) {
        this.trackListCommon = trackListCommon
        this.isAudio = isAudio
    }
}