package com.example.sample.socialmob.view.utils.music.helper;

import androidx.annotation.NonNull;
import com.devbrackets.android.exomedia.listener.*;
import com.example.sample.socialmob.view.utils.playlistcore.api.MediaPlayerApi;
import com.example.sample.socialmob.view.utils.playlistcore.listener.MediaStatusListener;
import com.example.sample.socialmob.view.utils.music.data.MediaItem;

public abstract class BaseMediaApi implements MediaPlayerApi<MediaItem>,
        OnPreparedListener,
        OnCompletionListener,
        OnErrorListener,
        OnSeekCompletionListener,
        OnBufferUpdateListener {

    protected boolean prepared;
    protected int bufferPercent;

    protected MediaStatusListener<MediaItem> mediaStatusListener;

    @Override
    public void setMediaStatusListener(@NonNull MediaStatusListener<MediaItem> listener) {
        mediaStatusListener = listener;
    }

    @Override
    public void onCompletion() {
        if (mediaStatusListener != null) {
            mediaStatusListener.onCompletion(this);
        }
    }

    @Override
    public boolean onError(Exception e) {
        try {
            return mediaStatusListener != null && mediaStatusListener.onError(this);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return false;
    }

    @Override
    public void onPrepared() {
        prepared = true;
        try {
            if (mediaStatusListener != null) {

                mediaStatusListener.onPrepared(this);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSeekComplete() {
        if (mediaStatusListener != null) {
            mediaStatusListener.onSeekComplete(this);
        }
    }

    @Override
    public void onBufferingUpdate(int percent) {
        bufferPercent = percent;

        if (mediaStatusListener != null) {
            mediaStatusListener.onBufferingUpdate(this, percent);
        }
    }
}
