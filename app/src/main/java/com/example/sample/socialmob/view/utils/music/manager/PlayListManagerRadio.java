package com.example.sample.socialmob.view.utils.music.manager;

import android.app.Application;
import androidx.annotation.NonNull;
import com.devbrackets.android.exomedia.listener.VideoControlsButtonListener;
import com.devbrackets.android.exomedia.ui.widget.VideoControls;
import com.example.sample.socialmob.view.utils.playlistcore.manager.ListPlaylistManager;
import com.example.sample.socialmob.view.utils.music.data.MediaItem;
import com.example.sample.socialmob.view.utils.music.helper.VideoApi;
import com.example.sample.socialmob.view.utils.music.service.MediaServiceRadio;

public class PlayListManagerRadio extends ListPlaylistManager<MediaItem> {

    public PlayListManagerRadio(Application application) {
        super(application, MediaServiceRadio.class);
    }

    /**
     * Note: You can call {@link #getMediaPlayers()} and add it manually in the activity,
     * however we have this helper method to allow registration of the media controls
     * listener provided by ExoMedia's {@link com.devbrackets.android.exomedia.ui.widget.VideoControls}
     */
    public void addVideoApi(@NonNull VideoApi videoApi) {
        getMediaPlayers().add(videoApi);
        updateVideoControls(videoApi);
        registerPlaylistListener(videoApi);
    }

    /**
     * Note: You can call {@link #getMediaPlayers()} and remove it manually in the activity,
     * however we have this helper method to remove the registered listener from {@link #addVideoApi(VideoApi)}
     */
    public void removeVideoApi(@NonNull VideoApi videoApi) {
        VideoControls controls = videoApi.videoView.getVideoControls();
        if (controls != null) {
            controls.setButtonListener(null);
        }

        unRegisterPlaylistListener(videoApi);
        getMediaPlayers().remove(videoApi);
    }

    /**
     * Updates the available controls on the VideoView and links the
     * button events to the playlist service and this.
     *
     * @param videoApi The VideoApi to link
     */
    private void updateVideoControls(@NonNull VideoApi videoApi) {
        VideoControls videoControls = videoApi.videoView.getVideoControls();
        if (videoControls != null) {
            videoControls.setPreviousButtonRemoved(false);
            videoControls.setNextButtonRemoved(false);
            videoControls.setButtonListener(new PlayListManagerRadio.ControlsListener());
        }
    }

    /**
     * An implementation of the {@link VideoControlsButtonListener} that provides
     * integration with the playlist service.
     */
    private class ControlsListener implements VideoControlsButtonListener {
        @Override
        public boolean onPlayPauseClicked() {
            invokePausePlay();
            return true;
        }

        @Override
        public boolean onPreviousClicked() {
//            invokePrevious();
            return false;
        }

        @Override
        public boolean onNextClicked() {
//            invokeNext();
            return false;
        }

        @Override
        public boolean onRewindClicked() {
            return false;
        }

        @Override
        public boolean onFastForwardClicked() {
            return false;
        }
    }

}
