package com.example.sample.socialmob.view.utils.music.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.sample.socialmob.R;
import com.example.sample.socialmob.repository.utils.RemoteConstant;
import com.example.sample.socialmob.view.utils.music.data.MediaItem;
import com.example.sample.socialmob.view.utils.playlistcore.components.image.ImageProvider;


public class MediaImageProvider implements ImageProvider<MediaItem> {
    interface OnImageUpdatedListener {
        void onImageUpdated();
    }

    @NonNull
    private RequestManager glide;
    @NonNull
    private OnImageUpdatedListener listener;

    @NonNull
    private NotificationImageTarget notificationImageTarget = new NotificationImageTarget();
    @NonNull
    private RemoteViewImageTarget remoteViewImageTarget = new RemoteViewImageTarget();

    @NonNull
    private Bitmap defaultNotificationImage;

    @Nullable
    private Bitmap notificationImage;
    @Nullable
    private Bitmap artworkImage;

    public MediaImageProvider(@NonNull Context context, @NonNull OnImageUpdatedListener listener) {
        glide = Glide.with(context.getApplicationContext());
        this.listener = listener;

        defaultNotificationImage = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_foreground);
    }

    @Override
    public int getNotificationIconRes() {
        return R.mipmap.ic_launcher_foreground;
    }

    @Override
    public int getRemoteViewIconRes() {
        return R.mipmap.ic_launcher_foreground;
    }

    @Nullable
    @Override
    public Bitmap getLargeNotificationImage() {
        return notificationImage != null ? notificationImage : defaultNotificationImage;
    }

    @Nullable
    @Override
    public Bitmap getRemoteViewArtwork() {
        return artworkImage;
    }

    @Override
    public void updateImages(@NonNull final MediaItem playlistItem) {

        // TODO : Bitmap recycle issue
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                if (playlistItem.getThumbnailUrl() != null && !playlistItem.getThumbnailUrl().equals("")) {

                    try {
                        glide.asBitmap().load(RemoteConstant.INSTANCE.getImageUrlFromWidth(playlistItem.getThumbnailUrl(), 10)).into(notificationImageTarget);
                    } catch (Exception | OutOfMemoryError e) {
                        e.printStackTrace();
                    }

                }
                if (playlistItem.getArtworkUrl() != null && !playlistItem.getArtworkUrl().equals("")) {
                    try {
                        glide.asBitmap().load(RemoteConstant.INSTANCE.getImageUrlFromWidth(playlistItem.getArtworkUrl(), 10)).into(notificationImageTarget);
                    } catch (Exception | OutOfMemoryError e) {
                        e.printStackTrace();
                    }
//            glide.asBitmap().load(playlistItem.getArtworkUrl()).into(remoteViewImageTarget);
                }


            }
        });
//        if (playlistItem.getThumbnailUrl() != null && !playlistItem.getThumbnailUrl().equals("")) {
//            new LoadImageTask(RemoteConstant.INSTANCE.getImageUrlFromWidth(playlistItem.getThumbnailUrl(), 10), glide,
//                    remoteViewImageTarget).execute();
//        }
//        if (playlistItem.getArtworkUrl() != null && !playlistItem.getArtworkUrl().equals("")) {
//            new LoadImageTask(RemoteConstant.INSTANCE.getImageUrlFromWidth(playlistItem.getArtworkUrl(), 10), glide,
//                    remoteViewImageTarget).execute();
//        }


    }
    /**
     * A class used to listen to the loading of the large notification images and perform
     * the correct functionality to update the notification once it is loaded.
     * <p>
     * <b>NOTE:</b> This is a Glide Image loader class
     */
    private class NotificationImageTarget extends SimpleTarget<Bitmap> {
        @Override
        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
            notificationImage = resource;
            listener.onImageUpdated();
        }
    }

    /**
     * A class used to listen to the loading of the large lock screen images and perform
     * the correct functionality to update the artwork once it is loaded.
     * <p>
     * <b>NOTE:</b> This is a Glide Image loader class
     */
    private class RemoteViewImageTarget extends SimpleTarget<Bitmap> {
        @Override
        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
            artworkImage = resource;
            listener.onImageUpdated();
        }
    }


}
