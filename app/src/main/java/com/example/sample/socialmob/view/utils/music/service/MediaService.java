package com.example.sample.socialmob.view.utils.music.service;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import com.example.sample.socialmob.view.utils.playlistcore.api.MediaPlayerApi;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DefaultPlaylistHandler;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PlaylistHandler;
import com.example.sample.socialmob.view.utils.playlistcore.service.BasePlaylistService;
import com.example.sample.socialmob.view.utils.music.data.MediaItem;
import com.example.sample.socialmob.view.utils.music.helper.AudioApi;
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager;
import com.example.sample.socialmob.view.utils.MyApplication;

/**
 * A simple service that extends {@link BasePlaylistService} in order to provide
 * the application specific information required.
 */
public class MediaService extends BasePlaylistService<MediaItem, PlaylistManager>  {

    @Override
    public void onCreate() {
        super.onCreate();

        // Adds the audio player implementation, otherwise there's nothing to play media with
        getPlaylistManager().getMediaPlayers().add(new AudioApi(getApplicationContext()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isMyServiceRunning(MediaService.class)) {
            Intent stopServiceIntent = new Intent(this, MediaService.class);
            stopService(stopServiceIntent);
        }

        // Releases and clears all the MediaPlayers
        for (MediaPlayerApi<MediaItem> player : getPlaylistManager().getMediaPlayers()) {
            player.release();
        }

        getPlaylistManager().getMediaPlayers().clear();
    }

    @NonNull
    @Override
    protected PlaylistManager getPlaylistManager() {
        return MyApplication.Companion.getPlaylistManager();
    }

    @NonNull
    @Override
    public PlaylistHandler<MediaItem> newPlaylistHandler() {
        MediaImageProvider imageProvider = new MediaImageProvider(getApplicationContext(), new MediaImageProvider.OnImageUpdatedListener() {
            @Override
            public void onImageUpdated() {
                getPlaylistHandler().updateMediaControls();
            }
        });

        return new DefaultPlaylistHandler.Builder<>(
                getApplicationContext(),
                getClass(),
                getPlaylistManager(),
                imageProvider
        ).build();
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}