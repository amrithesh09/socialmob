package com.example.sample.socialmob.view.utils.music.service;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import com.example.sample.socialmob.view.utils.playlistcore.api.MediaPlayerApi;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DefaultPlaylistHandler;
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PlaylistHandler;
import com.example.sample.socialmob.view.utils.playlistcore.service.BasePlaylistService;
import com.example.sample.socialmob.view.utils.music.data.MediaItem;
import com.example.sample.socialmob.view.utils.music.helper.AudioApi;
import com.example.sample.socialmob.view.utils.music.manager.PlayListManagerRadio;
import com.example.sample.socialmob.view.utils.MyApplication;

public class MediaServiceRadio extends BasePlaylistService<MediaItem, PlayListManagerRadio> {

    @Override
    public void onCreate() {
        super.onCreate();

        getPlaylistManager().getMediaPlayers().add(new AudioApi(getApplicationContext()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Releases and clears all the MediaPlayers
        for (MediaPlayerApi<MediaItem> player : getPlaylistManager().getMediaPlayers()) {
            player.release();
        }
        if (isMyServiceRunning(MediaServiceRadio.class)) {
            Intent stopServiceIntentMediaServiceRadio = new Intent(this, MediaServiceRadio.class);
            stopService(stopServiceIntentMediaServiceRadio);
        }
        if (getPlaylistManager().getMediaPlayers().size() > 0) {
            getPlaylistManager().getMediaPlayers().clear();
        }
    }

    @NonNull
    @Override
    protected PlayListManagerRadio getPlaylistManager() {
        return MyApplication.Companion.getPlaylistManagerRadio();
    }

    @NonNull
    @Override
    public PlaylistHandler<MediaItem> newPlaylistHandler() {
        MediaImageProvider imageProvider = new MediaImageProvider(getApplicationContext(), new MediaImageProvider.OnImageUpdatedListener() {
            @Override
            public void onImageUpdated() {
                getPlaylistHandler().updateMediaControls();
            }
        });

        return new DefaultPlaylistHandler.Builder<>(
                getApplicationContext(),
                getClass(),
                getPlaylistManager(),
                imageProvider
        ).build();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}