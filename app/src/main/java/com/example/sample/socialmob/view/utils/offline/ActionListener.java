package com.example.sample.socialmob.view.utils.offline;

public interface ActionListener {

    void onPauseDownload(int id);

    void onResumeDownload(int id);

    void onRemoveDownload(int id, String url);

    void onRetryDownload(int id);
}
