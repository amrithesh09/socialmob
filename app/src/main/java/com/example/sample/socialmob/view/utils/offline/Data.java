package com.example.sample.socialmob.view.utils.offline;

import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;

import com.example.sample.socialmob.repository.model.TrackListCommonDb;
import com.tonyodev.fetch2.Request;

import java.util.ArrayList;
import java.util.List;


public final class Data {

//    public static final String[] sampleUrls = new String[]{
//            "https://media-cdn1.socialmob.me/socialmob-music-production/track-files/686b8941-1bcf-4909-88db-4bdd24908db2.mp3"};

    private Data() {

    }

    @NonNull
    private static List<Request> getFetchRequests(List<TrackListCommonDb> trackListCommonDbs) {

        ArrayList<String> sampleUrls = new ArrayList<>();
        for (int i = 0; i < trackListCommonDbs.size(); i++) {
            sampleUrls.add(trackListCommonDbs.get(i).getTrackFile());
        }

        List<Request> requests = new ArrayList();
        if (sampleUrls.size() > 0) {
            requests = new ArrayList<>();
            for (String sampleUrl : sampleUrls) {
                final Request request = new Request(sampleUrl, getFilePath(sampleUrl));
                requests.add(request);
            }
        }
        return requests;

//        return null;
    }

    @NonNull
    public static List<Request> getFetchRequestWithGroupId(final int groupId, List<TrackListCommonDb> trackListCommonDbs) {
        final List<Request> requests = getFetchRequests(trackListCommonDbs);
        if (requests != null && requests.size() > 0) {
            for (Request request : requests) {
                request.setGroupId(groupId);
            }
            return requests;
        } else
            return null;
    }

    @NonNull
    private static String getFilePath(@NonNull final String url) {
        final Uri uri = Uri.parse(url);
        final String fileName = uri.getLastPathSegment();
        final String dir = getSaveDir();
        return (dir + "/Socialmobtracks/" + fileName);
    }

    @NonNull
    static String getNameFromUrl(final String url) {
        return Uri.parse(url).getLastPathSegment();
    }

    @NonNull
    public static String getSaveDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/.smtracks";
    }

}
