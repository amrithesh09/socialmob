package com.example.sample.socialmob.view.utils.offline

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.RequestManager
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.TrackListCommon
import com.example.sample.socialmob.model.music.TrackListCommonArtist
import com.example.sample.socialmob.model.music.TrackListCommonGenre
import com.example.sample.socialmob.model.music.TrackListCommonMedia
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.utils.BaseCommonActivity
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.cardview.CardView
import com.example.sample.socialmob.view.utils.galleryPicker.utils.SquareLayout
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.music.manager.PlaylistManager
import com.example.sample.socialmob.view.utils.offline.FileAdapter.DownloadData
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.listener.PlaylistListener
import com.example.sample.socialmob.viewmodel.music_menu.NowPlayingViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2.Fetch.Impl.getInstance
import com.tonyodev.fetch2core.Downloader.FileDownloaderType
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class DownloadListActivity : BaseCommonActivity(), ActionListener, PlaylistListener<MediaItem> {
    @Inject
    lateinit var glideRequestManager: RequestManager
    private var fileAdapter: FileAdapter? = null
    private var fetch: Fetch? = null
    private val nowPlayingViewModel: NowPlayingViewModel by viewModels()
    private var trackListCommonDbs: List<TrackListCommonDb?>? = ArrayList()
    private var trackListToList: List<TrackListCommonDb?>? = ArrayList()
    private var trackListToPlay: List<TrackListCommonDb?> = ArrayList()
    private var isAdded = false
    private var myDownloadsLayout: SquareLayout? = null
    private var gif_progress: LottieAnimationView? = null
    private var playlistManager: PlaylistManager? = null
    private var fab: FloatingActionButton? = null
    private var mTrackLocationGlobal = ""
    private var mOfflineTrackDownloaded: ArrayList<Download?>? = ArrayList()
    private var mPlayPositionFeaturedTrack = -1
    private var mLastClickTime = 0L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.offline_activty)

        if (nowPlayingViewModel.downloadTrackListModel != null) {
            nowPlayingViewModel.downloadTrackListModel!!.observe(
                    this,
                    { trackListCommonDbs: List<TrackListCommonDb?>? ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            if (!isAdded) {
                                isAdded = true
                                fileAdapter!!.setTrackData(trackListCommonDbs)
                                trackListToList = trackListCommonDbs
                                checkStoragePermissions()
                            }
                        }
                    })
        }
        if (nowPlayingViewModel.downloadTrueTrackListModel != null) {
            nowPlayingViewModel.downloadTrueTrackListModel!!.observe(
                    this,
                    { trackListCommonDbs: List<TrackListCommonDb?>? ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            trackListToPlay = trackListCommonDbs
                        }
                    })
        }
        if (nowPlayingViewModel.downloadFalseTrackListModel != null) {
            nowPlayingViewModel.downloadFalseTrackListModel!!.observe(
                    this,
                    { trackListCommonDbs: List<TrackListCommonDb?>? ->
                        if (trackListCommonDbs != null && trackListCommonDbs.size > 0) {
                            this.trackListCommonDbs = trackListCommonDbs
                        }
                    })
        }
        setUpViews()
        val fetchConfiguration: FetchConfiguration = FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(4)
                .setHttpDownloader(OkHttpDownloader(FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE) //                .setNotificationManager(new DefaultFetchNotificationManager(this) {
                //                    @NotNull
                //                    @Override
                //                    public Fetch getFetchInstanceForNamespace(@NotNull String namespace) {
                //                        return fetch;
                //                    }
                //                })
                .build()
        fetch = getInstance(fetchConfiguration)
    }

    private fun setUpViews() {
//        final SwitchCompat networkSwitch = findViewById(R.id.networkSwitch);
        gif_progress = findViewById(R.id.gif_progress)
        myDownloadsLayout = findViewById(R.id.my_downloads_layout)
        val cardLayout = findViewById<CardView>(R.id.cardLayout)
        val back_image_view = findViewById<ImageView>(R.id.back_image_view)
        val go_online = findViewById<TextView>(R.id.go_online)
        val title_linear = findViewById<LinearLayout>(R.id.title_linear)
        val no_data_linear = findViewById<LinearLayout>(R.id.no_data_linear)
        val progress_linear = findViewById<LinearLayout>(R.id.progress_linear)
        fab = findViewById(R.id.fab)
        val recyclerView = findViewById<RecyclerView>(R.id.offline_track_recycler_view)
        if (intent != null) {
            val intent = intent
            val mIsFrom = intent.getStringExtra("mIsFrom")
            if (mIsFrom != null && mIsFrom == "myDownloads") {
                cardLayout.visibility = View.GONE
                myDownloadsLayout?.visibility = View.VISIBLE
                title_linear.visibility = View.GONE
            }
        }
        no_data_linear.visibility = View.GONE
        progress_linear.visibility = View.GONE
        back_image_view.setOnClickListener { view: View? -> go_online.performClick() }
        go_online.setOnClickListener { finish() }
        gif_progress?.setOnClickListener { fab?.performClick() }
        fab?.setOnClickListener {
            if (java.lang.String.valueOf(
                            MyApplication.playlistManager!!.id
                    ).toInt() != -1
            ) {
                if (MyApplication.playlistManager != null && MyApplication.playlistManager!!.playlistHandler != null && MyApplication.playlistManager!!.playlistHandler!!.currentMediaPlayer != null &&
                        MyApplication.playlistManager!!.playlistHandler!!.currentMediaPlayer!!.isPlaying
                ) {
                    val detailIntent =
                            Intent(this@DownloadListActivity, NowPlayingActivity::class.java)
                    var mPlayPosition = "0"
                    if (MyApplication != null && MyApplication.playlistManager != null
                            && MyApplication.playlistManager!!.playlistHandler != null &&
                            MyApplication.playlistManager!!.playlistHandler!!.currentItemChange != null
                            && MyApplication.playlistManager!!.playlistHandler!!.currentItemChange!!.currentItem
                            != null && MyApplication.playlistManager!!.playlistHandler!!.currentItemChange!!.currentItem!!.id != -1L) {
                        mPlayPosition =
                                MyApplication.playlistManager!!.playlistHandler!!.currentItemChange!!.currentItem!!.id.toString()
                    }
                    detailIntent.putExtra(
                            RemoteConstant.mExtraIndex, mPlayPosition
                    )
                    val mImageUrlPass = MyApplication.playlistManager?.playlistHandler?.currentItemChange?.currentItem?.thumbnailUrl

                    detailIntent.putExtra(RemoteConstant.extraIndexImage, mImageUrlPass)
                    startActivity(detailIntent)
                } else if (MyApplication.playlistManager != null && MyApplication.playlistManager!!.playlistHandler != null && MyApplication.playlistManager!!.playlistHandler!!.currentMediaPlayer != null) {
                    val detailIntent =
                            Intent(this@DownloadListActivity, NowPlayingActivity::class.java)
                    detailIntent.putExtra(RemoteConstant.mExtraIndex, 0)
//                    detailIntent.putExtra(RemoteConstant.extraIndexImage, "")
                    startActivity(detailIntent)
                }
                //                    else{
//                        launchEmptyPlayList()
//                    }
            }
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        //        networkSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            if (isChecked) {
//                fetch.setGlobalNetworkType(NetworkType.WIFI_ONLY);
//            } else {
//                fetch.setGlobalNetworkType(NetworkType.ALL);
//            }
//        });
        fileAdapter = FileAdapter(this, this@DownloadListActivity, glideRequestManager)
        recyclerView.adapter = fileAdapter
    }

    fun playTrack(mTrackLocation: String) {
        try {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return
            } else {
                mTrackLocationGlobal = mTrackLocation
                try {
                    if (trackListToList != null && trackListToList?.isNotEmpty()!! &&
                            trackListToList?.get(mPlayPositionFeaturedTrack) != null && trackListToList!!.size >
                            mPlayPositionFeaturedTrack && mPlayPositionFeaturedTrack != -1) {
                        trackListToList?.get(mPlayPositionFeaturedTrack)?.is_playing = "false"
                        fileAdapter?.setTrackData(trackListToList)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                val mediaItems = LinkedList<MediaItem>()
                val playList: ArrayList<TrackListCommon> = ArrayList()
                var playPostion = 0
                for (i in trackListToPlay.indices) {
                    val trackListCommon = TrackListCommon()
                    trackListCommon.id = trackListToPlay[i]!!._id
                    trackListCommon.numberId = trackListToPlay[i]!!.number_id
                    val trackListCommonMedia = TrackListCommonMedia()
                    trackListCommon.trackName = trackListToPlay[i]!!.track_name
                    val uri = Uri.parse(trackListToPlay[i]!!.trackFile).lastPathSegment
                    trackListCommonMedia.trackFile =
                            "/storage/emulated/0/Download/.smtracks/Socialmobtracks/$uri"
                    trackListCommonMedia.coverFile = trackListToPlay[i]!!.trackImage
                    val artist = TrackListCommonArtist()
                    artist.artistName = trackListToPlay[i]!!.artist
                    trackListCommon.artist = artist
                    val genre = TrackListCommonGenre()
                    genre.genreName = trackListToPlay[i]!!.genre
                    trackListCommon.genre = genre
                    trackListCommon.media = trackListCommonMedia
                    playList.add(trackListCommon)
                    val uri1 = Uri.parse(mTrackLocation).lastPathSegment
                    if (uri != null && uri == uri1) {
                        playPostion = i
                    }
                }
                if (trackListToList != null && trackListToList!!.size > 0 && trackListToList!!.size > playPostion && trackListToList!![playPostion] != null && trackListToList!![playPostion]!!.number_id != ""
                ) {
                    mPlayPositionFeaturedTrack = playPostion
                    trackListToList!![playPostion]!!.is_playing = "true"
                    fileAdapter!!.setTrackData(trackListToList)
                }
                nowPlayingViewModel.createDbPlayListModelNotification(playList, "Offline tracks")
                if (playList.size > 0) {
                    mediaItems.clear()
                    for (i in playList.indices) {
                        val mediaItem = MediaItem(
                                playList[i], true
                        )
                        mediaItems.add(mediaItem)
                    }
                }
                if (MyApplication.playlistManager != null) {
                    MyApplication.playlistManager!!.setParameters(mediaItems, playPostion)
                    MyApplication.playlistManager!!.id = 4
                    MyApplication.playlistManager!!.currentPosition = playPostion
                    MyApplication.playlistManager!!.play(0, false)
                }
            }
            mLastClickTime = SystemClock.elapsedRealtime()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (!hasFocus) {
            Log.i("Tag", "Notification bar is pulled down")
        } else {
            onResume()
            Log.i("Tag", "Notification bar is pushed up")
        }
        super.onWindowFocusChanged(hasFocus)
    }

    override fun onResume() {
        super.onResume()
        fetch!!.getDownloadsInGroup(GROUP_ID) { downloads: List<Download> ->
            val list = ArrayList(downloads)
            Collections.sort(list) { first: Download, second: Download ->
                java.lang.Long.compare(
                        first.created,
                        second.created
                )
            }
            mOfflineTrackDownloaded = list
            for (download in list) {
                fileAdapter!!.addDownload(download!!)
            }
        }.addListener(fetchListener)
        playlistManager = MyApplication.playlistManager
        if (playlistManager != null) {
            playlistManager!!.registerPlaylistListener(this)
        }
        // TODO : Check is playing
        if (playlistManager != null && playlistManager!!.playlistHandler != null && playlistManager!!.playlistHandler!!.currentMediaPlayer != null && playlistManager!!.playlistHandler!!.currentMediaPlayer!!.isPlaying) {
            fab!!.visibility = View.GONE
            gif_progress!!.visibility = View.VISIBLE
            gif_progress!!.playAnimation()
            gif_progress!!.bringToFront()
            updateCurrentPlaybackInformation()
        } else {
            gif_progress!!.visibility = View.GONE
            fab!!.visibility = View.VISIBLE
            gif_progress!!.pauseAnimation()
            playPauseCommon("false")
            if (trackListToList != null
                    && trackListToList!!.size > 0
            ) {
                for (i in trackListToList!!.indices) {
                    trackListToList!![i]!!.is_playing = "false"
                }
                fileAdapter!!.setTrackData(trackListToList)
            }
        }

//
    }

    /**
     * Makes sure to update the UI to the current playback item.
     */
    private fun updateCurrentPlaybackInformation() {
        val itemChange = playlistManager!!.currentItemChange
        if (itemChange != null) {
            onPlaylistItemChanged(
                    itemChange.currentItem,
                    itemChange.hasNext,
                    itemChange.hasPrevious
            )
        }
        val currentPlaybackState = playlistManager!!.currentPlaybackState
        if (currentPlaybackState !== PlaybackState.STOPPED) {
            onPlaybackStateChanged(currentPlaybackState)
        }

//        String  mediaProgress = playlistManager.getCurrentProgress();
//        if (mediaProgress != null) {
//            onProgressUpdated(mediaProgress)
//        }
    }

    private fun playPauseCommon(mIsplaying: String) {
        try {
            var playPostion = 0
            if (playlistManager!!.currentItemChange != null && playlistManager!!.currentItemChange!!.currentItem != null && playlistManager!!.currentItemChange!!.currentItem!!.mediaUrl != null) {
                val mTrackLocation = playlistManager!!.currentItemChange!!.currentItem!!.mediaUrl
                for (i in trackListToList!!.indices) {
                    val uri = Uri.parse(trackListToList!![i]!!.trackFile).lastPathSegment
                    val uri1 = Uri.parse(mTrackLocation).lastPathSegment
                    if (uri != null && uri == uri1) {
                        playPostion = i
                    }
                }
                if (trackListToList!!.size > 0 && trackListToList!!.size > playPostion && trackListToList!![playPostion] != null && trackListToList!![playPostion]!!.number_id != ""
                ) {
                    mPlayPositionFeaturedTrack = playPostion
                    trackListToList!![playPostion]!!.is_playing = mIsplaying
                    fileAdapter!!.setTrackData(trackListToList)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        fetch!!.removeListener(fetchListener)
        playlistManager!!.unRegisterPlaylistListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        fetch!!.close()
    }

    private val fetchListener: FetchListener = object : AbstractFetchListener() {
        override fun onAdded(download: Download) {
            fileAdapter!!.addDownload(download)
        }

        override fun onQueued(download: Download, waitingOnNetwork: Boolean) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onCompleted(download: Download) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onError(download: Download, error: Error, throwable: Throwable?) {
            super.onError(download, error, throwable)
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onProgress(
                download: Download,
                etaInMilliseconds: Long,
                downloadedBytesPerSecond: Long
        ) {
            fileAdapter!!.update(download, etaInMilliseconds, downloadedBytesPerSecond)
        }

        override fun onPaused(download: Download) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onResumed(download: Download) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onCancelled(download: Download) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onRemoved(download: Download) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }

        override fun onDeleted(download: Download) {
            fileAdapter!!.update(
                    download,
                    UNKNOWN_REMAINING_TIME,
                    UNKNOWN_DOWNLOADED_BYTES_PER_SECOND
            )
        }
    }

    private fun checkStoragePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    STORAGE_PERMISSION_CODE
            )
        } else {
            enqueueDownloads()
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enqueueDownloads()
        } else {
            Snackbar.make(
                    myDownloadsLayout!!,
                    R.string.permission_not_enabled,
                    Snackbar.LENGTH_INDEFINITE
            ).show()
        }
    }

    private fun enqueueDownloads() {
        if (trackListCommonDbs != null && trackListCommonDbs!!.size > 0) {
//            final List<Request> requests = Data.getFetchRequestWithGroupId(GROUP_ID, trackListCommonDbs);
//            if (requests != null && requests.size() > 0) {
//                fetch.enqueue(requests, updatedRequests -> {
//
//                });
//            }
        }
    }

    override fun onPauseDownload(id: Int) {
        fetch!!.pause(id)
    }

    override fun onResumeDownload(id: Int) {
        fetch!!.resume(id)
    }

    override fun onRemoveDownload(id: Int, url: String) {
        fetch!!.remove(id)
        isAdded = false
        nowPlayingViewModel.clearOfflineTrackDb(url)
        if (playlistManager != null && playlistManager!!.playlistHandler != null && playlistManager!!.playlistHandler!!.currentMediaPlayer != null && playlistManager!!.playlistHandler!!.currentMediaPlayer!!.isPlaying) {
            playlistManager!!.playlistHandler!!.currentMediaPlayer!!.pause()
            onPlaybackStateChanged(PlaybackState.PAUSED)
        }
        val mediaItems = LinkedList<MediaItem>()
        val playList: ArrayList<TrackListCommon> = ArrayList()
        for (i in trackListToList!!.indices) {
            val trackListCommon = TrackListCommon()
            trackListCommon.id = trackListToList!![i]!!._id
            trackListCommon.numberId = trackListToList!![i]!!.number_id
            val trackListCommonMedia = TrackListCommonMedia()
            trackListCommon.trackName = trackListToList!![i]!!.track_name
            val uri = Uri.parse(trackListToList!![i]!!.trackFile).lastPathSegment
            trackListCommonMedia.trackFile =
                    "/storage/emulated/0/Download/.smtracks/Socialmobtracks/$uri"
            trackListCommonMedia.coverFile = trackListToList!![i]!!.trackImage
            val artist = TrackListCommonArtist()
            artist.artistName = trackListToList!![i]!!.artist
            trackListCommon.artist = artist
            val genre = TrackListCommonGenre()
            genre.genreName = trackListToList!![i]!!.genre
            trackListCommon.genre = genre
            trackListCommon.media = trackListCommonMedia
            playList.add(trackListCommon)
        }
        nowPlayingViewModel.createDbPlayListModelNotification(playList.toMutableList(), "Offline tracks")
        if (playList.size > 0) {
            mediaItems.clear()
            for (i in playList.indices) {
                val mediaItem = MediaItem(
                        playList[i], true
                )
                mediaItems.add(mediaItem)
            }
        }
        if (MyApplication.playlistManager != null) {
            MyApplication.playlistManager!!.setParameters(mediaItems, 0)
            MyApplication.playlistManager!!.id = 4
            MyApplication.playlistManager!!.currentPosition = 0
            MyApplication.playlistManager!!.play(0, true)
        }
    }

    override fun onRetryDownload(id: Int) {
        fetch!!.retry(id)
    }

    private fun changeStatusBarColor() {
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = ContextCompat.getColor(
                this,
                R.color.white
        )
    }

    private fun changeStatusBarColorBlack() {
        window.statusBarColor = ContextCompat.getColor(
                this,
                R.color.black
        )
    }

    fun updateLocalDb(mTrackId: String?) {
        nowPlayingViewModel.updateDownloadStatus(mTrackId!!)
    }

    private val mChangeId = ""
    override fun onPlaylistItemChanged(
            currentItem: MediaItem?,
            hasNext: Boolean,
            hasPrevious: Boolean
    ): Boolean {
        try {
            if (currentItem != null && currentItem.id != -1L) {
//                if (!mChangeId.equals("") && !mChangeId.equals(String.valueOf(currentItem.getId()))) {
                if (trackListToList != null
                        && trackListToList!!.size > 0
                ) {
                    for (i in trackListToList!!.indices) {
                        trackListToList!![i]!!.is_playing = "false"
                    }
                    fileAdapter!!.setTrackData(trackListToList)
                }
                var playPostion = 0
                val uri1 = Uri.parse(currentItem.mediaUrl).lastPathSegment
                for (i in trackListToList!!.indices) {
                    val uri = Uri.parse(trackListToList!![i]!!.trackFile).lastPathSegment
                    if (uri != null && uri == uri1) {
                        playPostion = i
                    }
                }
                if (trackListToList != null && trackListToList!!.size > 0 && trackListToList!!.size > playPostion && trackListToList!![playPostion] != null && trackListToList!![playPostion]!!.number_id != ""
                ) {
                    mPlayPositionFeaturedTrack = playPostion
                    trackListToList!![playPostion]!!.is_playing = "true"
                    fileAdapter!!.setTrackData(trackListToList)
                }


//                }
//                mChangeId = String.valueOf(currentItem.getId());
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    override fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean {
        when (playbackState) {
            PlaybackState.PLAYING -> {
                fab!!.visibility = View.GONE
                gif_progress!!.visibility = View.VISIBLE
                gif_progress!!.playAnimation()
                gif_progress!!.bringToFront()
            }
            PlaybackState.PAUSED -> {
                gif_progress!!.visibility = View.GONE
                fab!!.visibility = View.VISIBLE
                gif_progress!!.pauseAnimation()
            }
            PlaybackState.RETRIEVING -> {
            }
            PlaybackState.PREPARING -> {
            }
            PlaybackState.SEEKING -> {
            }
            PlaybackState.STOPPED -> {
            }
            PlaybackState.ERROR -> {
            }
            else -> fab!!.visibility = View.VISIBLE
        }
        return true
    }

    fun deleteAllTracks(downloads: List<DownloadData>) {
        try {
            for (i in downloads.indices) {
                fetch!!.remove(downloads[i].id)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val STORAGE_PERMISSION_CODE = 200
        private const val UNKNOWN_REMAINING_TIME: Long = -1
        private const val UNKNOWN_DOWNLOADED_BYTES_PER_SECOND: Long = 0
        private val GROUP_ID = "listGroup".hashCode()
        const val FETCH_NAMESPACE = "DownloadListActivity"
    }
}