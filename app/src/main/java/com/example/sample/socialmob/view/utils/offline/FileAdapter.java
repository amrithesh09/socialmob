package com.example.sample.socialmob.view.utils.offline;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.sample.socialmob.R;
import com.example.sample.socialmob.repository.model.TrackListCommonDb;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Status;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {

    @NonNull
    private final List<DownloadData> downloads = new ArrayList<>();
    @NonNull
    private final ActionListener actionListener;
    private final Context context;
    private final RequestManager requestManager;
    private final boolean isErrorOccured = false;
    private List<TrackListCommonDb> trackListCommonDbs;


    FileAdapter(@NonNull final ActionListener actionListener, Context context, RequestManager requestManager) {
        this.actionListener = actionListener;
        this.context = context;
        this.requestManager = requestManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.download_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.actionButton.setOnClickListener(null);
        holder.actionButton.setEnabled(true);

        final DownloadData downloadData = downloads.get(position);
        String url = "";
        if (downloadData.download != null) {
            url = downloadData.download.getUrl();
        }
        final Uri uri = Uri.parse(url);
        final Status status = downloadData.download.getStatus();
        final Context context = holder.itemView.getContext();

        String mTrackName = "";
        String mTrackGenre = "";
        String mTrackAuthor = "";
        String mTrackImage = "";
        String mTrackId = "";
        String misPlaying = "false";
        if (trackListCommonDbs != null && trackListCommonDbs.size() > 0) {
            for (int i = 0; i < trackListCommonDbs.size(); i++) {

                Uri mLocalFileUri = Uri.parse(trackListCommonDbs.get(i).getTrackFile());
                if (mLocalFileUri.getLastPathSegment().equals(uri.getLastPathSegment())) {
                    mTrackName = trackListCommonDbs.get(i).getTrack_name();
                    mTrackGenre = trackListCommonDbs.get(i).getGenre();
                    mTrackAuthor = trackListCommonDbs.get(i).getArtist();
                    mTrackImage = trackListCommonDbs.get(i).getTrackImage();
                    mTrackId = trackListCommonDbs.get(i).get_id();
                    misPlaying = trackListCommonDbs.get(i).is_playing();
                }
            }
        }

        if (misPlaying.equals("true")) {
            holder.track_gif_progress.setVisibility(View.VISIBLE);
            holder.track_gif_progress.playAnimation();
        } else {
            holder.track_gif_progress.setVisibility(View.GONE);
            holder.track_gif_progress.pauseAnimation();
        }


//        holder.titleTextView.setText(uri.getLastPathSegment());


        if (mTrackName.equals("") && mTrackAuthor.equals("") && mTrackGenre.equals("")) {
            if (context != null && downloads.size() > 0) {
                ((DownloadListActivity) context).deleteAllTracks(downloads);
            }

        }
        holder.titleTextView.setText(mTrackName);
        holder.artist_name.setText(mTrackAuthor);
        holder.genre_name.setText(mTrackGenre);


        requestManager.load(getImageUrlFromWidthLocal(mTrackImage, 500)).thumbnail(0.1f)
                .apply(RequestOptions.placeholderOf(R.drawable.emptypicture).error(R.drawable.emptypicture).centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH))
                .into(holder.track_image_view);


        holder.statusTextView.setText(getStatusString(status));

        int progress = downloadData.download.getProgress();
        if (progress == -1) { // Download progress is undermined at the moment.
            progress = 0;
        }
        holder.progressBar.setProgress(progress);
        holder.progressTextView.setText(context.getString(R.string.percent_progress, progress));

        if (downloadData.eta == -1) {
            holder.timeRemainingTextView.setText("");
        } else {
            holder.timeRemainingTextView.setText(TrackDownLoadUtils.getETAString(context, downloadData.eta));
        }

        if (downloadData.downloadedBytesPerSecond == 0) {
            holder.downloadedBytesPerSecondTextView.setText("");
        } else {
            holder.downloadedBytesPerSecondTextView.setText(TrackDownLoadUtils.getDownloadSpeedString(context, downloadData.downloadedBytesPerSecond));
        }

        switch (status) {
            case COMPLETED: {
                holder.actionButton.setText(R.string.view);
                holder.actionButton.setVisibility(View.GONE);
                holder.track_options.setVisibility(View.VISIBLE);

                //TODO: Update local db
                ((DownloadListActivity) context).updateLocalDb(mTrackId);

                holder.top_linear.setOnClickListener(view -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        Toast.makeText(context, "Downloaded Path:" + downloadData.download.getFile(), Toast.LENGTH_LONG).show();
                        if (downloadData.download != null && !downloadData.download.getFile().equals("")
                        ) {
                            ((DownloadListActivity) context).playTrack(downloadData.download.getFile());
                        }
                        return;
                    }
                    final File file = new File(downloadData.download.getFile());
                    final Uri uri1 = Uri.fromFile(file);
                    final Intent share = new Intent(Intent.ACTION_VIEW);
                    share.setDataAndType(uri1, TrackDownLoadUtils.getMimeType(context, uri1));
                    context.startActivity(share);
                });
                break;
            }
            case FAILED: {
                holder.actionButton.setVisibility(View.VISIBLE);
                holder.track_options.setVisibility(View.GONE);
                holder.actionButton.setText(R.string.retry);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onRetryDownload(downloadData.download.getId());
                });
                holder.top_linear.setOnClickListener(view -> {
                });
                break;
            }
            case PAUSED: {
                holder.actionButton.setVisibility(View.VISIBLE);
                holder.track_options.setVisibility(View.GONE);
                holder.actionButton.setText(R.string.resume);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onResumeDownload(downloadData.download.getId());
                });
                holder.top_linear.setOnClickListener(view -> {
                });
                break;
            }
            case DOWNLOADING:
            case QUEUED: {
                holder.actionButton.setVisibility(View.VISIBLE);
                holder.track_options.setVisibility(View.GONE);
                holder.actionButton.setText(R.string.pause);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onPauseDownload(downloadData.download.getId());
                });
                holder.top_linear.setOnClickListener(view -> {
                });
                break;
            }
            case ADDED: {
                holder.actionButton.setVisibility(View.VISIBLE);
                holder.track_options.setVisibility(View.GONE);
                holder.actionButton.setText(R.string.download);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onResumeDownload(downloadData.download.getId());
                });
                holder.top_linear.setOnClickListener(view -> {
                });
                break;
            }
            default: {
                break;
            }
        }

        //Set delete action
//        holder.itemView.setOnLongClickListener(v -> {
//            final Uri uri12 = Uri.parse(downloadData.download.getUrl());
//            new AlertDialog.Builder(context)
//                    .setMessage(context.getString(R.string.delete_title, uri12.getLastPathSegment()))
//                    .setPositiveButton(R.string.delete, (dialog, which) -> actionListener.onRemoveDownload(downloadData.download.getId()))
//                    .setNegativeButton(R.string.cancel, null)
//                    .show();
//
//            return true;
//        });
        holder.track_options.setOnClickListener(view -> {
            ViewGroup viewGroupNew = null;
            View viewDelete =
                    LayoutInflater.from(context)
                            .inflate(R.layout.options_menu_single_track, viewGroupNew);
            PopupWindow mQQPopDelete =
                    new PopupWindow(
                            viewDelete,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );
            mQQPopDelete.setAnimationStyle(R.style.RightTopPopAnim);
            mQQPopDelete.setFocusable(true);
//            mQQPopDelete.setBackgroundDrawable();
            mQQPopDelete.setOutsideTouchable(true);

            mQQPopDelete.showAsDropDown(holder.track_options, -230, -20);
            TextView deleteTextview = viewDelete.findViewById(R.id.delete_track);

            deleteTextview.setText(context.getString(R.string.delete));
            deleteTextview.setOnClickListener(view1 -> {
                mQQPopDelete.dismiss();

                AlertDialog.Builder dialogBuilderDelete = new AlertDialog.Builder(context);
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                if (mInflater != null) {
                    View dialogViewDelete =
                            mInflater.inflate(R.layout.common_dialog, null);
                    dialogBuilderDelete.setView(dialogViewDelete);
                    dialogBuilderDelete.setCancelable(false);

                    Button cancelButton = dialogViewDelete.findViewById(R.id.cancel_button);
                    Button okButton = dialogViewDelete.findViewById(R.id.ok_button);

                    AlertDialog alertDialog1 = dialogBuilderDelete.create();
                    alertDialog1.show();
                    okButton.setOnClickListener(view112 -> {
                        actionListener.onRemoveDownload(downloadData.download.getId(), downloadData.download.getUrl());
                        alertDialog1.dismiss();
                    });
                    cancelButton.setOnClickListener(view11 -> alertDialog1.dismiss());
                }
            });


        });

    }

    private String getImageUrlFromWidthLocal(String imageUrl, int width) {
        if (width == 0) {
            width = 500;
        }
        String url = "";
        if (imageUrl != null && imageUrl != "") {
            url = imageUrl + "?imwidth=" + width + "&impolicy=resize";
        }
        return url;
    }

    public void addDownload(@NonNull final Download download) {
        boolean found = false;
        DownloadData data = null;
        int dataPosition = -1;
        for (int i = 0; i < downloads.size(); i++) {
            final DownloadData downloadData = downloads.get(i);
            if (downloadData.id == download.getId()) {
                data = downloadData;
                dataPosition = i;
                found = true;
                break;
            }
        }
        if (!found) {
            final DownloadData downloadData = new DownloadData();
            downloadData.id = download.getId();
            downloadData.download = download;
            downloads.add(downloadData);
            notifyItemInserted(downloads.size() - 1);
        } else {
            data.download = download;
            notifyItemChanged(dataPosition);
        }
    }

    @Override
    public int getItemCount() {
        return downloads.size();
    }

    public void update(@NonNull final Download download, long eta, long downloadedBytesPerSecond) {
        for (int position = 0; position < downloads.size(); position++) {
            final DownloadData downloadData = downloads.get(position);
            if (downloadData.id == download.getId()) {
                switch (download.getStatus()) {
                    case REMOVED:
                    case DELETED: {
                        downloads.remove(position);
                        notifyItemRemoved(position);
                        break;
                    }
                    default: {
                        downloadData.download = download;
                        downloadData.eta = eta;
                        downloadData.downloadedBytesPerSecond = downloadedBytesPerSecond;
                        notifyItemChanged(position);
                    }
                }
                return;
            }
        }
    }

    private String getStatusString(Status status) {
        switch (status) {
            case COMPLETED:
                return "Done";
            case DOWNLOADING:
                return "Downloading";
            case FAILED:
                return "Error";
            case PAUSED:
                return "Paused";
            case QUEUED:
                return "Waiting in Queue";
            case REMOVED:
                return "Removed";
            case NONE:
                return "Not Queued";
            default:
                return "Unknown";
        }
    }

    public void setTrackData(List<TrackListCommonDb> trackListCommonDbs) {
        this.trackListCommonDbs = trackListCommonDbs;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final LinearLayout top_linear;
        public final ImageView track_image_view;
        public final ImageView track_options;
        public final TextView titleTextView;
        public final TextView artist_name;
        public final TextView genre_name;
        final TextView statusTextView;
        public final ProgressBar progressBar;
        public final TextView progressTextView;
        public final Button actionButton;
        final TextView timeRemainingTextView;
        final TextView downloadedBytesPerSecondTextView;
        final LottieAnimationView track_gif_progress;

        ViewHolder(View itemView) {
            super(itemView);
            track_gif_progress = itemView.findViewById(R.id.track_gif_progress);
            top_linear = itemView.findViewById(R.id.top_linear);
            track_image_view = itemView.findViewById(R.id.track_image_view);
            track_options = itemView.findViewById(R.id.track_options);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            artist_name = itemView.findViewById(R.id.artist_name);
            genre_name = itemView.findViewById(R.id.genre_name);
            statusTextView = itemView.findViewById(R.id.status_TextView);
            progressBar = itemView.findViewById(R.id.progressBar);
            actionButton = itemView.findViewById(R.id.actionButton);
            progressTextView = itemView.findViewById(R.id.progress_TextView);
            timeRemainingTextView = itemView.findViewById(R.id.remaining_TextView);
            downloadedBytesPerSecondTextView = itemView.findViewById(R.id.downloadSpeedTextView);
        }

    }

    public static class DownloadData {
        public int id;
        @Nullable
        public Download download;
        long eta = -1;
        long downloadedBytesPerSecond = 0;

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public String toString() {
            if (download == null) {
                return "";
            }
            return download.toString();
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof DownloadData && ((DownloadData) obj).id == id;
        }
    }

}
