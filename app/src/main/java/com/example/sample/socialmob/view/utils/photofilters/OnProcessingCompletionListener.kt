package com.example.sample.socialmob.view.utils.photofilters

import android.graphics.Bitmap

interface OnProcessingCompletionListener {
  fun onProcessingComplete(bitmap: Bitmap)
}