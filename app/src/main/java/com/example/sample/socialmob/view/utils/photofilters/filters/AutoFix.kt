package com.example.sample.socialmob.view.utils.photofilters.filters

data class AutoFix(var scale: Float = 0.5f) : Filter()