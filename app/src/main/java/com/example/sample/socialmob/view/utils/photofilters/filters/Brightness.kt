package com.example.sample.socialmob.view.utils.photofilters.filters

data class Brightness(var brightness: Float = 2.0f) : Filter()