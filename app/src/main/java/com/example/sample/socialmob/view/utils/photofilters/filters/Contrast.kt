package com.example.sample.socialmob.view.utils.photofilters.filters

data class Contrast(var contrast: Float = 1.4f) : Filter()