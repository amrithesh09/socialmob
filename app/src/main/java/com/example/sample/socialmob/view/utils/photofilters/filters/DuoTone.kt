package com.example.sample.socialmob.view.utils.photofilters.filters

import android.graphics.Color

data class DuoTone(
  var firstColor: Int = Color.YELLOW,
  var secondColor: Int = Color.BLACK
) : Filter()