package com.example.sample.socialmob.view.utils.photofilters.filters

data class FillLight(var strength: Float = .8f) : Filter()