package com.example.sample.socialmob.view.utils.photofilters.filters

data class FishEye(var scale: Float = .5f) : Filter()