package com.example.sample.socialmob.view.utils.photofilters.filters

data class Grain(var strength: Float = 1.0f) : Filter()