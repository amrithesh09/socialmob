package com.example.sample.socialmob.view.utils.photofilters.filters

data class Highlight(
  var black: Float = .1f,
  var white: Float = .7f
) : Filter()