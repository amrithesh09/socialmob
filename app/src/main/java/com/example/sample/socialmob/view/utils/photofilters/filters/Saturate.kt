package com.example.sample.socialmob.view.utils.photofilters.filters

data class Saturate(var scale: Float = .5f) : Filter()