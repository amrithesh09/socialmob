package com.example.sample.socialmob.view.utils.photofilters.filters

data class Temperature(var scale: Float = .9f) : Filter()