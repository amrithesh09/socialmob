package com.example.sample.socialmob.view.utils.photofilters.filters

import android.graphics.Color

data class Tint(var tint: Int = Color.MAGENTA) : Filter()