package com.example.sample.socialmob.view.utils.photofilters.filters

data class Vignette(var scale: Float = .5f) : Filter()