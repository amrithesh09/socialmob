package com.example.sample.socialmob.view.utils.playlistcore.annotation

import android.app.Service
import androidx.annotation.IntDef

@IntDef(
        Service.START_STICKY,
        Service.START_NOT_STICKY,
        Service.START_REDELIVER_INTENT
)
@Retention(AnnotationRetention.SOURCE)
annotation class ServiceContinuationMethod
