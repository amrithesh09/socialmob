package com.example.sample.socialmob.view.utils.playlistcore.annotation

import androidx.annotation.IntDef
import com.example.sample.socialmob.view.utils.playlistcore.manager.BasePlaylistManager

@IntDef(flag = true,
        value = [
            BasePlaylistManager.AUDIO,
            BasePlaylistManager.VIDEO
        ]
)
@Retention(AnnotationRetention.SOURCE)
annotation class SupportedMediaType
