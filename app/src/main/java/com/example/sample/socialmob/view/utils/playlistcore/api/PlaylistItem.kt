package com.example.sample.socialmob.view.utils.playlistcore.api

import com.example.sample.socialmob.view.utils.playlistcore.annotation.SupportedMediaType

interface PlaylistItem {
    val id: Long

    val downloaded: Boolean?

    val isFavorite: String

    @SupportedMediaType
    val mediaType: Int

    val mediaUrl: String?

    val downloadedMediaUri: String?

    val thumbnailUrl: String?

    val artworkUrl: String?

    val title: String?

    val album: String?

    val artist: String?

    val playTime: String?

    val mExtraId: String?

    val mExtraIdPodCast: String?


    val artistId: String?

    val albumName: String?

    val albumId: String?

    val playCount: String?

    val downloadedOffline: String?

    val playListName: String?


}
