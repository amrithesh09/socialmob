package com.example.sample.socialmob.view.utils.playlistcore.components.audiofocus

import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PlaylistHandler
import com.example.sample.socialmob.view.utils.playlistcore.api.PlaylistItem

interface AudioFocusProvider<I : PlaylistItem> {
    fun setPlaylistHandler(playlistHandler: PlaylistHandler<I>)

    fun refreshFocus()

    /**
     * Requests to obtain the audio focus
     *
     * @return `true` if the focus was granted
     */
    fun requestFocus(): Boolean

    /**
     * Requests the system to drop the audio focus
     *
     * @return `true` if the focus was lost
     */
    fun abandonFocus(): Boolean
}