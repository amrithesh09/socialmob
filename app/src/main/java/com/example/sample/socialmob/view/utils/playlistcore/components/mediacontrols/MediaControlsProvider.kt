package com.example.sample.socialmob.view.utils.playlistcore.components.mediacontrols

import android.support.v4.media.session.MediaSessionCompat
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaInfo

interface MediaControlsProvider {
    fun update(mediaInfo: MediaInfo, mediaSession: MediaSessionCompat)
}