package com.example.sample.socialmob.view.utils.playlistcore.components.mediasession

import android.support.v4.media.session.MediaSessionCompat
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaInfo

interface MediaSessionProvider {
    fun get(): MediaSessionCompat
    fun update(mediaInfo: MediaInfo)
}