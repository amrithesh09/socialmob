package com.example.sample.socialmob.view.utils.playlistcore.components.notification

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.media.app.NotificationCompat.MediaStyle
import com.example.sample.socialmob.R
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.music.NowPlayingActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.MyApplication.Companion.playlistManager
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaInfo
import com.example.sample.socialmob.view.utils.playlistcore.data.RemoteActions
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


/**
 * A default implementation for the [PlaylistNotificationProvider] that will correctly
 * handle both normal and large notifications, remote action registration, categories,
 * channels, etc.
 *
 * *Note* The default channel this provides only supports an English name and description
 * so it is recommended that you extend this class and override [buildNotificationChannel]
 * to provide your own custom name and description.
 */
open class DefaultPlaylistNotificationProvider(protected val context: Context) :
    PlaylistNotificationProvider {
    companion object {
        const val CHANNEL_ID = "PlaylistCoreMediaNotificationChannel"
    }

    private val notificationManager: NotificationManager by lazy {
        context.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    protected open val clickPendingIntent: PendingIntent?
        get() = null


    @SuppressLint("WrongConstant")
    override fun buildNotification(
        info: MediaInfo,
        mediaSession: MediaSessionCompat,
        serviceClass: Class<out Service>
    ): Notification {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            buildNotificationChannel()
        }

        try {
            return NotificationCompat.Builder(context, CHANNEL_ID).apply {
                Log.v("aakash","Step 1")
                if (info?.appIcon != null) {
                    setSmallIcon(info.appIcon)
                } else {
                    setSmallIcon(R.mipmap.ic_custom_launcher_new_color)
                }
                //setLargeIcon(resize(info.largeNotificationIcon!!, info.largeNotificationIcon!!.width, info.largeNotificationIcon!!.height))

                if (info.largeNotificationIcon != null && info.largeNotificationIcon!!.byteCount != 0) {
                    setLargeIcon(info.largeNotificationIcon)
                    Log.v("aakash","Step 5")
                } else
                    setLargeIcon(BitmapFactory.decodeResource(context.getResources(),R.mipmap.ic_custom_launcher_new_color))


                var contentText = info.album
                if (info.artist.isNotBlank()) {
                    contentText += if (contentText.isNotBlank()) " - " + info.artist else info.artist
                }

                setContentTitle(info.title)
                setContentText(contentText)

                // TODO: Notification Click
//            clickPendingIntent=

                var detailIntent: Intent? = null
                if (SharedPrefsUtils.getStringPreference(
                        MyApplication.application,
                        AppUtils.SELECTED_CATEGORY,
                        ""
                    ) == "Radio"
                ) {
                    detailIntent = Intent(MyApplication.application, HomeActivity::class.java)
//                    detailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    detailIntent.putExtra("mPos", "")
                    detailIntent.putExtra("mPosInner", "")
                    detailIntent.putExtra("radioNotification", true)
                    detailIntent.flags = Intent.FLAG_ACTIVITY_TASK_ON_HOME
//                    detailIntent.flags =  Intent.FLAG_ACTIVITY_SINGLE_TOP
                } else {

                    detailIntent = Intent(MyApplication.application, NowPlayingActivity::class.java)
                }
                detailIntent.action = Intent.ACTION_MAIN
                detailIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                if (playlistManager!!.id != null) {
                    detailIntent.putExtra("EXTRA_INDEX", playlistManager!!.id)
                }
                if (playlistManager != null && playlistManager!!.currentItem != null &&
                    playlistManager!!.currentItem!!.thumbnailUrl != null
                ) {
                    detailIntent.putExtra(
                        "EXTRA_INDEX_IMAGE",
                        playlistManager!!.currentItem!!.thumbnailUrl
                    )
                }

                var mFlag = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                val clickPendingIntent = PendingIntent.getActivity(
                    MyApplication.application, 0,
                    detailIntent, PendingIntent.FLAG_UPDATE_CURRENT
                )
                setContentIntent(clickPendingIntent)

                setDeleteIntent(createPendingIntent(serviceClass, RemoteActions.ACTION_STOP))

                val allowSwipe = !(info.mediaState.isPlaying)
                setAutoCancel(allowSwipe)
                setOngoing(!allowSwipe)

                //Set the notification category on lollipop
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    setCategory(Notification.CATEGORY_TRANSPORT)
                    setVisibility(Notification.VISIBILITY_PUBLIC)
                }
                setActions(this, info, serviceClass)
                setStyle(buildMediaStyle(mediaSession, serviceClass))
            }.build()
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("AmR_", "buildNotification_" +e.toString())
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        }
        Log.d("AmR_", "Notification_")
        return Notification()
    }

    protected open fun setActions(
        builder: NotificationCompat.Builder,
        info: MediaInfo,
        serviceClass: Class<out Service>
    ) {
        with(info.mediaState) {
            // Previous
            var actionIcon =
                if (isPreviousEnabled) R.drawable.playlistcore_notification_previous else R.drawable.playlistcore_notification_previous_disabled
            var title =
                context.resources.getString(R.string.playlistcore_default_notification_previous)
            builder.addAction(
                actionIcon,
                title,
                createPendingIntent(serviceClass, RemoteActions.ACTION_PREVIOUS)
            )

            // Play/Pause
            actionIcon = if (isPlaying) {
                title =
                    context.resources.getString(R.string.playlistcore_default_notification_pause)
                if (isLoading) R.drawable.playlistcore_notification_pause_disabled else R.drawable.playlistcore_notification_pause
            } else {
                title = context.resources.getString(R.string.playlistcore_default_notification_play)
                if (isLoading) R.drawable.playlistcore_notification_play_disabled else R.drawable.playlistcore_notification_play
            }
            builder.addAction(
                actionIcon,
                title,
                createPendingIntent(serviceClass, RemoteActions.ACTION_PLAY_PAUSE)
            )

            // Next
            actionIcon =
                if (isNextEnabled) R.drawable.playlistcore_notification_next else R.drawable.playlistcore_notification_next_disabled
            title = context.resources.getString(R.string.playlistcore_default_notification_next)
            builder.addAction(
                actionIcon,
                title,
                createPendingIntent(serviceClass, RemoteActions.ACTION_NEXT)
            )


        }
    }

    protected open fun buildMediaStyle(
        mediaSession: MediaSessionCompat,
        serviceClass: Class<out Service>
    ): MediaStyle {
        return with(MediaStyle()) {
            setMediaSession(mediaSession.sessionToken)
            setShowActionsInCompactView(0, 1, 2) // previous, play/pause, next
            setShowCancelButton(true)
            setCancelButtonIntent(createPendingIntent(serviceClass, RemoteActions.ACTION_STOP))
        }
    }

    /**
     * Builds the notification channel using the default name and description (English Only)
     * if the channel hasn't already been created
     */
    @TargetApi(Build.VERSION_CODES.O)
    protected open fun buildNotificationChannel() {
        if (notificationManager.getNotificationChannel(CHANNEL_ID) == null) {
            val name =
                context.resources.getString(R.string.playlistcore_default_notification_channel_name)
            val description =
                context.resources.getString(R.string.playlistcore_default_notification_channel_description)
            buildNotificationChannel(name, description)
        }
    }

    /**
     * Builds the notification channel using the specified name and description
     */
    @TargetApi(Build.VERSION_CODES.O)
    protected open fun buildNotificationChannel(name: CharSequence, description: String) {
        val channel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW)
        channel.description = description
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationManager.createNotificationChannel(channel)
    }

    /**
     * Creates a PendingIntent for the given action to the specified service
     *
     * @param action The action to use
     * @param serviceClass The service class to notify of intents
     * @return The resulting PendingIntent
     */
    protected open fun createPendingIntent(
        serviceClass: Class<out Service>,
        action: String
    ): PendingIntent {
        val intent = Intent(context, serviceClass)
        intent.action = action

        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun resize(image: Bitmap, maxWidth: Int, maxHeight: Int): Bitmap? {
        var image = image
        return if (maxHeight > 0 && maxWidth > 0) {
            val width = image.width
            val height = image.height
            val ratioBitmap = width.toFloat() / height.toFloat()
            val ratioMax = maxWidth.toFloat() / maxHeight.toFloat()
            var finalWidth = maxWidth
            var finalHeight = maxHeight
            if (ratioMax > ratioBitmap) {
                finalWidth = (maxHeight.toFloat() * ratioBitmap).toInt()
            } else {
                finalHeight = (maxWidth.toFloat() / ratioBitmap).toInt()
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true)
            image
        } else {
            image
        }
    }
}