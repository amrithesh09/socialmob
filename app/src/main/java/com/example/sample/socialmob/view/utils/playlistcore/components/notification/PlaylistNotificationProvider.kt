package com.example.sample.socialmob.view.utils.playlistcore.components.notification

import android.app.Notification
import android.app.Service
import android.support.v4.media.session.MediaSessionCompat
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaInfo

/**
 * Handles the creation and presentation of the Media notification
 * for the Playlist.
 */
interface PlaylistNotificationProvider {
    fun buildNotification(info: MediaInfo, mediaSession: MediaSessionCompat, serviceClass: Class<out Service>) : Notification
}