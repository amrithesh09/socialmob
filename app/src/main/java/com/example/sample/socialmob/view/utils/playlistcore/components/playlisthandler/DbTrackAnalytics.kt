package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DbTrackAnalytics {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTrackAnalytics(list: MutableList<PodCastAnalytics>)

    @Query("DELETE FROM podCastAnalytics")
    fun deleteAllTrackAnalytics()

    @Query("SELECT * FROM podCastAnalytics")
    fun getAllTrackAnalytics(): LiveData<List<PodCastAnalytics>>
}