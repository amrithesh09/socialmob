package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

//class DbTrackProgress {
//
//}

@Dao
interface DbTrackProgress {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDbTrackProgress(list: MutableList<PodCastProgress>)

    @Query("DELETE FROM podCastProgress")
    fun deleteAllDbTrackProgress()

    @Query("SELECT * FROM podCastProgress")
    fun getAllDbTrackProgress(): LiveData<List<PodCastProgress>>

    @Query("SELECT * FROM podCastProgress where episodeId=:episodeid")
    fun getPodCastProgress(episodeid: String): List<PodCastProgress>
}
