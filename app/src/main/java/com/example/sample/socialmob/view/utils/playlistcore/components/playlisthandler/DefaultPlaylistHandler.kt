package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.sample.socialmob.R
import com.example.sample.socialmob.model.music.PodcastHistoryTrack
import com.example.sample.socialmob.repository.dao.DbPodcastHistoryDao
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.MyApplication.Companion.application
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.events.EventMusicPlayError
import com.example.sample.socialmob.view.utils.music.data.MediaItem
import com.example.sample.socialmob.view.utils.playlistcore.api.MediaPlayerApi
import com.example.sample.socialmob.view.utils.playlistcore.api.PlaylistItem
import com.example.sample.socialmob.view.utils.playlistcore.components.audiofocus.AudioFocusProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.audiofocus.DefaultAudioFocusProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.image.ImageProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.mediacontrols.DefaultMediaControlsProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.mediacontrols.MediaControlsProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.mediasession.DefaultMediaSessionProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.mediasession.MediaSessionProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.notification.DefaultPlaylistNotificationProvider
import com.example.sample.socialmob.view.utils.playlistcore.components.notification.PlaylistNotificationProvider
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaInfo
import com.example.sample.socialmob.view.utils.playlistcore.data.MediaProgress
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaylistItemChange
import com.example.sample.socialmob.view.utils.playlistcore.listener.MediaStatusListener
import com.example.sample.socialmob.view.utils.playlistcore.listener.ProgressListener
import com.example.sample.socialmob.view.utils.playlistcore.listener.ServiceCallbacks
import com.example.sample.socialmob.view.utils.playlistcore.manager.BasePlaylistManager
import com.example.sample.socialmob.view.utils.playlistcore.util.MediaProgressPoll
import com.example.sample.socialmob.view.utils.playlistcore.util.SafeWifiLock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


@Suppress("MemberVisibilityCanPrivate")
open class DefaultPlaylistHandler<I : PlaylistItem, out M : BasePlaylistManager<I>> protected constructor(
        protected val context: Context,
        protected val serviceClass: Class<out Service>,
        protected val playlistManager: M,
        protected val imageProvider: ImageProvider<I>,
        protected val notificationProvider: PlaylistNotificationProvider,
        protected val mediaSessionProvider: MediaSessionProvider,
        protected val mediaControlsProvider: MediaControlsProvider,
        protected val audioFocusProvider: AudioFocusProvider<I>,
        var listener: Listener<I>?


) : PlaylistHandler<I>(playlistManager.mediaPlayers), ProgressListener, MediaStatusListener<I> {
    var podcastAnalyticsList: MutableList<PodCastAnalytics>? = ArrayList()
    var trackAnalyticsData: MutableList<TrackAnalyticsData>? = ArrayList()
    private var mStartTime = ""
    private var mEndTime = ""

    companion object {
        const val TAG = "DefaultPlaylistHandler"


        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)!!
        var dbPodcastTrackAnalyticsDao: DbTrackAnalytics = habitRoomDatabase.trackAnalytics()
        var dbPodcastHistoryDao: DbPodcastHistoryDao = habitRoomDatabase.dbPodcastHistory()
        var dbTrackAnalyticsDaoIdDurationTimestamp: TrackAnalytics =
                habitRoomDatabase.musicTrackAnalytics()
        var dbPddcastTrackProgressDaoIdPercentageMillies: DbTrackProgress =
                habitRoomDatabase.trackProgress()
        var podcastProgress: MutableList<PodCastProgress>? = ArrayList()
        var podcastAnalytics: LiveData<List<PodCastAnalytics>> =
                dbPodcastTrackAnalyticsDao.getAllTrackAnalytics()
        var trackAnalytics: LiveData<List<TrackAnalyticsData>> =
                dbTrackAnalyticsDaoIdDurationTimestamp.getAllTrackAnalytics()
    }

    interface Listener<I : PlaylistItem> {
        fun onMediaPlayerChanged(oldPlayer: MediaPlayerApi<I>?, newPlayer: MediaPlayerApi<I>?)
        fun onItemSkipped(item: I)
    }

    private val mediaInfo = MediaInfo()
    private val wifiLock = SafeWifiLock(context)

    private var mediaProgressPoll = MediaProgressPoll<I>()

    private val notificationManager: NotificationManager by lazy {
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    }

    protected lateinit var serviceCallbacks: ServiceCallbacks

    /**
     * Retrieves the ID to use for the notification and registering this
     * service as Foreground when media is playing
     */
    protected open val notificationId: Int
        get() = R.id.playlistcore_default_notification_id

    /**
     * Determines if media is currently playing
     */
    protected open val isPlaying: Boolean
        get() = currentMediaPlayer?.isPlaying ?: false

    protected open val isLoading: Boolean
        get() {
            return currentPlaybackState == PlaybackState.RETRIEVING ||
                    currentPlaybackState == PlaybackState.PREPARING ||
                    currentPlaybackState == PlaybackState.SEEKING
        }

    var currentPlaylistItem: I? = null

    private var pausedForSeek = false
    private var playingBeforeSeek = false

    private var startPaused = false
    private var seekToPosition: Long = -1

    private var sequentialErrors: Int = 0

    init {
        audioFocusProvider.setPlaylistHandler(this)
    }

    override fun setup(serviceCallbacks: ServiceCallbacks) {
        this.serviceCallbacks = serviceCallbacks

        mediaProgressPoll.progressListener = this
        playlistManager.playlistHandler = this
    }

    override fun tearDown() {
        setPlaybackState(PlaybackState.STOPPED)

        relaxResources()
        playlistManager.playlistHandler = null

        mediaInfo.clear()
    }

    override fun play() {

        if (!isPlaying) {
            currentMediaPlayer?.play()
        }

        mediaProgressPoll.start()
        setPlaybackState(PlaybackState.PLAYING)

        setupForeground()
        audioFocusProvider.requestFocus()


        // TODO : Add to Radio Play History
        if (MyApplication.playlistManager != null &&
                MyApplication.playlistManager?.currentItemChange != null &&
                MyApplication.playlistManager?.currentItemChange?.currentItem != null
        ) {
            val currentItemNew = MyApplication.playlistManager?.currentItemChange?.currentItem



            if (currentItemNew?.thumbnailUrl != null &&
                    currentItemNew.thumbnailUrl?.contains("podcast")!!
            ) {
                doAsync {
                    val mPodcastHistoryTrackList: MutableList<PodcastHistoryTrack>? = ArrayList()
                    val mPodcastHistoryTrack =
                            PodcastHistoryTrack(
                                    currentItemNew.id.toString(),
                                    currentItemNew.title!!,
                                    currentItemNew.mediaUrl!!,
                                    currentItemNew.album!!,
                                    "",
                                    "",
                                    "",
                                    currentItemNew.thumbnailUrl!!,
                                    currentItemNew.mExtraId!!,
                                    currentItemNew.mExtraIdPodCast!!,
                                    "false",
                                    System.currentTimeMillis().toString(),
                                    currentItemNew.playCount
                            )

                    if (!currentItemNew.thumbnailUrl?.contains("stations")!!) {
                        mPodcastHistoryTrackList?.add(mPodcastHistoryTrack)
                        dbPodcastHistoryDao.insertDbPodcastHistory(mPodcastHistoryTrackList!!)
                    }
                }
            } else {
                callApiRecentPlayedTrack(MyApplication.playlistManager?.currentItem?.mExtraId)
            }
        }
    }

    private fun callApiRecentPlayedTrack(id: String?) {
        if (InternetUtil.isInternetOn()) {
            if (id != null && id != "") {
                val fullData = RemoteConstant.getEncryptedString(
                        SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
                        SharedPrefsUtils.getStringPreference(application, RemoteConstant.mApiKey, "")!!,
                        RemoteConstant.mBaseUrl + "/api/v2/track/analytics/lastplayed?trackId=" + id.toString(),
                        RemoteConstant.postMethod
                )
                val mAuth =
                        RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                                application,
                                RemoteConstant.mAppId,
                                ""
                        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


                GlobalScope.launch(Dispatchers.IO) {
                    WebServiceClient.client.create(BackEndApi::class.java)
                            .recentPlayedTrack(mAuth, id.toString())
                }
            }
        }
    }

    override fun pause(transient: Boolean) {
        if (isPlaying) {
            currentMediaPlayer?.pause()
        }

        mediaProgressPoll.stop()
        setPlaybackState(PlaybackState.PAUSED)
        serviceCallbacks.endForeground(false)

        if (!transient) {
            audioFocusProvider.abandonFocus()
        }
    }

    override fun togglePlayPause() {
        if (isPlaying) {
            pause(false)
        } else {
            play()
        }
    }

    override fun stop() {
        currentMediaPlayer?.stop()

        setPlaybackState(PlaybackState.STOPPED)
        currentPlaylistItem?.let {
            playlistManager.playbackStatusListener?.onItemPlaybackEnded(it)
        }

        // let go of all resources
        relaxResources()

        playlistManager.reset()
        serviceCallbacks.stop()
    }

    override fun next() {
        val mRepeatMode =
                SharedPrefsUtils.getStringPreference(application, RemoteConstant.mRepeatStatus, "0")
                        ?.toInt()

        when (mRepeatMode) {
            RemoteConstant.REPEAT_MODE_NONE -> {
                playlistManager.next()
                startItemPlayback(0, !isPlaying)
            }

            RemoteConstant.REPEAT_MODE_THIS -> {
                try {
//                    if (playlistManager.currentPosition == 0) {
//                        playlistManager.currentPosition = 0
//                        playlistManager.play(0, false)
//                    } else {
//                        playlistManager.currentPosition = playlistManager.currentPosition.minus(1)
//                        playlistManager.play(0, false)
//                    }
                    playlistManager.play(0, false)

                } catch (e: Exception) {
                    stop()
                }
            }
            RemoteConstant.REPEAT_MODE_ALL -> {
                playlistManager.next()
                startItemPlayback(0, !isPlaying)
            }
        }


        mEndTime = System.currentTimeMillis().toString()
        SaveAnaytics(MyApplication.playlistManager?.currentItem)


    }

    @SuppressLint("StaticFieldLeak")
    private fun SaveAnaytics(currentItemNew: MediaItem?) {
        if (mEndTime != "" && mStartTime != "") {

            if (currentItemNew != null &&
                    currentItemNew.thumbnailUrl != null
            ) {
                // TODO : Podcast analytics
                if (currentItemNew != null &&
                        currentItemNew.thumbnailUrl != null &&
                        currentItemNew.thumbnailUrl?.contains("podcast")!!
                ) {

                    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
                    sdf.timeZone = TimeZone.getTimeZone("UTC")
                    println(sdf.format(Date()))
                    val formattedDate = sdf.format(Date())

                    doAsync {
                        val mTime: Long = getDateDiff(
                                mStartTime.toLong(),
                                mEndTime.toLong(),
                                TimeUnit.SECONDS
                        )

                        val mAnalytics = PodCastAnalytics(
                                System.currentTimeMillis().toInt(),
                                currentItemNew.mExtraId.toString(),
                                currentItemNew.mExtraIdPodCast.toString(),
                                mTime.toString(),
                                formattedDate
                        )
                        podcastAnalyticsList!!.add(mAnalytics)
                        dbPodcastTrackAnalyticsDao.insertTrackAnalytics(podcastAnalyticsList!!)

                        mStartTime = ""
                        mEndTime = ""
                    }

                    doAsync {
                        // TODO : TO get current play percentage

                        var mAnalytics: PodCastProgress? = null
                        var mMusicAnalytics: TrackAnalyticsData? = null

                        val mDuration =
                                MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.duration?.toInt()
                        var mCurrentPosition =
                                MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.currentPosition?.toInt()
                        var mProgress = mCurrentPosition ?: 0 * 100 / mDuration!!

                        val podCastProgress =
                                dbPddcastTrackProgressDaoIdPercentageMillies.getPodCastProgress(
                                        MyApplication.playlistManager?.currentItem?.id.toString()
                                )
                        if (podCastProgress.isNotEmpty()) {
                            if (podCastProgress[0].mPercentage.toInt() > mProgress) {
                                mProgress = podCastProgress[0].mPercentage.toInt()
                                mCurrentPosition = if (mProgress >= 97) {
                                    0
                                } else {
                                    podCastProgress[0].mMillisecond.toInt()
                                }
                                mAnalytics = PodCastProgress(
                                        MyApplication.playlistManager?.currentItem?.id.toString(),
                                        mProgress.toString(), mCurrentPosition.toString()
                                )
                                podcastProgress!!.add(mAnalytics)
                                dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                        podcastProgress!!
                                )
                            } else {
                                mAnalytics = PodCastProgress(
                                        MyApplication.playlistManager?.currentItem?.id.toString(),
                                        mProgress.toString(), mCurrentPosition.toString()
                                )
                                podcastProgress!!.add(mAnalytics)
                                dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                        podcastProgress!!
                                )
                            }
                        } else {
                            mAnalytics = PodCastProgress(
                                    MyApplication.playlistManager?.currentItem?.id.toString(),
                                    mProgress.toString(), mCurrentPosition.toString()
                            )
                            podcastProgress!!.add(mAnalytics)
                            dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                    podcastProgress!!
                            )
                        }
                    }


                } else {
                    try {// TODO : For track analytics
                        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
                        sdf.timeZone = TimeZone.getTimeZone("UTC")
                        println(sdf.format(Date()))
                        val formattedDate = sdf.format(Date())

                        if (mStartTime != "" &&
                                mEndTime != ""
                        ) {
                            doAsync {
                                val mTime: Long = getDateDiff(
                                        mStartTime.toLong(),
                                        mEndTime.toLong(),
                                        TimeUnit.SECONDS
                                )
                                var isOffline = "false"
                                val mediaUrl: String =
                                        MyApplication.playlistManager?.currentItem?.mediaUrl!!
                                if (mediaUrl.contains("/storage/emulated/")) {
                                    isOffline = "true"
                                } else {
                                    isOffline = "false"
                                }
                                val mAnalytics = TrackAnalyticsData(
                                        System.currentTimeMillis().toInt(),
                                        MyApplication.playlistManager?.currentItemChange?.currentItem?.mExtraId.toString(),
                                        mTime.toString(),
                                        formattedDate, isOffline
                                )
                                trackAnalyticsData!!.add(mAnalytics)
                                dbTrackAnalyticsDaoIdDurationTimestamp.insertTrackAnalytics(
                                        trackAnalyticsData!!
                                )

                                mStartTime = ""
                                mEndTime = ""
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    override fun previous() {
        mEndTime = System.currentTimeMillis().toString()
        SaveAnaytics(MyApplication.playlistManager?.currentItem)
        playlistManager.previous()
        startItemPlayback(0, !isPlaying)
    }

    override fun startSeek() {
        if (isPlaying) {
            pausedForSeek = true
            pause(true)
        }
    }

    override fun seek(positionMillis: Long) {
        performSeek(positionMillis)
    }

    override fun onPrepared(mediaPlayer: MediaPlayerApi<I>) {
        startMediaPlayer(mediaPlayer)
        sequentialErrors = 0
    }

    override fun onBufferingUpdate(mediaPlayer: MediaPlayerApi<I>, percent: Int) {
        //Makes sure to update listeners of buffer updates even when playback is paused
        if (!mediaPlayer.isPlaying && currentMediaProgress.bufferPercent != percent) {
            currentMediaProgress.update(mediaPlayer.currentPosition, percent, mediaPlayer.duration)
            onProgressUpdated(currentMediaProgress)
        }
    }

    override fun onSeekComplete(mediaPlayer: MediaPlayerApi<I>) {
        if (pausedForSeek || playingBeforeSeek) {
            play()
            pausedForSeek = false
            playingBeforeSeek = false
        } else {
            pause(false)
        }
    }

    override fun onCompletion(mediaPlayer: MediaPlayerApi<I>) {
        // Handles moving to the next playable item
        mEndTime = System.currentTimeMillis().toString()
        SaveAnaytics(MyApplication.playlistManager?.currentItem)
        next()
        startPaused = false
    }

    override fun onError(mediaPlayer: MediaPlayerApi<I>): Boolean {
        // Unless we've had 3 or more errors without an item successfully playing we will move to the next item
        if (++sequentialErrors <= 3) {
//            next()
            EventBus.getDefault()
                    .post(EventMusicPlayError(""))
            return false
        }

        setPlaybackState(PlaybackState.ERROR)

        serviceCallbacks.endForeground(true)
        wifiLock.release()
        mediaProgressPoll.stop()

        audioFocusProvider.abandonFocus()
        return false
    }

    /**
     * When the current media progress is updated we call through the
     * [BasePlaylistManager] to inform any listeners of the change
     */
    override fun onProgressUpdated(mediaProgress: MediaProgress): Boolean {
        currentMediaProgress = mediaProgress
        return playlistManager.onProgressUpdated(mediaProgress)
    }

    protected open fun setupForeground() {
        serviceCallbacks.runAsForeground(
                notificationId,
                notificationProvider.buildNotification(
                        mediaInfo,
                        mediaSessionProvider.get(),
                        serviceClass
                )
        )
    }

    /**
     * Performs the functionality to seek the current media item
     * to the specified position.  This should only be called directly
     * when performing the initial setup of playback position.  For
     * normal seeking process use the [.performSeekStarted] in
     * conjunction with [.performSeekEnded]
     *
     * @param position The position to seek to in milliseconds
     * @param updatePlaybackState True if the playback state should be updated
     */
    protected open fun performSeek(position: Long, updatePlaybackState: Boolean = true) {
        playingBeforeSeek = isPlaying
        currentMediaPlayer?.seekTo(position)

        if (updatePlaybackState) {
            setPlaybackState(PlaybackState.SEEKING)
        }
    }

    protected open fun initializeMediaPlayer(mediaPlayer: MediaPlayerApi<I>) {
        mediaPlayer.apply {
            reset()
            setMediaStatusListener(this@DefaultPlaylistHandler)
        }

        mediaProgressPoll.update(mediaPlayer)
        mediaProgressPoll.reset()
    }

    protected open fun updateMediaInfo() {
        // Generate the notification state
        mediaInfo.mediaState.isPlaying = isPlaying
        mediaInfo.mediaState.isLoading = isLoading
        mediaInfo.mediaState.isNextEnabled = playlistManager.isNextAvailable
        mediaInfo.mediaState.isPreviousEnabled = playlistManager.isPreviousAvailable

        // Updates the notification information
        mediaInfo.notificationId = notificationId
        mediaInfo.playlistItem = currentPlaylistItem

        mediaInfo.appIcon = imageProvider.notificationIconRes

        // TODO : Fatal Exception: android.app.RemoteServiceException
        if (imageProvider.remoteViewArtwork != null) {
            mediaInfo.artwork = imageProvider.remoteViewArtwork
        }
        if (imageProvider.largeNotificationImage != null) {
            mediaInfo.largeNotificationIcon = imageProvider.largeNotificationImage
        }
    }

    override fun updateMediaControls() {
        if (currentPlaylistItem == null) {
            return
        }

        updateMediaInfo()
        mediaSessionProvider.update(mediaInfo)
        mediaControlsProvider.update(mediaInfo, mediaSessionProvider.get())
        // Updates the notification
        try {
            notificationManager.notify(
                    mediaInfo.notificationId,
                    notificationProvider.buildNotification(
                            mediaInfo,
                            mediaSessionProvider.get(),
                            serviceClass
                    )
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun refreshCurrentMediaPlayer() {
        refreshCurrentMediaPlayer(currentMediaPlayer?.currentPosition ?: seekToPosition, !isPlaying)
    }

    protected open fun refreshCurrentMediaPlayer(seekPosition: Long, startPaused: Boolean) {
        currentPlaylistItem.let {
            seekToPosition = seekPosition
            this.startPaused = startPaused

            updateCurrentMediaPlayer(it)
            if (!play(currentMediaPlayer, it)) {
                next()
            }
        }
    }

    override fun onRemoteMediaPlayerConnectionChange(
            mediaPlayer: MediaPlayerApi<I>,
            state: MediaPlayerApi.RemoteConnectionState
    ) {
        // If the mediaPlayer that changed state is of lower priority than the current one we ignore the change
        currentMediaPlayer?.let {
            if (mediaPlayers.indexOf(it) < mediaPlayers.indexOf(mediaPlayer)) {
                Log.d(
                        TAG,
                        "Ignoring remote connection state change for $mediaPlayer because it is of lower priority than the current MediaPlayer"
                )
                return
            }
        }

        when (state) {
            MediaPlayerApi.RemoteConnectionState.CONNECTING -> {
                if (mediaPlayer != currentMediaPlayer) {
                    val resumePlayback = isPlaying
                    pause(true)
                    seekToPosition = currentMediaPlayer?.currentPosition ?: seekToPosition
                    startPaused = !resumePlayback
                }
                return
            }
            MediaPlayerApi.RemoteConnectionState.CONNECTED -> {
                if (mediaPlayer != currentMediaPlayer) {
                    refreshCurrentMediaPlayer(currentMediaProgress.position, startPaused)
                }
            }
            MediaPlayerApi.RemoteConnectionState.NOT_CONNECTED -> {
                if (mediaPlayer == currentMediaPlayer) {
                    refreshCurrentMediaPlayer(currentMediaProgress.position, startPaused)
                }
            }
        }
    }

    /**
     * Releases resources used by the service for playback. This includes the "foreground service"
     * status and notification, the wake locks, and the audioPlayer if requested
     */
    // TODO : CHECK NOTIFICATION OVERLAP
    protected open fun relaxResources() {
        mediaProgressPoll.release()
        currentMediaPlayer = null

        audioFocusProvider.abandonFocus()
        wifiLock.release()
        serviceCallbacks.endForeground(true)

        notificationManager.cancel(notificationId)
        mediaSessionProvider.get().release()
    }

    override fun startItemPlayback(positionMillis: Long, startPaused: Boolean) {
        this.seekToPosition = positionMillis
        this.startPaused = startPaused

        playlistManager.playbackStatusListener?.onItemPlaybackEnded(currentPlaylistItem)
        currentPlaylistItem = getNextPlayableItem()

        currentPlaylistItem.let {
            updateCurrentMediaPlayer(it)
            mediaItemChanged(it)

            if (play(currentMediaPlayer, it)) {
                return
            }
        }

        //If the playback wasn't handled, attempt to seek to the next playable item, otherwise stop the service
        if (playlistManager.isNextAvailable) {
            next()
        } else {
            try {
                // TODO : Check repeat status when next track not availabel
                val mRepeatMode =
                        SharedPrefsUtils.getStringPreference(
                                application,
                                RemoteConstant.mRepeatStatus,
                                "0"
                        )
                                ?.toInt()

                when (mRepeatMode) {

                    RemoteConstant.REPEAT_MODE_THIS -> {
                        try {
                            if (playlistManager.currentPosition == 0) {
                                playlistManager.currentPosition = 0
                                playlistManager.play(0, false)
                            } else {
                                playlistManager.currentPosition =
                                        playlistManager.currentPosition.minus(1)
                                playlistManager.play(0, false)
                            }


                        } catch (e: Exception) {
                            stop()
                        }
                    }
                    else -> {
                        // TODO : else start repeat track list
                        playlistManager.currentPosition = 0
                        playlistManager.play(0, false)
                    }
                }
            } catch (e: Exception) {
                stop()
            }

        }
    }

    protected open fun updateCurrentMediaPlayer(item: I?) {
        if (mediaPlayers.isEmpty()) {
            Log.d(TAG, "No media players available, stopping service")
            stop()
        }

        val newMediaPlayer = item?.let { getMediaPlayerForItem(it) }
        if (newMediaPlayer != currentMediaPlayer) {
            listener?.onMediaPlayerChanged(currentMediaPlayer, newMediaPlayer)
            currentMediaPlayer?.stop()
        }

        currentMediaPlayer = newMediaPlayer

    }

    protected open fun getMediaPlayerForItem(item: I): MediaPlayerApi<I>? {
        // We prioritize players higher in the list over the currentMediaPlayer
        return mediaPlayers.firstOrNull { it.handlesItem(item) }
    }

    /**
     * Starts the actual playback of the specified audio item.
     *
     * @return True if the item playback was correctly handled
     */
    protected open fun play(mediaPlayer: MediaPlayerApi<I>?, item: I?): Boolean {
        if (mediaPlayer == null || item == null) {
            return false
        }


        initializeMediaPlayer(mediaPlayer)
        audioFocusProvider.requestFocus()

        mediaPlayer.playItem(item)

        setupForeground()
        setPlaybackState(PlaybackState.PREPARING)

        wifiLock.update(!(currentPlaylistItem?.downloaded ?: true))
        return true
    }

    /**
     * Reconfigures the mediaPlayerApi according to audio focus settings and starts/restarts it. This
     * method starts/restarts the mediaPlayerApi respecting the current audio focus state. So if
     * we have focus, it will play normally; if we don't have focus, it will either leave the
     * mediaPlayerApi paused or set it to a low volume, depending on what is allowed by the
     * current focus settings.
     */
    protected open fun startMediaPlayer(mediaPlayer: MediaPlayerApi<I>) {
        //Seek to the correct position
        val seekRequested = seekToPosition > 0
        if (seekRequested) {
            performSeek(seekToPosition, false)
            seekToPosition = -1
        }

        //Start the playback only if requested, otherwise update the state to paused
        mediaProgressPoll.start()
        if (!mediaPlayer.isPlaying && !startPaused) {
            pausedForSeek = seekRequested
            play()
            playlistManager.playbackStatusListener?.onMediaPlaybackStarted(
                    currentPlaylistItem!!,
                    mediaPlayer.currentPosition,
                    mediaPlayer.duration
            )
        } else {
            setPlaybackState(PlaybackState.PAUSED)
        }

        audioFocusProvider.refreshFocus()
    }

    /**
     * Iterates through the playList, starting with the current item, until we reach an item we can play.
     * Normally this will be the current item, however if they don't have network then
     * it will be the next downloaded item.
     */
    protected open fun getNextPlayableItem(): I? {
        var item = playlistManager.currentItem
        while (item != null && getMediaPlayerForItem(item) == null) {
            listener?.onItemSkipped(item)
            item = playlistManager.next()
        }

        //If we are unable to get a next playable item, inform the listener we are at the end of the playlist
        item ?: playlistManager.playbackStatusListener?.onPlaylistEnded()
        return item
    }

    /**
     * Called when the current media item has changed, this will update the notification and
     * media control values.
     */
    protected open fun mediaItemChanged(item: I?) {
        //Validates that the currentPlaylistItem is for the currentItem
        if (!playlistManager.isPlayingItem(item)) {
            Log.d(TAG, "forcing currentPlaylistItem update")
            currentPlaylistItem = playlistManager.currentItem
        }

        item?.let {
            imageProvider.updateImages(it)
        }

        currentItemChange =
                PlaylistItemChange(
                        item,
                        playlistManager.isPreviousAvailable,
                        playlistManager.isNextAvailable
                ).apply {
                    playlistManager.onPlaylistItemChanged(currentItem, hasNext, hasPrevious)
                }
    }

    /**
     * Updates the current PlaybackState and informs any listening classes.
     *
     * @param state The new PlaybackState
     */
    protected open fun setPlaybackState(state: PlaybackState) {
        currentPlaybackState = state
        playlistManager.onPlaybackStateChanged(state)

        SharedPrefsUtils.setStringPreference(application, RemoteConstant.mState, "" + state)

        if (state.toString() == "PLAYING" || state.toString() == "PAUSED" || state.toString() == "STOPPED" || state.toString() == "PREPARING") {
            if (MyApplication.playlistManager != null &&
                    MyApplication.playlistManager?.currentItem != null &&
                    MyApplication.playlistManager?.currentItem?.thumbnailUrl != null &&
                    MyApplication.playlistManager?.currentItem?.thumbnailUrl?.contains("podcast")!!
            ) {


                doAsync {
                    // TODO : TO get current play percentage

                    var mAnalytics: PodCastProgress? = null
                    var mProgress: Int
                    var mMusicAnalytics: TrackAnalyticsData? = null

                    val mDuration =
                            MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.duration?.toInt()
                    var mCurrentPosition =
                            MyApplication.playlistManager?.playlistHandler?.currentMediaPlayer?.currentPosition?.toInt()
                    mProgress = try {
                        mCurrentPosition!! * 100 / mDuration!!
                    } catch (e: Exception) {
                        0
                    }

                    val podCastProgress =
                            dbPddcastTrackProgressDaoIdPercentageMillies.getPodCastProgress(
                                    MyApplication.playlistManager?.currentItem?.id.toString()
                            )
                    if (podCastProgress.isNotEmpty()) {
                        if (podCastProgress[0].mPercentage.toInt() > mProgress) {
                            mProgress = podCastProgress[0].mPercentage.toInt()
                            mCurrentPosition = if (mProgress >= 98) {
                                0
                            } else {
                                podCastProgress[0].mMillisecond.toInt()
                            }
                            mAnalytics = PodCastProgress(
                                    MyApplication.playlistManager?.currentItem?.id.toString(),
                                    mProgress.toString(), mCurrentPosition.toString()
                            )
                            podcastProgress!!.add(mAnalytics)
                            dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                    podcastProgress!!
                            )
                        } else {
                            mAnalytics = PodCastProgress(
                                    MyApplication.playlistManager?.currentItem?.id.toString(),
                                    mProgress.toString(), mCurrentPosition.toString()
                            )
                            podcastProgress!!.add(mAnalytics)
                            dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                    podcastProgress!!
                            )
                        }
                    } else {
                        mAnalytics = PodCastProgress(
                                MyApplication.playlistManager?.currentItem?.id.toString(),
                                mProgress.toString(), mCurrentPosition.toString()
                        )
                        podcastProgress!!.add(mAnalytics)
                        dbPddcastTrackProgressDaoIdPercentageMillies.insertDbTrackProgress(
                                podcastProgress!!
                        )
                    }
                }
            }

        }




        if (state.toString() == "PLAYING") {
//            println(">>>>>>>>>PLAYING"+MyApplication.playlistManager?.currentItem?.id)
            mStartTime = System.currentTimeMillis().toString()

        }

        if (mStartTime != "" && state.toString() == "PAUSED") {
            mEndTime = System.currentTimeMillis().toString()
        }

        // TODO : Play from list click( PREPARING) - ITEM - CLICK
        if (state.toString() == "PREPARING") {
            if (mStartTime != "") {
//                println(">>>>>>>>>PREPARING" + MyApplication.playlistManager?.currentItem?.id)
                mEndTime = System.currentTimeMillis().toString()
                SaveAnaytics(MyApplication.playlistManager?.currentItem)
            }
        }

        if (mEndTime != "" && mStartTime != "") {

            if (MyApplication.playlistManager != null &&
                    MyApplication.playlistManager?.currentItem != null &&
                    MyApplication.playlistManager?.currentItem?.thumbnailUrl != null
            ) {
                // TODO : Podcast analytics
                if (MyApplication.playlistManager != null &&
                        MyApplication.playlistManager?.currentItem != null &&
                        MyApplication.playlistManager?.currentItem?.thumbnailUrl != null &&
                        MyApplication.playlistManager?.currentItem?.thumbnailUrl?.contains("podcast")!!
                ) {

                    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
                    sdf.timeZone = TimeZone.getTimeZone("UTC")
                    println(sdf.format(Date()))
                    val formattedDate = sdf.format(Date())

                    doAsync {
                        val mTime: Long =
                                getDateDiff(mStartTime.toLong(), mEndTime.toLong(), TimeUnit.SECONDS)

                        val mAnalytics = PodCastAnalytics(
                                System.currentTimeMillis().toInt(),
                                MyApplication.playlistManager?.currentItem?.mExtraId.toString(),
                                MyApplication.playlistManager?.currentItem?.mExtraIdPodCast.toString(),
                                mTime.toString(),
                                formattedDate
                        )
                        podcastAnalyticsList!!.add(mAnalytics)
                        dbPodcastTrackAnalyticsDao.insertTrackAnalytics(podcastAnalyticsList!!)
                        mStartTime = ""
                        mEndTime = ""
                    }
                } else {
                    // TODO : For track analytics
                    if (mStartTime != "") {
                        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
                        sdf.timeZone = TimeZone.getTimeZone("UTC")
                        println(sdf.format(Date()))
                        val formattedDate = sdf.format(Date())

                        doAsync {
                            val mTime: Long = getDateDiff(
                                    mStartTime.toLong(),
                                    mEndTime.toLong(),
                                    TimeUnit.SECONDS
                            )
                            var isOffline = "false"
                            val mediaUrl: String =
                                    MyApplication.playlistManager?.currentItem?.mediaUrl!!
                            if (mediaUrl.contains("/storage/emulated/")) {
                                isOffline = "true"
                            } else {
                                isOffline = "false"
                            }
                            val mAnalytics = TrackAnalyticsData(
                                    System.currentTimeMillis().toInt(),
                                    MyApplication.playlistManager?.currentItemChange?.currentItem?.mExtraId.toString(),
                                    mTime.toString(),
                                    formattedDate, isOffline
                            )
                            trackAnalyticsData!!.add(mAnalytics)
                            dbTrackAnalyticsDaoIdDurationTimestamp.insertTrackAnalytics(
                                    trackAnalyticsData!!
                            )
                            mStartTime = ""
                            mEndTime = ""
                        }
                    }
                }
            }
        }
        trackAnalytics.observeForever {
            for (i in 0 until it!!.size) {
                println("mTrackId--" + it[i].episodeId + "--PlayTime--" + it[i].duration + "--TimeStamp--" + it[i].timestamp + "--offline--" + it[i].offline)
            }
        }

        // Makes sure the Media Controls are up-to-date
        if (state != PlaybackState.STOPPED && state != PlaybackState.ERROR) {
            updateMediaControls()
        }
    }

    private fun getDateDiff(timeUpdate: Long, timeNow: Long, timeUnit: TimeUnit): Long {
        val diffInMillis: Long = Math.abs(timeNow - timeUpdate)
        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS)
    }

    open class Builder<I : PlaylistItem, out M : BasePlaylistManager<I>>(
            protected val context: Context,
            protected val serviceClass: Class<out Service>,
            protected val playlistManager: M,
            protected val imageProvider: ImageProvider<I>
    ) {
        var notificationProvider: PlaylistNotificationProvider? = null
        var mediaSessionProvider: MediaSessionProvider? = null
        var mediaControlsProvider: MediaControlsProvider? = null
        var audioFocusProvider: AudioFocusProvider<I>? = null
        var listener: Listener<I>? = null

        fun build(): DefaultPlaylistHandler<I, M> {
            return DefaultPlaylistHandler(
                    context,
                    serviceClass,
                    playlistManager,
                    imageProvider,
                    notificationProvider ?: DefaultPlaylistNotificationProvider(context),
                    mediaSessionProvider ?: DefaultMediaSessionProvider(context, serviceClass),
                    mediaControlsProvider ?: DefaultMediaControlsProvider(context),
                    audioFocusProvider ?: DefaultAudioFocusProvider(context),
                    listener
            )


        }
    }

}