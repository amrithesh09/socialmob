package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "podCastAnalytics")
data class PodCastAnalytics(
    @PrimaryKey(autoGenerate = true) var mId: Int,
    @ColumnInfo(name = "episodeId") var episodeId: String,
    @ColumnInfo(name = "podcastId") var podcastId: String,
    @ColumnInfo(name = "duration") var duration: String,
    @ColumnInfo(name = "timestamp") var timestamp: String
)