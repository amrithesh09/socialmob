package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "podCastProgress")
data class PodCastProgress(
    @PrimaryKey
    @ColumnInfo(name = "episodeId") var episodeId: String,
    @ColumnInfo(name = "percentage") var mPercentage: String,
    @ColumnInfo(name = "millisecond") var mMillisecond: String
)