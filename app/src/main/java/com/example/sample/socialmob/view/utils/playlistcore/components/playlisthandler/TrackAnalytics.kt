package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

//class TrackAnalytics {
//
//}
@Dao
interface TrackAnalytics {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTrackAnalytics(list: MutableList<TrackAnalyticsData>)

    @Query("DELETE FROM trackAnalyticsData")
    fun deleteAllTrackAnalytics()

    @Query("SELECT * FROM trackAnalyticsData")
    fun getAllTrackAnalytics(): LiveData<List<TrackAnalyticsData>>
}