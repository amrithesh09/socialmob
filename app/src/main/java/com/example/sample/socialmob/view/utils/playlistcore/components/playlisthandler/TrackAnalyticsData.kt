package com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//class TrackAnalyticsData {
//
//}
@Entity(tableName = "trackAnalyticsData")
data class TrackAnalyticsData(
    @PrimaryKey(autoGenerate = true) var mId: Int,
    @ColumnInfo(name = "episodeId") var episodeId: String,
    @ColumnInfo(name = "duration") var duration: String,
    @ColumnInfo(name = "timestamp") var timestamp: String,
    @ColumnInfo(name = "offline") var offline: String
)