package com.example.sample.socialmob.view.utils.playlistcore.data

import com.example.sample.socialmob.view.utils.playlistcore.api.PlaylistItem

data class PlaylistItemChange<out T : PlaylistItem>(
        val currentItem: T?,
        val hasPrevious: Boolean,
        val hasNext: Boolean
)