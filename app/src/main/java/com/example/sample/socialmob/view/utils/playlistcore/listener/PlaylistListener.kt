package com.example.sample.socialmob.view.utils.playlistcore.listener

import com.example.sample.socialmob.view.utils.playlistcore.service.BasePlaylistService
import com.example.sample.socialmob.view.utils.playlistcore.api.PlaylistItem
import com.example.sample.socialmob.view.utils.playlistcore.data.PlaybackState

/**
 * A simple callback interface for listening to [BasePlaylistService]
 * changes.
 */
interface PlaylistListener<in T : PlaylistItem> {

    /**
     * Occurs when the currently playing item has changed
     *
     * @return True if the event has been handled
     */
    fun onPlaylistItemChanged(currentItem: T?, hasNext: Boolean, hasPrevious: Boolean): Boolean

    /**
     * Occurs when the current media state changes
     *
     * @return True if the event has been handled
     */
    fun onPlaybackStateChanged(playbackState: PlaybackState): Boolean
}
