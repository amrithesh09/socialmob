package com.example.sample.socialmob.view.utils.playlistcore.listener

import com.example.sample.socialmob.view.utils.playlistcore.data.MediaProgress

interface ProgressListener {

    /**
     * Occurs when the currently playing item has a progress change
     *
     * @return True if the progress update has been handled
     */
    fun onProgressUpdated(mediaProgress: MediaProgress): Boolean
}
