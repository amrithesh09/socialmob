package com.example.sample.socialmob.view.utils.playlistcore.util

import android.os.Handler
import android.os.HandlerThread

/**
 * A method repeater to easily perform update functions on a timed basis.
 * **NOTE:** the duration between repeats may not be exact.  If you require an exact
 * amount of elapsed time use the [StopWatch] instead.
 */
class Repeater {
    companion object {
        private val HANDLER_THREAD_NAME = "Repeater_HandlerThread"
        private val DEFAULT_REPEAT_DELAY = 33 // ~30 fps
    }

    /**
     * Determines if the Repeater is currently running
     *
     * @return True if the repeater is currently running
     */
    @Volatile
    var isRunning = false
        private set

    /**
     * The amount of time (in milliseconds) between repeater events.
     * Defaulted to [DEFAULT_REPEAT_DELAY]
     */
    var repeaterDelay = DEFAULT_REPEAT_DELAY

    private var delayedHandler: Handler? = null
    private var handlerThread: HandlerThread? = null

    private var listener: RepeatListener? = null
    private val pollRunnable = PollRunnable()

    /**
     * @param processOnStartingThread True if the repeating process should be handled on the same thread that created the Repeater
     */
    @JvmOverloads
    constructor(processOnStartingThread: Boolean = true) {
        if (processOnStartingThread) {
            delayedHandler = Handler()
        }
    }

    /**
     * @param handler The Handler to use for the repeating process
     */
    constructor(handler: Handler) {
        delayedHandler = handler
    }

    /**
     * Starts the repeater
     */
    fun start() {
        if (!isRunning) {
            isRunning = true

            if (delayedHandler == null) {
                handlerThread = HandlerThread(HANDLER_THREAD_NAME)
                handlerThread?.start()
                delayedHandler = Handler(handlerThread!!.looper)
            }

            pollRunnable.performPoll()
        }
    }

    /**
     * Stops the repeater
     */
    fun stop() {
        handlerThread?.quit()
        isRunning = false
    }

    /**
     * Sets the listener to be notified for each repeat

     * @param listener The listener or null
     */
    fun setRepeatListener(listener: RepeatListener?) {
        this.listener = listener
    }

    interface RepeatListener {
        fun onRepeat()
    }

    private inner class PollRunnable : Runnable {
        override fun run() {
            listener?.onRepeat()

            if (isRunning) {
                performPoll()
            }
        }

        fun performPoll() {
            delayedHandler?.postDelayed(pollRunnable, repeaterDelay.toLong())
        }
    }
}
