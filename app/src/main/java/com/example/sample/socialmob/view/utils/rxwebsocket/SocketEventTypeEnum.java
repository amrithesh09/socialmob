package com.example.sample.socialmob.view.utils.rxwebsocket;


public enum SocketEventTypeEnum {
    OPEN, CLOSING, CLOSED, FAILURE, MESSAGE
}
