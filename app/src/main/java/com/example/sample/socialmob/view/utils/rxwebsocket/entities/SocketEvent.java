package com.example.sample.socialmob.view.utils.rxwebsocket.entities;


import com.example.sample.socialmob.view.utils.rxwebsocket.SocketEventTypeEnum;

public class SocketEvent {

    private final SocketEventTypeEnum type;

    public SocketEvent(SocketEventTypeEnum type) {
        this.type = type;
    }

    public SocketEventTypeEnum getType() {
        return type;
    }

    @Override
    public String toString() {
        return "SocketEvent{" +
                "type=" + type +
                '}';
    }
}
