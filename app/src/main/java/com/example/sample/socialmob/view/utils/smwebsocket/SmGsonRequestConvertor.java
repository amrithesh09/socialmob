package com.example.sample.socialmob.view.utils.smwebsocket;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.io.IOException;
import java.nio.charset.Charset;

public class SmGsonRequestConvertor<T> implements SmWebSocketConverter<T, String> {
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final Gson gson;
    private final TypeAdapter<T> adapter;

    SmGsonRequestConvertor(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public String convert(T value) throws IOException {
        return adapter.toJson(value);
    }
}
