package com.example.sample.socialmob.view.utils.smwebsocket;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;


import java.io.IOException;
import java.io.StringReader;

public class SmGsonResponseConvertor<T> implements SmWebSocketConverter<String,T> {
    private final Gson           gson;
    private final TypeAdapter<T> adapter;

    SmGsonResponseConvertor(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override public T convert(String value) throws IOException {
        JsonReader jsonReader = gson.newJsonReader(new StringReader(value));
        try {
            return adapter.read(jsonReader);
        } finally {
            jsonReader.close();
        }
    }
}

