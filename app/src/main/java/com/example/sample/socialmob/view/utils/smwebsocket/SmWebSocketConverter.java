package com.example.sample.socialmob.view.utils.smwebsocket;

import java.lang.reflect.Type;

public interface SmWebSocketConverter<F, T> {
    T convert(F value) throws Throwable;

    /** Creates convertor instances based on a type and target usage. */
    abstract class Factory {

        public SmWebSocketConverter<String, ?> responseBodyConverter(Type type) {
            return null;
        }

        public SmWebSocketConverter<?, String> requestBodyConverter(Type type) {
            return null;
        }
    }
}
