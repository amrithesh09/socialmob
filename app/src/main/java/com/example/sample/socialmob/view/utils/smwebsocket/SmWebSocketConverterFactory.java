package com.example.sample.socialmob.view.utils.smwebsocket;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public final class SmWebSocketConverterFactory extends SmWebSocketConverter.Factory {
    public static SmWebSocketConverterFactory create() {
        return create(new Gson());
    }

    public static SmWebSocketConverterFactory create(Gson gson) {
        return new SmWebSocketConverterFactory(gson);
    }

    private final Gson gson;

    private SmWebSocketConverterFactory(Gson gson) {
        if (gson == null) throw new NullPointerException("gson == null");
        this.gson = gson;
    }

    @Override
    public SmWebSocketConverter<String, ?> responseBodyConverter(Type type) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new SmGsonResponseConvertor(gson, adapter);
    }

    @Override
    public SmWebSocketConverter<?, String> requestBodyConverter(Type type) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new SmGsonRequestConvertor<>(gson, adapter);
    }
}
