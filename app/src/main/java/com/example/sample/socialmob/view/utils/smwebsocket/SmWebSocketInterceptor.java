package com.example.sample.socialmob.view.utils.smwebsocket;

public interface SmWebSocketInterceptor {
    String intercept(String data);
}
