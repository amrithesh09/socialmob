package com.example.sample.socialmob.view.utils.stickyheader

import android.content.res.Resources

fun dpToPx(dp: Float): Int {
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}