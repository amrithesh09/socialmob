/*
 *  Copyright (C) Allegion - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *
 */

package com.example.sample.socialmob.view.utils.websocket;

public interface WebSocketInterceptor {
    String intercept(String data);
}
