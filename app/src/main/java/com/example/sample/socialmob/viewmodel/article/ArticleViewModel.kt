package com.example.sample.socialmob.viewmodel.article

import android.app.Application
import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.sample.socialmob.model.article.*
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.model.search.SearchTagResponseModelPayloadTag
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.article.ArticleDashBoardDao
import com.example.sample.socialmob.repository.dao.article.BookMarkedArticleDao
import com.example.sample.socialmob.repository.dao.article.HistoryArticleDao
import com.example.sample.socialmob.repository.dao.article.RecommendedArticleDao
import com.example.sample.socialmob.repository.dao.search.SearchTagDao
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.backlog.articles.AddCommentResponseModel
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticleViewModel(application: Application) : AndroidViewModel(application) {

    var mId: String = ""
    var mIdGlobal: String = ""
    var mAutGlobal: String = ""
    var mUserImage: String = ""

    private var bookMarkedArticleDao: BookMarkedArticleDao? = null
    private var historyArticleDao: HistoryArticleDao? = null
    private var recommendedArticleDao: RecommendedArticleDao? = null
    private var articleDashBoardDao: ArticleDashBoardDao? = null
    private var searchTagDao: SearchTagDao? = null


    var bookMarkedArticleLiveData: LiveData<List<Bookmark>>
    var recommendedArticleLiveData: LiveData<List<ArticleData>>
    var singleRecommendedArticleLiveData: LiveData<ArticleData>
    var singleArticleLiveData: MutableLiveData<SingleArticleApiModel>
    var tagArticleResponseModel: MutableLiveData<TagArticleResponseModel>
    var articleDashBoardResponseModelPayload: LiveData<List<HalfscreenDashboard>>
    var searchTagLiveData: LiveData<List<SearchTagResponseModelPayloadTag>>


    val boomarkedArticleListLiveData: MutableLiveData<MutableList<Bookmark>> = MutableLiveData()
    val allArticleListLiveData: MutableLiveData<MutableList<ArticleData>> = MutableLiveData()

    //    var allArticleList: MutableList<ArticleData>? = ArrayList()
    var bookmarkedArticleList: MutableList<Bookmark>? = ArrayList()

    var isLoadCompletedAllArticle: Boolean = false
    var isLoadAllArticle: Boolean = false
    var isLoadBookmarkedArticle: Boolean = false
    var isLoadCompletedBookmarkedArticle: Boolean = false


    var userReadArticleLiveData: MutableLiveData<MutableList<UserReadArticle>>? = MutableLiveData()
    var userReadArticlesHistory: MutableList<UserReadArticle>? = ArrayList()
    var isLoadHistoryArticle: Boolean = false
    var isLoadCompletedHistoryArticle: Boolean = false


    var mCommentLiveadta: MutableLiveData<MutableList<SingleArticleApiModelPayloadComment>>? =
        MutableLiveData()
    var mCommentList: MutableList<SingleArticleApiModelPayloadComment>? = ArrayList()
    var isLoadingComments: Boolean = false
    var isLoadCompletedCommentList: Boolean = false
    var mentionProfileResponseModel: MutableLiveData<MutableList<Profile>> =
        MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)
    private val serviceApi = WebServiceClient.client.create(BackEndApi::class.java)// get instance
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {

        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)

        historyArticleDao = habitRoomDatabase.historyArticleDao()
        bookMarkedArticleDao = habitRoomDatabase.bookMarkedArticleDao()
        recommendedArticleDao = habitRoomDatabase.recommendedArticleDao()
        articleDashBoardDao = habitRoomDatabase.articleDashBoardDao()
        searchTagDao = habitRoomDatabase.searchTagDao()


//        userReadArticleLiveData = historyArticleDao?.getAllHistoryArticle()!!
        bookMarkedArticleLiveData = bookMarkedArticleDao?.getAllBookMarkedArticle()!!
        recommendedArticleLiveData = recommendedArticleDao?.getAllRecommendedArticle()!!
        articleDashBoardResponseModelPayload = articleDashBoardDao?.getAllArticleDashBoard()!!
        searchTagLiveData = searchTagDao?.getAllSearchTag()!!


        singleRecommendedArticleLiveData = recommendedArticleDao?.getRecommendedArticleById(mId)!!
        singleArticleLiveData = MutableLiveData()
        tagArticleResponseModel = MutableLiveData()

    }


    fun getHalfScreenDashBoard(mAuth: String, mSlot: String) {

        uiScope.launch {
            serviceApi
                .getHalfScreenDAshBoard(mAuth)
                .enqueue(object : Callback<ArticleDashBoardResponseModel> {
                    override fun onFailure(
                        call: Call<ArticleDashBoardResponseModel>?,
                        t: Throwable?
                    ) {

                    }

                    override fun onResponse(
                        call: Call<ArticleDashBoardResponseModel>?,
                        response: Response<ArticleDashBoardResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> saveHalfScreenDashBoard(response)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "getHalfScreenDAshBoard api"
                            )
                        }
                    }

                })
        }
    }

    fun historyArticle(
        mAuth: String,
        mPlatform: String,
        mPlatformId: String,
        mOffset: String,
        mCount: String
    ) {
        isLoadHistoryArticle = true
        uiScope.launch {
            serviceApi
                .getHistoryArticle(mAuth, mPlatform, mPlatformId, mOffset, mCount)
                .enqueue(object : Callback<HistoryArticleResponseModel> {
                    override fun onFailure(
                        call: Call<HistoryArticleResponseModel>?,
                        t: Throwable?
                    ) {

                    }

                    override fun onResponse(
                        call: Call<HistoryArticleResponseModel>?,
                        response: Response<HistoryArticleResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> saveHistoryArticle(response, mOffset)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "TrendingTrack api"
                            )
                        }
                    }

                })

        }

    }

    fun getBookMarkedArticle(
        mAuth: String,
        mPlatform: String,
        mPlatformId: String,
        mOffset: String,
        mCount: String
    ) {
        isLoadBookmarkedArticle = false

        uiScope.launch {
            serviceApi
                .getBookMarkedArticle(mAuth, mPlatform, mPlatformId, mOffset, mCount)
                .enqueue(object : Callback<BookMarkedArticleResponseModel> {
                    override fun onFailure(
                        call: Call<BookMarkedArticleResponseModel>?,
                        t: Throwable?
                    ) {

                    }

                    override fun onResponse(
                        call: Call<BookMarkedArticleResponseModel>?,
                        response: Response<BookMarkedArticleResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> saveBookMarkedArticle(response, mOffset)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "Bookmarked Article  api"
                            )
                        }
                    }

                })

        }

    }


    fun recommendedArticle(
        mAuth: String,
        mPlatform: String,
        mPlatformId: String,
        mOffset: String,
        mCount: String
    ) {
        isLoadAllArticle = true
        uiScope.launch {
            serviceApi
                .getRecommendedArticle(mAuth, mPlatform, mOffset, mCount)
                .enqueue(object : Callback<ForYouArticleResponseModel> {
                    override fun onFailure(call: Call<ForYouArticleResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<ForYouArticleResponseModel>?,
                        response: Response<ForYouArticleResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> saveRecommendedArticle(response, mOffset)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "Recommended Article api"
                            )
                        }
                    }

                })

        }

    }


    private fun saveHalfScreenDashBoard(value: Response<ArticleDashBoardResponseModel>?) {

        SaveHalfScreenDashBoardAsyncTask(this.articleDashBoardDao!!).execute(value!!.body()!!.payload!!.articles)
    }

    class SaveHalfScreenDashBoardAsyncTask internal constructor(
        private val dashBoardDao: ArticleDashBoardDao
    ) :
        AsyncTask<List<HalfscreenDashboard>?, Void, Void>() {
        override fun doInBackground(vararg params: List<HalfscreenDashboard>?): Void? {
            dashBoardDao.deleteAllArticleDashBoard()
            dashBoardDao.insertArticleDashBoard(params[0]!!)
            return null
        }
    }


    private fun saveRecommendedArticle(
        response: Response<ForYouArticleResponseModel>,
        mOffset: String
    ) {
        isLoadAllArticle = false
        if (mOffset == "0") {
            SaveRecommendedAsyncTask(
                this.recommendedArticleDao!!,
                mOffset
            ).execute(response.body()!!.payload!!.articles)
        }
        if (response.body()!!.payload!!.articles!!.isEmpty()) {
            isLoadCompletedAllArticle = true
        }
//        if (response.body()!!.payload!!.articles!!.isNotEmpty()) {
//            allArticleList!!.addAll(response!!.body()!!.payload!!.articles as MutableList)
//        }


        allArticleListLiveData.postValue(response.body()?.payload?.articles as MutableList)

    }

    class SaveRecommendedAsyncTask internal constructor(
        private val articleDao: RecommendedArticleDao, private val mOffset: String
    ) :
        AsyncTask<List<ArticleData>?, Void, Void>() {
        override fun doInBackground(vararg params: List<ArticleData>?): Void? {
            if (mOffset == "0") {
                articleDao.deleteAllRecommendedArticle()
            }
            articleDao.insertRecommendedArticle(params[0]!!)
            return null
        }
    }

    private fun saveBookMarkedArticle(
        response: Response<BookMarkedArticleResponseModel>,
        mOffset: String
    ) {
        isLoadBookmarkedArticle = false
        if (mOffset == "0") {
            SaveBookMarkedAsyncTask(
                this.bookMarkedArticleDao!!
            ).execute(response.body()!!.payload!!.bookmarks)
        }
        if (response.body()!!.payload!!.bookmarks!!.isEmpty()) {
            isLoadCompletedBookmarkedArticle = true
        }
        if (response.body()!!.payload!!.bookmarks!!.isNotEmpty()) {
            bookmarkedArticleList!!.addAll(response.body()!!.payload!!.bookmarks as MutableList)
        }


        boomarkedArticleListLiveData.postValue(bookmarkedArticleList!!)
    }

    private fun saveHistoryArticle(value: Response<HistoryArticleResponseModel>?, mOffset: String) {
        isLoadHistoryArticle = false
        if (mOffset == "0") {
            SaveHistoryAsyncTask(
                this.historyArticleDao!!
            ).execute(value?.body()?.payload?.userReadArticles)
        }
        if (value?.body()?.payload?.userReadArticles?.isEmpty()!!) {
            isLoadCompletedHistoryArticle = true
        }
        if (value.body()?.payload?.userReadArticles?.isNotEmpty()!!) {
            userReadArticlesHistory?.addAll(value.body()?.payload?.userReadArticles as MutableList)
        }


        userReadArticleLiveData?.postValue(userReadArticlesHistory!!)
    }

    class SaveHistoryAsyncTask internal constructor(
        private val searchTrackDao: HistoryArticleDao
    ) :
        AsyncTask<List<UserReadArticle>?, Void, Void>() {
        override fun doInBackground(vararg params: List<UserReadArticle>?): Void? {
            searchTrackDao.deleteAllHistoryArticle()
            searchTrackDao.insertHistoryArticle(params[0]!!)
            return null
        }
    }

    class SaveBookMarkedAsyncTask internal constructor(
        private val searchTrackDao: BookMarkedArticleDao
    ) :
        AsyncTask<List<Bookmark>?, Void, Void>() {
        override fun doInBackground(vararg params: List<Bookmark>?): Void? {
            searchTrackDao.deleteAllBookMarkedArticle()
            searchTrackDao.insertBookMarkedArticle(params[0]!!)
            return null
        }
    }


    fun addToBookmark(mAuth: String, mId: String) {

        uiScope.launch {
            serviceApi
                .addToBookmark(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> updateResponseAddToBookMark()
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "addToBookmark api"
                            )
                        }
                    }

                })

        }

    }

    private fun updateResponseAddToBookMark() {
        doAsync {
            //            updateDbContentTrue("true", mId)
        }
    }


    fun getArticleDetails(mAuth: String, mId: String) {

        mAutGlobal = mAuth
        mIdGlobal = mId
        uiScope.launch {
            serviceApi
                .getArticleDetails(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<SingleArticleApiModel> {
                    override fun onFailure(call: Call<SingleArticleApiModel>?, t: Throwable?) {
                        t!!.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<SingleArticleApiModel>?,
                        response: Response<SingleArticleApiModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> populateList(response)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "Single Article Api"
                            )
                        }
                    }

                })
        }
    }


    private fun populateList(value: Response<SingleArticleApiModel>) {
        singleArticleLiveData.postValue(value.body()!!)
    }


    private fun updateDbContentTrue(mValue: String, mId: String) {
        doAsync {
            bookMarkedArticleDao!!.deleteBookmark(mId)
            historyArticleDao!!.updateBookmark(mValue, mId)
            recommendedArticleDao!!.updateBookmark(mValue, mId)
        }
    }

    fun removeBookmark(mAuth: String, mId: String) {

        uiScope.launch {
            serviceApi
                .removeBookmark(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { updateDbContentFalse("false", mId) }
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "removeBookmark api"
                            )
                        }
                    }

                })
        }
    }

    private fun updateDbContentFalse(mValue: String, mId: String) {
        doAsync {
            //            bookMarkedArticleDao!!.deleteBookmark(mId)
//            historyArticleDao!!.updateBookmark(mValue, mId)
//            recommendedArticleDao!!.updateBookmark(mValue, mId)
        }

    }

    fun getArticleByIdRecommended(mId: String) {
        this.mId = mId
        singleRecommendedArticleLiveData = recommendedArticleDao!!.getRecommendedArticleById(mId)
    }

    fun postComment(mAuth: String, contentId: String?, dataPost: CommentPostData) {

        uiScope.launch {
            serviceApi
                .postComment(mAuth, RemoteConstant.mPlatform, contentId!!, dataPost)
                .enqueue(object : Callback<AddCommentResponseModel> {
                    override fun onFailure(call: Call<AddCommentResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<AddCommentResponseModel>?,
                        response: Response<AddCommentResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> addCommentToList(response)//getArticleDetails(mAutGlobal, mIdGlobal)//updateDbContentFalse("false", mId)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "postComment api"
                            )
                        }
                    }

                })
        }
    }

    private fun addCommentToList(response: Response<AddCommentResponseModel>) {

        mCommentList?.add(0, response.body()!!.payload!!.comment!!)
        mCommentLiveadta!!.postValue(mCommentList!!)
    }


    fun deleteArticleComment(mAuth: String, mIdComment: String, mId: String) {

        uiScope.launch {
            serviceApi
                .deleteArticleComment(mAuth, RemoteConstant.mPlatform, mIdComment, mId)
                .enqueue(object : Callback<Any> {
                    override fun onFailure(call: Call<Any>?, t: Throwable?) {

                    }

                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        when (response!!.code()) {
                            200 -> println()//getArticleDetails(mAutGlobal, mIdGlobal) //updateDbContentFalse("false", mId)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "deleteArticleComment api"
                            )
                        }
                    }

                })

        }

    }


    fun updateArticleComment(mAuth: String, mId: String, dataPost: CommentPostData) {

        uiScope.launch {
            serviceApi
                .updateComment(mAuth, RemoteConstant.mPlatform, mId, dataPost)
                .enqueue(object : Callback<Any> {
                    override fun onFailure(call: Call<Any>?, t: Throwable?) {

                    }

                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        when (response!!.code()) {
                            200 -> getArticleDetails(
                                mAutGlobal,
                                mIdGlobal
                            )//updateDbContentFalse("false", mId)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "TrendingTrack api"
                            )
                        }
                    }

                })

        }

    }

    fun getTagArticle(mAuth: String, tagId: String) {
        uiScope.launch {
            serviceApi
                .getTagData(
                    mAuth,
                    RemoteConstant.mPlatformId,
                    tagId,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCount
                )
                .enqueue(object : Callback<TagArticleResponseModel> {
                    override fun onFailure(call: Call<TagArticleResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<TagArticleResponseModel>?,
                        response: Response<TagArticleResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> tagArticleResponseModel.postValue(response.body())
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "TrendingTrack api"
                            )
                        }
                    }

                })

        }

    }

    fun followTag(mAuth: String, mId: String) {

        uiScope.launch {
            serviceApi
                .followTag(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { updateDbSearchFollowTag("true", mId) }
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "Follow Tag  api"
                            )
                        }
                    }

                })

        }

    }


    private fun updateDbSearchFollowTag(mValue: String, mId: String) {
        doAsync { searchTagDao!!.updateFollowing(mValue, mId) }
//        AppUtils.showCommonToast(getApplication(), value.body()!!.payload!!.message!!)
    }

    fun unFollowTag(mAuth: String, mId: String) {

        uiScope.launch {
            serviceApi
                .unFollowTag(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { updateDbSearchUnFollowTag("false", mId) }
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "Un Follow Tag  api"
                            )
                        }
                    }

                })

        }

    }

    private fun updateDbSearchUnFollowTag(mValue: String, mId: String) {
        doAsync { searchTagDao!!.updateFollowing(mValue, mId) }
//        AppUtils.showCommonToast(getApplication(), value!!.body()!!.payload!!.message!!)
    }

    fun callArticleAnaltics(mContext: Context, mId: String) {
//        api/v1/content/{platformId}/{contentId}/activity
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mSlash +
                    RemoteConstant.mContentDataV1 + RemoteConstant.mSlash + RemoteConstant.mPlatformId +
                    RemoteConstant.mSlash + mId + RemoteConstant.pathAnalytics,
            RemoteConstant.postMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            mContext,
            RemoteConstant.mAppId, ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        val body = AnalyticsArticleBody()
        body.Action = "view"

        uiScope.launch {
            serviceApi
                .postArticleAnalytics(
                    mAuth,
                    RemoteConstant.mPlatform,
                    RemoteConstant.mPlatformId,
                    mId,
                    body
                )
                .enqueue(object : Callback<Any> {
                    override fun onFailure(call: Call<Any>?, t: Throwable?) {

                    }

                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        when (response!!.code()) {
                            200 -> println()
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "Article analytics api"
                            )
                        }
                    }

                })

        }


    }

    fun getAllComments(mAuth: String, mId: String, size: Int, mCount: String) {
        isLoadingComments = true
        uiScope.launch {
            serviceApi
                .getCommentData(mAuth, RemoteConstant.mPlatform, mId, size.toString(), mCount)
                .enqueue(object : Callback<ArticleCommentResponseModel> {
                    override fun onFailure(
                        call: Call<ArticleCommentResponseModel>?,
                        t: Throwable?
                    ) {

                    }

                    override fun onResponse(
                        call: Call<ArticleCommentResponseModel>?,
                        response: Response<ArticleCommentResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> populateCommentList(response)
                            else -> RemoteConstant.apiErrorDetails(
                                getApplication(), response.code(), "All comments api"
                            )
                        }
                    }

                })

        }
    }

    private fun populateCommentList(response: Response<ArticleCommentResponseModel>?) {
        isLoadingComments = false

        if (response?.body()?.payload?.comments?.isEmpty()!!) {
            isLoadCompletedCommentList = true
        }
        if (response.body()?.payload?.comments?.isNotEmpty()!!) {
            mCommentList?.addAll(response.body()?.payload?.comments as MutableList)
        }


        mCommentLiveadta!!.postValue(mCommentList!!)

    }

    fun getMentions(mContext: Context, mMnention: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mMentionProfile +
                    RemoteConstant.pathQuestionmark + "name=" + mMnention, RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        uiScope.launch {
            var response: Response<MentionProfileResponseModel>? = null
            kotlin.runCatching {
                response= serviceApi
                    .mentionProfile(
                        mAuth, RemoteConstant.mPlatform, mMnention
                    )
            }.onSuccess {
                when (response!!.code()) {
                    200 -> mentionProfileResponseModel.postValue(response?.body()?.payload?.profiles)
                    else -> RemoteConstant.apiErrorDetails(
                        getApplication(), response?.code()!!, "Get Mention Profile api"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }


}