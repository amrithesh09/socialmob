package com.example.sample.socialmob.viewmodel.chat

import android.app.Application
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.arthenica.mobileffmpeg.Config
import com.arthenica.mobileffmpeg.FFmpeg
import com.example.sample.socialmob.model.chat.*
import com.example.sample.socialmob.model.search.SearchProfileResponseModel
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.dao.DbChatFileList
import com.example.sample.socialmob.repository.dao.DbChatList
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.utils.ProgressBodyImage
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.ui.home_module.chat.model.*
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.ConnectedSocket
import com.example.sample.socialmob.view.utils.MyApplication
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import com.example.sample.socialmob.view.utils.rxwebsocket.entities.SocketClosedEvent
import com.example.sample.socialmob.view.utils.rxwebsocket.entities.SocketClosingEvent
import com.example.sample.socialmob.view.utils.rxwebsocket.entities.SocketOpenEvent
import com.example.sample.socialmob.viewmodel.feed.MessageListResponse
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class WebSocketChatViewModel(application: Application) : AndroidViewModel(application) {


    var dbChatList: DbChatList? = null
    var mDbChatFileList: DbChatFileList? = null
    var mOfflineMessage: LiveData<List<MessageListItem>>? = MutableLiveData()
    var newMessage: MutableLiveData<String>? = MutableLiveData()
    var uploadProgress: MutableLiveData<UploadProgress>? = MutableLiveData()
    var mIsUploading: MutableLiveData<Boolean>? = MutableLiveData()
    var mIsUploadingVideo: Boolean = false
    var mFfmpeg: Long? = null

    init {
        val habitRoomDatabase: SocialMobDatabase =
            SocialMobDatabase.getDatabase(MyApplication.application)
        dbChatList = habitRoomDatabase.dbChatList()
        mDbChatFileList = habitRoomDatabase.dbChatFileList()
        mOfflineMessage = dbChatList?.getAllMessage()
    }

    val getSingleChatResponseModel: MutableLiveData<ResultResponse<MutableList<MsgList>>> =
        MutableLiveData()
    val getPreSignedPayLoadList: MutableLiveData<ResultResponse<MutableList<PreSignedPayLoadList>>> =
        MutableLiveData()
    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)
    private val serviceApiChat =
        WebServiceClient.clientChat.create(BackEndApi::class.java)// get instance
    private var mMessageStringList: MutableList<MsgPayload>? = ArrayList()
    private val mListUploadCallBack: MutableList<Requests>? =
        ArrayList()
    private val serviceApiSingle =
        WebServiceClient.clientSingle.create(BackEndApi::class.java)// get instance
    val getAllChatResponseModel: MutableLiveData<ResultResponse<MutableList<ConvList>>> =
        MutableLiveData()
    val getAllChatSuggestionResponseModel: MutableLiveData<ResultResponse<MutableList<SearchProfileResponseModelPayloadProfile>>>? =
        MutableLiveData()
    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        viewModelJob.cancel()
        rxWebSocket?.close()?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                { success: Boolean ->
                }
            ) { throwable: Throwable ->
            }

        if (onOpen != null) {
            onOpen?.dispose()
        }
        if (onClosed != null) {
            onClosed?.dispose()
        }
        if (onClosing != null) {
            onClosing?.dispose()
        }
        if (onFailure != null) {
            onFailure?.dispose()
        }
        if (rxTaskMessageSend != null) {
            rxTaskMessageSend?.dispose()
        }
        if (rxTaskClose != null) {
            rxTaskClose?.dispose()
        }
        if (mFfmpeg != null) {
            FFmpeg.cancel(mFfmpeg!!)
        }

    }

    private var mGetRecentMessageListApiCall: Job? = null
    fun getRecentMessageList(nJwt: String, convId: String, mOffset: String, mCount: String) {

        var response: Response<MessageListResponse>? = null
        if (mOffset == "0") {
            if (mGetRecentMessageListApiCall != null) {
                mGetRecentMessageListApiCall?.cancel()
            }
            getSingleChatResponseModel?.postValue(
                ResultResponse.loading(
                    ArrayList(),
                    mOffset
                )
            )
        } else {
            getSingleChatResponseModel?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        mGetRecentMessageListApiCall = uiScope.launch(handler) {
            kotlin.runCatching {
                response = serviceApiChat
                    .getRecentMessageList(
                        nJwt, convId, mOffset, mCount, SharedPrefsUtils.getStringPreference(
                            getApplication(), RemoteConstant.mAppId, ""
                        )!!
                    )
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body() != null && response?.body()?.msgList != null &&
                            response?.body()?.msgList?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                getSingleChatResponseModel?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.msgList!!
                                    )
                                )
                            } else {
                                getSingleChatResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.msgList
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                getSingleChatResponseModel?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                getSingleChatResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        getSingleChatResponseModel?.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> {
                        getSingleChatResponseModel?.postValue(ResultResponse.error("", ArrayList()))
                    }
                }
            }.onFailure {
                getSingleChatResponseModel?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }

    }

    fun deleteMessage(nJwt: String, msgId: String?) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                serviceApiChat
                    .deleteMessage(nJwt, msgId!!)
            }.onSuccess {
            }.onFailure {
            }
        }
    }

    fun deleteMessageOffline(mTimeStamp: String) {

        doAsync {
            dbChatList?.deleteOfflineMessage(mTimeStamp)
        }
    }

    fun callApiMarkAllAsRead(nJwt: String, convId: String) {

        uiScope.launch(handler) {
            kotlin.runCatching {
                serviceApiChat
                    .callApiMarkAllAsRead(nJwt, convId)
            }.onSuccess {
            }.onFailure {
            }

        }
    }

    fun getChatSuggestionList(mAuthPenny: String, mOffset: String, size: Int) {

        if (mOffset == "0") {
            if (size == 0) {
                getAllChatSuggestionResponseModel?.postValue(
                    ResultResponse.loading(
                        ArrayList(),
                        mOffset
                    )
                )
            }
        } else {
            getAllChatSuggestionResponseModel?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        uiScope.launch(handler) {
            var response: Response<SearchProfileResponseModel>? = null
            kotlin.runCatching {
                response = serviceApiChat
                    .getChatSuggestion(mAuthPenny, RemoteConstant.mPlatform)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.profiles != null && response?.body()?.payload?.profiles?.size!! > 0) {
                            getAllChatSuggestionResponseModel?.postValue(
                                ResultResponse.success(
                                    response?.body()?.payload?.profiles!!
                                )
                            )
                        } else {
                            getAllChatSuggestionResponseModel?.postValue(
                                ResultResponse.noData(
                                    ArrayList()
                                )
                            )
                        }
                    }
                    502, 522, 523, 500 -> {
                        getAllChatSuggestionResponseModel?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        RemoteConstant.apiErrorDetails(
                            MyApplication.application,
                            response?.code()!!,
                            "Get Suggestion Chat list"
                        )
                    }
                }
            }.onFailure {
                getAllChatSuggestionResponseModel?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun getChatMessageList(mAuthPenny: String, mOffset: String, size: Int) {

        if (mOffset == "0") {
            if (size == 0) {
                getAllChatResponseModel.postValue(
                    ResultResponse.loading(
                        ArrayList(),
                        mOffset
                    )
                )
            }
        } else {
            getAllChatResponseModel.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        uiScope.launch(handler) {

            var response: Response<ConversationListResponse>? = null
            kotlin.runCatching {
                response = serviceApiChat
                    .getChatMessageList(mAuthPenny, RemoteConstant.mPlatform)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body() != null &&
                            response?.body()?.convList != null &&
                            response?.body()?.convList?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                getAllChatResponseModel.postValue(
                                    ResultResponse.success(
                                        response?.body()?.convList!!
                                    )
                                )
                            } else {
                                getAllChatResponseModel.postValue(
                                    ResultResponse.paginatedList(response?.body()?.convList)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                getAllChatResponseModel.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                getAllChatResponseModel.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        getAllChatResponseModel.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        RemoteConstant.apiErrorDetails(
                            MyApplication.application, response?.code()!!, "Get Chat list"
                        )
                    }
                }
            }.onFailure {
                getAllChatResponseModel.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }


    fun deleteConversation(mJwt: String?, convId: String?, mProfileId: String?) {

        if (mListUploadCallBack?.size!! > 0) {
            for (i in 0 until mListUploadCallBack.size) {
                if (mListUploadCallBack[i].mUserId == mProfileId) {
                    mListUploadCallBack[i].mCall.cancel()
                    val mFileLastIndex = mListUploadCallBack[i].mFileName.substring(
                        mListUploadCallBack[i].mFileName.lastIndexOf('/') + 1
                    )
                    updateFailed(mFileLastIndex)
                }
            }
        }
        if (mFfmpeg != null) {
            FFmpeg.cancel(mFfmpeg!!)
        }

        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                response = serviceApiChat
                    .deleteConversation(mJwt!!, convId!!)
            }.onSuccess {
                when (response?.code()) {
                    200 -> {

                    }
                    else -> {
                        AppUtils.showCommonToast(
                            MyApplication.application!!,
                            "Error occur please try again later"
                        )
                    }

                }
            }.onFailure {
                AppUtils.showCommonToast(
                    MyApplication.application!!,
                    "Error occur please try again later"
                )
            }
        }

    }

    var rxWebSocket: com.example.sample.socialmob.view.utils.rxwebsocket.RxWebSocket? = null
    var onOpen: Disposable? = null
    var onClosed: Disposable? = null
    var onClosing: Disposable? = null
    var onFailure: Disposable? = null
    var rxTaskMessageSend: Disposable? = null
    var rxTaskClose: Disposable? = null
    var rxTask: Disposable? = null
    var isWebSocketOpen = false
    var isFfmpegOnProgress = false
    var isWebSocketOpenCalled = false
    var isWebSocketCalled = false

//    private fun logEventChat() {
//
//        rxTask = websocket?.eventStream()
//            ?.observeOn(AndroidSchedulers.mainThread())
//            ?.doOnNext { event: SmRxWebsocket.Event? ->
//                when (event) {
//
//                    is SmRxWebsocket.Message -> {
//                        try {
//                            newMessage?.postValue(
//                                event.data().toString()
//                            )
//
//                        } catch (throwable: Throwable) {
//                            println("---->[MESSAGE RECEIVED]:" + event.data().toString())
//                            //                        logNewLine()
//                        }
//                    }
//                }
//            }
//            ?.subscribeOn(Schedulers.io())
//            ?.subscribe(
//                Functions.emptyConsumer(),
//                Consumer { throwable: Throwable -> println(throwable) })
//    }

    fun connectToSocketSm() {
        val mJwt =
            SharedPrefsUtils.getStringPreference(MyApplication.application, RemoteConstant.mJwt, "")
        val appId =
            SharedPrefsUtils.getStringPreference(
                getApplication(), RemoteConstant.mAppId, ""
            )

        val mWebSocketUrl = RemoteConstant.mBaseUrlWebSocket + mJwt + "&appId=" + appId

        if (mJwt != "") {
            rxWebSocket =
                com.example.sample.socialmob.view.utils.rxwebsocket.RxWebSocket(mWebSocketUrl)

            rxTask = rxWebSocket?.onTextMessage()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnNext { socketMessageEvent ->
                    newMessage?.postValue(socketMessageEvent?.text)
                }
                ?.subscribe(
                    Functions.emptyConsumer(),
                    Consumer { throwable: Throwable -> println(throwable) })
//                ?.subscribe({ socketMessageEvent: SocketMessageEvent? ->
//
//                    newMessage?.postValue(socketMessageEvent?.text)
//
//                }) { obj: Throwable -> obj.printStackTrace() }

            onOpen = rxWebSocket?.onOpen()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ socketOpenEvent: SocketOpenEvent? ->
                    isWebSocketCalled = false
                    isWebSocketOpenCalled = false
                    isWebSocketOpen = true
                    println("" + socketOpenEvent)
//                        Toast.makeText(application, " Connected ", Toast.LENGTH_SHORT).show()
                    println("---->" + " Connected ")
                    EventBus.getDefault().post(ConnectedSocket(true))
                }) { obj: Throwable -> obj.printStackTrace() }
            onClosed = rxWebSocket?.onClosed()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ socketClosedEvent: SocketClosedEvent? ->

                    println("---->" + "onClosed")
                }) { obj: Throwable -> obj.printStackTrace() }
            onClosing = rxWebSocket?.onClosing()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ socketClosingEvent: SocketClosingEvent? ->
                    println("" + socketClosingEvent)
                    println("---->" + "onClosing")
                }) { obj: Throwable -> obj.printStackTrace() }

            onFailure = rxWebSocket?.onFailure()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.delay(10, TimeUnit.SECONDS)
                ?.doOnNext { socketMessageEvent ->

                    println("---->" + "Disconnected")
                    // TODO : For loop call
                    isWebSocketOpen = false
                    if (MyApplication.isForeground && !isWebSocketOpenCalled && !isWebSocketOpen) {
                        println("---->" + "connect retry ")
                        if (mJwt != "") {
                            isWebSocketOpenCalled = true
                            rxWebSocket?.connect()
                        }
                    } else {
                        if (!isWebSocketOpen && MyApplication.isForeground && isWebSocketOpen) {
                            isWebSocketOpen = false
                            isWebSocketOpenCalled = false
                        }
                    }
                }
                ?.subscribe(
                    Functions.emptyConsumer(),
                    Consumer { throwable: Throwable -> println(throwable) })

            if (!isWebSocketOpen && MyApplication.isForeground) {
                isWebSocketOpenCalled = true
                rxWebSocket?.connect()
                println("---->" + "connect first ")
            }
        }
    }

    fun sendMessageSm(mMessage: String) {

        rxTaskMessageSend = rxWebSocket?.sendMessage(mMessage)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                { success: Boolean ->
//                            Snackbar.make(
//                                binding?.chatRecyclerView!!,
//                                "Message sent result: $success",
//                                Snackbar.LENGTH_SHORT
//                            ).show()
//                    println("" + success)
//                    println("" + success)
                }
            ) { throwable: Throwable ->
//                        Snackbar.make(
//                            binding?.chatRecyclerView!!,
//                            "Message error: " + throwable.message,
//                            Snackbar.LENGTH_SHORT
//                        ).show()
//                println("" + throwable)
//                println("" + throwable)
            }
    }

    fun closeSocketSm() {

        rxTaskClose = rxWebSocket?.close()?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                { success: Boolean ->
                }
            ) { throwable: Throwable ->
            }
    }

    fun preSignedUrlChat(
        imageSize: Int,
        nJwt: String,
        imageList: MutableList<String>?,
        videoUrl: String,
        mProfileId: String,
        videoThumbImageFile: String,
        msgId: String?,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks,
        mUploadCallbacksImage: ProgressBodyImage.UploadCallbacksImage,
        mTimeStampRetry: String?
    ) {

        mIsUploading?.postValue(true)
        // TODO : Creating an timestamp list for image ( For upload comparision )
        val mTimeStampList: MutableList<String>? = ArrayList()

        if (imageList?.size!! > 0) {
            val mLocalMessageList: MutableList<MsgList>? = ArrayList()


            var mTimeStamp = System.currentTimeMillis()
            for (i in 0 until imageList.size) {

                mTimeStamp += 100

                val mMsgListItem = MsgList()
                mMsgListItem.timestamp = mTimeStamp.toString()
                mTimeStampList?.add(mTimeStamp.toString())
                mMsgListItem.message_sender?._id = SharedPrefsUtils.getStringPreference(
                    getApplication(), RemoteConstant.mProfileId, ""
                )
                mMsgListItem.msg_type = "image"


                val newFile = MsgListFile()
                newFile.url = ""
                newFile.local_file_path = imageList[i]
                newFile.type = "image"
                newFile.status = "-1"
                mMsgListItem.file = newFile



                mLocalMessageList?.add(mMsgListItem)

                if (mLocalMessageList?.size == imageList.size) {
                    addLocalList(
                        imageSize, nJwt, imageList, videoUrl, mProfileId, mTimeStampList!!,
                        mLocalMessageList, videoThumbImageFile, msgId, mUploadCallbacks,
                        mUploadCallbacksImage, mTimeStampRetry
                    )
                }
            }


        } else {
            //TODO : For adding local list common method ( Since presigned url
            // will get after video compression)

            val mLocalMessageList: MutableList<MsgList>? = ArrayList()
            val mMsgListItem = MsgList()
            mMsgListItem.timestamp = System.currentTimeMillis().toString()
            mMsgListItem.message_sender?._id = SharedPrefsUtils.getStringPreference(
                getApplication(), RemoteConstant.mProfileId, ""
            )
            mMsgListItem.msg_type = "video"
            val newFile = MsgListFile()
            newFile.url = ""
            newFile.local_file_path = videoUrl
            newFile.type = "video"
            newFile.status = "-1"
            mMsgListItem.file = newFile



            mLocalMessageList?.add(mMsgListItem)
            addLocalList(
                imageSize,
                nJwt,
                imageList,
                videoUrl,
                mProfileId,
                mTimeStampList!!,
                mLocalMessageList,
                videoThumbImageFile,
                msgId,
                mUploadCallbacks, mUploadCallbacksImage, mTimeStampRetry
            )
        }


    }

    private fun addLocalList(
        imageSize: Int,
        nJwt: String,
        imageList: MutableList<String>,
        videoUrl: String,
        mProfileId: String,
        mTimeStampList: MutableList<String>,
        mLocalMessageList: MutableList<MsgList>?,
        videoThumbImageFile: String,
        msgId: String?,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks,
        mUploadCallbacksImage: ProgressBodyImage.UploadCallbacksImage,
        mTimeStampRetry: String?
    ) {


        if (mTimeStampRetry == "") {
            getSingleChatResponseModel?.postValue(
                ResultResponse.addLocalData(mLocalMessageList ?: ArrayList())
            )
        }

        // TODO : Creating an timestamp list for video ( For upload comparision )

        if (videoUrl != "") {
            mIsUploadingVideo = true
            try {
                val file =
                    File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
                if (!file.exists()) {
                    file.mkdir()
                }
                val mFileOutput =
                    File(file.toString() + "/" + System.currentTimeMillis() + "socialmob.mp4")
                val mFileInput =
                    File(videoUrl)

                val ffmpegCommand =
                    "-i \"" + mFileInput.absolutePath + "\" -strict experimental -vsync 2 -async 1 -c:v libx264 -profile:v baseline " + mFileOutput.absolutePath
                var videoUrlTimeStamp: String
                if (mTimeStampRetry == "") {
                    videoUrlTimeStamp = mLocalMessageList?.get(0)?.timestamp!!
                } else {
                    videoUrlTimeStamp = mTimeStampRetry!!
                }

                val videoUrlCompressed = mFileOutput.absolutePath
                uploadToServer(
                    imageSize,
                    nJwt,
                    imageList,
                    videoUrlCompressed,
                    mProfileId,
                    videoUrlTimeStamp,
                    mTimeStampList,
                    videoThumbImageFile,
                    msgId,
                    ffmpegCommand,
                    videoUrl,
                    mUploadCallbacks,
                    mUploadCallbacksImage,
                    mTimeStampRetry

                )
            } catch (e: Exception) {
                mIsUploadingVideo = true
                e.printStackTrace()
            }
        } else {
            val videoUrlTimeStamp = ""
            uploadToServer(
                imageSize,
                nJwt,
                imageList,
                videoUrl,
                mProfileId,
                videoUrlTimeStamp,
                mTimeStampList,
                videoThumbImageFile,
                msgId,
                "",
                videoUrl,
                mUploadCallbacks,
                mUploadCallbacksImage,
                mTimeStampRetry
            )
        }
    }


    private fun uploadToServer(
        imageSize: Int,
        nJwt: String,
        imageList: MutableList<String>?,
        videoUrl: String?,
        mProfileId: String,
        videoUrlTimeStamp: String,
        mTimeStampList: MutableList<String>?,
        videoThumbImageFile: String,
        msgId: String?,
        ffmpegCommand: String,
        videoUrlOgPath: String,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks,
        mUploadCallbacksImage: ProgressBodyImage.UploadCallbacksImage,
        mTimeStampRetry: String?
    ) {


        serviceApiChat
            .preSignedUrlChat(nJwt, imageSize.toString(), "0")
            .enqueue(object : Callback<PreSignedPayLoad> {
                override fun onFailure(call: Call<PreSignedPayLoad>?, t: Throwable?) {
                    getPreSignedPayLoadList?.postValue(ResultResponse.error("", ArrayList()))
                    Toast.makeText(
                        getApplication(), "Something went wrong please try again",
                        Toast.LENGTH_LONG
                    ).show()
                    mIsUploading?.postValue(false)
                }

                override fun onResponse(
                    call: Call<PreSignedPayLoad>?,
                    response: Response<PreSignedPayLoad>?
                ) {
                    when (response!!.code()) {
                        200 -> {
                            if (response.body() != null &&
                                response.body()?.payloadList != null &&
                                response.body()?.payloadList?.size!! > 0

                            ) {

                                //TODO : Insert to local db
                                val mPayloadList = response.body()?.payloadList
                                for (i in mPayloadList?.indices!!) {
                                    mPayloadList[i].status = "0"
                                }

                                mMessageStringList?.clear()

                                for (i in 0 until response.body()?.payloadList?.size!!) {


//                                    GlobalScope.launch(context = Dispatchers.IO) {
//                                        Thread.sleep(2000)

                                    if (response.body()?.payloadList?.get(i)?.type == "image") {


                                        val mFilename =
                                            response.body()?.payloadList?.get(i)?.filename

                                        val mSendMessage = SendMessage()
                                        val mMsgPayload = MsgPayload()

                                        mSendMessage.user = mProfileId
                                        if (msgId == "") {
                                            mSendMessage.type = "message"
                                        } else {
                                            mSendMessage.type = "update-file"
                                            mMsgPayload.msg_id = msgId
                                        }

                                        mMsgPayload.user = mProfileId
                                        mMsgPayload.msgType = "image"
                                        mMsgPayload.text = ""
                                        if (mTimeStampRetry == "") {
                                            mMsgPayload.timestamp =
                                                mTimeStampList?.get(i)?.toLong()
                                        } else {
                                            mMsgPayload.timestamp = mTimeStampRetry?.toLong()
                                        }

                                        val newFile = PayloadFile()
                                        newFile.filename = mFilename
                                        newFile.type = "image"
                                        newFile.local_file_path = imageList?.get(i)
                                        mMsgPayload.file = newFile

                                        mSendMessage.msgPayload =
                                            mMsgPayload



                                        mMessageStringList?.add(mMsgPayload)

                                        //TODO : Handle retry
                                        if (mTimeStampRetry == "") {
                                            if (mMessageStringList?.size == response.body()?.payloadList?.size) {
                                                val mSendMessageList = SendMessage()
                                                mSendMessageList.type = "message-list"
                                                mSendMessageList.user = mProfileId
                                                mSendMessageList.msg_payload_list =
                                                    mMessageStringList

                                                val mGson = Gson()
                                                val mMessage =
                                                    mGson.toJson(mSendMessageList)
                                                sendMessageSm(mMessage)
                                            }
                                        } else {
                                            val mGson = Gson()
                                            val mMessage =
                                                mGson.toJson(
                                                    mSendMessage
                                                )
                                            println("" + mMessage)
                                            sendMessageSm(mMessage)
                                        }


                                        val mRequestBodyImage =
                                            ProgressBodyImage(
                                                File(imageList?.get(i)!!),
                                                "image/jpeg"
                                                    .toMediaTypeOrNull(),
                                                mUploadCallbacksImage,
                                                mMsgPayload.timestamp?.toString()
                                            )


                                        val uploadCallBack: Call<ResponseBody> =
                                            serviceApiSingle
                                                .uploadImageProgressAmazon(
                                                    response.body()?.payloadList?.get(
                                                        i
                                                    )?.url!!, mRequestBodyImage
                                                )


                                        val mRequests = Requests(
                                            uploadCallBack,
                                            imageList[i], mFilename!!, mProfileId
                                        )
                                        mListUploadCallBack?.add(mRequests)

                                        uploadCallBack.enqueue(object :
                                            Callback<ResponseBody> {
                                            override fun onFailure(
                                                call: Call<ResponseBody>?,
                                                t: Throwable?
                                            ) {
                                                updateFailed(mFilename)
                                            }

                                            override fun onResponse(
                                                call: Call<ResponseBody>?,
                                                response: Response<ResponseBody>?
                                            ) {
                                                when (response!!.code()) {
                                                    200 -> {
                                                        updateSuccess(mFilename)
                                                    }
                                                    else -> {
                                                        RemoteConstant.apiErrorDetails(
                                                            getApplication(),
                                                            response.code(),
                                                            "Upload Amazon server Chat file"
                                                        )
                                                        updateFailed(mFilename)
                                                    }
                                                }
                                            }

                                        })


                                    }

//                                    }


                                }
                                mDbChatFileList?.insertFile(response.body()?.payloadList!!)
                            }
                        }
                        else -> {
                            getPreSignedPayLoadList?.postValue(
                                ResultResponse.error(
                                    "",
                                    ArrayList()
                                )
                            )
                        }

                    }
                }

            })

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            if (videoUrl != "") {
                mIsUploadingVideo = true
                var mThumbFileUrl: String
                var mThumbFileName = ""

                serviceApiChat
                    .preSignedUrlChat(nJwt, "1", "1")
                    .enqueue(object : Callback<PreSignedPayLoad> {
                        override fun onFailure(
                            call: Call<PreSignedPayLoad>?,
                            t: Throwable?
                        ) {
                            getPreSignedPayLoadList?.postValue(
                                ResultResponse.error(
                                    "",
                                    ArrayList()
                                )
                            )
                            Toast.makeText(
                                getApplication(), "Something went wrong please try again",
                                Toast.LENGTH_LONG
                            ).show()
                            mIsUploading?.postValue(false)
                        }

                        override fun onResponse(
                            call: Call<PreSignedPayLoad>?,
                            response: Response<PreSignedPayLoad>?
                        ) {
                            when (response!!.code()) {
                                200 -> {
                                    if (response.body() != null &&
                                        response.body()?.payloadList != null &&
                                        response.body()?.payloadList?.size!! > 0

                                    ) {
                                        for (il in 0 until response.body()?.payloadList?.size!!) {
                                            if (response.body()?.payloadList?.get(
                                                    il
                                                )?.type == "image"
                                            ) {
                                                mThumbFileUrl =
                                                    response.body()?.payloadList?.get(
                                                        il
                                                    )?.url!!
                                                mThumbFileName =
                                                    response.body()?.payloadList?.get(
                                                        il
                                                    )?.filename!!


                                                //TODO : Create an video thumbnail

                                                val mRequestBodyImageThumbnail =
                                                    File(
                                                        videoThumbImageFile
                                                    )
                                                        .asRequestBody(
                                                            "image/jpeg"
                                                                .toMediaTypeOrNull()
                                                        )

                                                serviceApiSingle
                                                    .uploadImageProgressAmazon(
                                                        mThumbFileUrl,
                                                        mRequestBodyImageThumbnail
                                                    )
                                                    .enqueue(object :
                                                        Callback<ResponseBody> {
                                                        override fun onFailure(
                                                            call: Call<ResponseBody>?,
                                                            t: Throwable?
                                                        ) {

                                                        }

                                                        override fun onResponse(
                                                            call: Call<ResponseBody>?,
                                                            response: Response<ResponseBody>?
                                                        ) {
                                                            when (response!!.code()) {
                                                                200 -> {

                                                                    println(
                                                                        ">>success"
                                                                    )
                                                                }
                                                                else -> RemoteConstant.apiErrorDetails(
                                                                    getApplication(),
                                                                    response.code(),
                                                                    "Upload Amazon server Chat file"
                                                                )
                                                            }
                                                        }

                                                    })
                                            } else {

                                                var mRequestBodyVideo: ProgressRequestBody?
                                                val mFilename =
                                                    response.body()?.payloadList?.get(
                                                        il
                                                    )?.filename

                                                if (mTimeStampRetry == "") {
                                                    mRequestBodyVideo =
                                                        ProgressRequestBody(
                                                            File(videoUrl!!),
                                                            "video/mp4"
                                                                .toMediaTypeOrNull(),
                                                            mUploadCallbacks, mTimeStampRetry
                                                        )
                                                } else {
                                                    mRequestBodyVideo =
                                                        ProgressRequestBody(
                                                            File(videoUrl!!),
                                                            "video/mp4"
                                                                .toMediaTypeOrNull(),
                                                            mUploadCallbacks, videoUrlTimeStamp
                                                        )
                                                }
                                                val mVideoUrl =
                                                    response.body()?.payloadList?.get(
                                                        il
                                                    )?.url!!

                                                if (mFfmpeg != null) {
                                                    FFmpeg.cancel(mFfmpeg!!)
                                                }

                                                mFfmpeg =
                                                    FFmpeg.executeAsync(
                                                        ffmpegCommand
                                                    ) { _, returnCode ->
                                                        when (returnCode) {
                                                            Config.RETURN_CODE_SUCCESS -> {
                                                                isFfmpegOnProgress = true

                                                                Log.i(
                                                                    Config.TAG,
                                                                    "Async command execution completed successfully."
                                                                )
                                                                val uploadCallBack: Call<ResponseBody> =
                                                                    serviceApiSingle
                                                                        .uploadImageProgressAmazon(
                                                                            mVideoUrl,
                                                                            mRequestBodyVideo
                                                                        )
                                                                val mRequests =
                                                                    Requests(
                                                                        uploadCallBack,
                                                                        videoUrlOgPath,
                                                                        mFilename!!,
                                                                        mProfileId
                                                                    )
                                                                mListUploadCallBack?.add(
                                                                    mRequests
                                                                )
                                                                uploadCallBack
                                                                    .enqueue(
                                                                        object :
                                                                            Callback<ResponseBody> {
                                                                            override fun onFailure(
                                                                                call: Call<ResponseBody>?,
                                                                                t: Throwable?
                                                                            ) {
                                                                                updateFailed(
                                                                                    mFilename
                                                                                )
                                                                                mIsUploadingVideo =
                                                                                    false
                                                                            }

                                                                            override fun onResponse(
                                                                                call: Call<ResponseBody>?,
                                                                                response: Response<ResponseBody>?
                                                                            ) {
                                                                                when (response!!.code()) {
                                                                                    200 -> {
                                                                                        //TODO : Delete file
                                                                                        //
                                                                                        val dir =
                                                                                            File(
                                                                                                Environment.getExternalStoragePublicDirectory(
                                                                                                    "socialmob"
                                                                                                ),
                                                                                                ""
                                                                                            )
                                                                                        if (dir.isDirectory) {
                                                                                            val children =
                                                                                                dir.list()
                                                                                            for (i in children.indices) {
                                                                                                File(
                                                                                                    dir,
                                                                                                    children[i]
                                                                                                ).delete()
                                                                                            }
                                                                                        }
                                                                                        println(
                                                                                            ">>>>Success"
                                                                                        )
                                                                                        updateSuccess(
                                                                                            mFilename
                                                                                        )
                                                                                        mIsUploadingVideo =
                                                                                            false

                                                                                    }
                                                                                    else -> {
                                                                                        mIsUploadingVideo =
                                                                                            false
                                                                                        RemoteConstant.apiErrorDetails(
                                                                                            getApplication(),
                                                                                            response.code(),
                                                                                            "Upload Amazon server Chat file"
                                                                                        )
                                                                                        updateFailed(
                                                                                            mFilename
                                                                                        )
                                                                                    }
                                                                                }
                                                                            }

                                                                        })

                                                            }
                                                            Config.RETURN_CODE_CANCEL -> {
                                                                isFfmpegOnProgress = false
                                                                Log.i(
                                                                    Config.TAG,
                                                                    "Async command execution cancelled by user."
                                                                )
                                                                updateFailed(
                                                                    mFilename!!
                                                                )
                                                                mIsUploadingVideo = false
                                                            }
                                                            else -> {
                                                                isFfmpegOnProgress = false
                                                                mIsUploadingVideo = false
                                                                updateFailed(
                                                                    mFilename!!
                                                                )
                                                                Log.i(
                                                                    Config.TAG,
                                                                    String.format(
                                                                        "Async command execution failed with rc=%d.",
                                                                        returnCode
                                                                    )
                                                                )
                                                            }
                                                        }
                                                    }

                                                val mSendMessage =
                                                    SendMessage()
                                                val mMsgPayload =
                                                    MsgPayload()

                                                mSendMessage.user =
                                                    mProfileId
                                                mSendMessage.type =
                                                    "message"
                                                if (msgId == "") {
                                                    mSendMessage.type =
                                                        "message"
                                                } else {
                                                    mSendMessage.type =
                                                        "update-file"
                                                    mMsgPayload.msg_id =
                                                        msgId
                                                }

                                                mMsgPayload.user =
                                                    mProfileId
                                                mMsgPayload.msgType =
                                                    "video"
                                                mMsgPayload.text = ""
                                                mMsgPayload.timestamp =
                                                    videoUrlTimeStamp.toLong()

                                                val newFile =
                                                    PayloadFile()
                                                newFile.filename =
                                                    mFilename
                                                newFile.type = "video"
                                                newFile.execution_id =
                                                    mFfmpeg?.toString()
                                                newFile.local_file_path =
                                                    videoUrlOgPath
                                                newFile.thumbnail_file_name =
                                                    mThumbFileName
                                                mMsgPayload.file =
                                                    newFile

                                                mSendMessage.msgPayload =
                                                    mMsgPayload


                                                val mGson = Gson()
                                                val mMessage =
                                                    mGson.toJson(
                                                        mSendMessage
                                                    )
                                                sendMessageSm(mMessage)


                                            }
                                        }


                                    }
                                }
                            }
                        }
                    })


            }
        }, 1000)

        Config.enableStatisticsCallback { statistics ->
            val timeInMilliseconds: Int = statistics.time
            if (timeInMilliseconds > 0) {
                val totalVideoDuration = 9000
                val completePercentage: String =
                    BigDecimal(timeInMilliseconds).multiply(BigDecimal(100))
                        .divide(BigDecimal(totalVideoDuration), 0, BigDecimal.ROUND_HALF_UP)
                        .toString()

                if (mTimeStampRetry == "") {
                    val mUploadProgress = UploadProgress(videoUrlTimeStamp, completePercentage)
                    uploadProgress?.postValue(mUploadProgress)
                } else {
                    val mUploadProgress = UploadProgress(mTimeStampRetry!!, completePercentage)
                    uploadProgress?.postValue(mUploadProgress)
                }

            }
        }

    }

    private var mUploadCancelSize = 0
    private fun updateFailed(mFilename: String) {
        val mUploadModel = UploadModel()
        val mUploadModelAckMsg = UploadModelAckMsg()

        mUploadCancelSize += 1
        mUploadSize += 1

        if (mListUploadCallBack?.size!! == mUploadCancelSize) {
            mIsUploading?.postValue(false)
            mUploadCancelSize = 0
            mUploadSize = 0
            mListUploadCallBack.clear()
        } else if (mListUploadCallBack.size == 1) {
            mIsUploading?.postValue(false)
            mListUploadCallBack.clear()
            mUploadCancelSize = 0
            mUploadSize = 0
        }

        println("///////updateFailed-mUploadSize-" + mUploadSize)
        println("///////updateFailed-mUploadCancelSize-" + mUploadCancelSize)
        println("////////////////////////////////////////////////////////////////////////////////////")


        doAsync {
            mDbChatFileList?.updateStatus(mFilename, "3")
        }
        mUploadModel.type = "ack"

        mUploadModelAckMsg.filename =
            mFilename
        mUploadModelAckMsg.status =
            "file-upload-failed"

        mUploadModel.ackMsg = mUploadModelAckMsg

        val mGsonmmUploadModel = Gson()
        val mMessagemUploadModel =
            mGsonmmUploadModel.toJson(
                mUploadModel
            )
        sendMessageSm(
            mMessagemUploadModel
        )
    }

    private var mUploadSize = 0
    private fun updateSuccess(mFilename: String) {
        mUploadSize += 1
        mUploadCancelSize += 1



        if (mListUploadCallBack?.size!! == mUploadSize) {
            mIsUploading?.postValue(false)
            mUploadSize = 0
            mUploadCancelSize = 0
            mListUploadCallBack.clear()
        } else if (mListUploadCallBack.size == 1) {
            mIsUploading?.postValue(false)
            mListUploadCallBack.clear()
            mUploadCancelSize = 0
            mUploadSize = 0
        }

        println("///////updateFailed-mUploadSize-" + mUploadSize)
        println("///////updateFailed-mUploadCancelSize-" + mUploadCancelSize)
        println("////////////////////////////////////////////////////////////////////////////////////")


        val mUploadModel = UploadModel()
        val mUploadModelAckMsg = UploadModelAckMsg()

        doAsync {
            mDbChatFileList?.deleteFile(mFilename)
        }

        mUploadModel.type = "ack"

        mUploadModelAckMsg.filename =
            mFilename
        mUploadModelAckMsg.status =
            "file-upload-complete"

        mUploadModel.ackMsg = mUploadModelAckMsg

        val mGsonmmUploadModel = Gson()
        val mMessagemUploadModel =
            mGsonmmUploadModel.toJson(
                mUploadModel
            )
        sendMessageSm(
            mMessagemUploadModel
        )

    }

    fun cancelApiCall(mFileName: String, mFileType: String, mFilePath: String) {
        try {
            if (mListUploadCallBack?.size!! > 0) {
                for (i in 0 until mListUploadCallBack.size) {
                    if (mListUploadCallBack[i].mFileName != "" && mListUploadCallBack[i].mFileName == mFileName) {
                        mListUploadCallBack[i].mCall.cancel()
//                        val mFileLastIndex = mFilePath.substring(mFilePath.lastIndexOf('/') + 1)
//                        updateFailed(mFileLastIndex)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (mFileType == "video") {
            mIsUploadingVideo = false
            if (mFfmpeg != null) {
                FFmpeg.cancel(mFfmpeg!!)
            }
            val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
            if (dir.isDirectory) {
                val children = dir.list()
                for (i in children.indices) {
                    File(dir, children[i]).delete()
                }
            }
            if (mListUploadCallBack?.size == 0) {
                mIsUploading?.postValue(false)
            }
        }

        val files: MutableList<String>? = ArrayList()
        val mFileLastIndex = mFilePath.substring(mFilePath.lastIndexOf('/') + 1)
        files?.add(mFileLastIndex)
        val mFilesList = FailedFiles(files!!)

        val mJwt = "Bearer " + SharedPrefsUtils.getStringPreference(
            getApplication(),
            RemoteConstant.mJwt, ""
        )
        var mApiCall: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                mApiCall = serviceApiChat
                    .updateFailedFiles(
                        mJwt, mFilesList,
                        SharedPrefsUtils.getStringPreference(
                            getApplication(),
                            RemoteConstant.mAppId,
                            ""
                        )!!
                    )
            }.onSuccess {
                when (mApiCall?.code()) {
                    200 -> {
                        doAsync {
                            mDbChatFileList?.deleteAllMessage()!!
                        }
                    }
                    502, 522, 523, 500 -> {
                    }
                    else -> {
                    }
                }
            }.onFailure {
            }
        }
    }


}