package com.example.sample.socialmob.viewmodel.feed

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.article.CommentPostData
import com.example.sample.socialmob.model.feed.CommentDeleteResponseModel
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.profile.CommentResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.repository.repo.feed.FeedImageRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class FeedImageViewModel @Inject constructor(
    private var feedImageRepository: FeedImageRepository,
    private var application: Context
) :
    ViewModel() {

    var responseModel: MutableLiveData<SingleFeedResponseModel> = MutableLiveData()
    var commentDeleteResponseModel: MutableLiveData<CommentDeleteResponseModel> = MutableLiveData()
    var commentPaginatedResponseModel: MutableLiveData<ResultResponse<List<CommentResponseModelPayloadComment>>>? =
        MutableLiveData()
    var commentPaginatedResponseModelCommentCount: MutableLiveData<String> = MutableLiveData()


    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    var mentionProfileResponseModel: MutableLiveData<MentionProfileResponseModel> =
        MutableLiveData()
    var mentionProfileResponseModelProfile: MutableLiveData<MutableList<Profile>> =
        MutableLiveData()
    private var mMentionApiCall: Job? = null

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun getFeedDetails(mPostId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!!,
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mApiKey,
                ""
            )!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mActionPathV1 +
                    RemoteConstant.mSlash +
                    mPostId, RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId, ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<SingleFeedResponseModel>? = null
            kotlin.runCatching {
                feedImageRepository.getPostFeedDataAction(mAuth, RemoteConstant.mPlatform, mPostId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        responseModel.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Post Image Data Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun getFeedDetailsPost(mPostId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!!,
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mApiKey,
                ""
            )!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash +
                    mPostId +
                    RemoteConstant.mPathDetails, RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId, ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<SingleFeedResponseModel>? = null
            kotlin.runCatching {
                feedImageRepository.getPostFeedData(mAuth, RemoteConstant.mPlatform, mPostId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        responseModel.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Post Image Data Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun likePost(mAuth: String, postId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                feedImageRepository.likePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Like Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun disLikePost(mAuth: String, postId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                feedImageRepository.disLikePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "TLike Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun deleteComment(mAuth: String, mPostId: String, mId: String) {

        uiScope.launch(handler) {
            var response: Response<CommentDeleteResponseModel>? = null
            kotlin.runCatching {
                feedImageRepository.deleteCommentPost(mAuth, RemoteConstant.mPlatform, mPostId, mId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        commentDeleteResponseModel.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete post comment api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun deletePost(mId: String, mContext: Context) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                feedImageRepository.deletePost(mAuth, RemoteConstant.mPlatform, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete Post"
                    )
                }
            }.onFailure {
            }
        }

    }

    fun getFeedComment(
        mAuth: String, mId: String, mOffset: String,
        mCount: String
    ) {
        var response: Response<CommentResponseModel>? = null
        uiScope.launch(handler) {

            if (mOffset == "0") {
                commentPaginatedResponseModel?.postValue(
                    ResultResponse.loading(
                        ArrayList(),
                        mOffset
                    )
                )
            } else {
                commentPaginatedResponseModel?.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            kotlin.runCatching {
                feedImageRepository.getFeedComments(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload?.comments != null && response?.body()?.payload?.comments?.size!! > 0) {
                            if (mOffset == "0") {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.comments!!
                                    )
                                )
                                commentPaginatedResponseModelCommentCount.postValue(response?.body()?.payload?.commentsCount)
                            } else {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.comments!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        commentPaginatedResponseModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Feed comments Api"
                    )
                }
            }.onFailure {
                commentPaginatedResponseModel?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }


    fun getMentions(mContext: Context, mMnention: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mMentionProfile +
                    RemoteConstant.pathQuestionmark + "name=" + mMnention, RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        if (mMentionApiCall != null) {
            mMentionApiCall?.cancel()
        }

        mMentionApiCall = uiScope.launch(handler) {
            var response: Response<MentionProfileResponseModel>? = null
            kotlin.runCatching {
                feedImageRepository.mentionProfile(
                    mAuth, RemoteConstant.mPlatform, mMnention
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> mentionProfileResponseModelProfile.postValue(response?.body()?.payload?.profiles)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get Mention Profile api"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun postComment(
        mAuth: String, mPostId: String, commentPostData: CommentPostData
    ) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                feedImageRepository.postCommentPost(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mPostId,
                    commentPostData
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        val mAuthFeedComment =
                            RemoteConstant.getAuthCommentList(
                                application,
                                mPostId,
                                "0",
                                RemoteConstant.mCount.toInt()
                            )
                        getFeedComment(
                            mAuthFeedComment,
                            mPostId,
                            "0",
                            RemoteConstant.mCount
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 502, "Post Feed comments api"
                    )
                }
            }.onFailure {
            }
        }
    }


    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }
}
