package com.example.sample.socialmob.viewmodel.feed

import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.feed.MobCategoryFeedModel
import com.example.sample.socialmob.model.feed.MobSpaceCategories
import com.example.sample.socialmob.model.feed.UploadFeedResponseModel
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.AddPlayListResponseModel
import com.example.sample.socialmob.model.music.CreatePlayListResponseModel
import com.example.sample.socialmob.model.music.PlayListResponseModel
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.profile.*
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.repo.feed.FeedRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.ui.home_module.feed.MainFeedFragment
import com.example.sample.socialmob.view.ui.profile.gallery.ProfileMediaGridFragment
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.SizeFromImage
import com.example.sample.socialmob.view.utils.SizeFromVideoFile
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor(
    private var feedRepository: FeedRepository,
    private var application: Context
) : ViewModel() {

    var feedsList: MutableLiveData<ResultResponse<List<PersonalFeedResponseModelPayloadFeed>>>? =
        MutableLiveData()
    var suggestedMobbers: MutableLiveData<MutableList<SearchProfileResponseModelPayloadProfile>>? =
        MutableLiveData()
    var mobSpaceCategories: MutableLiveData<MutableList<MobSpaceCategories>>? =
        MutableLiveData()

    private var createDbPlayListDao: CreateDbPlayList? = null

    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null
    val playListModel: MutableLiveData<List<PlaylistCommon>> = MutableLiveData()

    var isUploading: Boolean = false
    var mobCategoryFeedModel: MutableLiveData<MobCategoryFeedModel>? = MutableLiveData()

    private var mFileGlobal: List<GalleryData>? = null
    var mVideoMedia: MutableLiveData<VideoMedia> = MutableLiveData()
    var isUploaded: MutableLiveData<Boolean> = MutableLiveData()
    val profile: LiveData<Profile>
    private var loginDao: LoginDao? = null

    init {

        val habitRoomDatabase: SocialMobDatabase =
            SocialMobDatabase.getDatabase(application)
        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!
        loginDao = habitRoomDatabase.loginDao()
        profile = loginDao?.getAllProfileData()!!
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

//    companion object {
//        private var instance: FeedViewModel? = null
//        fun getInstance() =
//            instance ?: synchronized(FeedViewModel::class.java) {
//                instance ?: FeedViewModel(application!!).also { instance = it }
//            }
//    }

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


    fun getTimeLineFeed(mAuth: String, mOffset: String, mCount: String) {
        uiScope.launch(handler) {
            if (mOffset == "0") {
                feedsList?.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                feedsList?.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            var response: Response<PersonalFeedResponseModel>? = null
            kotlin.runCatching {
                feedRepository.getTimeLineFeed(mAuth, RemoteConstant.mPlatform, mOffset, mCount)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.feeds != null && response?.body()?.payload?.feeds?.size!! > 0) {
                            if (mOffset == "0") {
                                feedsList?.postValue(
                                    ResultResponse.success(response?.body()?.payload?.feeds!!)
                                )
                            } else {
                                feedsList?.postValue(ResultResponse.paginatedList(response?.body()?.payload?.feeds!!))
                            }
                        } else {
                            if (mOffset == "0") {
                                feedsList?.postValue(ResultResponse.noData(ArrayList()))
                            } else {
                                feedsList?.postValue(ResultResponse.emptyPaginatedList(ArrayList()))
                            }
                        }
                        if (response?.body()?.payload?.suggestedMobbers?.size ?: 0 > 0) {
                            suggestedMobbers?.postValue(response?.body()?.payload?.suggestedMobbers!! as MutableList)
                        }

                        if (response?.body()?.payload?.mobSpaceCategories?.size ?: 0 > 0) {
                            mobSpaceCategories?.postValue(response?.body()?.payload?.mobSpaceCategories !! as MutableList)
                        }

                        RemoteConstant.displayMobSpaceIntro = response?.body()?.payload?.displayMobSpaceIntro!!
                    }
                    502, 522, 523, 500 -> {
                        feedsList?.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> {
                        feedsList?.postValue(ResultResponse.error("", ArrayList()))
                    }

                }
            }.onFailure {
                feedsList?.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }


    fun likePost(mAuth: String, postId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                feedRepository.likePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Like Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun disLikePost(mAuth: String, postId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                feedRepository.disLikePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "DisLike Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun deletePost(mId: String, mContext: Context) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                feedRepository.deletePost(mAuth, RemoteConstant.mPlatform, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete Post"
                    )
                }
            }.onFailure {
            }
        }

    }

    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                feedRepository.createPlayList(mAuth, RemoteConstant.mPlatform, createPlayListBody)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println("")//refresh()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                getPlayListApi = feedRepository.getPlayList(mAuth)
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "Play Api"
                    )
                }
            }.onFailure {
                val mAuthNew =
                    RemoteConstant.getAuthWithoutOffsetAndCount(
                        application,
                        RemoteConstant.pathPlayList
                    )
                getPlayList(mAuthNew)
            }
        }
    }


    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                feedRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        refreshPlayList(response!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        response?.code() ?: 500,
                        "get CreateDbPlayList Track"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        AppUtils.showCommonToast(application, value.body()!!.payload!!.message)
    }

    fun getPreSignedUrl(
        mAuth: String, mType: String, mCount: String, mFile: List<GalleryData>,
        mThumb: ArrayList<File>?, mDescription: PostFeedData, ratio: String,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks
    ) {
        isUploading = true
        if (mType == "image" || mType == "video") {
            mFileGlobal = mFile
            uiScope.launch {
                feedRepository
                    .getPreSignedUrl(mAuth, RemoteConstant.mPlatform, mType, mCount)
                    .enqueue(object : Callback<ImagePreSignedUrlResponseModel> {
                        override fun onFailure(
                            call: Call<ImagePreSignedUrlResponseModel>?,
                            t: Throwable?
                        ) {
                            t?.printStackTrace()
                        }

                        override fun onResponse(
                            call: Call<ImagePreSignedUrlResponseModel>?,
                            response: Response<ImagePreSignedUrlResponseModel>?
                        ) {
                            when (response!!.code()) {
                                200 -> populateValue(
                                    response,
                                    mFile,
                                    mType,
                                    mThumb,
                                    mDescription,
                                    ratio,
                                    mUploadCallbacks
                                )
                                else -> {
                                    RemoteConstant.apiErrorDetails(
                                        application,
                                        response.code(),
                                        "getPreSignedUrl api"
                                    )
                                    isUploading = false
                                    isUploaded.postValue(false)
                                }
                            }
                        }

                    })
            }
        }
    }


    fun populateValue(
        mResponse: Response<ImagePreSignedUrlResponseModel>?,
        mFile: List<GalleryData>,
        mType: String,
        mThumb: ArrayList<File>?,
        mDescription: PostFeedData,
        ratio: String,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks
    ) {
        if (mType == "image") {
            var uploadedList = 0
            mFileGlobal = mFile
            mVideoMedia.postValue(mResponse!!.body()!!.payload!!.videoMedia)

            for (i in mFile.indices) {

                val mUrl = mResponse.body()!!.payload!!.videoMedia!!.rawFileUrls!![i]
                val mRequestBody =
                    File(mFile[i].photoUri).asRequestBody("image/jpeg".toMediaTypeOrNull()!!)

                uiScope.launch {
                    feedRepository
                        .uploadImageProgressAmazon(mUrl, mRequestBody)
                        .enqueue(object : Callback<ResponseBody> {
                            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                                t?.printStackTrace()
                            }

                            override fun onResponse(
                                call: Call<ResponseBody>?,
                                response: Response<ResponseBody>?
                            ) {
                                when (response!!.code()) {
                                    200 -> {
                                        uploadedList += 1
                                        if (uploadedList == mFile.size) {
                                            uploadImageOrVideoPost(
                                                mType,
                                                mResponse,
                                                mDescription,
                                                ratio
                                            )
                                        }
                                    }
                                    else -> {
                                        RemoteConstant.apiErrorDetails(
                                            application,
                                            response.code(),
                                            "Upload Image Api"
                                        )
                                        isUploading = false
                                        isUploaded.postValue(false)
                                    }

                                }
                            }

                        })
                }
            }


        } else {
//
            for (i in mFile.indices) {
                val mUrlVideo = mResponse!!.body()!!.payload!!.videoMedia!!.rawFileUrls!![0]

                val fileBody = ProgressRequestBody(
                    File(mFile[0].photoUri),
                    "video/mp4".toMediaTypeOrNull(),
                    mUploadCallbacks, ""
                )
                uiScope.launch(handler) {
                    feedRepository
                        .uploadImageProgressAmazon(mUrlVideo, fileBody)
                        .enqueue(object : Callback<ResponseBody> {
                            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                                t?.printStackTrace()
                                t?.printStackTrace()
                            }

                            override fun onResponse(
                                call: Call<ResponseBody>?,
                                response: Response<ResponseBody>?
                            ) {
                                when (response!!.code()) {
                                    200 -> {
                                        uploadImageOrVideoPost(
                                            mType,
                                            mResponse,
                                            mDescription,
                                            ratio
                                        )
                                        val mUrlImage =
                                            mResponse.body()!!.payload!!.videoMedia!!.thumbnailUrls!![i]
                                        val mRequestBodyImage =
                                            mThumb!![i]
                                                .asRequestBody("image/jpeg".toMediaTypeOrNull())

                                        uploadThumbnail(
                                            mUrlImage,
                                            mRequestBodyImage
                                        )
                                    }
                                    else -> RemoteConstant.apiErrorDetails(
                                        application,
                                        response.code(),
                                        "Upload video Api"
                                    )
                                }
                            }

                        })
                }
            }

        }
    }

    private fun uploadThumbnail(
        mUrlImage: String, mRequestBodyImage: RequestBody
    ) {
        uiScope.launch(handler) {
            feedRepository
                .uploadImageProgressAmazon(mUrlImage, mRequestBodyImage)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        Log.d("AmR_", "uploadThumbnail_" +call.toString())
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>?, response: Response<ResponseBody>?
                    ) {
                        when (response!!.code()) {
                            200 ->  { println()
                            Log.d("AmR_", "uploadThumbnail_200" +call.toString()) }
                            else -> RemoteConstant.apiErrorDetails(
                                application,
                                response.code(),
                                "Upload video Thumbnail Api"
                            )
                        }
                    }

                })
        }

    }


    private fun uploadImageOrVideoPost(
        mType: String, value: Response<ImagePreSignedUrlResponseModel>,
        mDescription: PostFeedData, ratio: String
    ) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(
                application, RemoteConstant.mAppId, ""
            )!!,
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mApiKey, ""
            )!!,
            RemoteConstant.mBaseUrl + RemoteConstant.createFeedPost, RemoteConstant.postMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        val postData: PostFeedData = mDescription
        postData.type = mType

        val mediaList: ArrayList<PostFeedDataMedia> = arrayListOf()

        for (i in value.body()?.payload?.videoMedia?.rawFileNames?.indices!!) {
            val mediaData = PostFeedDataMedia()
            mediaData.filename = value.body()?.payload?.videoMedia?.rawFileNames?.get(i)
            if (mType == "video") {
                mediaData.thumbnailFilename =
                    value.body()?.payload?.videoMedia?.thumbnailFileNames?.get(i)
                mediaData.ratio = ratio
            }
            if (mType == "image") {
                val size = SizeFromImage(mFileGlobal?.get(i)?.photoUri)
                mediaData.height = size.width().toString()
                mediaData.width = size.height().toString()
                mediaData.ratio = ratio
            } else {
                try {
                    val size = SizeFromVideoFile(mFileGlobal?.get(i)?.photoUri)
                    mediaData.height = size.width().toString()
                    mediaData.width = size.height().toString()

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            mediaList.add(mediaData)
        }
        postData.media = mediaList
        uiScope.launch(handler) {
            var response: Response<UploadFeedResponseModel>? = null
            kotlin.runCatching {
                feedRepository.createFeedPost(mAuth, RemoteConstant.mPlatform, postData).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        isUploading = false
                        isUploaded.postValue(true)
                        when {
                            SharedPrefsUtils.getStringPreference(
                                application,
                                "POST_FROM",
                                ""
                            ) == "CAMPAIGN_FEED" -> {
                                ProfileMediaGridFragment.idPost =
                                    response?.body()?.payload?.post?.id!!
                            }
                            else -> {
                                MainFeedFragment.idPost =
                                    response?.body()?.payload?.post?.id!!
                            }
                        }
                        doAsync {
                            for (element in mFileGlobal!!) {
                                delete(File(element.photoUri))
                            }
                        }
                    }
                    else -> {
                        isUploading = false
                        isUploaded.postValue(false)
                    }

                }
            }.onFailure {
            }
        }
    }

    fun delete(file: File) {
        val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
        if (dir.isDirectory) {
            val children = dir.list()
            for (i in children?.indices!!) {
                File(dir, children[i]).delete()
            }
        }
        val fileDelete =
            File(file.absolutePath)
        fileDelete.delete()
        if (fileDelete.exists()) {
            fileDelete.canonicalFile.delete()
            if (file.exists()) {
                application.deleteFile(
                    fileDelete.name
                )
            }
        }
        if (dir.exists()) {
            dir.delete()
        }
    }

    fun followProfile(mAuth: String, mId: String, mFollowingCount: String) {
        uiScope.launch(handler) {
            var mFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                feedRepository.followProfile(mAuth, RemoteConstant.mPlatform, mId).collect {
                    mFollowProfile = it
                }
            }.onSuccess {
                when (mFollowProfile?.code()) {
                    200 ->
                        if (mFollowProfile?.body()?.payload?.message != "request pending") {
                            val mCountNew = Integer.parseInt(mFollowingCount).plus(1)
                            updateFollowing(mCountNew.toString())
                        }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mFollowProfile?.code() ?: 500,
                        "Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }

    private fun updateFollowing(mCountNew: String) {
        doAsync {
            loginDao?.upDateFollowersCount(mCountNew)
        }
    }

    fun unFollowProfile(mAuth: String, mId: String, mFollowingCount: String) {
        uiScope.launch(handler) {
            var mUnFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                feedRepository.unFollowProfile(mAuth, RemoteConstant.mPlatform, mId).collect {
                    mUnFollowProfile = it
                }
            }.onSuccess {
                when (mUnFollowProfile?.code()) {
                    200 -> doAsync {
                        val mCountNew = Integer.parseInt(mFollowingCount).minus(1)
                        updateFollowing(mCountNew.toString())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mUnFollowProfile?.code()!!,
                        "Un Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }

    fun cancelRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mCancelRequest: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                feedRepository.cancelFollowUser(mAuth, RemoteConstant.mPlatform, mId).collect {
                    mCancelRequest = it
                }
            }.onSuccess {
                when (mCancelRequest?.code()) {
                    200 -> {
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mCancelRequest?.code() ?: 500,
                        "cancelRequest api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun setMobSpaceCategory(mId: String, name: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!!,
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mApiKey,
                ""
            )!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mobSubCategory,
            RemoteConstant.postMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var mCancelRequest: Response<MobCategoryFeedModel>? = null
            kotlin.runCatching {
                feedRepository.setMobSpaceCategory(mAuth, RemoteConstant.mPlatform, mId, name).collect {
                    mCancelRequest = it
                }
            }.onSuccess {
                when (mCancelRequest?.code()) {
                    200 -> {
                        if (mCancelRequest?.body()?.status?.success == true) {
                            mobCategoryFeedModel?.postValue(mCancelRequest?.body())
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mCancelRequest?.code() ?: 500,
                        "cancelRequest api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }
}