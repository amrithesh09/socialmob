package com.example.sample.socialmob.viewmodel.feed

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.music.AddPlayListResponseModel
import com.example.sample.socialmob.model.music.CreatePlayListResponseModel
import com.example.sample.socialmob.model.music.PlayListResponseModel
import com.example.sample.socialmob.model.music.PlaylistCommon
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModel
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadHashTag
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.repo.feed.PhotoGridRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MusicAndPhotoGridViewModel @Inject constructor(
    private var photoGridRepository: PhotoGridRepository,
    private var application: Context
) :
    ViewModel() {

    val mPhotoVideoList: MutableLiveData<ResultResponse<MutableList<PersonalFeedResponseModelPayloadFeed>>> =
        MutableLiveData()
    val playListModel: MutableLiveData<List<PlaylistCommon>> = MutableLiveData()

    var createDbPlayListDao: CreateDbPlayList? = null
    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null


    var isLoadCompletedFeedPhotoGrid: Boolean = false
    var isLoadingPhotoGrid: Boolean = false

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
        val habitRoomDatabase: SocialMobDatabase =
            SocialMobDatabase.getDatabase(application)
        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!
    }


    fun getPhotoGridmediagrid(
        mAuth: String,
        mOffset: String,
        mCount: String
    ) {
        var response: Response<PersonalFeedResponseModel>? = null
        uiScope.launch(handler) {
            if (mOffset == "0") {
                mPhotoVideoList.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                mPhotoVideoList.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            kotlin.runCatching {
                photoGridRepository.getPhotoGridMediaGrid(
                    mAuth, RemoteConstant.mPlatform, mOffset, mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload != null &&
                            response?.body()?.payload?.feeds != null &&
                            response?.body()?.payload?.feeds?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.feeds)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get PhotoGrid api"
                    )
                }
            }.onFailure {
                mPhotoVideoList.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    var hashTagData: PersonalFeedResponseModelPayloadHashTag? = null
    fun getMediaGridCampaign(
        mAuth: String, tagId: String, feedType: String, mOffset: String, mCount: String
    ) {
        var response: Response<PersonalFeedResponseModel>? = null
        isLoadingPhotoGrid = true
        uiScope.launch(handler) {
            kotlin.runCatching {
                photoGridRepository.getMediaGridCampaign(
                    mAuth,
                    RemoteConstant.mPlatform, tagId, feedType, mOffset, mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload != null &&
                            response?.body()?.payload?.feeds != null &&
                            response?.body()?.payload?.feeds?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                                hashTagData = response?.body()?.payload?.hashTag
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.feeds)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get PhotoGrid api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun getMediaGrid(mAuth: String, mProfileId: String, mOffset: String, mCount: String) {
        var response: Response<PersonalFeedResponseModel>? = null
        isLoadingPhotoGrid = true
        uiScope.launch(handler) {
            kotlin.runCatching {
                photoGridRepository.getMediaGrid(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mProfileId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload != null &&
                            response?.body()?.payload?.feeds != null &&
                            response?.body()?.payload?.feeds?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.feeds)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get PhotoGrid api"
                    )
                }
            }.onFailure {
                mPhotoVideoList.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                photoGridRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> doAsync {
                        AppUtils.showCommonToast(
                            application,
                            response?.body()?.payload?.message
                        )
                        refreshPlayList(response!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        response?.code() ?: 500,
                        "get CreateDbPlayList Track"
                    )
                }
            }.onFailure {
            }
        }
    }


    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        AppUtils.showCommonToast(application, value.body()!!.payload!!.message)
//        refresh()
    }

    private fun refresh() {
        val mAuthPlayList =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application,
                RemoteConstant.pathPlayList
            )
        getPlayList(mAuthPlayList)
    }

    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                photoGridRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "Play Api"
                    )
                }
            }.onFailure {
                val mAuthNew =
                    RemoteConstant.getAuthWithoutOffsetAndCount(
                        application,
                        RemoteConstant.pathPlayList
                    )
                getPlayList(mAuthNew)
            }
        }
    }


    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                photoGridRepository.createPlayList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    createPlayListBody
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refresh()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Error occur please try again later"
                )
            }
        }
    }


    fun deletePost(mId: String, mContext: Context) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                photoGridRepository.deletePost(mAuth, RemoteConstant.mPlatform, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete Post"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun likePost(mAuth: String, postId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                photoGridRepository.likePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Like Feed Post api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun disLikePost(mAuth: String, postId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                photoGridRepository.disLikePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Like Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }
}
