package com.example.sample.socialmob.viewmodel.login

import android.content.Context
import android.os.AsyncTask
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.login.*
import com.example.sample.socialmob.model.profile.ProfileSocialRegisterData
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.repo.login_module.LoginRepository
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.SingleLiveEvent
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.Util
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private var loginRepository: LoginRepository,
    private var application: Context
) :
    ViewModel() {

    var referralResponseModel: MutableLiveData<RefferalResponseModel> = MutableLiveData()
    var forgotPasswordResponseModel: MutableLiveData<ForgotPasswordResponseModel> =
        MutableLiveData()
    var btnSelected: ObservableBoolean? = null
    var email: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var passwordLiveData: MutableLiveData<String>? = MutableLiveData()
    var progressDialog: SingleLiveEvent<Boolean>? = null
    var userLogin: MutableLiveData<LoginResponse>? = null
    var isLoadingSocial: Boolean = false

    private var loginDao: LoginDao? = null

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)

        loginDao = habitRoomDatabase.loginDao()

        btnSelected = ObservableBoolean(false)
        progressDialog = SingleLiveEvent()
        email = ObservableField("")
        password = ObservableField("")
        userLogin = MutableLiveData()
    }

    fun onEmailChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(Util.isEmailValid(s.toString()) && password?.get()!!.length >= 2)


    }

    fun onPasswordChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(Util.isEmailValid(email?.get()!!) && s.toString().length >= 2)
        passwordLiveData!!.postValue(password.toString())

    }

    fun login(mEmail: String, mPassword: String) {
        if (InternetUtil.isInternetOn()) {
            progressDialog?.value = true

            var response: Response<LoginResponse>? = null
            uiScope.launch(handler) {
                kotlin.runCatching {
                    response = loginRepository
                        .loginApi(RemoteConstant.mPlatform, mEmail, mPassword)
                }.onSuccess {
                    progressDialog?.postValue(false)
                    when (response?.code()) {
                        200 -> updateResponse(response!!)
                        else -> {
                            progressDialog?.postValue(false)
                            AppUtils.showCommonToast(
                                application,
                                "Error occur please try again later"
                            )
                            RemoteConstant.apiError(response?.code() ?: 500)
                        }
                    }

                }.onFailure {
                    progressDialog?.postValue(false)
                    AppUtils.showCommonToast(
                        application,
                        "Error occur please try again later"
                    )
                }
            }
        } else {
            AppUtils.showCommonToast(application, "No internet connection")
        }
    }


    private fun updateResponse(response: Response<LoginResponse>) {

        if (response.body()?.payload?.success!!) {
            saveProfileData(response)
        } else {
            AppUtils.showCommonToast(
                application,
                response.body()!!.payload!!.message
            )
        }
    }

    private fun saveProfileData(response: Response<LoginResponse>) {
        userLogin?.postValue(response.body())
        if (response.body()?.payload?.success == true) {
            saveProfile(response)
        }

    }


    private fun saveProfile(value: Response<LoginResponse>) {
        SaveProfile(this.loginDao!!).execute(value.body()?.payload?.profile)
    }

    class SaveProfile internal constructor(private val mAsyncTaskDao: LoginDao) :
        AsyncTask<Profile, Void, Void>() {

        override fun doInBackground(vararg params: Profile): Void? {
            mAsyncTaskDao.deleteAllProfileData()
            mAsyncTaskDao.insertProfileData(params[0])
            return null
        }
    }

    fun verifyCode(verifyCode: String) {
        val codeModel = RefferalCode(verifyCode)

        uiScope.launch {
            loginRepository
                .referralCodeVerify(RemoteConstant.mPlatform, codeModel)
                .enqueue(object : Callback<RefferalResponseModel> {
                    override fun onFailure(call: Call<RefferalResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<RefferalResponseModel>?,
                        response: Response<RefferalResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> referralResponseModel.postValue(response.body())
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Verify code api"
                            )
                        }
                    }

                })
        }


    }

    var isError: MutableLiveData<Boolean> = MutableLiveData()
    fun socialLogin(mSocialData: ProfileSocialRegisterData) {
        isLoadingSocial = true
        uiScope.launch(handler) {
            var response: Response<LoginResponse>? = null
            kotlin.runCatching {
                response = loginRepository
                    .socialLogin(RemoteConstant.mPlatform, mSocialData)
            }.onSuccess {
                when (response?.code()) {
                    200 -> populateLoginResponse(response!!)
                    else -> {
                        RemoteConstant.apiErrorDetails(
                            application, response?.code() ?: 500, "Social Login api"
                        )
                        isError.postValue(false)
                    }
                }

            }.onFailure {
                it.printStackTrace()
                isError.postValue(false)
            }
        }
    }

    private fun populateLoginResponse(response: Response<LoginResponse>) {
        isLoadingSocial = false
        userLogin?.postValue(response.body())
    }

    fun forgotPassWord(mEmail: String) {
        uiScope.launch(handler) {
            var response: Response<ForgotPasswordResponseModel>? = null
            kotlin.runCatching {
                response = loginRepository
                    .forgotPassword(RemoteConstant.mPlatform, mEmail)
            }.onSuccess {
                when (response?.code()) {
                    200 -> forgotPasswordResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "SForgot password api"
                    )
                }
            }.onFailure {
            }
        }
    }

}