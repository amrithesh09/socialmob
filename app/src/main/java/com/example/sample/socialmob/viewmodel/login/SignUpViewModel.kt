package com.example.sample.socialmob.viewmodel.login

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.login.EmailAvailableResponseModel
import com.example.sample.socialmob.model.login.LoginResponse
import com.example.sample.socialmob.model.profile.ProfileRegisterData
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.repo.login_module.SignUpRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.AppUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(private var signUpRepository: SignUpRepository,
                                          private var application: Context
) :
    ViewModel() {

    var emailAvailableResponseModelBoolean: MutableLiveData<Boolean> = MutableLiveData()
    var loginResponse: MutableLiveData<LoginResponse> = MutableLiveData()
    private var loginDao: LoginDao? = null
    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    init {

        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        loginDao = habitRoomDatabase.loginDao()
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


    fun checkEmailAvailable(mEmail: String) {
        var response: Response<EmailAvailableResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                response = signUpRepository.emailAvailable(mEmail)
            }.onSuccess {
                when (response?.code()) {
                    200 -> populateValueEmailAvail(response!!)//emailAvailableResponseModel.postValue(response.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        response?.code() ?: 500,
                        "CheckEmail Available api"
                    )
                }
            }.onFailure {
                emailAvailableResponseModelBoolean.postValue(false)
            }
        }
    }

    private fun populateValueEmailAvail(response: Response<EmailAvailableResponseModel>) {
        if (response.body()?.payload?.available == true) {
            emailAvailableResponseModelBoolean.postValue(true)
        } else {
            emailAvailableResponseModelBoolean.postValue(false)
        }

    }

    fun mobEnter(profileData: ProfileRegisterData) {
        uiScope.launch(handler) {
            var response: Response<LoginResponse>? = null
            kotlin.runCatching {
                response = signUpRepository
                    .mobEnter(RemoteConstant.mPlatform, profileData)
            }.onSuccess {
                when (response?.code()) {
                    200 -> if (response?.isSuccessful!!) populateValue(response)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Mob Enter api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application!!, "Error Occur please try again later"
                )
            }
        }
    }

    private fun populateValue(response: Response<LoginResponse>?) {
        loginResponse.postValue(response!!.body())
        if (response.body() != null && response.body()?.payload != null && response.body()?.payload?.profile != null &&
            !response.body()?.payload?.message.equals("email already exists")
        ) {
            saveProfile(response)
        }
    }

    private fun saveProfile(value: Response<LoginResponse>) {
        doAsync {
            loginDao?.insertProfileData(value.body()?.payload?.profile!!)
        }
    }
}


