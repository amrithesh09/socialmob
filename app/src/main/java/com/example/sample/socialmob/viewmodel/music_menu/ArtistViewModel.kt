package com.example.sample.socialmob.viewmodel.music_menu

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.feed.CommentDeleteResponseModel
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.CommentResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.music.ArtistRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ArtistViewModel @Inject constructor(
    private val artistRepository: ArtistRepository,
    private var application: Context,
    private var cacheMapper: CacheMapperTrack
) : ViewModel() {
    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val videoListModel: MutableLiveData<MutableList<MusicVideoPayloadVideo>> = MutableLiveData()
    val mSimilarVideosPayload: MutableLiveData<SimilarVideosPayload> = MutableLiveData()
    var artistModel: MutableLiveData<ResultResponse<ArtistDetailsPayload>>? =
        MutableLiveData()
    var genreTrackListModel: MutableLiveData<ResultResponse<List<TrackListCommon>>>? =
        MutableLiveData()
    var commentsCount = 0

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private var createDbPlayListDao: CreateDbPlayList? = null

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        createDbPlayListDao = habitRoomDatabase.createplayListDao()
    }


    fun artistDetails(mAuth: String, mPlayListId: String) {
        artistModel?.postValue(ResultResponse.loading(ArtistDetailsPayload(), ""))
        var response: Response<ArtistDetails>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                artistRepository.getArtistDetails(mAuth, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 ->
                        if (response?.body() != null) {
                            artistModel?.postValue(ResultResponse.success(response?.body()?.payload!!))
                        } else {
                            artistModel?.postValue(
                                ResultResponse.noData(
                                    ArtistDetailsPayload()
                                )
                            )
                        }
                    502, 522, 523, 500 -> {
                        artistModel?.postValue(
                            ResultResponse.error(
                                "", ArtistDetailsPayload()
                            )
                        )
                    }
                    else -> {
                        artistModel?.postValue(ResultResponse.error("", ArtistDetailsPayload()))
                    }
                }

            }.onFailure {
                artistModel?.postValue(ResultResponse.error("", ArtistDetailsPayload()))
            }
        }
    }

    fun artistAllTracks(mAuth: String, mArtistId: String, mOffset: String) {
        if (mOffset == "0") {
            genreTrackListModel?.postValue(ResultResponse.loading(ArrayList(), mOffset))
        } else {
            genreTrackListModel?.postValue(
                ResultResponse.loadingPaginatedList(
                    ArrayList(),
                    mOffset
                )
            )
        }
        var response: Response<GenreListTrackModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                artistRepository
                    .artistAllTracks(
                        mAuth, RemoteConstant.mPlatform,
                        mArtistId, mOffset, RemoteConstant.mCount
                    ).collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()) {
                    200 -> populateGenreTrackModel(response, mOffset)
                    502, 522, 523, 500 -> {
                        genreTrackListModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                }

            }.onFailure {
                genreTrackListModel?.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    private fun populateGenreTrackModel(
        value: Response<GenreListTrackModel>?,
        mOffset: String
    ) {

        if (value?.body()?.payload?.tracks!!.isNotEmpty()) {
            if (mOffset == "0") {
                genreTrackListModel?.postValue(ResultResponse.success(value.body()?.payload?.tracks!!))
            } else {
                genreTrackListModel?.postValue(ResultResponse.paginatedList(value.body()?.payload?.tracks!!))
            }
        } else {
            if (mOffset == "0") {
                genreTrackListModel?.postValue(ResultResponse.noData(ArrayList()))
            } else {
                genreTrackListModel?.postValue(ResultResponse.emptyPaginatedList(value.body()?.payload?.tracks!!))
            }
        }
    }

    fun createPlayList(
        dbPlayList: MutableList<TrackListCommon>?,
        playListName: String,
        callback: (List<PlayListDataClass>) -> Unit
    ) {
        if (dbPlayList?.size ?: 0 > 0) {
            createDbPlayListDao?.deleteAllCreateDbPlayList()
            val mPlayList = cacheMapper.mapFromEntityList(
                dbPlayList ?: ArrayList(), playListName
            )
            createDbPlayListDao?.insertCreateDbPlayList(mPlayList)
            callback(mPlayList)
        }
    }

    fun removeFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                artistRepository
                    .removeFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun addToFavouritesVideo(isLiked: Boolean, mVideoId: String?) {

        val mMethod: String = if (isLiked) {
            RemoteConstant.putMethod
        } else {
            RemoteConstant.deleteMethod
        }
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!!,
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mApiKey,
                ""
            )!!,
            RemoteConstant.mBaseUrl + "/api/v1/trackvideo/" + mVideoId + "/like", mMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        if (isLiked) {
            uiScope.launch(handler) {
                var mAddToFav: Response<Any>? = null
                kotlin.runCatching {
                    artistRepository
                        .addFavouriteVideo(mAuth, RemoteConstant.mPlatform, mVideoId!!).collect {
                            mAddToFav = it
                        }

                }.onSuccess {
                    when (mAddToFav?.code()) {
                        200 -> {
                            println("sucesss")
                        }
                        else -> RemoteConstant.apiErrorDetails(
                            application,
                            mAddToFav?.code() ?: 500,
                            "Add video to favorites"
                        )
                    }
                }.onFailure {
                    AppUtils.showCommonToast(
                        application,
                        "Something went wrong please try again"
                    )
                }
            }
        } else {
            uiScope.launch(handler) {
                var mRemoveFav: Response<Any>? = null
                kotlin.runCatching {
                    artistRepository
                        .removeFavouriteVideo(mAuth, RemoteConstant.mPlatform, mVideoId!!).collect {
                            mRemoveFav = it
                        }
                }.onSuccess {
                    when (mRemoveFav?.code()) {
                        200 -> {
                            println("sucesss")
                        }
                        else -> RemoteConstant.apiErrorDetails(
                            application,
                            mRemoveFav?.code() ?: 500,
                            "Add video to favorites"
                        )
                    }
                }.onFailure {
                    AppUtils.showCommonToast(
                        application,
                        "Something went wrong please try again"
                    )
                }
            }
        }
    }

    fun addToFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                artistRepository
                    .addFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
            }
        }

    }

    val playListModel: MutableLiveData<List<PlaylistCommon>> = MutableLiveData()

    fun getPlayListApi(mAuth: String) {

        uiScope.launch(handler) {
            var getPlayListApi: Response<PlayListResponseModel>? = null
            kotlin.runCatching {
                artistRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code()!!, "Play List api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }

    fun createPlayListApi(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                artistRepository.createPlayList(mAuth, RemoteConstant.mPlatform, createPlayListBody)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refreshPlayList()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun refreshPlayList() {
        // TODO : Refresh Play List
        val mAuthPlayList =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application,
                RemoteConstant.pathPlayList
            )
        getPlayListApi(mAuthPlayList)
    }


    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {

        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                artistRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        AppUtils.showCommonToast(
                            application,
                            response?.body()?.payload?.message
                        )
                        println("")//refresh()
                    }

                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add playlist Track api"
                    )
                }
            }.onFailure {
            }

        }
    }

    fun followArtist(mIsFollowing: Boolean, mArtistId: String, activity: Context) {
        val mAuth: String
        if (mIsFollowing) {

            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + "/api/v1/artist/" + mArtistId + "/follow",
                RemoteConstant.putMethod
            )
            mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    activity, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        } else {
            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(activity, RemoteConstant.mApiKey, "")!!,
                RemoteConstant.mBaseUrl + "/api/v1/artist/" + mArtistId + "/follow",
                RemoteConstant.deleteMethod
            )
            mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    activity, RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        }
        uiScope.launch(handler) {
            var mApiCall: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                if (mIsFollowing) {
                    artistRepository
                        .followArtist(mAuth, RemoteConstant.mPlatform, mArtistId).collect {
                            mApiCall = it
                        }
                } else {
                    artistRepository
                        .unFollowArtist(mAuth, RemoteConstant.mPlatform, mArtistId).collect {
                            mApiCall = it
                        }
                }
            }.onSuccess {
                when (mApiCall?.code()) {
                    200 -> {
                        println("Sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, mApiCall?.code() ?: 500, "Add video to favorites"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }


    fun getVideoList(mAuth: String, mArtistId: String) {
        uiScope.launch(handler) {
            var mGetVideoList: Response<ArtistVideoList>? = null
            kotlin.runCatching {
                artistRepository.getVideoList(mAuth, mArtistId).collect {
                    mGetVideoList = it
                }
            }.onSuccess {
                when (mGetVideoList?.code()) {
                    200 -> {
                        videoListModel.postValue(mGetVideoList?.body()?.payload?.videos!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, mGetVideoList?.code() ?: 500, "get VideoList api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }


    fun getSimilarVideo(mAuth: String, mArtistId: String) {
        var mGetSimilarVideo: Response<SimilarVideos>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                artistRepository.getSimilarVideo(mAuth, mArtistId)?.collect {
                    mGetSimilarVideo = it
                }
            }.onSuccess {
                when (mGetSimilarVideo?.code()) {
                    200 -> {
                        mSimilarVideosPayload.postValue(mGetSimilarVideo?.body()?.payload)
                        commentsCount =
                            mGetSimilarVideo?.body()?.payload?.video?.commentsCount?.toInt()
                                ?: 0
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mGetSimilarVideo?.code() ?: 500,
                        "get similar video api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }

    var commentPaginatedResponseModel: MutableLiveData<ResultResponse<MutableList<CommentResponseModelPayloadComment>>>? =
        MutableLiveData()

    fun callApiComment(mVideoId: String?, mLastId: String?) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!!,
            SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mApiKey,
                ""
            )!!,
            RemoteConstant.mBaseUrl + "/api/v1/trackvideo/" + mVideoId + "/comment/list?lastCommentId=" + mLastId + "&count=10",
            RemoteConstant.mGetMethod
        )
        val mAuth: String = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            application,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        if (mLastId == "0") {
            commentPaginatedResponseModel?.postValue(
                ResultResponse.loading(
                    ArrayList(),
                    mLastId
                )
            )
        } else {
            commentPaginatedResponseModel?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mLastId!!)
            )
        }
        uiScope.launch(handler) {
            var mCallApiComment: Response<CommentResponseModel>? = null
            kotlin.runCatching {
                artistRepository
                    .getVideoComments(
                        mAuth, RemoteConstant.mPlatform, mVideoId!!, mLastId!!, "10"
                    ).collect {
                        mCallApiComment = it
                    }
            }.onSuccess {
                when (mCallApiComment?.code()) {
                    200 -> {
                        if (mCallApiComment?.body()?.payload != null &&
                            mCallApiComment?.body()?.payload?.comments != null && mCallApiComment?.body()?.payload?.comments?.size!! > 0
                        ) {
                            if (mLastId == "0") {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.success(
                                        mCallApiComment?.body()?.payload?.comments!!
                                    )
                                )
                            } else {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        mCallApiComment?.body()?.payload?.comments!!
                                    )
                                )
                            }
                        } else {
                            if (mLastId == "0") {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        commentPaginatedResponseModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mCallApiComment?.code() ?: 500,
                        "Feed comments Api"
                    )
                }

            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }

    fun postComment(mAuth: String, mVideoId: String?, commentPostData: CommentPostDataVideo) {
        var mPostCommentApi: Response<AddCommentResponseVideoModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                artistRepository
                    .postCommentVideo(
                        mAuth, RemoteConstant.mPlatform, mVideoId!!, commentPostData
                    ).collect {
                        mPostCommentApi = it
                    }
            }.onSuccess {
                when (mPostCommentApi?.code()) {
                    200 -> {
                        addCommentToLocalList(mPostCommentApi!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mPostCommentApi?.code() ?: 500,
                        "video postComment api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }

    private fun addCommentToLocalList(response: Response<AddCommentResponseVideoModel>) {
        val mCommentList: MutableList<CommentResponseModelPayloadComment> = ArrayList()
        mCommentList.add(0, response.body()?.payload?.comment!!)
        commentPaginatedResponseModel?.postValue(
            ResultResponse.paginatedList(
                mCommentList
            )
        )
    }

    fun createDBPlayListCommon(dbPlayList: MutableList<TrackListCommon>?, playListName: String) {
        if (dbPlayList?.size ?: 0 > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapper.mapFromEntityList(
                        dbPlayList!!,
                        playListName
                    )
                )
            }
        }
    }

    fun deleteComment(mAuth: String, mArtistId: String, mId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
            }.onSuccess {
                artistRepository.deleteVideoComment(mAuth, RemoteConstant.mPlatform, mArtistId, mId)
            }.onFailure {
                artistRepository.deleteVideoComment(mAuth, RemoteConstant.mPlatform, mArtistId, mId)
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    inner class DbPlayListAsyncTask internal constructor(private val mAsyncTaskDao: CreateDbPlayList) :
        AsyncTask<List<PlayListDataClass>, Void, Void>() {

        override fun doInBackground(vararg params: List<PlayListDataClass>): Void? {

            return null
        }
    }

    var mentionProfileResponseModel: MutableLiveData<MutableList<Profile>> =
        MutableLiveData()
    var mMentionApiCall: Job? = null

    fun getMentions(mContext: Context, mMnention: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mMentionProfile +
                    RemoteConstant.pathQuestionmark + "name=" + mMnention,
            RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        if (mMentionApiCall != null) {
            mMentionApiCall?.cancel()
        }
        mMentionApiCall = uiScope.launch(handler) {
            var response: Response<MentionProfileResponseModel>? = null
            kotlin.runCatching {
                artistRepository
                    .mentionProfile(
                        mAuth, RemoteConstant.mPlatform, mMnention
                    ).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> mentionProfileResponseModel.postValue(response?.body()?.payload?.profiles)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get Mention Profile api"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun postVideoAnalytics(mArtistId: String, mDuration: DurationModel, mAuth: String) {
        GlobalScope.launch(Dispatchers.IO) {
            var response: Response<CommentDeleteResponseModel>? = null
            kotlin.runCatching {
                artistRepository.videoAnalytcs(mAuth, mArtistId, mDuration).collect {
                    response = it
                }
            }.onSuccess {

            }.onFailure {
                it.printStackTrace()
            }
        }
    }
}