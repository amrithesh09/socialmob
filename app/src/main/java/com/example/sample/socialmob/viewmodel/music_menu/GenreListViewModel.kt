package com.example.sample.socialmob.viewmodel.music_menu

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperPodCast
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioPodCastListDao
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.music.GenreRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class GenreListViewModel @Inject constructor(
    private val genreRepository: GenreRepository,
    private val application: Context,
    private var cacheMapper: CacheMapperTrack,
    private var cacheMapperPodCast: CacheMapperPodCast
) : ViewModel() {

    var title = ""
    var imageUrl = ""
    var mArtistName = ""
    var mArtistId = ""
    var mArtistAlbumImage = ""
    var mGenreName = ""
    var profileId = ""
    var profileUsername = ""
    var podcastName = ""
    var description = ""
    var isVisibleShare = false
    var isVisibleItem = false

    var genreTrackListModel: MutableLiveData<ResultResponse<List<TrackListCommon>>>? =
        MutableLiveData()

    var isTrackDeleted: MutableLiveData<DeleteTrackResponseModelPayload>

    var radioPodCastListModel: LiveData<List<RadioPodCastEpisode>>

    private var auth = ""
    private var mId = ""
    private var radioPodCastListDao: RadioPodCastListDao? = null

    var isLoadCompletedradioPodcast: Boolean = false
    var isLoadingradioPodcast: Boolean = false


    var isObservePodcast: MutableLiveData<Boolean>? = MutableLiveData()
    var isEmptyPodcast: MutableLiveData<Boolean>? = MutableLiveData()
    var isCreatedPlayList: MutableLiveData<Boolean>? = MutableLiveData()
    var isCreatedCompletePlayList: Boolean = false
    private var createDbPlayListDao: CreateDbPlayList? = null


    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null
    val playListModel: MutableLiveData<List<PlaylistCommon>> = MutableLiveData()


    private var trackDownloadDao: TrackDownload? = null
    var downloadFalseTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null
    var downloadTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        radioPodCastListDao = habitRoomDatabase.radioPodcastListDao()

        radioPodCastListModel = radioPodCastListDao?.getRadioPodCastList()!!
        isTrackDeleted = MutableLiveData()

        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!

        trackDownloadDao = habitRoomDatabase.trackDownloadDao()
        downloadTrackListModel = trackDownloadDao?.getAllTrackDownload()!!
        downloadFalseTrackListModel = trackDownloadDao?.getTrackDownloadedFalse("false")!!
    }

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)
    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun getAlbumTrack(mAuth: String, mGenreId: String) {

        genreTrackListModel?.postValue(ResultResponse.loading(ArrayList(), ""))

        uiScope.launch(handler) {
            var response: Response<GenreListTrackModel>? = null
            kotlin.runCatching {
                genreRepository.getAlbumTrack(mAuth, RemoteConstant.mPlatform, mGenreId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body() != null &&
                            response?.body()?.payload != null &&
                            response?.body()?.payload?.album != null
                        ) {
                            if (response?.body()?.payload?.album?.artist != null &&
                                response?.body()?.payload?.album?.artist?.artistName != null
                            ) {
                                mArtistName =
                                    response?.body()?.payload?.album?.artist?.artistName!!
                            }
                            if (response?.body()?.payload?.album?.artistId != null) {
                                mArtistId =
                                    response?.body()?.payload?.album?.artistId!!
                            }
                            if (response?.body()?.payload?.album?.isSingles != null &&
                                response?.body()?.payload?.album?.isSingles == "false"
                            ) {

                                if (response?.body()?.payload?.album?.albumMedia != null &&
                                    response?.body()?.payload?.album?.albumMedia?.coverImage != null
                                ) {
                                    mArtistAlbumImage =
                                        response?.body()?.payload?.album?.albumMedia?.coverImage!!
                                }

                            } else {
                                if (response?.body()?.payload?.album?.singlesMedia != null &&
                                    response?.body()?.payload?.album?.singlesMedia?.coverImage != null
                                ) {
                                    mArtistAlbumImage =
                                        response?.body()?.payload?.album?.singlesMedia?.coverImage!!
                                }
                            }
                        }
                        mGenreName =
                            response?.body()?.payload?.album?.albumName ?: ""

                        if (response?.body()?.payload?.tracks?.size!! > 0) {
                            genreTrackListModel?.postValue(ResultResponse.success(response?.body()?.payload?.tracks!!))
                        } else {
                            genreTrackListModel?.postValue(ResultResponse.noData(ArrayList()))
                        }
                    }
                    502, 522, 523, 500 -> {
                        genreTrackListModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Genre playList api"
                    )
                }
            }.onFailure {
                genreTrackListModel?.postValue(ResultResponse.error("", ArrayList()))
            }

        }

    }

    fun getStationTrackList(mAuth: String, mGenreId: String) {
        genreTrackListModel?.postValue(ResultResponse.loading(ArrayList(), "0"))
        uiScope.launch(handler) {
            var response: Response<GenreListTrackModel>? = null
            kotlin.runCatching {
                genreRepository
                    .getStationTrackList(mAuth, RemoteConstant.mPlatform, mGenreId, "50").collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> populateGenreTrackModel(response, "0")
                    502, 522, 523, 500 -> {
                        genreTrackListModel?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Genre playList api"
                    )
                }
            }.onFailure {
                genreTrackListModel?.postValue(ResultResponse.error("", ArrayList()))
            }
        }

    }

    fun getGenreTrackList(mAuth: String, mGenreId: String, mOffset: String, mCount: String) {
        var response: Response<GenreListTrackModel>? = null
        if (mOffset == "0") {
            genreTrackListModel?.postValue(ResultResponse.loading(ArrayList(), mOffset))
        } else {
            genreTrackListModel?.postValue(
                ResultResponse.loadingPaginatedList(
                    ArrayList(),
                    mOffset
                )
            )
        }

        uiScope.launch(handler) {
            kotlin.runCatching {
                genreRepository.getGenreTrackList(
                    mAuth, RemoteConstant.mPlatform, mGenreId, mOffset, mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> populateGenreTrackModel(response, mOffset)
                    502, 522, 523, 500 -> {
                        genreTrackListModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Genre playList api"
                    )
                }
            }.onFailure {
                genreTrackListModel?.postValue(ResultResponse.error("", ArrayList()))
            }
        }

    }

    private fun populateGenreTrackModel(value: Response<GenreListTrackModel>?, mOffset: String) {

        if (value?.body()?.payload?.tracks!!.isNotEmpty()) {
            if (mOffset == "0") {
                genreTrackListModel?.postValue(ResultResponse.success(value.body()?.payload?.tracks!!))
            } else {
                genreTrackListModel?.postValue(ResultResponse.paginatedList(value.body()?.payload?.tracks!!))
            }
        } else {
            if (mOffset == "0") {
                genreTrackListModel?.postValue(ResultResponse.noData(ArrayList()))
            } else {
                genreTrackListModel?.postValue(ResultResponse.emptyPaginatedList(value.body()?.payload?.tracks!!))
            }
        }
    }


    fun getPodcastEpisodeList(
        mAuth: String,
        id: String,
        mOffset: String,
        mCount: String,
        mAscDesc: String
    ) {
        isLoadingradioPodcast = true

        uiScope.launch(handler) {
            var response: Response<RadioPodCastListModel>? = null
            kotlin.runCatching {
                genreRepository
                    .getPodCastEpisodeList(
                        mAuth, RemoteConstant.mPlatform, id, mOffset, mCount, mAscDesc
                    ).collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()!!) {
                    200 -> savePodcastEpisode(response!!, mOffset)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "getPodcastEpisodeList api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun getPlayListTrack(mAuth: String, id: String) {

        genreTrackListModel?.postValue(ResultResponse.loading(ArrayList(), ""))
        auth = mAuth
        mId = id
        uiScope.launch(handler) {
            var response: Response<SinglePlayListResponseModel>? = null
            kotlin.runCatching {
                genreRepository.getPlayListTrack(mAuth, id).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.tracks!!.isNotEmpty()) {
                            genreTrackListModel?.postValue(ResultResponse.success(response?.body()?.payload?.tracks!!))

                            if (response?.body()?.payload?.playlist?.playlistType == "system-generated"
                                || response?.body()?.payload?.playlist?.playlistType == "admin-generated"
                            ) {
                                if (response?.body()?.payload?.playlist?.media?.coverImage != null) {
                                    imageUrl =
                                        response?.body()?.payload?.playlist?.media?.coverImage!!
                                }
                            } else {
                                if (response?.body()?.payload?.playlist?.icon?.fileUrl != null) {
                                    imageUrl =
                                        response?.body()?.payload?.playlist?.icon?.fileUrl!!
                                }
                            }


                            if (response?.body()?.payload != null &&
                                response?.body()?.payload?.playlist != null &&
                                response?.body()?.payload?.playlist?.playlistName != null
                            ) {
                                mGenreName =
                                    response?.body()?.payload?.playlist?.playlistName!!
                                isVisibleShare =
                                    if (response?.body()?.payload?.playlist?.isPrivate.equals(
                                            ""
                                        )
                                    ) {
                                        true
                                    } else response?.body()?.payload?.playlist?.isPrivate.equals(
                                        "0"
                                    )
                                if (response?.body()?.payload?.playlist != null &&
                                    response?.body()?.payload?.playlist?.profileId != null
                                ) {
                                    profileId =
                                        response?.body()?.payload?.playlist?.profileId!!
                                }
                                profileUsername = if (response?.body()?.payload?.playlist != null &&
                                    response?.body()?.payload?.playlist?.profile != null &&
                                    response?.body()?.payload?.playlist?.profile?.username != null
                                ) {
                                    response?.body()?.payload?.playlist?.profile?.username!!
                                } else {
                                    ""
                                }

                            }

                        } else {
                            genreTrackListModel?.postValue(ResultResponse.noData(ArrayList()))
                        }
                    }
                    502, 522, 523, 500 -> {
                        genreTrackListModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "get CreateDbPlayList api"
                    )

                }
            }.onFailure {
                genreTrackListModel?.postValue(ResultResponse.error("", ArrayList()))
            }
        }

    }

    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {

        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                genreRepository
                    .addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("")
                        AppUtils.showCommonToast(
                            application,
                            response?.body()?.payload?.message
                        )
                    }//refresh()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add playlist Track api"
                    )
                }

            }.onFailure {
            }
        }
    }

    fun removeFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                genreRepository
                    .removeFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }


    fun deleteTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {

        uiScope.launch(handler) {
            var response: Response<DeleteTrackResponseModel>? = null
            kotlin.runCatching {
                genreRepository
                    .deleteTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> deleteTrackFromDb(
                        response?.body()
                    )
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Delete Track api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun deleteTrackFromDb(body: DeleteTrackResponseModel?) {


        AppUtils.showCommonToast(application, "Track removed from playlist")
//        getPlayListTrack(auth, mId)
//        singlePlayListDao!!.deleteTrackId(mTrackId)
        isTrackDeleted.postValue(body!!.payload)
//        singlePlayListResponseModelPayloadTrackModel =
//            singlePlayListDao?.getAllSinglePlayList(title)!!
//
//        //TODO : Call CreateDbPlayList for new list ( Since first Song is Playlist Image)
//        val mAuth = RemoteConstant.getAuthWithoutOffsetAndCount(
//            application,
//            RemoteConstant.pathPlayList
//        )
//        getPlayList(mAuth)
    }

    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                genreRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "Play List api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(application, "Something went wrong please try again")
            }
        }
    }

    fun filterRadioPodCast(mTitle: String) {
        title = mTitle
        radioPodCastListModel = radioPodCastListDao?.getRadioPodCastList()!!
    }

    private fun savePodcastEpisode(value: Response<RadioPodCastListModel>, mOffset: String) {
        if (mOffset == "0") {
            radioPodCastListDao!!.deleteAllRadioPodCastList()
            if (value.body()!!.payload!!.episodes!!.isEmpty()) {
                isEmptyPodcast?.postValue(true)
            }
        }
        doAsync {
            radioPodCastListDao?.insertRadioPodCastList(value.body()?.payload?.episodes!!)
            isObservePodcast?.postValue(true)
        }

        if (value.body()?.payload?.podcast != null) {
            if (value.body()?.payload?.podcast?.cover_image != null) {
                mArtistAlbumImage = value.body()?.payload?.podcast?.cover_image!!
            }
            if (value.body()?.payload?.podcast?.podcast_name != null) {
                podcastName = value.body()?.payload?.podcast?.podcast_name!!
            }
            if (value.body()?.payload?.podcast?.description != null) {
                description = value.body()?.payload?.podcast?.description!!
            }
        }
        isLoadingradioPodcast = false
        isLoadCompletedradioPodcast = value.body()!!.payload!!.episodes!!.isEmpty()

    }

    fun createDBPodCastTrackPlayList(
        dbPlayList: MutableList<RadioPodCastEpisode>?,
        playListName: String, callback: (List<PlayListDataClass>) -> Unit
    ) {
        doAsync {
            createDbPlayListDao?.deleteAllCreateDbPlayList()
            val mPodCastList = cacheMapperPodCast.mapFromEntityListPodCast(
                dbPlayList!!, playListName
            )
            createDbPlayListDao?.insertCreateDbPlayList(mPodCastList)
            callback(mPodCastList)
        }
    }

    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                genreRepository
                    .createPlayList(mAuth, RemoteConstant.mPlatform, createPlayListBody).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refresh()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun refresh() {
        val mAuthPlayList =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application,
                RemoteConstant.pathPlayList
            )
        getPlayList(mAuthPlayList)
    }

    fun createPlayList(
        dbPlayList: MutableList<TrackListCommon>?,
        playListName: String,
        callback: (List<PlayListDataClass>) -> Unit
    ) {
        createDbPlayListDao?.deleteAllCreateDbPlayList()
        isCreatedPlayList?.postValue(false)
        isCreatedCompletePlayList = false
        if (dbPlayList?.size ?: 0 > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()

                val mTrackList: List<PlayListDataClass> = cacheMapper.mapFromEntityList(
                    dbPlayList ?: ArrayList(), playListName
                )
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapper.mapFromEntityList(
                        dbPlayList ?: ArrayList(), playListName
                    )
                )
                isCreatedCompletePlayList = true
                isCreatedPlayList?.postValue(true)
                callback(mTrackList)
            }
        }
    }

    fun addToFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                genreRepository
                    .addFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun addToDownload(mTrackDetails: TrackListCommon) {
        val list: MutableList<TrackListCommonDb> = ArrayList()
        val trackListCommonDbModel = TrackListCommonDb(
            mTrackDetails.id!!,
            mTrackDetails.numberId!!,
            mTrackDetails.trackName!!,
            mTrackDetails.media?.coverFile!!,
            mTrackDetails.media?.trackFile!!,
            mTrackDetails.artist?.artistName!!,
            mTrackDetails.genre?.genreName!!,
            "false", "false"
        )

        list.add(trackListCommonDbModel)
        doAsync {
            trackDownloadDao?.insertTrackDownload(list)
        }
    }

}