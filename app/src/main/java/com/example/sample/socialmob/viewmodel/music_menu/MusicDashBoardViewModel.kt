package com.example.sample.socialmob.viewmodel.music_menu

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperPodCast
import com.example.sample.socialmob.di.util.CacheMapperPodCastHistory
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.dao.DbPodcastHistoryDao
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioDao
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.music.MusicDashBoardRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DbTrackProgress
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.PodCastProgress
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MusicDashBoardViewModel @Inject constructor(
    private val mMusicDashBoardRepository: MusicDashBoardRepository,
    private var application: Context,
    private var cacheMapper: CacheMapperTrack,
    private var cacheMapperPodCastHistory: CacheMapperPodCastHistory,
    private var cacheMapperPodCast: CacheMapperPodCast
) : ViewModel() {

    var radioImage = ""
    var radioStationName = ""

    private var podcastHistoryDao: DbPodcastHistoryDao? = null
    private var radioDao: RadioDao? = null

    val musicVideos: MutableLiveData<ResultResponse<MutableList<MusicVideoPayloadVideo>>> =
        MutableLiveData()
    var genresMutableLiveData: MutableLiveData<ResultResponse<MutableList<Genre>>>? =
        MutableLiveData()
    var musicMetaLiveData: MutableLiveData<ResultResponse<MusicMetaResponse>>? =
        MutableLiveData()
    var podcastsMetaLiveData: MutableLiveData<ResultResponse<PodcastMeta>>? =
        MutableLiveData()
    var genreTrackListModel: MutableLiveData<ResultResponse<List<TrackListCommon>>>? =
        MutableLiveData()

    var podcastHistoryModel: LiveData<List<PodcastHistoryTrack>>
    var podcastHistoryModelLastListen: LiveData<List<PodcastHistoryTrack>>
    var podCastModelLiveData: MutableLiveData<ResultResponse<MutableList<Podcast>>>? =
        MutableLiveData()
    val radioModel: LiveData<List<Station>>
    val playListModel: MutableLiveData<MutableList<PlaylistCommon>> = MutableLiveData()

    private var createDbPlayListDao: CreateDbPlayList? = null
    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null

    var isLoadCompletedPodcast: Boolean = false
    var isLoadingPodcast: Boolean = false
    var isLoadingMusicVideo: Boolean = false

    private var trackDownloadDao: TrackDownload? = null
    var downloadFalseTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null
    var mdbTrackProgressTrackList: LiveData<List<PodCastProgress>>? = MutableLiveData()
    private var dbTrackProgressDao: DbTrackProgress? = null

    init {

        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)

        podcastHistoryDao = habitRoomDatabase.dbPodcastHistory()
        radioDao = habitRoomDatabase.radioDao()

        podcastHistoryModel = podcastHistoryDao?.getAllDbPodcastHistory()!!
        podcastHistoryModelLastListen = podcastHistoryDao?.getAllDbPodcastHistoryLast()!!
        radioModel = radioDao?.getAllRadio()!!

        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!

        trackDownloadDao = habitRoomDatabase.trackDownloadDao()
        downloadFalseTrackListModel = trackDownloadDao?.getTrackDownloadedFalse("false")!!
        dbTrackProgressDao = habitRoomDatabase.trackProgress()
        mdbTrackProgressTrackList = dbTrackProgressDao?.getAllDbTrackProgress()
    }

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


    fun getGenreApiCall(mAuth: String) {
        uiScope.launch(handler) {
            mMusicDashBoardRepository.getGenre(mAuth, RemoteConstant.mPlatform).collect {
                genresMutableLiveData?.postValue(it)
            }
        }
    }


    fun getPodCastApi(mAuth: String, mOffset: String, mCount: String, mSortBy: String) {
        isLoadingPodcast = true
        uiScope.launch(handler) {
            kotlin.runCatching {
                mMusicDashBoardRepository
                    .getPodCastApi(
                        mAuth, RemoteConstant.mPlatform, mOffset, mCount, mSortBy
                    )
                    .collect {
                        podCastModelLiveData?.postValue(it)
                        isLoadingPodcast = false
                    }
            }
        }
    }

    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                mMusicDashBoardRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists?.toMutableList())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "PlayList Api"
                    )
                }
            }.onFailure {
                val mAuthNew =
                    RemoteConstant.getAuthWithoutOffsetAndCount(
                        application,
                        RemoteConstant.pathPlayList
                    )
                getPlayList(mAuthNew)
            }
        }
    }

    private fun saveRadioModel(value: Response<RadioResponseModel>) {
        doAsync {
            radioDao?.deleteAllRadio()
            radioDao?.insertRadio(value.body()?.payload?.stations ?: ArrayList())
        }
        if (value.body()?.payload?.stations?.isNotEmpty()!!) {
            radioStationName = value.body()?.payload?.stations?.get(0)?.name ?: ""
            radioImage = value.body()?.payload?.stations?.get(0)?.albumArt ?: ""
        }
    }


    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                mMusicDashBoardRepository
                    .addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refreshPlayList(response!!)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "get CreateDbPlayList Track"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        AppUtils.showCommonToast(application, value.body()?.payload?.message)
    }


    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {

        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                mMusicDashBoardRepository
                    .createPlayList(mAuth, RemoteConstant.mPlatform, createPlayListBody).collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        val mAuthNew =
                            RemoteConstant.getAuthWithoutOffsetAndCount(
                                application,
                                RemoteConstant.pathPlayList
                            )
                        getPlayList(mAuthNew)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun createDBPodCastHistoryTrackPlayList(
        dbPlayList: MutableList<PodcastHistoryTrack>?,
        playListName: String,  callback: (List<PlayListDataClass>) -> Unit
    ) {
        doAsync {
            createDbPlayListDao?.deleteAllCreateDbPlayList()
            val mPodCastList=cacheMapperPodCastHistory.mapFromEntityListPodCastHistory(
                dbPlayList!!, playListName
            )
            createDbPlayListDao?.insertCreateDbPlayList(mPodCastList)
            callback(mPodCastList)
        }
    }


    fun createDBPlayList(dbPlayList: MutableList<RadioPodCastEpisode>?, playListName: String,
                         callback: (List<PlayListDataClass>) -> Unit) {
        try {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                val mPodCastList=cacheMapperPodCast.mapFromEntityListPodCast(
                    dbPlayList!!, playListName
                )
                createDbPlayListDao?.insertCreateDbPlayList(mPodCastList)
                callback(mPodCastList)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun createDBPlayListCommon(
        dbPlayList: MutableList<TrackListCommon>?, playListName: String,
        callback: (List<PlayListDataClass>) -> Unit
    ) {
        if (dbPlayList?.size ?: 0 > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                val mTrackList =
                    cacheMapper.mapFromEntityList(dbPlayList ?: ArrayList(), playListName)
                createDbPlayListDao?.insertCreateDbPlayList(mTrackList)
                callback(mTrackList)
            }
        }
    }

    fun getMetaData(mAuth: String) {
        uiScope.launch(handler) {
            musicMetaLiveData?.postValue(ResultResponse.loading(MusicMetaResponse(), ""))
            var mApiMusicMeta: Response<MusicMetaResponse>? = null
            kotlin.runCatching {
                mMusicDashBoardRepository.getMusicMetaData(mAuth, RemoteConstant.mPlatform)
                    .collect {
                        mApiMusicMeta = it
                    }
            }.onSuccess {
                when (mApiMusicMeta?.code()) {
                    200 -> {
                        val mResult: ResultResponse<MusicMetaResponse> =
                            ResultResponse.success(mApiMusicMeta?.body()!!)
                        musicMetaLiveData?.postValue(mResult)
                    }
                    502, 522, 523, 500 -> {
                        musicMetaLiveData?.postValue(ResultResponse.error(""))
                    }
                    else -> {
                        RemoteConstant.apiErrorDetails(
                            application, mApiMusicMeta?.code() ?: 500, "Meta Api"
                        )
                    }
                }
            }.onFailure {
                musicMetaLiveData?.postValue(ResultResponse.error(""))
            }
        }
    }


//    private fun getRadioList(mAuth: String) {
//        uiScope.launch(handler) {
//            var mGetRadioList: Response<RadioResponseModel>? = null
//            kotlin.runCatching {
//                mMusicDashBoardRepository.getRadio(mAuth, RemoteConstant.mPlatform).collect {
//                    mGetRadioList = it
//                }
//            }.onSuccess {
//                when (mGetRadioList?.code()) {
//                    200 -> {
//                        saveRadioModel(mGetRadioList!!)
//                    }
//                    else -> RemoteConstant.apiErrorDetails(
//                        application, mGetRadioList?.code() ?: 500, "Radio List Api"
//                    )
//                }
//            }.onFailure {
//                AppUtils.showCommonToast(
//                    application,
//                    "Something went wrong please try again"
//                )
//            }
//        }
//    }


    fun podCastMeta(mAuth: String) {
        uiScope.launch(handler) {
            var mPodCastMetaApi: Response<PodcastMeta>? = null
            podcastsMetaLiveData?.postValue(ResultResponse.loading(PodcastMeta(), ""))
            kotlin.runCatching {
                mMusicDashBoardRepository.podCastMeta(mAuth, RemoteConstant.mPlatform).collect {
                    mPodCastMetaApi = it
                }
            }.onSuccess {
                when (mPodCastMetaApi?.code()) {
                    200 -> {
                        val mResult: ResultResponse<PodcastMeta> =
                            ResultResponse.success(mPodCastMetaApi?.body()!!)
                        podcastsMetaLiveData?.postValue(mResult)

                        val mAuthRadioList =
                            RemoteConstant.getAuthWithoutOffsetAndCount(
                                application,
                                RemoteConstant.pathRadio
                            )
//                        getRadioList(mAuthRadioList)
                    }
                    502, 522, 523, 500 -> {
                        podcastsMetaLiveData?.postValue(ResultResponse.error(""))
                        val mAuthRadioList =
                            RemoteConstant.getAuthWithoutOffsetAndCount(
                                application,
                                RemoteConstant.pathRadio
                            )
//                        getRadioList(mAuthRadioList)
                    }
                    else -> {

                        RemoteConstant.apiErrorDetails(
                            application,
                            mPodCastMetaApi?.code() ?: 500,
                            "Podcast Meta Api"
                        )
                    }
                }
            }.onFailure {
                podcastsMetaLiveData?.postValue(ResultResponse.error(""))
            }
        }
    }

    fun deletePlayList(mAuth: String, mId: String) {
        var response: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                mMusicDashBoardRepository.deletePlayList(mAuth, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        val mAuthNew =
                            RemoteConstant.getAuthWithoutOffsetAndCount(
                                application,
                                RemoteConstant.pathPlayList
                            )
                        getPlayList(mAuthNew)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Delete Play List Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun getStationTrackList(mAuth: String, mGenreId: String) {
        genreTrackListModel?.postValue(ResultResponse.loading(ArrayList(), "0"))
        uiScope.launch(handler) {
            var response: Response<GenreListTrackModel>? = null
            kotlin.runCatching {
                mMusicDashBoardRepository.getStationTrackList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mGenreId,
                    "50"
                ).collect {
                    response = it
                }

            }.onSuccess {
                when (response!!.code()) {
                    200 -> populateGenreTrackModel(response, "0")
                    502, 522, 523, 500 -> {
                        genreTrackListModel?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Genre playList api"
                    )
                }
            }.onFailure {
                genreTrackListModel?.postValue(ResultResponse.error("", ArrayList()))
            }
        }

    }

    private fun populateGenreTrackModel(value: Response<GenreListTrackModel>?, mOffset: String) {

        if (value?.body()?.payload?.tracks!!.isNotEmpty()) {
            if (mOffset == "0") {
                genreTrackListModel?.postValue(ResultResponse.success(value.body()?.payload?.tracks!!))
            } else {
                genreTrackListModel?.postValue(ResultResponse.paginatedList(value.body()?.payload?.tracks!!))
            }
        } else {
            if (mOffset == "0") {
                genreTrackListModel?.postValue(ResultResponse.noData(ArrayList()))
            } else {
                genreTrackListModel?.postValue(ResultResponse.emptyPaginatedList(value.body()?.payload?.tracks!!))
            }
        }


    }

    fun getMusicVideo(mCount: String) {
        isLoadingMusicVideo = true
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + "/api/v1/trackvideo/list?offset=" + mCount + "&count=" + RemoteConstant.mCount,
            RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            application, RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        var mGetSimilarVideo: Response<MusicVideoList>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                mMusicDashBoardRepository.getMusicVideo(mAuth, mCount).collect {
                    mGetSimilarVideo = it
                }
            }.onSuccess {
                when (mGetSimilarVideo?.code()) {
                    200 -> {
                        if (mCount == "0") {
                            musicVideos.postValue(ResultResponse.success(mGetSimilarVideo?.body()?.payload?.videos!!))
                        } else {
                            if (mGetSimilarVideo?.body()?.payload?.videos?.size ?: 0 > 0) {
                                musicVideos.postValue(
                                    ResultResponse.paginatedList(mGetSimilarVideo?.body()?.payload?.videos!!)
                                )
                            } else {
                                musicVideos.postValue(
                                    ResultResponse.loadingCompletedWithEmpty(mGetSimilarVideo?.body()?.payload?.videos!!)
                                )
                            }
                        }
                    }
                    else -> {
                    }
                }
                isLoadingMusicVideo = false
            }.onFailure {
                isLoadingMusicVideo = false
                AppUtils.showCommonToast(application, "Something went wrong please try again")
            }
        }
    }
}



