package com.example.sample.socialmob.viewmodel.music_menu

import android.content.Context
import android.os.AsyncTask
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperPodCast
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.chat.FailedFiles
import com.example.sample.socialmob.model.login.LoginResponse
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.dao.ContactListDao
import com.example.sample.socialmob.repository.dao.ContactModel
import com.example.sample.socialmob.repository.dao.DbChatFileList
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioPodCastListDao
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.repo.music.NowPlayingRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.ui.home_module.HomeActivity
import com.example.sample.socialmob.view.ui.home_module.chat.model.PreSignedPayLoadList
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.*
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.viewmodel.login.LoginViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class NowPlayingViewModel @Inject constructor(
    private val nowPlayingRepository: NowPlayingRepository,
    private val application: Context,
    private var cacheMapper: CacheMapperTrack,
    private var cacheMapperPodCast: CacheMapperPodCast
) : ViewModel() {


    var title = ""
    var isInternetAvailable: Boolean? = null
    var isInternetAvailableLayout: Boolean? = null
    var imageUrl: ObservableField<String>? = null

    var radioPodCastListModel: LiveData<List<RadioPodCastEpisode>>
    val playListModel: MutableLiveData<List<PlaylistCommon>> = MutableLiveData()
    val profile: LiveData<Profile>
    var name: ObservableField<String>? = null
    var location: ObservableField<String>? = null
    var followerCount: ObservableField<String>? = null
    var followingCount: ObservableField<String>? = null

    private var radioPodCastListDao: RadioPodCastListDao? = null
    private var contactListDao: ContactListDao? = null
    private var loginDao: LoginDao? = null
    var mDbChatFileList: DbChatFileList? = null

    private var createDbPlayListDao: CreateDbPlayList? = null

    private var trackDownloadDao: TrackDownload? = null
    var downloadFalseTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null
    var downloadTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null
    var mDbChatFileListLiveData: LiveData<List<PreSignedPayLoadList>>? = null
    var downloadTrueTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null

    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null
    var trackDetailsModel: MutableLiveData<TrackDetailsModel>
    var podcastResponseModel: MutableLiveData<PodcastResponseModel>
    var dbTrackAnalyticsDao: DbTrackAnalytics =
        DefaultPlaylistHandler.habitRoomDatabase.trackAnalytics()
    var dbMusicTrackAnalyticsDao: TrackAnalytics =
        DefaultPlaylistHandler.habitRoomDatabase.musicTrackAnalytics()
    var mAnalyticsData: LiveData<List<PodCastAnalytics>>? = null
    var mTrackAnalyticsData: LiveData<List<TrackAnalyticsData>>? = null
    var contactList: LiveData<List<ContactModel>>? = null
    var mTrackOfflineDelete: MutableLiveData<MutableList<String>>? = MutableLiveData()


    var isCreatedPlayList: MutableLiveData<Boolean>? = MutableLiveData()
    var mNotificationCount: MutableLiveData<Int>? = MutableLiveData()
    var mUnReadMessage: MutableLiveData<String>? = MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        radioPodCastListDao = habitRoomDatabase.radioPodcastListDao()
        contactListDao = habitRoomDatabase.contactListDaoDao()
        loginDao = habitRoomDatabase.loginDao()

        radioPodCastListModel = radioPodCastListDao?.getRadioPodCastList()!!
        profile = loginDao?.getAllProfileData()!!

        trackDetailsModel = MutableLiveData()
        podcastResponseModel = MutableLiveData()

        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        mDbChatFileList = habitRoomDatabase.dbChatFileList()
        mDbChatFileListLiveData = mDbChatFileList?.getAllFiles()!!
        trackDownloadDao = habitRoomDatabase.trackDownloadDao()
        downloadTrackListModel = trackDownloadDao?.getAllTrackDownload()!!
        downloadFalseTrackListModel = trackDownloadDao?.getTrackDownloadedFalse("false")!!
        downloadTrueTrackListModel = trackDownloadDao?.getTrackDownloadedTrue("true")!!

        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!
        mAnalyticsData = dbTrackAnalyticsDao.getAllTrackAnalytics()
        mTrackAnalyticsData = dbMusicTrackAnalyticsDao.getAllTrackAnalytics()
        contactList = contactListDao?.getAllContacts("false")
    }

    fun filterGenreList(mTitle: String) {
        title = mTitle
    }

    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                nowPlayingRepository.createPlayList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    createPlayListBody
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refreshPlayList()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List api"
                    )
                }
            }.onFailure {
            }
        }
    }


    private fun refreshPlayList() {
        // TODO : Refresh Play List
        val mAuthPlayList =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application,
                RemoteConstant.pathPlayList
            )
        getPlayList(mAuthPlayList)
    }

    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                nowPlayingRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "Play List api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }

    fun podcastAnalytcs(mAuth: String, mData: PodCastAnalyticsData) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                nowPlayingRepository.podcastAnalytcs(mAuth, mData).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> dbTrackAnalyticsDao.deleteAllTrackAnalytics()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Podcast analytics"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }


    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                nowPlayingRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> doAsync {
                        refreshPlayList(response!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "TrendingTrack api"
                    )
                }
            }.onFailure {
            }
        }
    }


    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        refreshPlayList()
        AppUtils.showCommonToast(application, value.body()?.payload?.message)
    }


    fun registerFcm(mAuth: String, token: String) {
        GlobalScope.launch(Dispatchers.IO) {
            kotlin.runCatching {
                nowPlayingRepository
                    .postDeviceToken(mAuth, RemoteConstant.mPlatform, token)
            }.onSuccess {
            }.onFailure {
            }
        }
    }

    fun getMyProfile(mAuth: String, mProfileId: String) {
        uiScope.launch(handler) {
            var response: Response<LoginResponse>? = null
            kotlin.runCatching {
                nowPlayingRepository.getMyProfile(mAuth, RemoteConstant.mPlatform, mProfileId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        saveProfileData(response!!)
                        if (response?.body() != null && response?.body()?.payload != null && response?.body()?.payload?.profile != null
                        ) {
                            if (response?.body()?.payload?.profile?.notificationCount != null) {
                                mNotificationCount?.postValue(response?.body()?.payload?.profile?.notificationCount)
                            }
                            if (response?.body()?.payload?.profile?.unreadMessageCount != null) {
                                mUnReadMessage?.postValue(
                                    response?.body()?.payload?.profile?.unreadMessageCount
                                        ?: "0"
                                )
                            }
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "User Profile Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun saveProfileData(response: Response<LoginResponse>) {
        if (response.body()!!.payload != null && response.body()!!.payload!!.profile != null) {
            saveProfile(response)
        }
    }

    private fun saveProfile(value: Response<LoginResponse>) {
        LoginViewModel.SaveProfile(this.loginDao!!).execute(value.body()!!.payload!!.profile)
    }


    fun createDbPlayListModelNotification(
        dbPlayList: MutableList<TrackListCommon>?,
        playListName: String
    ) {
        isCreatedPlayList?.postValue(false)
        if (dbPlayList!!.size > 0) {
            DbPlayListAsyncTask(this.createDbPlayListDao!!).execute(
                cacheMapper.mapFromEntityList(
                    dbPlayList, playListName
                )
            )
        }
    }

    fun createDbPlayListModelNotificationPodcast(
        dbPlayList: MutableList<RadioPodCastEpisode>?,
        playListName: String
    ) {
        if (dbPlayList!!.size > 0) {
            DbPlayListAsyncTask(this.createDbPlayListDao!!).execute(
                cacheMapperPodCast.mapFromEntityListPodCast(
                    dbPlayList, playListName
                )
            )
        }
    }

    fun getTrackDetails(mAuth: String, profileAnthemTrackId: String?) {
        var response: Response<TrackDetailsModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                nowPlayingRepository.getTrackDetails(
                    mAuth,
                    RemoteConstant.mPlatform,
                    profileAnthemTrackId!!
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        trackDetailsModel.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "TrendingTrack api"
                    )
                }
            }.onFailure {
                it.printStackTrace()
                it.printStackTrace()
            }
        }
    }

    suspend fun getPodCastDetails(mAuth: String, mSongId: String) {
        var response: Response<PodcastResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                nowPlayingRepository.getPodCastDetails(mAuth, RemoteConstant.mPlatform, mSongId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        podcastResponseModel.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "TTrack api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun trackAnalytics(mAuth: String, mData: List<TrackAnalyticsDataList>?) {
        uiScope.launch {
            var response: Response<Any>? = null
            kotlin.runCatching {
                nowPlayingRepository.trackAnalytics(mAuth, mData!!).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        doAsync {
                            dbMusicTrackAnalyticsDao.deleteAllTrackAnalytics()
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Track analytics"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun addToFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                nowPlayingRepository.addFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }

    }

    fun removeFavourites(mAuth: String, mTrackId: String) {
        val response: Response<Any>? = null
        uiScope.launch(handler) {
            nowPlayingRepository
                .removeFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId)
            kotlin.runCatching {
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun updateFavourite(liked: String, mTrackId: String) {
        doAsync {
            createDbPlayListDao?.setFavourite(liked, mTrackId)
        }
    }

    fun addToDownload(mTrackDetails: TrackListCommon) {
        val list: MutableList<TrackListCommonDb> = ArrayList()
        val trackListCommonDbModel = TrackListCommonDb(
            mTrackDetails.id!!,
            mTrackDetails.numberId!!,
            mTrackDetails.trackName!!,
            mTrackDetails.media?.coverFile!!,
            mTrackDetails.media?.trackFile!!,
            mTrackDetails.artist?.artistName!!,
            mTrackDetails.genre?.genreName!!,
            "false", "false"
        )


        list.add(trackListCommonDbModel)
        doAsync {
            trackDownloadDao?.insertTrackDownload(list)
        }
    }

    fun addToDownloadNowPlaying(mTrackDetails: TrackListCommonDb) {
        val list: MutableList<TrackListCommonDb> = ArrayList()
        list.add(mTrackDetails)
        doAsync {
            trackDownloadDao?.insertTrackDownload(list)
        }
    }

    fun updateDownloadStatus(mTrackId: String) {
        doAsync {
            trackDownloadDao?.updateDownloadStatus(mTrackId, "true")
        }
    }

    fun checkDeletedTrack(mAuth: String, mTrackIdList: TrackIdList?) {
        var mCheckDeletedTrack: Response<DeleteOfflineTrackResponseModel>? = null

        uiScope.launch(handler) {
            kotlin.runCatching {
                nowPlayingRepository.checkDeletedTrack(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mTrackIdList
                ).collect {
                    mCheckDeletedTrack = it
                }
            }.onSuccess {
                when (mCheckDeletedTrack?.code()) {
                    200 -> if (mCheckDeletedTrack?.body() != null &&
                        mCheckDeletedTrack?.body()?.payload != null &&
                        mCheckDeletedTrack?.body()?.payload?.track != null &&
                        mCheckDeletedTrack?.body()?.payload?.track?.size!! > 0
                    ) {
                        mTrackOfflineDelete?.postValue(mCheckDeletedTrack?.body()?.payload?.track)
                    }
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun clearNowPlayingDb() {

        doAsync {
            createDbPlayListDao?.deleteAllCreateDbPlayList()
        }
    }

    fun clearOfflineTrackDb(mUrl: String) {


        doAsync {
            trackDownloadDao?.deleteOfflineTrack(mUrl)
        }
    }


    inner class DbPlayListAsyncTask internal constructor(private val mAsyncTaskDao: CreateDbPlayList) :
        AsyncTask<List<PlayListDataClass>, Void, Void>() {

        override fun doInBackground(vararg params: List<PlayListDataClass>): Void? {
            mAsyncTaskDao.deleteAllCreateDbPlayList()
            mAsyncTaskDao.insertCreateDbPlayList(params[0])
            isCreatedPlayList?.postValue(true)
            return null
        }
    }

    private val serviceApiChat =
        WebServiceClient.clientChat.create(BackEndApi::class.java)// get instance

    fun updateFailedFiles(nJwt: String, mFilesList: FailedFiles) {
        var mApiCall: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                mApiCall = serviceApiChat
                    .updateFailedFiles(
                        nJwt, mFilesList, SharedPrefsUtils.getStringPreference(
                            application, RemoteConstant.mAppId, ""
                        )!!
                    )
            }.onSuccess {
                when (mApiCall?.code()) {
                    200 -> {
                        doAsync {
                            mDbChatFileList?.deleteAllMessage()!!
                        }
                    }
                    502, 522, 523, 500 -> {
                    }
                    else -> {
                    }
                }
            }.onFailure {
            }
        }
    }

    fun postContacts(mPostList: HomeActivity.ContactModelList) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    "/api/v2/contactlist/upload", RemoteConstant.postMethod
        )
        var mAuth = RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
            application,
            RemoteConstant.mAppId,
            ""
        )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]


        val serviceApi =
            WebServiceClient.client.create(BackEndApi::class.java)// get instance

        var mApiCall: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                mApiCall = serviceApi.upLoadContacts(mAuth, mPostList)
            }.onSuccess {
                when (mApiCall?.code()) {
                    200 -> {
                        if (mApiCall?.isSuccessful!!) {
                            var mData = mApiCall?.body()
                            mPostList.contacts.map {
                                doAsync {
                                    contactListDao?.updateAdded("true", it.phone)
                                }
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                    }
                    else -> {
                    }
                }
            }.onFailure {
            }
        }
    }

    fun insetContact(mPostList: MutableList<ContactModel>) {
        contactListDao?.insertContacts(mPostList)
    }
}

