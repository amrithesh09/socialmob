package com.example.sample.socialmob.viewmodel.music_menu

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.dao.DbPlayListImages
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.repo.music.PlayListRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class PlayListViewModel @Inject constructor(
    private val playListRepository: PlayListRepository,
    private val application: Context
) : ViewModel() {

    val playListModel: MutableLiveData<ResultResponse<MutableList<PlaylistCommon>>> =
        MutableLiveData()
    var iconData: MutableLiveData<ResultResponse<List<IconDatum>>>? =
        MutableLiveData()
    var mPlayListImages: LiveData<List<IconDatumIcon>>? = null

    var dbPlayListImagesDao: DbPlayListImages? = null
    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)


    init {
        val habitRoomDatabase: SocialMobDatabase = SocialMobDatabase.getDatabase(application)
        dbPlayListImagesDao = habitRoomDatabase.playListImages()
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun getIconData(mName: String): LiveData<List<IconDatumIcon>>? {
        mPlayListImages = dbPlayListImagesDao?.getAllIconData(mName)
        return mPlayListImages
    }


    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                playListRepository.createPlayList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    createPlayListBody
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refresh()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun refresh() {
        // TODO : Refresh List
        val mAuth: String =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application,
                RemoteConstant.pathPlayList
            )
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                playListRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        val mPlayList: MutableList<PlaylistCommon>? =
                            getPlayListApi?.body()?.payload?.playlists?.toMutableList()
                        playListModel.postValue(ResultResponse.success(mPlayList!!))
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "Play List api"
                    )
                }
            }.onFailure {
                playListModel.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }


    fun getPlayList(mAuth: String) {
        playListModel.postValue(ResultResponse.loading(ArrayList(), ""))
        var getPlayListApi: Response<PlayListResponseModel>? = null

        uiScope.launch(handler) {
            kotlin.runCatching {
                playListRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        val mPlayList: MutableList<PlaylistCommon>? =
                            getPlayListApi?.body()?.payload?.playlists?.toMutableList()
                        playListModel.postValue(ResultResponse.success(mPlayList!!))
                    }
                    502, 522, 523, 500 -> {
                        playListModel.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "Play List api"
                    )
                }
            }.onFailure {
                playListModel.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    fun deletePlayList(mAuth: String, mId: String) {
        var response: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                playListRepository.deletePlayList(mAuth, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        val mAuthNew =
                            RemoteConstant.getAuthWithoutOffsetAndCount(
                                application,
                                RemoteConstant.pathPlayList
                            )
                        var getPlayListApi: Response<PlayListResponseModel>? = null
                        kotlin.runCatching {
                            playListRepository.getPlayList(mAuthNew).collect {
                                getPlayListApi = it
                            }
                        }.onSuccess {
                            when (getPlayListApi!!.code()) {
                                200 -> {
                                    val mPlayList: MutableList<PlaylistCommon>? =
                                        getPlayListApi?.body()?.payload?.playlists?.toMutableList()
                                    playListModel.postValue(
                                        ResultResponse.success(mPlayList!!)
                                    )
                                }
                                else -> RemoteConstant.apiErrorDetails(
                                    application,
                                    getPlayListApi?.code() ?: 500,
                                    "Play List api"
                                )
                            }
                        }.onFailure {
                            playListModel.postValue(
                                ResultResponse.error("", ArrayList())
                            )
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Delete Play List Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun callApiPlayListImages() {
        val mAuth: String =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application, RemoteConstant.pathPlayListImage
            )
        uiScope.launch(handler) {
            iconData?.postValue(ResultResponse.loading(ArrayList(), ""))
            var response: Response<PlayListImages>? = null
            kotlin.runCatching {
                playListRepository.callApiPlayListImages(mAuth).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        iconData?.postValue(ResultResponse.success(response?.body()?.payload?.iconData!!))
                        doAsync {
                            dbPlayListImagesDao?.deleteAllIconData()
                            for (i in 0 until response?.body()?.payload?.iconData?.size!!)
                                dbPlayListImagesDao?.insertIconData(response?.body()?.payload?.iconData!![i].icons?.toMutableList()!!)
                        }
                    }
                    502, 522, 523, 500 -> {
                        iconData?.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> {
                        iconData?.postValue(ResultResponse.error("", ArrayList()))
                    }
                }
            }.onFailure {
                iconData?.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }
}