package com.example.sample.socialmob.viewmodel.music_menu

import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.repo.music.RecentlyPlayedRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RecentlyPlayedViewModel @Inject constructor(
    private val recentlyPlayedRepository: RecentlyPlayedRepository,
    private val application: Context,
    private var cacheMapper: CacheMapperTrack
) : ViewModel() {

    var recentPlayedTracks: MutableLiveData<ResultResponse<List<TrackListCommon>>>? =
        MutableLiveData()

    private var createDbPlayListDao: CreateDbPlayList? = null

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    var isCreatedPlayList: MutableLiveData<Boolean>? = MutableLiveData()
    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null

    private var trackDownloadDao: TrackDownload? = null
    var downloadFalseTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null
    var downloadTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        createDbPlayListDao = habitRoomDatabase.createplayListDao()


        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!

        trackDownloadDao = habitRoomDatabase.trackDownloadDao()
        downloadTrackListModel = trackDownloadDao?.getAllTrackDownload()!!
        downloadFalseTrackListModel = trackDownloadDao?.getTrackDownloadedFalse("false")!!

    }


    fun getFavouriteTracksApi(mAuth: String, mOffset: String, mCount: String) {
        val favorites: List<TrackListCommon>? = null

        if (mOffset == "0") {
            recentPlayedTracks?.postValue(ResultResponse.loading(favorites, mOffset))
        } else {
            recentPlayedTracks?.postValue(
                ResultResponse.loadingPaginatedList(favorites, mOffset)
            )
        }
        var response: Response<FavouriteTrackModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                recentlyPlayedRepository.getFavouriteTracks(
                    mAuth,
                    RemoteConstant.mPlatform, /*mProfileId,*/
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.favorites != null && response?.body()?.payload?.favorites?.size!! > 0) {
                            if (mOffset == "0") {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.favorites!!
                                    )
                                )
                            } else {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.favorites!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.noData(
                                        favorites
                                    )
                                )
                            } else {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        favorites
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        recentPlayedTracks?.postValue(ResultResponse.error("", favorites))
                    }
                    else -> {
                        recentPlayedTracks?.postValue(ResultResponse.error("", favorites))
                    }

                }

            }.onFailure {
                recentPlayedTracks?.postValue(ResultResponse.error("", favorites))
            }
        }

    }

    fun getRecentPlayTracks(mAuth: String, mOffset: String) {
        val favorites: List<TrackListCommon>? = null

        recentPlayedTracks?.postValue(ResultResponse.loading(favorites, mOffset))
        var response: Response<RecentlyPlayedTrackModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                recentlyPlayedRepository.getRecentlyPlayedTrack(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOffset,
                    RemoteConstant.mCount50
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.tracks?.size!! > 0) {
                            recentPlayedTracks?.postValue(
                                ResultResponse.success(
                                    response?.body()?.payload?.tracks!!
                                )
                            )
                        } else {
                            recentPlayedTracks?.postValue(
                                ResultResponse.noData(
                                    favorites
                                )
                            )
                        }
                    }
                    502, 522, 523, 500 -> {
                        recentPlayedTracks?.postValue(
                            ResultResponse.error(
                                "",
                                favorites
                            )
                        )
                    }
                    else -> {
                        recentPlayedTracks?.postValue(
                            ResultResponse.error(
                                "",
                                favorites
                            )
                        )
                    }
                }
            }.onFailure {
                recentPlayedTracks?.postValue(ResultResponse.error("", favorites))
            }
        }
    }

    fun createPlayList(dbPlayList: MutableList<TrackListCommon>?, playListName: String) {
        isCreatedPlayList?.postValue(false)
        if (dbPlayList!!.size > 0) {
            DbPlayListAsyncTask(this.createDbPlayListDao!!).execute(
                cacheMapper.mapFromEntityList(
                    dbPlayList,playListName
                )
            )
        }
    }

    fun createPlayListApi(mAuth: String, createPlayListBody: CreatePlayListBody) {
        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                recentlyPlayedRepository.createPlayList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    createPlayListBody
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        val mAuthPlayList =
                            RemoteConstant.getAuthWithoutOffsetAndCount(
                                application,
                                RemoteConstant.pathPlayList
                            )
                        getPlayList(mAuthPlayList)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List api"
                    )
                }
            }.onFailure {
            }
        }
    }

    val playListModel: MutableLiveData<List<PlaylistCommon>> = MutableLiveData()


    fun getPlayList(mAuth: String) {
        uiScope.launch(handler) {
            var getPlayListApi: Response<PlayListResponseModel>? = null
            kotlin.runCatching {
                recentlyPlayedRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code()!!, "PlayList Api"
                    )
                }
            }.onFailure {
                val mAuthNew =
                    RemoteConstant.getAuthWithoutOffsetAndCount(
                        application, RemoteConstant.pathPlayList
                    )
                getPlayList(mAuthNew)
            }
        }
    }


    fun addToFavourites(mAuth: String, mTrackId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                recentlyPlayedRepository
                    .addFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId)
            }.onSuccess {
            }.onFailure {
                RemoteConstant.apiErrorDetails(
                    application, 500, "Add track to favorites"
                )
            }
        }

    }

    fun removeFavourites(mAuth: String, mTrackId: String) {

        uiScope.launch(handler) {
            kotlin.runCatching {
                recentlyPlayedRepository
                    .removeFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId)
            }.onSuccess {
            }.onFailure {
                RemoteConstant.apiErrorDetails(
                    application, 500, "Add track to favorites"
                )
            }
        }
    }

    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        var response: Response<AddPlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                recentlyPlayedRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        refreshPlayList(response!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "TrendingTrack api"
                    )
                }
            }.onFailure {
            }
        }
    }


    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        refreshPlayList()
        AppUtils.showCommonToast(application, value.body()?.payload?.message)
    }

    private fun refreshPlayList() {
        // TODO : Refresh Play List
        val mAuthPlayList =
            RemoteConstant.getAuthWithoutOffsetAndCount(application, RemoteConstant.pathPlayList)
        getPlayList(mAuthPlayList)
    }

    fun addToDownload(mTrackDetails: TrackListCommon) {
        val list: MutableList<TrackListCommonDb> = ArrayList()
        val trackListCommonDbModel = TrackListCommonDb(
            mTrackDetails.id!!,
            mTrackDetails.numberId!!,
            mTrackDetails.trackName!!,
            mTrackDetails.media?.coverFile!!,
            mTrackDetails.media?.trackFile!!,
            mTrackDetails.artist?.artistName!!,
            mTrackDetails.genre?.genreName!!,
            "false", "false"
        )


        list.add(trackListCommonDbModel)
        doAsync {
            trackDownloadDao?.insertTrackDownload(list)
        }
    }


    inner class DbPlayListAsyncTask internal constructor(private val mAsyncTaskDao: CreateDbPlayList) :
        AsyncTask<List<PlayListDataClass>, Void, Void>() {

        override fun doInBackground(vararg params: List<PlayListDataClass>): Void? {
            mAsyncTaskDao.deleteAllCreateDbPlayList()
            mAsyncTaskDao.insertCreateDbPlayList(params[0])
            isCreatedPlayList?.postValue(true)
            return null
        }
    }


}
