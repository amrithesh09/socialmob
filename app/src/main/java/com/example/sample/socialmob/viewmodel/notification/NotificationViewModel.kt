package com.example.sample.socialmob.viewmodel.notification

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.profile.GetAllNotificationResponseModel
import com.example.sample.socialmob.model.profile.GetAllNotificationResponseModelPayloadNotification
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.repo.notification.NotificationRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository, private var application: Context
) : ViewModel() {

    var followResponseModel: MutableLiveData<FollowResponseModel> = MutableLiveData()
    val notificationUsersPaginatedList: MutableLiveData<ResultResponse<MutableList<GetAllNotificationResponseModelPayloadNotification>>>? =
        MutableLiveData()
    var isHasNotification: MutableLiveData<Boolean>? = MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        uiScope.cancel()
    }

    fun getAllNotification(mAuth: String, mOffset: String, mCount: String) {
        if (mOffset == "0") {
            notificationUsersPaginatedList?.postValue(
                ResultResponse.loading(
                    ArrayList(),
                    mOffset
                )
            )
        } else {
            notificationUsersPaginatedList?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }
        uiScope.launch(handler) {
            var mNotificationResult: Response<GetAllNotificationResponseModel>? = null
            kotlin.runCatching {
                notificationRepository.getAllNotification(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOffset,
                    mCount
                ).collect {
                    mNotificationResult = it
                }
            }.onSuccess {
                // do something with success response
                when (mNotificationResult?.code()) {
                    200 -> {
                        if (mNotificationResult?.body()?.payload != null &&
                            mNotificationResult?.body()?.payload?.notifications != null &&
                            mNotificationResult?.body()?.payload?.notifications?.size!! > 0
                        ) {
                            if (mNotificationResult?.body() != null &&
                                mNotificationResult?.body()?.payload != null &&
                                mNotificationResult?.body()?.payload?.unreadNotificationsCount != null
                            ) {
                                if (mNotificationResult?.body()?.payload?.unreadNotificationsCount ?: 0 > 0) {
                                    isHasNotification?.postValue(true)
                                } else {
                                    isHasNotification?.postValue(false)
                                }
                            }
                            if (mOffset == "0") {
                                notificationUsersPaginatedList?.postValue(
                                    ResultResponse.success(
                                        mNotificationResult?.body()?.payload?.notifications!!
                                    )
                                )
                            } else {
                                notificationUsersPaginatedList?.postValue(
                                    ResultResponse.paginatedList(mNotificationResult?.body()?.payload?.notifications)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                notificationUsersPaginatedList?.postValue(
                                    ResultResponse.noData(ArrayList())
                                )
                            } else {
                                notificationUsersPaginatedList?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        notificationUsersPaginatedList?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mNotificationResult?.code() ?: 500, "Get All Push Notification api"
                    )
                }


            }.onFailure {
                // do something on failure response
                notificationUsersPaginatedList?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }

        }
    }

    fun unFollowProfile(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mUnFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                notificationRepository.unFollowProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mUnFollowProfile = it
                    }
            }.onSuccess {
                when (mUnFollowProfile?.code()) {
                    200 -> followResponseModel.postValue(mUnFollowProfile?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mUnFollowProfile?.code() ?: 500,
                        "Un Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }

        }
    }

    fun followProfile(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mFollowApiCall: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                notificationRepository.followProfile(mAuth, RemoteConstant.mPlatform, mId).collect {
                    mFollowApiCall = it
                }
            }.onSuccess {
                when (mFollowApiCall?.code()) {
                    200 -> followResponseModel.postValue(mFollowApiCall?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mFollowApiCall?.code() ?: 500,
                        "Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun readSingleNotification(mAuth: String, mNotificationId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                notificationRepository
                    .readSingleNotification(mAuth, RemoteConstant.mPlatform, mNotificationId)
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun acceptRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                notificationRepository
                    .acceptRequest(mAuth, RemoteConstant.mPlatform, mId)
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun rejectRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                notificationRepository
                    .rejectRequest(mAuth, RemoteConstant.mPlatform, mId)
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun cancelRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                notificationRepository
                    .cancelFollowUser(mAuth, RemoteConstant.mPlatform, mId)
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun markAllAsRead(mAuth: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                notificationRepository
                    .markAllAsRead(mAuth, RemoteConstant.mPlatform)
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }
}