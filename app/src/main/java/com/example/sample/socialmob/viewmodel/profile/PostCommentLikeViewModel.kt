package com.example.sample.socialmob.viewmodel.profile

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.article.CommentPostData
import com.example.sample.socialmob.model.feed.CommentDeleteResponseModel
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.CommentResponseModel
import com.example.sample.socialmob.model.profile.CommentResponseModelPayloadComment
import com.example.sample.socialmob.model.profile.LikeListResponseModel
import com.example.sample.socialmob.model.profile.MentionProfileResponseModel
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.music.PostLIkeCommentRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class PostCommentLikeViewModel @Inject constructor(
    private val postLIkeCommentRepository: PostLIkeCommentRepository,
    private var application: Context
) : ViewModel() {


    var commentPaginatedResponseModelCommentCount: MutableLiveData<String> = MutableLiveData()
    var commentPaginatedResponseModel: MutableLiveData<ResultResponse<MutableList<CommentResponseModelPayloadComment>>>? =
        MutableLiveData()
    var trackCommentPaginatedResponseModel: MutableLiveData<ResultResponse<TrackCommentModelPayload>>? =
        MutableLiveData()
    var addedTrackCommentResponse: MutableLiveData<TrackCommentModelPayloadComment>? =
        MutableLiveData()
    var responsePaginatedListLikeModel: MutableLiveData<ResultResponse<MutableList<SearchProfileResponseModelPayloadProfile>>>? =
        MutableLiveData()


    var commentDeleteResponseModel: MutableLiveData<CommentDeleteResponseModel> = MutableLiveData()
    var mentionProfileResponseModel: MutableLiveData<MutableList<Profile>> =
        MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }


    var commentsCount = "0"
    var trackName = ""
    var likeCount = "0"
    var playCount = "0"
    var favorite = "false"
    var mTrackName = ""
    var mTrackImageUrl = ""

    private var createDbPlayListDao: CreateDbPlayList? = null

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        createDbPlayListDao = habitRoomDatabase.createplayListDao()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


    fun getTrackComment(
        mAuth: String, mId: String, mOffset: String, mCount: String
    ) {
        uiScope.launch(handler) {
            if (mOffset == "0") {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.loading(
                        TrackCommentModelPayload(),
                        mOffset
                    )
                )
            } else {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.loadingPaginatedList(TrackCommentModelPayload(), mOffset)
                )
            }
            var response: Response<TrackCommentModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.getTrackComments(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {

                        if (response?.body()?.payload != null) {
                            if (response?.body()?.payload?.commentsCount != null) {
                                commentsCount = response?.body()?.payload?.commentsCount!!
                            }
                            if (response?.body()?.payload?.track?.likeCount != null) {
                                likeCount = response?.body()?.payload?.track?.likeCount!!
                            }
                            if (response?.body()?.payload?.track?.playCount != null) {
                                playCount = response?.body()?.payload?.track?.playCount!!
                            }
                            if (response?.body()?.payload?.track?.favorite != null) {
                                favorite = response?.body()?.payload?.track?.favorite!!
                            }
                            if (response?.body()?.payload?.track != null) {
                                trackName = response?.body()?.payload?.track?.trackName!!
                            }
                        }


                        if (response?.body()?.payload?.comments != null
                            && response?.body()?.payload?.comments?.size!! > 0) {
                            if (mOffset == "0") {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload!!
                                    )
                                )
                            } else {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.noData(
                                        TrackCommentModelPayload()
                                    )
                                )
                            } else {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        TrackCommentModelPayload()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        trackCommentPaginatedResponseModel?.postValue(
                            ResultResponse.error(
                                "",
                                TrackCommentModelPayload()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Feed comments Api"
                    )
                }

            }.onFailure {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.error("", TrackCommentModelPayload())
                )
            }
        }
    }

    fun getPodcastComment(
        mAuth: String, mId: String, mOffset: String, mCount: String
    ) {
        uiScope.launch(handler) {
            if (mOffset == "0") {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.loading(
                        TrackCommentModelPayload(),
                        mOffset
                    )
                )
            } else {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.loadingPaginatedList(TrackCommentModelPayload(), mOffset)
                )
            }
            var response: Response<TrackCommentModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.getPodCastComments(
                    mAuth,
                    RemoteConstant.mPlatform,
                    "podcast",
                    mId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload != null) {
                            if (response?.body()?.payload?.commentsCount != null) {
                                commentsCount = response?.body()?.payload?.commentsCount!!
                            }
                            if (response?.body()?.payload?.track?.likeCount != null) {
                                likeCount = response?.body()?.payload?.track?.likeCount!!
                            }
                            if (response?.body()?.payload?.track?.playCount != null) {
                                playCount = response?.body()?.payload?.track?.playCount!!
                            }
                            if (response?.body()?.payload?.track?.favorite != null) {
                                favorite = response?.body()?.payload?.track?.favorite!!
                            }

                            //TODO: For podcast
                            if (response?.body()?.payload?.podcast != null) {
                                if (response?.body()?.payload?.podcast?.podcast_name != null) {
                                    mTrackName =
                                        response?.body()?.payload?.podcast?.podcast_name!!
                                }
                                if (response?.body()?.payload?.podcast?.cover_image != null) {
                                    mTrackImageUrl =
                                        response?.body()?.payload?.podcast?.cover_image!!
                                }
                            }

                        }
                        if (response?.body()?.payload?.comments != null
                            && response?.body()?.payload?.comments?.size!! > 0) {
                            if (mOffset == "0") {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload!!
                                    )
                                )
                            } else {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.noData(
                                        TrackCommentModelPayload()
                                    )
                                )
                            } else {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        TrackCommentModelPayload()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        trackCommentPaginatedResponseModel?.postValue(
                            ResultResponse.error(
                                "",
                                TrackCommentModelPayload()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Feed comments Api"
                    )
                }
            }.onFailure {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.error("", TrackCommentModelPayload())
                )
            }
        }
    }


    fun getPodcastEpisodeComment(
        mAuth: String, mId: String, mOffset: String, mCount: String
    ) {
        var mGetPodCastCommentEpisodeList: Response<TrackCommentModel>? = null
        uiScope.launch(handler) {
            if (mOffset == "0") {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.loading(
                        TrackCommentModelPayload(),
                        mOffset
                    )
                )
            } else {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.loadingPaginatedList(TrackCommentModelPayload(), mOffset)
                )
            }
            kotlin.runCatching {
                postLIkeCommentRepository.getPodCastEpisodeComments(
                    mAuth,
                    RemoteConstant.mPlatform,
                    "episode",
                    mId,
                    mOffset,
                    mCount
                ).collect {
                    mGetPodCastCommentEpisodeList = it
                }
            }.onSuccess {
                when (mGetPodCastCommentEpisodeList?.code()) {
                    200 -> {

                        if (mGetPodCastCommentEpisodeList?.body()?.payload != null) {
                            if (mGetPodCastCommentEpisodeList?.body()?.payload?.commentsCount != null) {
                                commentsCount =
                                    mGetPodCastCommentEpisodeList?.body()?.payload?.commentsCount!!
                            }
                            if (mGetPodCastCommentEpisodeList?.body()?.payload?.track?.likeCount != null) {
                                likeCount =
                                    mGetPodCastCommentEpisodeList?.body()?.payload?.track?.likeCount!!
                            }
                            if (mGetPodCastCommentEpisodeList?.body()?.payload?.track?.playCount != null) {
                                playCount =
                                    mGetPodCastCommentEpisodeList?.body()?.payload?.track?.playCount!!
                            }
                            if (mGetPodCastCommentEpisodeList?.body()?.payload?.track?.favorite != null) {
                                favorite =
                                    mGetPodCastCommentEpisodeList?.body()?.payload?.track?.favorite!!
                            }
                        }
                        if (mGetPodCastCommentEpisodeList?.body()?.payload?.comments != null && mGetPodCastCommentEpisodeList?.body()?.payload?.comments?.size!! > 0) {
                            if (mOffset == "0") {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.success(
                                        mGetPodCastCommentEpisodeList?.body()?.payload!!
                                    )
                                )
                            } else {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        mGetPodCastCommentEpisodeList?.body()?.payload
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.noData(
                                        TrackCommentModelPayload()
                                    )
                                )
                            } else {
                                trackCommentPaginatedResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        TrackCommentModelPayload()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        trackCommentPaginatedResponseModel?.postValue(
                            ResultResponse.error(
                                "",
                                TrackCommentModelPayload()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mGetPodCastCommentEpisodeList?.code() ?: 500,
                        "Feed comments Api"
                    )
                }

            }.onFailure {
                trackCommentPaginatedResponseModel?.postValue(
                    ResultResponse.error("", TrackCommentModelPayload())
                )
            }
        }
    }

    fun getFeedComment(
        mAuth: String, mId: String, mOffset: String, mCount: String
    ) {
        var response: Response<CommentResponseModel>? = null
        uiScope.launch(handler) {
            if (mOffset == "0") {
                commentPaginatedResponseModel?.postValue(
                    ResultResponse.loading(
                        ArrayList(),
                        mOffset
                    )
                )
            } else {
                commentPaginatedResponseModel?.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            kotlin.runCatching {
                postLIkeCommentRepository.getFeedComments(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.comments != null && response?.body()?.payload?.comments?.size!! > 0) {
                            if (mOffset == "0") {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.comments!!
                                    )
                                )
                                commentPaginatedResponseModelCommentCount.postValue(response?.body()?.payload?.commentsCount)
                            } else {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.comments!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                commentPaginatedResponseModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        commentPaginatedResponseModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Feed comments Api"
                    )
                }
            }.onFailure {
                commentPaginatedResponseModel?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun getFeedLikes(
        mAuth: String, mId: String, mOffset: String, mCount: String
    ) {
        var response: Response<LikeListResponseModel>? = null
        uiScope.launch(handler) {
            if (mOffset == "0") {
                responsePaginatedListLikeModel?.postValue(
                    ResultResponse.loading(
                        ArrayList(),
                        mOffset
                    )
                )
            } else {
                responsePaginatedListLikeModel?.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }

            kotlin.runCatching {
                postLIkeCommentRepository.getLikeListPost(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload?.likes != null && response?.body()?.payload?.likes?.size!! > 0) {
                            if (mOffset == "0") {
                                responsePaginatedListLikeModel?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.likes!!
                                    )
                                )
                            } else {
                                responsePaginatedListLikeModel?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.likes!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                responsePaginatedListLikeModel?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                responsePaginatedListLikeModel?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        responsePaginatedListLikeModel?.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 502, "Feed Like List Api"
                    )

                }
            }.onFailure {
                responsePaginatedListLikeModel?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun postCommentTrack(
        mAuth: String, id: String, commentPostData: CommentPostDataTrack
    ) {
        var mPostCommentTrack: Response<TrackAddCommentResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                postLIkeCommentRepository.postCommentTrack(
                    mAuth,
                    RemoteConstant.mPlatform,
                    id,
                    commentPostData
                ).collect {
                    mPostCommentTrack = it
                }
            }.onSuccess {
                when (mPostCommentTrack?.code()) {
                    200 -> {
                        if (mPostCommentTrack?.body() != null &&
                            mPostCommentTrack?.body()?.payload != null &&
                            mPostCommentTrack?.body()?.payload?.comment != null
                        ) {
                            addedTrackCommentResponse?.postValue(mPostCommentTrack?.body()?.payload?.comment)
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mPostCommentTrack?.code() ?: 500,
                        "Track comments api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }


    fun postCommentPodCast(
        mAuth: String, id: String, commentPostData: CommentPostDataTrack, target: String
    ) {
        var mPostCommentPodCast: Response<TrackAddCommentResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                postLIkeCommentRepository.postCommentPodCast(
                    mAuth,
                    RemoteConstant.mPlatform,
                    target,
                    id,
                    commentPostData
                ).collect {
                    mPostCommentPodCast = it
                }
            }.onSuccess {
                when (mPostCommentPodCast!!.code()) {
                    200 -> {
                        if (mPostCommentPodCast?.body() != null &&
                            mPostCommentPodCast?.body()?.payload != null &&
                            mPostCommentPodCast?.body()?.payload?.comment != null
                        ) {
                            addedTrackCommentResponse?.postValue(mPostCommentPodCast?.body()?.payload?.comment)
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mPostCommentPodCast?.code() ?: 500, "Podcast main comments api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again"
                )
            }
        }
    }

    fun postComment(
        mAuth: String, mPostId: String, commentPostData: CommentPostData
    ) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.postCommentPost(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mPostId,
                    commentPostData
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        val mAuthFeedComment =
                            RemoteConstant.getAuthCommentList(
                                application,
                                mPostId,
                                "0",
                                RemoteConstant.mCount.toInt()
                            )
                        getFeedComment(
                            mAuthFeedComment,
                            mPostId,
                            "0",
                            RemoteConstant.mCount
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 502, "Post Feed comments api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun followProfile(mAuth: String, mId: String) {
        uiScope.launch {
            var mFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.followProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mFollowProfile = it
                    }
            }.onSuccess {
                when (mFollowProfile?.code()) {
                    200 -> {
//                        updateDbSearchFollowProfile(mFollowProfile, mId)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mFollowProfile?.code() ?: 500,
                        "Follow Profile api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun unFollowProfile(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mUnFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.unFollowProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mUnFollowProfile = it
                    }
            }.onSuccess {
                when (mUnFollowProfile?.code()) {
                    200 -> println()//updateDbSearchUnFollowProfile(value, mId)
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mUnFollowProfile?.code() ?: 500,
                        "Un Follow Profile api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }


    fun deleteComment(mAuth: String, mPostId: String, mId: String) {
        uiScope.launch(handler) {
            var response: Response<CommentDeleteResponseModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.deleteCommentPost(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mPostId,
                    mId
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> commentDeleteResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete post comment api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun deleteTrackComment(mAuth: String, mPostId: String, mId: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                postLIkeCommentRepository
                    .deleteCommentTrack(mAuth, RemoteConstant.mPlatform, mPostId, mId)
            }.onSuccess {

            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }
    }

    fun deletePodCastComment(mAuth: String, mPostId: String, mId: String, mTarget: String) {
        uiScope.launch(handler) {
            kotlin.runCatching {
                postLIkeCommentRepository
                    .deletePodCastComment(mAuth, RemoteConstant.mPlatform, mTarget, mPostId, mId)
            }.onSuccess {

            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again"
                )
            }
        }

    }

    fun cancelRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mCancelRequest: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.cancelFollowUser(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mCancelRequest = it
                    }
            }.onSuccess {
                when (mCancelRequest?.code()) {
                    200 -> println()//followResponseModel.postValue(response.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mCancelRequest?.code() ?: 500,
                        "cancelRequest api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }

    private var mMentionApiCall: Job? = null
    fun getMentions(mContext: Context, mMnention: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mMentionProfile +
                    RemoteConstant.pathQuestionmark + "name=" + mMnention,
            RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        if (mMentionApiCall != null) {
            mMentionApiCall?.cancel()
        }


        mMentionApiCall = uiScope.launch(handler) {
            var response: Response<MentionProfileResponseModel>? = null
            kotlin.runCatching {
                postLIkeCommentRepository.mentionProfile(mAuth, RemoteConstant.mPlatform, mMnention)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> mentionProfileResponseModel.postValue(response?.body()?.payload?.profiles)
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        response?.code()!!,
                        "Get Mention Profile api"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun addToFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                postLIkeCommentRepository.addFavouriteTrack(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mTrackId
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }

    }

    fun removeFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            postLIkeCommentRepository.removeFavouriteTrack(
                mAuth,
                RemoteConstant.mPlatform,
                mTrackId
            ).collect {
                response = it
            }
            kotlin.runCatching {
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun updateFavourite(liked: String, mTrackId: String) {
        doAsync {
            createDbPlayListDao?.setFavourite(liked, mTrackId)
        }
    }

}