package com.example.sample.socialmob.viewmodel.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperPodCast
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModel
import com.example.sample.socialmob.model.profile.PersonalFeedResponseModelPayloadFeed
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.profile.ProfileDetailsRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.ui.home_module.music.dash_board.CommonFragmentActivity
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ProfileDetailsViewModel @Inject constructor(
    private val profileDetailsRepository: ProfileDetailsRepository,
    private val application: Context,
    private var cacheMapper: CacheMapperTrack,
    private var cacheMapperPodCast: CacheMapperPodCast
) : ViewModel() {
    val mPhotoVideoList: MutableLiveData<ResultResponse<MutableList<PersonalFeedResponseModelPayloadFeed>>> =
        MutableLiveData()

    val playListModel: MutableLiveData<List<PlaylistCommon>>? = MutableLiveData()
    var createDbPlayListDao: CreateDbPlayList? = null
    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null
    val followResponseModel: MutableLiveData<FollowResponseModel>
    val profile: LiveData<Profile>
    var trackDetailsModel: MutableLiveData<TrackDetailsModel>? = MutableLiveData()
    var podCastResponseModel: MutableLiveData<PodcastResponseModel>? = MutableLiveData()

    private var loginDao: LoginDao? = null

    init {
        val habitRoomDatabase: SocialMobDatabase? = SocialMobDatabase.getDatabase(application)
        followResponseModel = MutableLiveData()
        createDbPlayListDao = habitRoomDatabase?.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!

        loginDao = habitRoomDatabase?.loginDao()
        profile = loginDao?.getAllProfileData()!!
    }

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }


    fun getPhotoGridMediagrid(
        mAuth: String,
        mProfileId: String,
        mOffset: String,
        mCount: String
    ) {
        var response: Response<PersonalFeedResponseModel>? = null
        uiScope.launch(handler) {
            if (mOffset == "0") {
                mPhotoVideoList.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                mPhotoVideoList.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            kotlin.runCatching {
                profileDetailsRepository.getPhotoGridMediaGrid(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload?.feeds != null && response?.body()?.payload?.feeds?.size!! > 0) {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get PhotoGrid api"
                    )
                }
            }.onFailure {
                mPhotoVideoList.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }


    fun getMediaGrid(
        mAuth: String,
        mProfileId: String,
        mOffset: String,
        mCount: String
    ) {
        var response: Response<PersonalFeedResponseModel>? = null
        uiScope.launch(handler) {
            if (mOffset == "0") {
                mPhotoVideoList.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                mPhotoVideoList.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            kotlin.runCatching {
                profileDetailsRepository.getMediaGrid(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mProfileId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload?.feeds != null && response?.body()?.payload?.feeds?.size!! > 0) {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get PhotoGrid api"
                    )
                }
            }.onFailure {
                mPhotoVideoList.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }


    fun deletePost(mId: String, mContext: Context) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                response = profileDetailsRepository.deletePost(mAuth, RemoteConstant.mPlatform, mId)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete Post"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun likePost(mAuth: String, postId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                response = profileDetailsRepository
                    .likePost(mAuth, RemoteConstant.mPlatform, postId)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Like Feed Post api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun disLikePost(mAuth: String, postId: String) {

        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                response = profileDetailsRepository
                    .disLikePost(mAuth, RemoteConstant.mPlatform, postId)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Like Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun getMediaGridCampaign(
        mAuth: String, tagId: String, feedType: String, mOffset: String, mCount: String
    ) {
        var response: Response<PersonalFeedResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileDetailsRepository.getMediaGridCampaign(
                    mAuth,
                    RemoteConstant.mPlatform,
                    tagId,
                    feedType,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload != null &&
                            response?.body()?.payload?.feeds != null &&
                            response?.body()?.payload?.feeds?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.feeds)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get PhotoGrid api"
                    )
                }
            }.onFailure {
                mPhotoVideoList.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }


    fun getHAshTagFeed(
        mAuth: String, mTagId: String, mOffset: String, mCount: String, mContext: Activity
    ) {
        var response: Response<PersonalFeedResponseModel>? = null

        uiScope.launch(handler) {

            if (mOffset == "0") {
                mPhotoVideoList.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                mPhotoVideoList.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            kotlin.runCatching {
                profileDetailsRepository.getHashTagFeed(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mTagId,
                    "all",
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body()?.payload?.feeds != null && response?.body()?.payload?.feeds?.size!! > 0) {
                            if (mOffset == "0") {
                                if (response?.body()?.payload?.hashTag?.campaign_mode.equals("true")
                                ) {
                                    val detailIntent =
                                        Intent(mContext, CommonFragmentActivity::class.java)
                                    detailIntent.putExtra(
                                        "mId",
                                        SharedPrefsUtils.getStringPreference(
                                            mContext,
                                            RemoteConstant.mProfileId,
                                            ""
                                        )!!
                                    )
                                    detailIntent.putExtra(
                                        "mRecordId",
                                        response?.body()?.payload?.hashTag?.id
                                    )
                                    detailIntent.putExtra("mPath", "")
                                    detailIntent.putExtra("isFrom", "Campaign")
                                    detailIntent.putExtra("mTitle", "#Campaign")
                                    mContext.startActivity(detailIntent)
                                    mContext.overridePendingTransition(0, 0)
                                    mContext.finish()
                                } else {
                                    mPhotoVideoList.postValue(ResultResponse.success(response?.body()?.payload?.feeds!!))
                                }
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.feeds!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mPhotoVideoList.postValue(ResultResponse.noData(ArrayList()))
                            } else {
                                mPhotoVideoList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mPhotoVideoList.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Hash tag Feed api"
                    )
                }
            }.onFailure {
                mPhotoVideoList.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    fun unFollowProfile(mAuth: String, mId: String, mCount: String) {
        uiScope.launch(handler) {
            var mUnFollowProfileApi: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                profileDetailsRepository.unFollowProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mUnFollowProfileApi = it
                    }
            }.onSuccess {
                when (mUnFollowProfileApi?.code()) {
                    200 -> {
                        followResponseModel.postValue(mUnFollowProfileApi?.body())
                        if (mCount != "") {
                            val mNewCount: Int = Integer.parseInt(mCount).minus(1)
                            loginDao?.upDateFollowersCount(mNewCount.toString())
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mUnFollowProfileApi?.code() ?: 500,
                        "Un Follow Profile Api api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun cancelRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mCancelRequest: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                profileDetailsRepository.cancelFollowUser(mAuth, RemoteConstant.mPlatform, mId).collect {
                    mCancelRequest = it
                }
            }.onSuccess {
                when (mCancelRequest?.code()) {
                    200 -> {
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mCancelRequest?.code() ?: 500,
                        "cancelRequest api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun followProfile(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                profileDetailsRepository.followProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mFollowProfile = it
                    }
            }.onSuccess {
                when (mFollowProfile?.code()) {
                    200 -> followResponseModel.postValue(mFollowProfile?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, mFollowProfile?.code() ?: 500, "Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }

    fun getTrackDetails(mAuth: String, profileAnthemTrackId: String?) {
        var response: Response<TrackDetailsModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileDetailsRepository.getTrackDetails(
                    mAuth,
                    RemoteConstant.mPlatform,
                    profileAnthemTrackId!!
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        trackDetailsModel?.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "TrendingTrack api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun createDbPlayListModelNotification(
        dbPlayList: MutableList<TrackListCommon>?,
        playListName: String
    ) {
        if (dbPlayList?.size ?: 0 > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapper.mapFromEntityList(
                        dbPlayList ?: ArrayList(), playListName
                    )
                )
            }
        }
    }

    fun createDbPlayListModelNotificationPodcast(
        dbPlayList: MutableList<RadioPodCastEpisode>?,
        playListName: String
    ) {
        if (dbPlayList?.size ?: 0 > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapperPodCast.mapFromEntityListPodCast(dbPlayList!!, playListName)
                )
            }
        }
    }

    fun getPodCastDetails(mAuth: String, mSongId: String) {
        var response: Response<PodcastResponseModel>? = null
        uiScope.launch {
            kotlin.runCatching {
                profileDetailsRepository.getPodCastDetails(mAuth, RemoteConstant.mPlatform, mSongId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        podCastResponseModel?.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "POd api"
                    )
                }
            }.onFailure {
            }
        }
    }
}