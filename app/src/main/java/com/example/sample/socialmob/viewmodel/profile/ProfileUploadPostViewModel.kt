package com.example.sample.socialmob.viewmodel.profile

import android.content.Context
import android.provider.MediaStore
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.feed.ScrapperUrl
import com.example.sample.socialmob.model.feed.SingleFeedResponseModel
import com.example.sample.socialmob.model.feed.UploadFeedResponseModel
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.login.ProfilePicResponseModel
import com.example.sample.socialmob.model.profile.*
import com.example.sample.socialmob.model.search.HashTag
import com.example.sample.socialmob.repository.repo.profile.ProfileUploadRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.SharedPrefsUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ProfileUploadPostViewModel @Inject constructor(
    private val profileUploadRepository: ProfileUploadRepository,
    private val application: Context
) : ViewModel() {


    var hashTagsResponseModel: MutableLiveData<MutableList<HashTag>> = MutableLiveData()
    var mentionProfileResponseModel: MutableLiveData<MutableList<Profile>> =
        MutableLiveData()
    var scrapUrlResponseModel: MutableLiveData<ScrapUrlResponseModel> = MutableLiveData()
    var isUploaded: MutableLiveData<Boolean> = MutableLiveData()
    var textFeedResponseMode: MutableLiveData<UploadFeedResponseModel> = MutableLiveData()

    var subTagResponseModel: MutableLiveData<SubTagResponseModel> = MutableLiveData()
    var subTagsThrowable: MutableLiveData<Throwable> = MutableLiveData()


    var textFeedResponseMThrowable: MutableLiveData<String> = MutableLiveData()

    val hash: List<HashTag> = emptyList()
    var responseModel: MutableLiveData<SingleFeedResponseModel> = MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


    fun getPreSignedUrlProfile(mAuth: String, type: String, s: String, file: String, s1: String) {
        var response: Response<ProfilePicResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileUploadRepository.getImagePreDesignedUrl().collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> uploadProfilePic(response?.body(), file)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Upload profile Api api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun uploadProfilePic(body: ProfilePicResponseModel?, file: String) {

        val profilePicFileName = body?.payload?.filename
        val profilePicFile = body?.payload?.url

        SharedPrefsUtils.setStringPreference(
            application,
            "PROFILE_PIC_FILE_URL_NAME",
            profilePicFileName!!
        )
        SharedPrefsUtils.setStringPreference(application, "PROFILE_PIC_FILE_NAME", file)


        val mRequestBodyImage = File(file).asRequestBody("image/jpeg".toMediaTypeOrNull())
        var response: Response<ResponseBody>? = null

        uiScope.launch(handler) {
            kotlin.runCatching {
                profileUploadRepository.uploadImageProgressAmazonSuspend(profilePicFile!!, mRequestBodyImage
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> isUploaded.postValue(true)
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        response?.code() ?: 500,
                        "Upload Amazon server profile pic Api"
                    )
                }
            }.onFailure {
            }
        }

    }

    var mHashTagApi: Call<HashTagsResponseModel>? = null
    fun getHashTags(mContext: Context, mHashTag: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mHashTagData +
                    RemoteConstant.pathQuestionmark + "searchText=" + mHashTag + "&" +
                    "offset=0&count=20", RemoteConstant.mGetMethod
        )
        val mAuth = RemoteConstant.frameWorkName +
                SharedPrefsUtils.getStringPreference(
                    mContext,
                    RemoteConstant.mAppId,
                    ""
                )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        if (mHashTagApi != null) {
            mHashTagApi?.cancel()
        }
        uiScope.launch(handler) {
            mHashTagApi = profileUploadRepository
                .getHashTags(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mHashTag,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCounttwenty
                )

            mHashTagApi
                ?.enqueue(object : Callback<HashTagsResponseModel> {
                    override fun onFailure(call: Call<HashTagsResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<HashTagsResponseModel>?,
                        response: Response<HashTagsResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> hashTagsResponseModel.postValue(response.body()?.payload?.hashTags)
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Get Hash tag api"
                            )
                        }
                    }

                })
        }
    }

    private var mMentionApiCall: Job? = null

    fun getMentions(mContext: Context, mMnention: String) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mMentionProfile +
                    RemoteConstant.pathQuestionmark + "name=" + mMnention, RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        if (mMentionApiCall != null && mMentionApiCall?.isActive!!) {
            mMentionApiCall?.cancel()
        }
        mMentionApiCall = uiScope.launch(handler) {
            var response: Response<MentionProfileResponseModel>? = null
            kotlin.runCatching {
                profileUploadRepository.mentionProfile(mAuth, RemoteConstant.mPlatform, mMnention)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> mentionProfileResponseModel.postValue(response?.body()?.payload?.profiles)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Get Mention Profile api"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    private var mGetScrapDataJob: Job? = null
    fun getScrapData(mUrl: ScrapperUrl) {
        if (mGetScrapDataJob != null && !mGetScrapDataJob?.isCancelled!!) {
            mGetScrapDataJob?.cancel()
        }
        var response: Response<ScrapUrlResponseModel>? = null
        mGetScrapDataJob = uiScope.launch(handler) {
            kotlin.runCatching {
                response = profileUploadRepository.getUrlScrapped(mUrl)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> scrapUrlResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Url Scrapped api"
                    )
                }
            }.onFailure {
                println("" + it)
            }
        }
    }

    fun uploadTextPost(postData: PostFeedData) {

        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.createFeedPost, RemoteConstant.postMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]
        uiScope.launch(handler) {
            var response: Response<UploadFeedResponseModel>? = null
            kotlin.runCatching {
                profileUploadRepository.createFeedPost(mAuth, RemoteConstant.mPlatform, postData)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> textFeedResponseMode.postValue(response?.body())
                    else -> uploadFailureText(response!!)

                }
            }.onFailure {
                textFeedResponseMThrowable.postValue("error")
            }
        }
    }

    private fun uploadFailureText(response: Response<UploadFeedResponseModel>) {

        textFeedResponseMThrowable.postValue("error")
        RemoteConstant.apiErrorDetails(application, response.code(), "Upload Text api")
    }

    fun getSubHAshTag(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var response: Response<SubTagResponseModel>? = null
            kotlin.runCatching {
                profileUploadRepository.getSubHashTag(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mId,
                    RemoteConstant.mOffset,
                    RemoteConstant.mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> subTagResponseModel.postValue(response?.body())
                    else -> subTagFailureText(response!!)
                }
            }.onFailure {
                subTagsThrowable.postValue(it)
            }
        }
    }

    private fun subTagFailureText(response: Response<SubTagResponseModel>) {
        val t: Throwable? = null
        subTagsThrowable.postValue(t)
        RemoteConstant.apiErrorDetails(application, response.code(), "Sub Tags api")
    }

    fun getFeedDetails(mPostId: String) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash +
                    mPostId +
                    RemoteConstant.mPathDetails, RemoteConstant.mGetMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application,
                RemoteConstant.mAppId, ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {

            var response: Response<SingleFeedResponseModel>? = null
            kotlin.runCatching {
                profileUploadRepository.getPostFeedData(mAuth, RemoteConstant.mPlatform, mPostId
                ).collect {
                    response =it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> responseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Post Image Data Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun updatePost(postData: PostFeedData, mPostId: String) {
//
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(application, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl + RemoteConstant.mPostPath +
                    RemoteConstant.mSlash + mPostId, RemoteConstant.putMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                application, RemoteConstant.mAppId,
                ""
            )!! + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<UploadFeedResponseModel>? = null
            kotlin.runCatching {
                profileUploadRepository.updateFeedPost(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mPostId,
                    postData
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> textFeedResponseMode.postValue(response?.body())
                    else -> uploadFailureText(response!!)

                }
            }.onFailure {
                textFeedResponseMThrowable.postValue("error")
            }
        }
    }

    fun delete(context: Context, file: File) {
        val where = MediaStore.MediaColumns.DATA + "=?"
        val selectionArgs = arrayOf(file.absolutePath)
        val contentResolver = context.contentResolver
        val filesUri = MediaStore.Files.getContentUri("external")
        if (file.exists()) {
            contentResolver.delete(filesUri, where, selectionArgs)
        }
    }
}

