package com.example.sample.socialmob.viewmodel.profile

import android.content.Context
import android.os.AsyncTask
import android.os.Environment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperPodCast
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.feed.UploadFeedResponseModel
import com.example.sample.socialmob.model.login.*
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.profile.*
import com.example.sample.socialmob.model.search.FollowResponseModel
import com.example.sample.socialmob.model.search.SearchProfileResponseModelPayloadProfile
import com.example.sample.socialmob.repository.dao.DbPodcastHistoryDao
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.article.ArticleDashBoardDao
import com.example.sample.socialmob.repository.dao.article.BookMarkedArticleDao
import com.example.sample.socialmob.repository.dao.article.HistoryArticleDao
import com.example.sample.socialmob.repository.dao.article.RecommendedArticleDao
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioDao
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioPodCastListDao
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.dao.search.SearchArticleDao
import com.example.sample.socialmob.repository.dao.search.SearchTagDao
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.ProfileRepository
import com.example.sample.socialmob.repository.utils.InternetUtil
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.repository.utils.SingleLiveEvent
import com.example.sample.socialmob.view.ui.profile.feed.ProfileFeedFragment
import com.example.sample.socialmob.view.utils.*
import com.example.sample.socialmob.view.utils.galleryPicker.model.GalleryData
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DbTrackProgress
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import com.example.sample.socialmob.view.utils.retrofit.ProgressRequestBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val profileRepository: ProfileRepository,
    private val application: Context,
    private var cacheMapper: CacheMapperTrack,
    private var cacheMapperPodCast: CacheMapperPodCast
) : ViewModel() {


    val profile: LiveData<Profile>
    val mProfileFeedList: MutableLiveData<ResultResponse<MutableList<PersonalFeedResponseModelPayloadFeed>>>? =
        MutableLiveData()

    private var loginDao: LoginDao? = null
    private var radioPodcastListDao: RadioPodCastListDao? = null
    private var radioDao: RadioDao? = null
    private var createplayListDao: CreateDbPlayList? = null
    private var searchArticleDao: SearchArticleDao? = null
    private var searchTagDao: SearchTagDao? = null
    private var historyArticleDao: HistoryArticleDao? = null
    private var bookMarkedArticleDao: BookMarkedArticleDao? = null
    private var recommendedArticleDao: RecommendedArticleDao? = null
    private var articleDashBoardDao: ArticleDashBoardDao? = null
    private var dbTrackProgressDao: DbTrackProgress? = null
    private var dbPodcastHistoryDao: DbPodcastHistoryDao? = null
    private var trackDownloadDao: TrackDownload? = null

    var userProfileResponseModel: MutableLiveData<UserProfileResponseModel>
    var followingsResponseModelPayload: MutableLiveData<MutableList<Following>>
    var myProfileData: MutableLiveData<Payload>
    var trackDetailsModel: MutableLiveData<TrackDetailsModel>
    private val mUpdateProfileResponseModel: MutableLiveData<UpdateProfileResponseModel>
    val mProfileThrowable: SingleLiveEvent<Throwable>
    val pennySendThrowable: MutableLiveData<Throwable>
    val pennySendResponseModel: MutableLiveData<PennySendResponseModel>
    val followResponseModel: MutableLiveData<FollowResponseModel>
    val reportProfileResponse: MutableLiveData<ForgotPasswordResponseModel>


    val followerPaginatedList: MutableLiveData<ResultResponse<MutableList<Following>>> =
        MutableLiveData()
    val followingsPaginatedList: MutableLiveData<ResultResponse<MutableList<Following>>> =
        MutableLiveData()
    val blockedUsersResponseModelPayloadUser: MutableLiveData<ResultResponse<MutableList<SearchProfileResponseModelPayloadProfile>>> =
        MutableLiveData()

    private var createDbPlayListDao: CreateDbPlayList? = null
    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null
    var mPersonalProfile: MutableLiveData<MutableList<PersonalFeedResponseModelPayloadFeed>> =
        MutableLiveData()

    var isSucessChangePasswordLiveData: MutableLiveData<Boolean> = MutableLiveData()
    var isLoadingApiChangePassword: Boolean = false
    var isSucessChangePassword: Boolean = false

    var isUploading: Boolean = false

    var podcastResponseModel: MutableLiveData<PodcastResponseModel>? = MutableLiveData()

    var recentPlayedTracks: MutableLiveData<ResultResponse<List<TrackListCommon>>>? =
        MutableLiveData()
    private var mFileGlobal: List<GalleryData>? = null
    var mVideoMedia: MutableLiveData<VideoMedia> = MutableLiveData()
    var isUploaded: MutableLiveData<Boolean> = MutableLiveData()
    private val viewModelJob = SupervisorJob()

    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)

        loginDao = habitRoomDatabase.loginDao()
        profile = loginDao?.getAllProfileData()!!
        createDbPlayListDao = habitRoomDatabase.createplayListDao()

        radioPodcastListDao = habitRoomDatabase.radioPodcastListDao()
        radioDao = habitRoomDatabase.radioDao()
        createplayListDao = habitRoomDatabase.createplayListDao()
        searchArticleDao = habitRoomDatabase.searchArticleDao()
        searchTagDao = habitRoomDatabase.searchTagDao()
        historyArticleDao = habitRoomDatabase.historyArticleDao()
        bookMarkedArticleDao = habitRoomDatabase.bookMarkedArticleDao()
        recommendedArticleDao = habitRoomDatabase.recommendedArticleDao()
        articleDashBoardDao = habitRoomDatabase.articleDashBoardDao()
        dbTrackProgressDao = habitRoomDatabase.trackProgress()
        dbPodcastHistoryDao = habitRoomDatabase.dbPodcastHistory()
        trackDownloadDao = habitRoomDatabase.trackDownloadDao()

        userProfileResponseModel = MutableLiveData()
        followingsResponseModelPayload = MutableLiveData()
        myProfileData = MutableLiveData()
        trackDetailsModel = MutableLiveData()
        mPersonalProfile = MutableLiveData()
        pennySendResponseModel = MutableLiveData()
        mUpdateProfileResponseModel = MutableLiveData()
        followResponseModel = MutableLiveData()
        reportProfileResponse = MutableLiveData()
        mProfileThrowable = SingleLiveEvent()
        pennySendThrowable = MutableLiveData()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()
    }

    fun getProfileThrowable(): SingleLiveEvent<Throwable> {
        return mProfileThrowable
    }

    fun getEditProfileResponse(): MutableLiveData<UpdateProfileResponseModel> {
        return mUpdateProfileResponseModel
    }

    fun getUserProfile(mAuth: String, mProfileId: String) {

        uiScope.launch(handler) {
            var response: Response<UserProfileResponseModel>? = null
            kotlin.runCatching {
                profileRepository.getUserProfile(mAuth, RemoteConstant.mPlatform, mProfileId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()) {
                    200 -> setValueProfile(response!!)
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "User Profile Api"
                    )
                }
            }.onFailure {
            }
        }
    }

    private fun setValueProfile(value: Response<UserProfileResponseModel>) {

        userProfileResponseModel.postValue(value.body())
    }

    fun getMyProfile(mAuth: String, mProfileId: String) {
        uiScope.launch(handler) {
            var response: Response<LoginResponse>? = null
            kotlin.runCatching {
                profileRepository.getMyProfile(mAuth, RemoteConstant.mPlatform, mProfileId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        saveProfileData(response!!)// myProfileData.postValue(response.body() ?.payload ?)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "User Profile Api"
                    )
                }
            }.onFailure {
                println("" + it)
            }
        }

    }


    private fun saveProfileData(response: Response<LoginResponse>) {
        uiScope.launch(Dispatchers.Main) {
            myProfileData.value = response.body()?.payload
            if (response.body()?.payload != null && response.body()?.payload?.profile != null) {
                saveProfile(response)
            }
        }
    }

    private fun saveProfile(value: Response<LoginResponse>) {
        SaveProfile(this.loginDao!!).execute(value.body()?.payload?.profile)
    }


    class SaveProfile internal constructor(private val mAsyncTaskDao: LoginDao) :
        AsyncTask<Profile, Void, Void>() {
        override fun doInBackground(vararg params: Profile): Void? {
            mAsyncTaskDao.deleteAllProfileData()
            mAsyncTaskDao.insertProfileData(params[0])
            return null
        }
    }

    fun getFollowersList(
        mAuth: String,
        mProfileId: String,
        mOffset: String,
        mCount: String,
        mLastRecordId: String
    ) {
        uiScope.launch(handler) {

            if (mOffset == "0") {
                followerPaginatedList.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                followerPaginatedList.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            var response: Response<FollowersResponseModel>? = null
            kotlin.runCatching {
                profileRepository.getFollowersList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mProfileId,
                    mLastRecordId,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload != null &&
                            response?.body()?.payload?.followers != null &&
                            response?.body()?.payload?.followers?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                followerPaginatedList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.followers!!
                                    )
                                )
                            } else {
                                followerPaginatedList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.followers)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                if (response?.body()?.payload?.message == "private content") {
                                    followerPaginatedList.postValue(
                                        ResultResponse.noDataWithMessage(
                                            ArrayList(), response?.body()?.payload?.message!!
                                        )
                                    )
                                } else {
                                    followerPaginatedList.postValue(
                                        ResultResponse.noData(
                                            ArrayList()
                                        )
                                    )
                                }
                            } else {
                                followerPaginatedList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        followerPaginatedList.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "FollowersList api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun getFollowingList(
        mAuth: String,
        mProfileId: String,
        mOffset: String,
        mCount: String
    ) {
        uiScope.launch(handler) {

            if (mOffset == "0") {
                followingsPaginatedList.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                followingsPaginatedList.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            var response: Response<FollowingResponseModel>? = null
            kotlin.runCatching {
                profileRepository.getFollowingList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mProfileId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload != null &&
                            response?.body()?.payload?.followings != null &&
                            response?.body()?.payload?.followings?.size!! > 0
                        ) {
                            if (mOffset == "0") {
                                followingsPaginatedList.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.followings!!
                                    )
                                )
                            } else {
                                followingsPaginatedList.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.followings)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                if (response?.body()?.payload?.message == "private content") {
                                    followingsPaginatedList.postValue(
                                        ResultResponse.noDataWithMessage(
                                            ArrayList(), response?.body()?.payload?.message!!
                                        )
                                    )
                                } else {
                                    followingsPaginatedList.postValue(
                                        ResultResponse.noData(
                                            ArrayList()
                                        )
                                    )

                                }
                            } else {
                                followingsPaginatedList.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        followingsPaginatedList.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "FollowingList Api"
                    )
                }
            }.onFailure {
                followingsPaginatedList.postValue(ResultResponse.error("", ArrayList()))
            }
        }

    }


    fun getTrackDetails(mAuth: String, profileAnthemTrackId: String?) {
        var response: Response<TrackDetailsModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.getTrackDetails(
                    mAuth,
                    RemoteConstant.mPlatform,
                    profileAnthemTrackId!!
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> trackDetailsModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "TrendingTrack api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun getPersonalFeed(
        mAuth: String, mProfileId: String, mOffset: String,
        mCount: String
    ) {
        uiScope.launch(handler) {

            if (mOffset == "0") {
                mProfileFeedList?.postValue(ResultResponse.loading(ArrayList(), mOffset))
            } else {
                mProfileFeedList?.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            var response: Response<PersonalFeedResponseModel>? = null
            kotlin.runCatching {
                profileRepository.getPersonalFeed(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mProfileId,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.feeds != null && response?.body()?.payload?.feeds?.size!! > 0) {
                            if (mOffset == "0") {
                                mProfileFeedList?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            } else {
                                mProfileFeedList?.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.feeds!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                mProfileFeedList?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                mProfileFeedList?.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        mProfileFeedList?.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> {
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "Personal Feed"
                        )
                    }
                }

            }.onFailure {
                mProfileFeedList?.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    fun likePost(mAuth: String, postId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.likePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Like Feed Post api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun disLikePost(mAuth: String, postId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.disLikePost(mAuth, RemoteConstant.mPlatform, postId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Like Feed Post"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun blockUser(mAuth: String, mProfileId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                profileRepository.blockUser(mAuth, RemoteConstant.mPlatform, mProfileId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {

                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Block User"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun unbBlockUser(mAuth: String, mProfileId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                profileRepository.unblockUser(mAuth, RemoteConstant.mPlatform, mProfileId)
                    .collect { response = it }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {

                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Un Block User api"
                    )
                }
            }.onFailure {
            }
        }
    }


    fun getBlockedUsers(mAuth: String, mOffset: String, mCount: String) {
        uiScope.launch(handler) {
            if (mOffset == "0") {
                blockedUsersResponseModelPayloadUser.postValue(
                    ResultResponse.loading(
                        ArrayList(), mOffset
                    )
                )
            } else {
                blockedUsersResponseModelPayloadUser.postValue(
                    ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
                )
            }
            var response: Response<BlockedUsersResponseModel>? = null
            kotlin.runCatching {
                profileRepository.getBlockedUsers(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.users != null && response?.body()?.payload?.users?.size!! > 0) {
                            if (mOffset == "0") {
                                blockedUsersResponseModelPayloadUser.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.users!!
                                    )
                                )
                            } else {
                                blockedUsersResponseModelPayloadUser.postValue(
                                    ResultResponse.paginatedList(
                                        response?.body()?.payload?.users!!
                                    )
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                blockedUsersResponseModelPayloadUser.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                blockedUsersResponseModelPayloadUser.postValue(
                                    ResultResponse.emptyPaginatedList(
                                        ArrayList()
                                    )
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        blockedUsersResponseModelPayloadUser.postValue(
                            ResultResponse.error(
                                "",
                                ArrayList()
                            )
                        )
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Blocked api"
                    )
                }
            }.onFailure {
                blockedUsersResponseModelPayloadUser.postValue(
                    ResultResponse.error(
                        "",
                        ArrayList()
                    )
                )
            }
        }
    }

    fun updateProfile(mAuth: String, editProfileBody: EditProfileBody) {

        //TODO : Update User Namr and Name in DB
        doAsync {
            loginDao?.upDateName(editProfileBody.name)
            loginDao?.upDateUserName(editProfileBody.username)
            loginDao?.upDateLocation(editProfileBody.location)
        }
        uiScope.launch(handler) {
            var response: Response<UpdateProfileResponseModel>? = null
            kotlin.runCatching {
                profileRepository.updateProfile(
                    mAuth,
                    RemoteConstant.mPlatform,
                    editProfileBody
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        doAsync {
                            loginDao?.upDateName(editProfileBody.name)
                            loginDao?.upDateUserName(editProfileBody.username)
                            loginDao?.upDateLocation(editProfileBody.location)
//                            loginDao?.upDateProfileImage(editProfileBody.filename)
                        }
                        succesResponse(response!!)
                    }
                    else -> failureResponse(response!!)

                }
            }.onFailure {
                mProfileThrowable.postValue(it)
            }
        }
    }

    private fun succesResponse(response: Response<UpdateProfileResponseModel>) {

        if (response.body() != null && response.body()?.payload?.success == true) {
            mUpdateProfileResponseModel.postValue(response.body())
            if (InternetUtil.isInternetOn()) {
                val mAuth = RemoteConstant.getAuthUser(
                    application, SharedPrefsUtils.getStringPreference(
                        application,
                        RemoteConstant.mProfileId, ""
                    )!!
                )
                uiScope.launch(handler) {
                    var response1: Response<LoginResponse>? = null
                    kotlin.runCatching {
                        profileRepository.getMyProfile(
                            mAuth,
                            RemoteConstant.mPlatform,
                            SharedPrefsUtils.getStringPreference(
                                application,
                                RemoteConstant.mProfileId,
                                ""
                            )!!
                        ).collect {
                            response1 = it
                        }
                    }.onSuccess {
                        when (response1?.code()) {
                            200 -> {
                                saveProfileData(response1!!)// myProfileData.postValue(response.body() ?.payload ?)
                            }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response1?.code()!!, "User Profile Api"
                            )
                        }
                    }.onFailure {
                        println("" + it)
                    }
                }
            }
        } else {
            mUpdateProfileResponseModel.postValue(response.body())
            if (response.body()?.payload?.message != null) {
                AppUtils.showCommonToast(application, response.body()?.payload?.message)
            }
        }


    }

    private fun failureResponse(response: Response<UpdateProfileResponseModel>) {
        val t: Throwable? = null
        mProfileThrowable.postValue(t)
        RemoteConstant.apiErrorDetails(
            application, response.code(), "updateProfile api"
        )
    }

    fun changePassword(
        mAuth: String, mOldPassword: String, mNewPassWord: String, mReenterNewPassWord: String
    ) {
        isLoadingApiChangePassword = true
        uiScope.launch(handler) {
            var response: Response<ChangePasswordResponseModel>? = null
            kotlin.runCatching {
                profileRepository.changePassword(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOldPassword,
                    mNewPassWord,
                    mReenterNewPassWord,
                    "0"
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        populateChangePasswordResponse(response!!)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Change Password api"
                    )
                }
            }.onFailure {
                isSucessChangePassword = false
            }
        }
    }

    private fun populateChangePasswordResponse(response: Response<ChangePasswordResponseModel>) {
        isLoadingApiChangePassword = false
        if (response.body() != null && response.body()?.payload != null) {
            if (response.body()?.payload?.message != null) {
                AppUtils.showCommonToast(application, response.body()?.payload?.message)
            }

            isSucessChangePassword = if (response.body()?.payload?.success == true) {
                AppUtils.showCommonToast(application, "Password changed successfully.")
                isSucessChangePasswordLiveData.postValue(true)
                true
            } else {
                isSucessChangePasswordLiveData.postValue(false)
                false
            }


        }

    }

    fun cancelRequest(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mCancelRequest: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                profileRepository.cancelFollowUser(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mCancelRequest = it
                    }
            }.onSuccess {
                when (mCancelRequest?.code()) {
                    200 -> doAsync { followResponseModel.postValue(mCancelRequest?.body()) }
                    else -> RemoteConstant.apiErrorDetails(
                        application, mCancelRequest?.code() ?: 500, "cancelRequest api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun unFollowProfile(mAuth: String, mId: String, mCount: String) {

        uiScope.launch(handler) {
            var mUnFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                profileRepository.unFollowProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect {
                        mUnFollowProfile = it
                    }
            }.onSuccess {
                when (mUnFollowProfile?.code()) {
                    200 -> {
                        followResponseModel.postValue(mUnFollowProfile?.body())
                        doAsync {
                            if (mCount != "") {
                                val mNewCount: Int = Integer.parseInt(mCount).minus(1)
                                loginDao?.upDateFollowersCount(mNewCount.toString())
                            }
                        }
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, mUnFollowProfile?.code()!!, "Un Follow Profile Api api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun followProfile(mAuth: String, mId: String) {
        uiScope.launch(handler) {
            var mFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                profileRepository.followProfile(mAuth, RemoteConstant.mPlatform, mId).collect {
                    mFollowProfile = it
                }
            }.onSuccess {
                when (mFollowProfile?.code()) {
                    200 -> followResponseModel.postValue(mFollowProfile?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, mFollowProfile?.code() ?: 500, "Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun deletePost(mId: String, mContext: Context) {
        val fullData = RemoteConstant.getEncryptedString(
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mAppId, "")!!,
            SharedPrefsUtils.getStringPreference(mContext, RemoteConstant.mApiKey, "")!!,
            RemoteConstant.mBaseUrl +
                    RemoteConstant.mPostPathV1 +
                    RemoteConstant.mSlash + mId, RemoteConstant.deleteMethod
        )
        val mAuth =
            RemoteConstant.frameWorkName +
                    SharedPrefsUtils.getStringPreference(
                        mContext,
                        RemoteConstant.mAppId,
                        ""
                    )!! +
                    ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                profileRepository.deletePost(mAuth, RemoteConstant.mPlatform, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> println()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Delete Post"
                    )
                }
            }.onFailure {
            }
        }

    }


    fun createDBPlayList(dbPlayList: List<TrackListCommon>?, playListName: String) {

        if (dbPlayList?.isNotEmpty()!!) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapper.mapFromEntityList(
                        dbPlayList.toMutableList(), playListName
                    )
                )
            }
        }
    }


    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                profileRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> doAsync { refreshPlayList(response!!) }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "get CreateDbPlayList Track"
                    )
                }
            }.onFailure {
            }
        }
    }


    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        AppUtils.showCommonToast(application, value.body()?.payload?.message)
//        refresh()
    }


    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null

        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.getPlayList(mAuth).collect {
                    getPlayListApi = it
                }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 ->
                        if (getPlayListApi?.body()?.payload?.playlists?.size!! > 0) {
                            playListLiveData?.postValue(ResultResponse.success(getPlayListApi?.body()?.payload?.playlists!!))
                        }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code()!!, "Play Api"
                    )
                }
            }.onFailure {
                val mAuthNew =
                    RemoteConstant.getAuthWithoutOffsetAndCount(
                        application, RemoteConstant.pathPlayList
                    )
                getPlayList(mAuthNew)
            }
        }
    }

    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody, mId: String) {
        var response: Response<CreatePlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.createPlayList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    createPlayListBody
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        val mAuthNew = RemoteConstant.getAuthWithoutOffsetAndCount(
                            application, "api/v1/profile/$mId/playlists"
                        )
                        getUserPlayList(mAuthNew, mId)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Error Occur please try again later"
                )
            }
        }
    }

    fun reportProfile(mAuth: String, mProfileId: String, mMessage: String) {
        uiScope.launch(handler) {
            var response: Response<ForgotPasswordResponseModel>? = null
            kotlin.runCatching {
                profileRepository.reportUser(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mProfileId,
                    mMessage
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> doAsync { reportProfileResponse.postValue(response?.body()) }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Report Profile Api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Error Occur please try again later"
                )
            }

        }
    }

    fun clearDataBase() {
        doAsync {
            loginDao?.deleteAllProfileData()
            radioPodcastListDao?.deleteAllRadioPodCastList()
            radioDao?.deleteAllRadio()
            createplayListDao?.deleteAllCreateDbPlayList()
            searchArticleDao?.deleteAllSearchArticle()
            searchTagDao?.deleteAllSearchTag()
            historyArticleDao?.deleteAllHistoryArticle()
            bookMarkedArticleDao?.deleteAllBookMarkedArticle()
            recommendedArticleDao?.deleteAllRecommendedArticle()
            articleDashBoardDao?.deleteAllArticleDashBoard()
            dbTrackProgressDao?.deleteAllDbTrackProgress()
            dbPodcastHistoryDao?.deleteAllDbPodcastHistory()
            trackDownloadDao?.deleteAllTrackDownload()
        }


    }


    fun getPreSignedUrl(
        mAuth: String,
        mType: String,
        mCount: String,
        mFile: List<GalleryData>,
        mThumb: ArrayList<File>?,
        mDescription: PostFeedData,
        ratio: String,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks
    ) {

        isUploading = true
        if (mType == "image" || mType == "video") {
            mFileGlobal = mFile
            uiScope.launch(handler) {
                profileRepository
                    .getPreSignedUrl(mAuth, RemoteConstant.mPlatform, mType, mCount)
                    .enqueue(object : Callback<ImagePreSignedUrlResponseModel> {
                        override fun onFailure(
                            call: Call<ImagePreSignedUrlResponseModel>?,
                            t: Throwable?
                        ) {

                        }

                        override fun onResponse(
                            call: Call<ImagePreSignedUrlResponseModel>?,
                            response: Response<ImagePreSignedUrlResponseModel>?
                        ) {
                            when (response?.code()) {
                                200 ->
                                    uiScope.launch(handler) {
                                        populateValue(
                                            response,
                                            mFile,
                                            mType,
                                            mThumb,
                                            mDescription,
                                            ratio,
                                            mUploadCallbacks
                                        )
                                    }

                                else -> {
                                    RemoteConstant.apiErrorDetails(
                                        application,
                                        response?.code()!!,
                                        "getPreSignedUrl api"
                                    )
                                    isUploading = false
                                    isUploaded.postValue(false)
                                }
                            }
                        }

                    })
            }
        }

    }

    suspend fun populateValue(
        mResponse: Response<ImagePreSignedUrlResponseModel>?,
        mFile: List<GalleryData>,
        mType: String,
        mThumb: ArrayList<File>?,
        mDescription: PostFeedData,
        ratio: String,
        mUploadCallbacks: ProgressRequestBody.UploadCallbacks
    ) {
        withContext(Dispatchers.IO) {
            if (mType == "image") {
                var uploadedList = 0
                mFileGlobal = mFile
                mVideoMedia.postValue(mResponse?.body()?.payload?.videoMedia)

                for (i in 0 until mFile.size) {

                    val mUrl = mResponse?.body()?.payload?.videoMedia?.rawFileUrls!![i]
                    val mRequestBody =
                        File(mFile[i].photoUri)
                            .asRequestBody("image/jpeg".toMediaTypeOrNull())

                    uiScope.launch(handler) {
                        profileRepository
                            .uploadImageProgressAmazon(mUrl, mRequestBody)
                            .enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(
                                    call: Call<ResponseBody>?,
                                    t: Throwable?
                                ) {

                                }

                                override fun onResponse(
                                    call: Call<ResponseBody>?,
                                    response: Response<ResponseBody>?
                                ) {
                                    when (response?.code()) {
                                        200 -> {
                                            uploadedList += 1
                                            if (uploadedList == mFile.size) {
                                                uiScope.launch(handler) {
                                                    uploadImageOrVideoPost(
                                                        mType,
                                                        mResponse,
                                                        mDescription,
                                                        ratio
                                                    )
                                                }
                                            }
                                        }
                                        else -> {
                                            RemoteConstant.apiErrorDetails(
                                                application,
                                                response?.code()!!,
                                                "Upload Image Api"
                                            )
                                            isUploading = false
                                            isUploaded.postValue(false)
                                        }

                                    }
                                }

                            })
                    }
                }


            } else {
//
                for (i in mFile.indices) {
                    val mUrlVideo = mResponse?.body()?.payload?.videoMedia?.rawFileUrls!![0]
                    val fileBody = ProgressRequestBody(
                        File(mFile[0].photoUri),
                        "video/mp4".toMediaTypeOrNull(),
                        mUploadCallbacks, ""
                    )

                    uiScope.launch(handler) {
                        profileRepository
                            .uploadImageProgressAmazon(mUrlVideo, fileBody)
                            .enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(
                                    call: Call<ResponseBody>?,
                                    t: Throwable?
                                ) {

                                }

                                override fun onResponse(
                                    call: Call<ResponseBody>?,
                                    response: Response<ResponseBody>?
                                ) {
                                    when (response?.code()) {
                                        200 -> {
                                            uiScope.launch(handler) {
                                                uploadImageOrVideoPost(
                                                    mType,
                                                    mResponse,
                                                    mDescription,
                                                    ratio
                                                )
                                                val mUrlImage =
                                                    mResponse.body()?.payload?.videoMedia?.thumbnailUrls!![i]
                                                val mRequestBodyImage =
                                                    mThumb!![i]
                                                        .asRequestBody("image/jpeg".toMediaTypeOrNull())

                                                uploadThumbnail(
                                                    mUrlImage,
                                                    mRequestBodyImage,
                                                    mType,
                                                    mResponse,
                                                    mDescription
                                                )
                                            }
                                        }
                                        else -> RemoteConstant.apiErrorDetails(
                                            application, response?.code()!!, "Upload video Api"
                                        )
                                    }
                                }

                            })
                    }
                }
            }

        }
    }


    private fun uploadThumbnail(
        mUrlImage: String, mRequestBodyImage: RequestBody, mType: String,
        mResponse: Response<ImagePreSignedUrlResponseModel>, mDescription: PostFeedData
    ) {


        uiScope.launch(handler) {
            profileRepository
                .uploadImageProgressAmazon(mUrlImage, mRequestBodyImage)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<ResponseBody>?,
                        response: Response<ResponseBody>?
                    ) {
                        when (response?.code()) {
                            200 -> println()
                            else -> RemoteConstant.apiErrorDetails(
                                application, response?.code()!!, "Upload video Thumbnail Api"
                            )
                        }
                    }

                })
        }

    }

    suspend fun uploadImageOrVideoPost(
        mType: String, value: Response<ImagePreSignedUrlResponseModel>,
        mDescription: PostFeedData, ratio: String
    ) {
        withContext(Dispatchers.IO) {

            val fullData = RemoteConstant.getEncryptedString(
                SharedPrefsUtils.getStringPreference(application, RemoteConstant.mAppId, "")!!,
                SharedPrefsUtils.getStringPreference(
                    application,
                    RemoteConstant.mApiKey,
                    ""
                )!!,
                RemoteConstant.mBaseUrl + RemoteConstant.createFeedPost,
                RemoteConstant.postMethod
            )
            val mAuth =
                RemoteConstant.frameWorkName + SharedPrefsUtils.getStringPreference(
                    application,
                    RemoteConstant.mAppId,
                    ""
                ) + ":" + fullData[2] + ":" + fullData[0] + ":" + fullData[1]

            var postData: PostFeedData
            postData = mDescription
            postData.type = mType
//        postData.description = mDescription


            val mediaList: ArrayList<PostFeedDataMedia> = arrayListOf()

            for (i in 0 until value.body()?.payload?.videoMedia?.rawFileNames?.size!!) {
                val mediaData = PostFeedDataMedia()
                mediaData.filename = value.body()?.payload?.videoMedia?.rawFileNames!![i]
                if (mType == "video") {
                    mediaData.thumbnailFilename =
                        value.body()?.payload?.videoMedia?.thumbnailFileNames!![i]
                    mediaData.ratio = ratio
                }
                if (mType == "image") {
//                val options = BitmapFactory.Options()
//                options.inJustDecodeBounds = true
//                BitmapFactory.decodeFile(File(mFileGlobal ?[i].photoUri).absolutePath, options)
//                val imageHeight = options.outHeight
//                val imageWidth = options.outWidth
//
//                mediaData.height = imageHeight.toString()
//                mediaData.width = imageWidth.toString()
                    val size = SizeFromImage(mFileGlobal!![i].photoUri)
                    mediaData.height = size.width().toString()
                    mediaData.width = size.height().toString()
                    mediaData.ratio = ratio

                } else {
                    try {
                        val size = SizeFromVideoFile(mFileGlobal!![i].photoUri)
                        mediaData.height = size.width().toString()
                        mediaData.width = size.height().toString()

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                }
                mediaList.add(mediaData)
            }

            postData.media = mediaList
            uiScope.launch(handler) {
                var response: Response<UploadFeedResponseModel>? = null
                kotlin.runCatching {
                    profileRepository.createFeedPost(mAuth, RemoteConstant.mPlatform, postData)
                        .collect {
                            response = it
                        }
                }.onSuccess {
                    when (response?.code()) {
                        200 -> {
                            isUploading = false
                            isUploaded.postValue(true)
                            ProfileFeedFragment.idPost =
                                response?.body()?.payload?.post?.id!!
                            doAsync {
                                for (i in 0 until mFileGlobal?.size!!) {
                                    delete(
                                        File(mFileGlobal!![i].photoUri)
                                    )
                                }
                            }
                        }
                        else -> {
                            isUploading = false
                            isUploaded.postValue(false)
                        }

                    }
                }.onFailure {
                }
            }
        }

    }

    fun delete(file: File) {

        val dir = File(Environment.getExternalStoragePublicDirectory("socialmob"), "")
        if (dir.isDirectory) {
            val children = dir.list()
            for (i in children?.indices!!) {
                File(dir, children[i]).delete()
            }
        }
        if (dir.exists()) {
            dir.delete()

        }

        val fileDelete =
            File(file.absolutePath)
        fileDelete.delete()
        if (fileDelete.exists()) {
            fileDelete.canonicalFile.delete()
            if (file.exists()) {
                MyApplication.application?.deleteFile(
                    fileDelete.name
                )
            }
        }
    }

    var playListLiveData: MutableLiveData<ResultResponse<MutableList<PlaylistCommon>>>? =
        MutableLiveData()

    fun getUserPlayList(mAuth: String, mId: String) {
        var response: Response<PlayListSearchResponse>? = null
        uiScope.launch(handler) {
            playListLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))
            kotlin.runCatching {
                profileRepository.getUserPlayList(mAuth, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.playlists?.size!! > 0) {
                            playListLiveData?.postValue(ResultResponse.success(response?.body()?.payload?.playlists!!))
                        } else {
                            playListLiveData?.postValue(ResultResponse.noData(ArrayList()))
                        }
                    }
                    502, 522, 523, 500 -> {
                        playListLiveData?.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> {
                        playListLiveData?.postValue(ResultResponse.error("", ArrayList()))
                    }
                }
            }.onFailure {
                playListLiveData?.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    fun getFavouriteTracks(mAuth: String, mOffset: String, mCount: String) {
        uiScope.launch(handler) {
            getFavouriteTracksApi(mAuth, mOffset, mCount)

        }

    }

    private fun getFavouriteTracksApi(mAuth: String, mOffset: String, mCount: String) {
        if (mOffset == "0") {
            recentPlayedTracks?.postValue(ResultResponse.loading(ArrayList(), mOffset))
        } else {
            recentPlayedTracks?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }
        var response: Response<FavouriteTrackModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.getFavouriteTracks(
                    mAuth,
                    RemoteConstant.mPlatform, /*mProfileId,*/
                    mOffset,
                    mCount
                ).collect {
                    response = it
                }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload?.favorites != null && response?.body()?.payload?.favorites?.size!! > 0) {
                            if (mOffset == "0") {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.success(response?.body()?.payload?.favorites!!)
                                )
                            } else {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.favorites!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                recentPlayedTracks?.postValue(ResultResponse.noData(ArrayList()))
                            } else {
                                recentPlayedTracks?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        recentPlayedTracks?.postValue(ResultResponse.error("", ArrayList()))
                    }
                    else -> {
                        recentPlayedTracks?.postValue(ResultResponse.error("", ArrayList()))
                    }

                }
            }.onFailure {
                recentPlayedTracks?.postValue(ResultResponse.error("", ArrayList()))
            }
        }
    }

    fun deletePlayList(mAuth: String, mId: String) {
        var response: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.deletePlayList(mAuth, mId).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
//                                val mAuth = RemoteConstant.getAuthWithoutOffsetAndCount(
//                                    application, "api/v1/profile/$mId/playlists"
//                                )
//                                getUserPlayList(mAuth, mId)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Delete Play List Api"
                    )
                }
            }.onFailure {
            }
        }

    }

    fun createDbPlayListModelNotification(
        dbPlayList: MutableList<TrackListCommon>?,
        playListName: String
    ) {
        if (dbPlayList!!.size > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapper.mapFromEntityList(
                        dbPlayList, playListName
                    )
                )
            }
        }
    }
    fun getPodCastDetails(mAuth: String, mSongId: String) {
        var response: Response<PodcastResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                profileRepository.getPodCastDetails(mAuth, RemoteConstant.mPlatform, mSongId)
                    .collect {
                        response = it
                    }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        podcastResponseModel?.postValue(response?.body())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "TTrack api"
                    )
                }
            }.onFailure {
            }
        }

    }

    fun createDbPlayListModelNotificationPodcast(
        dbPlayList: MutableList<RadioPodCastEpisode>?, playListName: String
    ) {
        if (dbPlayList?.size ?: 0 > 0) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(
                    cacheMapperPodCast.mapFromEntityListPodCast(
                        dbPlayList ?: ArrayList(), playListName
                    )
                )
            }
        }
    }
}


