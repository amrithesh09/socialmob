package com.example.sample.socialmob.viewmodel.search

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.di.util.CacheMapperTrack
import com.example.sample.socialmob.model.chat.ConvList
import com.example.sample.socialmob.model.chat.ConversationListResponse
import com.example.sample.socialmob.model.feed.TopItemsNetworkResponse
import com.example.sample.socialmob.model.feed.TopItemsNetworkResponsePayload
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.music.*
import com.example.sample.socialmob.model.search.*
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.dao.search.SearchTagDao
import com.example.sample.socialmob.repository.model.PlayListDataClass
import com.example.sample.socialmob.repository.model.TrackListCommonDb
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.remote.BackEndApi
import com.example.sample.socialmob.repository.remote.WebServiceClient
import com.example.sample.socialmob.repository.repo.SearchRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.repository.utils.ResultResponse
import com.example.sample.socialmob.view.utils.AppUtils
import com.example.sample.socialmob.view.utils.playlistcore.model.CreatePlayListBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchRepository: SearchRepository,
    private var application: Context,
    private var cacheMapper: CacheMapperTrack
) : ViewModel() {

    var searchTagDao: SearchTagDao? = null
    val playListModel: MutableLiveData<List<PlaylistCommon>>? = MutableLiveData()

    var searchTagLiveData: LiveData<List<SearchTagResponseModelPayloadTag>>


    private var createDbPlayListDao: CreateDbPlayList? = null
    var createDbPlayListModel: LiveData<List<PlayListDataClass>>? = null

    var paginatedArticleList: MutableLiveData<MutableList<SearchArticleResponseModelPayloadContent>> =
        MutableLiveData()
    var articleList: MutableList<SearchArticleResponseModelPayloadContent>? = ArrayList()
    var isLoadCompletedArticle: Boolean = false
    var isLoadingArticle: Boolean = false


    var paginatedTagList: MutableLiveData<MutableList<SearchTagResponseModelPayloadTag>> =
        MutableLiveData()
    var tagList: MutableList<SearchTagResponseModelPayloadTag>? = ArrayList()
    var isLoadCompletedTag: Boolean = false
    var isLoadingTag: Boolean = false

    val profile: LiveData<Profile>
    private var loginDao: LoginDao? = null

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    private val viewModelJobSearch = SupervisorJob()
    private val uiScopeSearch = CoroutineScope(Dispatchers.IO + viewModelJobSearch)


    var topItemsMusicLiveData: MutableLiveData<ResultResponse<TopItemsMusicPayload>>? =
        MutableLiveData()
    var topItemsNetworkLiveData: MutableLiveData<ResultResponse<TopItemsNetworkResponsePayload>>? =
        MutableLiveData()
    var tracksListLiveData: MutableLiveData<ResultResponse<MutableList<TrackListCommon>>>? =
        MutableLiveData()
    var playListLiveData: MutableLiveData<ResultResponse<MutableList<PlaylistCommon>>>? =
        MutableLiveData()
    var artistLiveData: MutableLiveData<ResultResponse<MutableList<Artist>>>? =
        MutableLiveData()
    var searchHashTagLiveData: MutableLiveData<ResultResponse<MutableList<HashTag>>>? =
        MutableLiveData()
    var searchProfileLiveData: MutableLiveData<ResultResponse<MutableList<SearchProfileResponseModelPayloadProfile>>>? =
        MutableLiveData()
    private var trackDownloadDao: TrackDownload? = null
    var downloadFalseTrackListModel: LiveData<MutableList<TrackListCommonDb>>? = null

    private var mSearchTopNetworkCall: Response<TopItemsNetworkResponse>? = null
    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {

        val habitRoomDatabase = SocialMobDatabase.getDatabase(application)
        searchTagDao = habitRoomDatabase.searchTagDao()
        searchTagLiveData = searchTagDao?.getAllSearchTag()!!

        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        createDbPlayListModel = createDbPlayListDao?.getAllCreateDbPlayList()!!

        loginDao = habitRoomDatabase.loginDao()
        profile = loginDao?.getAllProfileData()!!

        trackDownloadDao = habitRoomDatabase.trackDownloadDao()
        downloadFalseTrackListModel = trackDownloadDao?.getTrackDownloadedFalse("false")!!
    }


    fun followProfile(mAuth: String, mId: String, mFollowingCount: String) {
        uiScope.launch(handler) {
            var mFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                searchRepository.followProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect { mFollowProfile = it }
            }.onSuccess {
                when (mFollowProfile?.code()) {
                    200 ->
                        if (mFollowProfile?.body()?.payload?.message != "request pending") {
                            val mCountNew = Integer.parseInt(mFollowingCount).plus(1)
                            updateFollowing(mCountNew.toString())
                        }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mFollowProfile?.code() ?: 500,
                        "Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application,
                    "Something went wrong please try again later"
                )
            }
        }
    }

    fun unFollowProfile(mAuth: String, mId: String, mFollowingCount: String) {
        uiScope.launch(handler) {
            var mUnFollowProfile: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                searchRepository.unFollowProfile(mAuth, RemoteConstant.mPlatform, mId)
                    .collect { mUnFollowProfile = it }
            }.onSuccess {
                when (mUnFollowProfile?.code()) {
                    200 -> doAsync {
                        val mCountNew = Integer.parseInt(mFollowingCount).minus(1)
                        updateFollowing(mCountNew.toString())
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mUnFollowProfile?.code() ?: 500,
                        "Un Follow Profile Api api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Something went wrong please try again later"
                )
            }
        }
    }

    fun followTag(mAuth: String, mId: String) {

        uiScope.launch {
            searchRepository
                .followTag(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { updateDbSearchFollowTag("true", mId) }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Follow Tag  api"
                            )
                        }
                    }

                })

        }

    }


    private fun updateDbSearchFollowTag(mValue: String, mId: String) {
        doAsync { searchTagDao!!.updateFollowing(mValue, mId) }
//        AppUtils.showCommonToast(application, value.body()!!.payload!!.message!!)
    }

    fun unFollowTag(mAuth: String, mId: String) {

        uiScope.launch {
            searchRepository
                .unFollowTag(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { updateDbSearchUnFollowTag("false", mId) }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Un Follow Tag  api"
                            )
                        }
                    }

                })

        }

    }

    private fun updateDbSearchUnFollowTag(mValue: String, mId: String) {
        doAsync { searchTagDao!!.updateFollowing(mValue, mId) }
//        AppUtils.showCommonToast(application, value!!.body()!!.payload!!.message!!)
    }

    fun addToBookmark(mAuth: String, mId: String) {

        uiScope.launch {
            searchRepository
                .addToBookmark(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> {

                            }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Follow Tag api"
                            )
                        }
                    }

                })
        }
    }


    fun removeBookmark(mAuth: String, mId: String) {

        uiScope.launch {
            searchRepository
                .removeBookmark(mAuth, RemoteConstant.mPlatform, mId)
                .enqueue(object : Callback<FollowResponseModel> {
                    override fun onFailure(call: Call<FollowResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<FollowResponseModel>?,
                        response: Response<FollowResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> {

                            }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "remove bookmark api"
                            )
                        }
                    }

                })
        }
    }

    var mApiSearchUsers: Job? = null

    fun searchProfile(mAuth: String, mOffset: String, mCount: String, mString: String) {

        if (mOffset == "0") {
            searchProfileLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            searchProfileLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        if (mApiSearchUsers != null && !mApiSearchUsers?.isCancelled!!) {
            mApiSearchUsers?.cancel()
        }
        if (searchTopNetworkJob != null && !searchTopNetworkJob?.isCancelled!!) {
            searchTopNetworkJob?.cancel()
        }

        mApiSearchUsers = uiScopeSearch.launch(handler) {
            var response: Response<SearchProfileResponseModel>? = null
            kotlin.runCatching {
                searchRepository.searchProfile(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOffset,
                    mCount,
                    mString
                ).collect { response = it }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.profiles != null && response?.body()?.payload?.profiles?.size!! > 0) {
                            if (mOffset == "0") {
                                searchProfileLiveData?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.profiles!!
                                    )
                                )
                            } else {
                                searchProfileLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.profiles)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                searchProfileLiveData?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                searchProfileLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        searchProfileLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        searchProfileLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "search profile "
                        )
                    }

                }
            }.onFailure {
                if (mApiSearchUsers == null)
                    searchProfileLiveData?.postValue(
                        ResultResponse.error("", ArrayList())
                    )
            }
        }
    }


    fun searchTrack(mAuth: String, mOffset: String, mCount: String, mString: String) {
        if (mOffset == "0") {
            tracksListLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            tracksListLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }
        var response: Response<SearchTrackResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.searchTrack(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mString,
                    mOffset,
                    mCount
                ).collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.tracks != null && response?.body()?.payload?.tracks?.size!! > 0) {
                            if (mOffset == "0") {
                                tracksListLiveData?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.tracks!!
                                    )
                                )
                            } else {
                                tracksListLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.tracks)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                tracksListLiveData?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                tracksListLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        tracksListLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        tracksListLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application,
                            response?.code() ?: 502,
                            "search Top List Music"
                        )
                    }

                }
            }.onFailure {
                tracksListLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }


    fun searchArticle(mAuth: String, mOffset: String, mCount: String, mString: String) {
        isLoadingArticle = true
        uiScope.launch {
            searchRepository
                .searchArticle(mAuth, RemoteConstant.mPlatform, mOffset, mCount, mString)
                .enqueue(object : Callback<SearchArticleResponseModel> {
                    override fun onFailure(call: Call<SearchArticleResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<SearchArticleResponseModel>?,
                        response: Response<SearchArticleResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { populateSearchArticle(response) }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Search Article api"
                            )
                        }
                    }

                })

        }

    }

    private fun populateSearchArticle(response: Response<SearchArticleResponseModel>?) {

        isLoadingArticle = false
        isLoadCompletedArticle = response!!.body()!!.payload!!.contents!!.isEmpty()

        if (response.body()!!.payload!!.contents!!.isNotEmpty()) {
            articleList!!.addAll(response.body()!!.payload!!.contents as MutableList)
        }
        paginatedArticleList.postValue(articleList)
    }


    fun searchHashTags(mAuth: String, mOffset: String, mCount: String, mString: String) {

        if (mOffset == "0") {
            searchHashTagLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            searchHashTagLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }
        uiScope.launch(handler) {
            var response: Response<SearchHashTagResponseModel>? = null
            kotlin.runCatching {
                searchRepository.searchHashTags(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mOffset,
                    mCount,
                    mString
                ).collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.hashTags != null && response?.body()?.payload?.hashTags?.size!! > 0) {
                            if (mOffset == "0") {
                                searchHashTagLiveData?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.hashTags!!
                                    )
                                )
                            } else {
                                searchHashTagLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.hashTags!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                searchHashTagLiveData?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                searchHashTagLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        searchHashTagLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        searchHashTagLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "search Top List Music"
                        )
                    }

                }
            }.onFailure {
                searchHashTagLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )

            }
        }
    }

    fun searchTags(mAuth: String, mOffset: String, mCount: String, mString: String) {
        isLoadingTag = true
        uiScope.launch {
            searchRepository
                .searchTags(mAuth, RemoteConstant.mPlatform, mOffset, mCount, mString)
                .enqueue(object : Callback<SearchTagResponseModel> {
                    override fun onFailure(call: Call<SearchTagResponseModel>?, t: Throwable?) {

                    }

                    override fun onResponse(
                        call: Call<SearchTagResponseModel>?,
                        response: Response<SearchTagResponseModel>?
                    ) {
                        when (response!!.code()) {
                            200 -> doAsync { populateTags(response) }
                            else -> RemoteConstant.apiErrorDetails(
                                application, response.code(), "Search Tag Api"
                            )
                        }
                    }

                })

        }

    }

    private fun populateTags(response: Response<SearchTagResponseModel>) {
//        saveTag(response, mOffset)
        isLoadingTag = false
        isLoadCompletedTag = response.body()!!.payload!!.tags!!.isEmpty()

        if (response.body()!!.payload!!.tags!!.isNotEmpty()) {
            tagList!!.addAll(response.body()!!.payload!!.tags as MutableList)
        }
        paginatedTagList.postValue(tagList)
    }

    fun cancelRequest(mAuth: String, mId: String) {

        uiScope.launch(handler) {
            var mCancelRequest: Response<FollowResponseModel>? = null
            kotlin.runCatching {
                searchRepository.cancelFollowUser(mAuth, RemoteConstant.mPlatform, mId)
                    .collect { mCancelRequest = it }
            }.onSuccess {
                when (mCancelRequest?.code()) {
                    200 -> {

                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        mCancelRequest?.code() ?: 500,
                        "cancelRequest api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun addTrackToPlayList(mAuth: String, mPlayListId: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<AddPlayListResponseModel>? = null
            kotlin.runCatching {
                searchRepository.addTrackToPlayList(mAuth, mTrackId, mPlayListId)
                    .collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refreshPlayList(response!!)
                    else -> RemoteConstant.apiErrorDetails(
                        application,
                        response?.code() ?: 500,
                        "get CreateDbPlayList Track"
                    )
                }
            }.onFailure {
            }
        }
    }


    private fun refreshPlayList(value: Response<AddPlayListResponseModel>) {
        AppUtils.showCommonToast(application, value.body()?.payload?.message)
//        refresh()
    }

    private fun refresh() {
        val mAuthPlayList =
            RemoteConstant.getAuthWithoutOffsetAndCount(
                application,
                RemoteConstant.pathPlayList
            )
        getPlayList(mAuthPlayList)
    }

    fun getPlayList(mAuth: String) {
        var getPlayListApi: Response<PlayListResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.getPlayList(mAuth).collect { getPlayListApi = it }
            }.onSuccess {
                when (getPlayListApi?.code()) {
                    200 -> {
                        playListModel?.postValue(getPlayListApi?.body()?.payload?.playlists)
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, getPlayListApi?.code() ?: 500, "PlayList Api"
                    )
                }
            }.onFailure {
                val mAuthNew =
                    RemoteConstant.getAuthWithoutOffsetAndCount(
                        application,
                        RemoteConstant.pathPlayList
                    )
                getPlayList(mAuthNew)
            }
        }
    }


    fun createPlayList(mAuth: String, createPlayListBody: CreatePlayListBody) {

        uiScope.launch(handler) {
            var response: Response<CreatePlayListResponseModel>? = null
            kotlin.runCatching {
                searchRepository.createPlayList(mAuth, RemoteConstant.mPlatform, createPlayListBody)
                    .collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> refresh()
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Create Play List Api"
                    )
                }
            }.onFailure {
                AppUtils.showCommonToast(
                    application, "Error occur please try again later"
                )
            }
        }
    }

    fun createDBPlayList(dbPlayList: List<TrackListCommon>?, playListName: String) {
        if (dbPlayList != null && dbPlayList.isNotEmpty() && dbPlayList[0].numberId != null) {
            doAsync {
                createDbPlayListDao?.deleteAllCreateDbPlayList()
                createDbPlayListDao?.insertCreateDbPlayList(cacheMapper.mapFromEntityList(
                    dbPlayList.toMutableList(),playListName))
            }
        }
    }

    private fun updateFollowing(mCountNew: String) {
        doAsync {
            loginDao?.upDateFollowersCount(mCountNew)
        }
    }


    fun searchTopListMusic(mAuth: String) {

        topItemsMusicLiveData?.postValue(ResultResponse.loading(TopItemsMusicPayload(), ""))

        uiScope.launch(handler) {
            var response: Response<TopItemsMusic>? = null
            kotlin.runCatching {
                searchRepository.searchTopItemsMusic(mAuth, RemoteConstant.mPlatform)
                    .collect { response = it }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        topItemsMusicLiveData?.postValue(ResultResponse.success(response?.body()?.payload!!))
                    }
                    502, 522, 523, 500 -> {
                        topItemsMusicLiveData?.postValue(
                            ResultResponse.error("", TopItemsMusicPayload())
                        )
                    }
                    else -> {

                        RemoteConstant.apiErrorDetails(
                            application,
                            response?.code() ?: 500,
                            "search Top List Music"
                        )
                    }
                }
            }.onFailure {
                topItemsMusicLiveData?.postValue(
                    ResultResponse.error("", TopItemsMusicPayload())
                )
            }
        }
    }

    var searchTopNetworkJob: Job? = null
    fun searchTopNetwork(mAuth: String) {
        topItemsNetworkLiveData?.postValue(
            ResultResponse.loading(TopItemsNetworkResponsePayload(), "")
        )
        searchTopNetworkJob = uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.searchTopNetwork(mAuth, RemoteConstant.mPlatform)
                    .collect { mSearchTopNetworkCall = it }
            }.onSuccess {
                when (mSearchTopNetworkCall?.code()) {
                    200 -> {
                        topItemsNetworkLiveData?.postValue(
                            ResultResponse.success(
                                mSearchTopNetworkCall?.body()?.payload!!
                            )
                        )
                    }
                    502, 522, 523, 500 -> {
                        topItemsNetworkLiveData?.postValue(
                            ResultResponse.error("", TopItemsNetworkResponsePayload())
                        )
                    }
                    else -> {
                        topItemsNetworkLiveData?.postValue(
                            ResultResponse.error("", TopItemsNetworkResponsePayload())
                        )
                        RemoteConstant.apiErrorDetails(
                            application,
                            mSearchTopNetworkCall?.code()!!,
                            "search Top List network"
                        )
                    }
                }

            }.onFailure {
                println("" + it)
            }
        }
    }


    fun searchPlayList(mAuth: String, mOffset: String, mCount: String, mSearchWord: String) {
        if (mOffset == "0") {
            playListLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            playListLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        var response: Response<PlayListSearchResponse>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.searchPlayList(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mSearchWord,
                    mOffset,
                    mCount
                ).collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.playlists != null && response?.body()?.payload?.playlists?.size!! > 0) {
                            if (mOffset == "0") {
                                playListLiveData?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.playlists!!
                                    )
                                )
                            } else {
                                playListLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.playlists!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                playListLiveData?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                playListLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        playListLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        playListLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "search Top List Music"
                        )
                    }

                }
            }.onFailure {
                playListLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun searchArtist(mAuth: String, mOffset: String, mCount: String, mSearchWord: String) {

        if (mOffset == "0") {
            artistLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            artistLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        var response: Response<ArtistSearchResponse>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.searchArtist(
                    mAuth,
                    RemoteConstant.mPlatform,
                    mSearchWord,
                    mOffset,
                    mCount
                ).collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.artists != null && response?.body()?.payload?.artists?.size!! > 0) {
                            if (mOffset == "0") {
                                artistLiveData?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.artists!!
                                    )
                                )
                            } else {
                                artistLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.artists!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                artistLiveData?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                artistLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        artistLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        artistLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "search Top List Music"
                        )
                    }

                }
            }.onFailure {
                artistLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun addToFavourites(mAuth: String, mTrackId: String) {
        uiScope.launch(handler) {
            var response: Response<Any>? = null
            kotlin.runCatching {
                searchRepository.addFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId)
                    .collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }

    }

    fun removeFavourites(mAuth: String, mTrackId: String) {
        var response: Response<Any>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.removeFavouriteTrack(mAuth, RemoteConstant.mPlatform, mTrackId)
                    .collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        println("sucesss")
                    }
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code() ?: 500, "Add track to favorites"
                    )
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    fun getAllArtist(mAuth: String?, mOffset: String, mCount: String) {

        if (mOffset == "0") {
            artistLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            artistLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }

        var response: Response<ArtistSearchResponse>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.getAllArtist(mAuth!!, RemoteConstant.mPlatform, mOffset, mCount)
                    .collect { response = it }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.artists != null && response?.body()?.payload?.artists?.size!! > 0) {
                            if (mOffset == "0") {
                                artistLiveData?.postValue(
                                    ResultResponse.success(
                                        response?.body()?.payload?.artists!!
                                    )
                                )
                            } else {
                                artistLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.artists!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                artistLiveData?.postValue(
                                    ResultResponse.noData(
                                        ArrayList()
                                    )
                                )
                            } else {
                                artistLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        artistLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        artistLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application,
                            response?.code() ?: 502,
                            "search Top List Music"
                        )
                    }

                }
            }.onFailure {
                artistLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun playListAll(mAuth: String, mOffset: String, mCount: String) {
        if (mOffset == "0") {
            playListLiveData?.postValue(ResultResponse.loading(ArrayList(), ""))

        } else {
            playListLiveData?.postValue(
                ResultResponse.loadingPaginatedList(ArrayList(), mOffset)
            )
        }
        var response: Response<PlayListSearchResponse>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                searchRepository.playListAll(mAuth, RemoteConstant.mPlatform, mOffset, mCount)
                    .collect { response = it }
            }.onSuccess {
                when (response?.code()) {
                    200 -> {
                        if (response?.body()?.payload != null && response?.body()?.payload?.playlists != null && response?.body()?.payload?.playlists?.size!! > 0) {
                            if (mOffset == "0") {
                                playListLiveData?.postValue(ResultResponse.success(response?.body()?.payload?.playlists!!))
                            } else {
                                playListLiveData?.postValue(
                                    ResultResponse.paginatedList(response?.body()?.payload?.playlists!!)
                                )
                            }
                        } else {
                            if (mOffset == "0") {
                                playListLiveData?.postValue(ResultResponse.noData(ArrayList()))
                            } else {
                                playListLiveData?.postValue(
                                    ResultResponse.emptyPaginatedList(ArrayList())
                                )
                            }
                        }
                    }
                    502, 522, 523, 500 -> {
                        playListLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        playListLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code() ?: 500, "Play list all API"
                        )
                    }
                }

            }.onFailure {
                playListLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun addToDownload(mTrackDetails: TrackListCommon) {
        val list: MutableList<TrackListCommonDb> = ArrayList()
        val trackListCommonDbModel: TrackListCommonDb = TrackListCommonDb(
            mTrackDetails.id!!,
            mTrackDetails.numberId!!,
            mTrackDetails.trackName!!,
            mTrackDetails.media?.coverFile!!,
            mTrackDetails.media?.trackFile!!,
            mTrackDetails.artist?.artistName!!,
            mTrackDetails.genre?.genreName!!,
            "false", "false"
        )


        list.add(trackListCommonDbModel)
        doAsync {
            trackDownloadDao?.insertTrackDownload(list)
        }
    }

    private val serviceApiChat =
        WebServiceClient.clientChat.create(BackEndApi::class.java)// get instance

    val getAllChatResponseModel: MutableLiveData<ResultResponse<MutableList<ConvList>>>? =
        MutableLiveData()

    fun searchUsers(nJwt: String, mSearchWord: String) {

        uiScope.launch(handler) {
            var response: Response<ConversationListResponse>? = null
            kotlin.runCatching {
                response = serviceApiChat
                    .searchChatUsers(nJwt, RemoteConstant.mPlatform, mSearchWord)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {
                        if (response?.body() != null &&
                            response?.body()?.convList != null &&
                            response?.body()?.convList?.size!! > 0
                        ) {

                            getAllChatResponseModel?.postValue(
                                ResultResponse.success(
                                    response?.body()?.convList!!
                                )
                            )
                        } else {
                            getAllChatResponseModel?.postValue(
                                ResultResponse.noData(ArrayList())
                            )
                        }
                    }

                    502, 522, 523, 500 -> {
                        getAllChatResponseModel?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        getAllChatResponseModel?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "search profile chat"
                        )
                    }
                }
            }.onFailure {
                getAllChatResponseModel?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }

    fun getNewChatUsers(nJwt: String) {

        uiScope.launch(handler) {

            var response: Response<SearchProfileResponseModel>? = null

            kotlin.runCatching {
                response = serviceApiChat
                    .getChatSuggestion(nJwt, RemoteConstant.mPlatform)
            }.onSuccess {
                when (response!!.code()) {
                    200 -> {

                        if (response?.body()?.payload != null && response?.body()?.payload?.profiles != null && response?.body()?.payload?.profiles?.size!! > 0) {
                            searchProfileLiveData?.postValue(
                                ResultResponse.success(
                                    response?.body()?.payload?.profiles!!
                                )
                            )
                        } else {
                            searchProfileLiveData?.postValue(
                                ResultResponse.noData(
                                    ArrayList()
                                )
                            )
                        }
                    }
                    502, 522, 523, 500 -> {
                        searchProfileLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                    }
                    else -> {
                        searchProfileLiveData?.postValue(
                            ResultResponse.error("", ArrayList())
                        )
                        RemoteConstant.apiErrorDetails(
                            application, response?.code()!!, "search profile "
                        )
                    }
                }
            }.onFailure {
                searchProfileLiveData?.postValue(
                    ResultResponse.error("", ArrayList())
                )
            }
        }
    }


}