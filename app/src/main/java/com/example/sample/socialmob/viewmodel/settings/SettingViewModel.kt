package com.example.sample.socialmob.viewmodel.settings

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.socialmob.model.login.Profile
import com.example.sample.socialmob.model.profile.NotificationResponseModel
import com.example.sample.socialmob.repository.dao.DbPodcastHistoryDao
import com.example.sample.socialmob.repository.dao.SocialMobDatabase
import com.example.sample.socialmob.repository.dao.article.ArticleDashBoardDao
import com.example.sample.socialmob.repository.dao.article.BookMarkedArticleDao
import com.example.sample.socialmob.repository.dao.article.HistoryArticleDao
import com.example.sample.socialmob.repository.dao.article.RecommendedArticleDao
import com.example.sample.socialmob.repository.dao.login.LoginDao
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioDao
import com.example.sample.socialmob.repository.dao.music_dashboard.RadioPodCastListDao
import com.example.sample.socialmob.repository.dao.music_dashboard.TrackDownload
import com.example.sample.socialmob.repository.dao.search.SearchArticleDao
import com.example.sample.socialmob.repository.dao.search.SearchTagDao
import com.example.sample.socialmob.repository.playlist.CreateDbPlayList
import com.example.sample.socialmob.repository.repo.SettingRepository
import com.example.sample.socialmob.repository.utils.RemoteConstant
import com.example.sample.socialmob.view.utils.playlistcore.components.playlisthandler.DbTrackProgress
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.jetbrains.anko.doAsync
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SettingViewModel @Inject constructor(
    private val settingRepository: SettingRepository,
    private var application: Context
) : ViewModel() {

    var notificationResponseModel: MutableLiveData<NotificationResponseModel> = MutableLiveData()
    var privacyResponseModel: MutableLiveData<NotificationResponseModel> = MutableLiveData()
    var loginDao: LoginDao? = null

    private var radioPodcastListDao: RadioPodCastListDao? = null
    private var radioDao: RadioDao? = null
    private var createplayListDao: CreateDbPlayList? = null
    private var searchArticleDao: SearchArticleDao? = null
    private var searchTagDao: SearchTagDao? = null
    private var historyArticleDao: HistoryArticleDao? = null
    private var bookMarkedArticleDao: BookMarkedArticleDao? = null
    private var recommendedArticleDao: RecommendedArticleDao? = null
    private var articleDashBoardDao: ArticleDashBoardDao? = null
    private var createDbPlayListDao: CreateDbPlayList? = null
    private var dbTrackProgressDao: DbTrackProgress? = null
    private var dbPodcastHistoryDao: DbPodcastHistoryDao? = null
    private var trackDownloadDao: TrackDownload? = null
    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    val profile: LiveData<Profile>

    val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    init {
        val habitRoomDatabase: SocialMobDatabase = SocialMobDatabase.getDatabase(application)

        loginDao = habitRoomDatabase.loginDao()
        createDbPlayListDao = habitRoomDatabase.createplayListDao()
        radioPodcastListDao = habitRoomDatabase.radioPodcastListDao()
        radioDao = habitRoomDatabase.radioDao()
        createplayListDao = habitRoomDatabase.createplayListDao()
        searchArticleDao = habitRoomDatabase.searchArticleDao()
        searchTagDao = habitRoomDatabase.searchTagDao()
        historyArticleDao = habitRoomDatabase.historyArticleDao()
        bookMarkedArticleDao = habitRoomDatabase.bookMarkedArticleDao()
        recommendedArticleDao = habitRoomDatabase.recommendedArticleDao()
        articleDashBoardDao = habitRoomDatabase.articleDashBoardDao()
        dbTrackProgressDao = habitRoomDatabase.trackProgress()
        dbPodcastHistoryDao = habitRoomDatabase.dbPodcastHistory()
        trackDownloadDao = habitRoomDatabase.trackDownloadDao()

        profile = loginDao?.getAllProfileData()!!
    }

    fun onPushNotification(mAuth: String) {
        uiScope.launch(handler) {
            var response: Response<NotificationResponseModel>? = null
            kotlin.runCatching {
                settingRepository.onPushNotification(mAuth, RemoteConstant.mPlatform).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> notificationResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "On Push Notification api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun offPushNotification(mAuth: String) {
        var response: Response<NotificationResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                settingRepository.offPushNotification(mAuth, RemoteConstant.mPlatform).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> notificationResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "Off Push Notification api"
                    )
                }
            }.onFailure {
            }
        }

    }

    fun changeToPublicAccount(mAuth: String) {
        uiScope.launch(handler) {
            var response: Response<NotificationResponseModel>? = null
            kotlin.runCatching {
                settingRepository.changeToPublicAccount(mAuth, RemoteConstant.mPlatform).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> privacyResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "changeToPublicAccount api"
                    )
                }
            }.onFailure {
            }
        }
    }

    fun changeToPrivateAccount(mAuth: String) {
        var response: Response<NotificationResponseModel>? = null
        uiScope.launch(handler) {
            kotlin.runCatching {
                settingRepository.changeToPrivateAccount(mAuth, RemoteConstant.mPlatform).collect {
                    response = it
                }
            }.onSuccess {
                when (response!!.code()) {
                    200 -> privacyResponseModel.postValue(response?.body())
                    else -> RemoteConstant.apiErrorDetails(
                        application, response?.code()!!, "changeToPrivateAccount api"
                    )
                }
            }.onFailure {
            }
        }

    }

    fun clearDataBase() {
        doAsync {
            loginDao?.deleteAllProfileData()
            radioPodcastListDao?.deleteAllRadioPodCastList()
            radioDao?.deleteAllRadio()
            createplayListDao?.deleteAllCreateDbPlayList()
            searchArticleDao?.deleteAllSearchArticle()
            searchTagDao?.deleteAllSearchTag()
            historyArticleDao?.deleteAllHistoryArticle()
            bookMarkedArticleDao?.deleteAllBookMarkedArticle()
            recommendedArticleDao?.deleteAllRecommendedArticle()
            articleDashBoardDao?.deleteAllArticleDashBoard()
            dbTrackProgressDao?.deleteAllDbTrackProgress()
            dbPodcastHistoryDao?.deleteAllDbPodcastHistory()
            trackDownloadDao?.deleteAllTrackDownload()
            dbPodcastHistoryDao?.deleteAllDbPodcastHistory()
        }
    }

    fun clearDataBaseWithoutProfile() {
        doAsync {
            radioPodcastListDao?.deleteAllRadioPodCastList()
            radioDao?.deleteAllRadio()
            createplayListDao?.deleteAllCreateDbPlayList()
            searchArticleDao?.deleteAllSearchArticle()
            searchTagDao?.deleteAllSearchTag()
            historyArticleDao?.deleteAllHistoryArticle()
            bookMarkedArticleDao?.deleteAllBookMarkedArticle()
            recommendedArticleDao?.deleteAllRecommendedArticle()
            articleDashBoardDao?.deleteAllArticleDashBoard()
            dbTrackProgressDao?.deleteAllDbTrackProgress()
            trackDownloadDao?.deleteAllTrackDownload()
            dbPodcastHistoryDao?.deleteAllDbPodcastHistory()
        }
    }
}