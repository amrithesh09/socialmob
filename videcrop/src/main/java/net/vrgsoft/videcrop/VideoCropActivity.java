package net.vrgsoft.videcrop;

import android.Manifest;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.exoplayer2.util.Util;

import net.vrgsoft.videcrop.cropview.window.CropVideoView;
import net.vrgsoft.videcrop.ffmpeg.FFmpeg;
import net.vrgsoft.videcrop.ffmpeg.FFtask;
import net.vrgsoft.videcrop.player.VideoPlayer;
import net.vrgsoft.videcrop.view.ProgressView;
import net.vrgsoft.videcrop.view.VideoSliceSeekBarH;

import java.io.File;
import java.util.Formatter;
import java.util.Locale;


public class VideoCropActivity extends AppCompatActivity implements VideoPlayer.OnProgressUpdateListener, VideoSliceSeekBarH.SeekBarChangeListener {
    private static final String VIDEO_CROP_INPUT_PATH = "VIDEO_CROP_INPUT_PATH";
    private static final String VIDEO_CROP_OUTPUT_PATH = "VIDEO_CROP_OUTPUT_PATH";
    private static final String VIDEO_CROP_OUTPUT_PATH_NEW = "VIDEO_CROP_OUTPUT_PATHNEW";
    private static final String IS_FROM = "IS_FROM";
    private static final int STORAGE_REQUEST = 100;

    private VideoPlayer mVideoPlayer;
    private StringBuilder formatBuilder;
    private Formatter formatter;

    private AppCompatImageView mIvPlay;
    //    private AppCompatImageView mIvAspectRatio;
    private ImageView video_trimmer_back_image_view;
    private TextView mIvDone;
    private VideoSliceSeekBarH mTmbProgress;
    private CropVideoView mCropVideoView;
    private TextView mTvProgress;
    private TextView mTvDuration;
    private TextView mTvAspectCustom;
    private TextView mTvAspectSquare;
    private TextView mTvAspectPortrait;
    private TextView mTvAspectLandscape;
    private TextView mTvAspect4by3;
    private TextView mTvAspect16by9;
    private TextView mTvCropProgress;
    private TextView title_text_view;
    private View mAspectMenu;
    private ProgressView mProgressBar;

    private String inputPath;
    private String outputPath;
    private String outputPathNew;
    private String mIsFrom;
    private boolean isVideoPlaying = false;
    private boolean isAspectMenuShown = false;
    private FFtask mFFTask;
    private FFmpeg mFFMpeg;

    private ConstraintLayout main_constrain;
    private Button ratio11btn;
    private Button ratio43btn;
    private Button ratio169btn;
    private String mVideoRatio;
    private Dialog dialog;

    public static Intent createIntent(Context context, String inputPath, String outputPath, String outputPathNew, String mIsFrom) {
        Intent intent = new Intent(context, VideoCropActivity.class);
        intent.putExtra(VIDEO_CROP_INPUT_PATH, inputPath);
        intent.putExtra(VIDEO_CROP_OUTPUT_PATH, outputPath);
        intent.putExtra(VIDEO_CROP_OUTPUT_PATH_NEW, outputPathNew);
        intent.putExtra(IS_FROM, mIsFrom);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeStatusBarColor();
        setContentView(R.layout.activity_crop_video_ffmpeg);

        formatBuilder = new StringBuilder();
        formatter = new Formatter(formatBuilder, Locale.getDefault());

        inputPath = getIntent().getStringExtra(VIDEO_CROP_INPUT_PATH);
        outputPath = getIntent().getStringExtra(VIDEO_CROP_OUTPUT_PATH);
        outputPathNew = getIntent().getStringExtra(VIDEO_CROP_OUTPUT_PATH_NEW);
        mIsFrom = getIntent().getStringExtra(IS_FROM);

        if (TextUtils.isEmpty(inputPath) || TextUtils.isEmpty(outputPath)) {
            Toast.makeText(this, "input and output paths must be valid and not null", Toast.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
            finish();
        }

        findViews();
        initListeners();

        requestStoragePermission();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#000000"));
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initPlayer(inputPath);
                } else {
                    Toast.makeText(this, "You must grant a write storage permission to use this functionality", Toast.LENGTH_SHORT).show();
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isVideoPlaying) {
            mVideoPlayer.play(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mVideoPlayer != null) {
            mVideoPlayer.play(false);
        }
    }

    @Override
    public void onDestroy() {

        if (mFFTask != null && !mFFTask.isProcessCompleted()) {
            mFFTask.sendQuitSignal();
        }
        if (mFFMpeg != null) {
            mFFMpeg.deleteFFmpegBin();
        }
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (mVideoPlayer != null) {
            mVideoPlayer.play(false);
        }
        if (mVideoPlayer != null) {
            mVideoPlayer.release();
        }
        super.onDestroy();
    }

    @Override
    public void onFirstTimeUpdate(long duration, long currentPosition) {
        mTmbProgress.setSeekBarChangeListener(this);
        mTmbProgress.setMaxValue(duration);
        mTmbProgress.setLeftProgress(0);
        mTmbProgress.setRightProgress(duration);
        mTmbProgress.setProgressMinDiff(0);
    }

    @Override
    public void onProgressUpdate(long currentPosition, long duration, long bufferedPosition) {
        mTmbProgress.videoPlayingProgress(currentPosition);
        if (!mVideoPlayer.isPlaying() || currentPosition >= mTmbProgress.getRightProgress()) {
            if (mVideoPlayer.isPlaying()) {
                playPause();
            }
        }

        mTmbProgress.setSliceBlocked(false);
        mTmbProgress.removeVideoStatusThumb();

//        mTmbProgress.setPosition(currentPosition);
//        mTmbProgress.setBufferedPosition(bufferedPosition);
//        mTmbProgress.setDuration(duration);
    }

    private void findViews() {
        mCropVideoView = findViewById(R.id.cropVideoView);
        mIvPlay = findViewById(R.id.ivPlay);
//        mIvAspectRatio = findViewById(R.id.ivAspectRatio);
        mIvDone = findViewById(R.id.ivDone);
        mTvProgress = findViewById(R.id.tvProgress);
        mTvDuration = findViewById(R.id.tvDuration);
        mTmbProgress = findViewById(R.id.tmbProgress);
        mAspectMenu = findViewById(R.id.aspectMenu);
        mTvAspectCustom = findViewById(R.id.tvAspectCustom);
        mTvAspectSquare = findViewById(R.id.tvAspectSquare);
        mTvAspectPortrait = findViewById(R.id.tvAspectPortrait);
        mTvAspectLandscape = findViewById(R.id.tvAspectLandscape);
        mTvAspect4by3 = findViewById(R.id.tvAspect4by3);
        mTvAspect16by9 = findViewById(R.id.tvAspect16by9);
        mProgressBar = findViewById(R.id.pbCropProgress);
        mTvCropProgress = findViewById(R.id.tvCropProgress);
        title_text_view = findViewById(R.id.title_text_view);
        video_trimmer_back_image_view = findViewById(R.id.video_trimmer_back_image_view);


        main_constrain = findViewById(R.id.main_constrain);
        ratio11btn = findViewById(R.id.ratio11btn);
        ratio43btn = findViewById(R.id.ratio43btn);
        ratio169btn = findViewById(R.id.ratio169btn);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/montheavy.otf");
        Typeface faceLacBlack = Typeface.createFromAsset(getAssets(), "fonts/latoblack.ttf");
        title_text_view.setTypeface(face);
        mIvDone.setTypeface(face);
        ratio11btn.setTypeface(faceLacBlack);
        ratio43btn.setTypeface(faceLacBlack);
        ratio169btn.setTypeface(faceLacBlack);

        mTvDuration.setTypeface(faceLacBlack);
        mTvProgress.setTypeface(faceLacBlack);

    }

    private void initListeners() {

        video_trimmer_back_image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ratio11btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(10, 10);
//                handleMenuVisibility();
                ratio11btn.setBackgroundResource(R.drawable.round_ratio_button);
                ratio43btn.setBackgroundResource(0);
                ratio169btn.setBackgroundResource(0);
                mVideoRatio = "1:1";
            }
        });
        ratio43btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(4, 3);
//                handleMenuVisibility();
                ratio11btn.setBackgroundResource(0);
                ratio43btn.setBackgroundResource(R.drawable.round_ratio_button);
                ratio169btn.setBackgroundResource(0);
                mVideoRatio = "4:3";
            }
        });
        ratio169btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(16, 9);
//                handleMenuVisibility();
                ratio11btn.setBackgroundResource(0);
                ratio43btn.setBackgroundResource(0);
                ratio169btn.setBackgroundResource(R.drawable.round_ratio_button);
                mVideoRatio = "16:9";

            }
        });
        mIvPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPause();
            }
        });
//        mIvAspectRatio.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                handleMenuVisibility();
//            }
//        });
        mTvAspectCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropVideoView.setFixedAspectRatio(false);
                handleMenuVisibility();
            }
        });
        mTvAspectSquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(10, 10);
                handleMenuVisibility();
            }
        });
        mTvAspectPortrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(8, 16);
                handleMenuVisibility();
            }
        });
        mTvAspectLandscape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(16, 8);
                handleMenuVisibility();
            }
        });
        mTvAspect4by3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(4, 3);
                handleMenuVisibility();
            }
        });
        mTvAspect16by9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropVideoView.setFixedAspectRatio(true);
                mCropVideoView.setAspectRatio(16, 9);
                handleMenuVisibility();
            }
        });
        mIvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                main_constrain.setClickable(false);
//                main_constrain.setEnabled(false);
//                dialog = new Dialog(VideoCropActivity.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                if (dialog.getWindow() != null) {
//                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                }
//
//                dialog.setCancelable(false);
//                dialog.show();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                handleCropStart();
                            }
                        }, 1000);

            }
        });

        ratio11btn.performClick();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void playPause() {
        isVideoPlaying = !mVideoPlayer.isPlaying();
        if (mVideoPlayer.isPlaying()) {
            mVideoPlayer.play(!mVideoPlayer.isPlaying());
            mTmbProgress.setSliceBlocked(false);
            mTmbProgress.removeVideoStatusThumb();
            mIvPlay.setImageResource(R.drawable.ic_play);
            return;
        }
        mVideoPlayer.seekTo(mTmbProgress.getLeftProgress());
        mVideoPlayer.play(!mVideoPlayer.isPlaying());
        mTmbProgress.videoPlayingProgress(mTmbProgress.getLeftProgress());
        mIvPlay.setImageResource(R.drawable.ic_pause);
    }

    private void initPlayer(String uri) {
        if (!new File(uri).exists()) {
            Toast.makeText(this, "File doesn't exists", Toast.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        mVideoPlayer = new VideoPlayer(this);
        mCropVideoView.setPlayer(mVideoPlayer.getPlayer());
        mVideoPlayer.initMediaSource(this, uri);
        mVideoPlayer.setUpdateListener(this);

        fetchVideoInfo(uri);
    }

    private void fetchVideoInfo(String uri) {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(new File(uri).getAbsolutePath());
            int videoWidth = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
            int videoHeight = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
            int rotationDegrees = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));

            mCropVideoView.initBounds(videoWidth, videoHeight, rotationDegrees);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleMenuVisibility() {
        isAspectMenuShown = !isAspectMenuShown;
        TimeInterpolator interpolator;
        if (isAspectMenuShown) {
            interpolator = new DecelerateInterpolator();
        } else {
            interpolator = new AccelerateInterpolator();
        }
        mAspectMenu.animate()
                .translationY(isAspectMenuShown ? 0 : Resources.getSystem().getDisplayMetrics().density * 400)
                .alpha(isAspectMenuShown ? 1 : 0)
                .setInterpolator(interpolator)
                .start();
    }

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST);
        } else {
            initPlayer(inputPath);
        }
    }

    @SuppressLint("DefaultLocale")
    private void handleCropStart() {
        Rect cropRect = mCropVideoView.getCropRect();
        long startCrop = mTmbProgress.getLeftProgress();
        long durationCrop = mTmbProgress.getRightProgress() - mTmbProgress.getLeftProgress();
        String start = Util.getStringForTime(formatBuilder, formatter, startCrop);
        String duration = Util.getStringForTime(formatBuilder, formatter, durationCrop);
        start += "." + startCrop % 1000;
        duration += "." + durationCrop % 1000;

        mFFMpeg = FFmpeg.getInstance(this);
        if (mFFMpeg.isSupported()) {
            //https://www.ostechnix.com/20-ffmpeg-commands-beginners/
//            https://androiddeveloperhelp.wordpress.com/2015/12/19/video-editing-android-example/
            String crop = String.format("crop=%d:%d:%d:%d:exact=0", cropRect.right, cropRect.bottom, cropRect.left, cropRect.top);
            String[] cmdNew = {"-y", "-ss", start, "-i", inputPath, "-t",
                    duration, "-vf", crop, "-strict", "experimental", "-vcodec", "libx264", "-crf", "24", "-acodec", "aac",
                    "-preset", "slow", "-profile", "baseline", outputPath};

            Intent intent = new Intent();
            intent.putExtra("RATIO", mVideoRatio);
            intent.putExtra("FFMPEG_CMD", cmdNew);
            intent.putExtra("DURATION_CROP", durationCrop);
            setResult(RESULT_OK, intent);
//                Log.e("onSuccess", message);
            finish();

//            mFFTask = mFFMpeg.execute(cmdNew, new ExecuteBinaryResponseHandler() {
//                @Override
//                public void onSuccess(String message) {
//                    Intent intent = new Intent();
//                    intent.putExtra("RATIO", mVideoRatio);
//                    setResult(RESULT_OK, intent);
//                    Log.e("onSuccess", message);
//                    finish();
//                }
//
//                @Override
//                public void onProgress(String message) {
//                    Log.e("onProgress", message);
//                }
//
//                @Override
//                public void onFailure(String message) {
//                    Toast.makeText(VideoCropActivity.this, "Failed to crop!", Toast.LENGTH_SHORT).show();
//                    Log.e("onFailure", message);
//                    dialog.dismiss();
//                }
//
//                @Override
//                public void onProgressPercent(float percent) {
//                    if (percent <= 100) {
//                        mProgressBar.setProgress((int) percent);
//                        mTvCropProgress.setText((int) percent + "%");
//                    }
//                }
//
//                @Override
//                public void onStart() {
//                    mIvDone.setEnabled(false);
//                    mIvPlay.setEnabled(false);
//                    mProgressBar.setVisibility(View.VISIBLE);
//                    mProgressBar.setProgress(0);
//                    mTvCropProgress.setVisibility(View.VISIBLE);
//                    mTvCropProgress.setText("0%");
//                }
//
//                @Override
//                public void onFinish() {
//                    mIvDone.setEnabled(true);
//                    mIvPlay.setEnabled(true);
//                    mProgressBar.setVisibility(View.INVISIBLE);
//                    mProgressBar.setProgress(0);
//                    mTvCropProgress.setVisibility(View.INVISIBLE);
//                    mTvCropProgress.setText("0%");
////                    Toast.makeText(VideoCropActivity.this, "FINISHED", Toast.LENGTH_SHORT).show();
//                }
//            }, durationCrop * 1.0f / 1000);
        }
    }

    @Override
    public void seekBarValueChanged(long leftThumb, long rightThumb) {
        if (mTmbProgress.getSelectedThumb() == 1) {
            mVideoPlayer.seekTo(leftThumb);
        }

        mTvDuration.setText(Util.getStringForTime(formatBuilder, formatter, rightThumb));
        mTvProgress.setText(Util.getStringForTime(formatBuilder, formatter, leftThumb));
    }
}
